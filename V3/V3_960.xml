<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">urticaceae</taxon_name>
    <taxon_name authority="Gaudichaud Beaupré" date="1830" rank="genus">LAPORTEA</taxon_name>
    <place_of_publication>
      <publication_title>Voy. Uranie</publication_title>
      <place_in_publication>12: 498. 1830, name conserved</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family urticaceae;genus LAPORTEA</taxon_hierarchy>
    <other_info_on_name type="etymology">For F.L. de Laporte de Castelnau, leader of expeditions to South America</other_info_on_name>
    <other_info_on_name type="fna_id">117613</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Heister ex Fabricius" date="unknown" rank="genus">Urticastrum</taxon_name>
    <taxon_hierarchy>genus Urticastrum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or perennial, with stinging and nonstinging hairs on same plant.</text>
      <biological_entity id="o16673" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o16674" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="toxicity" src="d0_s0" value="stinging" value_original="stinging" />
      </biological_entity>
      <biological_entity id="o16675" name="plant" name_original="plant" src="d0_s0" type="structure" />
      <relation from="o16673" id="r2288" name="with" negation="false" src="d0_s0" to="o16674" />
      <relation from="o16674" id="r2289" name="on" negation="false" src="d0_s0" to="o16675" />
    </statement>
    <statement id="d0_s1">
      <text>Stems simple, erect.</text>
      <biological_entity id="o16676" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate;</text>
      <biological_entity id="o16677" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules present.</text>
      <biological_entity id="o16678" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blades narrowly ovate to orbiculate, margins dentate or serrate;</text>
      <biological_entity id="o16679" name="leaf-blade" name_original="leaf-blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="narrowly ovate" name="shape" src="d0_s4" to="orbiculate" />
      </biological_entity>
      <biological_entity id="o16680" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cystoliths rounded.</text>
      <biological_entity id="o16681" name="cystolith" name_original="cystoliths" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences axillary and terminal, of paniculately arranged cymes.</text>
      <biological_entity id="o16682" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o16683" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="paniculately" name="arrangement" src="d0_s6" value="arranged" value_original="arranged" />
      </biological_entity>
      <relation from="o16682" id="r2290" name="part_of" negation="false" src="d0_s6" to="o16683" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers unisexual, proximal panicles staminate and distal pistillate, or staminate and pistillate flowers in same panicle;</text>
      <biological_entity id="o16684" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="unisexual" value_original="unisexual" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o16685" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="position_or_shape" src="d0_s7" value="distal" value_original="distal" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o16686" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o16687" name="panicle" name_original="panicle" src="d0_s7" type="structure" />
      <relation from="o16686" id="r2291" name="in" negation="false" src="d0_s7" to="o16687" />
    </statement>
    <statement id="d0_s8">
      <text>bracts absent.</text>
      <biological_entity id="o16688" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Staminate flowers: tepals 4-5, distinct, equal;</text>
      <biological_entity id="o16689" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o16690" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s9" to="5" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 4-5;</text>
      <biological_entity id="o16691" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o16692" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pistillode knoblike.</text>
      <biological_entity id="o16693" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o16694" name="pistillode" name_original="pistillode" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="knoblike" value_original="knoblike" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pistillate flowers: tepals 2-4, distinct, outer pair minute or absent, without hooked hairs;</text>
      <biological_entity id="o16695" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o16696" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s12" to="4" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity constraint="outer" id="o16697" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="minute" value_original="minute" />
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o16698" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="hooked" value_original="hooked" />
      </biological_entity>
      <relation from="o16697" id="r2292" name="without" negation="false" src="d0_s12" to="o16698" />
    </statement>
    <statement id="d0_s13">
      <text>staminodes absent;</text>
      <biological_entity id="o16699" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o16700" name="staminode" name_original="staminodes" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style persistent even in fruit, hooklike or elongate;</text>
      <biological_entity id="o16701" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o16702" name="style" name_original="style" src="d0_s14" type="structure">
        <character constraint="in fruit" constraintid="o16703" is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s14" value="hooklike" value_original="hooklike" />
        <character is_modifier="false" name="shape" src="d0_s14" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o16703" name="fruit" name_original="fruit" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>stigma extended along style.</text>
      <biological_entity id="o16704" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o16705" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character constraint="along style" constraintid="o16706" is_modifier="false" name="size" src="d0_s15" value="extended" value_original="extended" />
      </biological_entity>
      <biological_entity id="o16706" name="style" name_original="style" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Achenes stipitate, laterally compressed, ±orbicular, free from and not enclosed by perianth.</text>
      <biological_entity id="o16708" name="perianth" name_original="perianth" src="d0_s16" type="structure" />
      <relation from="o16707" id="r2293" name="enclosed by" negation="false" src="d0_s16" to="o16708" />
    </statement>
    <statement id="d0_s17">
      <text>x=13.</text>
      <biological_entity id="o16707" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s16" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s16" value="orbicular" value_original="orbicular" />
      </biological_entity>
      <biological_entity constraint="x" id="o16709" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="13" value_original="13" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, West Indies, Central America, South America, Asia, Africa, Pacific Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Species 22 (2 in the flora).</discussion>
  <discussion>Most species of Laportea occur in Africa and Madagascar.</discussion>
  <references>
    <reference>Chew, W.L. 1969. A monograph of Laportea (Urticaceae). Gard. Bull. Straits Settlem. 25: 111-178.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants with stinging hairs and stipitate-glandular hairs; base of leaf blade rounded or abruptly attenuate, auriculate; achenes less than 1.5mm.</description>
      <determination>1 Laportea aestuans</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants with stinging hairs but without stipitate-glandular hairs; base of leaf blade rounded, truncate, or broadly cuneate, not auriculate; achenes 2mm or more.</description>
      <determination>2 Laportea canadensis</determination>
    </key_statement>
  </key>
</bio:treatment>