<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="de Candolle" date="1824" rank="section">Hecatonia</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">sceleratus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 551. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section hecatonia;species sceleratus;</taxon_hierarchy>
    <other_info_on_name type="special_status">W2</other_info_on_name>
    <other_info_on_name type="fna_id">200008144</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hecatonia</taxon_name>
    <taxon_name authority="(Linnaeus) Fourreau" date="unknown" rank="species">scelerata</taxon_name>
    <taxon_hierarchy>genus Hecatonia;species scelerata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, glabrous, rooting at base, only very rarely rooting at proximal nodes.</text>
      <biological_entity id="o25913" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character constraint="at base" constraintid="o25914" is_modifier="false" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
        <character constraint="at proximal nodes" constraintid="o25915" is_modifier="false" modifier="only very rarely" name="architecture" notes="" src="d0_s0" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o25914" name="base" name_original="base" src="d0_s0" type="structure" />
      <biological_entity constraint="proximal" id="o25915" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline, basal and proximal cauline leaf-blades reniform to semicircular in outline, 3-lobed or parted, 1-5 × 1.6-6.8 cm, base truncate to cordate, segments usually again lobed or parted, sometimes undivided, margins crenate or crenate-lobulate, apex rounded or occasionally obtuse.</text>
      <biological_entity id="o25916" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="basal and proximal cauline" id="o25917" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" constraint="in base, segments, margins, apex" constraintid="o25918, o25919, o25920, o25921" from="reniform" name="shape" src="d0_s1" to="semicircular" />
      </biological_entity>
      <biological_entity id="o25918" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="true" name="shape" src="d0_s1" value="parted" value_original="parted" />
        <character char_type="range_value" from="1" from_unit="cm" is_modifier="true" name="length" src="d0_s1" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.6" from_unit="cm" is_modifier="true" name="width" src="d0_s1" to="6.8" to_unit="cm" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s1" to="cordate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s1" value="parted" value_original="parted" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s1" value="undivided" value_original="undivided" />
        <character is_modifier="false" name="shape" src="d0_s1" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="crenate-lobulate" value_original="crenate-lobulate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s1" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o25919" name="segment" name_original="segments" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="true" name="shape" src="d0_s1" value="parted" value_original="parted" />
        <character char_type="range_value" from="1" from_unit="cm" is_modifier="true" name="length" src="d0_s1" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.6" from_unit="cm" is_modifier="true" name="width" src="d0_s1" to="6.8" to_unit="cm" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s1" to="cordate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s1" value="parted" value_original="parted" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s1" value="undivided" value_original="undivided" />
        <character is_modifier="false" name="shape" src="d0_s1" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="crenate-lobulate" value_original="crenate-lobulate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s1" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o25920" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="true" name="shape" src="d0_s1" value="parted" value_original="parted" />
        <character char_type="range_value" from="1" from_unit="cm" is_modifier="true" name="length" src="d0_s1" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.6" from_unit="cm" is_modifier="true" name="width" src="d0_s1" to="6.8" to_unit="cm" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s1" to="cordate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s1" value="parted" value_original="parted" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s1" value="undivided" value_original="undivided" />
        <character is_modifier="false" name="shape" src="d0_s1" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="crenate-lobulate" value_original="crenate-lobulate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s1" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o25921" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="true" name="shape" src="d0_s1" value="parted" value_original="parted" />
        <character char_type="range_value" from="1" from_unit="cm" is_modifier="true" name="length" src="d0_s1" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.6" from_unit="cm" is_modifier="true" name="width" src="d0_s1" to="6.8" to_unit="cm" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s1" to="cordate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s1" value="parted" value_original="parted" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s1" value="undivided" value_original="undivided" />
        <character is_modifier="false" name="shape" src="d0_s1" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="crenate-lobulate" value_original="crenate-lobulate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s1" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: receptacle pubescent or glabrous;</text>
      <biological_entity id="o25922" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o25923" name="receptacle" name_original="receptacle" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sepals 3-5, reflexed from or near base, 2-5 × 1-3 mm, glabrous or sparsely hirsute;</text>
      <biological_entity id="o25924" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o25925" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="5" />
        <character constraint="from or near base" constraintid="o25926" is_modifier="false" name="orientation" src="d0_s3" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" notes="" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" notes="" src="d0_s3" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o25926" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>petals 3-5, 2-5 × 1-3 mm;</text>
      <biological_entity id="o25927" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o25928" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="5" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s4" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>nectary on petal surface, scale poorly developed and forming crescent-shaped or circular ridge surrounding but not covering nectary;</text>
      <biological_entity id="o25929" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o25930" name="nectary" name_original="nectary" src="d0_s5" type="structure" />
      <biological_entity constraint="petal" id="o25931" name="surface" name_original="surface" src="d0_s5" type="structure" />
      <biological_entity id="o25932" name="scale" name_original="scale" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="poorly" name="development" src="d0_s5" value="developed" value_original="developed" />
        <character is_modifier="false" name="position_relational" src="d0_s5" value="surrounding" value_original="surrounding" />
      </biological_entity>
      <biological_entity id="o25933" name="ridge" name_original="ridge" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="crescent--shaped" value_original="crescent--shaped" />
        <character is_modifier="true" name="shape" src="d0_s5" value="circular" value_original="circular" />
      </biological_entity>
      <biological_entity id="o25934" name="nectary" name_original="nectary" src="d0_s5" type="structure" />
      <relation from="o25930" id="r3517" name="on" negation="false" src="d0_s5" to="o25931" />
      <relation from="o25932" id="r3518" name="forming" negation="false" src="d0_s5" to="o25933" />
      <relation from="o25932" id="r3519" name="covering" negation="true" src="d0_s5" to="o25934" />
    </statement>
    <statement id="d0_s6">
      <text>style absent.</text>
      <biological_entity id="o25935" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o25936" name="style" name_original="style" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads of achenes ellipsoid or cylindric heads, 5-13 × 3-7 mm;</text>
      <biological_entity id="o25937" name="head" name_original="heads" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" notes="" src="d0_s7" to="13" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" notes="" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25938" name="achene" name_original="achenes" src="d0_s7" type="structure" />
      <biological_entity id="o25939" name="head" name_original="heads" src="d0_s7" type="structure" />
      <relation from="o25937" id="r3520" name="part_of" negation="false" src="d0_s7" to="o25938" />
    </statement>
    <statement id="d0_s8">
      <text>achenes 1-1.2 × 0.8-1 mm, glabrous;</text>
      <biological_entity id="o25940" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s8" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s8" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>beak deltate, usually straight, 0.1 mm.</text>
      <biological_entity id="o25941" name="beak" name_original="beak" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="deltate" value_original="deltate" />
        <character is_modifier="false" modifier="usually" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="0.1" value_original="0.1" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., N.S., N.W.T., Nfld. and Labr. (Nfld.), Ont., Que., Sask., Yukon; Ala., Alaska, Ariz., Ark., Calif., Colo., Conn., D.C., Del., Fla., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Mass., Md., Mich., Minn., Miss., Mo., Mont., N.C., N.Dak., N.H., N.J., N.Mex., N.Y., Nebr., Nev., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Va., Vt., W.Va., Wash., Wis., Wyo.; Eurasia</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>61.</number>
  <other_name type="common_name">Cursed crowsfoot</other_name>
  <other_name type="common_name">renoncule scélérate</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Ranunculus sceleratus varieties were used by the Thompson Indians as a poison for their arrow points (D. E. Moerman 1986).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Faces of achene with fine transverse wrinkles; leaf blades lobed or parted, segments undivided or lobed, margins crenate.</description>
      <determination>61a Ranunculus sceleratus var. sceleratus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Faces of achene smooth; leaf blades always parted (often deeply so), segments lobed or parted, margins deeply crenate or lobulate.</description>
      <determination>61b Ranunculus sceleratus var. multifidus</determination>
    </key_statement>
  </key>
</bio:treatment>