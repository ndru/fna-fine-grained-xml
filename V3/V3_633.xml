<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="(Prantl) L. D. Benson" date="1936" rank="section">epirotes</taxon_name>
    <taxon_name authority="Hooker" date="1829" rank="species">glaberrimus</taxon_name>
    <taxon_name authority="(Greene) Greene" date="1891" rank="variety">ellipticus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Francisc.</publication_title>
      <place_in_publication>1: 298. 1891</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section epirotes;species glaberrimus;variety ellipticus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501148</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ranunculus</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">ellipticus</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>2: 110. 1890</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Ranunculus;species ellipticus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ranunculus</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">oreogenes</taxon_name>
    <taxon_hierarchy>genus Ranunculus;species oreogenes;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Basal leaf-blades ovate or elliptic to very narrowly elliptic, 1.5-5.2 × 0.7-2 cm, base obtuse to attenuate, margins entire or rarely with 3 broad, shallow, distal crenae, apex acute to rounded.</text>
      <biological_entity constraint="basal" id="o9328" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s0" value="elliptic to very" value_original="elliptic to very" />
        <character is_modifier="false" name="shape" src="d0_s0" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s0" value="elliptic to very" value_original="elliptic to very" />
        <character is_modifier="false" modifier="very; narrowly" name="arrangement_or_shape" src="d0_s0" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s0" to="5.2" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s0" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9329" name="base" name_original="base" src="d0_s0" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s0" to="attenuate" />
      </biological_entity>
      <biological_entity id="o9330" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s0" value="rarely" value_original="rarely" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9331" name="crena" name_original="crenae" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="3" value_original="3" />
        <character is_modifier="true" name="width" src="d0_s0" value="broad" value_original="broad" />
        <character is_modifier="true" name="depth" src="d0_s0" value="shallow" value_original="shallow" />
      </biological_entity>
      <biological_entity id="o9332" name="apex" name_original="apex" src="d0_s0" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s0" to="rounded" />
      </biological_entity>
      <relation from="o9330" id="r1313" name="with" negation="false" src="d0_s0" to="o9331" />
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences: bracts with middle lobe much larger than lateral lobes.</text>
      <biological_entity id="o9333" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure" />
      <biological_entity constraint="middle" id="o9335" name="lobe" name_original="lobe" src="d0_s1" type="structure">
        <character constraint="than lateral lobes" constraintid="o9336" is_modifier="false" name="size" src="d0_s1" value="much larger" value_original="much larger" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o9336" name="lobe" name_original="lobes" src="d0_s1" type="structure" />
      <relation from="o9334" id="r1314" name="with" negation="false" src="d0_s1" to="o9335" />
    </statement>
    <statement id="d0_s2">
      <text>2n = 80.</text>
      <biological_entity id="o9334" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character constraint="than lateral lobes" constraintid="o9336" is_modifier="false" name="size" src="d0_s1" value="much larger" value_original="much larger" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9337" name="chromosome" name_original="" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="80" value_original="80" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late winter–summer (Mar–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late winter" />
        <character name="flowering time" char_type="range_value" to="Jul" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist seepy slopes and moist depressions in grassland</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="seepy slopes" modifier="moist" />
        <character name="habitat" value="moist depressions" constraint="in grassland" />
        <character name="habitat" value="grassland" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500-3600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3600" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Sask.; Ariz., Calif., Colo., Idaho, Mont., Nebr., Nev., N.Mex., N.Dak., Oreg., S.Dak., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>49b.</number>
  
</bio:treatment>