<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">thalictrum</taxon_name>
    <taxon_name authority="(de Candolle) B. Boivin" date="1944" rank="section">Heterogamia</taxon_name>
    <taxon_name authority="Wilken &amp; DeMott" date="1983" rank="species">heliophilum</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>35: 156. 1983</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus thalictrum;section heterogamia;species heliophilum;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501268</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots fibrous.</text>
      <biological_entity id="o17940" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="false" name="texture" src="d0_s0" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 14-50 cm, arising singly or in dense clusters of 2-3 from short, horizontal, fibrous-rooted rhizomes.</text>
      <biological_entity id="o17941" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="14" from_unit="cm" name="some_measurement" src="d0_s1" to="50" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="arising" value_original="arising" />
        <character constraint="in dense clusters; of" is_modifier="false" name="arrangement" src="d0_s1" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="in dense clusters" value_original="in dense clusters" />
        <character char_type="range_value" constraint="from rhizomes" constraintid="o17942" from="2" name="quantity" src="d0_s1" to="3" />
      </biological_entity>
      <biological_entity id="o17942" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="true" name="orientation" src="d0_s1" value="horizontal" value_original="horizontal" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, petiolate.</text>
      <biological_entity id="o17943" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade ternately compound, cauline blades gradually reduced upward, distalmost 2-ternate;</text>
      <biological_entity id="o17944" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="ternately" name="architecture" src="d0_s3" value="compound" value_original="compound" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o17945" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o17946" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="2-ternate" value_original="2-ternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets broadly obovate, apically 3-toothed, otherwise undivided, 5-8 × 4-5 mm, leathery, surfaces glabrous, glaucous.</text>
      <biological_entity id="o17947" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s4" value="3-toothed" value_original="3-toothed" />
        <character is_modifier="false" modifier="otherwise" name="architecture_or_shape" src="d0_s4" value="undivided" value_original="undivided" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o17948" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, panicles, many flowered.</text>
      <biological_entity id="o17949" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o17950" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s5" value="many" value_original="many" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="flowered" value_original="flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals 4, color unknown, lanceolate to ovate, 2-3 mm;</text>
      <biological_entity id="o17951" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o17952" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="4" value_original="4" />
        <character char_type="range_value" from="lanceolate" name="coloration" src="d0_s6" to="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments brownish, 2-3 mm;</text>
      <biological_entity id="o17953" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o17954" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="brownish" value_original="brownish" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 2-3 mm, apiculate;</text>
      <biological_entity id="o17955" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o17956" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stigma color unknown.</text>
      <biological_entity id="o17957" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o17958" name="stigma" name_original="stigma" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Achenes 4-5 (-6), not reflexed, nearly sessile;</text>
      <biological_entity id="o17959" name="achene" name_original="achenes" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="6" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s10" to="5" />
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stipe 0.1-0.2 mm;</text>
      <biological_entity id="o17960" name="stipe" name_original="stipe" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s11" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>body oblique-obovate, strongly laterally compressed, 4-5 mm, glabrous, glaucous, prominently 3-veined on each side, veins converging near apex, rarely branched or sinuous, not anastomosing-reticulate;</text>
      <biological_entity id="o17961" name="body" name_original="body" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblique-obovate" value_original="oblique-obovate" />
        <character is_modifier="false" modifier="strongly laterally" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glaucous" value_original="glaucous" />
        <character constraint="on side" constraintid="o17962" is_modifier="false" modifier="prominently" name="architecture" src="d0_s12" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o17962" name="side" name_original="side" src="d0_s12" type="structure" />
      <biological_entity id="o17963" name="vein" name_original="veins" src="d0_s12" type="structure">
        <character constraint="near apex" constraintid="o17964" is_modifier="false" name="arrangement" src="d0_s12" value="converging" value_original="converging" />
        <character is_modifier="false" modifier="rarely" name="architecture" notes="" src="d0_s12" value="branched" value_original="branched" />
        <character is_modifier="false" name="course" src="d0_s12" value="sinuous" value_original="sinuous" />
        <character is_modifier="false" modifier="not" name="architecture_or_coloration_or_relief" src="d0_s12" value="anastomosing-reticulate" value_original="anastomosing-reticulate" />
      </biological_entity>
      <biological_entity id="o17964" name="apex" name_original="apex" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>beak ca. 1.5 mm.</text>
      <biological_entity id="o17965" name="beak" name_original="beak" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–summer (Jun–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Decomposing shale of Green River Formation</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="decomposing shale" constraint="of green river formation" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="2500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>In a genus of primarily mesophytic plants, Thalictrum heliophilum is notable for its relatively xeric habitat. Known only from Garfield and Rio Blanco counties, northwestern Colorado, it is similar to the widespread T. fendleri; it may be distinguished by its smaller, leathery, glaucous leaflets and fewer achenes.</discussion>
  
</bio:treatment>