<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">papaveraceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">papaver</taxon_name>
    <taxon_name authority="Spach" date="1839" rank="section">Meconella</taxon_name>
    <taxon_name authority="Tolmatchew &amp; Petrovsky" date="1973" rank="species">gorodkovii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Zhurn. (Moscow &amp; Leningrad)</publication_title>
      <place_in_publication>58: 1128, fig. 1. 1973</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family papaveraceae;genus papaver;section meconella;species gorodkovii;</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500844</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, sometimes densly so, to 1.5 dm.</text>
      <biological_entity id="o4368" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="0" from_unit="dm" modifier="sometimes densly; densly" name="some_measurement" src="d0_s0" to="1.5" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves to 5 cm;</text>
      <biological_entity id="o4369" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>petiole 1/2-3/4 length of leaf;</text>
      <biological_entity id="o4370" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1/2 length of leaf" name="length" src="d0_s2" to="3/4 length of leaf" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade light green abaxially, dark green adaxially, lanceolate, 1×-lobed with 1, occasionally 2, pairs of lateral lobes, hirsute;</text>
      <biological_entity id="o4371" name="blade" name_original="blade" src="d0_s3" type="structure" constraint="lobe" constraint_original="lobe; lobe">
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s3" value="light green" value_original="light green" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s3" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character constraint="with 1" is_modifier="false" name="shape" src="d0_s3" value="1×-lobed" value_original="1×-lobed" />
        <character modifier="occasionally" name="quantity" src="d0_s3" value="2" value_original="2" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o4372" name="lobe" name_original="lobes" src="d0_s3" type="structure" />
      <relation from="o4371" id="r624" name="part_of" negation="false" src="d0_s3" to="o4372" />
    </statement>
    <statement id="d0_s4">
      <text>terminal lobe occasionally with small secondary lobes, apex obtuse, rounded.</text>
      <biological_entity constraint="terminal" id="o4373" name="lobe" name_original="lobe" src="d0_s4" type="structure" />
      <biological_entity constraint="secondary" id="o4374" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o4375" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o4373" id="r625" name="with" negation="false" src="d0_s4" to="o4374" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: scapes erect, sparsely to densely hispid.</text>
      <biological_entity id="o4376" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o4377" name="scape" name_original="scapes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s5" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers to 3.5 cm diam.;</text>
      <biological_entity id="o4378" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="diameter" src="d0_s6" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals yellow or white;</text>
      <biological_entity id="o4379" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers yellow;</text>
      <biological_entity id="o4380" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stigmas 5-6, disc flat.</text>
      <biological_entity id="o4381" name="stigma" name_original="stigmas" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s9" to="6" />
      </biological_entity>
      <biological_entity id="o4382" name="disc" name_original="disc" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules subglobose to obconic, to 1.2 cm, 1-2.5 times longer than broad, densely hirsute, trichomes dark-brown or black.</text>
      <biological_entity id="o4383" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s10" to="obconic" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s10" to="1.2" to_unit="cm" />
        <character is_modifier="false" name="length_or_size_or_width" src="d0_s10" value="1-2.5 times longer than broad" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o4384" name="trichome" name_original="trichomes" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="black" value_original="black" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul-early Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Well-drained gravels of floodplain terraces and coastal arctic screes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="well-drained gravels" constraint="of floodplain terraces and coastal arctic screes" />
        <character name="habitat" value="floodplain terraces" />
        <character name="habitat" value="coastal arctic screes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska; Asia (Russian Far East).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia (Russian Far East)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <discussion>According to A. I. Tolmatchew and V. V. Petrovsky (1975), this species is known in Alaska also from the Seward Peninsula, presumably based on a specimen at LE (St. Petersburg), which we have not seen.</discussion>
  
</bio:treatment>