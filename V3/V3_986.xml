<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">clematis</taxon_name>
    <taxon_name authority="Keener &amp; W. M. Dennis" date="1982" rank="subgenus">Viticella</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">viticella</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 543. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus clematis;subgenus viticella;species viticella;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">233500418</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Viticella</taxon_name>
    <taxon_name authority="(Linnaeus) Small" date="unknown" rank="species">viticella</taxon_name>
    <taxon_hierarchy>genus Viticella;species viticella;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Vines 2-4 (-6) m.</text>
      <biological_entity id="o8603" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="6" to_unit="m" />
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s0" to="4" to_unit="m" />
        <character name="growth_form" value="vine" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blade: leaflets 3-7, proximal leaflets sometimes 3-foliolate, lanceolate to broadly ovate or elliptic, unlobed or 1-3-lobed, 1.5-7 cm, somewhat leathery, margins entire.</text>
      <biological_entity id="o8604" name="leaf-blade" name_original="leaf-blade" src="d0_s1" type="structure" />
      <biological_entity id="o8605" name="leaflet" name_original="leaflets" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="7" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o8606" name="leaflet" name_original="leaflets" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="3-foliolate" value_original="3-foliolate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s1" to="broadly ovate or elliptic unlobed or 1-3-lobed" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s1" to="broadly ovate or elliptic unlobed or 1-3-lobed" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s1" to="7" to_unit="cm" />
        <character is_modifier="false" modifier="somewhat" name="texture" src="d0_s1" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o8607" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: sepals 4, blue to violet or rose-violet, 1.5-4 cm, length ca. 1.2-2 times width, abaxially pubescent;</text>
      <biological_entity id="o8608" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o8609" name="sepal" name_original="sepals" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="4" value_original="4" />
        <character char_type="range_value" from="blue" name="coloration" src="d0_s2" to="violet or rose-violet" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s2" to="4" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="1.2-2" value_original="1.2-2" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stamens green;</text>
      <biological_entity id="o8610" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o8611" name="stamen" name_original="stamens" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>beak glabrous.</text>
      <biological_entity id="o8612" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o8613" name="beak" name_original="beak" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, thickets and other secondary habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="other secondary habitats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ont.; native to Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" value="native to Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <discussion>Clematis viticella has also been reported from Quebec, New York, and Tennessee, but the reports have not been verified. It probably should be expected elsewhere.</discussion>
  
</bio:treatment>