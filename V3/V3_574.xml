<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">anemone</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">virginiana</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 540. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus anemone;species virginiana</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500087</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Aerial shoots 30-100 (-110) cm, from caudices, rarely with ascending rhizomes, caudices ascending to vertical.</text>
      <biological_entity id="o27980" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="110" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o27981" name="caudex" name_original="caudices" src="d0_s0" type="structure" />
      <biological_entity id="o27982" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o27983" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s0" to="vertical" />
      </biological_entity>
      <relation from="o27980" id="r3759" name="from" negation="false" src="d0_s0" to="o27981" />
      <relation from="o27980" id="r3760" modifier="rarely" name="with" negation="false" src="d0_s0" to="o27982" />
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves 1-5, ternate;</text>
      <biological_entity constraint="basal" id="o27984" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="5" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="ternate" value_original="ternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 5-35 cm;</text>
      <biological_entity id="o27985" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="35" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>terminal leaflet sessile or nearly so, oblanceolate to obovate, 2-9 (-12) × 2-5 (-7) cm, base cuneate to broadly cuneate, margins coarsely serrate and incised on distal 1/2, apex acuminate to narrowly acute, surfaces pilose, more so abaxially;</text>
      <biological_entity constraint="terminal" id="o27986" name="leaflet" name_original="leaflet" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character name="architecture" src="d0_s3" value="nearly" value_original="nearly" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="obovate" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="12" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="9" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="7" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s3" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o27987" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s3" to="broadly cuneate" />
      </biological_entity>
      <biological_entity id="o27988" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
        <character constraint="on distal 1/2" constraintid="o27989" is_modifier="false" name="shape" src="d0_s3" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity constraint="distal" id="o27989" name="1/2" name_original="1/2" src="d0_s3" type="structure" />
      <biological_entity id="o27990" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s3" to="narrowly acute" />
      </biological_entity>
      <biological_entity id="o27991" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lateral leaflets usually 1-2×-lobed or parted, occasionally unlobed;</text>
      <biological_entity constraint="lateral" id="o27992" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="1-2×-lobed" value_original="1-2×-lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="parted" value_original="parted" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s4" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ultimate lobes 10-30 (-40) mm wide.</text>
      <biological_entity constraint="ultimate" id="o27993" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="width" src="d0_s5" to="40" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s5" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences (1-) 3-9-flowered cymes;</text>
      <biological_entity id="o27994" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o27995" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="(1-)3-9-flowered" value_original="(1-)3-9-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle villous;</text>
      <biological_entity id="o27996" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>primary involucral-bracts 3 (-5), secondary involucral-bracts 2 (-3), (1-) 2-tiered, ternate, ±similar to basal leaves, bases distinct;</text>
      <biological_entity constraint="primary" id="o27997" name="involucral-bract" name_original="involucral-bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="5" />
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o27998" name="involucral-bract" name_original="involucral-bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="3" />
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="(1-)2-tiered" value_original="(1-)2-tiered" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="ternate" value_original="ternate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o27999" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o28000" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o27998" id="r3761" modifier="more or less" name="to" negation="false" src="d0_s8" to="o27999" />
    </statement>
    <statement id="d0_s9">
      <text>terminal leaflet ±sessile, elliptic to oblanceolate, 2-10 (-12) cm (2 cm in secondary involucre) × 2-5 (-7) cm, bases cuneate, margins coarsely serrate and incised on distal 1/2, apex acuminate to narrowly acute, surfaces pilose, more so abaxially;</text>
      <biological_entity constraint="terminal" id="o28001" name="leaflet" name_original="leaflet" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s9" to="oblanceolate" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s9" to="12" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s9" to="10" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s9" to="7" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s9" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28002" name="base" name_original="bases" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o28003" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s9" value="serrate" value_original="serrate" />
        <character constraint="on distal 1/2" constraintid="o28004" is_modifier="false" name="shape" src="d0_s9" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity constraint="distal" id="o28004" name="1/2" name_original="1/2" src="d0_s9" type="structure" />
      <biological_entity id="o28005" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s9" to="narrowly acute" />
      </biological_entity>
      <biological_entity id="o28006" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lateral leaflets unlobed or 1×-lobed or parted;</text>
      <biological_entity constraint="lateral" id="o28007" name="leaflet" name_original="leaflets" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" name="shape" src="d0_s10" value="1×-lobed" value_original="1×-lobed" />
        <character is_modifier="false" name="shape" src="d0_s10" value="parted" value_original="parted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ultimate lobes 8-25 (-35) mm wide.</text>
      <biological_entity constraint="ultimate" id="o28008" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="width" src="d0_s11" to="35" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s11" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers: sepals usually 5, green, yellow, or red (rarely white or abaxially green to green-yellow and adaxially green or yellow and tinged red), oblong, ovate, or obovate, 6.5-20 × 2.5-10mm, abaxially hairy, adaxially glabrous or nearly so;</text>
      <biological_entity id="o28009" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o28010" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="length" src="d0_s12" to="20" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s12" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s12" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 50-70.</text>
      <biological_entity id="o28011" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o28012" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s13" to="70" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Heads of achenes oblong-ellipsoid, rarely obconic;</text>
      <biological_entity id="o28013" name="head" name_original="heads" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong-ellipsoid" value_original="oblong-ellipsoid" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s14" value="obconic" value_original="obconic" />
      </biological_entity>
      <biological_entity id="o28014" name="achene" name_original="achenes" src="d0_s14" type="structure" />
      <relation from="o28013" id="r3762" name="part_of" negation="false" src="d0_s14" to="o28014" />
    </statement>
    <statement id="d0_s15">
      <text>pedicel 13-25 (-30) cm.</text>
      <biological_entity id="o28015" name="pedicel" name_original="pedicel" src="d0_s15" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s15" to="30" to_unit="cm" />
        <character char_type="range_value" from="13" from_unit="cm" name="some_measurement" src="d0_s15" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes: body obovoid, 2-3.7 × 1.5-2mm, not winged, densely woolly;</text>
      <biological_entity id="o28016" name="achene" name_original="achenes" src="d0_s16" type="structure" />
      <biological_entity id="o28017" name="body" name_original="body" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s16" to="3.7" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s16" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s16" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s16" value="woolly" value_original="woolly" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>beak curved, 1-1.5mm, puberulous, not plumose.</text>
      <biological_entity id="o28018" name="achene" name_original="achenes" src="d0_s17" type="structure" />
      <biological_entity id="o28019" name="beak" name_original="beak" src="d0_s17" type="structure">
        <character is_modifier="false" name="course" src="d0_s17" value="curved" value_original="curved" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s17" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="puberulous" value_original="puberulous" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s17" value="plumose" value_original="plumose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., N.S., Nfld. and Labr. (Nfld.), Ont., Que., Sask.; Ala., Ark., Conn., D.C., Del., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Mass., Md., Mich., Minn., Miss., Mo., N.C., N.Dak., N.H., N.J., N.Y., Nebr., Ohio, Okla., Pa., R.I., S.C., S.Dak., Tenn., Va., Vt., W.Va., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  <other_name type="common_name">Thimbleweed</other_name>
  <other_name type="common_name">tall anemone</other_name>
  <other_name type="common_name">anémone de Virginie</other_name>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <discussion>See C. S. Keener et al. (1995) for an analysis of infraspecific variation within Anemone virginiana from which the current treatment has been adopted.</discussion>
  <discussion>Varieties of Anemone virginiana used medicinally by native Americans were not specified; the species was used as an antidiarrheal, an aid for whooping cough, a stimulant, an emetic, a love potion, a remedy for tuberculosis, and a protection against witchcraft medicine (D. E. Moerman 1986).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals usually 5–10mm, abaxially densely tomentose; anthers 0.7–1(–1.2) mm; primarily Canadian, in dry woods, sandy ridges, and grasslands.</description>
      <determination>22b Anemone virginiana var. cylindroidea</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals (6–)10–21 mm, abaxially usually thinly pubescent; anthers (0.8–)1–1.7 mm; widely distributed, in moist habitats.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Base of involucral bracts cordate or reniform, rarely subtruncate, terminal leaflets light green, margins proximally mostly straight- to convex-sided, variously lobed or serrate, variously pubescent; anthers typically greater than 1.1 mm; heads of achenes ovoid to ovoid-cylindric, (9–)11–14 mm diam.; widely distributed.</description>
      <determination>22a Anemone virginiana var. virginiana</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Base of involucral bracts usually truncate to subtruncate, sometimes reniform or cordate, terminal leaflets deep green, margins proximally concave- to straight-sided, distally incised, thinly pubescent; anthers typically less than 1.2 mm; heads of achenes ±ovoid-cylindric, 8–10(–11) mm diam.; distributed primarily in New England, Great Lakes area, and adjacent Canada.</description>
      <determination>22c Anemone virginiana var. alba</determination>
    </key_statement>
  </key>
</bio:treatment>