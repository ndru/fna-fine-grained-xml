<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="N. I. Malyutin" date="1987" rank="subsection">exaltata</taxon_name>
    <taxon_name authority="(Huth) Huth" date="1893" rank="species">barbeyi</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Herb. Boissier</publication_title>
      <place_in_publication>1: 335. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection exaltata;species barbeyi;</taxon_hierarchy>
    <other_info_on_name type="special_status">W2</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500475</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="Aiton" date="unknown" rank="species">exaltatum</taxon_name>
    <taxon_name authority="Huth" date="unknown" rank="variety">(e) barbeyi</taxon_name>
    <place_of_publication>
      <publication_title>Helios</publication_title>
      <place_in_publication>10: 35. 1892</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Delphinium;species exaltatum;variety (e) barbeyi;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="(S. Watson) S. Watson" date="unknown" rank="species">occidentale</taxon_name>
    <taxon_name authority="(Huth) S. L. Welsh" date="unknown" rank="variety">barbeyi</taxon_name>
    <taxon_hierarchy>genus Delphinium;species occidentale;variety barbeyi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 50-150 cm;</text>
      <biological_entity id="o20732" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base green, glabrous.</text>
      <biological_entity id="o20733" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, 8-24, absent from proximal 1/5 of stem at anthesis;</text>
      <biological_entity id="o20734" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s2" to="24" />
        <character constraint="from proximal 1/5" constraintid="o20735" is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o20735" name="1/5" name_original="1/5" src="d0_s2" type="structure" />
      <biological_entity id="o20736" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o20735" id="r2800" name="part_of" negation="false" src="d0_s2" to="o20736" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 1-14 cm.</text>
      <biological_entity id="o20737" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="14" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blade round to reniform, 4-8 × 7-15 cm, glabrous;</text>
      <biological_entity id="o20738" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s4" to="reniform" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="width" src="d0_s4" to="15" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ultimate lobes 5-9, width 8-50 mm.</text>
      <biological_entity constraint="ultimate" id="o20739" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s5" to="9" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s5" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 10-50-flowered;</text>
      <biological_entity id="o20740" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="10-50-flowered" value_original="10-50-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pedicel 0.5-6 cm, glandular-puberulent;</text>
      <biological_entity id="o20741" name="pedicel" name_original="pedicel" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s7" to="6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles 1-4 (-8) mm from flowers, blue to green, awl-shaped, 5-14 mm, puberulent.</text>
      <biological_entity id="o20742" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="8" to_unit="mm" />
        <character char_type="range_value" constraint="from flowers" constraintid="o20743" from="1" from_unit="mm" name="location" src="d0_s8" to="4" to_unit="mm" />
        <character char_type="range_value" from="blue" name="coloration" notes="" src="d0_s8" to="green" />
        <character is_modifier="false" name="shape" src="d0_s8" value="awl--shaped" value_original="awl--shaped" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="14" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o20743" name="flower" name_original="flowers" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals dark bluish purple, sparsely puberulent, lateral sepals forward pointing, 13-23 × 5-8 mm, spurs ascending ca. 45°, downcurved apically, 10-18 mm;</text>
      <biological_entity id="o20744" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o20745" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark bluish" value_original="dark bluish" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s9" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o20746" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="forward" name="orientation" src="d0_s9" value="pointing" value_original="pointing" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s9" to="23" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20747" name="spur" name_original="spurs" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="45°" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="apically" name="orientation" src="d0_s9" value="downcurved" value_original="downcurved" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lower petal blades ± covering stamens, 4-7 mm, clefts 2-3 mm;</text>
      <biological_entity id="o20748" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="petal" id="o20749" name="blade" name_original="blades" src="d0_s10" type="structure" constraint_original="lower petal">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20750" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity id="o20751" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o20749" id="r2801" modifier="more or less" name="covering" negation="false" src="d0_s10" to="o20750" />
    </statement>
    <statement id="d0_s11">
      <text>hairs centered, mostly near base of cleft, sparse elsewhere, white or yellow.</text>
      <biological_entity id="o20752" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o20753" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="centered" value_original="centered" />
        <character is_modifier="false" modifier="of cleft , sparse elsewhere" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o20754" name="base" name_original="base" src="d0_s11" type="structure" />
      <relation from="o20753" id="r2802" modifier="mostly" name="near" negation="false" src="d0_s11" to="o20754" />
    </statement>
    <statement id="d0_s12">
      <text>Fruits 17-22 mm, 2.5-3 times longer than wide, puberulent.</text>
      <biological_entity id="o20755" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s12" to="22" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s12" value="2.5-3" value_original="2.5-3" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds wing-margined;</text>
      <biological_entity id="o20756" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="wing-margined" value_original="wing-margined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>seed-coat cells narrow but short, surfaces pustulate.</text>
      <biological_entity constraint="seed-coat" id="o20757" name="cell" name_original="cells" src="d0_s14" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s14" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>2n = 16.</text>
      <biological_entity id="o20758" name="surface" name_original="surfaces" src="d0_s14" type="structure">
        <character is_modifier="false" name="relief" src="d0_s14" value="pustulate" value_original="pustulate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20759" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Subalpine and alpine sites in wet soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="subalpine" constraint="in wet soils" />
        <character name="habitat" value="alpine sites" constraint="in wet soils" />
        <character name="habitat" value="wet soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2500-4100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4100" to_unit="m" from="2500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., N.Mex., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Subalpine larkspur</other_name>
  <other_name type="common_name">tall larkspur</other_name>
  <other_name type="common_name">Barbey's larkspur</other_name>
  <discussion>Delphinium barbeyi hybridizes extensively with D. glaucum in western Colorado and eastern Utah, where plants appearing to be hybrid [D. ×occidentale (S. Watson) S. Watson] are often far more common than plants of either putative parent. Several other names have been used for these plants, including D. elatum var. occidentale S. Watson, D. abietorum Tidestrom, and D. scopulorum subsp. occidentale (S. Watson) Abrams. Delphinium barbeyi is also known to hybridize with D. ramosum and D. sapellonis.</discussion>
  
</bio:treatment>