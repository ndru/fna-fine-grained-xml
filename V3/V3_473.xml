<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Linnaeus" date="unknown" rank="family">fumariaceae</taxon_name>
    <taxon_name authority="de Candolle in J. Lamarck and A. P. de Candolle" date="1805" rank="genus">corydalis</taxon_name>
    <taxon_name authority="(Rafinesque) de Candolle in A. P. de Candolle and A. L. P. de Candolle" date="1824" rank="species">flavula</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. de Candolle,  Prodr.</publication_title>
      <place_in_publication>1: 129. 1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fumariaceae;genus corydalis;species flavula</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500440</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Fumaria</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="species">flavula</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. (Desvaux)</publication_title>
      <place_in_publication>1: 224. 1808</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Fumaria;species flavula;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Capnodes</taxon_name>
    <taxon_name authority="(Rafinesque) Kuntze" date="unknown" rank="species">flavulum</taxon_name>
    <taxon_hierarchy>genus Capnodes;species flavulum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, from somewhat succulent roots.</text>
      <biological_entity id="o4131" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4132" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="somewhat" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
      </biological_entity>
      <relation from="o4131" id="r581" name="from" negation="false" src="d0_s0" to="o4132" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1-several, initially erect, often becoming prostrate or ascending, usually 1.5-3 dm.</text>
      <biological_entity id="o4133" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s1" to="several" />
        <character is_modifier="false" modifier="initially" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="often becoming; becoming" name="orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="1.5" from_unit="dm" modifier="usually" name="some_measurement" src="d0_s1" to="3" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves compound;</text>
      <biological_entity id="o4134" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade with 2 orders of leaflets and lobes;</text>
      <biological_entity id="o4135" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o4136" name="order" name_original="orders" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o4137" name="leaflet" name_original="leaflets" src="d0_s3" type="structure" />
      <biological_entity id="o4138" name="lobe" name_original="lobes" src="d0_s3" type="structure" />
      <relation from="o4135" id="r582" name="with" negation="false" src="d0_s3" to="o4136" />
      <relation from="o4136" id="r583" name="part_of" negation="false" src="d0_s3" to="o4137" />
      <relation from="o4136" id="r584" name="part_of" negation="false" src="d0_s3" to="o4138" />
    </statement>
    <statement id="d0_s4">
      <text>ultimate lobes elliptic, variable in size, margins incised, apex subapiculate.</text>
      <biological_entity constraint="ultimate" id="o4139" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="size" src="d0_s4" value="variable" value_original="variable" />
      </biological_entity>
      <biological_entity id="o4140" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o4141" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="subapiculate" value_original="subapiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemose, commonly 6-10-flowered, equaling or barely exceeding leaves, sometimes poorly developed, cleistogamous-flowered racemes present, inconspicuous, 1-5-flowered;</text>
      <biological_entity id="o4142" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
        <character is_modifier="false" modifier="commonly" name="architecture" src="d0_s5" value="6-10-flowered" value_original="6-10-flowered" />
        <character is_modifier="false" name="variability" src="d0_s5" value="equaling" value_original="equaling" />
        <character name="variability" src="d0_s5" value="barely exceeding leaves" value_original="barely exceeding leaves" />
        <character is_modifier="false" modifier="sometimes poorly" name="development" src="d0_s5" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity id="o4143" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="cleistogamous-flowered" value_original="cleistogamous-flowered" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s5" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-5-flowered" value_original="1-5-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts elliptic, 6-12 × 3-7 mm, proximal bracts often leaflike or variously incised, distal reduced and entire.</text>
      <biological_entity id="o4144" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s6" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o4145" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" modifier="variously" name="shape" src="d0_s6" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4146" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers erect;</text>
      <biological_entity id="o4147" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicel slender, 6-15 mm or more;</text>
      <biological_entity id="o4148" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="slender" value_original="slender" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals lanceolate, ca. 1 mm;</text>
      <biological_entity id="o4149" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals pale-yellow, spurred petal 7-9 mm, spur incurved, ca. 2 mm, crest high, marginal wing well developed, both crest and wing wrinkled or dentate, unspurred outer petal similar to spurred petal, 6-8 mm;</text>
      <biological_entity id="o4150" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="pale-yellow" value_original="pale-yellow" />
      </biological_entity>
      <biological_entity id="o4151" name="petal" name_original="petal" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s10" value="spurred" value_original="spurred" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4152" name="spur" name_original="spur" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="incurved" value_original="incurved" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o4153" name="crest" name_original="crest" src="d0_s10" type="structure">
        <character is_modifier="false" name="height" src="d0_s10" value="high" value_original="high" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o4154" name="wing" name_original="wing" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s10" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity id="o4155" name="crest" name_original="crest" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="wrinkled" value_original="wrinkled" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o4156" name="wing" name_original="wing" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="wrinkled" value_original="wrinkled" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o4157" name="petal" name_original="petal" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="unspurred" value_original="unspurred" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4158" name="petal" name_original="petal" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s10" value="spurred" value_original="spurred" />
      </biological_entity>
      <relation from="o4157" id="r585" name="to" negation="false" src="d0_s10" to="o4158" />
    </statement>
    <statement id="d0_s11">
      <text>inner petals 5-7 mm, blade apex ca. 2 times wider than distinctly lobed base, claw 2-3 mm;</text>
      <biological_entity constraint="inner" id="o4159" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="blade" id="o4160" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character constraint="base" constraintid="o4161" is_modifier="false" name="width" src="d0_s11" value="2 times wider than distinctly lobed base" />
      </biological_entity>
      <biological_entity id="o4161" name="base" name_original="base" src="d0_s11" type="structure" />
      <biological_entity id="o4162" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectariferous spur less than 1/2 length of petal spur;</text>
      <biological_entity constraint="nectariferous" id="o4163" name="spur" name_original="spur" src="d0_s12" type="structure">
        <character char_type="range_value" from="0 length of petal spur" name="length" src="d0_s12" to="1/2 length of petal spur" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style 1.5-2 mm;</text>
      <biological_entity id="o4164" name="style" name_original="style" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma wider than long, with 4 terminal papillae.</text>
      <biological_entity id="o4165" name="stigma" name_original="stigma" src="d0_s14" type="structure">
        <character is_modifier="false" name="width" src="d0_s14" value="wider than long" value_original="wider than long" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o4166" name="papilla" name_original="papillae" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="4" value_original="4" />
      </biological_entity>
      <relation from="o4165" id="r586" name="with" negation="false" src="d0_s14" to="o4166" />
    </statement>
    <statement id="d0_s15">
      <text>Capsules pendent, linear, straight or sometimes reflexed, (14-) 18-20 (-22) mm.</text>
      <biological_entity id="o4167" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="pendent" value_original="pendent" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s15" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="14" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="18" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="22" to_unit="mm" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s15" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds ca. 2 mm diam., minutely decorated on narrow marginal ring.</text>
      <biological_entity id="o4168" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character name="diameter" src="d0_s16" unit="mm" value="2" value_original="2" />
        <character constraint="on marginal ring" constraintid="o4169" is_modifier="false" modifier="minutely" name="relief" src="d0_s16" value="decorated" value_original="decorated" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o4169" name="ring" name_original="ring" src="d0_s16" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s16" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early–late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="early" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wooded slopes, bottomlands, and rock outcrops, in moist, loose soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wooded slopes" />
        <character name="habitat" value="bottomlands" />
        <character name="habitat" value="rock outcrops" />
        <character name="habitat" value="moist" />
        <character name="habitat" value="loose soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-650 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="650" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Ark., Del., D.C., Fla., Ill., Ind., Iowa, Kans., Ky., La., Md., Mich., Miss., Mo., Nebr., N.J., N.Y., N.C., Ohio, Okla., Pa., S.C., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  
</bio:treatment>