<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">hamamelidaceae</taxon_name>
    <taxon_name authority="Murray in Linnaeus" date="1774" rank="genus">fothergilla</taxon_name>
    <taxon_name authority="Loddiges" date="1829" rank="species">major</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Cab.</publication_title>
      <place_in_publication>16: plate 1520. 1829</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hamamelidaceae;genus fothergilla;species major</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500651</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Fothergilla</taxon_name>
    <taxon_name authority="Ashe" date="unknown" rank="species">monticola</taxon_name>
    <taxon_hierarchy>genus Fothergilla;species monticola;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 7-65 dm;</text>
      <biological_entity id="o19280" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="7" from_unit="dm" name="some_measurement" src="d0_s0" to="65" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branches robust.</text>
      <biological_entity id="o19281" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s1" value="robust" value_original="robust" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules 2.8-7 (-10.2) mm;</text>
      <biological_entity id="o19282" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o19283" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="10.2" to_unit="mm" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s2" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles 3-10 (-18) mm.</text>
      <biological_entity id="o19284" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o19285" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="18" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blade broadly elliptic or somewhat orbiculate to obovate, asymmetric, 2.5-13.5 × 4.2-12.5 cm, base rounded to truncate, rarely cuneate, often oblique, proximal margins entire, distal margins coarsely sinuate to repand, rarely entire, apex short-acuminate to rounded and mucronate;</text>
      <biological_entity id="o19286" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character name="arrangement_or_shape" src="d0_s4" value="somewhat" value_original="somewhat" />
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s4" to="obovate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="asymmetric" value_original="asymmetric" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s4" to="13.5" to_unit="cm" />
        <character char_type="range_value" from="4.2" from_unit="cm" name="width" src="d0_s4" to="12.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19287" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="truncate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" modifier="often" name="orientation_or_shape" src="d0_s4" value="oblique" value_original="oblique" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o19288" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="distal" id="o19289" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="coarsely sinuate" name="shape" src="d0_s4" to="repand" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o19290" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="short-acuminate" name="shape" src="d0_s4" to="rounded" />
        <character is_modifier="false" name="shape" src="d0_s4" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>surfaces abaxially glaucous or green, adaxially green, both surfaces stellate-pubescent or nearly glabrous;</text>
      <biological_entity id="o19291" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s5" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o19292" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>veins (4-) 5-6 (-7) pairs.</text>
      <biological_entity id="o19293" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s6" to="5" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="7" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences nearly sessile to short-pedunculate, 3-6 × 2-3 cm.</text>
      <biological_entity id="o19294" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character char_type="range_value" from="nearly sessile" name="architecture" src="d0_s7" to="short-pedunculate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s7" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s7" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx lobes persistent in fruit;</text>
      <biological_entity id="o19295" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="calyx" id="o19296" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character constraint="in fruit" constraintid="o19297" is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o19297" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>stamens (10-) 22-34;</text>
      <biological_entity id="o19298" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o19299" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" name="atypical_quantity" src="d0_s9" to="22" to_inclusive="false" />
        <character char_type="range_value" from="22" name="quantity" src="d0_s9" to="34" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments 6-17 mm.</text>
      <biological_entity id="o19300" name="flower" name_original="flowers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Fruiting spikes 3.5-7 × 1.5-2.5 cm.</text>
      <biological_entity id="o19301" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19302" name="spike" name_original="spikes" src="d0_s11" type="structure" />
      <relation from="o19301" id="r2602" name="fruiting" negation="false" src="d0_s11" to="o19302" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules 5.5-13 mm.</text>
      <biological_entity id="o19303" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s12" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 5-6 (-8) mm, apex pointed.</text>
      <biological_entity id="o19304" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="8" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = 72.</text>
      <biological_entity id="o19305" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="pointed" value_original="pointed" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19306" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (Apr–May).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="Apr-May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bluffs, dry rocky woodlands, talus slopes, riverbanks, upper piedmont to mountains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="dry rocky woodlands" />
        <character name="habitat" value="talus slopes" />
        <character name="habitat" value="riverbanks" />
        <character name="habitat" value="upper piedmont" />
        <character name="habitat" value="mountains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>150-1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="150" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Ga., N.C., S.C., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>The disjunct occurrence of Fothergilla major in Arkansas is a recent discovery.</discussion>
  
</bio:treatment>