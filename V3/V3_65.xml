<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">papaveraceae</taxon_name>
    <taxon_name authority="Chamisso in C. G. D. Nees" date="1793" rank="genus">eschscholzia</taxon_name>
    <taxon_name authority="(Greene) Greene" date="1886" rank="species">ramosa</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>13: 217. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family papaveraceae;genus eschscholzia;species ramosa</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500643</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eschscholzia</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">elegans</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="variety">ramosa</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Calif. Acad. Sci.</publication_title>
      <place_in_publication>1: 182. 1885</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eschscholzia;species elegans;variety ramosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, caulescent, erect, 5-30 cm, glabrous, sometimes glaucous.</text>
      <biological_entity id="o16710" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o16711" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade glabrous;</text>
      <biological_entity id="o16712" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ultimate lobes elongate, obtuse.</text>
      <biological_entity constraint="ultimate" id="o16713" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences cymose or 1-flowered;</text>
      <biological_entity id="o16714" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-flowered" value_original="1-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>buds erect, blunt or rounded short-acuminate, tip less than 1/4 length of bud.</text>
      <biological_entity id="o16715" name="bud" name_original="buds" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s5" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="short-acuminate" value_original="short-acuminate" />
      </biological_entity>
      <biological_entity id="o16716" name="tip" name_original="tip" src="d0_s5" type="structure">
        <character char_type="range_value" from="0 length of bud" name="length" src="d0_s5" to="1/4 length of bud" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: receptacle obconic, less than 2.5 cm broad, cup without spreading free rim;</text>
      <biological_entity id="o16717" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o16718" name="receptacle" name_original="receptacle" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s6" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16719" name="cup" name_original="cup" src="d0_s6" type="structure" />
      <biological_entity id="o16720" name="rim" name_original="rim" src="d0_s6" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="fusion" src="d0_s6" value="free" value_original="free" />
      </biological_entity>
      <relation from="o16719" id="r2294" name="without" negation="false" src="d0_s6" to="o16720" />
    </statement>
    <statement id="d0_s7">
      <text>calyx acuminate, glabrous;</text>
      <biological_entity id="o16721" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o16722" name="calyx" name_original="calyx" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals yellow, sometimes with orange spot at base, 5-20 mm.</text>
      <biological_entity id="o16723" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o16724" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character constraint="at base" constraintid="o16725" is_modifier="false" modifier="sometimes" name="coloration" src="d0_s8" value="orange spot" value_original="orange spot" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16725" name="base" name_original="base" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Capsules 4-7 cm.</text>
      <biological_entity id="o16726" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s9" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds brown, ellipsoid, 1.4-1.6 mm, reticulate.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 24.</text>
      <biological_entity id="o16727" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s10" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s10" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16728" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (Apr–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="Apr-Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open places, especially in chaparral</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open places" />
        <character name="habitat" value="chaparral" modifier="especially" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Island-poppy</other_name>
  <discussion>Within the flora area, Eschscholzia ramosa is known only from the California Channel Islands.</discussion>
  
</bio:treatment>