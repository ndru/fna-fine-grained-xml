<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">anemone</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">patens</taxon_name>
    <taxon_name authority="Pritzel" date="1841" rank="variety">multifida</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>15: 581. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus anemone;species patens;variety multifida</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500080</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anemone</taxon_name>
    <taxon_name authority="(Linnaeus) Miller" date="unknown" rank="species">patens</taxon_name>
    <taxon_name authority="(de Candolle) A. Gray" date="unknown" rank="variety">nuttalliana</taxon_name>
    <taxon_hierarchy>genus Anemone;species patens;variety nuttalliana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anemone</taxon_name>
    <taxon_name authority="(Linnaeus) Miller" date="unknown" rank="species">patens</taxon_name>
    <taxon_name authority="(Besser) Koch" date="unknown" rank="variety">wolfgangiana</taxon_name>
    <taxon_hierarchy>genus Anemone;species patens;variety wolfgangiana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pulsatilla</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">patens</taxon_name>
    <taxon_name authority="Krylov &amp; Sergievskaja" date="unknown" rank="subspecies">asiatica</taxon_name>
    <taxon_hierarchy>genus Pulsatilla;species patens;subspecies asiatica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pulsatilla</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">patens</taxon_name>
    <taxon_name authority="(Pritzel) Zämelis" date="unknown" rank="subspecies">multifida</taxon_name>
    <taxon_hierarchy>genus Pulsatilla;species patens;subspecies multifida;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Aerial shoots 5-40 (-60) cm, from caudices, caudices ascending to vertical.</text>
      <biological_entity id="o28106" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28107" name="caudex" name_original="caudices" src="d0_s0" type="structure" />
      <biological_entity id="o28108" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s0" to="vertical" />
      </biological_entity>
      <relation from="o28106" id="r3772" name="from" negation="false" src="d0_s0" to="o28107" />
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves (3-) 5-8 (-10), primarily 3-foliolate with each leaflet dichotomously dissected;</text>
      <biological_entity constraint="basal" id="o28109" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s1" to="5" to_inclusive="false" />
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="10" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s1" to="8" />
        <character constraint="with leaflet" constraintid="o28110" is_modifier="false" modifier="primarily" name="architecture" src="d0_s1" value="3-foliolate" value_original="3-foliolate" />
      </biological_entity>
      <biological_entity id="o28110" name="leaflet" name_original="leaflet" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="dichotomously" name="shape" src="d0_s1" value="dissected" value_original="dissected" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 5-10 (-13) cm;</text>
      <biological_entity id="o28111" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="13" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>terminal leaflet petiolulate to nearly sessile, obovate in outline, (2.5-) 3-5 cm, base narrowly cuneate, margins dichotomously dissected throughout, apex acute to obtuse, surfaces villous, rarely glabrous;</text>
      <biological_entity constraint="terminal" id="o28112" name="leaflet" name_original="leaflet" src="d0_s3" type="structure">
        <character char_type="range_value" from="petiolulate" name="architecture" src="d0_s3" to="nearly sessile" />
        <character constraint="in outline" constraintid="o28113" is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s3" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" notes="" src="d0_s3" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28113" name="outline" name_original="outline" src="d0_s3" type="structure" />
      <biological_entity id="o28114" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o28115" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="dichotomously; throughout" name="shape" src="d0_s3" value="dissected" value_original="dissected" />
      </biological_entity>
      <biological_entity id="o28116" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="obtuse" />
      </biological_entity>
      <biological_entity id="o28117" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lateral leaflets 3-4×-parted (±dichotomously);</text>
      <biological_entity constraint="lateral" id="o28118" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="3-4×-parted" value_original="3-4×-parted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ultimate segments 2-4 mm wide.</text>
      <biological_entity constraint="ultimate" id="o28119" name="segment" name_original="segments" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 1-flowered;</text>
      <biological_entity id="o28120" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-flowered" value_original="1-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle villous or glabrate;</text>
      <biological_entity id="o28121" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>involucral-bracts primarily 3, 1-tiered, simple, dissimilar to basal leaves, (2-) 2.5-4 cm, bases clasping, connate, margins deeply laciniate throughout, surfaces villous, rarely glabrous to nearly glabrous;</text>
      <biological_entity id="o28122" name="involucral-bract" name_original="involucral-bracts" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="1-tiered" value_original="1-tiered" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="simple" value_original="simple" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s8" to="2.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" notes="" src="d0_s8" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o28123" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o28124" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s8" value="clasping" value_original="clasping" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o28125" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="deeply; throughout" name="shape" src="d0_s8" value="laciniate" value_original="laciniate" />
      </biological_entity>
      <biological_entity id="o28126" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
        <character char_type="range_value" from="rarely glabrous" name="pubescence" src="d0_s8" to="nearly glabrous" />
      </biological_entity>
      <relation from="o28122" id="r3773" name="to" negation="false" src="d0_s8" to="o28123" />
    </statement>
    <statement id="d0_s9">
      <text>segments usually 4-6, filiform to linear, unlobed, 1-2 (-3) mm wide.</text>
      <biological_entity id="o28127" name="segment" name_original="segments" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s9" to="6" />
        <character char_type="range_value" from="filiform" name="shape" src="d0_s9" to="linear" />
        <character is_modifier="false" name="shape" src="d0_s9" value="unlobed" value_original="unlobed" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals 5-8, blue, purple, to rarely nearly white, oblong to elliptic, (18-) 20-40 × (8-) 10-15 mm, abaxially villous, adaxially glabrous;</text>
      <biological_entity id="o28128" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o28129" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="8" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="blue" value_original="blue" />
        <character char_type="range_value" from="purple" name="coloration" src="d0_s10" to="rarely nearly white" />
        <character char_type="range_value" from="purple" name="coloration" src="d0_s10" to="rarely nearly white" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s10" to="elliptic" />
        <character char_type="range_value" from="18" from_unit="mm" name="atypical_length" src="d0_s10" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s10" to="40" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_width" src="d0_s10" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s10" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s10" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals present;</text>
      <biological_entity id="o28130" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o28131" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 150-200.</text>
      <biological_entity id="o28132" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o28133" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="150" name="quantity" src="d0_s12" to="200" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Heads of achenes spheric to ovoid;</text>
      <biological_entity id="o28134" name="head" name_original="heads" src="d0_s13" type="structure">
        <character char_type="range_value" from="spheric" name="shape" src="d0_s13" to="ovoid" />
      </biological_entity>
      <biological_entity id="o28135" name="achene" name_original="achenes" src="d0_s13" type="structure" />
      <relation from="o28134" id="r3774" name="part_of" negation="false" src="d0_s13" to="o28135" />
    </statement>
    <statement id="d0_s14">
      <text>pedicel 10-18 (-22) cm.</text>
      <biological_entity id="o28136" name="pedicel" name_original="pedicel" src="d0_s14" type="structure">
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s14" to="22" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s14" to="18" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes: body ellipsoid to obovoid, 3-4 (-6) × ca. 1 mm, not winged, villous;</text>
      <biological_entity id="o28137" name="achene" name_original="achenes" src="d0_s15" type="structure" />
      <biological_entity id="o28138" name="body" name_original="body" src="d0_s15" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s15" to="obovoid" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s15" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s15" to="4" to_unit="mm" />
        <character name="width" src="d0_s15" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s15" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>beak curved, 20-40 mm, long-villous, plumose.</text>
      <biological_entity id="o28139" name="achene" name_original="achenes" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>2n=16.</text>
      <biological_entity id="o28140" name="beak" name_original="beak" src="d0_s16" type="structure">
        <character is_modifier="false" name="course" src="d0_s16" value="curved" value_original="curved" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s16" to="40" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="long-villous" value_original="long-villous" />
        <character is_modifier="false" name="shape" src="d0_s16" value="plumose" value_original="plumose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28141" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (Apr–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Aug" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies, open slopes, sometimes open woods or granite outcrops in woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
        <character name="habitat" value="open slopes" />
        <character name="habitat" value="open woods" modifier="sometimes" />
        <character name="habitat" value="granite outcrops" />
        <character name="habitat" value="woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-3800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3800" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Ont., Sask., Yukon; Alaska, Colo., Idaho, Ill., Iowa, Minn., Mont., Nebr., N.Mex., N.Dak., S.Dak., Utah, Wis., Wyo.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2a.</number>
  <other_name type="common_name">Pasqueflower</other_name>
  <other_name type="common_name">prairie-smoke</other_name>
  <other_name type="common_name">prairie-crocus</other_name>
  <other_name type="common_name">pulsatille</other_name>
  <discussion>Anemone patens var. multifida has frequently been recognized as a subspecies of A. patens. Although A. patens var. wolfgangiana has been used by some authors, A. patens var. multifida has priority.</discussion>
  <discussion>Pasqueflower (as Anemone patens var. wolfgangiana) is the floral emblem of Manitoba and (as Pulsatilla hirsutissima) the state flower of South Dakota.</discussion>
  <discussion>Native Americans used fresh leaves of Anemone patens var. multifida medicinally to treat rheumatism and neuralgia; crushed leaves for poultices; pulverized leaves to smell to alleviate headaches; and made decoctions from roots to treat lung problems (D. E. Moerman 1986).</discussion>
  <discussion>The names Pulsatilla hirsutissima (Pursh) Britton and P. ludoviciana A. Heller are illegitimate.</discussion>
  
</bio:treatment>