<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="section">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">acris</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 554. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section ranunculus;species acris;</taxon_hierarchy>
    <other_info_on_name type="special_status">W2</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501104</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ranunculus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">acris</taxon_name>
    <taxon_name authority="Beck" date="unknown" rank="variety">latisectus</taxon_name>
    <taxon_hierarchy>genus Ranunculus;species acris;variety latisectus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect from short caudex or rhizome, never rooting nodally, hispid, strigose, or glabrous, base not bulbous.</text>
      <biological_entity id="o15540" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character constraint="from rhizome" constraintid="o15542" is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="never; nodally" name="architecture" notes="" src="d0_s0" value="rooting" value_original="rooting" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15541" name="caudex" name_original="caudex" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o15542" name="rhizome" name_original="rhizome" src="d0_s0" type="structure" />
      <biological_entity id="o15543" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="bulbous" value_original="bulbous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots never tuberous.</text>
      <biological_entity id="o15544" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="never" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaf-blades pentagonal in outline, deeply 3-5-parted, 1.8-5.2 × 2.7-9.8 cm, segments 1-2×-lobed or parted, ultimate segments narrowly elliptic or oblong to lanceolate, margins toothed or lobulate, apex acute to rounded.</text>
      <biological_entity constraint="basal" id="o15545" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character constraint="in outline" constraintid="o15546" is_modifier="false" name="shape" src="d0_s2" value="pentagonal" value_original="pentagonal" />
        <character is_modifier="false" modifier="deeply" name="shape" notes="" src="d0_s2" value="3-5-parted" value_original="3-5-parted" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="length" src="d0_s2" to="5.2" to_unit="cm" />
        <character char_type="range_value" from="2.7" from_unit="cm" name="width" src="d0_s2" to="9.8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15546" name="outline" name_original="outline" src="d0_s2" type="structure" />
      <biological_entity id="o15547" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="1-2×-lobed" value_original="1-2×-lobed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="parted" value_original="parted" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o15548" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s2" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o15549" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lobulate" value_original="lobulate" />
      </biological_entity>
      <biological_entity id="o15550" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: receptacle glabrous;</text>
      <biological_entity id="o15551" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o15552" name="receptacle" name_original="receptacle" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals spreading, 4-6 (-9) × 2-5 mm, hispid;</text>
      <biological_entity id="o15553" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o15554" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="9" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s4" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals 5, yellow, 8-11 (-17) × 7-13 mm.</text>
      <biological_entity id="o15555" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o15556" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="17" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s5" to="11" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s5" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads of achenes globose, 5-7 (-10) mm wide;</text>
      <biological_entity id="o15557" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="globose" value_original="globose" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="width" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15558" name="achene" name_original="achenes" src="d0_s6" type="structure" />
      <relation from="o15557" id="r2154" name="part_of" negation="false" src="d0_s6" to="o15558" />
    </statement>
    <statement id="d0_s7">
      <text>achenes 2-3 × 1.8-2.4 mm, glabrous, margin forming narrow rib 0.1-0.2 mm wide;</text>
      <biological_entity id="o15559" name="achene" name_original="achenes" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s7" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s7" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15560" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s7" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15561" name="rib" name_original="rib" src="d0_s7" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
      </biological_entity>
      <relation from="o15560" id="r2155" name="forming" negation="false" src="d0_s7" to="o15561" />
    </statement>
    <statement id="d0_s8">
      <text>beak persistent, deltate, usually with tip short or long, straight or curved, subulate, 0.2-1 mm. 2n = 14.</text>
      <biological_entity id="o15562" name="beak" name_original="beak" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s8" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s8" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s8" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15563" name="tip" name_original="tip" src="d0_s8" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15564" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="14" value_original="14" />
      </biological_entity>
      <relation from="o15562" id="r2156" modifier="usually" name="with" negation="false" src="d0_s8" to="o15563" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (May–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Meadows, stream banks, roadsides, and old fields</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="old fields" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Largely introduced; Greenland; St. Pierre and Miquelon; Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.W.T., N.S., Ont., P.E.I., Que., Sask.; Ala., Alaska, Ariz., Calif., Conn., Del., D.C., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., Maine, Md., Mass., Mich., Minn., Mo., Mont., Nebr., Nev., N.H., N.J., N.Y., N.C., N.Dak., Ohio, Oreg., Pa., R.I., S.C., S.Dak., Tenn., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; South America; Eurasia; Pacific Islands; Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Largely" establishment_means="introduced" />
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Renoncule âcre</other_name>
  <other_name type="common_name">bouton d'or</other_name>
  <discussion>Ranunculus acris is variable in form and division of leaves, size of achene beak, and form of indument on the proximal stem. Most North American plants are weedy and have poorly differentiated caudices; these forms probably were introduced from Eurasia. Rhizomatous plants with large flowers (parenthetic measurements above) found in the Aleutian Islands of Alaska and in Greenland are probably native. Aleutian populations of this form have been called R. acris var. frigidus Regel or R. grandis Honda var. austrokurilensis (Tatewaki) H. Hara. Both names were originally applied to Asiatic plants, and their applicability to American specimens is open to question.</discussion>
  <discussion>Some Native American tribes used Ranunculus acris as an analgesic, a dermatological or oral aid, an antidiarrheal, antihermorrhagic, and a sedative (D. E. Moerman 1986).</discussion>
  
</bio:treatment>