<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="(Prantl) L. D. Benson" date="1936" rank="section">epirotes</taxon_name>
    <taxon_name authority="Goldie" date="1822" rank="species">rhomboideus</taxon_name>
    <place_of_publication>
      <publication_title>Edinburgh J. Sci.</publication_title>
      <place_in_publication>6: 329. 1822</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section epirotes;species rhomboideus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501204</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, 5-22 cm, pilose or occasionally glabrous, each with 3-12 flowers.</text>
      <biological_entity id="o25398" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="22" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o25399" name="flower" name_original="flowers" src="d0_s0" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s0" to="12" />
      </biological_entity>
      <relation from="o25398" id="r3428" name="with" negation="false" src="d0_s0" to="o25399" />
    </statement>
    <statement id="d0_s1">
      <text>Roots slender, 0.8-1.8 mm thick.</text>
      <biological_entity id="o25400" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="thickness" src="d0_s1" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves persistent, blades ovate to rhombic, undivided or rarely innermost 3-parted, 1.1-5.3 × 0.9-3.6 cm, base obtuse, margins crenate with 5 crenae, apex rounded.</text>
      <biological_entity constraint="basal" id="o25401" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o25402" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="rhombic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="undivided" value_original="undivided" />
      </biological_entity>
      <biological_entity id="o25403" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="rarely" name="position" src="d0_s2" value="innermost" value_original="innermost" />
        <character is_modifier="false" name="shape" src="d0_s2" value="3-parted" value_original="3-parted" />
        <character char_type="range_value" from="1.1" from_unit="cm" name="length" src="d0_s2" to="5.3" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="cm" name="width" src="d0_s2" to="3.6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o25404" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o25405" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character constraint="with crenae" constraintid="o25406" is_modifier="false" name="shape" src="d0_s2" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o25406" name="crena" name_original="crenae" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o25407" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: pedicels pilose;</text>
      <biological_entity id="o25408" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o25409" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>receptacle pilose;</text>
      <biological_entity id="o25410" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o25411" name="receptacle" name_original="receptacle" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals 4-6 × 1.5-3 mm, abaxially pilose, hairs colorless;</text>
      <biological_entity id="o25412" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o25413" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s5" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o25414" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="colorless" value_original="colorless" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals 5, 6-8 × 2-4 mm;</text>
      <biological_entity id="o25415" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o25416" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>nectary scale glabrous.</text>
      <biological_entity id="o25417" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="nectary" id="o25418" name="scale" name_original="scale" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Heads of achenes depressed-globose, 4-6 × 5-7 mm;</text>
      <biological_entity id="o25419" name="head" name_original="heads" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="depressed-globose" value_original="depressed-globose" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s8" to="6" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25420" name="achene" name_original="achenes" src="d0_s8" type="structure" />
      <relation from="o25419" id="r3429" name="part_of" negation="false" src="d0_s8" to="o25420" />
    </statement>
    <statement id="d0_s9">
      <text>achenes 1.8-2.2 × 1.2-1.8 mm, glabrous;</text>
      <biological_entity id="o25421" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s9" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s9" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>beak slender, curved, 0.2-0.3 mm. 2n = 16.</text>
      <biological_entity id="o25422" name="beak" name_original="beak" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character is_modifier="false" name="course" src="d0_s10" value="curved" value_original="curved" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s10" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25423" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (Apr–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="Apr-Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies, or occasionally open woods or thickets</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
        <character name="habitat" value="open woods" modifier="occasionally" />
        <character name="habitat" value="thickets" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Ont., Sask.; Ill., Iowa, Mich., Minn., Mont., Nebr., N.Dak., S.Dak., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>38.</number>
  <discussion>In addition to the range given above, L. D. Benson (1948) cited nineteenth-century specimens from Quebec, New York, Massachusetts, and Rhode Island. No modern specimens have been seen from those areas.</discussion>
  
</bio:treatment>