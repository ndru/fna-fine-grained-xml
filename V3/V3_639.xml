<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">helleborus</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">viridis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 557. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus helleborus;species viridis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">233500660</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, 1.2-3.5 dm.</text>
      <biological_entity id="o22855" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1.2" from_unit="dm" name="some_measurement" src="d0_s0" to="3.5" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems fluted and ridged.</text>
      <biological_entity id="o22856" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="fluted" value_original="fluted" />
        <character is_modifier="false" name="shape" src="d0_s1" value="ridged" value_original="ridged" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 2-5 or more;</text>
      <biological_entity id="o22857" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal leaves with petioles to 30cm;</text>
      <biological_entity constraint="basal" id="o22858" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o22859" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="30" to_unit="cm" />
      </biological_entity>
      <relation from="o22858" id="r3120" name="with" negation="false" src="d0_s3" to="o22859" />
    </statement>
    <statement id="d0_s4">
      <text>blades to 40cm wide, lobes 6-15, 2-cleft or incised, 6-21 × 1.5-4.2 cm, margins sharply serrate;</text>
      <biological_entity id="o22860" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s4" to="40" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22861" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s4" to="15" />
        <character is_modifier="false" name="shape" src="d0_s4" value="2-cleft" value_original="2-cleft" />
        <character is_modifier="false" name="shape" src="d0_s4" value="incised" value_original="incised" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s4" to="21" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s4" to="4.2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22862" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline leaves similar to basal but smaller, sessile or short-petioled.</text>
      <biological_entity constraint="cauline" id="o22863" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="short-petioled" value_original="short-petioled" />
      </biological_entity>
      <biological_entity constraint="basal" id="o22864" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="smaller" value_original="smaller" />
      </biological_entity>
      <relation from="o22863" id="r3121" name="to" negation="false" src="d0_s5" to="o22864" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences: peduncles 2-5 cm.</text>
      <biological_entity id="o22865" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o22866" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers pendent, 35-60 mm diam.;</text>
      <biological_entity id="o22867" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="pendent" value_original="pendent" />
        <character char_type="range_value" from="35" from_unit="mm" name="diameter" src="d0_s7" to="60" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals scarcely imbricate, 9-20 mm wide;</text>
      <biological_entity id="o22868" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="scarcely" name="arrangement" src="d0_s8" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s8" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals upwardly curved, cornucopia-like with involute margins.</text>
      <biological_entity id="o22869" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="upwardly" name="course" src="d0_s9" value="curved" value_original="curved" />
        <character constraint="with margins" constraintid="o22870" is_modifier="false" name="shape" src="d0_s9" value="cornucopia-like" value_original="cornucopia-like" />
      </biological_entity>
      <biological_entity id="o22870" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape_or_vernation" src="d0_s9" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Follicles 3-6, connate at base, swollen;</text>
      <biological_entity id="o22871" name="follicle" name_original="follicles" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s10" to="6" />
        <character constraint="at base" constraintid="o22872" is_modifier="false" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="swollen" value_original="swollen" />
      </biological_entity>
      <biological_entity id="o22872" name="base" name_original="base" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>body 14-25 mm;</text>
      <biological_entity id="o22873" name="body" name_original="body" src="d0_s11" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s11" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>beak persistent.</text>
      <biological_entity id="o22874" name="beak" name_original="beak" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering winter–early spring (Dec–Mar).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early spring" from="winter" />
        <character name="flowering time" char_type="range_value" to="Mar" from="Dec" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waste places, abandoned gardens, shaded roadsides, and calcareous woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waste places" />
        <character name="habitat" value="abandoned gardens" />
        <character name="habitat" value="shaded roadsides" />
        <character name="habitat" value="calcareous woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-400m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ill., Md., Mich., N.J., N.Y., Ohio, Pa., W.Va.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Green hellebore</other_name>
  <discussion>This species is not as commonly planted as it once was, and most records are old.</discussion>
  
</bio:treatment>