<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Gray" date="unknown" rank="family">ceratophyllaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ceratophyllum</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">demersum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 992. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ceratophyllaceae;genus ceratophyllum;species demersum</taxon_hierarchy>
    <other_info_on_name type="special_status">W2</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200007091</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ceratophyllum</taxon_name>
    <taxon_name authority="Chamisso" date="unknown" rank="species">apiculatum</taxon_name>
    <taxon_hierarchy>genus Ceratophyllum;species apiculatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems to 3 m;</text>
      <biological_entity id="o6138" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="3" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>apical leaf whorls densely crowded.</text>
      <biological_entity constraint="leaf" id="o6139" name="whorl" name_original="whorls" src="d0_s1" type="structure" constraint_original="apical leaf">
        <character is_modifier="false" modifier="densely" name="arrangement" src="d0_s1" value="crowded" value_original="crowded" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves bright green, coarse-textured.</text>
      <biological_entity id="o6140" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="bright green" value_original="bright green" />
        <character is_modifier="false" name="texture" src="d0_s2" value="coarse-textured" value_original="coarse-textured" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade simple or forked into 2-4 (-5) ultimate segments (forking of largest leaves 1st or 2d order, rarely 3d order), segments not inflated, mature leaf whorls 1.5-6 cm diam., marginal denticles conspicuous, usually strongly raised on broad base of green tissue;</text>
      <biological_entity id="o6141" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character constraint="into ultimate segments" constraintid="o6142" is_modifier="false" name="shape" src="d0_s3" value="forked" value_original="forked" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o6142" name="segment" name_original="segments" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s3" to="5" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <biological_entity id="o6143" name="segment" name_original="segments" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o6144" name="whorl" name_original="whorls" src="d0_s3" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s3" value="mature" value_original="mature" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="diameter" src="d0_s3" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6146" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="true" name="width" src="d0_s3" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o6147" name="tissue" name_original="tissue" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="true" name="width" src="d0_s3" value="broad" value_original="broad" />
      </biological_entity>
      <relation from="o6145" id="r853" modifier="usually strongly" name="raised on" negation="false" src="d0_s3" to="o6146" />
      <relation from="o6145" id="r854" modifier="usually strongly" name="raised on" negation="false" src="d0_s3" to="o6147" />
    </statement>
    <statement id="d0_s4">
      <text>1st leaves of plumule simple.</text>
      <biological_entity constraint="marginal" id="o6145" name="denticle" name_original="denticles" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o6148" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o6149" name="plumule" name_original="plumule" src="d0_s4" type="structure" />
      <relation from="o6148" id="r855" name="part_of" negation="false" src="d0_s4" to="o6149" />
    </statement>
    <statement id="d0_s5">
      <text>Achene dark green or reddish-brown, body (excluding spines) 3.5-6 × 2-4 × 1-2.5 mm, basal spines or tubercles 2 (rarely absent), straight or curved, 0.1-12 mm, spine bases occasionally inconspicuously webbed, marginal spines absent, terminal spine straight, 0.5-14 mm, margins wingless.</text>
      <biological_entity id="o6150" name="achene" name_original="achene" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
      <biological_entity id="o6151" name="body" name_original="body" src="d0_s5" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s5" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="height" src="d0_s5" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o6152" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s5" value="curved" value_original="curved" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o6153" name="tubercle" name_original="tubercles" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s5" value="curved" value_original="curved" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="spine" id="o6154" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="occasionally inconspicuously" name="pubescence" src="d0_s5" value="webbed" value_original="webbed" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o6155" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o6156" name="spine" name_original="spine" src="d0_s5" type="structure">
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>2n = 24, 38, 40, 48.</text>
      <biological_entity id="o6157" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="wingless" value_original="wingless" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6158" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="24" value_original="24" />
        <character name="quantity" src="d0_s6" value="38" value_original="38" />
        <character name="quantity" src="d0_s6" value="40" value_original="40" />
        <character name="quantity" src="d0_s6" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–late fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fresh to slightly brackish rivers, streams, ditches, lakes, ponds, pools, marshes, swamps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fresh to slightly brackish rivers" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="pools" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="swamps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., N.W.T., N.S., Ont., P.E.I., Que., Sask., Yukon; Ala., Alaska, Ariz., Ark., Calif., Colo., Conn., Del., D.C., Fla., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Coontail</other_name>
  <other_name type="common_name">hornwort</other_name>
  <other_name type="common_name">cornifle nageante</other_name>
  <discussion>Specimens of Ceratophyllum demersum with short basal spines or tubercles have been misidentified as C. submersum Linnaeus, a species not known in the New World despite reports to the contrary. Ceratophyllum demersum is the most common species of Ceratophyllum in North America and also the least likely to be found with fruit, its reproduction being primarily asexual. Predominantly low leaf order is, therefore, the most reliable means of identifying this species.</discussion>
  <discussion>Noted for its prolific growth, Ceratophyllum demersum occasionally has attained status as a serious weed.</discussion>
  
</bio:treatment>