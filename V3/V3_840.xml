<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">berberidaceae</taxon_name>
    <taxon_name authority="C. Morren &amp; Decaisne" date="1834" rank="genus">vancouveria</taxon_name>
    <taxon_name authority="Calloni" date="1887" rank="species">planipetala</taxon_name>
    <place_of_publication>
      <publication_title>Malpighia</publication_title>
      <place_in_publication>1: 266, plate 6. 1887</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family berberidaceae;genus vancouveria;species planipetala</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501342</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Vancouveria</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">parviflora</taxon_name>
    <taxon_hierarchy>genus Vancouveria;species parviflora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves persistent, 2 (-3) -ternately compound, 10-30 cm;</text>
      <biological_entity id="o17348" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="2(-3)-ternately" name="architecture" src="d0_s0" value="compound" value_original="compound" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>petiole 1-15 cm, sparsely hairy, becoming glabrous.</text>
      <biological_entity id="o17349" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="15" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="becoming" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaflet blades rounded-deltate to rounded-pentagonal, often broader than long, obscurely 3-lobed, base cordate, margins conspicuously thickened, crisped, apex minutely notched;</text>
      <biological_entity constraint="leaflet" id="o17350" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="rounded-deltate" name="shape" src="d0_s2" to="rounded-pentagonal" />
        <character is_modifier="false" name="width" src="d0_s2" value="often broader than long" value_original="often broader than long" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s2" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity id="o17351" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o17352" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="conspicuously" name="size_or_width" src="d0_s2" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="shape" src="d0_s2" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o17353" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s2" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>surfaces abaxially glabrous or sparsely hairy, adaxially glabrous.</text>
      <biological_entity id="o17354" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: peduncle 1-2 dm;</text>
      <biological_entity id="o17355" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o17356" name="peduncle" name_original="peduncle" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s4" to="2" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pedicel 1-3 cm, stipitate-glandular.</text>
      <biological_entity id="o17357" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o17358" name="pedicel" name_original="pedicel" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="3" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 20-50;</text>
      <biological_entity id="o17359" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s6" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracteoles 6-9, white to yellow, glands absent;</text>
      <biological_entity id="o17360" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s7" to="9" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="yellow" />
      </biological_entity>
      <biological_entity id="o17361" name="gland" name_original="glands" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 6, white to yellow, oblanceolate, 4-5 mm, glands absent;</text>
      <biological_entity id="o17362" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="6" value_original="6" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s8" to="yellow" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17363" name="gland" name_original="glands" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 6, white, sometimes lavender-tinged, 3-4 mm, margins entire, apex notched, not reflexed, lateral lobes bearing nectaries, nectaries golden;</text>
      <biological_entity id="o17364" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="6" value_original="6" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="lavender-tinged" value_original="lavender-tinged" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17365" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o17366" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="notched" value_original="notched" />
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o17367" name="lobe" name_original="lobes" src="d0_s9" type="structure" />
      <biological_entity id="o17368" name="nectary" name_original="nectaries" src="d0_s9" type="structure" />
      <biological_entity id="o17369" name="nectary" name_original="nectaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="golden" value_original="golden" />
      </biological_entity>
      <relation from="o17367" id="r2380" name="bearing" negation="false" src="d0_s9" to="o17368" />
    </statement>
    <statement id="d0_s10">
      <text>filaments without glands.</text>
      <biological_entity id="o17370" name="filament" name_original="filaments" src="d0_s10" type="structure" />
      <biological_entity id="o17371" name="gland" name_original="glands" src="d0_s10" type="structure" />
      <relation from="o17370" id="r2381" name="without" negation="false" src="d0_s10" to="o17371" />
    </statement>
    <statement id="d0_s11">
      <text>Follicles greenish brown, 4-7 mm including beak, beak 2 mm, glands absent.</text>
      <biological_entity id="o17372" name="follicle" name_original="follicles" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="greenish brown" value_original="greenish brown" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17373" name="beak" name_original="beak" src="d0_s11" type="structure" />
      <biological_entity id="o17374" name="beak" name_original="beak" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o17375" name="gland" name_original="glands" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o17372" id="r2382" name="including" negation="false" src="d0_s11" to="o17373" />
    </statement>
    <statement id="d0_s12">
      <text>Seeds 1-2, black, lunate, 3-4 mm. 2n = 24.</text>
      <biological_entity id="o17376" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s12" to="2" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lunate" value_original="lunate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17377" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (May–Jun); fruiting spring–summer (Jun–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="May-Jun" />
        <character name="fruiting time" char_type="range_value" to="summer" from="spring" />
        <character name="fruiting time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Redwood forests, shaded areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="redwood forests" />
        <character name="habitat" value="shaded areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50-1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Redwood-ivy</other_name>
  <other_name type="common_name">redwood inside-out flower</other_name>
  
</bio:treatment>