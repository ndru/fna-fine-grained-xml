<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="(N. I. Malyutin) M. J. Warnock" date="1995" rank="subsection">grumosa</taxon_name>
    <taxon_name authority="A. Gray" date="1887" rank="species">nuttallii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>12: 54. 1887</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection grumosa;species nuttallii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500526</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 20-60 (-90) cm;</text>
      <biological_entity id="o2796" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base usually reddish, pubescent.</text>
      <biological_entity id="o2797" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly on proximal 1/2 of stem;</text>
      <biological_entity id="o2798" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o2799" name="1/2" name_original="1/2" src="d0_s2" type="structure" />
      <biological_entity id="o2800" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o2798" id="r359" name="on" negation="false" src="d0_s2" to="o2799" />
      <relation from="o2799" id="r360" name="part_of" negation="false" src="d0_s2" to="o2800" />
    </statement>
    <statement id="d0_s3">
      <text>basal leaves 0-2 at anthesis;</text>
      <biological_entity constraint="basal" id="o2801" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="0" name="quantity" src="d0_s3" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves 3-10 at anthesis;</text>
      <biological_entity constraint="cauline" id="o2802" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="3" name="quantity" src="d0_s4" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.3-19 cm.</text>
      <biological_entity id="o2803" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s5" to="19" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaf-blade round to reniform, 2-8 × 3-14 cm, pubescent;</text>
      <biological_entity id="o2804" name="leaf-blade" name_original="leaf-blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s6" to="reniform" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="8" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s6" to="14" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ultimate lobes 5-18, width 4-7 mm (basal), 1-5 mm (cauline), widest at middle or in proximal 1/2.</text>
      <biological_entity constraint="ultimate" id="o2805" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="18" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
        <character constraint="at middle or in proximal 1/2" is_modifier="false" name="width" src="d0_s7" value="widest" value_original="widest" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences 5-25 (-40) -flowered, at least 2 times longer than wide;</text>
      <biological_entity id="o2806" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="5-25(-40)-flowered" value_original="5-25(-40)-flowered" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicel 1.5-4 (-9) cm, puberulent;</text>
      <biological_entity id="o2807" name="pedicel" name_original="pedicel" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="9" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s9" to="4" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles 2-3 mm from flowers, green, linear, 4-6 mm, puberulent.</text>
      <biological_entity id="o2808" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="from flowers" constraintid="o2809" from="2" from_unit="mm" name="location" src="d0_s10" to="3" to_unit="mm" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o2809" name="flower" name_original="flowers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals bluish purple to yellowish, (± drab), often partly fading upon drying, puberulent, lateral sepals ± spreading, 8-11 × 3-6 mm, spurs straight, slightly ascending, 9-13 mm;</text>
      <biological_entity id="o2810" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o2811" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="bluish purple" name="coloration" src="d0_s11" to="yellowish" />
        <character constraint="upon lateral sepals" constraintid="o2812" is_modifier="false" modifier="often partly" name="coloration" src="d0_s11" value="fading" value_original="fading" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" notes="" src="d0_s11" to="11" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" notes="" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o2812" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="true" name="condition" src="d0_s11" value="drying" value_original="drying" />
        <character is_modifier="true" name="pubescence" src="d0_s11" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o2813" name="spur" name_original="spurs" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s11" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower petal blades ± covering stamens, 4-6 mm, clefts 0.5-2 mm;</text>
      <biological_entity id="o2814" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="petal" id="o2815" name="blade" name_original="blades" src="d0_s12" type="structure" constraint_original="lower petal">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2816" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o2817" name="cleft" name_original="clefts" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o2815" id="r361" modifier="more or less" name="covering" negation="false" src="d0_s12" to="o2816" />
    </statement>
    <statement id="d0_s13">
      <text>hairs well dispersed, mostly near margins and base of cleft, white to yellow or blue.</text>
      <biological_entity id="o2818" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o2819" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="well" name="density" src="d0_s13" value="dispersed" value_original="dispersed" />
      </biological_entity>
      <biological_entity id="o2820" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character char_type="range_value" from="white" modifier="of cleft" name="coloration" src="d0_s13" to="yellow or blue" />
      </biological_entity>
      <biological_entity id="o2821" name="base" name_original="base" src="d0_s13" type="structure">
        <character char_type="range_value" from="white" modifier="of cleft" name="coloration" src="d0_s13" to="yellow or blue" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits 10-14 (-18) mm, 3.5-4 times longer than wide, pubescent.</text>
      <biological_entity id="o2822" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="18" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s14" to="14" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s14" value="3.5-4" value_original="3.5-4" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds wing-margined;</text>
      <biological_entity id="o2823" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="wing-margined" value_original="wing-margined" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>seed-coat cell surfaces smooth.</text>
      <biological_entity constraint="cell" id="o2824" name="surface" name_original="surfaces" src="d0_s16" type="structure" constraint_original="seed-coat cell">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>54.</number>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <discussion>Delphinium nuttallii has often been confused with D. menziesii; it may be distinguished by consistently smaller flowers and usually more flowers per plant than in the latter. Interestingly, each species produces both blue-purple and yellowish flower colors, in separate populations.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals blue or bluish purple; spur more than 10 mm.</description>
      <determination>54a Delphinium nuttallii subsp. nuttallii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals white or light yellow; spur less than 10 mm.</description>
      <determination>54b Delphinium nuttallii subsp. ochroleucum</determination>
    </key_statement>
  </key>
</bio:treatment>