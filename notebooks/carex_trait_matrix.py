
# coding: utf-8

# In[2]:


# Extract a taxon by trait matrix of presence/absence for Carex spp., 
# or potentially a two column CSV with species names and trait names
# Use property organization and syntactical assertions to do so!


# In[ ]:


# Step one: extract the properties through XML scraping
# What have we been able to export from the volume importer?


# In[ ]:


# How to implement inheritence in my script


# In[14]:


get_ipython().run_line_magic('load_ext', 'autoreload')
get_ipython().run_line_magic('autoreload', '2')

import os
import sys
import pandas as pd
import numpy as np
import csv
import re
from lxml import etree


# In[25]:


module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path)
from src.functions.parse import *


# In[6]:


# Read in Output documents and run query

fields = ['File', 'Volume', 'AcceptedFamily', 'AcceptedGenus', 'AcceptedTaxonHierarchy', 'AcceptedTaxonName', 'AcceptedAuthority', 'BasionymTaxonNames', 'SynonymTaxonNames', 'Distribution']
file_name = "taxa_names_for_cam.csv"


# In[12]:


for subdir, dirs, files in os.walk(".."):
    for file in files:
        #print(os.path.join(subdir, file))
        file_path = subdir + os.sep + file
        taxon = None

        if (file_path.endswith(".xml")):
            print("Searching" + file_path)
            row = collect_data(file_path)
            # csvwriter.writerow(row)
            print(row)


# In[33]:


file_path = '/Users/jocelynpender/fna/fna-data/fna-fine-grained-xml/V23/V23_3.xml'
tree = etree.parse(file_path)

traits = tree.findall("//biological_entity")
for trait in traits:
    string_trait = trait.attrib['name']
    characters = trait.findall(".//character")
    print(string_trait)
    for character in characters:
        character = character.attrib['name']
        print(character)
    


# In[ ]:


const formatPropertyName = (entity, character) => {
  if (character.hasAttribute('constraint')) {
    return `${entity.getAttribute('name')} ${character.getAttribute('name')} ${character.getAttribute('constraint')}`
  } else {
    return `${entity.getAttribute('name')} ${character.getAttribute('name')}`
  }
}

