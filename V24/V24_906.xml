<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">641</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">AGROSTIS</taxon_name>
    <taxon_name authority="Roth" date="unknown" rank="species">gigantea</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus agrostis;species gigantea</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agrostis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">stolonifera</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">major</taxon_name>
    <taxon_hierarchy>genus agrostis;species stolonifera;variety major</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agrostis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">gigantea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">ramosa</taxon_name>
    <taxon_hierarchy>genus agrostis;species gigantea;variety ramosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agrostis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">gigantea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">dispar</taxon_name>
    <taxon_hierarchy>genus agrostis;species gigantea;variety dispar</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agrostis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">alba</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">major</taxon_name>
    <taxon_hierarchy>genus agrostis;species alba;variety major</taxon_hierarchy>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Redtop</other_name>
  <other_name type="common_name">Agrostide blanche</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, rhizomes to 25 cm, not stoloniferous.</text>
      <biological_entity id="o24550" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o24551" name="rhizom" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="25" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 20-120 cm, erect, sometimes geniculate at the base, sometimes rooting at the lower nodes, with 4-7 nodes.</text>
      <biological_entity id="o24552" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="120" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="at base" constraintid="o24553" is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
        <character constraint="at lower nodes" constraintid="o24554" is_modifier="false" modifier="sometimes" name="architecture" notes="" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o24553" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity constraint="lower" id="o24554" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <biological_entity id="o24555" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s2" to="7" />
      </biological_entity>
      <relation from="o24552" id="r3877" name="with" negation="false" src="d0_s2" to="o24555" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly cauline, sheaths smooth or sparsely scabridulous;</text>
      <biological_entity id="o24556" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o24557" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o24558" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely" name="relief" src="d0_s3" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules longer than wide, dorsal surfaces usually scabrous, sometimes smooth, apices rounded to truncate, erose to lacerate, basal ligules 1-4.5 mm, upper ligules 2-7 mm;</text>
      <biological_entity id="o24559" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character constraint="than wide , dorsal surfaces" constraintid="o24560" is_modifier="false" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_relief" notes="" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o24560" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="true" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character is_modifier="false" modifier="usually" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o24561" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded to truncate" value_original="rounded to truncate" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s4" value="erose" value_original="erose" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lacerate" value_original="lacerate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o24562" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="upper" id="o24563" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 4-10 cm long, 3-8 mm wide, flat.</text>
      <biological_entity id="o24564" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s5" to="10" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 8-25 (30) cm long, less than 1/2 the length of the culm, (1.5) 3-15 cm wide, erect, open, broadly ovate, exserted from the upper sheaths at maturity, lowest node with (1) 3-8 branches;</text>
      <biological_entity id="o24565" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character name="length" src="d0_s6" unit="cm" value="30" value_original="30" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s6" to="25" to_unit="cm" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s6" to="1/2" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o24566" name="culm" name_original="culm" src="d0_s6" type="structure">
        <character name="width" notes="alterIDs:o24566" src="d0_s6" unit="cm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" notes="alterIDs:o24566" src="d0_s6" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="upper" id="o24567" name="sheath" name_original="sheaths" src="d0_s6" type="structure" />
      <biological_entity constraint="lowest" id="o24568" name="node" name_original="node" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="maturity" value_original="maturity" />
      </biological_entity>
      <biological_entity id="o24569" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s6" value="1" value_original="1" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="8" />
      </biological_entity>
      <relation from="o24565" id="r3878" name="exserted from the" negation="false" src="d0_s6" to="o24567" />
      <relation from="o24565" id="r3879" name="at" negation="false" src="d0_s6" to="o24568" />
      <relation from="o24568" id="r3880" name="with" negation="false" src="d0_s6" to="o24569" />
    </statement>
    <statement id="d0_s7">
      <text>branches scabrous, spreading during and after anthesis, usually some branches spikelet-bearing to the base, lower branches 4-9 cm, usually with many shorter secondary branches resulting in crowding of the spikelets, spikelets restricted to the distal 1/2 of the branches and not crowded in shade plants;</text>
      <biological_entity id="o24570" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
        <character constraint="after branches" constraintid="o24571" is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o24571" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s7" value="anthesis" value_original="anthesis" />
      </biological_entity>
      <biological_entity id="o24572" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity constraint="lower" id="o24573" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s7" to="9" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="shorter secondary" id="o24574" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="many" value_original="many" />
        <character constraint="of spikelets" constraintid="o24575" is_modifier="false" name="arrangement" src="d0_s7" value="crowding" value_original="crowding" />
      </biological_entity>
      <biological_entity id="o24575" name="spikelet" name_original="spikelets" src="d0_s7" type="structure" />
      <biological_entity id="o24576" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character constraint="in plants" constraintid="o24579" is_modifier="false" modifier="not" name="arrangement" src="d0_s7" value="crowded" value_original="crowded" />
      </biological_entity>
      <biological_entity id="o24577" name="the distal 1/2" name_original="the distal 1/2" src="d0_s7" type="structure" />
      <biological_entity id="o24578" name="branch" name_original="branches" src="d0_s7" type="structure" />
      <biological_entity id="o24579" name="plant" name_original="plants" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration_or_habitat" src="d0_s7" value="shade" value_original="shade" />
      </biological_entity>
      <relation from="o24571" id="r3881" name="to" negation="false" src="d0_s7" to="o24572" />
      <relation from="o24573" id="r3882" modifier="usually" name="with" negation="false" src="d0_s7" to="o24574" />
      <relation from="o24576" id="r3883" name="restricted to" negation="false" src="d0_s7" to="o24577" />
      <relation from="o24576" id="r3884" name="restricted to" negation="false" src="d0_s7" to="o24578" />
    </statement>
    <statement id="d0_s8">
      <text>pedicels 0.3-3.4 (4.2) mm.</text>
      <biological_entity id="o24580" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="mm" value="4.2" value_original="4.2" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s8" to="3.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets narrowly ovate to lance¬olate, green and slightly to strongly suffused with purple.</text>
      <biological_entity id="o24581" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="false" modifier="slightly to strongly; strongly" name="coloration" src="d0_s9" value="suffused with purple" value_original="suffused with purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Glumes subequal, 1.7-3.2 mm, lanceolate, 1-veined, acute to apiculate;</text>
      <biological_entity id="o24582" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s10" to="3.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="1-veined" value_original="1-veined" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower glumes scabrous on the distal 1/2 of the midvein;</text>
      <biological_entity constraint="lower" id="o24583" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character constraint="on distal 1/2" constraintid="o24584" is_modifier="false" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o24584" name="1/2" name_original="1/2" src="d0_s11" type="structure" />
      <biological_entity id="o24585" name="midvein" name_original="midvein" src="d0_s11" type="structure" />
      <relation from="o24584" id="r3885" name="part_of" negation="false" src="d0_s11" to="o24585" />
    </statement>
    <statement id="d0_s12">
      <text>upper glumes scabridulous on the distal 1/2 of the midvein;</text>
      <biological_entity constraint="upper" id="o24586" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character constraint="on distal 1/2" constraintid="o24587" is_modifier="false" name="relief" src="d0_s12" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o24587" name="1/2" name_original="1/2" src="d0_s12" type="structure" />
      <biological_entity id="o24588" name="midvein" name_original="midvein" src="d0_s12" type="structure" />
      <relation from="o24587" id="r3886" name="part_of" negation="false" src="d0_s12" to="o24588" />
    </statement>
    <statement id="d0_s13">
      <text>callus hairs to 0.5 mm, sparse;</text>
      <biological_entity constraint="callus" id="o24589" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="count_or_density" src="d0_s13" value="sparse" value_original="sparse" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 1.5-2.2 mm, opaque to translucent, smooth, 3-5-veined, veins usually obscure, sometimes prominent throughout or distally, often excurrent to 0.2 mm, apices usually acute, sometimes obtuse or truncate, usually unawned, rarely with a 0.4-1.5 (3) mm straight awn arising from near the apices to near the base;</text>
      <biological_entity id="o24590" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="opaque" name="coloration" src="d0_s14" to="translucent" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
      <biological_entity id="o24591" name="vein" name_original="veins" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="prominence" src="d0_s14" value="obscure" value_original="obscure" />
        <character is_modifier="false" modifier="sometimes; throughout; throughout; distally" name="prominence" src="d0_s14" value="prominent" value_original="prominent" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s14" value="excurrent" value_original="excurrent" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s14" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24592" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s14" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s14" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s14" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o24593" name="awn" name_original="awn" src="d0_s14" type="structure">
        <character is_modifier="true" name="atypical_some_measurement" src="d0_s14" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="0.4" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s14" to="1.5" to_unit="mm" />
        <character is_modifier="true" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character constraint="from " constraintid="o24594" is_modifier="false" name="orientation" src="d0_s14" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o24594" name="apex" name_original="apices" src="d0_s14" type="structure" />
      <biological_entity id="o24595" name="base" name_original="base" src="d0_s14" type="structure" />
      <relation from="o24592" id="r3887" modifier="usually; rarely" name="with" negation="false" src="d0_s14" to="o24593" />
      <relation from="o24594" id="r3888" name="near" negation="false" src="d0_s14" to="o24595" />
    </statement>
    <statement id="d0_s15">
      <text>paleas 0.7-1.4 mm, about Vi the length of the lemmas, veins visible;</text>
      <biological_entity id="o24596" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s15" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24597" name="lemma" name_original="lemmas" src="d0_s15" type="structure" />
      <biological_entity id="o24598" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character is_modifier="false" name="length" src="d0_s15" value="visible" value_original="visible" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers 3, 1-1.4 mm.</text>
      <biological_entity id="o24599" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Caryopses 1-1.5 mm;</text>
      <biological_entity id="o24600" name="caryopse" name_original="caryopses" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s17" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>endosperm solid.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 42.</text>
      <biological_entity id="o24601" name="endosperm" name_original="endosperm" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="solid" value_original="solid" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24602" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Agrostis gigantea grows in fields, roadsides, ditches, and other disturbed habitats, mostly at lower elevations. It is a serious agricultural weed, as well as a valuable soil stabilizer. In the Flora region, its range extends from the subarctic to Mexico; it is considered to be native to Eurasia. It is more heat tolerant than most species of Agrostis.</discussion>
  <discussion>Agrostis gigantea has been confused with A. stolonifera (see next), from which it differs in having rhizomes and a more open panicle. Agrostis stolonifera has elongated leafy stolons, mainly all above the surface, that root at the nodes, and the panicles are condensed and often less strongly pigmented than in A. gigantea. Its distribution tends to be more northern and coastal where ditches and pond margins are common habitats, and its stolons enable it to form loose mats. Agrostis gigantea is ecologically adapted to a more extreme climate—hot summers/cold winters and drought—than A. stolonifera. It is also similar to A. capillaris (p. 639) and A. castellana (p. 639); it differs from both in its longer ligules, from A. capillaris in its less open panicles with spikelets near the base of the branches, and from A. castellana in being more extensively rhizomatous.</discussion>
  <discussion>When Agrostis gigantea grows in damp hollows under trees it becomes more like A. stolonifera, particularly when the inflorescence is young, not expanded, and pale. If the rootstock is not collected, identification is a major problem.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wash.;Del.;D.C.;Wis.;W.Va.;Mass.;Maine;N.H.;R.I.;Vt.;Wyo.;N.Mex.;Tex.;La.;N.Dak.;Nebr.;Tenn.;Pa.;Okla.;Alaska;Nev.;Va.;Colo.;Calif.;Ala.;Ark.;Ill.;Ga.;Ind.;Iowa;Ariz.;Idaho;Md.;Ohio;Utah;Mo.;Minn.;Mich.;Kans.;Mont.;Miss.;S.C.;Ky.;Oreg.;Alta.;B.C.;Greenland;Man.;N.B.;Nfld. and Labr. (Labr.);N.S.;N.W.T.;Ont.;P.E.I.;Que.;Sask.;Yukon</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>