<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="treatment_page">329</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ELYMUS</taxon_name>
    <taxon_name authority="(Scribn. &amp; J.G. Sm.) Gould" date="unknown" rank="species">arizonicus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus elymus;species arizonicus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pseudoroegneria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">arizonica</taxon_name>
    <taxon_hierarchy>genus pseudoroegneria;species arizonica</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agropyron</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">arizonicum</taxon_name>
    <taxon_hierarchy>genus agropyron;species arizonicum</taxon_hierarchy>
  </taxon_identification>
  <number>29</number>
  <other_name type="common_name">Arizona wheatgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, not rhizomatous.</text>
      <biological_entity id="o16754" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 45-100 cm, erect or decumbent at the base;</text>
      <biological_entity id="o16755" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="45" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="at base" constraintid="o16756" is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
      </biological_entity>
      <biological_entity id="o16756" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>nodes glabrous or almost so.</text>
      <biological_entity id="o16757" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s2" value="almost" value_original="almost" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves evenly distributed over the lower 1/2 of the culms;</text>
      <biological_entity id="o16758" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="over lower 1/2" constraintid="o16759" is_modifier="false" modifier="evenly" name="arrangement" src="d0_s3" value="distributed" value_original="distributed" />
      </biological_entity>
      <biological_entity constraint="lower" id="o16759" name="1/2" name_original="1/2" src="d0_s3" type="structure" />
      <biological_entity id="o16760" name="culm" name_original="culms" src="d0_s3" type="structure" />
      <relation from="o16759" id="r2689" name="part_of" negation="false" src="d0_s3" to="o16760" />
    </statement>
    <statement id="d0_s4">
      <text>sheaths glabrous;</text>
      <biological_entity id="o16761" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>auricles usually present, to 1 mm;</text>
      <biological_entity id="o16762" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules to 1 mm on the basal leaves, 1-3 mm on the flag leaves;</text>
      <biological_entity id="o16763" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="on basal leaves" constraintid="o16764" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
        <character char_type="range_value" constraint="on flag, leaves" constraintid="o16765, o16766" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o16764" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o16765" name="flag" name_original="flag" src="d0_s6" type="structure" />
      <biological_entity id="o16766" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>blades 2.5-6 mm wide, lax, abaxial surfaces smooth and glabrous, adaxial surfaces scabrous, with scattered 0.5-1 mm hairs, veins close together.</text>
      <biological_entity id="o16767" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s7" to="6" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o16768" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o16769" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o16770" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s7" value="scattered" value_original="scattered" />
        <character char_type="range_value" from="0.5" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16771" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="together" name="arrangement" src="d0_s7" value="close" value_original="close" />
      </biological_entity>
      <relation from="o16769" id="r2690" name="with" negation="false" src="d0_s7" to="o16770" />
    </statement>
    <statement id="d0_s8">
      <text>Spikes 12-25 cm long, 2.5-6 cm wide including the awns, 10-15 mm wide excluding the awns, flexuous, usually nodding or pendent at maturity, with 1 spikelet per node;</text>
      <biological_entity id="o16772" name="spike" name_original="spikes" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s8" to="25" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s8" to="6" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s8" to="15" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s8" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s8" value="nodding" value_original="nodding" />
        <character constraint="at maturity" is_modifier="false" name="orientation" src="d0_s8" value="pendent" value_original="pendent" />
      </biological_entity>
      <biological_entity id="o16773" name="awn" name_original="awns" src="d0_s8" type="structure" />
      <biological_entity id="o16774" name="awn" name_original="awns" src="d0_s8" type="structure" />
      <biological_entity id="o16775" name="spikelet" name_original="spikelet" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o16776" name="node" name_original="node" src="d0_s8" type="structure" />
      <relation from="o16772" id="r2691" name="including the" negation="false" src="d0_s8" to="o16773" />
      <relation from="o16772" id="r2692" name="excluding the" negation="false" src="d0_s8" to="o16774" />
      <relation from="o16772" id="r2693" name="with" negation="false" src="d0_s8" to="o16775" />
      <relation from="o16775" id="r2694" name="per" negation="false" src="d0_s8" to="o16776" />
    </statement>
    <statement id="d0_s9">
      <text>internodes 11-17 mm long, 0.4-1 mm wide, glabrous, mostly smooth, scabrous on the edges.</text>
      <biological_entity id="o16777" name="internode" name_original="internodes" src="d0_s9" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s9" to="17" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s9" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="mostly" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character constraint="on edges" constraintid="o16778" is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o16778" name="edge" name_original="edges" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 14-26 mm long, 6-8 mm wide, appressed to divergent, 1.5-2 times as long as the internodes, with 4-6 florets;</text>
      <biological_entity id="o16779" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="length" src="d0_s10" to="26" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s10" to="8" to_unit="mm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="divergent" value_original="divergent" />
        <character constraint="internode" constraintid="o16780" is_modifier="false" name="length" src="d0_s10" value="1.5-2 times as long as the internodes" />
      </biological_entity>
      <biological_entity id="o16780" name="internode" name_original="internodes" src="d0_s10" type="structure" />
      <biological_entity id="o16781" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s10" to="6" />
      </biological_entity>
      <relation from="o16779" id="r2695" name="with" negation="false" src="d0_s10" to="o16781" />
    </statement>
    <statement id="d0_s11">
      <text>rachillas glabrous;</text>
    </statement>
    <statement id="d0_s12">
      <text>disarticulation above the glumes and beneath each floret.</text>
      <biological_entity id="o16782" name="rachilla" name_original="rachillas" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o16783" name="floret" name_original="floret" src="d0_s12" type="structure" />
      <relation from="o16782" id="r2696" name="above the glumes and beneath" negation="false" src="d0_s12" to="o16783" />
    </statement>
    <statement id="d0_s13">
      <text>Glumes narrowly lanceolate, margins about 0.2 mm wide, 3 (5) -veined, the bases flat, evidently veined, margins hyaline, widest at about midlength, acute or acuminate, unawned or awned, awns to 4 mm, straight;</text>
      <biological_entity id="o16784" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o16785" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character name="width" src="d0_s13" unit="mm" value="0.2" value_original="0.2" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="3(5)-veined" value_original="3(5)-veined" />
      </biological_entity>
      <biological_entity id="o16786" name="base" name_original="bases" src="d0_s13" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s13" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="evidently" name="architecture" src="d0_s13" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity id="o16787" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="hyaline" value_original="hyaline" />
        <character constraint="at midlength" constraintid="o16788" is_modifier="false" name="width" src="d0_s13" value="widest" value_original="widest" />
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s13" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o16788" name="midlength" name_original="midlength" src="d0_s13" type="structure" />
      <biological_entity id="o16789" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower glumes 5-9 mm;</text>
      <biological_entity constraint="lower" id="o16790" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper glumes 8-10 mm;</text>
      <biological_entity constraint="upper" id="o16791" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s15" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lemmas 8-15 mm, scabrous, rounded on the back, awns 10-25 mm, arcuately diverging;</text>
      <biological_entity id="o16792" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s16" to="15" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s16" value="scabrous" value_original="scabrous" />
        <character constraint="on back" constraintid="o16793" is_modifier="false" name="shape" src="d0_s16" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o16793" name="back" name_original="back" src="d0_s16" type="structure" />
      <biological_entity id="o16794" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s16" to="25" to_unit="mm" />
        <character is_modifier="false" modifier="arcuately" name="orientation" src="d0_s16" value="diverging" value_original="diverging" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>paleas as long as or longer than the lemmas, tapering, apices truncate, about 0.3 mm wide;</text>
      <biological_entity id="o16795" name="palea" name_original="paleas" src="d0_s17" type="structure">
        <character constraint="than the lemmas" constraintid="o16796" is_modifier="false" name="length_or_size" src="d0_s17" value="longer" value_original="longer" />
        <character is_modifier="false" name="shape" src="d0_s17" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o16796" name="lemma" name_original="lemmas" src="d0_s17" type="structure" />
      <biological_entity id="o16797" name="apex" name_original="apices" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="truncate" value_original="truncate" />
        <character name="width" src="d0_s17" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>anthers 3-5 mm. 2n = 28.</text>
      <biological_entity id="o16798" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s18" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16799" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Elymus arizonicus grows in moist, rocky soil in mountain canyons of the southwestern United States and northern Mexico. When mature, the drooping spike and solitary spikelets make E. arizonicus easy to identify. Immature specimens, or those mounted so that the spike appears erect, are easily mistaken for Pseudoroegneria spicata (p. 281), but they have thicker culms and longer ligules, more basal leaves, and wider leaf blades.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.;Ariz.;N.Mex.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>