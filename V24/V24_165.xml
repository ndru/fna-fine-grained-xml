<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">129</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">STIPEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">ACHNATHERUM</taxon_name>
    <taxon_name authority="(Vasey) Barkworth" date="unknown" rank="species">parishii</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">parishii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe stipeae;genus achnatherum;species parishii;subspecies parishii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stipa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">coronata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">parishii</taxon_name>
    <taxon_hierarchy>genus stipa;species coronata;subspecies parishii</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <other_name type="common_name">Parish's needlegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 20-80 cm tall, 1.5-2 mm thick, mostly glabrous, pubescent below the nodes.</text>
      <biological_entity id="o8208" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="height" src="d0_s0" to="80" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="thickness" src="d0_s0" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character constraint="below nodes" constraintid="o8209" is_modifier="false" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o8209" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Basal sheath margins hairy distaily.</text>
      <biological_entity constraint="sheath" id="o8210" name="margin" name_original="margins" src="d0_s1" type="structure" constraint_original="basal sheath">
        <character is_modifier="false" modifier="distaily" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hairs 1-3.2 mm;</text>
      <biological_entity id="o8211" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="3.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 11-30+ cm long, 2.5-4.2 mm wide, usually flat or only partly closed, sometimes completely convolute, straight to somewhat arcuate distaily.</text>
      <biological_entity id="o8212" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="11" from_unit="cm" name="length" src="d0_s3" to="30" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s3" to="4.2" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="only partly; only partly" name="condition" src="d0_s3" value="closed" value_original="closed" />
        <character is_modifier="false" modifier="sometimes completely" name="arrangement_or_shape" src="d0_s3" value="convolute" value_original="convolute" />
        <character is_modifier="false" name="course" src="d0_s3" value="straight to somewhat" value_original="straight to somewhat" />
        <character is_modifier="false" modifier="somewhat; distaily" name="course_or_shape" src="d0_s3" value="arcuate" value_original="arcuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Panicles 11-15 cm long, 2-4 cm wide.</text>
      <biological_entity id="o8213" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character char_type="range_value" from="11" from_unit="cm" name="length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Florets 5.5-6.5 mm;</text>
      <biological_entity id="o8214" name="floret" name_original="florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s5" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>awns 15-35 mm;</text>
      <biological_entity id="o8215" name="awn" name_original="awns" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s6" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>paleas sparsely hairy between the veins, hairs about 1/2 as long as the lemma hairs, apices usually rounded, occasionally somewhat pinched;</text>
      <biological_entity id="o8216" name="palea" name_original="paleas" src="d0_s7" type="structure">
        <character constraint="between veins" constraintid="o8217" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o8217" name="vein" name_original="veins" src="d0_s7" type="structure" />
      <biological_entity id="o8218" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character constraint="as-long-as lemma hairs" constraintid="o8219" name="quantity" src="d0_s7" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o8219" name="hair" name_original="hairs" src="d0_s7" type="structure" />
      <biological_entity id="o8220" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 3.5-4.5 mm, glabrous.</text>
      <biological_entity id="o8221" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s8" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Caryopses 5-6 mm, fusiform.</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = unknown.</text>
      <biological_entity id="o8222" name="caryopse" name_original="caryopses" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="fusiform" value_original="fusiform" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8223" name="chromosome" name_original="" src="d0_s10" type="structure" />
    </statement>
  </description>
  <discussion>Achnatherum parishii subsp. parishii grows on dry, rocky slopes, in desert shrub and pinyon-juniper associations, from the coastal ranges of California to northeastern Nevada, eastern Utah, and the Grand Canyon in Arizona. Its range extends into Baja California, Mexico. It differs from A. coronatum in its shorter culms and once-geniculate awns, and from subsp. depauperatum in its longer culms, hairy sheath margins, and sparsely hairy paleas.</discussion>
  
</bio:treatment>