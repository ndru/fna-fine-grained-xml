<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">81</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Endl." date="unknown" rank="tribe">MELICEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="genus">GLYCERIA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Glyceria</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe meliceae;genus glyceria;section glyceria</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, rarely annual.</text>
      <biological_entity id="o23673" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="rarely" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Sheaths compressed, usually at least weakly keeled.</text>
      <biological_entity id="o23674" name="sheath" name_original="sheaths" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="usually at-least weakly" name="shape" src="d0_s1" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences 0.5-5 cm wide if the branches appressed, to 20 cm wide if divergent;</text>
      <biological_entity id="o23675" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="if branches" constraintid="o23676" from="0.5" from_unit="cm" name="width" src="d0_s2" to="5" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o23676" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" notes="" src="d0_s2" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branches usually appressed to ascending, divergent at anthesis;</text>
      <biological_entity id="o23677" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="usually appressed" name="orientation" src="d0_s3" to="ascending" />
        <character constraint="at anthesis" is_modifier="false" name="arrangement" src="d0_s3" value="divergent" value_original="divergent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>pedicels 0.5-5 mm.</text>
      <biological_entity id="o23678" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spikelets cylindrical and terete, except at anthesis when slightly laterally compressed, rectangular in side view, appressed to the panicle branches, lengths 5-22 times widths.</text>
      <biological_entity id="o23679" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="when slightly laterally compressed; in side view" name="shape" src="d0_s5" value="rectangular" value_original="rectangular" />
        <character constraint="to panicle branches" constraintid="o23680" is_modifier="false" name="fixation_or_orientation" src="d0_s5" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="l_w_ratio" notes="" src="d0_s5" value="5-22" value_original="5-22" />
      </biological_entity>
      <biological_entity constraint="panicle" id="o23680" name="branch" name_original="branches" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Lemmas rounded over the back, apices acute to rounded or truncate, entire to irregularly lobed;</text>
      <biological_entity id="o23681" name="lemma" name_original="lemmas" src="d0_s6" type="structure">
        <character constraint="over back" constraintid="o23682" is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o23682" name="back" name_original="back" src="d0_s6" type="structure" />
      <biological_entity id="o23683" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="truncate" value_original="truncate" />
        <character char_type="range_value" from="entire" name="shape" src="d0_s6" to="irregularly lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>paleas keeled, keels usually winged distally, tips parallel or almost so, sometimes extending into teeth, truncate, rounded, or notched between the keels;</text>
      <biological_entity id="o23684" name="palea" name_original="paleas" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o23685" name="keel" name_original="keels" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually; distally" name="architecture" src="d0_s7" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o23686" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="parallel" value_original="parallel" />
        <character name="arrangement" src="d0_s7" value="almost" value_original="almost" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s7" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="notched" value_original="notched" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character constraint="between keels" constraintid="o23688" is_modifier="false" name="shape" src="d0_s7" value="notched" value_original="notched" />
      </biological_entity>
      <biological_entity id="o23687" name="tooth" name_original="teeth" src="d0_s7" type="structure" />
      <biological_entity id="o23688" name="keel" name_original="keels" src="d0_s7" type="structure" />
      <relation from="o23686" id="r3760" modifier="sometimes" name="extending into" negation="false" src="d0_s7" to="o23687" />
    </statement>
    <statement id="d0_s8">
      <text>lodicules connate;</text>
      <biological_entity id="o23689" name="lodicule" name_original="lodicules" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers 3.</text>
      <biological_entity id="o23690" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Caryopses ovoid-oblong;</text>
      <biological_entity id="o23691" name="caryopse" name_original="caryopses" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovoid-oblong" value_original="ovoid-oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>hila about as long as the caryopses, linear.</text>
      <biological_entity id="o23692" name="hilum" name_original="hila" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" notes="" src="d0_s11" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o23693" name="caryopse" name_original="caryopses" src="d0_s11" type="structure" />
    </statement>
  </description>
  <discussion>Glyceria sect. Glyceria includes about 15 species. Seven species grow in the Flora region, three of which are introduced. In addition, there is one named hybrid. They grow in and beside shallow, still or slowly moving fresh water, such as along the edges of lakes and ponds and in low areas in wet meadows.</discussion>
  
</bio:treatment>