<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John W. Thieretf;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">742</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">CORYNEPHORUS</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus corynephorus</taxon_hierarchy>
  </taxon_identification>
  <number>14.55</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial.</text>
      <biological_entity id="o18883" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms to 60 cm, erect.</text>
      <biological_entity id="o18884" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths open;</text>
      <biological_entity id="o18885" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>auricles absent;</text>
      <biological_entity id="o18886" name="auricle" name_original="auricles" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules membranous;</text>
      <biological_entity id="o18887" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades flat or involute.</text>
      <biological_entity id="o18888" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s5" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal panicles.</text>
      <biological_entity id="o18889" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity constraint="terminal" id="o18890" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets pedicellate, laterally compressed, with 2 bisexual florets;</text>
      <biological_entity id="o18891" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s7" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o18892" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="true" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <relation from="o18891" id="r2984" name="with" negation="false" src="d0_s7" to="o18892" />
    </statement>
    <statement id="d0_s8">
      <text>rachillas prolonged beyond the base of the distal floret, pilose;</text>
      <biological_entity id="o18894" name="base" name_original="base" src="d0_s8" type="structure" constraint="floret" constraint_original="floret; floret" />
      <biological_entity constraint="distal" id="o18895" name="floret" name_original="floret" src="d0_s8" type="structure" />
      <relation from="o18894" id="r2985" name="part_of" negation="false" src="d0_s8" to="o18895" />
    </statement>
    <statement id="d0_s9">
      <text>disarticulation above the glumes, beneath the florets.</text>
      <biological_entity id="o18893" name="rachilla" name_original="rachillas" src="d0_s8" type="structure">
        <character constraint="beyond base" constraintid="o18894" is_modifier="false" name="length" src="d0_s8" value="prolonged" value_original="prolonged" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o18896" name="glume" name_original="glumes" src="d0_s9" type="structure" />
      <biological_entity id="o18897" name="floret" name_original="florets" src="d0_s9" type="structure" />
      <relation from="o18893" id="r2986" name="above" negation="false" src="d0_s9" to="o18896" />
    </statement>
    <statement id="d0_s10">
      <text>Glumes subequal, exceeding the florets, lanceolate, membranous, acute, unawned;</text>
      <biological_entity id="o18898" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="texture" src="d0_s10" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o18899" name="floret" name_original="florets" src="d0_s10" type="structure" />
      <relation from="o18898" id="r2987" name="exceeding the" negation="false" src="d0_s10" to="o18899" />
    </statement>
    <statement id="d0_s11">
      <text>lower glumes 1-veined;</text>
      <biological_entity constraint="lower" id="o18900" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes 3-veined at the base;</text>
      <biological_entity constraint="upper" id="o18901" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character constraint="at base" constraintid="o18902" is_modifier="false" name="architecture" src="d0_s12" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o18902" name="base" name_original="base" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>calluses pilose;</text>
      <biological_entity id="o18903" name="callus" name_original="calluses" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas about 1/2 as long as the glumes, ovatelanceolate, membranous, 1-veined, acute, awned from just above the base, awns geniculate, articulated near the middle, with a ring of minute, conical protuberances near the joint, proximal segment yellowbrown to dark-brown, smooth, thicker than the distal segment, distal segment pale green to whitish, clavate;</text>
      <biological_entity id="o18904" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character constraint="as-long-as glumes" constraintid="o18905" name="quantity" src="d0_s14" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="shape" notes="" src="d0_s14" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="texture" src="d0_s14" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
        <character constraint="just above base" constraintid="o18906" is_modifier="false" name="architecture_or_shape" src="d0_s14" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o18905" name="glume" name_original="glumes" src="d0_s14" type="structure" />
      <biological_entity id="o18906" name="base" name_original="base" src="d0_s14" type="structure" />
      <biological_entity id="o18907" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="geniculate" value_original="geniculate" />
        <character constraint="near middle segment" constraintid="o18908" is_modifier="false" name="architecture" src="d0_s14" value="articulated" value_original="articulated" />
      </biological_entity>
      <biological_entity constraint="middle" id="o18908" name="segment" name_original="segment" src="d0_s14" type="structure" />
      <biological_entity id="o18909" name="ring" name_original="ring" src="d0_s14" type="structure" />
      <biological_entity id="o18910" name="protuberance" name_original="protuberances" src="d0_s14" type="structure">
        <character is_modifier="true" name="size" src="d0_s14" value="minute" value_original="minute" />
        <character is_modifier="true" name="shape" src="d0_s14" value="conical" value_original="conical" />
      </biological_entity>
      <biological_entity id="o18911" name="joint" name_original="joint" src="d0_s14" type="structure" />
      <biological_entity constraint="proximal" id="o18912" name="segment" name_original="segment" src="d0_s14" type="structure">
        <character char_type="range_value" from="yellowbrown" name="coloration" src="d0_s14" to="dark-brown" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character constraint="than the distal segment" constraintid="o18913" is_modifier="false" name="width" src="d0_s14" value="thicker" value_original="thicker" />
      </biological_entity>
      <biological_entity constraint="distal" id="o18913" name="segment" name_original="segment" src="d0_s14" type="structure" />
      <biological_entity constraint="distal" id="o18914" name="segment" name_original="segment" src="d0_s14" type="structure">
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s14" to="whitish" />
        <character is_modifier="false" name="shape" src="d0_s14" value="clavate" value_original="clavate" />
      </biological_entity>
      <relation from="o18907" id="r2988" name="with" negation="false" src="d0_s14" to="o18909" />
      <relation from="o18909" id="r2989" name="part_of" negation="false" src="d0_s14" to="o18910" />
      <relation from="o18909" id="r2990" name="near" negation="false" src="d0_s14" to="o18911" />
    </statement>
    <statement id="d0_s15">
      <text>paleas shorter than the lemmas, membranous;</text>
      <biological_entity id="o18915" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character constraint="than the lemmas" constraintid="o18916" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="texture" src="d0_s15" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o18916" name="lemma" name_original="lemmas" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>lodicules 2, free, glabrous, toothed;</text>
      <biological_entity id="o18917" name="lodicule" name_original="lodicules" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="2" value_original="2" />
        <character is_modifier="false" name="fusion" src="d0_s16" value="free" value_original="free" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s16" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 3;</text>
      <biological_entity id="o18918" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovaries glabrous.</text>
      <biological_entity id="o18919" name="ovary" name_original="ovaries" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Caryopses shorter than the lemmas, concealed at maturity, usually adhering to the lemmas and/or paleas, longitudinally grooved.</text>
      <biological_entity id="o18921" name="lemma" name_original="lemmas" src="d0_s19" type="structure" />
      <biological_entity id="o18922" name="lemma" name_original="lemmas" src="d0_s19" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s19" value="maturity" value_original="maturity" />
      </biological_entity>
      <biological_entity id="o18923" name="palea" name_original="paleas" src="d0_s19" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s19" value="maturity" value_original="maturity" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>x = 7.</text>
      <biological_entity id="o18920" name="caryopse" name_original="caryopses" src="d0_s19" type="structure">
        <character constraint="than the lemmas" constraintid="o18921" is_modifier="false" name="height_or_length_or_size" src="d0_s19" value="shorter" value_original="shorter" />
        <character constraint="at lemmas, paleas" constraintid="o18922, o18923" is_modifier="false" name="prominence" src="d0_s19" value="concealed" value_original="concealed" />
        <character is_modifier="false" modifier="longitudinally" name="architecture" notes="" src="d0_s19" value="grooved" value_original="grooved" />
      </biological_entity>
      <biological_entity constraint="x" id="o18924" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Corynephorus is a Eurasian and North African genus of five species, one of which has been introduced into the Flora region. Flowering specimens are easily recognized by their distinctive awns, but sterile plants, with their involute, glaucous leaves, resemble involute-leaved species of Festuca, such as F. trachyphylla.</discussion>
  <references>
    <reference>Albers, F. 1973. Cytosystematische Untersuchungen in der Subtribus Descbampsiineae Holub (Tribus Aveneae Nees): I. Zwei Arten der Gattung Corynephorus P.B. Preslia 45:11-18</reference>
    <reference>Douglas, G.W., G.B. Straley, and D.V. Meidinger (eds) 1994. The Vascular Plants of British Columbia: Part 4-Monocotyledons. Research Branch, Ministry of Forests, Victoria, British Columbia, Canada. 257 pp.</reference>
    <reference>Jirasek, V. and J. Chrtek. 1962. Systematische Studie über die Arten der Gattung Corynephorus Pal.-Beauv. (Poaceae). Preslia 34:374-386.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Maine;N.H.;N.J.;Mass.;Wash.;B.C.;Mich.;N.Y.;Pa.;Conn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>