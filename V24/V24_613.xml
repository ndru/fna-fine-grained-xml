<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">432</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">FESTUCA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Festuca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Festuca</taxon_name>
    <taxon_name authority="Rydb." date="unknown" rank="species">saximontana</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">saximontana</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus festuca;subgenus festuca;section festuca;species saximontana;variety saximontana</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 25-50 (60) cm, usually 3-5 times the height of the vegetative shoot leaves, usually glabrous below the inflorescences, sometimes sparsely scabrous or pubescent;</text>
      <biological_entity id="o21302" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character name="atypical_some_measurement" src="d0_s0" unit="cm" value="60" value_original="60" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character is_modifier="false" name="height" src="d0_s0" value="3-5 times the height of" />
        <character constraint="below inflorescences" constraintid="o21304" is_modifier="false" modifier="usually" name="pubescence" notes="" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s0" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="shoot" id="o21303" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s0" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <biological_entity id="o21304" name="inflorescence" name_original="inflorescences" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>blades usually scabrous on the abaxial surfaces, scabrules to 0.1 mm;</text>
      <biological_entity id="o21305" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character constraint="on abaxial surfaces" constraintid="o21306" is_modifier="false" modifier="usually" name="pubescence_or_relief" src="d0_s1" value="scabrous" value_original="scabrous" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" notes="" src="d0_s1" to="0.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o21306" name="surface" name_original="surfaces" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>abaxial sclerenchyma in 3-5 strands, sometimes partly confluent or forming a continuous band.</text>
      <biological_entity constraint="abaxial" id="o21307" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes partly" name="arrangement" notes="" src="d0_s2" value="confluent" value_original="confluent" />
        <character name="arrangement" src="d0_s2" value="forming a continuous band" value_original="forming a continuous band" />
      </biological_entity>
      <biological_entity id="o21308" name="strand" name_original="strands" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s2" to="5" />
      </biological_entity>
      <relation from="o21307" id="r3371" name="in" negation="false" src="d0_s2" to="o21308" />
    </statement>
    <statement id="d0_s3">
      <text>Lemmas smooth or scabrous distally.</text>
      <biological_entity id="o21309" name="lemma" name_original="lemmas" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="distally" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Festuca saximontana var. saximontana grows throughout the range of the species.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Wis.;Alta.;B.C.;Greenland;Man.;Nfld. and Labr.;N.W.T.;Ont.;Que.;Sask.;Yukon;N.Mex.;Wash.;N.Y.;Utah;Calif.;Mich.;Ariz.;Colo.;Idaho;Nev.;Oreg.;Vt.;Mont.;Alaska;Iowa;Kans.;Minn.;N.Dak.;Nebr.;S.Dak.;Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>