<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">679</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Scop." date="unknown" rank="genus">SESLERIA</taxon_name>
    <taxon_name authority="(Scop.) F.W. Schultz" date="unknown" rank="species">autumnalis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus sesleria;species autumnalis</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Autumn moorgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, rhizomatous.</text>
      <biological_entity id="o30079" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 25-70 cm, erect, glabrous;</text>
      <biological_entity id="o30080" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s1" to="70" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes 4-5.</text>
      <biological_entity id="o30081" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s2" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves distichous, keeled, folded;</text>
      <biological_entity constraint="basal" id="o30082" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="distichous" value_original="distichous" />
        <character is_modifier="false" name="shape" src="d0_s3" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="folded" value_original="folded" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths glabrous, lower sheaths scabrous, eventually disintegrating into wavy fibers, upper sheaths smooth;</text>
      <biological_entity id="o30083" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="lower" id="o30084" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
        <character constraint="into fibers" constraintid="o30085" is_modifier="false" modifier="eventually" name="dehiscence" src="d0_s4" value="disintegrating" value_original="disintegrating" />
      </biological_entity>
      <biological_entity id="o30085" name="fiber" name_original="fibers" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity constraint="upper" id="o30086" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules of lower leaves to 1 mm, shortly ciliate;</text>
      <biological_entity id="o30087" name="ligule" name_original="ligules" src="d0_s5" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character is_modifier="false" modifier="to 1 mm; shortly" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="lower" id="o30088" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o30087" id="r4740" name="part_of" negation="false" src="d0_s5" to="o30088" />
    </statement>
    <statement id="d0_s6">
      <text>blades 25-35 (40) cm long, 2-4 mm wide;</text>
      <biological_entity id="o30089" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character name="length" src="d0_s6" unit="cm" value="40" value_original="40" />
        <character char_type="range_value" from="25" from_unit="cm" name="length" src="d0_s6" to="35" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades of cauline leaves usually flat, rarely dolded;</text>
      <biological_entity id="o30090" name="blade" name_original="blades" src="d0_s7" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character is_modifier="false" modifier="usually" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o30091" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o30090" id="r4741" name="part_of" negation="false" src="d0_s7" to="o30091" />
    </statement>
    <statement id="d0_s8">
      <text>blades of flag leaves 4-8 (12) cm, diverging at about midculm.</text>
      <biological_entity id="o30092" name="blade" name_original="blades" src="d0_s8" type="structure" constraint="flag; flag">
        <character name="atypical_some_measurement" src="d0_s8" unit="cm" value="12" value_original="12" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s8" to="8" to_unit="cm" />
        <character constraint="at midculm" constraintid="o30095" is_modifier="false" name="orientation" src="d0_s8" value="diverging" value_original="diverging" />
      </biological_entity>
      <biological_entity id="o30093" name="flag" name_original="flag" src="d0_s8" type="structure" />
      <biological_entity id="o30094" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o30095" name="midculm" name_original="midculm" src="d0_s8" type="structure" />
      <relation from="o30092" id="r4742" name="part_of" negation="false" src="d0_s8" to="o30093" />
      <relation from="o30092" id="r4743" name="part_of" negation="false" src="d0_s8" to="o30094" />
    </statement>
    <statement id="d0_s9">
      <text>Panicles 4.5-10 cm long, 4-7 mm wide, cylindrical, lax, grayish to whitish, branches to 1 cm;</text>
      <biological_entity id="o30096" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character char_type="range_value" from="4.5" from_unit="cm" name="length" src="d0_s9" to="10" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="lax" value_original="lax" />
        <character char_type="range_value" from="grayish" name="coloration" src="d0_s9" to="whitish" />
      </biological_entity>
      <biological_entity id="o30097" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pedicels with 1-2 hyaline, scalelike bracts at the base.</text>
      <biological_entity id="o30098" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <biological_entity id="o30099" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s10" to="2" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="hyaline" value_original="hyaline" />
        <character is_modifier="true" name="shape" src="d0_s10" value="scale-like" value_original="scalelike" />
      </biological_entity>
      <biological_entity id="o30100" name="base" name_original="base" src="d0_s10" type="structure" />
      <relation from="o30098" id="r4744" name="with" negation="false" src="d0_s10" to="o30099" />
      <relation from="o30099" id="r4745" name="at" negation="false" src="d0_s10" to="o30100" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 5-7 mm long, wedge-shaped, with 2 (3) florets.</text>
      <biological_entity id="o30101" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s11" to="7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="wedge--shaped" value_original="wedge--shaped" />
      </biological_entity>
      <biological_entity id="o30102" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
      <relation from="o30101" id="r4746" name="with" negation="false" src="d0_s11" to="o30102" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes subequal, 4-5 mm, usually extending above the florets, narrowly lanceolate, hyaline, glabrous, 1-veined, acute, midveins extending into 1-2 (4) mm awns;</text>
      <biological_entity id="o30103" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="usually; narrowly" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o30104" name="floret" name_original="florets" src="d0_s12" type="structure" />
      <biological_entity id="o30105" name="midvein" name_original="midveins" src="d0_s12" type="structure" />
      <biological_entity id="o30106" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character is_modifier="true" name="atypical_some_measurement" src="d0_s12" unit="mm" value="4" value_original="4" />
        <character char_type="range_value" from="1" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o30103" id="r4747" modifier="usually" name="extending above the" negation="false" src="d0_s12" to="o30104" />
      <relation from="o30105" id="r4748" name="extending into" negation="false" src="d0_s12" to="o30106" />
    </statement>
    <statement id="d0_s13">
      <text>lemmas 3-5.5 mm, 3 (7) -veined, broadly lanceolate to ovate, hyaline, bases shortly and finely pubescent on the veins and margins, glabrous between the veins, midveins forming (0.5) 1-1.5 mm teeth, marginal veins forming 0.5-1 mm teeth;</text>
      <biological_entity id="o30107" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="5.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="3(7)-veined" value_original="3(7)-veined" />
        <character char_type="range_value" from="broadly lanceolate" name="shape" src="d0_s13" to="ovate" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o30108" name="base" name_original="bases" src="d0_s13" type="structure">
        <character constraint="on margins" constraintid="o30110" is_modifier="false" modifier="finely" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
        <character constraint="between veins" constraintid="o30111" is_modifier="false" name="pubescence" notes="" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o30109" name="vein" name_original="veins" src="d0_s13" type="structure" />
      <biological_entity id="o30110" name="margin" name_original="margins" src="d0_s13" type="structure" />
      <biological_entity id="o30111" name="vein" name_original="veins" src="d0_s13" type="structure" />
      <biological_entity id="o30112" name="midvein" name_original="midveins" src="d0_s13" type="structure" />
      <biological_entity id="o30113" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character is_modifier="true" name="atypical_some_measurement" src="d0_s13" unit="mm" value="0.5" value_original="0.5" />
        <character char_type="range_value" from="1" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s13" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o30114" name="vein" name_original="veins" src="d0_s13" type="structure" />
      <biological_entity id="o30115" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o30112" id="r4749" name="forming" negation="false" src="d0_s13" to="o30113" />
      <relation from="o30114" id="r4750" name="forming" negation="false" src="d0_s13" to="o30115" />
    </statement>
    <statement id="d0_s14">
      <text>paleas 4-5 mm, finely ciliate on the keels, apices rounded, notched, veins forming short awn tips;</text>
      <biological_entity id="o30116" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
        <character constraint="on keels" constraintid="o30117" is_modifier="false" modifier="finely" name="architecture_or_pubescence_or_shape" src="d0_s14" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o30117" name="keel" name_original="keels" src="d0_s14" type="structure" />
      <biological_entity id="o30118" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s14" value="notched" value_original="notched" />
      </biological_entity>
      <biological_entity id="o30119" name="vein" name_original="veins" src="d0_s14" type="structure" />
      <biological_entity constraint="awn" id="o30120" name="tip" name_original="tips" src="d0_s14" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s14" value="short" value_original="short" />
      </biological_entity>
      <relation from="o30119" id="r4751" name="forming" negation="false" src="d0_s14" to="o30120" />
    </statement>
    <statement id="d0_s15">
      <text>anthers 2.4-2.8 mm, white.</text>
      <biological_entity id="o30121" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s15" to="2.8" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Caryopses 2-3 mm, elliptical in outline, shortly and stiffly pubescent distally.</text>
      <biological_entity id="o30123" name="outline" name_original="outline" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>2n = 28.</text>
      <biological_entity id="o30122" name="caryopse" name_original="caryopses" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
        <character constraint="in outline" constraintid="o30123" is_modifier="false" name="arrangement_or_shape" src="d0_s16" value="elliptical" value_original="elliptical" />
        <character is_modifier="false" modifier="shortly; stiffly; distally" name="pubescence" notes="" src="d0_s16" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30124" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sesleria autumnalis is native from northern and eastern Italy to central Albania, where it grows primarily on limestone-derived soils in deciduous, often littoral forests, and is frequently co-dominant with Ostrya virginiana. It sometimes forms intermediates with S. nitida (Deyl 1980). It is now popular in the Flora region as an ornamental, being an attractive, low-maintenance plant that provides excellent ground cover. It can be grown in open areas, and is drought- and shade-tolerant once established.</discussion>
  
</bio:treatment>