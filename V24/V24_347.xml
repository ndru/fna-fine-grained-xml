<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">245</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">HORDEUM</taxon_name>
    <taxon_name authority="Nevski" date="unknown" rank="species">brachyantherum</taxon_name>
    <taxon_name authority="(Covas &amp; Stebbins) Bothmer, N. Jacobsen &amp; Seberg" date="unknown" rank="subspecies">californicum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus hordeum;species brachyantherum;subspecies californicum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hordeum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">californicum</taxon_name>
    <taxon_hierarchy>genus hordeum;species californicum</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <other_name type="common_name">California barley</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants loosely cespitose.</text>
      <biological_entity id="o26647" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="loosely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 20-65 cm, usually slender.</text>
      <biological_entity id="o26648" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="65" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal sheaths usually densely pubescent;</text>
      <biological_entity constraint="basal" id="o26649" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually densely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades to 11.5 cm long, to 3.5 (5.5) mm wide, usually hairy with spreading hairs of mixed lengths on both sides, rarely glabrous or almost glabrous.</text>
      <biological_entity id="o26650" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s3" to="11.5" to_unit="cm" />
        <character name="width" src="d0_s3" unit="mm" value="5.5" value_original="5.5" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s3" to="3.5" to_unit="mm" />
        <character constraint="with hairs" constraintid="o26651" is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="rarely" name="pubescence" notes="" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="almost; almost" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o26651" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o26652" name="side" name_original="sides" src="d0_s3" type="structure" />
      <relation from="o26651" id="r4212" modifier="of mixed lengths" name="on" negation="false" src="d0_s3" to="o26652" />
    </statement>
    <statement id="d0_s4">
      <text>Glumes 9-19 mm, usually spreading at maturity;</text>
      <biological_entity id="o26653" name="glume" name_original="glumes" src="d0_s4" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s4" to="19" to_unit="mm" />
        <character constraint="at maturity" is_modifier="false" modifier="usually" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lemmas usually awned, awns to 7.5 mm, usually divergent at maturity;</text>
      <biological_entity id="o26654" name="lemma" name_original="lemmas" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o26655" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="7.5" to_unit="mm" />
        <character constraint="at maturity" is_modifier="false" modifier="usually" name="arrangement" src="d0_s5" value="divergent" value_original="divergent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>anthers 1.1-4 mm. 2n = 14.</text>
      <biological_entity id="o26656" name="anther" name_original="anthers" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26657" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hordeum brachyantherum subsp. californicum is restricted to California. It grows on dry and moist grass slopes, in meadows and rocky stream beds, along stream margins, and around vernal pools, in oak woodlands and disturbed ground, and in serpentine, alkaline, and granitic soils, up to 2300 m. Records from outside California, and many from inside California, are based on misidentified specimens, usually of H. brachyantherum subsp. brachyantherum.</discussion>
  
</bio:treatment>