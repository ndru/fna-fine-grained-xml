<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">220</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">BROMEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">BROMUS</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="section">Bromopsis</taxon_name>
    <taxon_name authority="(Shear) Rydb." date="unknown" rank="species">lanatipes</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe bromeae;genus bromus;section bromopsis;species lanatipes</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bromus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">anomalus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">lanatipes</taxon_name>
    <taxon_hierarchy>genus bromus;species anomalus;variety lanatipes</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bromopsis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">lanatipes</taxon_name>
    <taxon_hierarchy>genus bromopsis;species lanatipes</taxon_hierarchy>
  </taxon_identification>
  <number>29</number>
  <other_name type="common_name">Wooly brome</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>not rhizomatous.</text>
      <biological_entity id="o5272" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 40-90 cm, erect;</text>
      <biological_entity id="o5273" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s2" to="90" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes 3-5 (7), mainly pubescent;</text>
      <biological_entity id="o5274" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character name="atypical_quantity" src="d0_s3" value="7" value_original="7" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="5" />
        <character is_modifier="false" modifier="mainly" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>internodes mostly glabrous, puberulent near the nodes.</text>
      <biological_entity id="o5275" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character constraint="near nodes" constraintid="o5276" is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o5276" name="node" name_original="nodes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Basal sheaths densely pilose or glabrous;</text>
      <biological_entity constraint="basal" id="o5277" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>upper sheaths glabrous or almost so, midrib of the culm leaves not abruptly narrowed just below the collar;</text>
      <biological_entity constraint="upper" id="o5278" name="sheath" name_original="sheaths" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s6" value="almost" value_original="almost" />
      </biological_entity>
      <biological_entity id="o5279" name="midrib" name_original="midrib" src="d0_s6" type="structure">
        <character constraint="just below collar" constraintid="o5281" is_modifier="false" modifier="not abruptly" name="shape" src="d0_s6" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity constraint="culm" id="o5280" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o5281" name="collar" name_original="collar" src="d0_s6" type="structure" />
      <relation from="o5279" id="r854" name="part_of" negation="false" src="d0_s6" to="o5280" />
    </statement>
    <statement id="d0_s7">
      <text>auricles absent;</text>
      <biological_entity id="o5282" name="auricle" name_original="auricles" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>ligules 1-2 mm, glabrous, truncate or obtuse, sometimes lacerate;</text>
      <biological_entity id="o5283" name="ligule" name_original="ligules" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s8" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s8" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>blades 5-20 cm long, to 7 mm wide, flat, both surfaces glabrous, sometimes scabrous.</text>
      <biological_entity id="o5284" name="blade" name_original="blades" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s9" to="20" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o5285" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Panicles 10-25 cm, open, nodding;</text>
      <biological_entity id="o5286" name="panicle" name_original="panicles" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s10" to="25" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="open" value_original="open" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>branches ascending to spreading.</text>
      <biological_entity id="o5287" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s11" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 10-30 mm, elliptic to lanceolate, terete to moderately laterally compressed, with 7-12 (16) florets.</text>
      <biological_entity id="o5288" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s12" to="30" to_unit="mm" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s12" to="lanceolate terete" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s12" to="lanceolate terete" />
      </biological_entity>
      <biological_entity id="o5289" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s12" value="16" value_original="16" />
        <character char_type="range_value" from="7" is_modifier="true" name="quantity" src="d0_s12" to="12" />
      </biological_entity>
      <relation from="o5288" id="r855" name="with" negation="false" src="d0_s12" to="o5289" />
    </statement>
    <statement id="d0_s13">
      <text>Glumes usually glabrous, sometimes pubescent;</text>
      <biological_entity id="o5290" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower glumes 5-6.5 (7) mm, 1 (3) -veined;</text>
      <biological_entity constraint="lower" id="o5291" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="7" value_original="7" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="6.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="1(3)-veined" value_original="1(3)-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper glumes (6) 7-9 mm, 3-veined, not mucronate;</text>
      <biological_entity constraint="upper" id="o5292" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="6" value_original="6" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s15" to="9" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s15" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lemmas 8-11 mm, elliptic, rounded over the midvein, backs and margins pubescent, sometimes nearly glabrous, apices truncate or obtuse, entire, rarely emarginate, lobes shorter than 1 mm;</text>
      <biological_entity id="o5293" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s16" to="11" to_unit="mm" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s16" value="elliptic" value_original="elliptic" />
        <character constraint="over margins" constraintid="o5296" is_modifier="false" name="shape" src="d0_s16" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="sometimes nearly" name="pubescence" notes="" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o5294" name="midvein" name_original="midvein" src="d0_s16" type="structure" />
      <biological_entity id="o5295" name="back" name_original="backs" src="d0_s16" type="structure" />
      <biological_entity id="o5296" name="margin" name_original="margins" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o5297" name="apex" name_original="apices" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s16" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity id="o5298" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s16" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>awns 2-4 mm, straight, arising less than 1.5 mm below the lemma apices;</text>
      <biological_entity id="o5299" name="awn" name_original="awns" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s17" to="4" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s17" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="arising" value_original="arising" />
        <character char_type="range_value" constraint="below lemma apices" constraintid="o5300" from="0" from_unit="mm" name="some_measurement" src="d0_s17" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o5300" name="apex" name_original="apices" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>anthers 1.8-4 mm. 2n = 28.</text>
      <biological_entity id="o5301" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s18" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5302" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bromus lanatipes grows in a wide range of habitats at 800-2500 m, from Wyoming through the southwestern United States to northern Mexico.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Okla.;Ariz.;Colo.;N.Mex.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>