<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">430</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">FESTUCA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Festuca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Festuca</taxon_name>
    <taxon_name authority="Rydb." date="unknown" rank="species">saximontana</taxon_name>
    <taxon_name authority="(St.-Yves) Fred. &amp; Pavlick" date="unknown" rank="variety">purpusiana</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus festuca;subgenus festuca;section festuca;species saximontana;variety purpusiana</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms (5) 8-20 (25) cm, usually 2-3 times the height of the vegetative shoot leaves, usually glabrous below the inflorescence.</text>
      <biological_entity id="o21953" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character name="atypical_some_measurement" src="d0_s0" unit="cm" value="5" value_original="5" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character is_modifier="false" name="height" src="d0_s0" value="2-3 times the height of" />
        <character constraint="below inflorescence" constraintid="o21955" is_modifier="false" modifier="usually" name="pubescence" notes="" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="shoot" id="o21954" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s0" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <biological_entity id="o21955" name="inflorescence" name_original="inflorescence" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Outer vegetative shoot sheaths mostly stramineous;</text>
      <biological_entity constraint="shoot" id="o21956" name="sheath" name_original="sheaths" src="d0_s1" type="structure" constraint_original="outer shoot">
        <character is_modifier="true" name="reproduction" src="d0_s1" value="vegetative" value_original="vegetative" />
        <character is_modifier="false" modifier="mostly" name="coloration" src="d0_s1" value="stramineous" value_original="stramineous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades smooth or scabrous on the abaxial surfaces, ribs on the adaxial surfaces with hairs shorter than 0.06 mm;</text>
      <biological_entity id="o21957" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character constraint="on abaxial surfaces" constraintid="o21958" is_modifier="false" name="pubescence_or_relief" src="d0_s2" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o21958" name="surface" name_original="surfaces" src="d0_s2" type="structure" />
      <biological_entity id="o21959" name="rib" name_original="ribs" src="d0_s2" type="structure" />
      <biological_entity constraint="adaxial" id="o21960" name="surface" name_original="surfaces" src="d0_s2" type="structure" />
      <biological_entity id="o21961" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s2" unit="mm" value="0.06" value_original="0.06" />
      </biological_entity>
      <relation from="o21959" id="r3469" name="on" negation="false" src="d0_s2" to="o21960" />
      <relation from="o21960" id="r3470" name="with" negation="false" src="d0_s2" to="o21961" />
    </statement>
    <statement id="d0_s3">
      <text>abaxial sclerenchyma in 5-7 narrow abaxial strands.</text>
      <biological_entity constraint="abaxial" id="o21962" name="strand" name_original="strands" src="d0_s3" type="structure" />
      <biological_entity constraint="abaxial" id="o21963" name="strand" name_original="strands" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s3" to="7" />
        <character is_modifier="true" name="size_or_width" src="d0_s3" value="narrow" value_original="narrow" />
      </biological_entity>
      <relation from="o21962" id="r3471" name="in" negation="false" src="d0_s3" to="o21963" />
    </statement>
    <statement id="d0_s4">
      <text>Lemmas usually scabrous towards the apices and often along the margins.</text>
      <biological_entity id="o21964" name="lemma" name_original="lemmas" src="d0_s4" type="structure">
        <character constraint="towards apices" constraintid="o21965" is_modifier="false" modifier="usually" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o21965" name="apex" name_original="apices" src="d0_s4" type="structure" />
      <biological_entity id="o21966" name="margin" name_original="margins" src="d0_s4" type="structure" />
      <relation from="o21964" id="r3472" modifier="often" name="along" negation="false" src="d0_s4" to="o21966" />
    </statement>
  </description>
  <discussion>Festuca saximontana var. purpusiana grows in subalpine or lower alpine habitats. The distribution of this taxon is poorly known; it probably extends from Alaska south to northern California. It is also reported from the Chukchi Peninsula in eastern Russia (Tzvelev 1976).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Wash.;Utah;Alaska;Colo.;Nev.;Oreg.;Calif.;Alta.;B.C.;N.W.T.;Yukon</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>