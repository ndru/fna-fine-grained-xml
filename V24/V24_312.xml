<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">222</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">BROMEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">BROMUS</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="section">Bromopsis</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="species">richardsonii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe bromeae;genus bromus;section bromopsis;species richardsonii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bromposis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">richardsonii</taxon_name>
    <taxon_hierarchy>genus bromposis;species richardsonii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bromopsis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">canadensis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">ricbardsonii</taxon_name>
    <taxon_hierarchy>genus bromopsis;species canadensis;subspecies ricbardsonii</taxon_hierarchy>
  </taxon_identification>
  <number>32</number>
  <other_name type="common_name">Richardson's brome</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>not rhizomatous.</text>
      <biological_entity id="o25513" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 50-110 (145) cm, erect to spreading;</text>
      <biological_entity id="o25514" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="145" value_original="145" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s2" to="110" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes (3) 4-5 (6), usually glabrous, sometimes pubescent;</text>
      <biological_entity id="o25515" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character name="atypical_quantity" src="d0_s3" value="3" value_original="3" />
        <character name="atypical_quantity" src="d0_s3" value="6" value_original="6" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s3" to="5" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>internodes usually glabrous.</text>
      <biological_entity id="o25516" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Basal sheaths often retrorsely pilose;</text>
      <biological_entity constraint="basal" id="o25517" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often retrorsely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>culm sheaths glabrous, often tufted-pilose near the auricle position, midrib of the culm leaves not abruptly narrowed just below the collar;</text>
      <biological_entity constraint="culm" id="o25518" name="sheath" name_original="sheaths" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="often" name="position" src="d0_s6" value="tufted-pilose" value_original="tufted-pilose" />
      </biological_entity>
      <biological_entity id="o25519" name="midrib" name_original="midrib" src="d0_s6" type="structure">
        <character constraint="just below collar" constraintid="o25521" is_modifier="false" modifier="not abruptly" name="shape" src="d0_s6" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity constraint="culm" id="o25520" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o25521" name="collar" name_original="collar" src="d0_s6" type="structure" />
      <relation from="o25519" id="r4033" name="part_of" negation="false" src="d0_s6" to="o25520" />
    </statement>
    <statement id="d0_s7">
      <text>auricles absent;</text>
      <biological_entity id="o25522" name="auricle" name_original="auricles" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>ligules 0.4-2 mm, glabrous, rounded, erose, ciliolate;</text>
      <biological_entity id="o25523" name="ligule" name_original="ligules" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s8" value="erose" value_original="erose" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>blades 10-35 cm long, 3-12 mm wide, flat, glabrous.</text>
      <biological_entity id="o25524" name="blade" name_original="blades" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s9" to="35" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s9" to="12" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Panicles 10-20 (25) cm, open, nodding;</text>
      <biological_entity id="o25525" name="panicle" name_original="panicles" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="cm" value="25" value_original="25" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s10" to="20" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="open" value_original="open" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>branches ascending to spreading or drooping, filiform.</text>
      <biological_entity id="o25526" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s11" to="spreading or drooping" />
        <character is_modifier="false" name="shape" src="d0_s11" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 15-25 (40) mm, elliptic to lanceolate, terete to moderately laterally compressed, with (4) 6-10 (15) florets.</text>
      <biological_entity id="o25527" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="mm" value="40" value_original="40" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s12" to="25" to_unit="mm" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s12" to="lanceolate terete" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s12" to="lanceolate terete" />
      </biological_entity>
      <biological_entity id="o25528" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s12" value="4" value_original="4" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s12" value="15" value_original="15" />
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s12" to="10" />
      </biological_entity>
      <relation from="o25527" id="r4034" name="with" negation="false" src="d0_s12" to="o25528" />
    </statement>
    <statement id="d0_s13">
      <text>Glumes usually glabrous, sometimes pubescent;</text>
      <biological_entity id="o25529" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower glumes 7.5-12.5 mm, 1 (3) -veined;</text>
      <biological_entity constraint="lower" id="o25530" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" src="d0_s14" to="12.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="1(3)-veined" value_original="1(3)-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper glumes 8.9-11.3 mm, 3-veined, often mucronate;</text>
      <biological_entity constraint="upper" id="o25531" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character char_type="range_value" from="8.9" from_unit="mm" name="some_measurement" src="d0_s15" to="11.3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s15" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lemmas 9-14 (16) mm, elliptic, rounded over the midvein, margins more or less densely pilose on the lower 1/2 or 3/4, lower lemmas in a spikelet glabrous across the backs, uppermost lemmas with appressed hairs on the backs, apices obtuse, entire;</text>
      <biological_entity id="o25532" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character name="atypical_some_measurement" src="d0_s16" unit="mm" value="16" value_original="16" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s16" to="14" to_unit="mm" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s16" value="elliptic" value_original="elliptic" />
        <character constraint="over midvein" constraintid="o25533" is_modifier="false" name="shape" src="d0_s16" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o25533" name="midvein" name_original="midvein" src="d0_s16" type="structure" />
      <biological_entity id="o25534" name="margin" name_original="margins" src="d0_s16" type="structure">
        <character constraint="on " constraintid="o25536" is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s16" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="lower" id="o25535" name="1/2" name_original="1/2" src="d0_s16" type="structure" />
      <biological_entity constraint="lower" id="o25536" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character constraint="on " constraintid="o25538" name="quantity" src="d0_s16" value="3/4" value_original="3/4" />
      </biological_entity>
      <biological_entity constraint="lower" id="o25537" name="1/2" name_original="1/2" src="d0_s16" type="structure" />
      <biological_entity constraint="lower" id="o25538" name="lemma" name_original="lemmas" src="d0_s16" type="structure" />
      <biological_entity id="o25539" name="spikelet" name_original="spikelet" src="d0_s16" type="structure">
        <character constraint="across backs" constraintid="o25540" is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o25540" name="back" name_original="backs" src="d0_s16" type="structure" />
      <biological_entity constraint="uppermost" id="o25541" name="lemma" name_original="lemmas" src="d0_s16" type="structure" />
      <biological_entity id="o25542" name="hair" name_original="hairs" src="d0_s16" type="structure">
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s16" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o25543" name="back" name_original="backs" src="d0_s16" type="structure" />
      <biological_entity id="o25544" name="apex" name_original="apices" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o25538" id="r4035" name="in" negation="false" src="d0_s16" to="o25539" />
      <relation from="o25541" id="r4036" name="with" negation="false" src="d0_s16" to="o25542" />
      <relation from="o25542" id="r4037" name="on" negation="false" src="d0_s16" to="o25543" />
    </statement>
    <statement id="d0_s17">
      <text>awns (2) 3-5 mm, straight, arising less than 1.5 mm below the lemma apices;</text>
      <biological_entity id="o25545" name="awn" name_original="awns" src="d0_s17" type="structure">
        <character name="atypical_some_measurement" src="d0_s17" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s17" to="5" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s17" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="arising" value_original="arising" />
        <character char_type="range_value" constraint="below lemma apices" constraintid="o25546" from="0" from_unit="mm" name="some_measurement" src="d0_s17" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o25546" name="apex" name_original="apices" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>anthers 1.6-2.7 mm. 2n = 28.</text>
      <biological_entity id="o25547" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s18" to="2.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25548" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bromus richardsonii grows in meadows and open woods in the upper montane and subalpine zones, at 2000-4000 m in the southern Rocky Mountains, and at lower elevations northwards. Its range extends from southern Alaska to southern California and northern Baja California, Mexico; it is found as far east as Saskatchewan, South Dakota, and western Texas. Specimens with pubescent nodes and glumes are apparently confined to the southwestern United States.</discussion>
  
</bio:treatment>