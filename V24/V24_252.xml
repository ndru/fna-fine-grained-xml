<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">184</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">STIPEAE</taxon_name>
    <taxon_name authority="Arriaga &amp; Barkworth" date="unknown" rank="genus">AMELICHLOA</taxon_name>
    <taxon_name authority="(Hack.) Arriaga &amp; Barkworth" date="unknown" rank="species">clandestina</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe stipeae;genus amelichloa;species clandestina</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stipa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">clandestina</taxon_name>
    <taxon_hierarchy>genus stipa;species clandestina</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Achnatherum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">clandestinum</taxon_name>
    <taxon_hierarchy>genus achnatherum;species clandestinum</taxon_hierarchy>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Mexican needlegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, with knotty, rhizomatous bases.</text>
      <biological_entity id="o27932" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o27933" name="base" name_original="bases" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="knotty" value_original="knotty" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
      </biological_entity>
      <relation from="o27932" id="r4387" name="with" negation="false" src="d0_s0" to="o27933" />
    </statement>
    <statement id="d0_s1">
      <text>Culms 50-90 cm tall, 1-2.9 thick, erect, glabrous;</text>
      <biological_entity id="o27934" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="height" src="d0_s1" to="90" to_unit="cm" />
        <character char_type="range_value" from="1" name="thickness" src="d0_s1" to="2.9" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes usually 3.</text>
      <biological_entity id="o27935" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths mostly glabrous, mar¬gins ciliate distally;</text>
      <biological_entity id="o27936" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>collars glabrous, often with tufts of hair to 1.5 mm on the margins;</text>
      <biological_entity id="o27937" name="collar" name_original="collars" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o27938" name="tuft" name_original="tufts" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="on margins" constraintid="o27940" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27939" name="hair" name_original="hair" src="d0_s4" type="structure" />
      <biological_entity id="o27940" name="margin" name_original="margins" src="d0_s4" type="structure" />
      <relation from="o27937" id="r4388" modifier="often" name="with" negation="false" src="d0_s4" to="o27938" />
      <relation from="o27938" id="r4389" name="part_of" negation="false" src="d0_s4" to="o27939" />
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.5-1.5 mm, membranous, ciliate, cilia 1-2 mm, slightly longer at the leaf margins;</text>
      <biological_entity id="o27941" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o27942" name="cilium" name_original="cilia" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
        <character constraint="at leaf margins" constraintid="o27943" is_modifier="false" modifier="slightly" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o27943" name="margin" name_original="margins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>blades 10-50 cm long, usually tightly convolute and 0.4-1 mm in diameter, 2-4 mm wide when flat, erect, abaxial surfaces smooth, adaxial surfaces usually glabrous.</text>
      <biological_entity id="o27944" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s6" to="50" to_unit="cm" />
        <character is_modifier="false" modifier="usually tightly" name="arrangement_or_shape" src="d0_s6" value="convolute" value_original="convolute" />
        <character char_type="range_value" constraint="in abaxial surfaces" constraintid="o27945" from="0.4" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o27945" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="true" name="character" src="d0_s6" value="diameter" value_original="diameter" />
        <character char_type="range_value" from="2" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="true" name="width" src="d0_s6" value="wide" value_original="wide" />
        <character is_modifier="true" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="true" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o27946" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 10-20 cm long, 1-5 cm wide, bases sometimes included in the leaf-sheaths;</text>
      <biological_entity id="o27947" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s7" to="20" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s7" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o27948" name="base" name_original="bases" src="d0_s7" type="structure" />
      <biological_entity id="o27949" name="sheath" name_original="leaf-sheaths" src="d0_s7" type="structure" />
      <relation from="o27948" id="r4390" modifier="sometimes" name="included in the" negation="false" src="d0_s7" to="o27949" />
    </statement>
    <statement id="d0_s8">
      <text>branches ascending to spreading, longest lower branches 4-12 cm.</text>
      <biological_entity id="o27950" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s8" to="spreading" />
      </biological_entity>
      <biological_entity constraint="longest lower" id="o27951" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s8" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Glumes subequal, 6.3-13 mm, linear-lanceolate, 3-veined, midveins smooth, scabrous, or with stiff hairs, varying within a panicle, apices acuminate;</text>
      <biological_entity id="o27952" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="6.3" from_unit="mm" name="some_measurement" src="d0_s9" to="13" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o27953" name="midvein" name_original="midveins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
        <character constraint="within panicle" constraintid="o27955" is_modifier="false" name="variability" notes="" src="d0_s9" value="varying" value_original="varying" />
      </biological_entity>
      <biological_entity id="o27954" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s9" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o27955" name="panicle" name_original="panicle" src="d0_s9" type="structure" />
      <biological_entity id="o27956" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o27953" id="r4391" name="with" negation="false" src="d0_s9" to="o27954" />
    </statement>
    <statement id="d0_s10">
      <text>florets 5.5-8 mm long, about 0.8-1.3 mm thick, fusiform;</text>
      <biological_entity id="o27957" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="length" src="d0_s10" to="8" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="thickness" src="d0_s10" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="fusiform" value_original="fusiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>calluses 0.3-0.6 mm, blunt, strigose, hairs 0.5-0.8 mm;</text>
      <biological_entity id="o27958" name="callus" name_original="calluses" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s11" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o27959" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lemmas pubescent on the proximal 1/2, the pubescence over the midvein extending to the awn, hairs 0.5-0.8 mm, distal portion glabrous, tapering to the apices, apices with 0.7-1 mm hairs around the base of the awn;</text>
      <biological_entity id="o27960" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character constraint="on proximal 1/2" constraintid="o27961" is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o27961" name="1/2" name_original="1/2" src="d0_s12" type="structure" />
      <biological_entity id="o27962" name="midvein" name_original="midvein" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="extending" value_original="extending" />
      </biological_entity>
      <biological_entity id="o27963" name="awn" name_original="awn" src="d0_s12" type="structure" />
      <biological_entity id="o27964" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o27965" name="portion" name_original="portion" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character constraint="to apices" constraintid="o27966" is_modifier="false" name="shape" src="d0_s12" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o27966" name="apex" name_original="apices" src="d0_s12" type="structure" />
      <biological_entity id="o27967" name="apex" name_original="apices" src="d0_s12" type="structure" />
      <biological_entity id="o27968" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27969" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o27970" name="awn" name_original="awn" src="d0_s12" type="structure" />
      <relation from="o27960" id="r4392" name="over" negation="false" src="d0_s12" to="o27962" />
      <relation from="o27962" id="r4393" name="to" negation="false" src="d0_s12" to="o27963" />
      <relation from="o27967" id="r4394" name="with" negation="false" src="d0_s12" to="o27968" />
      <relation from="o27968" id="r4395" name="around" negation="false" src="d0_s12" to="o27969" />
      <relation from="o27969" id="r4396" name="part_of" negation="false" src="d0_s12" to="o27970" />
    </statement>
    <statement id="d0_s13">
      <text>awns 11-23 mm, glabrous or scabrous, usually twice-geniculate;</text>
      <biological_entity id="o27971" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s13" to="23" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="shape" src="d0_s13" value="2 times-geniculate" value_original="2 times-geniculate " />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>paleas 3.5-6.5 mm, 3/4 - 9/10 as long as the lemmas, pubescent over the central portion, apices involute;</text>
      <biological_entity id="o27972" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s14" to="6.5" to_unit="mm" />
        <character char_type="range_value" constraint="as-long-as lemmas" constraintid="o27973" from="3/4" name="quantity" src="d0_s14" to="9/10" />
        <character constraint="over central portion" constraintid="o27974" is_modifier="false" name="pubescence" notes="" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o27973" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
      <biological_entity constraint="central" id="o27974" name="portion" name_original="portion" src="d0_s14" type="structure" />
      <biological_entity id="o27975" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s14" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lodicules 3;</text>
      <biological_entity id="o27976" name="lodicule" name_original="lodicules" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers 3-4 mm, penicillate.</text>
      <biological_entity id="o27977" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s16" value="penicillate" value_original="penicillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Caryopses about 3 mm long, 1-1.4 mm thick;</text>
      <biological_entity id="o27978" name="caryopse" name_original="caryopses" src="d0_s17" type="structure">
        <character name="length" src="d0_s17" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="1" from_unit="mm" name="thickness" src="d0_s17" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>style bases upright, centric.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = unknown.</text>
      <biological_entity constraint="style" id="o27979" name="base" name_original="bases" src="d0_s18" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s18" value="upright" value_original="upright" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27980" name="chromosome" name_original="" src="d0_s19" type="structure" />
    </statement>
  </description>
  <discussion>Amelichloa clandestina is native from northern Mexico to Colombia. It has been accidentally introduced to pastures and roadsides in Texas, and is now established there. Reports from California need to be checked. They may reflect misidentifications of A. brachychaeta, which differs from A. clandestina in its shorter ligules, florets, and anthers.</discussion>
  
</bio:treatment>