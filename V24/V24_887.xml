<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">621</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Scribn." date="unknown" rank="genus">SPHENOPHOLIS</taxon_name>
    <taxon_name authority="(Chapm.) Scribn." date="unknown" rank="species">filiformis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus sphenopholis;species filiformis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Southern wedgegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 20-100 cm.</text>
      <biological_entity id="o31614" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Sheaths smooth, usually glabrous, some¬times pubescent;</text>
      <biological_entity id="o31615" name="sheath" name_original="sheaths" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>ligules 0.4-0.75 mm, erose-ciliate;</text>
      <biological_entity id="o31616" name="ligule" name_original="ligules" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s2" to="0.75" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s2" value="erose-ciliate" value_original="erose-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 2-45 cm long, 0.3-1.5 (2) mm wide, involute to filiform.</text>
      <biological_entity id="o31617" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="45" to_unit="cm" />
        <character name="width" src="d0_s3" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s3" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="involute" name="shape" src="d0_s3" to="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Panicles 5-15 cm long, 0.5-1 (2) cm wide, sometimes nodding, spikelets loosely to densely arranged.</text>
      <biological_entity id="o31618" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="15" to_unit="cm" />
        <character name="width" src="d0_s4" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="1" to_unit="cm" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s4" value="nodding" value_original="nodding" />
      </biological_entity>
      <biological_entity id="o31619" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="loosely to densely" name="arrangement" src="d0_s4" value="arranged" value_original="arranged" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spikelets 2.3-5 mm.</text>
      <biological_entity id="o31620" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Lower glumes less than 1/3 as wide as the upper glumes, rarely slightly wider;</text>
      <biological_entity constraint="lower" id="o31621" name="glume" name_original="glumes" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="as-wide-as upper glumes" constraintid="o31622" from="0" name="quantity" src="d0_s6" to="1/3" />
        <character is_modifier="false" modifier="rarely slightly" name="width" notes="" src="d0_s6" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity constraint="upper" id="o31622" name="glume" name_original="glumes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>upper glumes 1.8-2.9 mm, obovate to oblanceolate, width/length ratio 0.16-0.45, apices rounded to truncate;</text>
      <biological_entity constraint="upper" id="o31623" name="glume" name_original="glumes" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s7" to="2.9" to_unit="mm" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s7" to="oblanceolate" />
        <character is_modifier="false" name="width/length" src="d0_s7" value="ratio" value_original="ratio" />
        <character char_type="range_value" from="0.16" name="quantity" src="d0_s7" to="0.45" />
      </biological_entity>
      <biological_entity id="o31624" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s7" to="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lowest lemmas 2-3 mm, scabridulous distally;</text>
      <biological_entity constraint="lowest" id="o31625" name="lemma" name_original="lemmas" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="relief" src="d0_s8" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>distal lemmas scabrous on the sides, unawned or infrequently awned, awns (0.1) 1-3 mm;</text>
      <biological_entity constraint="distal" id="o31626" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character constraint="on sides" constraintid="o31627" is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="infrequently" name="architecture_or_shape" notes="" src="d0_s9" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o31627" name="side" name_original="sides" src="d0_s9" type="structure" />
      <biological_entity id="o31628" name="awn" name_original="awns" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="0.1" value_original="0.1" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers (0.5) 1-1.9 mm. 2n = 14.</text>
      <biological_entity id="o31629" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="0.5" value_original="0.5" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31630" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sphenopholis filiformis grows in sandy soils of pine and mixed pine forests, at 0-500 m, in the southeastern United States. It is found primarily in the coastal plain, but extends to the piedmont. Smith (1991) reported it for northern Arkansas (Nielsen 4946, identification not verified). Sphenopholis filiformis differs from occasional forms of S. obtusata with somewhat scabrous distal lemmas in having narrower leaves.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Va.;Okla.;Ga.;Tex.;La.;Ala.;Tenn.;N.C.;S.C.;Ark.;Miss.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>