<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">552</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Soreng" date="unknown" rank="section">Madropoa</taxon_name>
    <taxon_name authority="Soreng" date="unknown" rank="subsection">Madropoa</taxon_name>
    <taxon_name authority="Soreng &amp; D.J. Keil" date="unknown" rank="species">diaboli</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section madropoa;subsection madropoa;species diaboli</taxon_hierarchy>
  </taxon_identification>
  <number>38</number>
  <other_name type="common_name">Diablo bluegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>loosely tufted, forming airy mounds to 30 cm across, shortly rhizomatous and stoloniferous.</text>
      <biological_entity id="o12140" name="mound" name_original="mounds" src="d0_s1" type="structure" />
      <relation from="o12139" id="r1924" name="forming airy" negation="false" src="d0_s1" to="o12140" />
    </statement>
    <statement id="d0_s2">
      <text>Basal branching extra, pseudo, and intravaginal.</text>
      <biological_entity id="o12139" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="loosely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="position" src="d0_s2" value="intravaginal" value_original="intravaginal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms 26-50 cm tall, 0.5-0.9 mm thick, bases decum¬bent or nearly erect, frequently branching above the base, terete or weakly compressed;</text>
      <biological_entity id="o12141" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="26" from_unit="cm" name="height" src="d0_s3" to="50" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="thickness" src="d0_s3" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12142" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="bent" value_original="bent" />
        <character is_modifier="false" modifier="nearly" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character constraint="above base" constraintid="o12143" is_modifier="false" modifier="frequently" name="architecture" src="d0_s3" value="branching" value_original="branching" />
        <character is_modifier="false" name="shape" src="d0_s3" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o12143" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>nodes terete, 1-2 exserted.</text>
      <biological_entity id="o12144" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="2" />
        <character is_modifier="false" name="position" src="d0_s4" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths closed for 2/5 – 7/10 their length, weakly keeled, sparsely scabrous, glabrous, bases of basal sheaths glabrous, distal sheath lengths 0.6-2.4 times blade lengths;</text>
      <biological_entity id="o12145" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="length" src="d0_s5" value="closed" value_original="closed" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s5" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o12146" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o12147" name="sheath" name_original="sheaths" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o12148" name="sheath" name_original="sheath" src="d0_s5" type="structure">
        <character constraint="blade" constraintid="o12149" is_modifier="false" name="length" src="d0_s5" value="0.6-2.4 times blade lengths" value_original="0.6-2.4 times blade lengths" />
      </biological_entity>
      <biological_entity id="o12149" name="blade" name_original="blade" src="d0_s5" type="structure" />
      <relation from="o12146" id="r1925" name="part_of" negation="false" src="d0_s5" to="o12147" />
    </statement>
    <statement id="d0_s6">
      <text>collars scabrous or pubescent on the margins;</text>
      <biological_entity id="o12150" name="collar" name_original="collars" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character constraint="on margins" constraintid="o12151" is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o12151" name="margin" name_original="margins" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>ligules (1) 2-3 mm, moderately densely scabrous, truncate, obtuse, or acute, lacerate to entire;</text>
      <biological_entity id="o12152" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character name="atypical_some_measurement" src="d0_s7" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="moderately densely" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="shape" src="d0_s7" value="truncate" value_original="truncate" />
        <character char_type="range_value" from="acute lacerate" name="shape" src="d0_s7" to="entire" />
        <character char_type="range_value" from="acute lacerate" name="shape" src="d0_s7" to="entire" />
        <character char_type="range_value" from="acute lacerate" name="shape" src="d0_s7" to="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>innovation blades to 20 cm, adaxial surfaces sparsely scabrous, glabrous or hispidulous on and between the veins;</text>
      <biological_entity constraint="innovation" id="o12153" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s8" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o12154" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character constraint="on and between veins" constraintid="o12155" is_modifier="false" name="pubescence" src="d0_s8" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
      <biological_entity id="o12155" name="vein" name_original="veins" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>cauline blades 1.5-2.4 mm wide, folded or flat, thin, soft, abaxial surfaces smooth, veins prominent, keel and margins scabrous, adaxial surfaces moderately scabrous over the veins, sparsely scabrous between the veins, apices narrowly prow-shaped, flag leaf-blades 2.9-8.6 (11) cm.</text>
      <biological_entity constraint="cauline" id="o12156" name="blade" name_original="blades" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="width" src="d0_s9" value="thin" value_original="thin" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s9" value="soft" value_original="soft" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o12157" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o12158" name="vein" name_original="veins" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s9" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o12159" name="keel" name_original="keel" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o12160" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o12161" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character constraint="over veins" constraintid="o12162" is_modifier="false" modifier="moderately" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
        <character constraint="between veins" constraintid="o12163" is_modifier="false" modifier="sparsely" name="pubescence_or_relief" notes="" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o12162" name="vein" name_original="veins" src="d0_s9" type="structure" />
      <biological_entity id="o12163" name="vein" name_original="veins" src="d0_s9" type="structure" />
      <biological_entity id="o12164" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
      <biological_entity id="o12165" name="blade-leaf" name_original="leaf-blades" src="d0_s9" type="structure">
        <character name="atypical_distance" src="d0_s9" unit="cm" value="11" value_original="11" />
        <character char_type="range_value" from="2.9" from_unit="cm" name="distance" src="d0_s9" to="8.6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Panicles (4) 5.5-10.5 (12.5) cm, erect, ovoid to broadly pyramidal, open, or eventually loosely contracted, sparse, with 10-40 spikelets;</text>
      <biological_entity id="o12166" name="panicle" name_original="panicles" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="cm" value="4" value_original="4" />
        <character char_type="range_value" from="5.5" from_unit="cm" name="some_measurement" src="d0_s10" to="10.5" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s10" to="broadly pyramidal" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="open" value_original="open" />
        <character is_modifier="false" modifier="eventually loosely" name="condition_or_size" src="d0_s10" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="count_or_density" src="d0_s10" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o12167" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s10" to="40" />
      </biological_entity>
      <relation from="o12166" id="r1926" name="with" negation="false" src="d0_s10" to="o12167" />
    </statement>
    <statement id="d0_s11">
      <text>nodes with 1-2 branches;</text>
      <biological_entity id="o12168" name="node" name_original="nodes" src="d0_s11" type="structure" />
      <biological_entity id="o12169" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s11" to="2" />
      </biological_entity>
      <relation from="o12168" id="r1927" name="with" negation="false" src="d0_s11" to="o12169" />
    </statement>
    <statement id="d0_s12">
      <text>branches 2.1-4.5 (7) cm, ascending, lax, angled, angles moderately to densely scabrous, less scabrous between the angles, with 1-9 spikelets.</text>
      <biological_entity id="o12170" name="branch" name_original="branches" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="cm" value="7" value_original="7" />
        <character char_type="range_value" from="2.1" from_unit="cm" name="some_measurement" src="d0_s12" to="4.5" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s12" value="lax" value_original="lax" />
        <character is_modifier="false" name="shape" src="d0_s12" value="angled" value_original="angled" />
      </biological_entity>
      <biological_entity id="o12171" name="angle" name_original="angles" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence_or_relief" src="d0_s12" value="scabrous" value_original="scabrous" />
        <character constraint="between angles" constraintid="o12172" is_modifier="false" modifier="less" name="pubescence_or_relief" src="d0_s12" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o12172" name="angle" name_original="angles" src="d0_s12" type="structure" />
      <biological_entity id="o12173" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s12" to="9" />
      </biological_entity>
      <relation from="o12171" id="r1928" name="with" negation="false" src="d0_s12" to="o12173" />
    </statement>
    <statement id="d0_s13">
      <text>Spikelets 5.3-9 mm, lengths to 3 times widths, laterally compressed, not sexually dimorphic;</text>
      <biological_entity id="o12174" name="spikelet" name_original="spikelets" src="d0_s13" type="structure">
        <character char_type="range_value" from="5.3" from_unit="mm" name="some_measurement" src="d0_s13" to="9" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s13" value="0-3" value_original="0-3" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s13" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="not sexually" name="growth_form" src="d0_s13" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>florets (2) 3-6 (7);</text>
      <biological_entity id="o12175" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character name="atypical_quantity" src="d0_s14" value="2" value_original="2" />
        <character name="atypical_quantity" src="d0_s14" value="7" value_original="7" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s14" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>rachilla internodes 1-1.3 mm, visible from the sides, usually sparsely to densely, coarsely scabrous, infrequently smooth.</text>
      <biological_entity constraint="rachilla" id="o12176" name="internode" name_original="internodes" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.3" to_unit="mm" />
        <character constraint="from sides" constraintid="o12177" is_modifier="false" name="prominence" src="d0_s15" value="visible" value_original="visible" />
        <character is_modifier="false" modifier="usually sparsely; sparsely to densely; densely; coarsely" name="pubescence_or_relief" notes="" src="d0_s15" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="infrequently" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o12177" name="side" name_original="sides" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Glumes distinctly keeled;</text>
      <biological_entity id="o12178" name="glume" name_original="glumes" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s16" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lower glumes (2) 2.7-3.8 mm, 3-veined, upper glumes (2.3) 2.9-3.9 mm;</text>
      <biological_entity constraint="lower" id="o12179" name="glume" name_original="glumes" src="d0_s17" type="structure">
        <character name="atypical_some_measurement" src="d0_s17" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="some_measurement" src="d0_s17" to="3.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity constraint="upper" id="o12180" name="glume" name_original="glumes" src="d0_s17" type="structure">
        <character name="atypical_some_measurement" src="d0_s17" unit="mm" value="2.3" value_original="2.3" />
        <character char_type="range_value" from="2.9" from_unit="mm" name="some_measurement" src="d0_s17" to="3.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>calluses diffusely webbed, hairs 1/3-1/2 the lemma length;</text>
      <biological_entity id="o12181" name="callus" name_original="calluses" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="diffusely" name="pubescence" src="d0_s18" value="webbed" value_original="webbed" />
      </biological_entity>
      <biological_entity id="o12182" name="hair" name_original="hairs" src="d0_s18" type="structure">
        <character char_type="range_value" from="1/3" name="quantity" src="d0_s18" to="1/2" />
      </biological_entity>
      <biological_entity id="o12183" name="lemma" name_original="lemma" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>lemmas (3.2) 4.25-5 mm, lanceolate to narrowly lanceolate, distinctly keeled, glabrous, moderately to densely, infrequently sparsely, scabrous, lateral-veins prominent, margins narrowly scarious, glabrous, apices acute to narrowly acute;</text>
      <biological_entity id="o12184" name="lemma" name_original="lemmas" src="d0_s19" type="structure">
        <character name="atypical_some_measurement" src="d0_s19" unit="mm" value="3.2" value_original="3.2" />
        <character char_type="range_value" from="4.25" from_unit="mm" name="some_measurement" src="d0_s19" to="5" to_unit="mm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s19" to="narrowly lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s19" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="moderately to densely; densely; infrequently sparsely; sparsely" name="pubescence_or_relief" src="d0_s19" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o12185" name="lateral-vein" name_original="lateral-veins" src="d0_s19" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s19" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o12186" name="margin" name_original="margins" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="narrowly" name="texture" src="d0_s19" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o12187" name="apex" name_original="apices" src="d0_s19" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s19" to="narrowly acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>paleas 3/4 as long as to subequal to the lemmas, keels scabrous, intercostal regions scabrous;</text>
      <biological_entity id="o12188" name="palea" name_original="paleas" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="3/4" value_original="3/4" />
        <character constraint="to lemmas" constraintid="o12189" is_modifier="false" name="size" src="d0_s20" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o12189" name="lemma" name_original="lemmas" src="d0_s20" type="structure" />
      <biological_entity id="o12190" name="keel" name_original="keels" src="d0_s20" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s20" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o12191" name="region" name_original="regions" src="d0_s20" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s20" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>anthers (1.4) 1.75-2.6 mm, or vestigial (0.1-0.2 mm).</text>
    </statement>
    <statement id="d0_s22">
      <text>2n = unknown.</text>
      <biological_entity id="o12192" name="anther" name_original="anthers" src="d0_s21" type="structure">
        <character name="atypical_some_measurement" src="d0_s21" unit="mm" value="1.4" value_original="1.4" />
        <character char_type="range_value" from="1.75" from_unit="mm" name="some_measurement" src="d0_s21" to="2.6" to_unit="mm" />
        <character is_modifier="false" name="prominence" src="d0_s21" value="vestigial" value_original="vestigial" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12193" name="chromosome" name_original="" src="d0_s22" type="structure" />
    </statement>
  </description>
  <discussion>Poa diaboli, which is sequentially gynomonoecious, is endemic to upper shaly slopes, in soft coastal scrub and openings in Bishop Pine stands, in the coastal mountains of San Luis Obispo County, California. It is closely related to P. confinis (see previous), from which it differs by a suite of characters. The two species are also ecologically and geographically distinct.</discussion>
  
</bio:treatment>