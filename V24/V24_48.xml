<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Edward E. Terrell;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">47</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">EHRHARTOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ORYZEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ZIZANIA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily ehrhartoideae;tribe oryzeae;genus zizania</taxon_hierarchy>
  </taxon_identification>
  <number>5.04</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>aquatic, usually rooted in the substrate;</text>
      <biological_entity id="o22983" name="substrate" name_original="substrate" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>sometimes rhizomatous or stoloniferous;</text>
    </statement>
    <statement id="d0_s3">
      <text>monoecious.</text>
      <biological_entity id="o22982" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s1" value="aquatic" value_original="aquatic" />
        <character constraint="in substrate" constraintid="o22983" is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="rooted" value_original="rooted" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" name="reproduction" src="d0_s3" value="monoecious" value_original="monoecious" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Culms to 5 m, erect and emergent or floating.</text>
      <biological_entity id="o22984" name="culm" name_original="culms" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s4" to="5" to_unit="m" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="location" src="d0_s4" value="emergent" value_original="emergent" />
        <character is_modifier="false" name="location" src="d0_s4" value="floating" value_original="floating" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves concentrated on the lower portion of the stem or evenly distributed;</text>
      <biological_entity id="o22985" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="on lower portion" constraintid="o22986" is_modifier="false" name="arrangement_or_density" src="d0_s5" value="concentrated" value_original="concentrated" />
        <character is_modifier="false" modifier="evenly" name="arrangement" src="d0_s5" value="distributed" value_original="distributed" />
      </biological_entity>
      <biological_entity constraint="lower" id="o22986" name="portion" name_original="portion" src="d0_s5" type="structure" />
      <biological_entity id="o22987" name="stem" name_original="stem" src="d0_s5" type="structure" />
      <relation from="o22986" id="r3656" name="part_of" negation="false" src="d0_s5" to="o22987" />
    </statement>
    <statement id="d0_s6">
      <text>sheaths open, not inflated;</text>
      <biological_entity id="o22988" name="sheath" name_original="sheaths" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="inflated" value_original="inflated" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules membranous or scarious, glabrous;</text>
      <biological_entity id="o22989" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character is_modifier="false" name="texture" src="d0_s7" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pseudopetioles absent;</text>
      <biological_entity id="o22990" name="pseudopetiole" name_original="pseudopetioles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>blades flat, aerial or floating, scabrous or smooth.</text>
      <biological_entity id="o22991" name="blade" name_original="blades" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="location" src="d0_s9" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="location" src="d0_s9" value="floating" value_original="floating" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Inflorescences terminal panicles;</text>
      <biological_entity id="o22992" name="inflorescence" name_original="inflorescences" src="d0_s10" type="structure" />
      <biological_entity constraint="terminal" id="o22993" name="panicle" name_original="panicles" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>branches usually unisexual, lower branches staminate, upper branches pistillate, middle branches sometimes with staminate and pistillate spikelets intermixed;</text>
      <biological_entity id="o22994" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s11" value="unisexual" value_original="unisexual" />
      </biological_entity>
      <biological_entity constraint="lower" id="o22995" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="upper" id="o22996" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="middle" id="o22997" name="branch" name_original="branches" src="d0_s11" type="structure" />
      <biological_entity id="o22998" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <relation from="o22997" id="r3657" name="with" negation="false" src="d0_s11" to="o22998" />
    </statement>
    <statement id="d0_s12">
      <text>pedicel apices cupulate;</text>
    </statement>
    <statement id="d0_s13">
      <text>disarticulation beneath the spikelets, in cultivated strains disarticulation delayed, the spikelets tending not to shatter until harvested.</text>
      <biological_entity constraint="pedicel" id="o22999" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="cupulate" value_original="cupulate" />
      </biological_entity>
      <biological_entity id="o23000" name="spikelet" name_original="spikelets" src="d0_s13" type="structure" />
      <biological_entity id="o23001" name="strain" name_original="strains" src="d0_s13" type="structure">
        <character is_modifier="true" name="condition" src="d0_s13" value="cultivated" value_original="cultivated" />
      </biological_entity>
      <biological_entity id="o23002" name="spikelet" name_original="spikelets" src="d0_s13" type="structure" />
      <relation from="o22999" id="r3658" name="beneath" negation="false" src="d0_s13" to="o23000" />
    </statement>
    <statement id="d0_s14">
      <text>Spikelets unisexual, with 1 floret.</text>
      <biological_entity id="o23003" name="spikelet" name_original="spikelets" src="d0_s14" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s14" value="unisexual" value_original="unisexual" />
      </biological_entity>
      <biological_entity id="o23004" name="floret" name_original="floret" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="1" value_original="1" />
      </biological_entity>
      <relation from="o23003" id="r3659" name="with" negation="false" src="d0_s14" to="o23004" />
    </statement>
    <statement id="d0_s15">
      <text>Glumes absent;</text>
      <biological_entity id="o23005" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>calluses inconspicuous;</text>
      <biological_entity id="o23006" name="callus" name_original="calluses" src="d0_s16" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s16" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lemmas 5-veined;</text>
      <biological_entity id="o23007" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="5-veined" value_original="5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>paleas 3-veined;</text>
      <biological_entity id="o23008" name="palea" name_original="paleas" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>lodicules 2, membranous.</text>
      <biological_entity id="o23009" name="lodicule" name_original="lodicules" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="2" value_original="2" />
        <character is_modifier="false" name="texture" src="d0_s19" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Staminate spikelets pendant, terete or appearing so;</text>
      <biological_entity id="o23010" name="spikelet" name_original="spikelets" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="orientation" src="d0_s20" value="pendant" value_original="pendant" />
        <character is_modifier="false" name="shape" src="d0_s20" value="terete" value_original="terete" />
        <character name="shape" src="d0_s20" value="appearing" value_original="appearing" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>lemmas membranous;</text>
      <biological_entity id="o23011" name="lemma" name_original="lemmas" src="d0_s21" type="structure">
        <character is_modifier="false" name="texture" src="d0_s21" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>paleas membranous, loosely enclosing the stamens;</text>
      <biological_entity id="o23012" name="palea" name_original="paleas" src="d0_s22" type="structure">
        <character is_modifier="false" name="texture" src="d0_s22" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o23013" name="stamen" name_original="stamens" src="d0_s22" type="structure" />
      <relation from="o23012" id="r3660" modifier="loosely" name="enclosing the" negation="false" src="d0_s22" to="o23013" />
    </statement>
    <statement id="d0_s23">
      <text>anthers 6.</text>
      <biological_entity id="o23014" name="anther" name_original="anthers" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>Pistillate spikelets terete;</text>
      <biological_entity id="o23015" name="spikelet" name_original="spikelets" src="d0_s24" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s24" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="shape" src="d0_s24" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>lemmas chartaceous or coriaceous, margins involute and clasping the margins of the paleas, apices acute to acuminate, sometimes awned, awns terminal, slender, scabridulous;</text>
      <biological_entity id="o23016" name="lemma" name_original="lemmas" src="d0_s25" type="structure">
        <character is_modifier="false" name="texture" src="d0_s25" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" name="texture" src="d0_s25" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o23017" name="margin" name_original="margins" src="d0_s25" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s25" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity id="o23018" name="margin" name_original="margins" src="d0_s25" type="structure">
        <character is_modifier="true" name="architecture_or_fixation" src="d0_s25" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o23019" name="palea" name_original="paleas" src="d0_s25" type="structure" />
      <biological_entity id="o23020" name="apex" name_original="apices" src="d0_s25" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s25" to="acuminate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s25" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o23021" name="awn" name_original="awns" src="d0_s25" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s25" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="size" src="d0_s25" value="slender" value_original="slender" />
        <character is_modifier="false" name="relief" src="d0_s25" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <relation from="o23018" id="r3661" name="part_of" negation="false" src="d0_s25" to="o23019" />
    </statement>
    <statement id="d0_s26">
      <text>styles 2, bases not fused, stigmas laterally exserted, plumose.</text>
      <biological_entity id="o23022" name="style" name_original="styles" src="d0_s26" type="structure">
        <character name="quantity" src="d0_s26" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o23023" name="base" name_original="bases" src="d0_s26" type="structure">
        <character is_modifier="false" modifier="not" name="fusion" src="d0_s26" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o23024" name="stigma" name_original="stigmas" src="d0_s26" type="structure">
        <character is_modifier="false" modifier="laterally" name="position" src="d0_s26" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="shape" src="d0_s26" value="plumose" value_original="plumose" />
      </biological_entity>
    </statement>
    <statement id="d0_s27">
      <text>Caryopses cylindrical;</text>
      <biological_entity id="o23025" name="caryopse" name_original="caryopses" src="d0_s27" type="structure">
        <character is_modifier="false" name="shape" src="d0_s27" value="cylindrical" value_original="cylindrical" />
      </biological_entity>
    </statement>
    <statement id="d0_s28">
      <text>embryos linear, often as long as the caryopses;</text>
      <biological_entity id="o23026" name="embryo" name_original="embryos" src="d0_s28" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s28" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o23027" name="caryopse" name_original="caryopses" src="d0_s28" type="structure" />
      <relation from="o23026" id="r3662" modifier="often" name="as long as" negation="false" src="d0_s28" to="o23027" />
    </statement>
    <statement id="d0_s29">
      <text>hila linear, x = 15.</text>
      <biological_entity id="o23028" name="hilum" name_original="hila" src="d0_s29" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s29" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity constraint="x" id="o23029" name="chromosome" name_original="" src="d0_s29" type="structure">
        <character name="quantity" src="d0_s29" value="15" value_original="15" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Zizania includes three North American and one eastern Asian species. Zizania aquatica and Z. palustris are important constituents of aquatic plant communities in North America, providing food and shelter for numerous animal species. Zizania palustris is also an important food source for humans. Zizania texana is federally listed as an endangered species in the United States. Zizania latifolia, an Asian species, is available through horticultural outlets despite its potential for harboring a fungus that would devastate the native species (for additional information, see the comment following the species description).</discussion>
  <references>
    <reference>Aiken, S.G., P.F. Lee, D. Punter, and J.M. Stewart. 1988. Wild Rice in Canada. Agriculture Canada Publication 1830. NC Press, Toronto, Ontario, Canada. 130 pp.</reference>
    <reference>Dore, W.G. 1969. Wild Rice. Canada Department of Agriculture Publication No. 1393. Information Canada, Ottawa, Ontario, Canada. 84 pp.</reference>
    <reference>Duvall, M.R. and D.D. Biesboer. 1988. Nonreciprocal hybridization failure in crosses between annual wild-rice species (Zizania palustris x Z. aquatica: Poaceae). Syst. Bot. 13:229-234</reference>
    <reference>Environment Walkato. 2002-2007. Regional Pest Management Strategy. Walkato Regional Council, Hamilton East, New Zealand, http://www.ew.govt.nz/policyandplans/rpmsintro/ rpms2002/operative5.2.7.htm/</reference>
    <reference>Liu, L. and S.M. Phillips. 2006. Zizania. Pp. 187-188 in Z.-Y. Wu, P.H. Raven, and D.-Y. Hong (eds.). Flora of China, vol. 22 (Poaceae). Science Press, Beijing, Peoples Republic of China and Missouri Botanical Garden Press, St. Louis, Missouri, U.S.A. 653 pp. http://flora.huh.harvard.edu/china/mss/volume22/index.htm/</reference>
    <reference>Terrell, E.E. and L.R. Batra. 1982. Zizania latifolia and Ustilago esculenta, a grass-fungus association. Econ. Bot. 36:274-285</reference>
    <reference>Terrell, E.E., W.H.P. Emery, and H.E. Beaty. 1978. Observations on Zizania texana (Texas wildrice), an endangered species. Bull. Torrey Bot. Club 105:50-57</reference>
    <reference>Terrell, E.E., P.M. Peterson, J.L. Reveal, and M.R. Duvall. 1997. Taxonomy of North American species of Zizania (Poaceae). Sida 17:533-549</reference>
    <reference>Warwick, S.I. and S.G. Aiken. 1986. Electrophoretic evidence for the recognition of two species in annual wild rice (Zizania, Poaceae). Syst. Bot. 11:464-473.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wash.;Del.;Wis.;Ariz.;Idaho;Minn.;N.Dak.;Nebr.;Ohio;S.Dak.;W.Va.;Mass.;Maine;N.H.;R.I.;Vt.;Fla.;Tex.;La.;Iowa;Kans.;Pa.;Ala.;Ark.;Ga.;Ill.;Md.;Mo.;Miss.;N.C.;S.C.;D.C;Va.;Colo.;Calif.;Ind.;Alta.;B.C.;Man.;N.B.;Nfld. and Labr. (Labr.);N.S.;Ont.;P.E.I.;Que.;Sask.;Mich.;Mont.;Oreg.;Tenn.;Pacific Islands (Hawaii);Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Culms decumbent, completely immersed or the upper parts of the culm emergent; known only from the San Marcos River in Hays County, Texas</description>
      <determination>3 Zizania texana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Culms usually erect at maturity, rarely completely immersed; plants not known from Texas.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Plants rhizomatous, perennial; middle branches of the panicles with both staminate and pistillate spikelets, other branches with either staminate or pistillate spikelets; plants cultivated as ornamentals</description>
      <determination>4 Zizania latifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Plants without rhizomes, annual; all panicle branches unisexual, with either staminate or pistillate spikelets; plants native and widespread, also cultivated for grain.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Lemmas of the pistillate spikelets flexible and chartaceous, dull or sublustrous, bearing short, scattered hairs, these not or only slightly more dense towards the apices; aborted pistillate spikelets 0.4-1 mm wide; pistillate inflorescence branches usually divaricate at maturity</description>
      <determination>1 Zizania aquatic</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Lemmas of the pistillate spikelets stiff and coriaceous or indurate, lustrous, glabrous or with lines of short hairs, the apices more densely hairy; aborted pistillate spikelets 0.6-2.6 mm wide; pistillate inflorescence branches usually appressed at maturity, or with 1 to few, somewhat spreading branches</description>
      <determination>2 Zizania palustris</determination>
    </key_statement>
  </key>
</bio:treatment>