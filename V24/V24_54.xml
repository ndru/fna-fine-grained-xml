<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">50</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">EHRHARTOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ORYZEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ZIZANIA</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">palustris</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">palustris</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily ehrhartoideae;tribe oryzeae;genus zizania;species palustris;variety palustris</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Zizania</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">aquatica</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">angustifolia</taxon_name>
    <taxon_hierarchy>genus zizania;species aquatica;variety angustifolia</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <other_name type="common_name">Northern wildrice</other_name>
  <other_name type="common_name">Zizanie des marais</other_name>
  <other_name type="common_name">Folle avoine</other_name>
  <other_name type="common_name">Rlz sauvage</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 2 m.</text>
      <biological_entity id="o14236" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="2" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Blades 3-21 mm wide.</text>
      <biological_entity id="o14237" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s1" to="21" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Pistillate part of inflorescences 1-8 (15) cm wide;</text>
      <biological_entity id="o14238" name="part" name_original="part" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="pistillate" value_original="pistillate" />
        <character name="width" src="d0_s2" unit="cm" value="15" value_original="15" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14239" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <relation from="o14238" id="r2266" name="part_of" negation="false" src="d0_s2" to="o14239" />
    </statement>
    <statement id="d0_s3">
      <text>branches appressed or ascending, or with 1 to few branches somewhat divergent;</text>
      <biological_entity id="o14240" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character constraint="to branches" constraintid="o14241" name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o14241" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="few" value_original="few" />
        <character is_modifier="false" modifier="somewhat" name="arrangement" src="d0_s3" value="divergent" value_original="divergent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lower pistillate branches with 2-8 spikelets.</text>
      <biological_entity constraint="lower" id="o14242" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o14243" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s4" to="8" />
      </biological_entity>
      <relation from="o14242" id="r2267" name="with" negation="false" src="d0_s4" to="o14243" />
    </statement>
  </description>
  <discussion>Zizania palustris var. palustris grows in the shallow water of lakes and streams, often forming extensive stands in northern lakes. It has been introduced to British Columbia, Nova Scotia, Idaho, Arizona, and West Virginia for waterfowl food; some of the stands in the Canadian prairies may also have resulted from planting (Aiken et al. 1988).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Maine;N.H.;N.C.;Colo.;Wash.;Ariz.;Idaho;N.Dak.;Nebr.;S.Dak.;W.Va.;B.C.;Man.;N.B.;N.S.;Ont.;P.E.I.;Que.;Minn.;Mich.;Wis.;N.Y.;Pa.;Vt.;Mont.;Oreg.;R.I.;Ala.;Ark.;Ill.;Kans.;Md.;Mo.;Ind.;Iowa;Mass.;Conn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>