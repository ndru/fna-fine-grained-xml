<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">322</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ELYMUS</taxon_name>
    <taxon_name authority="(Link) Gould" date="unknown" rank="species">trachycaulus</taxon_name>
    <taxon_name authority="(Lange) Á. Löve &amp; D. Love" date="unknown" rank="subspecies">virescens</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus elymus;species trachycaulus;subspecies virescens</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Elymus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">virescens</taxon_name>
    <taxon_hierarchy>genus elymus;species virescens</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 20-80 cm.</text>
      <biological_entity id="o29007" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Spikes 5-10 cm long, 0.5-0.8 (1) cm wide, 2-sided;</text>
      <biological_entity id="o29008" name="spike" name_original="spikes" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s1" to="10" to_unit="cm" />
        <character name="width" src="d0_s1" unit="cm" value="1" value_original="1" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s1" to="0.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes 5-10 mm.</text>
      <biological_entity id="o29009" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Spikelets usually with the bases concealed.</text>
      <biological_entity id="o29010" name="spikelet" name_original="spikelets" src="d0_s3" type="structure" />
      <biological_entity id="o29011" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="concealed" value_original="concealed" />
      </biological_entity>
      <relation from="o29010" id="r4567" name="with" negation="false" src="d0_s3" to="o29011" />
    </statement>
    <statement id="d0_s4">
      <text>Glumes 9.5-13.5 mm, 1 vein scabrous over most of its length, the basal 1/4 usually smooth, the remaining veins scabrous or smooth, awned, awns 1.5-2 mm;</text>
      <biological_entity id="o29012" name="glume" name_original="glumes" src="d0_s4" type="structure">
        <character char_type="range_value" from="9.5" from_unit="mm" name="some_measurement" src="d0_s4" to="13.5" to_unit="mm" />
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o29013" name="vein" name_original="vein" src="d0_s4" type="structure">
        <character constraint="over" is_modifier="false" name="length" src="d0_s4" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character name="quantity" src="d0_s4" value="1/4" value_original="1/4" />
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o29014" name="vein" name_original="veins" src="d0_s4" type="structure" />
      <biological_entity id="o29015" name="awn" name_original="awns" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o29013" id="r4568" name="remaining" negation="false" src="d0_s4" to="o29014" />
    </statement>
    <statement id="d0_s5">
      <text>lemmas awned, awns 2.5-10 mm, often curved.</text>
      <biological_entity id="o29016" name="lemma" name_original="lemmas" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o29017" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="often" name="course" src="d0_s5" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Elymus trachycaulus subsp. virescens is restricted to Greenland. It is very consistent in its morphology.</discussion>
  
</bio:treatment>