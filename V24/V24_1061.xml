<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">748</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Pers." date="unknown" rank="genus">TRISETUM</taxon_name>
    <taxon_name authority="(L.) P. Beauv." date="unknown" rank="species">flavescens</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus trisetum;species flavescens</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>6</number>
  <other_name type="common_name">Yellow oatgrass</other_name>
  <other_name type="common_name">Avoine jaunâtre</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, sometimes with both fertile and sterile shoots;</text>
      <biological_entity id="o21836" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s0" value="fertile" value_original="fertile" />
        <character is_modifier="true" name="reproduction" src="d0_s0" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o21835" id="r3450" modifier="sometimes" name="with" negation="false" src="d0_s0" to="o21836" />
    </statement>
    <statement id="d0_s1">
      <text>usually cespitose, sometimes rhizomatous, rhizomes usually short, to 7 cm in sandy soils.</text>
      <biological_entity id="o21835" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="usually" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o21837" name="rhizom" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character char_type="range_value" constraint="in soils" constraintid="o21838" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21838" name="soil" name_original="soils" src="d0_s1" type="structure">
        <character is_modifier="true" name="habitat" src="d0_s1" value="sandy" value_original="sandy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (10) 50-80 (130) cm, solitary or clumped, erect or decumbent, glabrous, sometimes scabrous or pubescent near the upper nodes.</text>
      <biological_entity id="o21839" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="10" value_original="10" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s2" to="80" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="clumped" value_original="clumped" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="scabrous" value_original="scabrous" />
        <character constraint="near upper nodes" constraintid="o21840" is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="upper" id="o21840" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves usually evenly distributed;</text>
      <biological_entity id="o21841" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually evenly" name="arrangement" src="d0_s3" value="distributed" value_original="distributed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths glabrous or pilose, throats often with 2+ mm hairs;</text>
      <biological_entity id="o21842" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o21843" name="throat" name_original="throats" src="d0_s4" type="structure" />
      <biological_entity id="o21844" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s4" upper_restricted="false" />
      </biological_entity>
      <relation from="o21843" id="r3451" name="with" negation="false" src="d0_s4" to="o21844" />
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.5-1 (2) mm, obtuse, lacerate, sometimes ciliolate, hairs to 0.5 mm;</text>
      <biological_entity id="o21845" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character name="atypical_some_measurement" src="d0_s5" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lacerate" value_original="lacerate" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o21846" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 5-15 (18) cm long, 1.5-4 (6) mm wide, flat or involute, lax, pubescent or pilose.</text>
      <biological_entity id="o21847" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character name="length" src="d0_s6" unit="cm" value="18" value_original="18" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="15" to_unit="cm" />
        <character name="width" src="d0_s6" unit="mm" value="6" value_original="6" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s6" value="involute" value_original="involute" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="lax" value_original="lax" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 5-20 cm long, 1.5-7 cm wide, erect or nodding, glistening yellowish-brown, sometimes purple-tinged or variegated;</text>
      <biological_entity id="o21848" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s7" to="20" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s7" to="7" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="nodding" value_original="nodding" />
        <character is_modifier="false" name="reflectance" src="d0_s7" value="glistening" value_original="glistening" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish-brown" value_original="yellowish-brown" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="purple-tinged" value_original="purple-tinged" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="variegated" value_original="variegated" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches 2-4 (6) cm, ascending to divergent, often flexuous, sometimes naked below.</text>
      <biological_entity id="o21849" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="cm" value="6" value_original="6" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s8" to="4" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="divergent" value_original="divergent" />
        <character is_modifier="false" modifier="often" name="course" src="d0_s8" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" modifier="sometimes; below" name="architecture" src="d0_s8" value="naked" value_original="naked" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 4-8 mm, subsessile or on pedicels to 5 mm, with (2) 3 (4) florets;</text>
      <biological_entity id="o21850" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="on pedicels" value_original="on pedicels" />
      </biological_entity>
      <biological_entity id="o21851" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21852" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s9" value="4" value_original="4" />
      </biological_entity>
      <relation from="o21850" id="r3452" name="on" negation="false" src="d0_s9" to="o21851" />
      <relation from="o21850" id="r3453" name="with" negation="false" src="d0_s9" to="o21852" />
    </statement>
    <statement id="d0_s10">
      <text>rachilla internodes to 1+ mm;</text>
      <biological_entity constraint="rachilla" id="o21853" name="internode" name_original="internodes" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>rachilla hairs to 1.5 mm;</text>
    </statement>
    <statement id="d0_s12">
      <text>disarticulation above the glumes, beneath the florets.</text>
      <biological_entity constraint="rachilla" id="o21854" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21855" name="glume" name_original="glumes" src="d0_s12" type="structure" />
      <biological_entity id="o21856" name="floret" name_original="florets" src="d0_s12" type="structure" />
      <relation from="o21854" id="r3454" name="above" negation="false" src="d0_s12" to="o21855" />
    </statement>
    <statement id="d0_s13">
      <text>Glumes unequal, shiny;</text>
      <biological_entity id="o21857" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="reflectance" src="d0_s13" value="shiny" value_original="shiny" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower glumes 2.5-4.7 mm, narrowly lanceolate to subulate;</text>
      <biological_entity constraint="lower" id="o21858" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s14" to="4.7" to_unit="mm" />
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s14" to="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper glumes 4-7 mm long, as long as or longer than the lowest florets, twice as wide as the lower glumes, lance-elliptic, acute;</text>
      <biological_entity constraint="upper" id="o21859" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s15" to="7" to_unit="mm" />
        <character constraint="glume" constraintid="o21862" is_modifier="false" name="width" notes="" src="d0_s15" value="2 times as wide as the lower glumes" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s15" value="lance-elliptic" value_original="lance-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o21861" name="floret" name_original="florets" src="d0_s15" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s15" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o21860" name="floret" name_original="florets" src="d0_s15" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s15" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="lower" id="o21862" name="glume" name_original="glumes" src="d0_s15" type="structure" />
      <relation from="o21859" id="r3455" name="as long as" negation="false" src="d0_s15" to="o21861" />
    </statement>
    <statement id="d0_s16">
      <text>callus hairs to 0.5 mm;</text>
      <biological_entity constraint="callus" id="o21863" name="hair" name_original="hairs" src="d0_s16" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lemmas 3.5-6.3 mm, ovatelanceolate, minutely pubescent, bifid or bicuspidate, teeth conspicuous, usually 3-6 mm, awned, awns (3) 5-9 mm, arising from the upper 1/3 of the lemmas and exceeding the apices, geniculate, tightly twisted below;</text>
      <biological_entity id="o21864" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s17" to="6.3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s17" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="bifid" value_original="bifid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="bicuspidate" value_original="bicuspidate" />
      </biological_entity>
      <biological_entity id="o21865" name="tooth" name_original="teeth" src="d0_s17" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s17" value="conspicuous" value_original="conspicuous" />
        <character char_type="range_value" from="3" from_unit="mm" modifier="usually" name="some_measurement" src="d0_s17" to="6" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o21866" name="awn" name_original="awns" src="d0_s17" type="structure">
        <character name="atypical_some_measurement" src="d0_s17" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s17" to="9" to_unit="mm" />
        <character constraint="from upper 1/3" constraintid="o21867" is_modifier="false" name="orientation" src="d0_s17" value="arising" value_original="arising" />
        <character is_modifier="false" name="shape" src="d0_s17" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" modifier="tightly; below" name="architecture" src="d0_s17" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity constraint="upper" id="o21867" name="1/3" name_original="1/3" src="d0_s17" type="structure" />
      <biological_entity id="o21868" name="lemma" name_original="lemmas" src="d0_s17" type="structure" />
      <biological_entity id="o21869" name="apex" name_original="apices" src="d0_s17" type="structure" />
      <relation from="o21867" id="r3456" name="part_of" negation="false" src="d0_s17" to="o21868" />
      <relation from="o21866" id="r3457" name="exceeding the" negation="false" src="d0_s17" to="o21869" />
    </statement>
    <statement id="d0_s18">
      <text>paleas 3-5.5 mm;</text>
      <biological_entity id="o21870" name="palea" name_original="paleas" src="d0_s18" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s18" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>anthers 1.3-2.8 mm.</text>
      <biological_entity id="o21871" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s19" to="2.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Caryopses 2.5-3 mm, glabrous.</text>
    </statement>
    <statement id="d0_s21">
      <text>2n = 28.</text>
      <biological_entity id="o21872" name="caryopse" name_original="caryopses" src="d0_s20" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s20" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s20" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21873" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Trisetum flavescens grows in seeded pastures, roadsides, and as a weed in croplands. Native to Europe, west Asia, and north Africa, it was introduced into the Flora region because of its drought resistance, wide soil tolerance, and high palatability to domestic livestock. It is one of the few range plants known to contain calcinogenic glycosides, which can lead to vitamin D toxicity in grazing animals (Dixon 1995). This species seems not to have persisted in southern Ontario (Michael Oldham, pers. comm.). Several infraspecific taxa have been recognized; no attempt has been made to determine which are present in the Flora region.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.J.;Mass.;Wash.;Calif.;Kans.;N.Y.;Pacific Islands (Hawaii);Vt.;Miss.;Okla.;Alta.;B.C.;N.S.;Ont.;Que.;Mo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>