<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">560</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Soreng" date="unknown" rank="section">Madropoa</taxon_name>
    <taxon_name authority="Hitchc. ex Soreng" date="unknown" rank="subsection">Epiles</taxon_name>
    <taxon_name authority="Vasey" date="unknown" rank="species">cusickii</taxon_name>
    <taxon_name authority="Soreng" date="unknown" rank="subspecies">pallida</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section madropoa;subsection epiles;species cusickii;subspecies pallida</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants densely tufted.</text>
    </statement>
    <statement id="d0_s1">
      <text>Basal branching intravaginal.</text>
      <biological_entity id="o5940" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" name="position" src="d0_s1" value="intravaginal" value_original="intravaginal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 10-40 (55) cm, mostly erect, with 0 (1) scarcely exserted nodes.</text>
      <biological_entity id="o5941" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="55" value_original="55" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="40" to_unit="cm" />
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o5942" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="true" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="true" modifier="scarcely" name="position" src="d0_s2" value="exserted" value_original="exserted" />
      </biological_entity>
      <relation from="o5941" id="r955" name="with" negation="false" src="d0_s2" to="o5942" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths closed for 1/4-2/3 their length, distal sheath lengths 3.6-10 times blade lengths;</text>
      <biological_entity id="o5943" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="length" src="d0_s3" value="closed" value_original="closed" />
      </biological_entity>
      <biological_entity constraint="distal" id="o5944" name="sheath" name_original="sheath" src="d0_s3" type="structure">
        <character constraint="blade" constraintid="o5945" is_modifier="false" name="length" src="d0_s3" value="3.6-10 times blade lengths" value_original="3.6-10 times blade lengths" />
      </biological_entity>
      <biological_entity id="o5945" name="blade" name_original="blade" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>innovation blades 0.5-1 mm wide, apices usually narrowly prow-shaped;</text>
      <biological_entity constraint="innovation" id="o5946" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5947" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually narrowly" name="shape" src="d0_s4" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline blades usually less than 1.5 mm wide, flat, folded, or involute, usually narrowly prow-shaped, infrequently broadly prow-shaped, flag leaf-blades 0.5-2 (3) cm.</text>
      <biological_entity constraint="cauline" id="o5948" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s5" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s5" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="involute" value_original="involute" />
        <character is_modifier="false" modifier="usually narrowly" name="shape" src="d0_s5" value="prow--shaped" value_original="prow--shaped" />
        <character is_modifier="false" modifier="infrequently broadly" name="shape" src="d0_s5" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
      <biological_entity id="o5949" name="blade-leaf" name_original="leaf-blades" src="d0_s5" type="structure">
        <character name="atypical_distance" src="d0_s5" unit="cm" value="3" value_original="3" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="distance" src="d0_s5" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 2-6 cm, contracted, with 10-40 spikelets;</text>
      <biological_entity id="o5950" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="6" to_unit="cm" />
        <character is_modifier="false" name="condition_or_size" src="d0_s6" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o5951" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s6" to="40" />
      </biological_entity>
      <relation from="o5950" id="r956" name="with" negation="false" src="d0_s6" to="o5951" />
    </statement>
    <statement id="d0_s7">
      <text>nodes with 1-3 branches;</text>
      <biological_entity id="o5952" name="node" name_original="nodes" src="d0_s7" type="structure" />
      <biological_entity id="o5953" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o5952" id="r957" name="with" negation="false" src="d0_s7" to="o5953" />
    </statement>
    <statement id="d0_s8">
      <text>branches 0.5-1.7 cm, stout, moderately to densely scabrous, with 2-5 spikelets.</text>
      <biological_entity id="o5954" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="1.7" to_unit="cm" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s8" value="stout" value_original="stout" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o5955" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <relation from="o5954" id="r958" name="with" negation="false" src="d0_s8" to="o5955" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 4-10 mm.</text>
      <biological_entity id="o5956" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Calluses glabrous;</text>
      <biological_entity id="o5957" name="callus" name_original="calluses" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lemmas 4-7 mm, glabrous;</text>
      <biological_entity id="o5958" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers vestigial (0.1-0.2 mm) or 2-3.5 mm. 2n = 56, 56+11, 59.</text>
      <biological_entity id="o5959" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="vestigial" value_original="vestigial" />
        <character name="prominence" src="d0_s12" value="2-3.5 mm" value_original="2-3.5 mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5960" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="56" value_original="56" />
        <character char_type="range_value" from="56" name="quantity" src="d0_s12" upper_restricted="false" />
        <character name="quantity" src="d0_s12" value="11" value_original="11" />
        <character name="quantity" src="d0_s12" value="59" value_original="59" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa cusickii subsp. pallida grows in forb-rich mountain grasslands to alpine habitats, from the southern Yukon Territory to California, across the Great Basin and through the Rocky Mountains to central Colorado. It is found mainly east and north of subsp. cusickii, but pistillate plants extend into the range of that subspecies in the eastern alpine peaks of California, Nevada, and Oregon. The shorter branch length serves to distinguish it from the narrow-panicled subsp. cusickii forms in most cases. It hybridizes with P. fendleriana (p. 556), forming P. xnematophylla (see next). The hybrids may have hairy lemmas or, less often, broader leaf blades and glabrous lemmas. Poa cusickii subsp. pallida was included in Hitchock's (1951) circumscription of Poa pringlei, along with P. keckii and P. suksdorfii.</discussion>
  
</bio:treatment>