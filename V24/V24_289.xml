<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">207</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">BROMEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">BROMUS</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="section">Bromopsis</taxon_name>
    <taxon_name authority="Scribn." date="unknown" rank="species">pumpellianus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">pumpellianus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe bromeae;genus bromus;section bromopsis;species pumpellianus;subspecies pumpellianus</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants rhizomatous.</text>
      <biological_entity id="o10184" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 50-120 cm, erect;</text>
      <biological_entity id="o10185" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s1" to="120" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes 2-3 (4), usually pubescent, sometimes glabrous;</text>
      <biological_entity id="o10186" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character name="atypical_quantity" src="d0_s2" value="4" value_original="4" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="3" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes glabrous or pubescent.</text>
      <biological_entity id="o10187" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths pilose, villous, or glabrous;</text>
      <biological_entity id="o10188" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>auricles present on the lower leaves or absent;</text>
      <biological_entity id="o10189" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character constraint="on lower leaves" constraintid="o10190" is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="lower" id="o10190" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>ligules to 3 mm;</text>
      <biological_entity id="o10191" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 9-17 (25) cm long, (3) 4-8 (9) mm wide, abaxial surfaces glabrous or pilose, adaxial surfaces usually pilose, rarely glabrous.</text>
      <biological_entity id="o10192" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="25" value_original="25" />
        <character char_type="range_value" from="9" from_unit="cm" name="length" src="d0_s7" to="17" to_unit="cm" />
        <character name="width" src="d0_s7" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10193" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o10194" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles 10-20 cm, open or contracted, erect or nodding;</text>
      <biological_entity id="o10195" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s8" to="20" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="open" value_original="open" />
        <character is_modifier="false" name="condition_or_size" src="d0_s8" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>branches erect to spreading.</text>
      <biological_entity id="o10196" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s9" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 20-30 mm.</text>
      <biological_entity id="o10197" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s10" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Glumes glabrous, pubescent, or hirsute;</text>
      <biological_entity id="o10198" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower glumes (4) 5-9 mm, 1-veined;</text>
      <biological_entity constraint="lower" id="o10199" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="mm" value="4" value_original="4" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="9" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-veined" value_original="1-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper glumes (5) 8-11 mm, 3-veined;</text>
      <biological_entity constraint="upper" id="o10200" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="5" value_original="5" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s13" to="11" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 9-14 mm, pubescent on the lower portion of the back and along the margins;</text>
      <biological_entity id="o10201" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s14" to="14" to_unit="mm" />
        <character constraint="on lower portion" constraintid="o10202" is_modifier="false" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="lower" id="o10202" name="portion" name_original="portion" src="d0_s14" type="structure" />
      <biological_entity id="o10203" name="back" name_original="back" src="d0_s14" type="structure" />
      <biological_entity id="o10204" name="margin" name_original="margins" src="d0_s14" type="structure" />
      <relation from="o10202" id="r1626" name="part_of" negation="false" src="d0_s14" to="o10203" />
      <relation from="o10201" id="r1627" name="along" negation="false" src="d0_s14" to="o10204" />
    </statement>
    <statement id="d0_s15">
      <text>awns usually present, to 6 mm, sometimes absent;</text>
      <biological_entity id="o10205" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s15" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers 3.5-7 mm. 2n = 56.</text>
      <biological_entity id="o10206" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s16" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10207" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bromus pumpellianus subsp. pumpellianus grows on sandy and gravelly stream banks and lake shores, sand dunes, meadows, dry grassy slopes, and road verges.</discussion>
  
</bio:treatment>