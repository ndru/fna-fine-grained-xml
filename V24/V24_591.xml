<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">418</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">FESTUCA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Festuca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Festuca</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">rubra</taxon_name>
    <taxon_name authority="(J. Presl) Pavlick" date="unknown" rank="subspecies">secunda</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus festuca;subgenus festuca;section festuca;species rubra;subspecies secunda</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <other_name type="common_name">Secund red fescue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants rhizomatous, usually loosely cespitose, with several culms arising from the same tuft.</text>
      <biological_entity id="o2413" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="usually loosely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o2414" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="several" value_original="several" />
        <character constraint="from tuft" constraintid="o2415" is_modifier="false" name="orientation" src="d0_s0" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o2415" name="tuft" name_original="tuft" src="d0_s0" type="structure" />
      <relation from="o2413" id="r405" name="with" negation="false" src="d0_s0" to="o2414" />
    </statement>
    <statement id="d0_s1">
      <text>Culms (20) 30-70 (80) cm.</text>
      <biological_entity id="o2416" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="20" value_original="20" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="70" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths glabrous or pubescent, sometimes glaucous, shredding into fibers, cauline sheaths usually wide, loosely enclosing the culms;</text>
      <biological_entity id="o2417" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="usually" name="width" src="d0_s2" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity id="o2418" name="fiber" name_original="fibers" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o2419" name="sheath" name_original="sheaths" src="d0_s2" type="structure" />
      <biological_entity id="o2420" name="culm" name_original="culms" src="d0_s2" type="structure" />
      <relation from="o2417" id="r406" name="shredding into" negation="false" src="d0_s2" to="o2418" />
      <relation from="o2417" id="r407" name="shredding into" negation="false" src="d0_s2" to="o2419" />
      <relation from="o2417" id="r408" modifier="loosely" name="enclosing the" negation="false" src="d0_s2" to="o2420" />
    </statement>
    <statement id="d0_s3">
      <text>vegetative shoot blades 0.7-1.5 mm in diameter, conduplicate, sometimes glaucous, abaxial surfaces more or less evenly scabrous, adaxial surfaces pilose on the ribs;</text>
      <biological_entity constraint="shoot" id="o2421" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s3" value="vegetative" value_original="vegetative" />
        <character char_type="range_value" constraint="in abaxial surfaces" constraintid="o2422" from="0.7" from_unit="mm" name="some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2422" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="true" name="character" src="d0_s3" value="diameter" value_original="diameter" />
        <character is_modifier="true" name="arrangement_or_vernation" src="d0_s3" value="conduplicate" value_original="conduplicate" />
        <character is_modifier="true" modifier="sometimes" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="more or less evenly" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o2423" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character constraint="on ribs" constraintid="o2424" is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o2424" name="rib" name_original="ribs" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>cauline blades sometimes to 2.5 mm wide, sometimes flat;</text>
      <biological_entity constraint="cauline" id="o2425" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s4" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>abaxial sclerenchyma in small strands;</text>
      <biological_entity id="o2427" name="strand" name_original="strands" src="d0_s5" type="structure">
        <character is_modifier="true" name="size" src="d0_s5" value="small" value_original="small" />
      </biological_entity>
      <relation from="o2426" id="r409" name="in" negation="false" src="d0_s5" to="o2427" />
    </statement>
    <statement id="d0_s6">
      <text>abaxial sclerenchyma rarely present.</text>
      <biological_entity constraint="abaxial" id="o2426" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="abaxial" value_original="abaxial" />
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 7.5-12 cm, more or less open, usually secund, often partially included in the uppermost leaf-sheaths at maturity;</text>
      <biological_entity id="o2428" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character char_type="range_value" from="7.5" from_unit="cm" name="some_measurement" src="d0_s7" to="12" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s7" value="secund" value_original="secund" />
      </biological_entity>
      <biological_entity constraint="uppermost" id="o2429" name="sheath" name_original="leaf-sheaths" src="d0_s7" type="structure" />
      <relation from="o2428" id="r410" modifier="often partially" name="included in the" negation="false" src="d0_s7" to="o2429" />
    </statement>
    <statement id="d0_s8">
      <text>branches lax, scabrous.</text>
      <biological_entity id="o2430" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s8" value="lax" value_original="lax" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 9.5-13 mm, green to violet, sometimes glaucous, with 4-7 florets.</text>
      <biological_entity id="o2431" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="9.5" from_unit="mm" name="some_measurement" src="d0_s9" to="13" to_unit="mm" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s9" to="violet" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s9" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o2432" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s9" to="7" />
      </biological_entity>
      <relation from="o2431" id="r411" name="with" negation="false" src="d0_s9" to="o2432" />
    </statement>
    <statement id="d0_s10">
      <text>Lower glumes 3.1-4.5 mm;</text>
      <biological_entity constraint="lower" id="o2433" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.1" from_unit="mm" name="some_measurement" src="d0_s10" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes (4.5) 5-5.6 mm;</text>
      <biological_entity constraint="upper" id="o2434" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character name="atypical_some_measurement" src="d0_s11" unit="mm" value="4.5" value_original="4.5" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="5.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lemmas 5.8-6.6 mm, attenuate in side view, mostly green, sometimes with violet borders, glabrous or more or less uniformly hairy, margins and apices scabrous, apices awned, awns 1-5 mm;</text>
      <biological_entity id="o2435" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="5.8" from_unit="mm" name="some_measurement" src="d0_s12" to="6.6" to_unit="mm" />
        <character is_modifier="false" modifier="in side view" name="shape" src="d0_s12" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="mostly" name="coloration" src="d0_s12" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less uniformly" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o2436" name="border" name_original="borders" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="violet" value_original="violet" />
      </biological_entity>
      <biological_entity id="o2437" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s12" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o2438" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s12" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o2439" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o2440" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
      <relation from="o2435" id="r412" modifier="sometimes" name="with" negation="false" src="d0_s12" to="o2436" />
    </statement>
    <statement id="d0_s13">
      <text>anthers 2.8-3.9 mm. 2n = 42.</text>
      <biological_entity id="o2441" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s13" to="3.9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2442" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Festuca rubra subsp. secunda grows on pebble beaches and in soil pockets on rocks, meadows, cliffs, banks, and stabilized sand dunes along seashores with high annual rainfall, on the Pacific coast of North America from Alaska south to Oregon.</discussion>
  
</bio:treatment>