<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">243</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">HORDEUM</taxon_name>
    <taxon_name authority="Nevski" date="unknown" rank="species">brachyantherum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">brachyantherum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus hordeum;species brachyantherum;subspecies brachyantherum</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <other_name type="common_name">Meadow barley</other_name>
  <other_name type="common_name">Northern barley</other_name>
  <other_name type="common_name">Orge a antheres courtes</other_name>
  <other_name type="common_name">Orge des pres</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants densely cespitose.</text>
      <biological_entity id="o19614" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 30-95 cm, often robust, sometimes slender.</text>
      <biological_entity id="o19615" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="95" to_unit="cm" />
        <character is_modifier="false" modifier="often" name="fragility" src="d0_s1" value="robust" value_original="robust" />
        <character is_modifier="false" modifier="sometimes" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal sheaths usually glabrous, sometimes sparsely pubescent;</text>
      <biological_entity constraint="basal" id="o19616" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades to 19 cm long, to 8 mm wide, both sides usually glabrous, sometimes with hairs to 0.5 mm on both surfaces.</text>
      <biological_entity id="o19617" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s3" to="19" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19618" name="side" name_original="sides" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19619" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="on surfaces" constraintid="o19620" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19620" name="surface" name_original="surfaces" src="d0_s3" type="structure" />
      <relation from="o19618" id="r3112" modifier="sometimes" name="with" negation="false" src="d0_s3" to="o19619" />
    </statement>
    <statement id="d0_s4">
      <text>Glumes 7-17 mm, usually straight at maturity;</text>
      <biological_entity id="o19621" name="glume" name_original="glumes" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s4" to="17" to_unit="mm" />
        <character constraint="at maturity" is_modifier="false" modifier="usually" name="course" src="d0_s4" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lemmas usually awned, awns to 6.5 mm, usually straight at maturity;</text>
      <biological_entity id="o19622" name="lemma" name_original="lemmas" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o19623" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="6.5" to_unit="mm" />
        <character constraint="at maturity" is_modifier="false" modifier="usually" name="course" src="d0_s5" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>anthers 0.8-3.5 mm. 2n = 28, 42.</text>
      <biological_entity id="o19624" name="anther" name_original="anthers" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19625" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="28" value_original="28" />
        <character name="quantity" src="d0_s6" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hordeum brachy anther um subsp. brachyantherum grows in pastures and along streams and lake shores, from sea level to 4000 m. Its range extends from Kamchatka through western North America to Baja California, Mexico. It is also known from disjunct locations in Newfoundland and Labrador and the eastern United Sates. The latter are probably recent introductions; the Newfoundland populations are harder to explain. One population from California is known to be hexaploid.</discussion>
  
</bio:treatment>