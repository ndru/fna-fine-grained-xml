<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="treatment_page">146</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">STIPEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">PIPTATHERUM</taxon_name>
    <taxon_name authority="(Torr.) Dorn" date="unknown" rank="species">pungens</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe stipeae;genus piptatherum;species pungens</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oryzopsis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pungens</taxon_name>
    <taxon_hierarchy>genus oryzopsis;species pungens</taxon_hierarchy>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Sharp piptatherum</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, not rhizomatous.</text>
      <biological_entity id="o18835" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 10-90 cm, usually glabrous, occasionally puberulent beneath the nodes;</text>
      <biological_entity id="o18837" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>basal branching intravaginal.</text>
      <biological_entity id="o18836" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="90" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character constraint="beneath nodes" constraintid="o18837" is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="position" src="d0_s2" value="intravaginal" value_original="intravaginal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basally concentrated;</text>
      <biological_entity id="o18838" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="basally" name="arrangement_or_density" src="d0_s3" value="concentrated" value_original="concentrated" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths smooth or somewhat scabrous;</text>
      <biological_entity id="o18839" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="somewhat" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.5-2.5 mm, truncate to acute;</text>
      <biological_entity id="o18840" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s5" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades (6) 18-45 cm long, 0.5-1.8 mm wide, flat to convolute, abaxial surfaces scabridulous to scabrous, adaxial surfaces scabrous.</text>
      <biological_entity id="o18841" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character name="length" src="d0_s6" unit="cm" value="6" value_original="6" />
        <character char_type="range_value" from="18" from_unit="cm" name="length" src="d0_s6" to="45" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s6" to="convolute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o18842" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character char_type="range_value" from="scabridulous" name="relief" src="d0_s6" to="scabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o18843" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 4-6 cm, lower nodes with 1-2 primary branches;</text>
      <biological_entity id="o18844" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s7" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="lower" id="o18845" name="node" name_original="nodes" src="d0_s7" type="structure" />
      <biological_entity constraint="primary" id="o18846" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="2" />
      </biological_entity>
      <relation from="o18845" id="r2979" name="with" negation="false" src="d0_s7" to="o18846" />
    </statement>
    <statement id="d0_s8">
      <text>branches 0.8-4 cm, straight, usually strongly ascending, ascending to divergent at anthesis.</text>
      <biological_entity id="o18847" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s8" to="4" to_unit="cm" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="usually strongly" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character constraint="at anthesis" is_modifier="false" name="arrangement" src="d0_s8" value="divergent" value_original="divergent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Glumes subequal, 3.5-4.5 mm long, 1.4-2 mm wide, from 1 mm shorter than to slightly exceeding the florets, ovate, usually 1-veined, sometimes 3-5-veined near the base, apices rounded or acute;</text>
      <biological_entity id="o18848" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s9" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
        <character constraint="than to slightly exceeding the florets" constraintid="o18849" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
        <character is_modifier="false" modifier="from 1 mm" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s9" value="1-veined" value_original="1-veined" />
        <character constraint="near base" constraintid="o18850" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s9" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
      <biological_entity id="o18849" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="slightly" name="position_relational" src="d0_s9" value="exceeding" value_original="exceeding" />
      </biological_entity>
      <biological_entity id="o18850" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o18851" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>florets 3-4 mm, dorsally compressed;</text>
      <biological_entity id="o18852" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="dorsally" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>calluses 0.2-0.3 mm, rounded, hairy, disarticulation scars circular;</text>
      <biological_entity id="o18853" name="callus" name_original="calluses" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s11" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o18854" name="scar" name_original="scars" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s11" value="circular" value_original="circular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lemmas evenly pubescent, margins not overlapping at maturity;</text>
      <biological_entity id="o18855" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="evenly" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o18856" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character constraint="at maturity" is_modifier="false" modifier="not" name="arrangement" src="d0_s12" value="overlapping" value_original="overlapping" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>awns 1-2 mm, straight, slightly twisted, caducous, often absent even from immature florets;</text>
      <biological_entity id="o18857" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s13" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="duration" src="d0_s13" value="caducous" value_original="caducous" />
        <character constraint="from florets" constraintid="o18858" is_modifier="false" modifier="often" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o18858" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s13" value="immature" value_original="immature" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>paleas equaling or almost equaling the lemma lobes, similar in texture and pubescence to the lemmas;</text>
      <biological_entity id="o18859" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character is_modifier="false" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o18860" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="true" modifier="almost" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o18861" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
      <relation from="o18859" id="r2980" name="to" negation="false" src="d0_s14" to="o18861" />
    </statement>
    <statement id="d0_s15">
      <text>anthers 0.8-1.8 mm, usually not penicillate;</text>
      <biological_entity id="o18862" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s15" to="1.8" to_unit="mm" />
        <character is_modifier="false" modifier="usually not" name="shape" src="d0_s15" value="penicillate" value_original="penicillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovaries with a conelike extension bearing a 2-branched style.</text>
      <biological_entity id="o18863" name="ovary" name_original="ovaries" src="d0_s16" type="structure" />
      <biological_entity id="o18864" name="extension" name_original="extension" src="d0_s16" type="structure">
        <character is_modifier="true" name="shape" src="d0_s16" value="conelike" value_original="conelike" />
      </biological_entity>
      <biological_entity id="o18865" name="style" name_original="style" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="2-branched" value_original="2-branched" />
      </biological_entity>
      <relation from="o18863" id="r2981" name="with" negation="false" src="d0_s16" to="o18864" />
      <relation from="o18864" id="r2982" name="bearing a" negation="false" src="d0_s16" to="o18865" />
    </statement>
    <statement id="d0_s17">
      <text>Caryopses about 1.8 mm long, about 0.9 mm wide;</text>
      <biological_entity id="o18866" name="caryopse" name_original="caryopses" src="d0_s17" type="structure">
        <character name="length" src="d0_s17" unit="mm" value="1.8" value_original="1.8" />
        <character name="width" src="d0_s17" unit="mm" value="0.9" value_original="0.9" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>hila linear, 9/10 as long as to equaling the caryopses.</text>
      <biological_entity id="o18868" name="caryopse" name_original="caryopses" src="d0_s18" type="structure">
        <character is_modifier="true" name="variability" src="d0_s18" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>2n = 22, 24.</text>
      <biological_entity id="o18867" name="hilum" name_original="hila" src="d0_s18" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s18" value="linear" value_original="linear" />
        <character constraint="to caryopses" constraintid="o18868" name="quantity" src="d0_s18" value="9/10" value_original="9/10" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18869" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="22" value_original="22" />
        <character name="quantity" src="d0_s19" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Piptatherum pungens grows in sandy to rocky soils and open habitats, from southern Yukon Territory across Canada to the Great Lakes region and eastern Pennsylvania, and, as a disjunct, in the western Great Plains and the southern Rocky Mountains. Its apparent absence from Idaho and Montana, and almost complete absence from Wyoming, is puzzling. The awns of P. pungens fall off so rapidly that it is sometimes mistaken for Milium or Agrostis, but the only perennial species of Milium in the Flora region has leaf blades 8-17 mm wide, and no species of Agrostis has such stiff lemmas and well-developed paleas. Its deciduous, shorter awns distinguish it from P. canadense.</discussion>
  
</bio:treatment>