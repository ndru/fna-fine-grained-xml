<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">663</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Desf." date="unknown" rank="genus">POLYPOGON</taxon_name>
    <taxon_name authority="(Gouan) Breistr." date="unknown" rank="species">viridis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus polypogon;species viridis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polypogon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">semiverticillatus</taxon_name>
    <taxon_hierarchy>genus polypogon;species semiverticillatus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agrostis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">viridis</taxon_name>
    <taxon_hierarchy>genus agrostis;species viridis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agrostis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">verticillata</taxon_name>
    <taxon_hierarchy>genus agrostis;species verticillata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agrostis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">semiverticillata</taxon_name>
    <taxon_hierarchy>genus agrostis;species semiverticillata</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Water beardgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, often flowering the first-year.</text>
      <biological_entity id="o31309" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="often" name="life_cycle" src="d0_s0" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="structure_subtype" src="d0_s0" value="first-year" value_original="first-year" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 10-90 cm, sometimes decumbent and rooting at the lower nodes.</text>
      <biological_entity id="o31310" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="90" to_unit="cm" />
        <character is_modifier="false" modifier="sometimes" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character constraint="at lower nodes" constraintid="o31311" is_modifier="false" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o31311" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Sheaths glabrous, smooth;</text>
      <biological_entity id="o31312" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules to 5 mm;</text>
      <biological_entity id="o31313" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 2-13 cm long, 1-6 mm wide.</text>
      <biological_entity id="o31314" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="13" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles 2-10 cm, ovate-oblong to pyramidal, dense but interrupted, pale green to purplish;</text>
      <biological_entity id="o31315" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s5" to="10" to_unit="cm" />
        <character char_type="range_value" from="ovate-oblong" name="shape" src="d0_s5" to="pyramidal" />
        <character is_modifier="false" name="density" src="d0_s5" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="interrupted" value_original="interrupted" />
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s5" to="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pedicels not developed;</text>
      <biological_entity id="o31316" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="development" src="d0_s6" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stipes 0.1-0.6 mm.</text>
      <biological_entity id="o31317" name="stipe" name_original="stipes" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s7" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Glumes 1.5-2 mm, scabrous on the back and keel, apices obtuse or truncate, unawned;</text>
      <biological_entity id="o31318" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
        <character constraint="on keel" constraintid="o31320" is_modifier="false" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o31319" name="back" name_original="back" src="d0_s8" type="structure" />
      <biological_entity id="o31320" name="keel" name_original="keel" src="d0_s8" type="structure" />
      <biological_entity id="o31321" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s8" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lemmas about 1 mm, erose, unawned;</text>
      <biological_entity id="o31322" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s9" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>paleas subequal to the lemmas;</text>
      <biological_entity id="o31323" name="palea" name_original="paleas" src="d0_s10" type="structure">
        <character constraint="to lemmas" constraintid="o31324" is_modifier="false" name="size" src="d0_s10" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o31324" name="lemma" name_original="lemmas" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>anthers 0.3-0.5 mm. 2n = 28, 42.</text>
      <biological_entity id="o31325" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s11" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31326" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="28" value_original="28" />
        <character name="quantity" src="d0_s11" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Polypogon viridis grows in mesic habitats associated with rivers, streams, and irrigation ditches. It is native from southern Europe to Pakistan, but is now established in the Flora region, particularly the southwestern United States. Records from the Atlantic coast are based on plants found on ballast dumps; there have been no recent collections from these locations.</discussion>
  <discussion>In Europe, Polypogon viridis hybridizes with P. monspeliensis, forming P. xadscendens Guss. ex Bertol.; no such hybrids have been reported from the Flora region.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Colo.;N.Mex.;Tex.;B.C.;N.J.;Va.;Calif.;Pacific Islands (Hawaii);Wyo.;Utah;S.C.;Okla.;Wash.;Ariz.;Oreg.;Nev.;Conn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>