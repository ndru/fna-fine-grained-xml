<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">295</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ELYMUS</taxon_name>
    <taxon_name authority="R. Brooks &amp; J.J.N. Campb." date="unknown" rank="species">macgregorii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus elymus;species macgregorii</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Early wildrye</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, not rhizomatous, usually glaucous.</text>
      <biological_entity id="o12839" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 40-120 cm, erect or slightly decumbent;</text>
      <biological_entity id="o12840" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s1" to="120" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes 4-8, mostly exposed, glabrous.</text>
      <biological_entity id="o12841" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s2" to="8" />
        <character is_modifier="false" modifier="mostly" name="prominence" src="d0_s2" value="exposed" value_original="exposed" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves even¬ly distributed;</text>
      <biological_entity id="o12842" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="ly" name="arrangement" src="d0_s3" value="distributed" value_original="distributed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths usually glabrous, rarely villous;</text>
      <biological_entity id="o12843" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>auricles 2-3 mm, usually purplish black when fresh, sometimes light-brown;</text>
      <biological_entity id="o12844" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="when fresh" name="coloration" src="d0_s5" value="purplish black" value_original="purplish black" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s5" value="light-brown" value_original="light-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules shorter than 1 mm;</text>
      <biological_entity id="o12845" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s6" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 7-15 mm wide, lax, dark glossy green under the glaucous bloom, adaxial surfaces usually glabrous, occasionally villous.</text>
      <biological_entity id="o12846" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s7" to="15" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="lax" value_original="lax" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="dark" value_original="dark" />
        <character is_modifier="false" name="reflectance" src="d0_s7" value="glossy" value_original="glossy" />
        <character constraint="under bloom" constraintid="o12847" is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o12847" name="bloom" name_original="bloom" src="d0_s7" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s7" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o12848" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s7" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikes 4-12 cm long, (1.7) 2.2-3 (4) 4 cm wide, erect, exserted, with (6) 9-16 (20) nodes and 2 spikelets at all or most nodes, sometimes with 3 at some nodes;</text>
      <biological_entity id="o12849" name="spike" name_original="spikes" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s8" to="12" to_unit="cm" />
        <character name="width" src="d0_s8" unit="cm" value="1.7" value_original="1.7" />
        <character char_type="range_value" from="2.2" from_unit="cm" name="width" src="d0_s8" to="34" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="position" src="d0_s8" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o12850" name="node" name_original="nodes" src="d0_s8" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="6" value_original="6" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="20" value_original="20" />
        <character char_type="range_value" from="9" is_modifier="true" name="quantity" src="d0_s8" to="16" />
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o12851" name="spikelet" name_original="spikelets" src="d0_s8" type="structure" />
      <biological_entity id="o12852" name="node" name_original="nodes" src="d0_s8" type="structure">
        <character constraint="at nodes" constraintid="o12853" modifier="with" name="quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o12853" name="node" name_original="nodes" src="d0_s8" type="structure" />
      <relation from="o12849" id="r2050" name="with" negation="false" src="d0_s8" to="o12850" />
      <relation from="o12851" id="r2051" name="at" negation="false" src="d0_s8" to="o12852" />
    </statement>
    <statement id="d0_s9">
      <text>internodes 4-7 mm long, about 0.3 mm thick and 2-angled at the thinnest sections, usually glabrous or scabridulous beneath the spikelets.</text>
      <biological_entity id="o12854" name="internode" name_original="internodes" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="7" to_unit="mm" />
        <character name="thickness" src="d0_s9" unit="mm" value="0.3" value_original="0.3" />
        <character constraint="at sections" constraintid="o12855" is_modifier="false" name="shape" src="d0_s9" value="2-angled" value_original="2-angled" />
        <character is_modifier="false" modifier="usually" name="pubescence" notes="" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character constraint="beneath spikelets" constraintid="o12856" is_modifier="false" name="relief" src="d0_s9" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o12855" name="section" name_original="sections" src="d0_s9" type="structure" />
      <biological_entity id="o12856" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 10-15 mm, strongly divergent, glaucous, maturing to pale yellowish-brown, with (2) 3-4 florets, lowest florets functional;</text>
      <biological_entity id="o12857" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="strongly" name="arrangement" src="d0_s10" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="life_cycle" src="d0_s10" value="maturing" value_original="maturing" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pale yellowish-brown" value_original="pale yellowish-brown" />
      </biological_entity>
      <biological_entity id="o12858" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s10" value="2" value_original="2" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s10" to="4" />
      </biological_entity>
      <relation from="o12857" id="r2052" name="with" negation="false" src="d0_s10" to="o12858" />
    </statement>
    <statement id="d0_s11">
      <text>disarticulation below the glumes and each floret, the lowest floret often falling with the glumes.</text>
      <biological_entity constraint="lowest" id="o12859" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="false" name="function" src="d0_s10" value="functional" value_original="functional" />
      </biological_entity>
      <biological_entity id="o12860" name="glume" name_original="glumes" src="d0_s11" type="structure" />
      <biological_entity id="o12861" name="floret" name_original="floret" src="d0_s11" type="structure" />
      <biological_entity constraint="lowest" id="o12862" name="floret" name_original="floret" src="d0_s11" type="structure">
        <character constraint="with glumes" constraintid="o12863" is_modifier="false" modifier="often" name="life_cycle" src="d0_s11" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity id="o12863" name="glume" name_original="glumes" src="d0_s11" type="structure" />
      <relation from="o12859" id="r2053" name="below" negation="false" src="d0_s11" to="o12860" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes subequal, entire, the basal 1-3 mm terete or subterete, indurate, without evident venation, moderately bowed out, glume bodies 8-16 mm long, 1-1.8 mm wide, linear-lanceolate, widening or parallel-sided above the base, (2) 4-5 (8) -veined, usually glabrous, occasionally hirsute, sometimes scabrous, margins firm, awns (10) 15-20 (25) mm, straight except the awns of the lowest spikelets occasionally contorted;</text>
      <biological_entity id="o12864" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
        <character is_modifier="false" name="position" src="d0_s12" value="basal" value_original="basal" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subterete" value_original="subterete" />
        <character is_modifier="false" name="texture" src="d0_s12" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="width" src="d0_s12" value="widening" value_original="widening" />
        <character constraint="above base" constraintid="o12866" is_modifier="false" name="architecture" src="d0_s12" value="parallel-sided" value_original="parallel-sided" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s12" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="sometimes" name="pubescence_or_relief" src="d0_s12" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="glume" id="o12865" name="body" name_original="bodies" src="d0_s12" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s12" value="evident" value_original="evident" />
        <character is_modifier="true" modifier="moderately" name="shape" src="d0_s12" value="bowed" value_original="bowed" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s12" to="16" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" notes="" src="d0_s12" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12866" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s12" value="(2)4-5(8)-veined" value_original="(2)4-5(8)-veined" />
      </biological_entity>
      <biological_entity id="o12867" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="texture" src="d0_s12" value="firm" value_original="firm" />
      </biological_entity>
      <biological_entity id="o12868" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="mm" value="10" value_original="10" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s12" to="20" to_unit="mm" />
        <character constraint="except awns" constraintid="o12869" is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o12869" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="occasionally" name="arrangement_or_shape" src="d0_s12" value="contorted" value_original="contorted" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o12870" name="spikelet" name_original="spikelets" src="d0_s12" type="structure" />
      <relation from="o12864" id="r2054" name="without" negation="false" src="d0_s12" to="o12865" />
      <relation from="o12869" id="r2055" name="part_of" negation="false" src="d0_s12" to="o12870" />
    </statement>
    <statement id="d0_s13">
      <text>lemmas 6-12 mm, usually glabrous, sometimes scabrous, occasionally villous, awns (15) 20-30 mm, straight;</text>
      <biological_entity id="o12871" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="12" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence_or_relief" src="d0_s13" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s13" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o12872" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="15" value_original="15" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s13" to="30" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>paleas 6-10 mm, apices obtuse;</text>
      <biological_entity id="o12873" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12874" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 2-4 mm.</text>
    </statement>
    <statement id="d0_s16">
      <text>Anthesis usually mid-may to mid-june.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 28.</text>
      <biological_entity id="o12875" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="life_cycle" src="d0_s16" value="anthesis" value_original="anthesis" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12876" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Elymus macgregorii grows in moist, deep, alluvial or residual, calcareous or other base-rich soils in woods and thickets, mostly east of the 100th Meridian in the contiguous United States. It used to be confused with E. glabriflorus (p. 296) or E. virginicus (p. 298), but it reaches anthesis about a month earlier than sympatric populations of these species. In most of its range, E. macgregorii has purplish black auricles; light brown auricles may be locally abundant, particularly in populations at the limits of its range.</discussion>
  <discussion>Elymus macregorii hybridizes with several species, but especially E. virginicus and E. hystrix (p. 316) (Campbell 2000). Western plants often have smaller, more condensed spikes and distinctly villous leaves, suggesting a transition to E. virginicus var. jejunus (p. 300). Transitions to E. virginicus var. jejunus can also be recognized to the north, where the dates of anthesis are delayed, but even in Maine, E. macgregorii reaches anthesis about 10 days earlier than E. virginicus (Campbell and Haines 2002). Plants with villous lemmas grow at scattered locations; they have not been reported in distinct habitats, nor in large enough populations to warrant taxonomic recognition.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.;Ky.;Del.;Ala.;Ark.;Conn.;D.C.;Fla.;Ga.;Iowa;Ill.;Ind.;Kans.;La.;Mass.;Md.;Maine;Mo.;Miss.;N.C.;N.Dak.;Nebr.;N.H.;N.S.;N.Y.;Ohio;Okla.;Pa.;R.I.;S.Dak.;Tenn.;Tex.;Va.;Vt.;Wis.;W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>