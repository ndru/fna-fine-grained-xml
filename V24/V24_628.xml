<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">440</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">FESTUCA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Festuca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Festuca</taxon_name>
    <taxon_name authority="Vasey" date="unknown" rank="species">viridula</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus festuca;subgenus festuca;section festuca;species viridula</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>42</number>
  <other_name type="common_name">Mountain bunchgrass</other_name>
  <other_name type="common_name">Greenleaf fescue</other_name>
  <other_name type="common_name">Green fescue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants loosely or densely cespitose, without rhizomes.</text>
      <biological_entity id="o8817" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8818" name="rhizom" name_original="rhizomes" src="d0_s0" type="structure" />
      <relation from="o8817" id="r1421" name="without" negation="false" src="d0_s0" to="o8818" />
    </statement>
    <statement id="d0_s1">
      <text>Culms 35-80 (100) cm, smooth, glabrous throughout;</text>
      <biological_entity id="o8819" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="100" value_original="100" />
        <character char_type="range_value" from="35" from_unit="cm" name="some_measurement" src="d0_s1" to="80" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes usually not exposed.</text>
      <biological_entity id="o8820" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually not" name="prominence" src="d0_s2" value="exposed" value_original="exposed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths closed for less than 1/2 their length, usually glabrous, sometimes pubescent, strongly reined, persistent or slowly shredding into fibers;</text>
      <biological_entity id="o8821" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="length" src="d0_s3" value="closed" value_original="closed" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="strongly" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character name="duration" src="d0_s3" value="slowly" value_original="slowly" />
      </biological_entity>
      <biological_entity id="o8822" name="fiber" name_original="fibers" src="d0_s3" type="structure" />
      <relation from="o8821" id="r1422" name="shredding into" negation="false" src="d0_s3" to="o8822" />
    </statement>
    <statement id="d0_s4">
      <text>collars glabrous;</text>
      <biological_entity id="o8823" name="collar" name_original="collars" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules (0.2) 0.3-0.8 (1) mm;</text>
      <biological_entity id="o8824" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character name="atypical_some_measurement" src="d0_s5" unit="mm" value="0.2" value_original="0.2" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s5" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 0.5-1.3 mm in diameter when conduplicate, to 2.5 mm wide when flat, persistent, abaxial surfaces glabrous and smooth, adaxial surfaces scabrous or pubescent, veins 5-9 (12), ribs 5-9, blades of the lower cauline leaves usually reduced to stiff horny points, blades of the upper cauline leaves longer and more flexuous;</text>
      <biological_entity id="o8825" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" modifier="when conduplicate" name="diameter" src="d0_s6" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" modifier="when flat" name="width" src="d0_s6" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8826" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o8827" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o8828" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character name="atypical_quantity" src="d0_s6" value="12" value_original="12" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="9" />
      </biological_entity>
      <biological_entity id="o8829" name="rib" name_original="ribs" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="9" />
      </biological_entity>
      <biological_entity id="o8830" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="lower cauline" id="o8831" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o8832" name="point" name_original="points" src="d0_s6" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s6" value="stiff" value_original="stiff" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="horny" value_original="horny" />
      </biological_entity>
      <biological_entity id="o8833" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
        <character is_modifier="false" name="course" src="d0_s6" value="flexuous" value_original="flexuous" />
      </biological_entity>
      <biological_entity constraint="upper cauline" id="o8834" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o8830" id="r1423" name="part_of" negation="false" src="d0_s6" to="o8831" />
      <relation from="o8833" id="r1424" name="part_of" negation="false" src="d0_s6" to="o8834" />
    </statement>
    <statement id="d0_s7">
      <text>abaxial sclerenchyma in strands about as wide as the adjacent veins;</text>
      <biological_entity id="o8836" name="strand" name_original="strands" src="d0_s7" type="structure" />
      <biological_entity id="o8837" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s7" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <relation from="o8835" id="r1425" name="in" negation="false" src="d0_s7" to="o8836" />
      <relation from="o8836" id="r1426" name="as wide as" negation="false" src="d0_s7" to="o8837" />
    </statement>
    <statement id="d0_s8">
      <text>adaxial sclerenchyma developed;</text>
      <biological_entity constraint="abaxial" id="o8835" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="adaxial" value_original="adaxial" />
        <character is_modifier="false" name="development" src="d0_s8" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pillars and girders often present.</text>
      <biological_entity id="o8838" name="whole-organism" name_original="" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Inflorescences (4) 8-15 cm, open or somewhat contracted, with 1-2 branches per node;</text>
      <biological_entity id="o8839" name="inflorescence" name_original="inflorescences" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="cm" value="4" value_original="4" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s10" to="15" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="open" value_original="open" />
        <character is_modifier="false" modifier="somewhat" name="condition_or_size" src="d0_s10" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o8840" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s10" to="2" />
      </biological_entity>
      <biological_entity id="o8841" name="node" name_original="node" src="d0_s10" type="structure" />
      <relation from="o8839" id="r1427" name="with" negation="false" src="d0_s10" to="o8840" />
      <relation from="o8840" id="r1428" name="per" negation="false" src="d0_s10" to="o8841" />
    </statement>
    <statement id="d0_s11">
      <text>branches lax, spreading or loosely erect, lower branches with 2+ spikelets.</text>
      <biological_entity id="o8842" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s11" value="lax" value_original="lax" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="loosely" name="orientation" src="d0_s11" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity constraint="lower" id="o8843" name="branch" name_original="branches" src="d0_s11" type="structure" />
      <biological_entity id="o8844" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s11" upper_restricted="false" />
      </biological_entity>
      <relation from="o8843" id="r1429" name="with" negation="false" src="d0_s11" to="o8844" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 9-15 mm, with (2) 3-6 (7) florets.</text>
      <biological_entity id="o8845" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s12" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8846" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s12" value="7" value_original="7" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s12" to="6" />
      </biological_entity>
      <relation from="o8845" id="r1430" name="with" negation="false" src="d0_s12" to="o8846" />
    </statement>
    <statement id="d0_s13">
      <text>Glumes exceeded by the upper florets, ovatelanceolate to lanceolate, glabrous, smooth or scabridulous distally;</text>
      <biological_entity id="o8847" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="ovatelanceolate" name="shape" notes="" src="d0_s13" to="lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="distally" name="relief" src="d0_s13" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity constraint="upper" id="o8848" name="floret" name_original="florets" src="d0_s13" type="structure" />
      <relation from="o8847" id="r1431" name="exceeded by" negation="false" src="d0_s13" to="o8848" />
    </statement>
    <statement id="d0_s14">
      <text>lower glumes (2.4) 2.8-5 mm, distinctly shorter than the adjacent lemmas;</text>
      <biological_entity constraint="lower" id="o8849" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="2.4" value_original="2.4" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
        <character constraint="than the adjacent lemmas" constraintid="o8850" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="distinctly shorter" value_original="distinctly shorter" />
      </biological_entity>
      <biological_entity id="o8850" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s14" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper glumes 4.5-7 (8.5) mm;</text>
      <biological_entity constraint="upper" id="o8851" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="8.5" value_original="8.5" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s15" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lemmas (4.8) 6-8.5 mm, lanceolate to ovatelanceolate, glabrous, smooth or slightly scabrous, apices acute, unawned or awned, awns 0.2-1.5 (2) mm;</text>
      <biological_entity id="o8852" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character name="atypical_some_measurement" src="d0_s16" unit="mm" value="4.8" value_original="4.8" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s16" to="8.5" to_unit="mm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s16" to="ovatelanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="pubescence_or_relief" src="d0_s16" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o8853" name="apex" name_original="apices" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o8854" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character name="atypical_some_measurement" src="d0_s16" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>paleas about as long as the lemmas, intercostal region scabrous or puberulent distally;</text>
      <biological_entity id="o8855" name="palea" name_original="paleas" src="d0_s17" type="structure" />
      <biological_entity id="o8856" name="lemma" name_original="lemmas" src="d0_s17" type="structure" />
      <biological_entity constraint="intercostal" id="o8857" name="region" name_original="region" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s17" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <relation from="o8855" id="r1432" name="as long as" negation="false" src="d0_s17" to="o8856" />
    </statement>
    <statement id="d0_s18">
      <text>anthers (2) 2.5-4 (5) mm;</text>
      <biological_entity id="o8858" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character name="atypical_some_measurement" src="d0_s18" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s18" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovary apices densely pubescent.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 28.</text>
      <biological_entity constraint="ovary" id="o8859" name="apex" name_original="apices" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s19" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8860" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Festuca viridula grows in low alpine and subalpine meadows, forest openings, and open forests, at (900)1500-3000 m, from southern British Columbia east to Montana and south to central California and Nevada. It is highly palatable to livestock, and is an important forage species in some areas.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Wash.;B.C.;Calif.;Idaho;Mont.;Oreg.;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>