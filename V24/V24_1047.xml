<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">739</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">AVENA</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">sterilis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus avena;species sterilis</taxon_hierarchy>
  </taxon_identification>
  <number>6</number>
  <other_name type="common_name">Animated oats</other_name>
  <other_name type="common_name">Avoine sterile</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o23299" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 30-120 cm, initially prostrate, becoming erect at maturity.</text>
      <biological_entity id="o23300" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="120" to_unit="cm" />
        <character is_modifier="false" modifier="initially" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character constraint="at maturity" is_modifier="false" modifier="becoming" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths glabrous or hairy;</text>
      <biological_entity id="o23301" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules 3-8 mm, acute to truncate-mucronate;</text>
      <biological_entity id="o23302" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="8" to_unit="mm" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="truncate-mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 8-60 cm long, 4-18 mm wide, scabridulous, often ciliolate on the margins.</text>
      <biological_entity id="o23303" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s4" to="60" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="18" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s4" value="scabridulous" value_original="scabridulous" />
        <character constraint="on margins" constraintid="o23304" is_modifier="false" modifier="often" name="pubescence" src="d0_s4" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o23304" name="margin" name_original="margins" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Panicles 10-45 cm long, 5-25 cm wide.</text>
      <biological_entity id="o23305" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s5" to="45" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s5" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spikelets 24-50 mm, with 2-5 florets;</text>
      <biological_entity id="o23307" name="floret" name_original="florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="5" />
      </biological_entity>
      <relation from="o23306" id="r3699" name="with" negation="false" src="d0_s6" to="o23307" />
    </statement>
    <statement id="d0_s7">
      <text>disarticulation beneath the basal floret the florets falling as a unit;</text>
      <biological_entity id="o23306" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="24" from_unit="mm" name="some_measurement" src="d0_s6" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o23308" name="floret" name_original="floret" src="d0_s7" type="structure" />
      <biological_entity id="o23310" name="unit" name_original="unit" src="d0_s7" type="structure" />
      <relation from="o23306" id="r3700" name="beneath" negation="false" src="d0_s7" to="o23308" />
    </statement>
    <statement id="d0_s8">
      <text>disarticulation scar oval to round-elliptic.</text>
      <biological_entity id="o23309" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character constraint="as unit" constraintid="o23310" is_modifier="false" name="life_cycle" src="d0_s7" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity id="o23311" name="scar" name_original="scar" src="d0_s8" type="structure">
        <character char_type="range_value" from="oval" name="shape" src="d0_s8" to="round-elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Glumes subequal, 20-50 mm, 9-11-veined;</text>
      <biological_entity id="o23312" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s9" to="50" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="9-11-veined" value_original="9-11-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>calluses bearded, hairs to 1/5 the length of the lemmas;</text>
      <biological_entity id="o23313" name="callus" name_original="calluses" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="bearded" value_original="bearded" />
      </biological_entity>
      <biological_entity id="o23314" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s10" to="1/5" />
      </biological_entity>
      <biological_entity id="o23315" name="lemma" name_original="lemmas" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>lemmas 17-40 mm, usually densely strigose below midlength, sometimes sparsely strigose, glabrous or scabridulous, apices bidentate to bisubulate teeth 1-1.5 mm, awns 30-90 mm, arising in the middle 1/3;</text>
      <biological_entity id="o23316" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s11" to="40" to_unit="mm" />
        <character constraint="below midlength" is_modifier="false" modifier="usually densely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s11" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o23317" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="bidentate" value_original="bidentate" />
      </biological_entity>
      <biological_entity id="o23318" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23319" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s11" to="90" to_unit="mm" />
        <character constraint="in middle" constraintid="o23320" is_modifier="false" name="orientation" src="d0_s11" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o23320" name="middle" name_original="middle" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="1/3" value_original="1/3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lodicules without a lobe on the wing;</text>
      <biological_entity id="o23321" name="lodicule" name_original="lodicules" src="d0_s12" type="structure" />
      <biological_entity id="o23322" name="lobe" name_original="lobe" src="d0_s12" type="structure" />
      <biological_entity id="o23323" name="wing" name_original="wing" src="d0_s12" type="structure" />
      <relation from="o23321" id="r3701" name="without" negation="false" src="d0_s12" to="o23322" />
      <relation from="o23322" id="r3702" name="on" negation="false" src="d0_s12" to="o23323" />
    </statement>
    <statement id="d0_s13">
      <text>anthers 2.5-4 mm. 2n = 42.</text>
      <biological_entity id="o23324" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23325" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Avena sterilis is native from the Mediterranean region to Afghanistan; it now grows on all continents. It has become naturalized in California, where it can be found in fields, vineyards, orchards, and on hillsides. It has been reported from Oregon, but no specimens could be found to substantiate the report. Dore &amp; McNeill (1980) also report it from Ottawa and Guelph, Ontario. It is listed as a noxious weed by the U. S. Department of Agriculture.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.;Que.;Calif.;N.J.;Pa.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>