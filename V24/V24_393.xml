<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">274</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché, Hana Pazdírková, and Christine Roberts</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">TRITICUM</taxon_name>
    <taxon_name authority="Nevski" date="unknown" rank="species">carthlicum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus triticum;species carthlicum</taxon_hierarchy>
  </taxon_identification>
  <number>10</number>
  <other_name type="common_name">Persian wheat</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 60-100 cm;</text>
      <biological_entity id="o3182" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>nodes glabrous or pubescent;</text>
      <biological_entity id="o3183" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes mostly hollow, solid for 1 cm below the spikes.</text>
      <biological_entity id="o3184" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s2" value="hollow" value_original="hollow" />
        <character constraint="below spikes" constraintid="o3185" is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
      </biological_entity>
      <biological_entity id="o3185" name="spike" name_original="spikes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Blades to 10 mm wide, puberulent.</text>
      <biological_entity id="o3186" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s3" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikes 8-16 cm, thicker than wide to about as thick as wide, not branched at the base;</text>
      <biological_entity id="o3187" name="spike" name_original="spikes" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s4" to="16" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s4" value="thicker than wide to about as-thick-as wide" />
        <character constraint="at base" constraintid="o3188" is_modifier="false" modifier="not" name="architecture" src="d0_s4" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o3188" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>rachises glabrous or shortly ciliate at the nodes and margins, not disarticulating.</text>
      <biological_entity id="o3189" name="rachis" name_original="rachises" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character constraint="at margins" constraintid="o3191" is_modifier="false" modifier="shortly" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s5" value="disarticulating" value_original="disarticulating" />
      </biological_entity>
      <biological_entity id="o3190" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <biological_entity id="o3191" name="margin" name_original="margins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Spikelets 10-15 mm, with 3-5 florets, 2-4 seed-forming.</text>
      <biological_entity id="o3192" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3193" name="floret" name_original="florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="5" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="4" />
      </biological_entity>
      <relation from="o3192" id="r511" name="with" negation="false" src="d0_s6" to="o3193" />
    </statement>
    <statement id="d0_s7">
      <text>Glumes 7-9 mm, coriaceous, loosely appressed to the lower florets, with 1 prominent keel, terminating in an awn, awns 1-6 cm;</text>
      <biological_entity id="o3194" name="glume" name_original="glumes" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s7" value="coriaceous" value_original="coriaceous" />
        <character constraint="to lower florets" constraintid="o3195" is_modifier="false" modifier="loosely" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity constraint="lower" id="o3195" name="floret" name_original="florets" src="d0_s7" type="structure" />
      <biological_entity id="o3196" name="keel" name_original="keel" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
        <character is_modifier="true" name="prominence" src="d0_s7" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o3197" name="awn" name_original="awn" src="d0_s7" type="structure" />
      <biological_entity id="o3198" name="awn" name_original="awns" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="6" to_unit="cm" />
      </biological_entity>
      <relation from="o3194" id="r512" name="with" negation="false" src="d0_s7" to="o3196" />
      <relation from="o3194" id="r513" name="terminating in" negation="false" src="d0_s7" to="o3197" />
    </statement>
    <statement id="d0_s8">
      <text>lemmas 8.5-12.5 mm, lower 2 lemmas awned, awns to 13 cm;</text>
      <biological_entity id="o3199" name="lemma" name_original="lemmas" src="d0_s8" type="structure">
        <character char_type="range_value" from="8.5" from_unit="mm" name="some_measurement" src="d0_s8" to="12.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lower" id="o3200" name="lemma" name_original="lemmas" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o3201" name="lemma" name_original="lemmas" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o3202" name="awn" name_original="awns" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s8" to="13" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>paleas not splitting at maturity.</text>
      <biological_entity id="o3203" name="palea" name_original="paleas" src="d0_s9" type="structure">
        <character constraint="at maturity" is_modifier="false" modifier="not" name="architecture_or_dehiscence" src="d0_s9" value="splitting" value_original="splitting" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Caryopses red;</text>
      <biological_entity id="o3204" name="caryopse" name_original="caryopses" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>endosperm flinty.</text>
    </statement>
    <statement id="d0_s12">
      <text>Haplomes AuB.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 28.</text>
      <biological_entity id="o3205" name="endosperm" name_original="endosperm" src="d0_s11" type="structure" />
      <biological_entity constraint="2n" id="o3206" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Triticum carthlicum is of evolutionary interest because, morphologically, its spikes resemble those of T. aestivum rather than those of free-threshing tetraploid wheats such as T. durum, T. turgidum, and T. polonicum. It is still occasionally cultivated in Georgia, Armenia, Azerbaijan, northern Iraq, and Iran because of its resistance to drought, frost, and ergot infection. A morphologically similar form of T. aestivum with awned glumes, known as 'carthlicoides', is often found intermixed with T. carthlicum.</discussion>
  
</bio:treatment>