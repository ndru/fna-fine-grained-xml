<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">218</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">BROMEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">BROMUS</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="section">Bromopsis</taxon_name>
    <taxon_name authority="Shear" date="unknown" rank="species">pacificus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe bromeae;genus bromus;section bromopsis;species pacificus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bromopsis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pacifica</taxon_name>
    <taxon_hierarchy>genus bromopsis;species pacifica</taxon_hierarchy>
  </taxon_identification>
  <number>24</number>
  <other_name type="common_name">Pacific brome</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>not rhizomatous.</text>
      <biological_entity id="o21719" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 60-170 cm, erect;</text>
      <biological_entity id="o21720" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s2" to="170" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes (5) 6-8, pubescent;</text>
      <biological_entity id="o21721" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character name="atypical_quantity" src="d0_s3" value="5" value_original="5" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s3" to="8" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>internodes usually glabrous, sometimes pubescent near the nodes.</text>
      <biological_entity id="o21722" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character constraint="near nodes" constraintid="o21723" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o21723" name="node" name_original="nodes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Sheaths pilose, midrib of the culm leaves not abruptly narrowed just below the collar;</text>
      <biological_entity id="o21724" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o21725" name="midrib" name_original="midrib" src="d0_s5" type="structure">
        <character constraint="just below collar" constraintid="o21727" is_modifier="false" modifier="not abruptly" name="shape" src="d0_s5" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity constraint="culm" id="o21726" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o21727" name="collar" name_original="collar" src="d0_s5" type="structure" />
      <relation from="o21725" id="r3426" name="part_of" negation="false" src="d0_s5" to="o21726" />
    </statement>
    <statement id="d0_s6">
      <text>auricles absent;</text>
      <biological_entity id="o21728" name="auricle" name_original="auricles" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules 2-4 mm, glabrous, truncate, erose or lacerate;</text>
      <biological_entity id="o21729" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s7" value="erose" value_original="erose" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blades 20-35 (37) mm long, 6-16 mm wide, flat, abaxial surfaces glabrous, adaxial surfaces pilose.</text>
      <biological_entity id="o21730" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character name="length" src="d0_s8" unit="mm" value="37" value_original="37" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s8" to="35" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s8" to="16" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o21731" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o21732" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Panicles 10-25 cm, open, nodding;</text>
      <biological_entity id="o21733" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s9" to="25" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>branches ascending, spreading, or drooping.</text>
      <biological_entity id="o21734" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="drooping" value_original="drooping" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="drooping" value_original="drooping" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 20-30 mm, elliptic to lanceolate, terete to moderately laterally compressed, with (4) 6-10 florets.</text>
      <biological_entity id="o21735" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s11" to="30" to_unit="mm" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s11" to="lanceolate terete" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s11" to="lanceolate terete" />
      </biological_entity>
      <biological_entity id="o21736" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s11" value="4" value_original="4" />
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s11" to="10" />
      </biological_entity>
      <relation from="o21735" id="r3427" name="with" negation="false" src="d0_s11" to="o21736" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes pubescent;</text>
      <biological_entity id="o21737" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 6-8.5 mm, 1 (3) -veined;</text>
      <biological_entity constraint="lower" id="o21738" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="8.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="1(3)-veined" value_original="1(3)-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 8-11.5 mm, 3-veined, not mucronate;</text>
      <biological_entity constraint="upper" id="o21739" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s14" to="11.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s14" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 10-12 mm, lanceolate, rounded over the midvein, backs pubescent, margins more densely so, apices acute, entire;</text>
      <biological_entity id="o21740" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s15" to="12" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character constraint="over midvein" constraintid="o21741" is_modifier="false" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o21741" name="midvein" name_original="midvein" src="d0_s15" type="structure" />
      <biological_entity id="o21742" name="back" name_original="backs" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o21743" name="margin" name_original="margins" src="d0_s15" type="structure" />
      <biological_entity id="o21744" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="densely" name="shape" src="d0_s15" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>awns 3.5-7 mm, straight, arising less than 1.5 mm below the lemma apices;</text>
      <biological_entity id="o21745" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s16" to="7" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="arising" value_original="arising" />
        <character char_type="range_value" constraint="below lemma apices" constraintid="o21746" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o21746" name="apex" name_original="apices" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>anthers 2-4 mm. 2n = 28.</text>
      <biological_entity id="o21747" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s17" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21748" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bromus pacificus grows in moist thickets, openings, and ravines along the Pacific coast from southeastern Alaska to northern California, with a few occurrences further inland.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska;Oreg.;Wash.;B.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>