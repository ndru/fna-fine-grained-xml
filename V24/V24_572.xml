<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">407</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">FESTUCA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Festuca</taxon_name>
    <taxon_name authority="Krivot." date="unknown" rank="section">Breviaristatae</taxon_name>
    <taxon_name authority="Trin." date="unknown" rank="species">altaica</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus festuca;subgenus festuca;section breviaristatae;species altaica</taxon_hierarchy>
  </taxon_identification>
  <number>8</number>
  <other_name type="common_name">Northern rough fescue</other_name>
  <other_name type="common_name">Altai fescue</other_name>
  <other_name type="common_name">Fétuque d'altai</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants densely cespitose, rarely with short rhizomes.</text>
      <biological_entity id="o15124" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o15125" name="rhizom" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
      </biological_entity>
      <relation from="o15124" id="r2410" modifier="rarely" name="with" negation="false" src="d0_s0" to="o15125" />
    </statement>
    <statement id="d0_s1">
      <text>Culms (25) 30-90 (120) cm, glabrous or slightly scabrous;</text>
      <biological_entity id="o15126" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="25" value_original="25" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="90" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s1" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes usually not exposed.</text>
      <biological_entity id="o15127" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually not" name="prominence" src="d0_s2" value="exposed" value_original="exposed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths closed for less than 1/3 their length, glabrous or scabrous, persistent, not shredding into fibers;</text>
      <biological_entity id="o15128" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="length" src="d0_s3" value="closed" value_original="closed" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o15129" name="fiber" name_original="fibers" src="d0_s3" type="structure" />
      <relation from="o15128" id="r2411" name="shredding into" negation="true" src="d0_s3" to="o15129" />
    </statement>
    <statement id="d0_s4">
      <text>collars glabrous;</text>
      <biological_entity id="o15130" name="collar" name_original="collars" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.2-0.6 (1) mm;</text>
      <biological_entity id="o15131" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character name="atypical_some_measurement" src="d0_s5" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades deciduous, 2-4 mm wide, convolute, conduplicate, sometimes flat, 1-2.5 mm in diameter when conduplicate, yellow-green to dark green, abaxial surfaces scabrous, adaxial surfaces glabrous or pubescent, smooth or scabrous, veins 7-15 (17), ribs 5-9;</text>
      <biological_entity id="o15132" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s6" value="convolute" value_original="convolute" />
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s6" value="conduplicate" value_original="conduplicate" />
        <character is_modifier="false" modifier="sometimes" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character char_type="range_value" constraint="in veins" constraintid="o15136" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o15134" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="true" name="character" src="d0_s6" value="diameter" value_original="diameter" />
        <character is_modifier="true" name="arrangement_or_vernation" src="d0_s6" value="conduplicate" value_original="conduplicate" />
        <character char_type="range_value" from="yellow-green" is_modifier="true" name="coloration" src="d0_s6" to="dark green" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o15135" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o15136" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character is_modifier="true" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character name="atypical_quantity" src="d0_s6" value="17" value_original="17" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s6" to="15" />
      </biological_entity>
      <biological_entity id="o15137" name="rib" name_original="ribs" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>abaxial sclerenchyma in strands about as wide as the adjacent veins;</text>
      <biological_entity id="o15139" name="strand" name_original="strands" src="d0_s7" type="structure" />
      <biological_entity id="o15140" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s7" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <relation from="o15138" id="r2412" name="in" negation="false" src="d0_s7" to="o15139" />
      <relation from="o15139" id="r2413" name="as wide as" negation="false" src="d0_s7" to="o15140" />
    </statement>
    <statement id="d0_s8">
      <text>adaxial sclerenchyma present;</text>
    </statement>
    <statement id="d0_s9">
      <text>girders associated with the major veins.</text>
      <biological_entity constraint="abaxial" id="o15138" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="adaxial" value_original="adaxial" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o15141" name="vein" name_original="veins" src="d0_s9" type="structure">
        <character is_modifier="true" name="size" src="d0_s9" value="major" value_original="major" />
      </biological_entity>
      <relation from="o15138" id="r2414" name="associated with the" negation="false" src="d0_s9" to="o15141" />
    </statement>
    <statement id="d0_s10">
      <text>Inflorescences 5-16 cm, open, often secund, with 1-2 (3) branches per node;</text>
      <biological_entity id="o15142" name="inflorescence" name_original="inflorescences" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s10" to="16" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="open" value_original="open" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s10" value="secund" value_original="secund" />
      </biological_entity>
      <biological_entity id="o15143" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s10" value="3" value_original="3" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s10" to="2" />
      </biological_entity>
      <biological_entity id="o15144" name="node" name_original="node" src="d0_s10" type="structure" />
      <relation from="o15142" id="r2415" name="with" negation="false" src="d0_s10" to="o15143" />
      <relation from="o15143" id="r2416" name="per" negation="false" src="d0_s10" to="o15144" />
    </statement>
    <statement id="d0_s11">
      <text>branches lax, spreading, lower branches usually recurved or reflexed, spikelets borne towards the ends of the branches.</text>
      <biological_entity id="o15145" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s11" value="lax" value_original="lax" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="lower" id="o15146" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s11" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o15147" name="spikelet" name_original="spikelets" src="d0_s11" type="structure" />
      <biological_entity id="o15148" name="end" name_original="ends" src="d0_s11" type="structure" />
      <biological_entity id="o15149" name="branch" name_original="branches" src="d0_s11" type="structure" />
      <relation from="o15147" id="r2417" name="borne towards the" negation="false" src="d0_s11" to="o15148" />
      <relation from="o15147" id="r2418" name="borne towards the" negation="false" src="d0_s11" to="o15149" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 8-14 mm, usually purple, lustrous, with 3-4 (6) florets.</text>
      <biological_entity id="o15150" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s12" to="14" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="coloration_or_density" src="d0_s12" value="purple" value_original="purple" />
        <character is_modifier="false" name="reflectance" src="d0_s12" value="lustrous" value_original="lustrous" />
      </biological_entity>
      <biological_entity id="o15151" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s12" value="6" value_original="6" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s12" to="4" />
      </biological_entity>
      <relation from="o15150" id="r2419" name="with" negation="false" src="d0_s12" to="o15151" />
    </statement>
    <statement id="d0_s13">
      <text>Glumes glabrous or slightly scabrous, distinctly shorter than the adjacent lemmas;</text>
      <biological_entity id="o15152" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s13" value="scabrous" value_original="scabrous" />
        <character constraint="than the adjacent lemmas" constraintid="o15153" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="distinctly shorter" value_original="distinctly shorter" />
      </biological_entity>
      <biological_entity id="o15153" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s13" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower glumes 4-6.8 (8.5) mm;</text>
      <biological_entity constraint="lower" id="o15154" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="8.5" value_original="8.5" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="6.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper glumes (4.5) 5.3-7.5 (10) mm;</text>
      <biological_entity constraint="upper" id="o15155" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="4.5" value_original="4.5" />
        <character char_type="range_value" from="5.3" from_unit="mm" name="some_measurement" src="d0_s15" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lemmas (6.5) 7.5-9 (12) mm, chartaceous, scabrous, at least on the veins, keeled on the lover 1/2, veins 5, prominent, apices attenuate or short-awned, awns 0.2-0.7 mm;</text>
      <biological_entity id="o15156" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character name="atypical_some_measurement" src="d0_s16" unit="mm" value="6.5" value_original="6.5" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" src="d0_s16" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s16" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s16" value="scabrous" value_original="scabrous" />
        <character constraint="on the lover" is_modifier="false" name="shape" notes="" src="d0_s16" value="keeled" value_original="keeled" />
        <character name="quantity" src="d0_s16" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o15157" name="vein" name_original="veins" src="d0_s16" type="structure" />
      <biological_entity id="o15158" name="vein" name_original="veins" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
        <character is_modifier="false" name="prominence" src="d0_s16" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o15159" name="apex" name_original="apices" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="short-awned" value_original="short-awned" />
      </biological_entity>
      <biological_entity id="o15160" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s16" to="0.7" to_unit="mm" />
      </biological_entity>
      <relation from="o15156" id="r2420" modifier="at-least" name="on" negation="false" src="d0_s16" to="o15157" />
    </statement>
    <statement id="d0_s17">
      <text>paleas about as long as or a little shorter than the lemmas, intercostal region puberulent distally;</text>
      <biological_entity id="o15161" name="palea" name_original="paleas" src="d0_s17" type="structure">
        <character constraint="than the lemmas" constraintid="o15162" is_modifier="false" name="height_or_length_or_size" src="d0_s17" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o15162" name="lemma" name_original="lemmas" src="d0_s17" type="structure" />
      <biological_entity constraint="intercostal" id="o15163" name="region" name_original="region" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s17" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>anthers 2.6-4.5 (5) mm;</text>
      <biological_entity id="o15164" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character name="atypical_some_measurement" src="d0_s18" unit="mm" value="5" value_original="5" />
        <character char_type="range_value" from="2.6" from_unit="mm" name="some_measurement" src="d0_s18" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovary apices usually sparsely pubescent, rarely glabrous.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 28.</text>
      <biological_entity constraint="ovary" id="o15165" name="apex" name_original="apices" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="usually sparsely" name="pubescence" src="d0_s19" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15166" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Festuca altaica is a plant of rocky alpine habitats, arctic tundra, and open boreal or subalpine forests. Its primary distribution extends from Alaska eastward to the western Northwest Territories, and south in the alpine regions of British Columbia and west-central Alberta. Disjunct populations occur in Quebec, western Labrador and Newfoundland, and in Michigan, where it may be introduced. From the Bering Sea it extends westward to the Altai Mountains of central Asia.</discussion>
  <discussion>The spikelets of Festuca altaica are lustrous and usually intensely purplish; plants with greenish spikelets have been named F. altaica f. pallida Jordal. A form producing pseudoviviparous spikelets, F. altaica f. vivipara Jordal, has been described from Alaska.</discussion>
  <other_name type="invalid_name">Festuca scabrella; within the Flora region, misapplied</other_name>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska;Mich.;Alta.;B.C.;Man.;Nfld. and Labr.;N.W.T.;Que.;Sask.;Yukon</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>