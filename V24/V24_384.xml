<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">270</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché, Hana Pazdírková, and Christine Roberts</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">TRITICUM</taxon_name>
    <taxon_name authority="Boiss." date="unknown" rank="species">boeoticum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus triticum;species boeoticum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Triticum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">monococcum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">boeoticum</taxon_name>
    <taxon_hierarchy>genus triticum;species monococcum;subspecies boeoticum</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Wild einkorn</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms to 160 cm, decumbent at the base;</text>
      <biological_entity id="o26247" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="160" to_unit="cm" />
        <character constraint="at base" constraintid="o26248" is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
      </biological_entity>
      <biological_entity id="o26248" name="base" name_original="base" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>nodes pubescent;</text>
      <biological_entity id="o26249" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes mostly hollow, solid for 1 cm below the spike.</text>
      <biological_entity id="o26250" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s2" value="hollow" value_original="hollow" />
        <character constraint="below spike" constraintid="o26251" is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
      </biological_entity>
      <biological_entity id="o26251" name="spike" name_original="spike" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Blades 5-15 mm wide, blue-green, hirsute, with long hairs over the veins, shorter hairs between the veins, hairs stiff.</text>
      <biological_entity id="o26252" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="15" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="blue-green" value_original="blue-green" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o26253" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o26254" name="vein" name_original="veins" src="d0_s3" type="structure" />
      <biological_entity constraint="vein" constraintid="o26256" id="o26255" name="hair" name_original="hairs" src="d0_s3" type="structure" constraint_original="vein" />
      <biological_entity id="o26256" name="vein" name_original="veins" src="d0_s3" type="structure" />
      <biological_entity id="o26257" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s3" value="stiff" value_original="stiff" />
      </biological_entity>
      <relation from="o26252" id="r4137" name="with" negation="false" src="d0_s3" to="o26253" />
      <relation from="o26253" id="r4138" name="over" negation="false" src="d0_s3" to="o26254" />
    </statement>
    <statement id="d0_s4">
      <text>Spikes 5-14 cm, wider than thick;</text>
      <biological_entity id="o26258" name="spike" name_original="spikes" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s4" to="14" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s4" value="wider than thick" value_original="wider than thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>rachises densely ciliate at the nodes and margins;</text>
      <biological_entity id="o26259" name="rachis" name_original="rachises" src="d0_s5" type="structure">
        <character constraint="at margins" constraintid="o26261" is_modifier="false" modifier="densely" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o26260" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <biological_entity id="o26261" name="margin" name_original="margins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>internodes 3-5 mm;</text>
    </statement>
    <statement id="d0_s7">
      <text>disarticulation spon¬taneous, dispersal units wedge-shaped.</text>
      <biological_entity id="o26262" name="internode" name_original="internodes" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26263" name="unit" name_original="units" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="wedge--shaped" value_original="wedge--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 12-17 mm, rectangular, with 2-3 florets, 1-2 seed-forming.</text>
      <biological_entity id="o26264" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s8" to="17" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="rectangular" value_original="rectangular" />
      </biological_entity>
      <biological_entity id="o26265" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="3" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s8" to="2" />
      </biological_entity>
      <relation from="o26264" id="r4139" name="with" negation="false" src="d0_s8" to="o26265" />
    </statement>
    <statement id="d0_s9">
      <text>Glumes 6-11 mm, coriaceous, tightly appressed to the lower florets, 2-keeled, with 2 prominent teeth;</text>
      <biological_entity id="o26266" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s9" value="coriaceous" value_original="coriaceous" />
        <character constraint="to lower florets" constraintid="o26267" is_modifier="false" modifier="tightly" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="2-keeled" value_original="2-keeled" />
      </biological_entity>
      <biological_entity constraint="lower" id="o26267" name="floret" name_original="florets" src="d0_s9" type="structure" />
      <biological_entity id="o26268" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="prominence" src="d0_s9" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o26266" id="r4140" name="with" negation="false" src="d0_s9" to="o26268" />
    </statement>
    <statement id="d0_s10">
      <text>lemmas 10-14 mm, first (and sometimes the second) lemma awned, awns to 11 cm, third lemma usually unawned;</text>
      <biological_entity id="o26269" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26270" name="lemma" name_original="lemma" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o26271" name="awn" name_original="awns" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s10" to="11" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o26272" name="lemma" name_original="lemma" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>paleas splitting at maturity;</text>
      <biological_entity id="o26273" name="palea" name_original="paleas" src="d0_s11" type="structure">
        <character constraint="at maturity" is_modifier="false" name="architecture_or_dehiscence" src="d0_s11" value="splitting" value_original="splitting" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 3-6 mm.</text>
      <biological_entity id="o26274" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Caryopses of the lowest floret in each spikelet usually blue, that of the second amber or red with blue mottling;</text>
      <biological_entity id="o26275" name="caryopse" name_original="caryopses" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="red with blue mottling" value_original="red with blue mottling" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o26276" name="floret" name_original="floret" src="d0_s13" type="structure" />
      <biological_entity id="o26277" name="spikelet" name_original="spikelet" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s13" value="blue" value_original="blue" />
      </biological_entity>
      <biological_entity id="o26278" name="amber" name_original="amber" src="d0_s13" type="substance" />
      <relation from="o26275" id="r4141" name="part_of" negation="false" src="d0_s13" to="o26276" />
      <relation from="o26275" id="r4142" name="in" negation="false" src="d0_s13" to="o26277" />
      <relation from="o26275" id="r4143" name="part_of" negation="false" src="d0_s13" to="o26278" />
    </statement>
    <statement id="d0_s14">
      <text>endosperm flinty.</text>
    </statement>
    <statement id="d0_s15">
      <text>Haplome Ab.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 14.</text>
      <biological_entity id="o26279" name="endosperm" name_original="endosperm" src="d0_s14" type="structure" />
      <biological_entity constraint="2n" id="o26280" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Triticum boeoticum is a wild diploid wheat that is native from the Balkans through the Caucasus to Iran and Afghanistan and south to Iraq. It is morph¬ologically similar to and, in its native range, sometimes sympatric with T. urartu, another wild diploid wheat. Triticum monococcum is the domesticated derivative of T. boeoticum.</discussion>
  <discussion>Boissier published the combination for this species both as "Triticum baeoticum" and "T. boeoticum". Because the type specimen is from Boeotia [Greece], "boeoticum" is the correct spelling of the epithet.</discussion>
  
</bio:treatment>