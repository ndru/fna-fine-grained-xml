<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Grass Phylogeny Working Group;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">14</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Luerss." date="unknown" rank="subfamily">BAMBUSOIDEAE</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily bambusoideae</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually perennial, rarely annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous.</text>
      <biological_entity id="o26018" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="rarely" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms woody or herbaceous, hollow or solid;</text>
    </statement>
    <statement id="d0_s3">
      <text>often developing complex vegetative branching;</text>
      <biological_entity id="o26019" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="texture" src="d0_s2" value="woody" value_original="woody" />
        <character is_modifier="false" name="texture" src="d0_s2" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="hollow" value_original="hollow" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="often" name="development" src="d0_s3" value="developing" value_original="developing" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="complex" value_original="complex" />
        <character is_modifier="false" name="reproduction" src="d0_s3" value="vegetative" value_original="vegetative" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaves distichous, if complex vegetative branching present, leaves of the culms (culm leaves) differing from those of the vegetative branches (foliage leaves);</text>
      <biological_entity id="o26020" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="distichous" value_original="distichous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="complex" value_original="complex" />
        <character is_modifier="false" name="reproduction" src="d0_s4" value="vegetative" value_original="vegetative" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branching" value_original="branching" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o26021" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o26022" name="culm" name_original="culms" src="d0_s4" type="structure" />
      <biological_entity id="o26023" name="those" name_original="those" src="d0_s4" type="structure" />
      <biological_entity id="o26024" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s4" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <relation from="o26021" id="r4100" name="part_of" negation="false" src="d0_s4" to="o26022" />
      <relation from="o26021" id="r4101" name="differing from" negation="false" src="d0_s4" to="o26023" />
      <relation from="o26021" id="r4102" name="differing from" negation="false" src="d0_s4" to="o26024" />
    </statement>
    <statement id="d0_s5">
      <text>auricles often present;</text>
      <biological_entity id="o26025" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>abaxial ligules rarely present on the culm leaves, usually present on the foliage leaves;</text>
      <biological_entity constraint="abaxial" id="o26026" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character constraint="on culm leaves" constraintid="o26027" is_modifier="false" modifier="rarely" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="culm" id="o26027" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character constraint="on foliage leaves" constraintid="o26028" is_modifier="false" modifier="usually" name="presence" notes="" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="foliage" id="o26028" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>adaxial ligules membranous or chartaceous, ciliate or not;</text>
      <biological_entity constraint="adaxial" id="o26029" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character is_modifier="false" name="texture" src="d0_s7" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s7" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
        <character name="architecture_or_pubescence_or_shape" src="d0_s7" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pseudopetioles sometimes present on the culm leaves, usually present on the foliage leaves;</text>
      <biological_entity id="o26030" name="pseudopetiole" name_original="pseudopetioles" src="d0_s8" type="structure">
        <character constraint="on culm leaves" constraintid="o26031" is_modifier="false" modifier="sometimes" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="culm" id="o26031" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character constraint="on foliage leaves" constraintid="o26032" is_modifier="false" modifier="usually" name="presence" notes="" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="foliage" id="o26032" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>blades usually relatively broad, venation parallel, often with evident cross venation;</text>
      <biological_entity id="o26034" name="vein" name_original="venation" src="d0_s9" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s9" value="evident" value_original="evident" />
        <character is_modifier="true" name="shape" src="d0_s9" value="cross" value_original="cross" />
      </biological_entity>
      <relation from="o26033" id="r4103" modifier="often" name="with" negation="false" src="d0_s9" to="o26034" />
    </statement>
    <statement id="d0_s10">
      <text>mesophyll nonradiate;</text>
    </statement>
    <statement id="d0_s11">
      <text>adaxial palisade layer usually absent;</text>
      <biological_entity id="o26033" name="blade" name_original="blades" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually relatively" name="width" src="d0_s9" value="broad" value_original="broad" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="parallel" value_original="parallel" />
        <character is_modifier="false" name="position" src="d0_s11" value="adaxial" value_original="adaxial" />
      </biological_entity>
      <biological_entity id="o26035" name="layer" name_original="layer" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>fusoid cells usually well developed, large;</text>
      <biological_entity constraint="fusoid" id="o26036" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually well" name="development" src="d0_s12" value="developed" value_original="developed" />
        <character is_modifier="false" name="size" src="d0_s12" value="large" value_original="large" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>arm cells usually well developed and highly invaginated;</text>
    </statement>
    <statement id="d0_s14">
      <text>Kranz anatomy not developed;</text>
      <biological_entity constraint="arm" id="o26037" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually well; highly" name="development" src="d0_s13" value="developed" value_original="developed" />
        <character is_modifier="false" name="anatomy" src="d0_s14" value="kranz" value_original="kranz" />
        <character is_modifier="false" modifier="not" name="development" src="d0_s14" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>midribs complex or simple;</text>
      <biological_entity id="o26038" name="midrib" name_original="midribs" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="complex" value_original="complex" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stomates with dome-shaped, triangular, or more rarely parallel-sided subsidiary-cells;</text>
      <biological_entity id="o26039" name="stomate" name_original="stomates" src="d0_s16" type="structure" />
      <biological_entity id="o26040" name="subsidiary-cell" name_original="subsidiary-cells" src="d0_s16" type="structure">
        <character is_modifier="true" name="shape" src="d0_s16" value="dome--shaped" value_original="dome--shaped" />
        <character is_modifier="true" name="shape" src="d0_s16" value="triangular" value_original="triangular" />
        <character is_modifier="true" modifier="rarely" name="architecture" src="d0_s16" value="parallel-sided" value_original="parallel-sided" />
      </biological_entity>
      <relation from="o26039" id="r4104" name="with" negation="false" src="d0_s16" to="o26040" />
    </statement>
    <statement id="d0_s17">
      <text>adaxial bulliform cells present;</text>
    </statement>
    <statement id="d0_s18">
      <text>bicellular microhairs present, terminal cells tapered;</text>
      <biological_entity constraint="adaxial" id="o26041" name="cell" name_original="cells" src="d0_s17" type="structure">
        <character is_modifier="true" name="shape" src="d0_s17" value="bulliform" value_original="bulliform" />
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o26042" name="microhair" name_original="microhairs" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o26043" name="cell" name_original="cells" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="tapered" value_original="tapered" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>papillae common and abundant.</text>
      <biological_entity id="o26044" name="papilla" name_original="papillae" src="d0_s19" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s19" value="common" value_original="common" />
        <character is_modifier="false" name="quantity" src="d0_s19" value="abundant" value_original="abundant" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Inflorescences spicate, racemose, or paniculate, comprising spikelets or pseudospikelets, the spikelets lacking subtending bracts and prophylls, completing their development during 1 period of growth, the pseudospikelets having subtending bracts, prophylls, and basal bud-bearing bracts developing 2 or more orders of true spikelets with different phases of maturity.</text>
      <biological_entity id="o26045" name="inflorescence" name_original="inflorescences" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="spicate" value_original="spicate" />
        <character is_modifier="false" name="arrangement" src="d0_s20" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="arrangement" src="d0_s20" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" name="arrangement" src="d0_s20" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="arrangement" src="d0_s20" value="paniculate" value_original="paniculate" />
      </biological_entity>
      <biological_entity id="o26046" name="spikelet" name_original="spikelets" src="d0_s20" type="structure" />
      <biological_entity id="o26047" name="pseudospikelet" name_original="pseudospikelets" src="d0_s20" type="structure" />
      <biological_entity id="o26048" name="spikelet" name_original="spikelets" src="d0_s20" type="structure" />
      <biological_entity constraint="subtending" id="o26049" name="bract" name_original="bracts" src="d0_s20" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s20" value="lacking" value_original="lacking" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o26050" name="prophyll" name_original="prophylls" src="d0_s20" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s20" value="lacking" value_original="lacking" />
      </biological_entity>
      <biological_entity id="o26051" name="pseudospikelet" name_original="pseudospikelets" src="d0_s20" type="structure" />
      <biological_entity constraint="subtending" id="o26052" name="bract" name_original="bracts" src="d0_s20" type="structure" />
      <biological_entity id="o26053" name="prophyll" name_original="prophylls" src="d0_s20" type="structure" />
      <biological_entity constraint="basal" id="o26054" name="spikelet" name_original="spikelets" src="d0_s20" type="structure" />
      <biological_entity id="o26055" name="bract" name_original="bracts" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="bud-bearing" value_original="bud-bearing" />
        <character is_modifier="false" modifier="during 1 period; of growth" name="development" src="d0_s20" value="developing" value_original="developing" />
        <character constraint="of spikelets" constraintid="o26056" name="quantity" src="d0_s20" unit="or moreorders" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o26056" name="spikelet" name_original="spikelets" src="d0_s20" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s20" value="true" value_original="true" />
      </biological_entity>
      <biological_entity id="o26057" name="phase" name_original="phases" src="d0_s20" type="structure" />
      <relation from="o26045" id="r4105" name="comprising" negation="false" src="d0_s20" to="o26046" />
      <relation from="o26045" id="r4106" name="comprising" negation="false" src="d0_s20" to="o26047" />
      <relation from="o26051" id="r4107" modifier="during 1 period; of growth" name="having" negation="false" src="d0_s20" to="o26052" />
      <relation from="o26056" id="r4108" modifier="of maturity" name="with" negation="false" src="d0_s20" to="o26057" />
    </statement>
    <statement id="d0_s21">
      <text>Spikelets bisexual or unisexual, with 1 to many florets.</text>
      <biological_entity id="o26058" name="spikelet" name_original="spikelets" src="d0_s21" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s21" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s21" value="unisexual" value_original="unisexual" />
        <character constraint="to florets" constraintid="o26059" modifier="with" name="quantity" src="d0_s21" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o26059" name="floret" name_original="florets" src="d0_s21" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s21" value="many" value_original="many" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Glumes absent or 1-2+;</text>
      <biological_entity id="o26060" name="glume" name_original="glumes" src="d0_s22" type="structure">
        <character is_modifier="false" name="presence" src="d0_s22" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s22" to="2" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>lemmas without uncinate hairs, sometimes awned, awns single;</text>
      <biological_entity id="o26061" name="lemma" name_original="lemmas" src="d0_s23" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" notes="" src="d0_s23" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o26062" name="hair" name_original="hairs" src="d0_s23" type="structure">
        <character is_modifier="true" name="shape" src="d0_s23" value="uncinate" value_original="uncinate" />
      </biological_entity>
      <biological_entity id="o26063" name="awn" name_original="awns" src="d0_s23" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s23" value="single" value_original="single" />
      </biological_entity>
      <relation from="o26061" id="r4109" name="without" negation="false" src="d0_s23" to="o26062" />
    </statement>
    <statement id="d0_s24">
      <text>paleas well developed;</text>
      <biological_entity id="o26064" name="palea" name_original="paleas" src="d0_s24" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s24" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>lodicules (0) 3 (6+), membranous, vascularized, often ciliate;</text>
      <biological_entity id="o26065" name="lodicule" name_original="lodicules" src="d0_s25" type="structure">
        <character name="atypical_quantity" src="d0_s25" value="0" value_original="0" />
        <character name="quantity" src="d0_s25" value="3" value_original="3" />
        <character char_type="range_value" from="6" name="atypical_quantity" src="d0_s25" upper_restricted="false" />
        <character is_modifier="false" name="texture" src="d0_s25" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="often" name="architecture_or_pubescence_or_shape" src="d0_s25" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>anthers usually 2, 3,or 6, rarely 10-120;</text>
      <biological_entity id="o26066" name="anther" name_original="anthers" src="d0_s26" type="structure">
        <character name="quantity" src="d0_s26" value="2" value_original="2" />
        <character name="quantity" src="d0_s26" value="3" value_original="3" />
        <character name="quantity" src="d0_s26" value="6" value_original="6" />
        <character char_type="range_value" from="10" modifier="rarely" name="quantity" src="d0_s26" to="120" />
      </biological_entity>
    </statement>
    <statement id="d0_s27">
      <text>ovaries glabrous or hairy, sometimes with an apical appendage;</text>
      <biological_entity constraint="apical" id="o26068" name="appendage" name_original="appendage" src="d0_s27" type="structure" />
      <relation from="o26067" id="r4110" modifier="sometimes" name="with" negation="false" src="d0_s27" to="o26068" />
    </statement>
    <statement id="d0_s28">
      <text>haustorial synergids absent;</text>
      <biological_entity id="o26067" name="ovary" name_original="ovaries" src="d0_s27" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s27" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s27" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="presence" src="d0_s28" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s29">
      <text>styles or style-branches 1-4.</text>
      <biological_entity id="o26069" name="style" name_original="styles" src="d0_s29" type="structure" />
      <biological_entity id="o26070" name="style-branch" name_original="style-branches" src="d0_s29" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s29" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s30">
      <text>Caryopses: hila linear, usually as long as the caryopses;</text>
      <biological_entity id="o26071" name="caryopse" name_original="caryopses" src="d0_s30" type="structure" />
      <biological_entity id="o26072" name="hilum" name_original="hila" src="d0_s30" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s30" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o26073" name="caryopse" name_original="caryopses" src="d0_s30" type="structure" />
      <relation from="o26072" id="r4111" modifier="usually" name="as long as" negation="false" src="d0_s30" to="o26073" />
    </statement>
    <statement id="d0_s31">
      <text>endosperm hard, without lipid, containing compound starch-grains;</text>
      <biological_entity id="o26074" name="caryopse" name_original="caryopses" src="d0_s31" type="structure" />
      <biological_entity id="o26075" name="endosperm" name_original="endosperm" src="d0_s31" type="structure">
        <character is_modifier="false" name="texture" src="d0_s31" value="hard" value_original="hard" />
      </biological_entity>
      <biological_entity id="o26076" name="lipid" name_original="lipid" src="d0_s31" type="structure" />
      <biological_entity id="o26077" name="starch-grain" name_original="starch-grains" src="d0_s31" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s31" value="compound" value_original="compound" />
      </biological_entity>
      <relation from="o26075" id="r4112" name="without" negation="false" src="d0_s31" to="o26076" />
      <relation from="o26075" id="r4113" name="containing" negation="false" src="d0_s31" to="o26077" />
    </statement>
    <statement id="d0_s32">
      <text>embryos small relative to the caryopses;</text>
      <biological_entity id="o26078" name="caryopse" name_original="caryopses" src="d0_s32" type="structure" />
      <biological_entity id="o26079" name="embryo" name_original="embryos" src="d0_s32" type="structure">
        <character constraint="to caryopses" constraintid="o26080" is_modifier="false" name="size" src="d0_s32" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o26080" name="caryopse" name_original="caryopses" src="d0_s32" type="structure" />
    </statement>
    <statement id="d0_s33">
      <text>epiblasts present;</text>
      <biological_entity id="o26081" name="caryopse" name_original="caryopses" src="d0_s33" type="structure">
        <character is_modifier="false" name="presence" src="d0_s33" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s34">
      <text>scutellar cleft present;</text>
      <biological_entity id="o26082" name="caryopse" name_original="caryopses" src="d0_s34" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s34" value="cleft" value_original="cleft" />
        <character is_modifier="false" name="presence" src="d0_s34" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s35">
      <text>mesocotyl internode absent;</text>
      <biological_entity id="o26083" name="caryopse" name_original="caryopses" src="d0_s35" type="structure" />
      <biological_entity id="o26084" name="internode" name_original="internode" src="d0_s35" type="structure">
        <character is_modifier="false" name="presence" src="d0_s35" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s36">
      <text>embryonic leaf margins overlapping, x = 7,9, 10, 11, 12.</text>
      <biological_entity id="o26085" name="caryopse" name_original="caryopses" src="d0_s36" type="structure" />
      <biological_entity constraint="leaf" id="o26086" name="margin" name_original="margins" src="d0_s36" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s36" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity constraint="x" id="o26087" name="chromosome" name_original="" src="d0_s36" type="structure">
        <character name="quantity" src="d0_s36" value="7" value_original="7" />
        <character name="quantity" src="d0_s36" value="9" value_original="9" />
        <character name="quantity" src="d0_s36" value="10" value_original="10" />
        <character name="quantity" src="d0_s36" value="11" value_original="11" />
        <character name="quantity" src="d0_s36" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The Bambusoideae includes two tribes, the woody Bambuseae and the herbaceous Olyreae. Their range includes tropical and temperate regions of Asia, Australia, and the Americas, primarily Central and South America. Three species of Bambuseae are native to the Flora region; there are no native species of Olyreae.</discussion>
  <discussion>Members of the Bambusoideae grow in temperate and tropical forests, high montane grasslands, along riverbanks, and sometimes in savannahs. They are mainly forest understory or margin plants with a limited ability to reproduce, disperse, or survive outside their forest environment. Many have relatively small geographic ranges, and there is a high degree of endemism. The conservation status of most bamboos is not known; all are intrinsically vulnerable because of their breeding behavior and reliance upon a benign forest habitat. Only the C3 photosynthetic pathway is found in the subfamily.</discussion>
  <references>
    <reference>Grass Phylogeny Working Group. 2001. Phylogeny and subfamilial classification of the grasses (Poaceae). Ann. Missouri Bot. Gard. 88:373-457</reference>
    <reference>Judziewicz, E.J., L.G. Clark, X. Londono, and M.J. Stem. 1999. American Bamboos. Smithsonian Institution Press, Washington, D.C., U.S.A. 392 pp.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Culms woody, usually taller than 1 m, developing complex vegetative branching from the upper nodes; abaxial ligules present on the foliage leaves, rarely present on the culm leaves</description>
      <determination>2 Bambuseae</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Culms herbaceous, usually shorter than 1 m; complex vegetative branching not developed; abaxial ligules not present.</description>
      <determination>3 Olyreae</determination>
    </key_statement>
  </key>
</bio:treatment>