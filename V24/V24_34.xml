<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Edward E. Terrell;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">36</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">EHRHARTOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ORYZEAE</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily ehrhartoideae;tribe oryzeae</taxon_hierarchy>
  </taxon_identification>
  <number>5</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>synoecious or monoecious.</text>
      <biological_entity id="o1411" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="reproduction" src="d0_s1" value="synoecious" value_original="synoecious" />
        <character is_modifier="false" name="reproduction" src="d0_s1" value="monoecious" value_original="monoecious" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms annual, 20-500 cm tall, aerenchymatous, sometimes floating.</text>
      <biological_entity id="o1412" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="annual" value_original="annual" />
        <character char_type="range_value" from="20" from_unit="cm" name="height" src="d0_s2" to="500" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="aerenchymatous" value_original="aerenchymatous" />
        <character is_modifier="false" modifier="sometimes" name="growth_form_or_location" src="d0_s2" value="floating" value_original="floating" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves aerenchymatous;</text>
      <biological_entity id="o1413" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="aerenchymatous" value_original="aerenchymatous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>auricles present or absent;</text>
      <biological_entity id="o1414" name="auricle" name_original="auricles" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules membranous or scarious, sometimes absent;</text>
      <biological_entity id="o1415" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="texture" src="d0_s5" value="scarious" value_original="scarious" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pseudopetioles sometimes present;</text>
      <biological_entity id="o1416" name="pseudopetiole" name_original="pseudopetioles" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades with parallel veins, cross venation not evident;</text>
      <biological_entity id="o1417" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="cross" value_original="cross" />
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s7" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o1418" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s7" value="parallel" value_original="parallel" />
      </biological_entity>
      <relation from="o1417" id="r217" name="with" negation="false" src="d0_s7" to="o1418" />
    </statement>
    <statement id="d0_s8">
      <text>abaxial blade epidermes with microhairs and transversely dumbbell-shaped silica-bodies;</text>
      <biological_entity id="o1420" name="microhair" name_original="microhairs" src="d0_s8" type="structure" />
      <biological_entity id="o1421" name="silica-body" name_original="silica-bodies" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="transversely" name="shape" src="d0_s8" value="dumbbell--shaped" value_original="dumbbell--shaped" />
      </biological_entity>
      <relation from="o1419" id="r218" name="with" negation="false" src="d0_s8" to="o1420" />
      <relation from="o1419" id="r219" name="with" negation="false" src="d0_s8" to="o1421" />
    </statement>
    <statement id="d0_s9">
      <text>first seedling leaf without a blade.</text>
      <biological_entity constraint="blade" id="o1419" name="epiderme" name_original="epidermes" src="d0_s8" type="structure" constraint_original="abaxial blade" />
      <biological_entity constraint="seedling" id="o1422" name="leaf" name_original="leaf" src="d0_s9" type="structure" />
      <biological_entity id="o1423" name="blade" name_original="blade" src="d0_s9" type="structure" />
      <relation from="o1422" id="r220" name="without" negation="false" src="d0_s9" to="o1423" />
    </statement>
    <statement id="d0_s10">
      <text>Inflorescences usually panicles, sometimes racemes or spikes;</text>
      <biological_entity id="o1425" name="panicle" name_original="panicles" src="d0_s10" type="structure" />
      <biological_entity id="o1426" name="raceme" name_original="racemes" src="d0_s10" type="structure" />
      <biological_entity id="o1427" name="spike" name_original="spikes" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>disarticulation below the spikelets, not occurring in cultivated taxa.</text>
      <biological_entity id="o1424" name="inflorescence" name_original="inflorescences" src="d0_s10" type="structure" />
      <biological_entity id="o1428" name="spikelet" name_original="spikelets" src="d0_s11" type="structure" />
      <biological_entity id="o1429" name="taxon" name_original="taxa" src="d0_s11" type="structure">
        <character is_modifier="true" name="condition" src="d0_s11" value="cultivated" value_original="cultivated" />
      </biological_entity>
      <relation from="o1424" id="r221" name="below" negation="false" src="d0_s11" to="o1428" />
      <relation from="o1428" id="r222" name="occurring in" negation="true" src="d0_s11" to="o1429" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets laterally compressed or terete, with 1 bisexual or unisexual floret, sometimes with 2 sterile florets below the sexual floret, these no more than 1/2 (9/10) the length of the fertile floret;</text>
      <biological_entity id="o1430" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s12" value="terete" value_original="terete" />
        <character name="quantity" src="d0_s12" unit="+[9/0]" value="1" value_original="1" />
        <character name="quantity" src="d0_s12" unit="+[9/0]" value="/2" value_original="/2" />
        <character name="quantity" src="d0_s12" unit="+[9/0]" value="[9/10]" value_original="[9/10]" />
      </biological_entity>
      <biological_entity id="o1431" name="floret" name_original="floret" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="true" name="reproduction" src="d0_s12" value="unisexual" value_original="unisexual" />
      </biological_entity>
      <biological_entity id="o1432" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="true" name="reproduction" src="d0_s12" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o1433" name="floret" name_original="floret" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="quantity" notes="" src="d0_s12" value="no" value_original="no" />
      </biological_entity>
      <biological_entity id="o1434" name="floret" name_original="floret" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
      <relation from="o1430" id="r223" name="with" negation="false" src="d0_s12" to="o1431" />
      <relation from="o1430" id="r224" modifier="sometimes" name="with" negation="false" src="d0_s12" to="o1432" />
      <relation from="o1432" id="r225" name="below" negation="false" src="d0_s12" to="o1433" />
    </statement>
    <statement id="d0_s13">
      <text>unisexual spikelets in the same or different panicles;</text>
      <biological_entity id="o1435" name="spikelet" name_original="spikelets" src="d0_s13" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s13" value="unisexual" value_original="unisexual" />
      </biological_entity>
      <biological_entity id="o1436" name="panicle" name_original="panicles" src="d0_s13" type="structure" />
      <relation from="o1435" id="r226" name="in" negation="false" src="d0_s13" to="o1436" />
    </statement>
    <statement id="d0_s14">
      <text>rachillas not prolonged.</text>
      <biological_entity id="o1437" name="rachilla" name_original="rachillas" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not" name="length" src="d0_s14" value="prolonged" value_original="prolonged" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Glumes absent or highly reduced, forming an annular ring or lobes at the pedicel apices;</text>
      <biological_entity id="o1438" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="highly" name="size" src="d0_s15" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o1439" name="ring" name_original="ring" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="annular" value_original="annular" />
      </biological_entity>
      <biological_entity id="o1440" name="lobe" name_original="lobes" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="annular" value_original="annular" />
      </biological_entity>
      <biological_entity constraint="pedicel" id="o1441" name="apex" name_original="apices" src="d0_s15" type="structure" />
      <relation from="o1438" id="r227" name="forming an" negation="false" src="d0_s15" to="o1439" />
      <relation from="o1438" id="r228" name="forming an" negation="false" src="d0_s15" to="o1440" />
      <relation from="o1438" id="r229" name="at" negation="false" src="d0_s15" to="o1441" />
    </statement>
    <statement id="d0_s16">
      <text>sterile florets 1/8 – 1/2 (9/10) as long as the spikelets;</text>
      <biological_entity id="o1442" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s16" value="sterile" value_original="sterile" />
        <character char_type="range_value" constraint="as-long-as spikelets" constraintid="o1443" from="1/8" name="quantity" src="d0_s16" to="1/29/10" />
      </biological_entity>
      <biological_entity id="o1443" name="spikelet" name_original="spikelets" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>fertile lemmas 3-14-veined, membranous or coriaceous, apices entire, unawned or with a terminal awn;</text>
      <biological_entity id="o1444" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s17" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="3-14-veined" value_original="3-14-veined" />
        <character is_modifier="false" name="texture" src="d0_s17" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="texture" src="d0_s17" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o1445" name="apex" name_original="apices" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o1446" name="awn" name_original="awn" src="d0_s17" type="structure" />
      <relation from="o1445" id="r230" name="with" negation="false" src="d0_s17" to="o1446" />
    </statement>
    <statement id="d0_s18">
      <text>paleas similar to the lemmas, 3-10-veined, 1-keeled;</text>
      <biological_entity id="o1447" name="palea" name_original="paleas" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s18" value="3-10-veined" value_original="3-10-veined" />
        <character is_modifier="false" name="shape" src="d0_s18" value="1-keeled" value_original="1-keeled" />
      </biological_entity>
      <biological_entity id="o1448" name="lemma" name_original="lemmas" src="d0_s18" type="structure" />
      <relation from="o1447" id="r231" name="to" negation="false" src="d0_s18" to="o1448" />
    </statement>
    <statement id="d0_s19">
      <text>lodicules 2;</text>
      <biological_entity id="o1449" name="lodicule" name_original="lodicules" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>anthers usually 6 (1-16);</text>
      <biological_entity id="o1450" name="anther" name_original="anthers" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" unit="[1-1]" value="6" value_original="6" />
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s20" to="16" unit="[1-1]" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>styles 2, bases fused or free, stigmas linear, plumose.</text>
      <biological_entity id="o1451" name="style" name_original="styles" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o1452" name="base" name_original="bases" src="d0_s21" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s21" value="fused" value_original="fused" />
        <character is_modifier="false" name="fusion" src="d0_s21" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o1453" name="stigma" name_original="stigmas" src="d0_s21" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s21" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s21" value="plumose" value_original="plumose" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Fruits usually caryopses, sometimes achenes, ovoid, oblong, or cylindrical;</text>
      <biological_entity id="o1454" name="fruit" name_original="fruits" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s22" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s22" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s22" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s22" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s22" value="cylindrical" value_original="cylindrical" />
      </biological_entity>
      <biological_entity id="o1455" name="caryopse" name_original="caryopses" src="d0_s22" type="structure" />
      <biological_entity id="o1456" name="achene" name_original="achenes" src="d0_s22" type="structure" />
    </statement>
    <statement id="d0_s23">
      <text>embryos of the F+FP or F+PP type, small or elongate, with or without a scutellar tail;</text>
      <biological_entity id="o1457" name="embryo" name_original="embryos" src="d0_s23" type="structure" constraint="fp; fp" />
      <biological_entity id="o1458" name="fp" name_original="fp" src="d0_s23" type="structure" />
      <biological_entity id="o1459" name="tail" name_original="tail" src="d0_s23" type="structure">
        <character is_modifier="true" name="shape" src="d0_s23" value="elongate" value_original="elongate" />
      </biological_entity>
      <relation from="o1457" id="r232" name="part_of" negation="false" src="d0_s23" to="o1458" />
      <relation from="o1457" id="r233" modifier="of the f + fp or f + pp type , small or elongate , with or without a scutellar tail" name="part_of" negation="false" src="d0_s23" to="o1459" />
    </statement>
    <statement id="d0_s24">
      <text>hila usually linear, x = 12, 15, 17.</text>
      <biological_entity id="o1460" name="hilum" name_original="hila" src="d0_s24" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement_or_course_or_shape" src="d0_s24" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity constraint="x" id="o1461" name="chromosome" name_original="" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="12" value_original="12" />
        <character name="quantity" src="d0_s24" value="15" value_original="15" />
        <character name="quantity" src="d0_s24" value="17" value_original="17" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The Oryzeae include about 10-12 genera and 70-100 species. Its members are native to temperate, subtropical, and tropical regions. Oryza sativa is one of the world's most important crop species. Four genera are native to the Flora region; two are introduced.</discussion>
  <references>
    <reference>Clayton, W.D. and S.A. Renvoize. 1986. Genera Graminum: Grasses of the World. Kew Bull., Addit. Ser. 13. Her Majesty's Stationery Office, London, England. 389 pp.</reference>
    <reference>Guo, Y.-L. and S. Ge. 2005. Molecular phylogeny of Oryzeae (Poaceae) based on DNA sequences from chloroplast, mitochondrial, and nuclear genomes. Amer. J. Bot. 92:1548-1558</reference>
    <reference>Liu, L. and S.M. Phillips. 2006. Oryzeae. P. 183 in Z.-Y. Wu, P.H. Raven, and D.-Y. Hong (eds.). Flora of China, vol. 22 (Poaceae). Science Press, Beijing, Peoples Republic of China and Missouri Botanical Garden Press, St. Louis, Missouri, U.S.A. 653 pp. http://flora.huh.harvard.edu/china/mss/volume22/index.htm/.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lemma margins free; fruits achenes, ellipsoid, obovoid, ovoid or subglobose, beaked with a shell-like pericarp.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lemmas of the pistillate spikelets awned; plants emergent, more than 1 m tall</description>
      <determination>5.05 Zizaniopsis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lemmas of the pistillate spikelets unawned; plants emergent and less than 1 m tall or submerged aquatics</description>
      <determination>5.06 Luziola</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lemmas and paleas clasping along their margins; fruits caryopses, cylindrical or laterally compressed, not beaked.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Spikelets unisexual; caryopses terete</description>
      <determination>5.04 Zizcmia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Spikelets bisexual; caryopses laterally compressed or terete.</description>
      <next_statement_id>4.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Sterile florets present below the fertile floret, 1/8 – 1/2 (9/10) as long as the spikelets</description>
      <determination>5.01 Oryza</determination>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Glumes absent.</description>
      <next_statement_id>5.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Leaf blades aerial, not pseudopetiolate, linear to broadly lanceolate; spikelets pedicellate, without stipelike calluses; lemmas unawned; widespread native species</description>
      <determination>5.02 Leersia</determination>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Leaf blades floating, pseudopetiolate, elliptic to ovate or ovate-lanceolate; spikelets on stipelike calluses (1)2-10 mm long; lemmas awned; aquatic ornamental species, not known to be established in the Flora region</description>
      <determination>5.03 Hygroryza</determination>
    </key_statement>
  </key>
</bio:treatment>