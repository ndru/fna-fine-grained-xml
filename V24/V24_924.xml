<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">654</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">AGROSTIS</taxon_name>
    <taxon_name authority="Rydb." date="unknown" rank="species">variabilis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus agrostis;species variabilis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>21</number>
  <other_name type="common_name">Mountain bent</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, rarely rhizomatous, rhizomes to 2 cm.</text>
      <biological_entity id="o17121" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o17122" name="rhizom" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 5-30 cm, erect, sometimes geniculate at the base, with 2-5 (7) nodes.</text>
      <biological_entity id="o17123" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="30" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="at base" constraintid="o17124" is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
      </biological_entity>
      <biological_entity id="o17124" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o17125" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s2" value="7" value_original="7" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s2" to="5" />
      </biological_entity>
      <relation from="o17123" id="r2739" name="with" negation="false" src="d0_s2" to="o17125" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly basal, forming dense tufts;</text>
      <biological_entity id="o17126" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o17127" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o17128" name="tuft" name_original="tufts" src="d0_s3" type="structure">
        <character is_modifier="true" name="density" src="d0_s3" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o17126" id="r2740" name="forming" negation="false" src="d0_s3" to="o17128" />
    </statement>
    <statement id="d0_s4">
      <text>sheaths smooth;</text>
      <biological_entity id="o17129" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules (0.7) 1-2.8 mm, dorsal surfaces usually scabridulous, sometimes smooth, apices rounded to truncate, lacerate to erose;</text>
      <biological_entity id="o17130" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character name="atypical_some_measurement" src="d0_s5" unit="mm" value="0.7" value_original="0.7" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o17131" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o17132" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="truncate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lacerate" value_original="lacerate" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s5" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 3-7 cm long, 0.5-2 mm wide, flat, becoming folded or involute.</text>
      <biological_entity id="o17133" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s6" to="7" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="becoming" name="shape" src="d0_s6" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles (1) 2.5-6 cm long, 0.3-1.2 (2) cm wide, cylindric to lanceolate, usually dense, exserted from the upper sheaths at maturity, lowest node with 1-5 branches;</text>
      <biological_entity id="o17134" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="1" value_original="1" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s7" to="6" to_unit="cm" />
        <character name="width" src="d0_s7" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s7" to="1.2" to_unit="cm" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s7" to="lanceolate" />
        <character is_modifier="false" modifier="usually" name="density" src="d0_s7" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity constraint="upper" id="o17135" name="sheath" name_original="sheaths" src="d0_s7" type="structure" />
      <biological_entity constraint="lowest" id="o17136" name="node" name_original="node" src="d0_s7" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s7" value="maturity" value_original="maturity" />
      </biological_entity>
      <biological_entity id="o17137" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
      <relation from="o17134" id="r2741" name="exserted from the" negation="false" src="d0_s7" to="o17135" />
      <relation from="o17134" id="r2742" name="at" negation="false" src="d0_s7" to="o17136" />
      <relation from="o17136" id="r2743" name="with" negation="false" src="d0_s7" to="o17137" />
    </statement>
    <statement id="d0_s8">
      <text>branches usually scabridulous, sometimes smooth, ascending to erect, branching at or near the base and spikelet-bearing to the base, to branching in the distal 2/3, lower branches 0.5-1.5 cm;</text>
      <biological_entity id="o17138" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="relief" src="d0_s8" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s8" to="erect" />
        <character constraint="at or near base" constraintid="o17139" is_modifier="false" name="architecture" src="d0_s8" value="branching" value_original="branching" />
        <character constraint="in distal 2/3" constraintid="o17141" is_modifier="false" name="architecture" notes="" src="d0_s8" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o17139" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity id="o17140" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity constraint="distal" id="o17141" name="2/3" name_original="2/3" src="d0_s8" type="structure" />
      <biological_entity constraint="lower" id="o17142" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="1.5" to_unit="cm" />
      </biological_entity>
      <relation from="o17138" id="r2744" name="to" negation="false" src="d0_s8" to="o17140" />
    </statement>
    <statement id="d0_s9">
      <text>pedicels 0.4-2.8 (4.3) mm.</text>
      <biological_entity id="o17143" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="4.3" value_original="4.3" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s9" to="2.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets ovate to lanceolate, greenish purple.</text>
      <biological_entity id="o17144" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s10" to="lanceolate" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="greenish purple" value_original="greenish purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Glumes subequal to equal, 1.8-2.5 mm, smooth, or scabrous on the keel and sometimes elsewhere, 1-veined, acute to acuminate;</text>
      <biological_entity id="o17145" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character constraint="on keel" constraintid="o17146" is_modifier="false" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="sometimes elsewhere; elsewhere" name="architecture" notes="" src="d0_s11" value="1-veined" value_original="1-veined" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s11" to="acuminate" />
      </biological_entity>
      <biological_entity id="o17146" name="keel" name_original="keel" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>callus hairs to 0.2 mm, sparse to abundant;</text>
      <biological_entity constraint="callus" id="o17147" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="0.2" to_unit="mm" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s12" to="abundant" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemmas 1.5-2 mm, smooth, translucent, (3) 5-veined, veins usually prominent distally, sometimes obscure throughout, apices acute, entire, usually unawned, rarely awned, awns to 1 (2.8) mm, arising beyond the midpoint, usually not reaching the lemma apices;</text>
      <biological_entity id="o17148" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s13" value="translucent" value_original="translucent" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="(3)5-veined" value_original="(3)5-veined" />
      </biological_entity>
      <biological_entity id="o17149" name="vein" name_original="veins" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually; distally" name="prominence" src="d0_s13" value="prominent" value_original="prominent" />
        <character is_modifier="false" modifier="sometimes; throughout" name="prominence" src="d0_s13" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o17150" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="usually; rarely" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o17151" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="2.8" value_original="2.8" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
        <character constraint="beyond midpoint" constraintid="o17152" is_modifier="false" name="orientation" src="d0_s13" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o17152" name="midpoint" name_original="midpoint" src="d0_s13" type="structure" />
      <biological_entity constraint="lemma" id="o17153" name="apex" name_original="apices" src="d0_s13" type="structure" />
      <relation from="o17151" id="r2745" modifier="usually not" name="reaching the" negation="false" src="d0_s13" to="o17153" />
    </statement>
    <statement id="d0_s14">
      <text>paleas to 0.2 mm, thin;</text>
      <biological_entity id="o17154" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s14" to="0.2" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s14" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 3, 0.4-0.7 (1) mm.</text>
      <biological_entity id="o17155" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="3" value_original="3" />
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s15" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Caryopses 1-1.3 mm;</text>
      <biological_entity id="o17156" name="caryopse" name_original="caryopses" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>endosperm soft.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 28.</text>
      <biological_entity id="o17157" name="endosperm" name_original="endosperm" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s17" value="soft" value_original="soft" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17158" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Agrostis variabilis grows in alpine and subalpine meadows and forests and on talus slopes, at elevations up to 4000 m, from British Columbia and Alberta south to California and New Mexico. It can appear similar to dwarf forms of Podagrostis humilis (p. 694), but differs from that species in not having paleas.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.;N.Mex.;Wash.;Utah;Alta.;B.C.;Idaho;Mont.;Wyo.;Calif.;Nev.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>