<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">36</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">EHRHARTOIDEAE</taxon_name>
    <taxon_name authority="Nevski" date="unknown" rank="tribe">EHRHARTEAE</taxon_name>
    <taxon_name authority="Thunb." date="unknown" rank="genus">EHRHARTA</taxon_name>
    <taxon_name authority="Sm." date="unknown" rank="species">longiflora</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily ehrhartoideae;tribe ehrharteae;genus ehrharta;species longiflora</taxon_hierarchy>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Longflowered veldtgrass</other_name>
  <other_name type="common_name">Annual veldtgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o30760" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 15-90 cm, erect or often geniculate basally, often with secondary inflorescences developing from the lower nodes.</text>
      <biological_entity id="o30761" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s1" to="90" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="often; basally" name="shape" src="d0_s1" value="geniculate" value_original="geniculate" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o30762" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure">
        <character constraint="from lower nodes" constraintid="o30763" is_modifier="false" name="development" src="d0_s1" value="developing" value_original="developing" />
      </biological_entity>
      <biological_entity constraint="lower" id="o30763" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <relation from="o30761" id="r4844" modifier="often" name="with" negation="false" src="d0_s1" to="o30762" />
    </statement>
    <statement id="d0_s2">
      <text>Sheaths obviously veined, keeled, submembranous, sparsely pubescent at the base;</text>
      <biological_entity id="o30764" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="obviously" name="architecture" src="d0_s2" value="veined" value_original="veined" />
        <character is_modifier="false" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="texture" src="d0_s2" value="submembranous" value_original="submembranous" />
        <character constraint="at base" constraintid="o30765" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o30765" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>auricles ciliate;</text>
      <biological_entity id="o30766" name="auricle" name_original="auricles" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 1-2.5 mm, lacerate;</text>
      <biological_entity id="o30767" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 6-20 cm long, (1) 2.5-15 mm wide, flat, usually softly pubescent or glabrescent, sometimes scabridulous or smooth.</text>
      <biological_entity id="o30768" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s5" to="20" to_unit="cm" />
        <character name="width" src="d0_s5" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s5" to="15" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="usually softly" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" modifier="sometimes" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="relief" src="d0_s5" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Primary panicles 9-15 cm, erect, open, sometimes reduced to a raceme;</text>
      <biological_entity constraint="primary" id="o30769" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s6" to="15" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character constraint="to raceme" constraintid="o30770" is_modifier="false" modifier="sometimes" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o30770" name="raceme" name_original="raceme" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>branches usually ascending, sometimes spreading;</text>
      <biological_entity id="o30771" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicels usually curved or bent, sometimes flexuous.</text>
      <biological_entity id="o30772" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="course" src="d0_s8" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s8" value="bent" value_original="bent" />
        <character is_modifier="false" modifier="sometimes" name="course" src="d0_s8" value="flexuous" value_original="flexuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 8-30 mm, including the awns.</text>
      <biological_entity id="o30773" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30774" name="awn" name_original="awns" src="d0_s9" type="structure" />
      <relation from="o30773" id="r4845" name="including the" negation="false" src="d0_s9" to="o30774" />
    </statement>
    <statement id="d0_s10">
      <text>Lower glumes 3-3.5 mm long, 0.7-0.8 mm wide, 5-veined;</text>
      <biological_entity constraint="lower" id="o30775" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s10" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="5-veined" value_original="5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 4-4.5 mm long, to 3/4 the length of the spikelets, 1-1.5 mm wide, 7-veined;</text>
      <biological_entity constraint="upper" id="o30776" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s11" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s11" to="3/4" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" notes="" src="d0_s11" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="7-veined" value_original="7-veined" />
      </biological_entity>
      <biological_entity id="o30777" name="spikelet" name_original="spikelets" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>sterile lemmas 6-13 mm, indurate, glabrous or sparsely hispidulous, sometimes faintly transversely rugose, scabrous distally, lower sterile lemmas 3-7-veined, awned, awns 2-20 mm, upper sterile lemmas with a short stalklike base and 2 inconspicuously bearded ridges, awned or unawned;</text>
      <biological_entity id="o30778" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="13" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s12" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="hispidulous" value_original="hispidulous" />
        <character is_modifier="false" modifier="sometimes faintly transversely" name="relief" src="d0_s12" value="rugose" value_original="rugose" />
        <character is_modifier="false" modifier="distally" name="pubescence_or_relief" src="d0_s12" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="lower" id="o30779" name="lemma" name_original="lemmas" src="d0_s12" type="structure" />
      <biological_entity id="o30780" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-7-veined" value_original="3-7-veined" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o30781" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="upper" id="o30782" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s12" value="awned" value_original="awned" />
        <character name="architecture_or_shape" src="d0_s12" value="unawned" value_original="unawned" />
      </biological_entity>
      <biological_entity id="o30783" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s12" value="short" value_original="short" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s12" value="stalklike" value_original="stalklike" />
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o30784" name="ridge" name_original="ridges" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="inconspicuously" name="pubescence" src="d0_s12" value="bearded" value_original="bearded" />
      </biological_entity>
      <relation from="o30782" id="r4846" name="with" negation="false" src="d0_s12" to="o30783" />
    </statement>
    <statement id="d0_s13">
      <text>bisexual lemmas 4-7 mm, 7-veined, unawned;</text>
      <biological_entity id="o30785" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="7-veined" value_original="7-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers 3, about 1.2 mm.</text>
      <biological_entity id="o30786" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
        <character name="some_measurement" src="d0_s14" unit="mm" value="1.2" value_original="1.2" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Caryopses about 3-4 mm. 2n = 24, 48.</text>
      <biological_entity id="o30787" name="caryopse" name_original="caryopses" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30788" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="24" value_original="24" />
        <character name="quantity" src="d0_s15" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ehrharta longiflora is a southern African species, well-established in Australia, that in the Flora region is established near Torrey Pines State Park in southern California. It is said to prefer shaded areas on hillsides and disturbed areas such as gardens and roadsides, usually growing in light sandy to loamy soils. Three varieties have been described; they are not treated here.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>