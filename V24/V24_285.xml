<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">206</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">BROMEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">BROMUS</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="section">Bromopsis</taxon_name>
    <taxon_name authority="Leyss." date="unknown" rank="species">inermis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe bromeae;genus bromus;section bromopsis;species inermis</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bromus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">inermis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="forma">villosus</taxon_name>
    <taxon_hierarchy>genus bromus;species inermis;forma villosus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bromus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">inermis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="forma">aristatus</taxon_name>
    <taxon_hierarchy>genus bromus;species inermis;forma aristatus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bromopsis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">inermis</taxon_name>
    <taxon_hierarchy>genus bromopsis;species inermis</taxon_hierarchy>
  </taxon_identification>
  <number>8</number>
  <other_name type="common_name">Smooth brome</other_name>
  <other_name type="common_name">Hungarian brome</other_name>
  <other_name type="common_name">Brome inerme</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, rhizomes short to long-creeping.</text>
      <biological_entity id="o1852" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1853" name="rhizom" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="long-creeping" value_original="long-creeping" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 50-130 cm, erect, single or a few together;</text>
      <biological_entity id="o1854" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s2" to="130" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="quantity" src="d0_s2" value="single" value_original="single" />
        <character is_modifier="false" modifier="together" name="quantity" src="d0_s2" value="few" value_original="few" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes (2) 3-5 (6), usually glabrous, rarely pubescent;</text>
      <biological_entity id="o1855" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character name="atypical_quantity" src="d0_s3" value="2" value_original="2" />
        <character name="atypical_quantity" src="d0_s3" value="6" value_original="6" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="5" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>internodes usually glabrous, rarely pubescent.</text>
      <biological_entity id="o1856" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths usually glab¬rous, rarely pubescent or pilose;</text>
      <biological_entity id="o1857" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>auricles sometimes present;</text>
      <biological_entity id="o1858" name="auricle" name_original="auricles" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules to 3 mm, glabrous, truncate, erose;</text>
      <biological_entity id="o1859" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s7" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blades 11-35 (42) cm long, 5-15 mm wide, flat, usually glabrous, rarely pubescent or pilose.</text>
      <biological_entity id="o1860" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character name="length" src="d0_s8" unit="cm" value="42" value_original="42" />
        <character char_type="range_value" from="11" from_unit="cm" name="length" src="d0_s8" to="35" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s8" to="15" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Panicles 10-20 cm, open, erect;</text>
      <biological_entity id="o1861" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s9" to="20" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>branches ascending or spreading.</text>
      <biological_entity id="o1862" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 20-40 mm, elliptic to lanceolate, terete to moderately laterally compressed, sometimes purplish, with (5) 8-10 florets.</text>
      <biological_entity id="o1863" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s11" to="40" to_unit="mm" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s11" to="lanceolate terete" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s11" to="lanceolate terete" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s11" value="purplish" value_original="purplish" />
      </biological_entity>
      <biological_entity id="o1864" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s11" value="5" value_original="5" />
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s11" to="10" />
      </biological_entity>
      <relation from="o1863" id="r289" name="with" negation="false" src="d0_s11" to="o1864" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes glabrous;</text>
      <biological_entity id="o1865" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes (4) 6-8 (9) mm, 1 (3) -veined;</text>
      <biological_entity constraint="lower" id="o1866" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="4" value_original="4" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="1(3)-veined" value_original="1(3)-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes (5) 7-10 mm, 3-veined;</text>
      <biological_entity constraint="upper" id="o1867" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="5" value_original="5" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 9-13 mm, elliptic to lanceolate, rounded over the midvein, usually glabrous and smooth, sometimes scabrous, margins sometimes sparsely puberulent, the basal part of the backs less frequently so, apices acute to obtuse, entire;</text>
      <biological_entity id="o1868" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s15" to="13" to_unit="mm" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s15" to="lanceolate" />
        <character constraint="over midvein" constraintid="o1869" is_modifier="false" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="usually" name="pubescence" notes="" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sometimes" name="pubescence_or_relief" src="d0_s15" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o1869" name="midvein" name_original="midvein" src="d0_s15" type="structure" />
      <biological_entity id="o1870" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s15" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1871" name="part" name_original="part" src="d0_s15" type="structure" />
      <biological_entity id="o1872" name="back" name_original="backs" src="d0_s15" type="structure" />
      <biological_entity id="o1873" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s15" to="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o1871" id="r290" name="part_of" negation="false" src="d0_s15" to="o1872" />
    </statement>
    <statement id="d0_s16">
      <text>awns absent or to 3 mm, straight, arising less than 1.5 mm below the lemma apices;</text>
      <biological_entity id="o1874" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s16" value="0-3 mm" value_original="0-3 mm" />
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="arising" value_original="arising" />
        <character constraint="below lemma apices" constraintid="o1875" modifier="less than" name="some_measurement" src="d0_s16" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o1875" name="apex" name_original="apices" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>anthers 3.5-6 mm. 2n = 28, 56.</text>
      <biological_entity id="o1876" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s17" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1877" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="28" value_original="28" />
        <character name="quantity" src="d0_s17" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bromus inermis is native to Eurasia, and is now found in disturbed sites in Alaska, Greenland, and most of Canada as well as south throughout most of the contiguous United States except the southeast. It has also been used for rehabilitation, and is planted extensively for forage in pastures and rangelands from Alaska and the Yukon Territory toTexas.</discussion>
  <discussion>Bromus inermis is similar to B. pumpellianus, differing mainly in having glabrous lemmas, nodes, and leaf blades, but lack of pubescence is not a consistently reliable distinguishing character. Bromus inermis also resembles a recently introduced species, B. riparius, from which it differs primarily in its shorter or nonexistent awns.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Pa.;Conn.;N.J.;N.Y.;Wash.;Va.;Del.;D.C.;Wis.;W.Va.;Kans.;Minn.;N.Dak.;Nebr.;Okla.;S.Dak.;Mass.;Maine;N.H.;R.I.;Vt.;Wyo.;N.Mex.;Tex.;La.;Ariz.;Idaho;Mich.;N.C.;Tenn.;Calif.;Nev.;Ark.;Colo.;Ga.;Iowa;Ill.;Ind.;Ky.;Md.;Mo.;Miss.;Mont.;Ohio;Oreg.;Utah;Alaska;Alta.;B.C.;Greenland;Man.;N.B.;Nfld. and Labr.;N.S.;N.W.T.;Nunavut;Ont.;P.E.I.;Que.;Sask.;Yukon;S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>