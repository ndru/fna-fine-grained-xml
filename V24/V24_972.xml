<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">688</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="C.E. Hubb." date="unknown" rank="genus">PARAPHOLIS</taxon_name>
    <taxon_name authority="(L.) C.E. Hubb." date="unknown" rank="species">incurva</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus parapholis;species incurva</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Curved sicklegrass</other_name>
  <other_name type="common_name">Sicklegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 2-35 cm, erect to decumbent, smooth, glabrous, branching at any node.</text>
      <biological_entity id="o17634" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="decumbent" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s0" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character constraint="at node" constraintid="o17635" is_modifier="false" name="architecture" src="d0_s0" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o17635" name="node" name_original="node" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Sheaths of upper leaves strongly inflated, margins expanded, enclosing the lowest spikelets;</text>
      <biological_entity id="o17636" name="sheath" name_original="sheaths" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s1" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity constraint="upper" id="o17637" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o17638" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o17639" name="spikelet" name_original="spikelets" src="d0_s1" type="structure" />
      <relation from="o17636" id="r2816" name="part_of" negation="false" src="d0_s1" to="o17637" />
      <relation from="o17638" id="r2817" name="enclosing the" negation="false" src="d0_s1" to="o17639" />
    </statement>
    <statement id="d0_s2">
      <text>ligules to 1.5 mm;</text>
      <biological_entity id="o17640" name="ligule" name_original="ligules" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 1-3 (10) cm long, 1-3 mm wide, adaxial surfaces scabrid.</text>
      <biological_entity id="o17641" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character name="length" src="d0_s3" unit="cm" value="10" value_original="10" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17642" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s3" value="scabrid" value_original="scabrid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikes 1-15 cm, solitary, curved and twisted, rigid, with 2-20 spikelets.</text>
      <biological_entity id="o17643" name="spike" name_original="spikes" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="15" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="course" src="d0_s4" value="curved" value_original="curved" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="texture" src="d0_s4" value="rigid" value_original="rigid" />
      </biological_entity>
      <biological_entity id="o17644" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s4" to="20" />
      </biological_entity>
      <relation from="o17643" id="r2818" name="with" negation="false" src="d0_s4" to="o17644" />
    </statement>
    <statement id="d0_s5">
      <text>Spikelets 4.5-7.5 mm, usually slightly longer than the internodes, more or less cleistogamous.</text>
      <biological_entity id="o17645" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s5" to="7.5" to_unit="mm" />
        <character constraint="than the internodes" constraintid="o17646" is_modifier="false" name="length_or_size" src="d0_s5" value="usually slightly longer" value_original="usually slightly longer" />
        <character is_modifier="false" modifier="more or less" name="reproduction" src="d0_s5" value="cleistogamous" value_original="cleistogamous" />
      </biological_entity>
      <biological_entity id="o17646" name="internode" name_original="internodes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Glumes lanceolate, acuminate, keels rarely slightly winged;</text>
      <biological_entity id="o17647" name="glume" name_original="glumes" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o17648" name="keel" name_original="keels" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="rarely slightly" name="architecture" src="d0_s6" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers 0.5-1.3 mm. 2n = 32, 36, 38, 42.</text>
      <biological_entity id="o17649" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17650" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="32" value_original="32" />
        <character name="quantity" src="d0_s7" value="36" value_original="36" />
        <character name="quantity" src="d0_s7" value="38" value_original="38" />
        <character name="quantity" src="d0_s7" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Parapholis incurva is established at various locations on the coasts of the contiguous United States. It grows in both poorly drained and well-drained disturbed soils, at and above the high tide mark. It tends to grow in more saline soils, and at lower elevations with respect to the tide, than P. strigosa.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md.;N.J.;N.C.;Del.;Tex.;La.;Calif.;Mich.;Ala.;Va.;Pa.;Miss.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>