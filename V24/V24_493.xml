<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">346</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Bee F. Gunn</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="B.R. Baum" date="unknown" rank="genus">×ELYLEYMUS</taxon_name>
    <taxon_name authority="(Lepage) Barkworth" date="unknown" rank="species">mossii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus ×elyleymus;species mossii</taxon_hierarchy>
  </taxon_identification>
  <number>5</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, sometimes rhizomatous.</text>
      <biological_entity id="o7964" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms to 80 cm, glabrous.</text>
      <biological_entity id="o7965" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="80" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths smooth, glabrous;</text>
      <biological_entity id="o7966" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>auricles 0.5-1.5 mm;</text>
      <biological_entity id="o7967" name="auricle" name_original="auricles" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.5-1.2 mm;</text>
      <biological_entity id="o7968" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 3-7 mm wide, flat, adaxial surfaces and margins scabrous.</text>
      <biological_entity id="o7969" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="7" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7970" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7971" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences spikes, 8-15 cm long, 10-25 mm wide including the awns, 8-15 mm wide excluding the awns, with (1) 2 sessile or subsessile spikelets per node;</text>
      <biological_entity constraint="inflorescences" id="o7972" name="spike" name_original="spikes" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s6" to="15" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s6" to="25" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7973" name="awn" name_original="awns" src="d0_s6" type="structure" />
      <biological_entity id="o7974" name="awn" name_original="awns" src="d0_s6" type="structure" />
      <biological_entity id="o7975" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s6" value="1" value_original="1" />
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="subsessile" value_original="subsessile" />
      </biological_entity>
      <biological_entity id="o7976" name="node" name_original="node" src="d0_s6" type="structure" />
      <relation from="o7972" id="r1291" name="including the" negation="false" src="d0_s6" to="o7973" />
      <relation from="o7972" id="r1292" name="excluding the" negation="false" src="d0_s6" to="o7974" />
      <relation from="o7972" id="r1293" name="with" negation="false" src="d0_s6" to="o7975" />
      <relation from="o7975" id="r1294" name="per" negation="false" src="d0_s6" to="o7976" />
    </statement>
    <statement id="d0_s7">
      <text>internodes 4-6 mm, scabrous or hispid on the angles.</text>
      <biological_entity id="o7977" name="internode" name_original="internodes" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="scabrous" value_original="scabrous" />
        <character constraint="on angles" constraintid="o7978" is_modifier="false" name="pubescence" src="d0_s7" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o7978" name="angle" name_original="angles" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 12-17 mm, usually with 5 florets.</text>
      <biological_entity id="o7979" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s8" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7980" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="5" value_original="5" />
      </biological_entity>
      <relation from="o7979" id="r1295" modifier="usually" name="with" negation="false" src="d0_s8" to="o7980" />
    </statement>
    <statement id="d0_s9">
      <text>Glumes 9-15 mm long including the awns, 0.8-1 mm wide, 1-3-veined, hairy;</text>
      <biological_entity id="o7981" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s9" to="15" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s9" to="1" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="1-3-veined" value_original="1-3-veined" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o7982" name="awn" name_original="awns" src="d0_s9" type="structure" />
      <relation from="o7981" id="r1296" name="including the" negation="false" src="d0_s9" to="o7982" />
    </statement>
    <statement id="d0_s10">
      <text>lemmas 8-13 mm, villous, awned, awns 4-15 mm;</text>
      <biological_entity id="o7983" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="13" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="villous" value_original="villous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o7984" name="awn" name_original="awns" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>paleas 8-10 mm, glabrous between the keels, margins shortly ciliate;</text>
      <biological_entity id="o7985" name="palea" name_original="paleas" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
        <character constraint="between keels" constraintid="o7986" is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7986" name="keel" name_original="keels" src="d0_s11" type="structure" />
      <biological_entity id="o7987" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="shortly" name="architecture_or_pubescence_or_shape" src="d0_s11" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 2-3 mm.</text>
      <biological_entity id="o7988" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Lepage (1965) stated that it was obvious that Elymus canadensis was one parent of this hybrid, but that it would be necessary to discover which species of Agropyron [in the traditional sense] grew in the neighborhood to determine the other parent. He gave "Agropyron (?) trachycaulum" as a possibility. Elymus canadensis, however, is generally absent from the region around Lake Louise, Alberta (Moss 1983), where the holotype was collected. Barkworth (2006) argued that the parents are probably E. glaucus and Leymus innovatus, both species that are common in the holotype area.</discussion>
  
</bio:treatment>