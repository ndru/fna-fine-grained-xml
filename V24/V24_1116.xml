<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">784</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ALOPECURUS</taxon_name>
    <taxon_name authority="Sobol." date="unknown" rank="species">aequalis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">aequalis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus alopecurus;species aequalis;variety aequalis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Alopecurus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">aequalis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">natans</taxon_name>
    <taxon_hierarchy>genus alopecurus;species aequalis;variety natans</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <other_name type="common_name">Shortawn foxtail</other_name>
  <other_name type="common_name">Vulpin a courtes aretes</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 9-75 cm.</text>
      <biological_entity id="o6691" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s0" to="75" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Blades 1-5 (8) mm wide.</text>
      <biological_entity id="o6692" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character name="width" src="d0_s1" unit="mm" value="8" value_original="8" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Panicles 1-9 mm long, 3-6 mm wide.</text>
      <biological_entity id="o6693" name="panicle" name_original="panicles" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s2" to="9" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Spikelets usually not purplish-tinged;</text>
      <biological_entity id="o6694" name="spikelet" name_original="spikelets" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually not" name="coloration" src="d0_s3" value="purplish-tinged" value_original="purplish-tinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>glumes 1.8-3 mm;</text>
      <biological_entity id="o6695" name="glume" name_original="glumes" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>awns not exceeding the lemmas or exceeding them by less than 1 mm;</text>
      <biological_entity id="o6696" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character constraint="by 0-1 mm" is_modifier="false" name="position_relational" src="d0_s5" value="exceeding" value_original="exceeding" />
      </biological_entity>
      <biological_entity id="o6697" name="lemma" name_original="lemmas" src="d0_s5" type="structure" />
      <relation from="o6696" id="r1086" name="exceeding the" negation="true" src="d0_s5" to="o6697" />
    </statement>
    <statement id="d0_s6">
      <text>anthers 0.5-0.9 mm.</text>
      <biological_entity id="o6698" name="anther" name_original="anthers" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Alopecurus aequalis var. aequalis is the widespread variety in the Flora region. It is listed as threatened in the state of Connecticut.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Minn.;Wash.;Del.;Wis.;Ariz.;Calif.;Conn.;Ill.;Ind.;Kans.;Ky.;Mass.;Md.;Maine;Mich.;Mo.;Mont.;N.Dak.;Nebr.;N.Y.;Oreg.;Pa.;S.Dak.;Tenn.;Utah;Va.;Wyo.;Alta.;B.C.;Greenland;Man.;N.B.;Nfld. and Labr.;N.S.;N.W.T.;Ont.;P.E.I.;Que.;Sask.;Yukon;N.H.;N.J.;N.Mex.;Nev.;Colo.;Alaska;Vt.;Iowa;Idaho;Ohio</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>