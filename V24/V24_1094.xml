<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PHALARIS</taxon_name>
    <taxon_name authority="Walter" date="unknown" rank="species">caroliniana</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus phalaris;species caroliniana</taxon_hierarchy>
  </taxon_identification>
  <number>10</number>
  <other_name type="common_name">Carolina canarygrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o29693" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms to 150 cm.</text>
      <biological_entity id="o29694" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="150" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Ligules 1.5-7 mm, truncate to broadly acute;</text>
      <biological_entity id="o29695" name="ligule" name_original="ligules" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s2" to="7" to_unit="mm" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s2" to="broadly acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 1.5-15 cm long, 2-11 mm wide, smooth, shiny green, apices acuminate.</text>
      <biological_entity id="o29696" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="11" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reflectance" src="d0_s3" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o29697" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Panicles 0.5-8 (8.5) cm long, 0.8-2 cm wide, ovoid to subcylindrical, not lobed;</text>
      <biological_entity id="o29698" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character name="length" src="d0_s4" unit="cm" value="8.5" value_original="8.5" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s4" to="2" to_unit="cm" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s4" to="subcylindrical" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches not evident, spikelets borne singly, not clustered.</text>
      <biological_entity id="o29699" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s5" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o29700" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
        <character is_modifier="false" modifier="not" name="arrangement_or_growth_form" src="d0_s5" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spikelets homogamous, with 3 florets, terminal floret bisexual;</text>
      <biological_entity id="o29701" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="homogamous" value_original="homogamous" />
      </biological_entity>
      <biological_entity id="o29702" name="floret" name_original="florets" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="3" value_original="3" />
      </biological_entity>
      <relation from="o29701" id="r4680" name="with" negation="false" src="d0_s6" to="o29702" />
    </statement>
    <statement id="d0_s7">
      <text>disarticulation above the glumes, beneath the sterile florets.</text>
      <biological_entity constraint="terminal" id="o29703" name="floret" name_original="floret" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o29704" name="glume" name_original="glumes" src="d0_s7" type="structure" />
      <biological_entity id="o29705" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o29703" id="r4681" name="above" negation="false" src="d0_s7" to="o29704" />
    </statement>
    <statement id="d0_s8">
      <text>Glumes 3.8-6 (8) mm long, 0.8-1.5 mm wide, keels smooth or scabridulous, narrowly to broadly winged distally, wings 0.1-0.5 mm wide, entire, smooth, lateral-veins prominent, usually smooth, sometimes scabridulous, apices acute or acuminate;</text>
      <biological_entity id="o29706" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character name="length" src="d0_s8" unit="mm" value="8" value_original="8" />
        <character char_type="range_value" from="3.8" from_unit="mm" name="length" src="d0_s8" to="6" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29707" name="keel" name_original="keels" src="d0_s8" type="structure">
        <character is_modifier="false" name="relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s8" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="narrowly to broadly; distally" name="architecture" src="d0_s8" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o29708" name="wing" name_original="wings" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s8" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o29709" name="lateral-vein" name_original="lateral-veins" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="prominent" value_original="prominent" />
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sometimes" name="relief" src="d0_s8" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o29710" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sterile florets 2, equal to subequal, 1.5-2.5 mm, 1/2 or more the length of the bisexual florets, the basal 0.2-0.5 mm glabrous, the remainder hairy;</text>
      <biological_entity id="o29711" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="sterile" value_original="sterile" />
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
        <character name="quantity" src="d0_s9" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o29712" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity constraint="basal" id="o29713" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" src="d0_s9" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bisexual florets 2.9-4.7 mm long, 0.9-1.8 mm wide, shiny, stramineous when immature, brown when mature, apices hairy, acuminate to beaked;</text>
      <biological_entity id="o29714" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="2.9" from_unit="mm" name="length" src="d0_s10" to="4.7" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s10" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s10" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="when immature" name="coloration" src="d0_s10" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" modifier="when mature" name="coloration" src="d0_s10" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity id="o29715" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s10" to="beaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers 1.5-2 mm. 2n = 14.</text>
      <biological_entity id="o29716" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29717" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Phalaris caroliniana grows in wet, marshy, and swampy ground. It is a common species in suitable habitats through much of the southern portion of the Flora region and in northern Mexico. It has also been found in Puerto Rico, where it may be an introduction, and in Europe and Australia, where it is undoubtedly an introduction. It appears to hybridize with P. lemmonii and P. caroliniana (Baldini 1995).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Del.;Fla.;N.Mex.;Tex.;La.;Tenn.;N.C.;S.C.;Nev.;Va.;Colo.;Calif.;Ala.;Ark.;Ga.;Ariz.;Md.;Kans.;Okla.;Ohio;Mo.;Miss.;Ky.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>