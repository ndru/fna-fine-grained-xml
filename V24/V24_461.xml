<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">327</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ELYMUS</taxon_name>
    <taxon_name authority="(Scribn. &amp; J.G. Sm.) Gould" date="unknown" rank="species">lanceolatus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus elymus;species lanceolatus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agropyron</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">dasystachyum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">riparium</taxon_name>
    <taxon_hierarchy>genus agropyron;species dasystachyum;variety riparium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agropyron</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">dasystachyum</taxon_name>
    <taxon_hierarchy>genus agropyron;species dasystachyum</taxon_hierarchy>
  </taxon_identification>
  <number>27</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants strongly rhizomatous, sometimes glaucous.</text>
      <biological_entity id="o31536" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 22-130 cm, erect;</text>
      <biological_entity id="o31537" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="22" from_unit="cm" name="some_measurement" src="d0_s1" to="130" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes glabrous.</text>
      <biological_entity id="o31538" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves often mostly basal, sometimes more evenly distributed;</text>
      <biological_entity id="o31539" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes; evenly" name="arrangement" notes="" src="d0_s3" value="distributed" value_original="distributed" />
      </biological_entity>
      <biological_entity id="o31540" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="often mostly" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths glabrous or pubescent;</text>
      <biological_entity id="o31541" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>auricles usually present on the lower leaves, 0.5-1.5 mm;</text>
      <biological_entity id="o31542" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character constraint="on lower leaves" constraintid="o31543" is_modifier="false" modifier="usually" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lower" id="o31543" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.1-0.5 mm, erose, sometimes ciliolate;</text>
      <biological_entity id="o31544" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s6" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s6" value="erose" value_original="erose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s6" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 1.5-6 mm wide, generally involute, abaxial surfaces usually glabrous, adaxial surfaces strigose, ribs subequal in size and spacing.</text>
      <biological_entity id="o31545" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s7" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="generally" name="shape_or_vernation" src="d0_s7" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o31546" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o31547" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o31548" name="rib" name_original="ribs" src="d0_s7" type="structure">
        <character constraint="in size and spacing" is_modifier="false" name="size" src="d0_s7" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikes 3.5-26 cm long, 0.5-1 cm wide, erect to slightly nodding, usually with 1 spikelet per node, sometimes with 2 at a few nodes;</text>
      <biological_entity id="o31549" name="spike" name_original="spikes" src="d0_s8" type="structure">
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s8" to="26" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s8" to="1" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s8" to="slightly nodding" />
      </biological_entity>
      <biological_entity id="o31550" name="spikelet" name_original="spikelet" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o31551" name="node" name_original="node" src="d0_s8" type="structure">
        <character constraint="at nodes" constraintid="o31552" modifier="with" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o31552" name="node" name_original="nodes" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="few" value_original="few" />
      </biological_entity>
      <relation from="o31549" id="r4951" modifier="usually" name="with" negation="false" src="d0_s8" to="o31550" />
      <relation from="o31550" id="r4952" name="per" negation="false" src="d0_s8" to="o31551" />
    </statement>
    <statement id="d0_s9">
      <text>internodes 3.5-15 mm long, 0.1-0.8 mm wide, glabrous or hairy.</text>
      <biological_entity id="o31553" name="internode" name_original="internodes" src="d0_s9" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s9" to="15" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s9" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 8-31 mm, 1.5-3 times longer than the internodes, appressed, with 3-11 florets;</text>
      <biological_entity id="o31554" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="31" to_unit="mm" />
        <character is_modifier="false" name="length_or_size" src="d0_s10" value="1.5-3 times longer than" value_original="1.5-3 times longer than" />
        <character is_modifier="false" name="fixation_or_orientation" notes="" src="d0_s10" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o31555" name="internode" name_original="internodes" src="d0_s10" type="structure" />
      <biological_entity id="o31556" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s10" to="11" />
      </biological_entity>
      <relation from="o31554" id="r4953" name="with" negation="false" src="d0_s10" to="o31556" />
    </statement>
    <statement id="d0_s11">
      <text>rachillas glabrous or hairy, hairs to 1 mm;</text>
      <biological_entity id="o31557" name="rachilla" name_original="rachillas" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>disarticulation above the glumes, beneath each floret.</text>
      <biological_entity id="o31558" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31559" name="glume" name_original="glumes" src="d0_s12" type="structure" />
      <biological_entity id="o31560" name="floret" name_original="floret" src="d0_s12" type="structure" />
      <relation from="o31558" id="r4954" name="above" negation="false" src="d0_s12" to="o31559" />
    </statement>
    <statement id="d0_s13">
      <text>Glumes subequal, 5-14 mm long, 1/2 - 3/4 the length of the adjacent lemmas, 0.7-1.3 mm wide, lanceolate, glabrous or hairy, smooth or scabrous, 3-5-veined, flat or weakly, often asymmetrically keeled, keels straight, margins narrow, tapering from the base or from beyond midlength, apices acute to acuminate, sometimes mucronate or shortly awned;</text>
      <biological_entity id="o31561" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s13" to="14" to_unit="mm" />
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s13" to="3/4" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" notes="" src="d0_s13" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s13" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="3-5-veined" value_original="3-5-veined" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s13" value="flat" value_original="flat" />
        <character name="prominence_or_shape" src="d0_s13" value="weakly" value_original="weakly" />
        <character is_modifier="false" modifier="often asymmetrically" name="shape" src="d0_s13" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o31562" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s13" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o31563" name="keel" name_original="keels" src="d0_s13" type="structure">
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o31564" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s13" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="shape" src="d0_s13" value="tapering" value_original="tapering" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 7-12 mm, glabrous or hairy, hairs all alike, sometimes scabrous, acute to awn-tipped, awns to 2 mm, straight;</text>
      <biological_entity id="o31565" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o31566" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character is_modifier="false" name="variability" src="d0_s14" value="alike" value_original="alike" />
        <character is_modifier="false" modifier="sometimes" name="pubescence_or_relief" src="d0_s14" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="awn-tipped" value_original="awn-tipped" />
      </biological_entity>
      <biological_entity id="o31567" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>paleas about equal to the lemmas, keels straight below the apices, smooth or scabrous proximally, sometimes hairy, scabrous distally, intercostal region glabrous or with hairs, apices 0.2-0.3 mm wide;</text>
      <biological_entity id="o31568" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character constraint="to lemmas" constraintid="o31569" is_modifier="false" name="variability" src="d0_s15" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o31569" name="lemma" name_original="lemmas" src="d0_s15" type="structure" />
      <biological_entity id="o31570" name="keel" name_original="keels" src="d0_s15" type="structure">
        <character constraint="below apices" constraintid="o31571" is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s15" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s15" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="distally" name="pubescence_or_relief" src="d0_s15" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o31571" name="apex" name_original="apices" src="d0_s15" type="structure" />
      <biological_entity constraint="intercostal" id="o31572" name="region" name_original="region" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="with hairs" value_original="with hairs" />
      </biological_entity>
      <biological_entity id="o31573" name="hair" name_original="hairs" src="d0_s15" type="structure" />
      <biological_entity id="o31574" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s15" to="0.3" to_unit="mm" />
      </biological_entity>
      <relation from="o31572" id="r4955" name="with" negation="false" src="d0_s15" to="o31573" />
    </statement>
    <statement id="d0_s16">
      <text>anthers (2.5) 3-6 mm. 2n = 28.</text>
      <biological_entity id="o31575" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character name="atypical_some_measurement" src="d0_s16" unit="mm" value="2.5" value_original="2.5" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31576" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Elymus lanceolatus grows in sand and clay soils and dry to mesic habitats. It is found primarily in the western half of the Flora region, between the coastal mountains and 95° W longitude, with the exception of E. lanceolatus subsp. psammophilus, which extends around the Great Lakes. Three subspecies are recognized, primarily on the basis of their lemma and palea pubescence.</discussion>
  <discussion>Elymus lanceolatus is primarily outcrossing, and hybridizes with several species of Triticeae. Elymus albicans (p. 334) is thought to be derived from hybridization with the awned phase of Pseudoroegneria spicata (p. 281). Judging from specimens of controlled hybrids, hybridization with E. trachycaulus (p. 321) and unawned plants of P. spicata probably occur, but would be almost impossible to detect without careful observation in the field. Experimental hybrids are partially fertile, and capable of backcrossing to either parent (Dewey 1965, 1967, 1968, 1975, 1976).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.;Nev.;Colo.;N.Mex.;Alaska;Ill.;Mont.;Wyo.;Mich.;Wis.;Idaho;Alta.;B.C.;Man.;Ont.;Sask.;Yukon;Wash.;Ariz.;Calif.;N.Dak.;Nebr.;S.Dak.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lemmas densely hairy, hairs flexible, some 1 mm long or longer</description>
      <determination>Elymus lanceolatus subsp. psammophilus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lemmas glabrous or with stiff hairs shorter than 1 mm.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lemmas with hairs, not scabrous</description>
      <determination>Elymus lanceolatus subsp. lanceolatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lemmas smooth, sometimes scabrous distally, mosdy glabrous, sometimes the lemma margins hairy proximally</description>
      <determination>Elymus lanceolatus subsp. riparius</determination>
    </key_statement>
  </key>
</bio:treatment>