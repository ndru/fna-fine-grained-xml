<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">332</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ELYMUS</taxon_name>
    <taxon_name authority="Gould" date="unknown" rank="species">sierrae</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus elymus;species sierrae</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agropyron</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pringlei</taxon_name>
    <taxon_hierarchy>genus agropyron;species pringlei</taxon_hierarchy>
  </taxon_identification>
  <number>32</number>
  <other_name type="common_name">Sierra wheatgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, not rhizomatous.</text>
      <biological_entity id="o23722" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 20-50 cm, prostrate or decumbent and geniculate;</text>
      <biological_entity id="o23723" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="50" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="shape" src="d0_s1" value="geniculate" value_original="geniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes 1-2, exposed, glabrous.</text>
      <biological_entity id="o23724" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="2" />
        <character is_modifier="false" name="prominence" src="d0_s2" value="exposed" value_original="exposed" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basally concentrated;</text>
      <biological_entity id="o23725" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="basally" name="arrangement_or_density" src="d0_s3" value="concentrated" value_original="concentrated" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths glabrous;</text>
      <biological_entity id="o23726" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>auricles usually present, to 1 mm on the lower leaves;</text>
      <biological_entity id="o23727" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character char_type="range_value" constraint="on lower leaves" constraintid="o23728" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lower" id="o23728" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.2-0.5 mm, erose;</text>
      <biological_entity id="o23729" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s6" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s6" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 1-5 mm wide, flat, abaxial surfaces smooth, glabrous, adaxial surfaces prominently ridged over the veins, with scattered hairs, hairs to 0.2 mm, veins closely spaced.</text>
      <biological_entity id="o23730" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="5" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o23731" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o23732" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character constraint="over veins" constraintid="o23733" is_modifier="false" modifier="prominently" name="shape" src="d0_s7" value="ridged" value_original="ridged" />
      </biological_entity>
      <biological_entity id="o23733" name="vein" name_original="veins" src="d0_s7" type="structure" />
      <biological_entity id="o23734" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s7" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity id="o23735" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23736" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="closely" name="arrangement" src="d0_s7" value="spaced" value_original="spaced" />
      </biological_entity>
      <relation from="o23732" id="r3764" name="with" negation="false" src="d0_s7" to="o23734" />
    </statement>
    <statement id="d0_s8">
      <text>Spikes 5-15 cm long, 1.5-2.5 cm wide including the awns, 0.7-1.2 cm wide excluding the awns, flexuous, erect to nodding distally, with 1 spikelet at most nodes, occasionally some of the lower nodes with 2 spikelets;</text>
      <biological_entity id="o23737" name="spike" name_original="spikes" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s8" to="15" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s8" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s8" to="1.2" to_unit="cm" />
        <character is_modifier="false" name="course" src="d0_s8" value="flexuous" value_original="flexuous" />
        <character char_type="range_value" from="erect" modifier="distally" name="orientation" src="d0_s8" to="nodding" />
      </biological_entity>
      <biological_entity id="o23738" name="awn" name_original="awns" src="d0_s8" type="structure" />
      <biological_entity id="o23739" name="awn" name_original="awns" src="d0_s8" type="structure" />
      <biological_entity constraint="spikelet" id="o23740" name="0-node" name_original="0-nodes" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="lower" id="o23741" name="node" name_original="nodes" src="d0_s8" type="structure" />
      <biological_entity id="o23742" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <relation from="o23737" id="r3765" name="including the" negation="false" src="d0_s8" to="o23738" />
      <relation from="o23737" id="r3766" name="excluding the" negation="false" src="d0_s8" to="o23739" />
      <relation from="o23737" id="r3767" name="with" negation="false" src="d0_s8" to="o23740" />
      <relation from="o23737" id="r3768" modifier="occasionally" name="part_of" negation="false" src="d0_s8" to="o23741" />
      <relation from="o23737" id="r3769" name="with" negation="false" src="d0_s8" to="o23742" />
    </statement>
    <statement id="d0_s9">
      <text>internodes 5-15 mm long, 0.2-0.5 mm wide, both surfaces glabrous, edges ciliate, not scabrous.</text>
      <biological_entity id="o23743" name="internode" name_original="internodes" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s9" to="15" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s9" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23744" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23745" name="edge" name_original="edges" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" modifier="not" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 15-20 mm, ascending to divergent, with 3-7 florets;</text>
      <biological_entity id="o23746" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s10" to="20" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o23747" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s10" to="7" />
      </biological_entity>
      <relation from="o23746" id="r3770" name="with" negation="false" src="d0_s10" to="o23747" />
    </statement>
    <statement id="d0_s11">
      <text>rachillas glabrous;</text>
    </statement>
    <statement id="d0_s12">
      <text>disarticulation above the glumes, beneath each floret.</text>
      <biological_entity id="o23748" name="rachilla" name_original="rachillas" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23749" name="glume" name_original="glumes" src="d0_s12" type="structure" />
      <biological_entity id="o23750" name="floret" name_original="floret" src="d0_s12" type="structure" />
      <relation from="o23748" id="r3771" name="above" negation="false" src="d0_s12" to="o23749" />
    </statement>
    <statement id="d0_s13">
      <text>Glumes subequal, 6-9 mm long, 0.7-1 mm wide, lanceolate, glabrous, the bases evidently veined, apices entire, tapering into a 3-10 mm awn;</text>
      <biological_entity id="o23751" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s13" to="9" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s13" to="1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23752" name="base" name_original="bases" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="evidently" name="architecture" src="d0_s13" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity id="o23753" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
        <character constraint="into awn" constraintid="o23754" is_modifier="false" name="shape" src="d0_s13" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o23754" name="awn" name_original="awn" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 12-16 mm, glabrous, sometimes scabridulous, apices bidentate, awned, awns 15-30 mm, arcuately diverging to strongly recurved;</text>
      <biological_entity id="o23755" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s14" to="16" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="relief" src="d0_s14" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o23756" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="bidentate" value_original="bidentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o23757" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s14" to="30" to_unit="mm" />
        <character char_type="range_value" from="arcuately diverging" name="orientation" src="d0_s14" to="strongly recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>paleas subequal to the lemmas, apices about 0.4 mm wide;</text>
      <biological_entity id="o23758" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character constraint="to lemmas" constraintid="o23759" is_modifier="false" name="size" src="d0_s15" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o23759" name="lemma" name_original="lemmas" src="d0_s15" type="structure" />
      <biological_entity id="o23760" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character name="width" src="d0_s15" unit="mm" value="0.4" value_original="0.4" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers 2-3.5 mm. 2n = 28.</text>
      <biological_entity id="o23761" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23762" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Elymus sierrae is best known from rocky slopes and ridgetops in the Sierra Nevada, at 2100-3400 m, and is also found in Washington and Oregon. It resembles E. scribneri (see previous), differing in its non-disarticulating rachises, longer rachis internodes, and longer anthers. Hybrids with E. elymoides (p. 318) have glumes with awns 15+ mm long, and some spikelets with narrower glume bases and shorter anthers. Specimens with wide-margined glumes suggest hybridization with E. violaceus (p. 324).</discussion>
  
</bio:treatment>