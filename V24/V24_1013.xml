<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">717</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Adans." date="unknown" rank="genus">CALAMAGROSTIS</taxon_name>
    <taxon_name authority="Thurb." date="unknown" rank="species">breweri</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus calamagrostis;species breweri</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>12</number>
  <other_name type="common_name">Shorthair reedgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants sometimes with sterile culms;</text>
      <biological_entity id="o6060" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s0" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o6059" id="r975" name="with" negation="false" src="d0_s0" to="o6060" />
    </statement>
    <statement id="d0_s1">
      <text>densely cespitose, often with rhizomes to 5 cm long, 1-2 mm thick.</text>
      <biological_entity id="o6059" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s1" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="thickness" src="d0_s1" to="2" to_unit="mm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6061" name="rhizom" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s1" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="thickness" src="d0_s1" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (18) 29-54 cm, unbranched, smooth beneath the panicles;</text>
      <biological_entity id="o6062" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="18" value_original="18" />
        <character char_type="range_value" from="29" from_unit="cm" name="some_measurement" src="d0_s2" to="54" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character constraint="beneath panicles" constraintid="o6063" is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o6063" name="panicle" name_original="panicles" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>nodes 1-2 (3).</text>
      <biological_entity id="o6064" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character name="atypical_quantity" src="d0_s3" value="3" value_original="3" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basally concentrated;</text>
      <biological_entity id="o6065" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="basally" name="arrangement_or_density" src="d0_s4" value="concentrated" value_original="concentrated" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sheaths and collars smooth or slightly scabrous;</text>
      <biological_entity id="o6066" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o6067" name="collar" name_original="collars" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 1.7-4.1 (6) mm, usually lacerate;</text>
      <biological_entity id="o6068" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character name="atypical_some_measurement" src="d0_s6" unit="mm" value="6" value_original="6" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s6" to="4.1" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades (2) 10-15 cm long, 0.9-1.7 mm wide when flat, 0.4-0.6 mm in diameter when dry and involute, abaxial surfaces scabrous, adaxial surfaces sparsely hairy.</text>
      <biological_entity id="o6069" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s7" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="mm" modifier="when flat" name="width" src="d0_s7" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" modifier="when dry involute" name="diameter" src="d0_s7" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6070" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o6071" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles (4) 5.7-8.5 cm long, 0.7-5.2 cm wide, usually open, sometimes contracted, mostly erect, dark purple;</text>
      <biological_entity id="o6072" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character name="length" src="d0_s8" unit="cm" value="4" value_original="4" />
        <character char_type="range_value" from="5.7" from_unit="cm" name="length" src="d0_s8" to="8.5" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s8" to="5.2" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s8" value="open" value_original="open" />
        <character is_modifier="false" modifier="sometimes" name="condition_or_size" src="d0_s8" value="contracted" value_original="contracted" />
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark purple" value_original="dark purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>branches (1) 2-3 (3.5) cm, smooth, spikelets usually confined to the ends of the branches.</text>
      <biological_entity id="o6073" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="cm" value="1" value_original="1" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s9" to="3" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o6074" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
      <biological_entity id="o6075" name="end" name_original="ends" src="d0_s9" type="structure" />
      <biological_entity id="o6076" name="branch" name_original="branches" src="d0_s9" type="structure" />
      <relation from="o6074" id="r976" name="confined to the" negation="false" src="d0_s9" to="o6075" />
      <relation from="o6074" id="r977" name="confined to the" negation="false" src="d0_s9" to="o6076" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 3-5 mm;</text>
      <biological_entity id="o6077" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>rachilla prolongations about 1.5 mm, hairs 1.5-2 mm.</text>
      <biological_entity id="o6078" name="rachillum" name_original="rachilla" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o6079" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes rounded, usually smooth, occasionally scabrous at the apices, lateral-veins obscure, apices acute to attenuate;</text>
      <biological_entity id="o6080" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character constraint="at apices" constraintid="o6081" is_modifier="false" modifier="occasionally" name="pubescence_or_relief" src="d0_s12" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o6081" name="apex" name_original="apices" src="d0_s12" type="structure" />
      <biological_entity id="o6082" name="lateral-vein" name_original="lateral-veins" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o6083" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s12" to="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>callus hairs 0.3-1.2 mm, 0.2-0.5 times as long as the lemmas, sparse;</text>
      <biological_entity constraint="callus" id="o6084" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s13" to="1.2" to_unit="mm" />
        <character constraint="lemma" constraintid="o6085" is_modifier="false" name="length" src="d0_s13" value="0.2-0.5 times as long as the lemmas" />
        <character is_modifier="false" name="count_or_density" src="d0_s13" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o6085" name="lemma" name_original="lemmas" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>lemmas 2.5-4 mm, 0.5-1 mm shorter than the glumes;</text>
      <biological_entity id="o6086" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
        <character constraint="than the glumes" constraintid="o6087" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o6087" name="glume" name_original="glumes" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>awns 3.5-5.5 mm, attached to the lower 1/10 – 3/10 of the lemmas, exserted, bent;</text>
      <biological_entity id="o6088" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s15" to="5.5" to_unit="mm" />
        <character is_modifier="false" name="fixation" src="d0_s15" value="attached" value_original="attached" />
        <character is_modifier="false" name="position" src="d0_s15" value="lower" value_original="lower" />
        <character char_type="range_value" constraint="of lemmas" constraintid="o6089" from="1/10" name="quantity" src="d0_s15" to="3/10" />
        <character is_modifier="false" name="position" notes="" src="d0_s15" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="shape" src="d0_s15" value="bent" value_original="bent" />
      </biological_entity>
      <biological_entity id="o6089" name="lemma" name_original="lemmas" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>anthers 1.3-2.6 mm. 2n = 42.</text>
      <biological_entity id="o6090" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s16" to="2.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6091" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Calamagrostis breweri grows in moist subalpine and alpine meadows, lake margins, and stream banks, at 1700-2600 m, from Mount Hood in Oregon south to north of the Carson Pass area in Alpine and Amador counties, California. It differs from C. bolanderi (p. 719) in having basally concentrated leaves.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>