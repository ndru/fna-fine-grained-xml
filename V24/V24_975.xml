<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">689</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Hack." date="unknown" rank="genus">SCRIBNERIA</taxon_name>
    <taxon_name authority="(Thurb.) Hack" date="unknown" rank="species">bolanderi</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus scribneria;species bolanderi</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Scribner grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms (3) 10-35 cm.</text>
      <biological_entity id="o8802" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character name="atypical_some_measurement" src="d0_s0" unit="cm" value="3" value_original="3" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Ligules 2-4 mm;</text>
      <biological_entity id="o8803" name="ligule" name_original="ligules" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s1" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades 1-3 cm long, 0.8-1.6 mm wide, abaxial surfaces scabrous over the midveins.</text>
      <biological_entity id="o8804" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s2" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s2" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8805" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character constraint="over midveins" constraintid="o8806" is_modifier="false" name="pubescence_or_relief" src="d0_s2" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o8806" name="midvein" name_original="midveins" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Spikes (2) 4-11 cm long, 1-2.5 mm wide.</text>
      <biological_entity id="o8807" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character name="length" src="d0_s3" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s3" to="11" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikelets (3) 4-7 mm, slightly longer than the adjacent internodes.</text>
      <biological_entity id="o8808" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character name="atypical_some_measurement" src="d0_s4" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="7" to_unit="mm" />
        <character constraint="than the adjacent internodes" constraintid="o8809" is_modifier="false" name="length_or_size" src="d0_s4" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o8809" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Lemmas glabrous or scabridulous distally and on the keels;</text>
      <biological_entity id="o8810" name="lemma" name_original="lemmas" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o8811" name="keel" name_original="keels" src="d0_s5" type="structure" />
      <relation from="o8810" id="r1420" name="on" negation="false" src="d0_s5" to="o8811" />
    </statement>
    <statement id="d0_s6">
      <text>awns 2-4 mm, inconspicuous;</text>
      <biological_entity id="o8812" name="awn" name_original="awns" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="prominence" src="d0_s6" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>paleas generally smaller than the lemmas, apices notched.</text>
      <biological_entity id="o8813" name="palea" name_original="paleas" src="d0_s7" type="structure">
        <character constraint="than the lemmas" constraintid="o8814" is_modifier="false" name="size" src="d0_s7" value="generally smaller" value_original="generally smaller" />
      </biological_entity>
      <biological_entity id="o8814" name="lemma" name_original="lemmas" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>2n = 26.</text>
      <biological_entity id="o8815" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="notched" value_original="notched" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8816" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Scribneria bolanderi grows between 500-3000 m, from Washington to Baja California, Mexico. It grows in diverse habitats, ranging from dry, sandy or rocky soils to seepages and vernal pools. It is often overlooked because it is relatively inconspicuous.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.;Oreg.;Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>