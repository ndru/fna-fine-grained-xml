<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">784</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ALOPECURUS</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">geniculatus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus alopecurus;species geniculatus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Alopecurus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pallescens</taxon_name>
    <taxon_hierarchy>genus alopecurus;species pallescens</taxon_hierarchy>
  </taxon_identification>
  <number>4</number>
  <other_name type="common_name">Water foxtail</other_name>
  <other_name type="common_name">Vulpin genicule</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose.</text>
      <biological_entity id="o13471" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (5) 10-60 cm, erect or decumbent, rooting at the lower nodes.</text>
      <biological_entity id="o13472" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="5" value_original="5" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="60" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character constraint="at lower nodes" constraintid="o13473" is_modifier="false" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o13473" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Ligules 2-5 mm, obtuse;</text>
      <biological_entity id="o13474" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 2-12 cm long, 1-4 (7) mm wide;</text>
      <biological_entity id="o13475" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="12" to_unit="cm" />
        <character name="width" src="d0_s4" unit="mm" value="7" value_original="7" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>upper sheaths somewhat inflated.</text>
      <biological_entity constraint="upper" id="o13476" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s5" value="inflated" value_original="inflated" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 1.5-7 cm long, 4-8 mm wide.</text>
      <biological_entity id="o13477" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s6" to="7" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Glumes 1.9-3.5 mm, connate at the base, membranous, pubescent, keels not winged, ciliate, apices obtuse, parallel, often purplish;</text>
      <biological_entity id="o13478" name="glume" name_original="glumes" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s7" to="3.5" to_unit="mm" />
        <character constraint="at base" constraintid="o13479" is_modifier="false" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character is_modifier="false" name="texture" notes="" src="d0_s7" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o13479" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o13480" name="keel" name_original="keels" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="winged" value_original="winged" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o13481" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="parallel" value_original="parallel" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lemmas 2.5-3 mm, connate in the lower 1/2, glabrous or with a few scattered hairs at the apices, apices truncate to obtuse, awns 3-5 (6) mm, geniculate, exceeding the lemmas by (1.2) 2-4 mm;</text>
      <biological_entity id="o13482" name="lemma" name_original="lemmas" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character constraint="in lower 1/2" constraintid="o13483" is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="with a few scattered hairs" />
      </biological_entity>
      <biological_entity constraint="lower" id="o13483" name="1/2" name_original="1/2" src="d0_s8" type="structure" />
      <biological_entity id="o13484" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="few" value_original="few" />
        <character is_modifier="true" name="arrangement" src="d0_s8" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity id="o13485" name="apex" name_original="apices" src="d0_s8" type="structure" />
      <biological_entity id="o13486" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s8" to="obtuse" />
      </biological_entity>
      <biological_entity id="o13487" name="awn" name_original="awns" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="mm" value="6" value_original="6" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="geniculate" value_original="geniculate" />
      </biological_entity>
      <biological_entity id="o13488" name="lemma" name_original="lemmas" src="d0_s8" type="structure" />
      <relation from="o13482" id="r2150" name="with" negation="false" src="d0_s8" to="o13484" />
      <relation from="o13484" id="r2151" name="at" negation="false" src="d0_s8" to="o13485" />
      <relation from="o13487" id="r2152" name="exceeding the" negation="false" src="d0_s8" to="o13488" />
    </statement>
    <statement id="d0_s9">
      <text>anthers (0.9) 1.4-2.2 mm, yellow.</text>
      <biological_entity id="o13489" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="0.9" value_original="0.9" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s9" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Caryopses 1-1.5 mm. 2n = 28.</text>
      <biological_entity id="o13490" name="caryopse" name_original="caryopses" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13491" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Alopecurus geniculatus is native to Eurasia and parts of North America, growing in shallow water, ditches, open wet meadows, shores, and streambanks, from lowland to montane zones. It has been naturalized in eastern North America. The status of populations in the west, including the Queen Charlotte Islands in British Columbia, is less certain. Many occur in moist sites within native rangeland, but these areas have also been affected by European settlement, although less intensively and for a shorter period than those in eastern North America.</discussion>
  <discussion>Alopecurus xhaussknechtianus Asch. 5c Graebn. is a hybrid between A. geniculatus and A. aequalis, which occurs fairly frequently in areas of sympatry, particularly in drier midcontinental areas from Alberta to Saskatchewan, south to Arizona and New Mexico. The hybrids are sterile and appear to have 2n = 14.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Wash.;Va.;D.C.;Wis.;Minn.;N.Dak.;Okla.;S.Dak.;Conn.;Mass.;Maine;N.H.;R.I.;Vt.;Wyo.;N.J.;N.Mex.;N.Y.;Pa.;Calif.;Nev.;Ariz.;Iowa;Md.;Mich.;Ohio;Tex.;Utah;W.Va.;Colo.;Alaska;Ill.;Idaho;Alta.;B.C.;Greenland;N.B.;Nfld. and Labr.;N.S.;Ont.;P.E.I.;Que.;Sask.;Yukon;Mont.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>