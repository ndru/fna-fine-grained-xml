<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">127</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">STIPEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">ACHNATHERUM</taxon_name>
    <taxon_name authority="(Vasey) Barkworth" date="unknown" rank="species">parishii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe stipeae;genus achnatherum;species parishii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stipa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">parishii</taxon_name>
    <taxon_hierarchy>genus stipa;species parishii</taxon_hierarchy>
  </taxon_identification>
  <number>11</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants tightly cespitose, not rhizomatous.</text>
      <biological_entity id="o8559" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="tightly" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 14-80 cm tall, 0.8-2 mm thick, internodes glabrous or pubescent below the nodes;</text>
      <biological_entity id="o8560" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="14" from_unit="cm" name="height" src="d0_s1" to="80" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="thickness" src="d0_s1" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8561" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character constraint="below nodes" constraintid="o8562" is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o8562" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>nodes 3-5, glabrous.</text>
      <biological_entity id="o8563" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="5" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal sheaths mostly glabrous, sometimes pubescent at the base, flat and ribbonlike with age, margins sometimes hairy distally, hairs adjacent to the ligules 0.5-3 mm;</text>
      <biological_entity constraint="basal" id="o8564" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character constraint="at base" constraintid="o8565" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="prominence_or_shape" notes="" src="d0_s3" value="flat" value_original="flat" />
        <character constraint="with age" constraintid="o8566" is_modifier="false" name="architecture_or_shape" src="d0_s3" value="ribbonlike" value_original="ribbonlike" />
      </biological_entity>
      <biological_entity id="o8565" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o8566" name="age" name_original="age" src="d0_s3" type="structure" />
      <biological_entity id="o8567" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes; distally" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o8568" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character constraint="to ligules" constraintid="o8569" is_modifier="false" name="arrangement" src="d0_s3" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o8569" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>collars glabrous;</text>
      <biological_entity id="o8570" name="collar" name_original="collars" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules truncate, abaxial surfaces pubescent, ciliate, cilia as long as or longer than the basal membrane, ligules of basal leaves 0.3-0.8 mm, of upper leaves 0.5-1.5 mm, asymmetric;</text>
      <biological_entity id="o8571" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8572" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o8573" name="cilium" name_original="cilia" src="d0_s5" type="structure" />
      <biological_entity constraint="basal" id="o8575" name="membrane" name_original="membrane" src="d0_s5" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="basal" id="o8574" name="membrane" name_original="membrane" src="d0_s5" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o8576" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s5" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="asymmetric" value_original="asymmetric" />
      </biological_entity>
      <biological_entity constraint="basal" id="o8577" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="upper" id="o8578" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o8573" id="r1389" name="as long as" negation="false" src="d0_s5" to="o8575" />
      <relation from="o8576" id="r1390" name="part_of" negation="false" src="d0_s5" to="o8577" />
      <relation from="o8576" id="r1391" name="part_of" negation="false" src="d0_s5" to="o8578" />
    </statement>
    <statement id="d0_s6">
      <text>blades 4-30+ cm long, 1-4.2 mm wide, usually flat and more or less straight, sometimes tightly convolute and arcuate.</text>
      <biological_entity id="o8579" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s6" to="30" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="4.2" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="sometimes tightly" name="arrangement_or_shape" src="d0_s6" value="convolute" value_original="convolute" />
        <character is_modifier="false" name="course_or_shape" src="d0_s6" value="arcuate" value_original="arcuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 7-15 cm long, 1.5-4 cm wide;</text>
      <biological_entity id="o8580" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s7" to="15" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s7" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches strongly ascending at maturity, longest branches 1.5-4 cm.</text>
      <biological_entity id="o8581" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character constraint="at longest branches" constraintid="o8582" is_modifier="false" modifier="strongly" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="longest" id="o8582" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="maturity" value_original="maturity" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s8" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Glumes unequal to subequal, narrowly lanceolate, 3-5-veined;</text>
      <biological_entity id="o8583" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="unequal" name="size" src="d0_s9" to="subequal" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lower glumes 9-15 mm long, 0.9-1.2 mm wide;</text>
      <biological_entity constraint="lower" id="o8584" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s10" to="15" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s10" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 8-15 mm;</text>
      <biological_entity constraint="upper" id="o8585" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>florets 4.8-6.5 mm long, 0.8-1 mm thick, fusiform, terete;</text>
      <biological_entity id="o8586" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="4.8" from_unit="mm" name="length" src="d0_s12" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="thickness" src="d0_s12" to="1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="shape" src="d0_s12" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>calluses 0.2-0.8 mm, acute;</text>
      <biological_entity id="o8587" name="callus" name_original="calluses" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s13" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas evenly and densely hairy, hairs 1.5-3.5 mm at midlength, apical hairs 2.5-5 mm;</text>
      <biological_entity id="o8588" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o8589" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="at apical hairs" constraintid="o8590" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o8590" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character is_modifier="true" name="position" src="d0_s14" value="midlength" value_original="midlength" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>awns 10-35 mm, persistent, once-geniculate, first segment scabrous or strigose, hairs to 0.3 mm, terminal segment straight;</text>
      <biological_entity id="o8591" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s15" to="35" to_unit="mm" />
        <character is_modifier="false" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s15" value="once-geniculate" value_original="once-geniculate" />
      </biological_entity>
      <biological_entity id="o8592" name="segment" name_original="segment" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o8593" name="hair" name_original="hairs" src="d0_s15" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s15" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o8594" name="segment" name_original="segment" src="d0_s15" type="structure">
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas 2.5-4.5 mm, 1/2 - 4/5 times the length of the lemmas, hairy between the veins, hairs often as long as those on the lemmas but not as dense, apices usually rounded, occasionally somewhat pinched;</text>
      <biological_entity id="o8595" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s16" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="length" src="d0_s16" value="1/2-4/5 times the length of" />
        <character constraint="between veins" constraintid="o8597" is_modifier="false" name="pubescence" notes="" src="d0_s16" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o8596" name="lemma" name_original="lemmas" src="d0_s16" type="structure" />
      <biological_entity id="o8597" name="vein" name_original="veins" src="d0_s16" type="structure" />
      <biological_entity id="o8598" name="hair" name_original="hairs" src="d0_s16" type="structure" />
      <biological_entity id="o8599" name="lemma" name_original="lemmas" src="d0_s16" type="structure" />
      <biological_entity id="o8600" name="apex" name_original="apices" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="as dense; usually" name="shape" src="d0_s16" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o8598" id="r1392" modifier="as-long-as" name="on" negation="false" src="d0_s16" to="o8599" />
    </statement>
    <statement id="d0_s17">
      <text>anthers 2.3-4.5 mm, dehiscent, not penicillate.</text>
      <biological_entity id="o8601" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s17" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="dehiscence" src="d0_s17" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s17" value="penicillate" value_original="penicillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Caryopses 3-6 mm, fusiform.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = unknown.</text>
      <biological_entity id="o8602" name="caryopse" name_original="caryopses" src="d0_s18" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s18" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s18" value="fusiform" value_original="fusiform" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8603" name="chromosome" name_original="" src="d0_s19" type="structure" />
    </statement>
  </description>
  <discussion>Achnatherum parishii grows from the coastal ranges of California to Nevada and Utah, south to Baja California, Mexico, and to the Grand Canyon in Arizona. It differs from A. coronatum in its once-geniculate awns, more densely pubescent paleas, and generally smaller stature; from A. scribneri in its shorter, blunter calluses and more abundant lemma hairs; and from A. perplexum in having longer hairs on its lemmas.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah;Calif.;Ariz.;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Basal sheath margins glabrous or hairy distaily, hairs to 0.5 mm long; culms 14-35 cm tall</description>
      <determination>Achnatherum parishii subsp. depauperatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Basal sheath margins hairy distaily, hairs 1-3.2 mm long; culms 20-80 cm tall</description>
      <determination>Achnatherum parishii subsp. parishii</determination>
    </key_statement>
  </key>
</bio:treatment>