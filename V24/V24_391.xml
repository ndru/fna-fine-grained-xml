<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">274</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché, Hana Pazdírková, and Christine Roberts</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">TRITICUM</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">turgidum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus triticum;species turgidum</taxon_hierarchy>
  </taxon_identification>
  <number>8</number>
  <other_name type="common_name">Rivet wheat</other_name>
  <other_name type="common_name">Cone wheat</other_name>
  <other_name type="common_name">Ble poulard</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 120-180 cm;</text>
      <biological_entity id="o8883" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="120" from_unit="cm" name="some_measurement" src="d0_s0" to="180" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>nodes glabrous;</text>
      <biological_entity id="o8884" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes mostly hollow, solid for 1 cm below the spikes.</text>
      <biological_entity id="o8885" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s2" value="hollow" value_original="hollow" />
        <character constraint="below spikes" constraintid="o8886" is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
      </biological_entity>
      <biological_entity id="o8886" name="spike" name_original="spikes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Blades to 18 mm wide, shortly pubescent to villous.</text>
      <biological_entity id="o8887" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s3" to="18" to_unit="mm" />
        <character char_type="range_value" from="shortly pubescent" name="pubescence" src="d0_s3" to="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikes 7-14 cm, about as wide as thick, except when branched below;</text>
      <biological_entity id="o8888" name="spike" name_original="spikes" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s4" to="14" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s4" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>rachises hairy at the nodes and margins, not disarticulating.</text>
      <biological_entity id="o8889" name="rachis" name_original="rachises" src="d0_s5" type="structure">
        <character constraint="at margins" constraintid="o8891" is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s5" value="disarticulating" value_original="disarticulating" />
      </biological_entity>
      <biological_entity id="o8890" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <biological_entity id="o8891" name="margin" name_original="margins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Spikelets 10-16 mm, with 5-7 florets, 2-5 seed-forming.</text>
      <biological_entity id="o8892" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8893" name="floret" name_original="florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s6" to="7" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="5" />
      </biological_entity>
      <relation from="o8892" id="r1433" name="with" negation="false" src="d0_s6" to="o8893" />
    </statement>
    <statement id="d0_s7">
      <text>Glumes 8-11 mm, coriaceous, loosely appressed to the lower florets, with 1 prominent keel, terminating in a tooth, tooth to 0.3 cm;</text>
      <biological_entity id="o8894" name="glume" name_original="glumes" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="11" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s7" value="coriaceous" value_original="coriaceous" />
        <character constraint="to lower florets" constraintid="o8895" is_modifier="false" modifier="loosely" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity constraint="lower" id="o8895" name="floret" name_original="florets" src="d0_s7" type="structure" />
      <biological_entity id="o8896" name="keel" name_original="keel" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
        <character is_modifier="true" name="prominence" src="d0_s7" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o8897" name="tooth" name_original="tooth" src="d0_s7" type="structure" />
      <biological_entity id="o8898" name="tooth" name_original="tooth" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s7" to="0.3" to_unit="cm" />
      </biological_entity>
      <relation from="o8894" id="r1434" name="with" negation="false" src="d0_s7" to="o8896" />
      <relation from="o8894" id="r1435" name="terminating in" negation="false" src="d0_s7" to="o8897" />
    </statement>
    <statement id="d0_s8">
      <text>lemmas 10-13 mm, lowest 2 lemmas awned, awns to 20 cm;</text>
      <biological_entity id="o8899" name="lemma" name_original="lemmas" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="13" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s8" value="lowest" value_original="lowest" />
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o8900" name="lemma" name_original="lemmas" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o8901" name="awn" name_original="awns" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s8" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>paleas not splitting at maturity.</text>
      <biological_entity id="o8902" name="palea" name_original="paleas" src="d0_s9" type="structure">
        <character constraint="at maturity" is_modifier="false" modifier="not" name="architecture_or_dehiscence" src="d0_s9" value="splitting" value_original="splitting" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Endosperm mealy.</text>
    </statement>
    <statement id="d0_s11">
      <text>Haplomes AuB.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 28.</text>
      <biological_entity id="o8903" name="endosperm" name_original="endosperm" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s10" value="mealy" value_original="mealy" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8904" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Triticum turgidum is the tallest of the wheats, and differs from other species of domesticated wheat in having branched-spike forms. It is grown primarily in southern Europe, northern Iraq, southern Iran, and western Pakistan. As treated here, T. turgidum is a narrowly distributed taxon of minor importance in plant breeding. Under genomic classifications, however, the name is applied to all AUB taxa, e.g., to T. polonicum, T. durum, and T. carthlicum, as well as to T. turgidum sensu stricto.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Y.;Man.;Ont.;Sask.;Conn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>