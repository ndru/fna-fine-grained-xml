<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">142</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">STIPEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">ACHNATHERUM</taxon_name>
    <taxon_name authority="(Bol.) Barkworth" date="unknown" rank="species">×bloomeri</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe stipeae;genus achnatherum;species ×bloomeri</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">×Stiporyzopsis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">bloomeri</taxon_name>
    <taxon_hierarchy>genus ×stiporyzopsis;species bloomeri</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oryzopsis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">×bloomeri</taxon_name>
    <taxon_hierarchy>genus oryzopsis;species ×bloomeri</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stipa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">bloomeri</taxon_name>
    <taxon_hierarchy>genus stipa;species bloomeri</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <discussion>Achnatherum ×bloomeri and other hybrids involving A. hymenoides</discussion>
  <discussion>Numerous natural hybrids exist between Achnatherum hymenoides and other members of the Stipeae. Johnson (1945, 1960, 1962, 1963, 1972) described several of these; all are sterile. Using the treatment adopted here, Johnson's hybrids have as the second parent A. occidentale (all subspecies), A. thurberianum, A. scribneri, A. robustum, Jarava speciosa, and Nassella viridula. Evidence from herbarium specimens suggests that A. hymenoides also forms sterile hybrids with other species of Achnatherum. The name Achnatherum ×bloomeri applies only to hybrids between A. hymenoides and A. occidentale subsp. occidentale, but plants keying here may include any of the other interspecific hybrids. They all differ from A. hymenoides in having more elongated florets and awns 10-20 mm long, and from their other parent, in most instances, in having longer lemma hairs and more saccate glumes. Identification of the second parent is best made in the field by noting which other species of Stipeae are present, bearing in mind that species that are not in anthesis at the same time in one year might have sufficient overlap for hybridization in other years.</discussion>
  <discussion>Of the two intergeneric hybrids mentioned above, that with Nassella viridula is treated as ×Achnella caduca (see p. 169). It differs from Achnatherum hymenoides in its longer glumes and florets, and from other A. hymenoides hybrids in having a readily deciduous awn. No binomial has been proposed for the hybrid with Jarava speciosa. There is one fertile intergeneric hybrid involving A. hymenoides, A. contractum. It is included in Achnatherum and described below because it resembles other members of Achnatherum more than it does Piptatherum, the genus of the other parent.</discussion>
  <discussion>Sterile hybrids have anthers that do not dehisce, and contain few, poorly formed pollen grains. They also fail to form good caryopses, but this is also true of some non-hybrid plants. In the case of non-hybrid plants, failure to form good caryopses can result from failure to capture pollen or from incompatibility between the pollen grain and the pistillate plant. It is not known which, if either, of these explanations accounts for the large number of empty caryopses found in Achnatherum hymenoides.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.;N.Mex.;Utah;Calif.;Oreg.;Wyo.;Mont.;Wash.;Ariz.;Idaho;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>