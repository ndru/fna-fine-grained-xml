<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">83</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Endl." date="unknown" rank="tribe">MELICEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="genus">GLYCERIA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Glyceria</taxon_name>
    <taxon_name authority="Torr." date="unknown" rank="species">acutiflora</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe meliceae;genus glyceria;section glyceria;species acutiflora</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>13</number>
  <other_name type="common_name">Creeping mannagrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial.</text>
      <biological_entity id="o18441" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 30-100 cm tall, 3-6 mm thick, spongy, usually decumbent and rooting at the lower nodes.</text>
      <biological_entity id="o18442" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="height" src="d0_s1" to="100" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="thickness" src="d0_s1" to="6" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s1" value="spongy" value_original="spongy" />
        <character is_modifier="false" modifier="usually" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character constraint="at lower nodes" constraintid="o18443" is_modifier="false" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o18443" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Sheaths smooth, weakly keeled;</text>
      <biological_entity id="o18444" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules 5-9 mm;</text>
      <biological_entity id="o18445" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 10-15 cm long, 3-8 mm wide, abaxial surfaces smooth, adaxial surfaces of the midcauline leaves often papillose.</text>
      <biological_entity id="o18446" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o18447" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o18448" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="relief" src="d0_s4" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o18449" name="midcauline" name_original="midcauline" src="d0_s4" type="structure" />
      <biological_entity id="o18450" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <relation from="o18448" id="r2907" name="part_of" negation="false" src="d0_s4" to="o18449" />
      <relation from="o18448" id="r2908" name="part_of" negation="false" src="d0_s4" to="o18450" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences often racemes, sometimes panicles, 15-35 cm long, 1-2 cm wide, open at anthesis, bases often enclosed in the flag leaf-sheaths at maturity;</text>
      <biological_entity id="o18451" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="length" notes="" src="d0_s5" to="35" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="2" to_unit="cm" />
        <character constraint="at anthesis" is_modifier="false" name="architecture" src="d0_s5" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o18452" name="raceme" name_original="racemes" src="d0_s5" type="structure" />
      <biological_entity id="o18453" name="panicle" name_original="panicles" src="d0_s5" type="structure" />
      <biological_entity id="o18454" name="base" name_original="bases" src="d0_s5" type="structure" />
      <biological_entity id="o18455" name="flag" name_original="flag" src="d0_s5" type="structure" />
      <biological_entity id="o18456" name="sheath" name_original="leaf-sheaths" src="d0_s5" type="structure" />
      <relation from="o18454" id="r2909" modifier="often" name="enclosed in the" negation="false" src="d0_s5" to="o18455" />
      <relation from="o18454" id="r2910" modifier="often" name="enclosed in the" negation="false" src="d0_s5" to="o18456" />
    </statement>
    <statement id="d0_s6">
      <text>branches 5.5-8 cm (absent in racemose plants), solitary or in pairs, appressed, most branches with 1-3 spikelets, the lower branches sometimes with more than 3;</text>
      <biological_entity id="o18457" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="5.5" from_unit="cm" name="some_measurement" src="d0_s6" to="8" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="in pairs" value_original="in pairs" />
        <character is_modifier="false" name="fixation_or_orientation" notes="" src="d0_s6" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o18458" name="pair" name_original="pairs" src="d0_s6" type="structure" />
      <biological_entity id="o18459" name="branch" name_original="branches" src="d0_s6" type="structure" />
      <biological_entity id="o18460" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity constraint="lower" id="o18461" name="branch" name_original="branches" src="d0_s6" type="structure" />
      <relation from="o18457" id="r2911" name="in" negation="false" src="d0_s6" to="o18458" />
      <relation from="o18459" id="r2912" name="with" negation="false" src="d0_s6" to="o18460" />
    </statement>
    <statement id="d0_s7">
      <text>pedicels 1.5-2.5 mm.</text>
      <biological_entity id="o18462" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 20-45 mm long, 2.5-3 mm wide, cylindrical and terete except slightly laterally compressed at anthesis, rectangular in side view, with 5-12 florets.</text>
      <biological_entity id="o18463" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s8" to="45" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s8" value="terete" value_original="terete" />
        <character constraint="at florets" constraintid="o18464" is_modifier="false" modifier="slightly laterally" name="shape" src="d0_s8" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o18464" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="anthesis" value_original="anthesis" />
        <character is_modifier="true" name="shape" src="d0_s8" value="rectangular" value_original="rectangular" />
        <character char_type="range_value" from="5" is_modifier="true" modifier="in side view" name="quantity" src="d0_s8" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Glumes unequal, acute;</text>
      <biological_entity id="o18465" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lower glumes 1.3-4.5 mm;</text>
      <biological_entity constraint="lower" id="o18466" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s10" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 3-7 mm;</text>
      <biological_entity constraint="upper" id="o18467" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>rachilla internodes 2-3 mm;</text>
      <biological_entity constraint="rachilla" id="o18468" name="internode" name_original="internodes" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemmas 6-8.5 mm, scabridulous, 7-veined, gradually tapering from near midlength to the narrowly acute (less than 45°) or acuminate apices;</text>
      <biological_entity id="o18469" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="8.5" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s13" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="7-veined" value_original="7-veined" />
        <character constraint="from midlength" constraintid="o18470" is_modifier="false" modifier="gradually" name="shape" src="d0_s13" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o18470" name="midlength" name_original="midlength" src="d0_s13" type="structure" />
      <biological_entity id="o18471" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="narrowly" name="shape" src="d0_s13" value="acute" value_original="acute" />
        <character is_modifier="true" name="shape" src="d0_s13" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o18470" id="r2913" name="to" negation="false" src="d0_s13" to="o18471" />
    </statement>
    <statement id="d0_s14">
      <text>paleas 0.7-3 mm longer than the lemmas, keels winged, tips parallel, intercostal region truncate, often splitting, apices appearing bifid, with 0.4-1 mm teeth;</text>
      <biological_entity id="o18472" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
        <character constraint="than the lemmas" constraintid="o18473" is_modifier="false" name="length_or_size" src="d0_s14" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o18473" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
      <biological_entity id="o18474" name="keel" name_original="keels" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o18475" name="tip" name_original="tips" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s14" value="parallel" value_original="parallel" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o18476" name="region" name_original="region" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="often" name="architecture_or_dehiscence" src="d0_s14" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o18477" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="bifid" value_original="bifid" />
      </biological_entity>
      <biological_entity id="o18478" name="tooth" name_original="teeth" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o18477" id="r2914" name="with" negation="false" src="d0_s14" to="o18478" />
    </statement>
    <statement id="d0_s15">
      <text>anthers 3, 1-2 mm.</text>
      <biological_entity id="o18479" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="3" value_original="3" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Caryopses about 3 mm. 2n = 40.</text>
      <biological_entity id="o18480" name="caryopse" name_original="caryopses" src="d0_s16" type="structure">
        <character name="some_measurement" src="d0_s16" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18481" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Glyceria acutiflora grows in wet soils and shallow water of the northeastern United States, extending from Michigan and Missouri to the Atlantic coast between southwestern Maine and Delaware. Its long paleas make G. acutiflora the most distinctive North American species of sect. Glyceria.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Del.;W.Va.;N.H.;Tenn.;Pa.;Mass.;R.I.;Vt.;Va.;Ala.;Ga.;Ind.;Maine;Md.;Ohio;Mo.;Mich.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>