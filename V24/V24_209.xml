<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Mary E. Barkworth;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">154</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">STIPEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">STIPA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe stipeae;genus stipa</taxon_hierarchy>
  </taxon_identification>
  <number>10.07</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>tufted or cespitose, not rhizomatous.</text>
      <biological_entity id="o11224" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 10-200 cm, herbaceous, not branching at the upper nodes;</text>
      <biological_entity constraint="upper" id="o11226" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>basal branching usually intravaginal;</text>
      <biological_entity id="o11225" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="200" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s2" value="herbaceous" value_original="herbaceous" />
        <character constraint="at upper nodes" constraintid="o11226" is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="usually" name="position" src="d0_s3" value="intravaginal" value_original="intravaginal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>prophylls shorter than the sheaths.</text>
      <biological_entity id="o11227" name="prophyll" name_original="prophylls" src="d0_s4" type="structure">
        <character constraint="than the sheaths" constraintid="o11228" is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o11228" name="sheath" name_original="sheaths" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Leaves mostly basal;</text>
      <biological_entity id="o11229" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o11230" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s5" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cleistogenes usually not developed;</text>
      <biological_entity id="o11231" name="cleistogene" name_original="cleistogenes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually not" name="development" src="d0_s6" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sheaths open;</text>
      <biological_entity id="o11232" name="sheath" name_original="sheaths" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>auricles absent;</text>
      <biological_entity id="o11233" name="auricle" name_original="auricles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ligules membranous, sometimes stiffly so, upper and lower ligules similar or upper ligules longer than those below;</text>
      <biological_entity id="o11234" name="ligule" name_original="ligules" src="d0_s9" type="structure">
        <character is_modifier="false" name="texture" src="d0_s9" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity constraint="upper and lower upper" id="o11236" name="ligule" name_original="ligules" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>blades prominently ribbed, usually tightly convolute when dry.</text>
      <biological_entity id="o11237" name="blade" name_original="blades" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture_or_shape" src="d0_s10" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s10" value="convolute" value_original="convolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Inflorescences terminal panicles, usually contracted.</text>
      <biological_entity id="o11238" name="inflorescence" name_original="inflorescences" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="condition_or_size" notes="" src="d0_s11" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o11239" name="panicle" name_original="panicles" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 12-90 mm, with 1 floret;</text>
      <biological_entity id="o11240" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s12" to="90" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11241" name="floret" name_original="floret" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
      <relation from="o11240" id="r1783" name="with" negation="false" src="d0_s12" to="o11241" />
    </statement>
    <statement id="d0_s13">
      <text>rachillas not prolonged beyond the base of the floret;</text>
      <biological_entity id="o11243" name="base" name_original="base" src="d0_s13" type="structure" constraint="floret" constraint_original="floret; floret" />
      <biological_entity id="o11244" name="floret" name_original="floret" src="d0_s13" type="structure" />
      <relation from="o11243" id="r1784" name="part_of" negation="false" src="d0_s13" to="o11244" />
    </statement>
    <statement id="d0_s14">
      <text>disarticulation above the glumes, beneath the floret.</text>
      <biological_entity id="o11242" name="rachilla" name_original="rachillas" src="d0_s13" type="structure">
        <character constraint="beyond base" constraintid="o11243" is_modifier="false" modifier="not" name="length" src="d0_s13" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity id="o11245" name="glume" name_original="glumes" src="d0_s14" type="structure" />
      <biological_entity id="o11246" name="floret" name_original="floret" src="d0_s14" type="structure" />
      <relation from="o11242" id="r1785" name="above" negation="false" src="d0_s14" to="o11245" />
    </statement>
    <statement id="d0_s15">
      <text>Glumes much longer than the floret, hyaline to membranous, usually acuminate, 1-3-veined;</text>
      <biological_entity id="o11247" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character constraint="than the floret" constraintid="o11248" is_modifier="false" name="length_or_size" src="d0_s15" value="much longer" value_original="much longer" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="texture" src="d0_s15" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s15" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="1-3-veined" value_original="1-3-veined" />
      </biological_entity>
      <biological_entity id="o11248" name="floret" name_original="floret" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>florets 3-27 mm, terete to slightly laterally compressed;</text>
      <biological_entity id="o11249" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="27" to_unit="mm" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s16" to="slightly laterally compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>calluses (1) 1.5-6 mm, sharp or blunt, antrorsely hairy;</text>
      <biological_entity id="o11250" name="callus" name_original="calluses" src="d0_s17" type="structure">
        <character name="atypical_some_measurement" src="d0_s17" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s17" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s17" value="sharp" value_original="sharp" />
        <character is_modifier="false" name="shape" src="d0_s17" value="blunt" value_original="blunt" />
        <character is_modifier="false" modifier="antrorsely" name="pubescence" src="d0_s17" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>lemmas coriaceous to indurate, tan to brown, smooth, glabrous or hairy, hairs sometimes uniformly distributed, sometimes in lines, margins flat, slightly overlapping at maturity, apices awned, lemma-awn junction evident;</text>
      <biological_entity id="o11251" name="lemma" name_original="lemmas" src="d0_s18" type="structure">
        <character char_type="range_value" from="coriaceous" name="texture" src="d0_s18" to="indurate" />
        <character char_type="range_value" from="tan" name="coloration" src="d0_s18" to="brown" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o11252" name="hair" name_original="hairs" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="sometimes uniformly" name="arrangement" src="d0_s18" value="distributed" value_original="distributed" />
      </biological_entity>
      <biological_entity id="o11253" name="line" name_original="lines" src="d0_s18" type="structure" />
      <biological_entity id="o11254" name="margin" name_original="margins" src="d0_s18" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s18" value="flat" value_original="flat" />
        <character constraint="at maturity" is_modifier="false" modifier="slightly" name="arrangement" src="d0_s18" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o11255" name="apex" name_original="apices" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s18" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity constraint="lemma-awn" id="o11256" name="junction" name_original="junction" src="d0_s18" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s18" value="evident" value_original="evident" />
      </biological_entity>
      <relation from="o11252" id="r1786" modifier="sometimes" name="in" negation="false" src="d0_s18" to="o11253" />
    </statement>
    <statement id="d0_s19">
      <text>awns 50-500 mm, persistent, usually once or twice-geniculate, sometimes plumose in whole or in part, basal segment often strongly twisted;</text>
      <biological_entity id="o11257" name="awn" name_original="awns" src="d0_s19" type="structure">
        <character char_type="range_value" from="50" from_unit="mm" name="some_measurement" src="d0_s19" to="500" to_unit="mm" />
        <character is_modifier="false" name="duration" src="d0_s19" value="persistent" value_original="persistent" />
        <character constraint="part" constraintid="o11258" is_modifier="false" name="size_or_quantity" src="d0_s19" value="2 times-geniculate sometimes plumose in whole or in part" />
        <character constraint="part" constraintid="o11259" is_modifier="false" name="size_or_quantity" src="d0_s19" value="2 times-geniculate sometimes plumose in whole or in part" />
      </biological_entity>
      <biological_entity id="o11258" name="part" name_original="part" src="d0_s19" type="structure" />
      <biological_entity id="o11259" name="part" name_original="part" src="d0_s19" type="structure" />
      <biological_entity constraint="basal" id="o11260" name="segment" name_original="segment" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="often strongly" name="architecture" src="d0_s19" value="twisted" value_original="twisted" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>paleas from shorter than to subequal to the lemmas, glabrous, 2-veined, not keeled, flat between the veins, apices sometimes scarious, sometimes similar in texture to the body;</text>
      <biological_entity id="o11261" name="palea" name_original="paleas" src="d0_s20" type="structure">
        <character constraint="to lemmas" constraintid="o11262" is_modifier="false" modifier="from" name="size" src="d0_s20" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s20" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s20" value="2-veined" value_original="2-veined" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s20" value="keeled" value_original="keeled" />
        <character constraint="between veins" constraintid="o11263" is_modifier="false" name="prominence_or_shape" src="d0_s20" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o11262" name="lemma" name_original="lemmas" src="d0_s20" type="structure" />
      <biological_entity id="o11263" name="vein" name_original="veins" src="d0_s20" type="structure" />
      <biological_entity id="o11264" name="apex" name_original="apices" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="sometimes" name="texture" src="d0_s20" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o11265" name="body" name_original="body" src="d0_s20" type="structure" />
      <relation from="o11264" id="r1787" modifier="sometimes" name="to" negation="false" src="d0_s20" to="o11265" />
    </statement>
    <statement id="d0_s21">
      <text>lodicules 2 or 3, glabrous or pilose;</text>
      <biological_entity id="o11266" name="lodicule" name_original="lodicules" src="d0_s21" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s21" value="count" value_original="count" />
        <character is_modifier="false" name="pubescence" src="d0_s21" value="list" value_original="list" />
        <character name="quantity" src="d0_s21" unit="or punct glabrous or pilose" value="2" value_original="2" />
        <character name="quantity" src="d0_s21" unit="or punct glabrous or pilose" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>anthers 3;</text>
      <biological_entity id="o11267" name="anther" name_original="anthers" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>styles 2 (3,4), free at the base, if 3 or 4, then 1 or 2 distinctly shorter.</text>
      <biological_entity id="o11268" name="style" name_original="styles" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="2" value_original="2" />
        <character name="quantity" src="d0_s23" value="3" value_original="3" />
        <character name="quantity" src="d0_s23" value="4" value_original="4" />
        <character constraint="at base" constraintid="o11269" is_modifier="false" name="fusion" src="d0_s23" value="free" value_original="free" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s23" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o11269" name="base" name_original="base" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" unit="or" value="3" value_original="3" />
        <character name="quantity" src="d0_s23" unit="or" value="4" value_original="4" />
        <character name="quantity" src="d0_s23" unit="or distinctly" value="1" value_original="1" />
        <character name="quantity" src="d0_s23" unit="or distinctly" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>Caryopses fusiform, not ribbed, x = 11.</text>
      <biological_entity id="o11270" name="caryopse" name_original="caryopses" src="d0_s24" type="structure">
        <character is_modifier="false" name="shape" src="d0_s24" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s24" value="ribbed" value_original="ribbed" />
      </biological_entity>
      <biological_entity constraint="x" id="o11271" name="chromosome" name_original="" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <discussion>As treated here, Stipa is a genus of 150-200 species, all of which are native to Eurasia or northern Africa. Until recently, the genus was interpreted as including almost all species of Stipeae with cylindrical florets. In several parts of the world, this broader interpretation still prevails. Jacobs et al. (2006) found that even the European members of Stipa included in their study appeared to be polyphyletic. The most appropriate circumscription of the genus, and its size, is difficult to determine in the absence of a study that encompasses the Eurasian and North African members of the tribe.</discussion>
  <discussion>Two species of Stipa grow in the Flora region. Stipa pulcherrima is cultivated as an ornamental; Stipa capensis has been introduced, probably accidentally.</discussion>
  <references>
    <reference>Freitag, H. 1985. The genus Stipa in southwest and south Asia. Notes Roy. Bot. Gard. Edinburgh 42:355-487</reference>
    <reference>Jacobs, S.W.L., R. Bayer, J. Everett, M.O. Arriaga, M.E. Barkworth, A. Sabin-Badereau, M.A. Torres, E Vazquez, and N. Bagnall. 2006. Systematics of the tribe Stipeae using molecular data. Aliso 23:349-361.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Plants perennial; glumes 60-90 mm long; awns plumose on the distal segment, hairs 5-6 mm long</description>
      <determination>1 Stipa pulcherrima</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Plants annual; glumes 12-20 mm long; awns glabrous on the distal segment</description>
      <determination>2 Stipa capensis</determination>
    </key_statement>
  </key>
</bio:treatment>