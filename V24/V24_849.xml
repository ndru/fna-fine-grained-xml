<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Jacques Cayouette;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">601</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="J. Cay. &amp; Darbysh." date="unknown" rank="genus">×DUPOA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus ×dupoa</taxon_hierarchy>
  </taxon_identification>
  <number>14.14</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous.</text>
      <biological_entity id="o27061" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 25-80 cm, glabrous, often glaucous, with 2-4 nodes.</text>
      <biological_entity id="o27062" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s2" to="80" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o27063" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
      <relation from="o27062" id="r4270" name="with" negation="false" src="d0_s2" to="o27063" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths closed for 1/3-2/3 their length, glabrous, not conspicuously glaucous;</text>
      <biological_entity id="o27064" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="length" src="d0_s3" value="closed" value_original="closed" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="not conspicuously" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>auricles absent;</text>
      <biological_entity id="o27065" name="auricle" name_original="auricles" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules membranous, whitish or yellowish-brown, acute to obtuse, lacerate to erose, ciliate;</text>
      <biological_entity id="o27066" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellowish-brown" value_original="yellowish-brown" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lacerate" value_original="lacerate" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s5" value="erose" value_original="erose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades glabrous.</text>
      <biological_entity id="o27067" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences panicles.</text>
      <biological_entity constraint="inflorescences" id="o27068" name="panicle" name_original="panicles" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets with (1) 2-3 (4) florets.</text>
      <biological_entity id="o27069" name="spikelet" name_original="spikelets" src="d0_s8" type="structure" />
      <biological_entity id="o27070" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="4" value_original="4" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <relation from="o27069" id="r4271" name="with" negation="false" src="d0_s8" to="o27070" />
    </statement>
    <statement id="d0_s9">
      <text>Glumes usually not exceeding the florets, glabrous, often glaucous, acute to acuminate;</text>
      <biological_entity id="o27071" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s9" value="glaucous" value_original="glaucous" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="acuminate" />
      </biological_entity>
      <biological_entity id="o27072" name="floret" name_original="florets" src="d0_s9" type="structure" />
      <relation from="o27071" id="r4272" modifier="usually not" name="exceeding the" negation="false" src="d0_s9" to="o27072" />
    </statement>
    <statement id="d0_s10">
      <text>calluses bearded, hairs often crinkled below the keel and/or absent from the sides;</text>
      <biological_entity id="o27073" name="callus" name_original="calluses" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="bearded" value_original="bearded" />
      </biological_entity>
      <biological_entity id="o27074" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character constraint="below keel" constraintid="o27075" is_modifier="false" modifier="often" name="shape" src="d0_s10" value="crinkled" value_original="crinkled" />
      </biological_entity>
      <biological_entity id="o27075" name="keel" name_original="keel" src="d0_s10" type="structure">
        <character constraint="from sides" constraintid="o27076" is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o27076" name="side" name_original="sides" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>lemmas usually scabrous, sometimes smooth towards the base, acute;</text>
      <biological_entity id="o27077" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
        <character constraint="towards base" constraintid="o27078" is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o27078" name="base" name_original="base" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>paleas smooth or slightly scabrous;</text>
      <biological_entity id="o27079" name="palea" name_original="paleas" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="pubescence_or_relief" src="d0_s12" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers 3, indehiscent.</text>
      <biological_entity id="o27080" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="3" value_original="3" />
        <character is_modifier="false" name="dehiscence" src="d0_s13" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Caryopses absent.</text>
      <biological_entity id="o27081" name="caryopse" name_original="caryopses" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>×Dupoa is a sterile hybrid between Dupontia and Poa. Only one species is known.</discussion>
  <references>
    <reference>Aiken, S.G., L.L. Consaul, and M.J. Dallwitz. 1995 on. Grasses of the Canadian Arctic Archipelago: Descriptions, illustrations, identification and information retrieval, http://www.mun.ca/biology/delta/arcticf/poa/index.htm</reference>
    <reference>Cayouette, J. and S.J. Darbyshire. 1993. The intergeneric hybrid grass "Poa labradorica". Nordic J. Bot. 13:615-629</reference>
    <reference>Darbyshire, S.J., J. Cayouette, and S.I. Warwick. 1992. The intergeneric hybrid origin of Poa labradorica Steudel (Poaceae). PI. Syst. Evol. 181:57-76.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nfld. and Labr. (Nfld.);Que.;Alaska</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>