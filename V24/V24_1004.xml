<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">712</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Adans." date="unknown" rank="genus">CALAMAGROSTIS</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">howellii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus calamagrostis;species howellii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Hovell's reedgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants sometimes with sterile culms;</text>
      <biological_entity id="o24780" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s0" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o24779" id="r3914" name="with" negation="false" src="d0_s0" to="o24780" />
    </statement>
    <statement id="d0_s1">
      <text>usually densely cespitose, occasionally with rhizomes shorter than 1 cm.</text>
      <biological_entity id="o24779" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually densely" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o24781" name="rhizom" name_original="rhizomes" src="d0_s1" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s1" unit="cm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (25) 35^45 (60) cm, unbranched, smooth or slightly scabrous beneath the panicles;</text>
      <biological_entity id="o24782" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_quantity" src="d0_s2" value="25" value_original="25" />
        <character name="quantity" src="d0_s2" value="35" value_original="35" />
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="60" value_original="60" />
        <character name="some_measurement" src="d0_s2" unit="cm" value="45" value_original="45" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character constraint="beneath panicles" constraintid="o24783" is_modifier="false" modifier="slightly" name="pubescence_or_relief" src="d0_s2" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o24783" name="panicle" name_original="panicles" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>nodes 1-2.</text>
      <biological_entity id="o24784" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths and collars smooth;</text>
      <biological_entity id="o24785" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o24786" name="collar" name_original="collars" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules (2.5) 3.5-6 mm, acute, lacerate;</text>
      <biological_entity id="o24787" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character name="atypical_some_measurement" src="d0_s5" unit="mm" value="2.5" value_original="2.5" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades (9) 12-20 (25) cm long, 1-2.5 (3) mm wide, flat to involute, abaxial surfaces smooth, adaxial surfaces finely scabrous, glabrous or sparsely hairy.</text>
      <biological_entity id="o24788" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character name="length" src="d0_s6" unit="cm" value="9" value_original="9" />
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s6" to="20" to_unit="cm" />
        <character name="width" src="d0_s6" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s6" to="involute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24789" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o24790" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles (5) 7-12 (15) cm long, (2) 3.5-6.5 (8) cm wide, loose, open, straw-colored or green to purplish;</text>
      <biological_entity id="o24791" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="5" value_original="5" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s7" to="12" to_unit="cm" />
        <character name="width" src="d0_s7" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="width" src="d0_s7" to="6.5" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s7" value="loose" value_original="loose" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s7" to="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches (2) 3.5-5 (7) cm, smooth or sparsely scabrous, spikelets usually confined to the distal 1/2.</text>
      <biological_entity id="o24792" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s8" to="5" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o24793" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s8" value="distal" value_original="distal" />
        <character name="quantity" src="d0_s8" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets (5.5) 6-8 mm;</text>
      <biological_entity id="o24794" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="5.5" value_original="5.5" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>rachilla prolongations 1-1.5 (2) mm, hairs (1.5) 2-2.5 (3) mm.</text>
      <biological_entity id="o24795" name="rachillum" name_original="rachilla" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24796" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Glumes rounded to slightly keeled, smooth or scabrous distally, lateral-veins usually prominent and raised, apices acuminate;</text>
      <biological_entity id="o24797" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s11" to="slightly keeled" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="distally" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o24798" name="lateral-vein" name_original="lateral-veins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="prominence" src="d0_s11" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="prominence" src="d0_s11" value="raised" value_original="raised" />
      </biological_entity>
      <biological_entity id="o24799" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>callus hairs 2-3 (4.5) mm, 0.4-0.6 (0.7) times as long as the lemmas, abundant;</text>
      <biological_entity constraint="callus" id="o24800" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="mm" value="4.5" value_original="4.5" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
        <character constraint="lemma" constraintid="o24801" is_modifier="false" name="length" src="d0_s12" value="0.4-0.6(0.7) times as long as the lemmas" />
        <character is_modifier="false" name="quantity" src="d0_s12" value="abundant" value_original="abundant" />
      </biological_entity>
      <biological_entity id="o24801" name="lemma" name_original="lemmas" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>lemmas 4.5-5 mm, about 2 mm shorter than the glumes;</text>
      <biological_entity id="o24802" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
        <character name="some_measurement" src="d0_s13" unit="mm" value="2" value_original="2" />
        <character constraint="than the glumes" constraintid="o24803" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o24803" name="glume" name_original="glumes" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>awns (10) 13-16 mm, attached to the lower 1/5 – 2/5 of the lemmas, exserted, stout, easily distinguished from the callus hairs, strongly bent;</text>
      <biological_entity id="o24804" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="10" value_original="10" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s14" to="16" to_unit="mm" />
        <character is_modifier="false" name="fixation" src="d0_s14" value="attached" value_original="attached" />
        <character is_modifier="false" name="position" src="d0_s14" value="lower" value_original="lower" />
        <character char_type="range_value" constraint="of lemmas" constraintid="o24805" from="1/5" name="quantity" src="d0_s14" to="2/5" />
        <character is_modifier="false" name="position" notes="" src="d0_s14" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s14" value="stout" value_original="stout" />
        <character constraint="from callus hairs" constraintid="o24806" is_modifier="false" modifier="easily" name="prominence" src="d0_s14" value="distinguished" value_original="distinguished" />
        <character is_modifier="false" modifier="strongly" name="shape" notes="" src="d0_s14" value="bent" value_original="bent" />
      </biological_entity>
      <biological_entity id="o24805" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
      <biological_entity constraint="callus" id="o24806" name="hair" name_original="hairs" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>anthers (2) 2.5-3 (4) mm.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 28.</text>
      <biological_entity id="o24807" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24808" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Calamagrostis howellii grows on dry rocky slopes, banks, ledges, and in cliff crevices, sometimes on basalt, from 100-500 m. It grows only in the Columbia River Gorge of Washington and Oregon.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.;Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>