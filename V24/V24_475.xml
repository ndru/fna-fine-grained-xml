<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">336</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ELYMUS</taxon_name>
    <taxon_name authority="K.B. Jensen &amp; Asay" date="unknown" rank="species">hoffmannii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus elymus;species hoffmannii</taxon_hierarchy>
  </taxon_identification>
  <number>36</number>
  <other_name type="common_name">Hoffmann's wheatgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants slightly to moderately rhizomatous.</text>
      <biological_entity id="o8694" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="slightly to moderately" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 54-135 cm, glabrous.</text>
      <biological_entity id="o8695" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="54" from_unit="cm" name="some_measurement" src="d0_s1" to="135" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves evenly distributed;</text>
      <biological_entity id="o8696" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="evenly" name="arrangement" src="d0_s2" value="distributed" value_original="distributed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheaths glabrous;</text>
      <biological_entity id="o8697" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>auricles absent or to 1 mm;</text>
      <biological_entity id="o8698" name="auricle" name_original="auricles" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s4" value="0-1 mm" value_original="0-1 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.6-1 mm, truncate, erose;</text>
      <biological_entity id="o8699" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s5" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 5-13 mm wide, flat to involute, abaxial surfaces smooth, glabrous, adaxial surfaces glabrous, veins closely spaced, all more or less equally prominent, smooth or scabrous.</text>
      <biological_entity id="o8700" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="13" to_unit="mm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s6" to="involute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8701" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o8702" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o8703" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="closely" name="arrangement" src="d0_s6" value="spaced" value_original="spaced" />
        <character is_modifier="false" modifier="more or less equally" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spikes 10-50 cm long, 0.8-1.8 cm wide, with 1 spikelet per node, glabrous below the spikelets;</text>
      <biological_entity id="o8704" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s7" to="50" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s7" to="1.8" to_unit="cm" />
        <character constraint="below spikelets" constraintid="o8707" is_modifier="false" name="pubescence" notes="" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o8705" name="spikelet" name_original="spikelet" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o8706" name="node" name_original="node" src="d0_s7" type="structure" />
      <biological_entity id="o8707" name="spikelet" name_original="spikelets" src="d0_s7" type="structure" />
      <relation from="o8704" id="r1404" name="with" negation="false" src="d0_s7" to="o8705" />
      <relation from="o8705" id="r1405" name="per" negation="false" src="d0_s7" to="o8706" />
    </statement>
    <statement id="d0_s8">
      <text>internodes 5-8 mm long, about 0.2 mm thick, about 0.3 mm wide, both surfaces hairy, hairs 0.2-0.4 mm.</text>
      <biological_entity id="o8708" name="internode" name_original="internodes" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s8" to="8" to_unit="mm" />
        <character name="thickness" src="d0_s8" unit="mm" value="0.2" value_original="0.2" />
        <character name="width" src="d0_s8" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
      <biological_entity id="o8709" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o8710" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s8" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 15-27 mm, appressed to ascending, with 5-7 florets;</text>
      <biological_entity id="o8711" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s9" to="27" to_unit="mm" />
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s9" to="ascending" />
      </biological_entity>
      <biological_entity id="o8712" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s9" to="7" />
      </biological_entity>
      <relation from="o8711" id="r1406" name="with" negation="false" src="d0_s9" to="o8712" />
    </statement>
    <statement id="d0_s10">
      <text>rachillas scabridulous;</text>
    </statement>
    <statement id="d0_s11">
      <text>disarticulation above the glumes, beneath the florets.</text>
      <biological_entity id="o8713" name="rachilla" name_original="rachillas" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o8714" name="glume" name_original="glumes" src="d0_s11" type="structure" />
      <biological_entity id="o8715" name="floret" name_original="florets" src="d0_s11" type="structure" />
      <relation from="o8713" id="r1407" name="above" negation="false" src="d0_s11" to="o8714" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes equal, 5-11 mm long, 1.3-1.8 mm wide, stiff, lanceolate to linear-lanceolate, strongly rounded to keeled distally, keels inconspicuous and smooth on the proximal 1/3-1/2, conspicuous and with a few teeth distally, lateral-veins inconspicuous, hyaline margins 0.1-0.2 mm wide, apices acuminate to awned, awns to 8 mm;</text>
      <biological_entity id="o8716" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="variability" src="d0_s12" value="equal" value_original="equal" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s12" to="11" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s12" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s12" value="stiff" value_original="stiff" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s12" to="linear-lanceolate strongly rounded" />
        <character char_type="range_value" from="lanceolate" modifier="distally" name="shape" src="d0_s12" to="linear-lanceolate strongly rounded" />
      </biological_entity>
      <biological_entity id="o8717" name="keel" name_original="keels" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="inconspicuous" value_original="inconspicuous" />
        <character constraint="on proximal 1/3-1/2" constraintid="o8718" is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="prominence" notes="" src="d0_s12" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o8718" name="1/3-1/2" name_original="1/3-1/2" src="d0_s12" type="structure" />
      <biological_entity id="o8719" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o8720" name="lateral-vein" name_original="lateral-veins" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o8721" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s12" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8722" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s12" to="awned" />
      </biological_entity>
      <biological_entity id="o8723" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
      </biological_entity>
      <relation from="o8717" id="r1408" name="with" negation="false" src="d0_s12" to="o8719" />
    </statement>
    <statement id="d0_s13">
      <text>lemmas 7-12 mm, glabrous, smooth, apices unawned or awned, awns to 12 mm, straight;</text>
      <biological_entity id="o8724" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s13" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o8725" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o8726" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="12" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>paleas ciliate on the keels, apices about 0.6 mm wide;</text>
      <biological_entity id="o8727" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character constraint="on keels" constraintid="o8728" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s14" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o8728" name="keel" name_original="keels" src="d0_s14" type="structure" />
      <biological_entity id="o8729" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character name="width" src="d0_s14" unit="mm" value="0.6" value_original="0.6" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 4-7 mm. 2n = 42.</text>
      <biological_entity id="o8730" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s15" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8731" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Elymus hoffmannii was described from a breeding line of plants developed from seeds collected in Erzurum Province, Turkey by J.A. Hoffmann and R.J. Metzger (Jensen &amp; Asay 1996). There is no information available about its native distribution. As indicated in the key, E. hoffmannii differs from E. repens (see previous) primarily in its evenly prominent, closely spaced leaf veins and, usually, in having longer awns.</discussion>
  <discussion>The description of Elymus hoffmannii was explicitly written to encompass the cultivar 'NewHy' that is derived from an artificial cross between E. repens and Pseudoroegneria spicata (p. 281). Because of its morphological similarity to plants obtained from the Turkish seed, Jensen and Asay suggested that E. hoffmannii had a similar parentage. 'NewHy' was released as a cultivar in the 1980s. Its distribution within the Flora region is not known.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>