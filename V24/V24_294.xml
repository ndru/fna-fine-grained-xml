<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">211</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">BROMEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">BROMUS</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="section">Bromopsis</taxon_name>
    <taxon_name authority="(Hitchc.) Saarela &amp; P.M. Peterson" date="unknown" rank="species">hallii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe bromeae;genus bromus;section bromopsis;species hallii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bromus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">orcuttianus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">hallii</taxon_name>
    <taxon_hierarchy>genus bromus;species orcuttianus;variety hallii</taxon_hierarchy>
  </taxon_identification>
  <number>15</number>
  <other_name type="common_name">Hall's brome</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>not rhizomatous.</text>
      <biological_entity id="o13971" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 90-150 cm, erect;</text>
      <biological_entity id="o13972" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="90" from_unit="cm" name="some_measurement" src="d0_s2" to="150" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes 1-2 (3), pubescent or puberulent;</text>
      <biological_entity id="o13973" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character name="atypical_quantity" src="d0_s3" value="3" value_original="3" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="2" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>internodes usually puberulent, occasionally glabrous, pilose to densely pubescent below the nodes.</text>
      <biological_entity id="o13974" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" constraint="below nodes" constraintid="o13975" from="pilose" name="pubescence" src="d0_s4" to="densely pubescent" />
      </biological_entity>
      <biological_entity id="o13975" name="node" name_original="nodes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Sheaths densely pubescent to pilose, hairs to 1 mm, collars pilose, hairs to 2 mm;</text>
      <biological_entity id="o13976" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character char_type="range_value" from="densely pubescent" name="pubescence" src="d0_s5" to="pilose" />
      </biological_entity>
      <biological_entity id="o13977" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13978" name="collar" name_original="collars" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o13979" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>auricles absent;</text>
      <biological_entity id="o13980" name="auricle" name_original="auricles" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules 0.5-2.5 mm long, sparsely to densely pubescent, obtuse, erose;</text>
      <biological_entity id="o13981" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s7" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s7" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blades 7.5-16.5 cm long, 3-12 mm wide, flat, densely pubescent on both surfaces.</text>
      <biological_entity id="o13982" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character char_type="range_value" from="7.5" from_unit="cm" name="length" src="d0_s8" to="16.5" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="12" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character constraint="on surfaces" constraintid="o13983" is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o13983" name="surface" name_original="surfaces" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Panicles 5-16 cm, open;</text>
      <biological_entity id="o13984" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s9" to="16" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>branches erect, ascending and appressed to slightly spreading.</text>
      <biological_entity id="o13985" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s10" to="slightly spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 25-35 (45) mm, terete to moderately laterally compressed, with 3-7 florets.</text>
      <biological_entity id="o13986" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character name="atypical_some_measurement" src="d0_s11" unit="mm" value="45" value_original="45" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s11" to="35" to_unit="mm" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s11" to="moderately laterally compressed" />
      </biological_entity>
      <biological_entity id="o13987" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s11" to="7" />
      </biological_entity>
      <relation from="o13986" id="r2223" name="with" negation="false" src="d0_s11" to="o13987" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes sparsely to densely pubescent;</text>
      <biological_entity id="o13988" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 5-8 (9) mm, 1 (3) -veined;</text>
      <biological_entity constraint="lower" id="o13989" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="9" value_original="9" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="1(3)-veined" value_original="1(3)-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes (7) 8-9 mm, 3-veined, sometimes mucronate;</text>
      <biological_entity constraint="upper" id="o13990" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="7" value_original="7" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s14" to="9" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s14" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 10-14 mm, elliptic, rounded over the midvein, backs sparsely to densely pubescent, margins pubescent, apices entire;</text>
      <biological_entity id="o13991" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s15" to="14" to_unit="mm" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s15" value="elliptic" value_original="elliptic" />
        <character constraint="over midvein" constraintid="o13992" is_modifier="false" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o13992" name="midvein" name_original="midvein" src="d0_s15" type="structure" />
      <biological_entity id="o13993" name="back" name_original="backs" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o13994" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o13995" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>awns 3.5-7 mm, straight, arising less than 1.5 mm below the lemma apices;</text>
      <biological_entity id="o13996" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s16" to="7" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="arising" value_original="arising" />
        <character char_type="range_value" constraint="below lemma apices" constraintid="o13997" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o13997" name="apex" name_original="apices" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>anthers 3-6 mm. 2n = unknown.</text>
      <biological_entity id="o13998" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s17" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13999" name="chromosome" name_original="" src="d0_s17" type="structure" />
    </statement>
  </description>
  <discussion>Bromus hallii grows in southern California on dry, open or shaded hillsides, rocky slopes, and in montane pine woods, from 1500-2700 m.</discussion>
  
</bio:treatment>