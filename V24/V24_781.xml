<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">559</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="Soreng" date="unknown" rank="section">Madropoa</taxon_name>
    <taxon_name authority="Hitchc. ex Soreng" date="unknown" rank="subsection">Epiles</taxon_name>
    <taxon_name authority="Vasey" date="unknown" rank="species">cusickii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section madropoa;subsection epiles;species cusickii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>42</number>
  <other_name type="common_name">Cusick's bluegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually densely tufted, rarely moderately densely tufted, usually neither rhizomatous nor stoloniferous, infrequently short-rhizomatous or stoloniferous, rarely with distinct rhizomes.</text>
      <biological_entity id="o15628" name="rhizom" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="infrequently" name="architecture" src="d0_s1" value="short-rhizomatous" value_original="short-rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="true" name="fusion" src="d0_s1" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal branching intravaginal or intra and extravaginal.</text>
      <biological_entity id="o15627" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="usually densely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="rarely moderately densely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="position" src="d0_s2" value="intravaginal" value_original="intravaginal" />
        <character is_modifier="false" name="position" src="d0_s2" value="extravaginal" value_original="extravaginal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms 10-60 (70) cm tall, 0.5-1.8 mm thick, erect or the bases decumbent, terete or weakly compressed;</text>
      <biological_entity id="o15629" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character name="height" src="d0_s3" unit="cm" value="70" value_original="70" />
        <character char_type="range_value" from="10" from_unit="cm" name="height" src="d0_s3" to="60" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="thickness" src="d0_s3" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o15630" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="shape" src="d0_s3" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>nodes terete, 0-2 exserted.</text>
      <biological_entity id="o15631" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s4" to="2" />
        <character is_modifier="false" name="position" src="d0_s4" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths closed for 1/4-3/4 their length, terete, smooth or scabrous, glabrous, bases of basal sheaths glabrous, distal sheath lengths 1.6-10 times blade lengths;</text>
      <biological_entity id="o15632" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="length" src="d0_s5" value="closed" value_original="closed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15633" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o15634" name="sheath" name_original="sheaths" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o15635" name="sheath" name_original="sheath" src="d0_s5" type="structure">
        <character constraint="blade" constraintid="o15636" is_modifier="false" name="length" src="d0_s5" value="1.6-10 times blade lengths" value_original="1.6-10 times blade lengths" />
      </biological_entity>
      <biological_entity id="o15636" name="blade" name_original="blade" src="d0_s5" type="structure" />
      <relation from="o15633" id="r2498" name="part_of" negation="false" src="d0_s5" to="o15634" />
    </statement>
    <statement id="d0_s6">
      <text>collars smooth or scabrous, glabrous;</text>
      <biological_entity id="o15637" name="collar" name_original="collars" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules of cauline leaves 1-3 (6) mm, smooth or scabrous, truncate to acute, ligules of the innovation leaves 0.2-0.5 (2.5) mm, scabrous, usually truncate;</text>
      <biological_entity id="o15638" name="ligule" name_original="ligules" src="d0_s7" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character name="atypical_some_measurement" src="d0_s7" unit="mm" value="6" value_original="6" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s7" to="acute" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o15639" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o15640" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character name="atypical_some_measurement" src="d0_s7" unit="mm" value="2.5" value_original="2.5" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s7" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s7" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity constraint="innovation" id="o15641" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o15638" id="r2499" name="part_of" negation="false" src="d0_s7" to="o15639" />
      <relation from="o15640" id="r2500" name="part_of" negation="false" src="d0_s7" to="o15641" />
    </statement>
    <statement id="d0_s8">
      <text>innovation blades sometimes distinctly different from the cauline blades, 0.5-2 mm wide, involute, moderately thick, moderately firm, adaxial surfaces usually densely scabrous or hispidulous to softly puberulent, infrequently nearly smooth and glabrous;</text>
      <biological_entity constraint="innovation" id="o15642" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s8" value="involute" value_original="involute" />
        <character is_modifier="false" modifier="moderately" name="width" src="d0_s8" value="thick" value_original="thick" />
        <character is_modifier="false" modifier="moderately" name="texture" src="d0_s8" value="firm" value_original="firm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o15643" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" notes="" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o15644" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character char_type="range_value" from="hispidulous" name="pubescence" src="d0_s8" to="softly puberulent" />
        <character is_modifier="false" modifier="infrequently nearly" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o15642" id="r2501" name="from" negation="false" src="d0_s8" to="o15643" />
    </statement>
    <statement id="d0_s9">
      <text>cauline blades subequal or the midcauline blades longest or the blades gradually reduced in length distally, 0.5-3 mm wide, flat, folded, or involute, usually thin, usually withering, abaxial surfaces smooth or scabrous, apices narrowly to broadly prow-shaped, flag leaf-blades 0.5-5 (6) cm.</text>
      <biological_entity constraint="cauline" id="o15645" name="blade" name_original="blades" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity constraint="midcauline" id="o15646" name="blade" name_original="blades" src="d0_s9" type="structure">
        <character is_modifier="false" name="length" src="d0_s9" value="longest" value_original="longest" />
      </biological_entity>
      <biological_entity id="o15647" name="blade" name_original="blades" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="gradually; distally" name="length" src="d0_s9" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s9" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s9" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s9" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s9" value="involute" value_original="involute" />
        <character is_modifier="false" modifier="usually" name="width" src="d0_s9" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="usually" name="life_cycle" src="d0_s9" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o15648" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o15649" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s9" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
      <biological_entity id="o15650" name="blade-leaf" name_original="leaf-blades" src="d0_s9" type="structure">
        <character name="atypical_distance" src="d0_s9" unit="cm" value="6" value_original="6" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="distance" src="d0_s9" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Panicles 2-10 (12) cm, usually erect, contracted or loosely contracted, narrowly lanceoloid to ovoid, congested or moderately congested, with 10-100 spikelets and 1-3 (5) branches per node;</text>
      <biological_entity id="o15651" name="panicle" name_original="panicles" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="cm" value="12" value_original="12" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s10" to="10" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="condition_or_size" src="d0_s10" value="contracted" value_original="contracted" />
        <character is_modifier="false" modifier="loosely" name="condition_or_size" src="d0_s10" value="contracted" value_original="contracted" />
        <character char_type="range_value" from="narrowly lanceoloid" name="shape" src="d0_s10" to="ovoid" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s10" value="congested" value_original="congested" />
        <character is_modifier="false" modifier="moderately" name="architecture_or_arrangement" src="d0_s10" value="congested" value_original="congested" />
      </biological_entity>
      <biological_entity id="o15652" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s10" to="100" />
        <character name="atypical_quantity" src="d0_s10" value="5" value_original="5" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s10" to="3" />
      </biological_entity>
      <biological_entity id="o15653" name="branch" name_original="branches" src="d0_s10" type="structure" />
      <biological_entity id="o15654" name="node" name_original="node" src="d0_s10" type="structure" />
      <relation from="o15651" id="r2502" name="with" negation="false" src="d0_s10" to="o15652" />
      <relation from="o15653" id="r2503" name="per" negation="false" src="d0_s10" to="o15654" />
    </statement>
    <statement id="d0_s11">
      <text>branches 0.5-4 (5) cm, erect or steeply ascending, fairly straight, slender to stout, terete to angled, smooth or scabrous, with 1-15 spikelets.</text>
      <biological_entity id="o15655" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character name="atypical_some_measurement" src="d0_s11" unit="cm" value="5" value_original="5" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s11" to="4" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="steeply" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="fairly" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character char_type="range_value" from="slender" name="size" src="d0_s11" to="stout" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s11" to="angled" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o15656" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s11" to="15" />
      </biological_entity>
      <relation from="o15655" id="r2504" name="with" negation="false" src="d0_s11" to="o15656" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets (3) 4-10 mm, lengths to 3 times widths, broadly lanceolate to narrowly ovate, laterally compressed, not sexually dimorphic;</text>
      <biological_entity id="o15657" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s12" value="0-3" value_original="0-3" />
        <character char_type="range_value" from="broadly lanceolate" name="shape" src="d0_s12" to="narrowly ovate" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="not sexually" name="growth_form" src="d0_s12" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>florets 2-6;</text>
      <biological_entity id="o15658" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s13" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>rachilla internodes 0.5-1.2 mm, smooth or scabrous.</text>
      <biological_entity constraint="rachilla" id="o15659" name="internode" name_original="internodes" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s14" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Glumes lanceolate, distinctly keeled;</text>
      <biological_entity id="o15660" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s15" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower glumes 3-veined, distinctly shorter than the lowest lemmas;</text>
      <biological_entity constraint="lower" id="o15661" name="glume" name_original="glumes" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="3-veined" value_original="3-veined" />
        <character constraint="than the lowest lemmas" constraintid="o15662" is_modifier="false" name="height_or_length_or_size" src="d0_s16" value="distinctly shorter" value_original="distinctly shorter" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o15662" name="lemma" name_original="lemmas" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>calluses glabrous or diffusely webbed, hairs less than 1/4 the lemma length;</text>
      <biological_entity id="o15663" name="callus" name_original="calluses" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="diffusely" name="pubescence" src="d0_s17" value="webbed" value_original="webbed" />
      </biological_entity>
      <biological_entity id="o15664" name="hair" name_original="hairs" src="d0_s17" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s17" to="1/4" />
      </biological_entity>
      <biological_entity id="o15665" name="lemma" name_original="lemma" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>lemmas (3) 4-7 mm, lanceolate to broadly lanceolate, distinctly keeled, membranous to thinly membranous, smooth or sparsely to densely scabrous, glabrous or the keels and/or marginal veins puberulent proximally, lateral-veins obscure to prominent, margins glabrous, apices acute;</text>
      <biological_entity id="o15666" name="lemma" name_original="lemmas" src="d0_s18" type="structure">
        <character name="atypical_some_measurement" src="d0_s18" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s18" to="7" to_unit="mm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s18" to="broadly lanceolate" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s18" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="membranous" name="texture" src="d0_s18" to="thinly membranous" />
        <character char_type="range_value" from="smooth or" name="pubescence" src="d0_s18" to="sparsely densely scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15667" name="keel" name_original="keels" src="d0_s18" type="structure" />
      <biological_entity constraint="marginal" id="o15668" name="vein" name_original="veins" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s18" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o15669" name="lateral-vein" name_original="lateral-veins" src="d0_s18" type="structure">
        <character char_type="range_value" from="obscure" name="prominence" src="d0_s18" to="prominent" />
      </biological_entity>
      <biological_entity id="o15670" name="margin" name_original="margins" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15671" name="apex" name_original="apices" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>palea keels scabrous, intercostal regions glabrous;</text>
      <biological_entity constraint="palea" id="o15672" name="keel" name_original="keels" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s19" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o15673" name="region" name_original="regions" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>anthers vestigial (0.1-0.2 mm), aborted late in development, or 2-3.5 mm. 2n = 28, 28+11, 56, 56+11, 59, ca. 70.</text>
      <biological_entity id="o15674" name="anther" name_original="anthers" src="d0_s20" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s20" value="vestigial" value_original="vestigial" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s20" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15675" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="28" value_original="28" />
        <character char_type="range_value" from="28" name="quantity" src="d0_s20" upper_restricted="false" />
        <character name="quantity" src="d0_s20" value="11" value_original="11" />
        <character name="quantity" src="d0_s20" value="56" value_original="56" />
        <character char_type="range_value" from="56" name="quantity" src="d0_s20" upper_restricted="false" />
        <character name="quantity" src="d0_s20" value="11" value_original="11" />
        <character name="quantity" src="d0_s20" value="59" value_original="59" />
        <character name="quantity" src="d0_s20" value="70" value_original="70" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa cusickii grows in rich meadows in sagebrush scrub to rocky alpine slopes, from the southwestern Yukon Territory to Manitoba and North Dakota, south to central California and eastern Colorado. It is gynodioecious or dioecious.</discussion>
  <discussion>Sexually reproducing plants of Poa cusickii subspp. cusickii and pallida grow in different geographic areas, but pistillate plants of these two subspecies have overlapping ranges. Only pistillate plants are known in Poa cusickii subspp. epilis and purpurascens. All the alpine plants studied were pistillate.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev.;Colo.;Wash.;Wyo.;Utah;Calif.;Oreg.;Mont.;Alta.;B.C.;Man.;Sask.;Yukon;Idaho;N.Dak.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Panicle branches smooth or slightly scabrous, or the basal blades more than 1.5 mm wide and flat or folded; cauline blades more than 1.5 mm wide, often flat; some basal branching extravaginal; lemmas and calluses sometimes sparsely puberulent.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lemmas usually glabrous, rarely plants from the Rocky Mountains with puberulent keels and marginal veins; calluses glabrous; panicles erect, usually contracted; branches smooth to slightly scabrous</description>
      <determination>Poa cusickii subsp. epilis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lemmas rarely completely glabrous, at least some florets v/ith sparsely puberulent keels, the marginal veins glabrous or puberulent; calluses frequendy with a sparse, short web; panicles somewhat lax and loosely contracted; branches smooth or sparsely to moderately scabrous</description>
      <determination>Poa cusickii subsp. purpurascens</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Panicle branches moderately to strongly scabrous; basal and cauline blades usually less than 1.5 mm wide, involute, rarely flat or folded; basal branching intravaginal; lemmas and calluses glabrous.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Panicle branches longer than 1.7 cm in at least some panicles; panicles open or contracted</description>
      <determination>Poa cusickii subsp. cusickii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Panicle branches up to 1.7 cm long, stout; panicles contracted</description>
      <determination>Poa cusickii subsp. pallida</determination>
    </key_statement>
  </key>
</bio:treatment>