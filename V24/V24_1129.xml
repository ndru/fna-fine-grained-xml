<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Poa</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="species">arctica</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section poa;species arctica</taxon_hierarchy>
  </taxon_identification>
  <number>16</number>
  <other_name type="common_name">Arctic bluegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually strongly anthocyanic;</text>
    </statement>
    <statement id="d0_s2">
      <text>rhizomatous, rhizomes usually well developed, sometimes poorly developed, shoots usually solitary.</text>
      <biological_entity id="o20828" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="usually strongly" name="coloration" src="d0_s1" value="anthocyanic" value_original="anthocyanic" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o20829" name="rhizom" name_original="rhizomes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually well" name="development" src="d0_s2" value="developed" value_original="developed" />
        <character is_modifier="false" modifier="sometimes poorly" name="development" src="d0_s2" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal branching mainly extravaginal.</text>
      <biological_entity id="o20830" name="shoot" name_original="shoots" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="mainly" name="position" src="d0_s3" value="extravaginal" value_original="extravaginal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Culms 7.5-60 cm, slender to stout, terete or weakly compressed, bases usually decumbent, not branching above the bases;</text>
      <biological_entity id="o20831" name="culm" name_original="culms" src="d0_s4" type="structure">
        <character char_type="range_value" from="7.5" from_unit="cm" name="some_measurement" src="d0_s4" to="60" to_unit="cm" />
        <character char_type="range_value" from="slender" name="size" src="d0_s4" to="stout" />
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s4" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o20832" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="growth_form_or_orientation" src="d0_s4" value="decumbent" value_original="decumbent" />
        <character constraint="above bases" constraintid="o20833" is_modifier="false" modifier="not" name="architecture" src="d0_s4" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o20833" name="base" name_original="bases" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>nodes terete, proximal nodes usually not exserted, 0-2 exserted above.</text>
      <biological_entity id="o20834" name="node" name_original="nodes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o20835" name="node" name_original="nodes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually not" name="position" src="d0_s5" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s5" to="2" />
        <character is_modifier="false" name="position" src="d0_s5" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Sheaths closed for (1/6) 1/5 – 2/5 their length, terete, glabrous, smooth or sparsely scabrous, bases of basal sheaths glabrous, distal sheath lengths 1.4-4 (5.3) times blade lengths;</text>
      <biological_entity id="o20836" name="sheath" name_original="sheaths" src="d0_s6" type="structure">
        <character is_modifier="false" name="length" src="d0_s6" value="closed" value_original="closed" />
        <character is_modifier="false" name="shape" src="d0_s6" value="terete" value_original="terete" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o20837" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o20838" name="sheath" name_original="sheaths" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o20839" name="sheath" name_original="sheath" src="d0_s6" type="structure">
        <character constraint="blade" constraintid="o20840" is_modifier="false" name="length" src="d0_s6" value="1.4-4(5.3) times blade lengths" value_original="1.4-4(5.3) times blade lengths" />
      </biological_entity>
      <biological_entity id="o20840" name="blade" name_original="blade" src="d0_s6" type="structure" />
      <relation from="o20837" id="r3292" name="part_of" negation="false" src="d0_s6" to="o20838" />
    </statement>
    <statement id="d0_s7">
      <text>collars smooth, glabrous;</text>
      <biological_entity id="o20841" name="collar" name_original="collars" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>ligules (1) 2-7 mm, glabrous, smooth or sparsely to infrequently moderately scabrous, apices usually rounded to obtuse or acute, rarely truncate, entire or lacerate;</text>
      <biological_entity id="o20842" name="ligule" name_original="ligules" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely to infrequently moderately" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o20843" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="usually rounded" name="shape" src="d0_s8" to="obtuse or acute" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s8" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>blades 1-6 mm wide, flat or folded, somewhat involute, smooth, glabrous, apices broadly prow-shaped, cauline blades subequal or gradually reduced distally, flag leaf-blades 0.7-9 cm.</text>
      <biological_entity id="o20844" name="blade" name_original="blades" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s9" value="folded" value_original="folded" />
        <character is_modifier="false" modifier="somewhat" name="shape_or_vernation" src="d0_s9" value="involute" value_original="involute" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20845" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="prow--shaped" value_original="prow--shaped" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o20846" name="blade" name_original="blades" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s9" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o20847" name="blade-leaf" name_original="leaf-blades" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="distance" src="d0_s9" to="9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Panicles (2) 3.5-15 cm, ovoid to broadly pyramidal, usually open, sparse, with 10-40 (60) spikelets, proximal internodes shorter than 1.5 (3) cm, with (1) 2-5 branches per node;</text>
      <biological_entity id="o20848" name="panicle" name_original="panicles" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s10" to="15" to_unit="cm" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s10" to="broadly pyramidal" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s10" value="open" value_original="open" />
        <character is_modifier="false" name="count_or_density" src="d0_s10" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o20849" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s10" value="60" value_original="60" />
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s10" to="40" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o20850" name="internode" name_original="internodes" src="d0_s10" type="structure">
        <character modifier="shorter than" name="atypical_some_measurement" src="d0_s10" unit="cm" value="3" value_original="3" />
        <character modifier="shorter than" name="some_measurement" src="d0_s10" unit="cm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o20851" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s10" value="1" value_original="1" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
      <biological_entity id="o20852" name="node" name_original="node" src="d0_s10" type="structure" />
      <relation from="o20848" id="r3293" name="with" negation="false" src="d0_s10" to="o20849" />
      <relation from="o20850" id="r3294" name="with" negation="false" src="d0_s10" to="o20851" />
      <relation from="o20851" id="r3295" name="per" negation="false" src="d0_s10" to="o20852" />
    </statement>
    <statement id="d0_s11">
      <text>branches 1.5-6 cm, spreading soon after emergence from the sheath, thin, sinuous, and flexuous to fairly stout and straight, terete, smooth or sparsely to infrequently moderately scabrous, with (1) 2-5 spikelets, the spikelets not crowded.</text>
      <biological_entity id="o20853" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s11" to="6" to_unit="cm" />
        <character constraint="after emergence" constraintid="o20854" is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="width" notes="" src="d0_s11" value="thin" value_original="thin" />
        <character is_modifier="false" name="course" src="d0_s11" value="sinuous" value_original="sinuous" />
        <character is_modifier="false" name="course" src="d0_s11" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" modifier="fairly" name="fragility_or_size" src="d0_s11" value="stout" value_original="stout" />
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s11" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely to infrequently moderately" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o20854" name="emergence" name_original="emergence" src="d0_s11" type="structure" />
      <biological_entity id="o20855" name="sheath" name_original="sheath" src="d0_s11" type="structure" />
      <biological_entity id="o20856" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s11" value="1" value_original="1" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s11" to="5" />
      </biological_entity>
      <biological_entity id="o20857" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s11" value="crowded" value_original="crowded" />
      </biological_entity>
      <relation from="o20854" id="r3296" name="from" negation="false" src="d0_s11" to="o20855" />
      <relation from="o20853" id="r3297" name="with" negation="false" src="d0_s11" to="o20856" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets (3.5) 4.5-8 mm, lengths to 3.5 times widths, laterally compressed, sometimes bulbiferous;</text>
      <biological_entity id="o20858" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="mm" value="3.5" value_original="3.5" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s12" value="0-3.5" value_original="0-3.5" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s12" value="bulbiferous" value_original="bulbiferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>florets (2) 3-6, infrequently bulb-forming;</text>
      <biological_entity id="o20859" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character name="atypical_quantity" src="d0_s13" value="2" value_original="2" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s13" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>rachilla internodes smooth or muriculate, proximal internodes glabrous or sparsely softly puberulent to long-villous.</text>
      <biological_entity constraint="rachilla" id="o20860" name="internode" name_original="internodes" src="d0_s14" type="structure">
        <character is_modifier="false" name="relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s14" value="muriculate" value_original="muriculate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o20861" name="internode" name_original="internodes" src="d0_s14" type="structure">
        <character char_type="range_value" from="sparsely softly puberulent" name="pubescence" src="d0_s14" to="long-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Glumes lanceolate to broadly lanceolate, distinctly or weakly keeled, keels usually smooth, sometimes sparsely scabrous distally, lateral-veins usually moderately pronounced;</text>
      <biological_entity id="o20862" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s15" to="broadly lanceolate distinctly or weakly keeled" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s15" to="broadly lanceolate distinctly or weakly keeled" />
      </biological_entity>
      <biological_entity id="o20863" name="keel" name_original="keels" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sometimes sparsely; distally" name="pubescence_or_relief" src="d0_s15" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o20864" name="lateral-vein" name_original="lateral-veins" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually moderately" name="prominence" src="d0_s15" value="pronounced" value_original="pronounced" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower glumes (3) 3.5-5 (6) mm, 3-veined;</text>
      <biological_entity constraint="lower" id="o20865" name="glume" name_original="glumes" src="d0_s16" type="structure">
        <character name="atypical_some_measurement" src="d0_s16" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper glumes 3.5-5.5 (6.5) mm, nearly equaling to slightly exceeding the lowest lemmas, or distinctly shorter;</text>
      <biological_entity constraint="upper" id="o20866" name="glume" name_original="glumes" src="d0_s17" type="structure">
        <character name="atypical_some_measurement" src="d0_s17" unit="mm" value="6.5" value_original="6.5" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s17" to="5.5" to_unit="mm" />
        <character is_modifier="false" modifier="nearly" name="variability" src="d0_s17" value="equaling" value_original="equaling" />
        <character is_modifier="false" modifier="distinctly" name="height_or_length_or_size" src="d0_s17" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o20867" name="lemma" name_original="lemmas" src="d0_s17" type="structure" />
      <relation from="o20866" id="r3298" modifier="slightly" name="exceeding the" negation="false" src="d0_s17" to="o20867" />
    </statement>
    <statement id="d0_s18">
      <text>calluses glabrous or webbed, hairs sparse and short to over 1/3 – 2/3 the lemma length;</text>
      <biological_entity id="o20868" name="callus" name_original="calluses" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="webbed" value_original="webbed" />
      </biological_entity>
      <biological_entity id="o20869" name="hair" name_original="hairs" src="d0_s18" type="structure">
        <character is_modifier="false" name="count_or_density" src="d0_s18" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="length" src="d0_s18" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>lemmas (2.7) 3-6 (7) mm, lanceolate to broadly lanceolate, usually strongly purple, distinctly keeled, keels, marginal veins, and lateral-veins long-villous, hairs on the lateral-veins sometimes shorter, lateral-veins prominent, intercostal regions short-villous to softly puberulent at least near the base, glabrous elsewhere, smooth to weakly muriculate and/or usually sparsely scabrous, infrequently moderately scabrous, margins broadly hyaline, glabrous, apices acute;</text>
      <biological_entity id="o20870" name="lemma" name_original="lemmas" src="d0_s19" type="structure">
        <character name="atypical_some_measurement" src="d0_s19" unit="mm" value="2.7" value_original="2.7" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s19" to="6" to_unit="mm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s19" to="broadly lanceolate" />
        <character is_modifier="false" modifier="usually strongly" name="coloration_or_density" src="d0_s19" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s19" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o20871" name="keel" name_original="keels" src="d0_s19" type="structure" />
      <biological_entity constraint="marginal" id="o20872" name="vein" name_original="veins" src="d0_s19" type="structure" />
      <biological_entity id="o20873" name="lateral-vein" name_original="lateral-veins" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="long-villous" value_original="long-villous" />
      </biological_entity>
      <biological_entity id="o20874" name="hair" name_original="hairs" src="d0_s19" type="structure" />
      <biological_entity id="o20875" name="lateral-vein" name_original="lateral-veins" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="sometimes" name="height_or_length_or_size" src="d0_s19" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o20876" name="lateral-vein" name_original="lateral-veins" src="d0_s19" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s19" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o20877" name="region" name_original="regions" src="d0_s19" type="structure">
        <character char_type="range_value" constraint="near base" constraintid="o20878" from="short-villous" name="pubescence" src="d0_s19" to="softly puberulent" />
        <character is_modifier="false" modifier="elsewhere" name="pubescence" notes="" src="d0_s19" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s19" to="weakly muriculate" />
        <character is_modifier="false" modifier="usually sparsely" name="pubescence_or_relief" src="d0_s19" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="infrequently moderately" name="pubescence_or_relief" src="d0_s19" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o20878" name="base" name_original="base" src="d0_s19" type="structure" />
      <biological_entity id="o20879" name="margin" name_original="margins" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="broadly" name="coloration" src="d0_s19" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20880" name="apex" name_original="apices" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o20874" id="r3299" name="on" negation="false" src="d0_s19" to="o20875" />
    </statement>
    <statement id="d0_s20">
      <text>palea keels usually short to long-villous for most of their length, rarely nearly glabrous and scabrous, intercostal regions broad, usually at least sparsely softly puberulent, rarely glabrous, apices scabrous;</text>
      <biological_entity constraint="palea" id="o20881" name="keel" name_original="keels" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="usually" name="height_or_length_or_size" src="d0_s20" value="short" value_original="short" />
        <character constraint="for" is_modifier="false" name="length" src="d0_s20" value="long-villous" value_original="long-villous" />
        <character is_modifier="false" modifier="rarely nearly" name="pubescence" src="d0_s20" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s20" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o20882" name="region" name_original="regions" src="d0_s20" type="structure">
        <character is_modifier="false" name="width" src="d0_s20" value="broad" value_original="broad" />
        <character is_modifier="false" modifier="usually at-least sparsely softly" name="pubescence" src="d0_s20" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s20" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20883" name="apex" name_original="apices" src="d0_s20" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s20" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>anthers 1.4-2.5 mm, sometimes aborted late in development.</text>
    </statement>
    <statement id="d0_s22">
      <text>2n = 36, 42, 56, 60, 62-68, 70, ca. 72, 74-76, 78-80, 82-84, 86, 88, 99, 106.</text>
      <biological_entity id="o20884" name="anther" name_original="anthers" src="d0_s21" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s21" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20885" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="36" value_original="36" />
        <character name="quantity" src="d0_s22" value="42" value_original="42" />
        <character name="quantity" src="d0_s22" value="56" value_original="56" />
        <character name="quantity" src="d0_s22" value="60" value_original="60" />
        <character char_type="range_value" from="62" name="quantity" src="d0_s22" to="68" />
        <character name="quantity" src="d0_s22" value="70" value_original="70" />
        <character name="quantity" src="d0_s22" value="72" value_original="72" />
        <character char_type="range_value" from="74" name="quantity" src="d0_s22" to="76" />
        <character char_type="range_value" from="78" name="quantity" src="d0_s22" to="80" />
        <character char_type="range_value" from="82" name="quantity" src="d0_s22" to="84" />
        <character name="quantity" src="d0_s22" value="86" value_original="86" />
        <character name="quantity" src="d0_s22" value="88" value_original="88" />
        <character name="quantity" src="d0_s22" value="99" value_original="99" />
        <character name="quantity" src="d0_s22" value="106" value_original="106" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa arctica is a common circumboreal species of arctic and alpine regions, growing mainly in mesic to subhydric, acidic tundra and alpine meadows, and on rocky slopes. It extends south in the Rocky Mountains to New Mexico. The frequency of sterile anthers in plants of the high arctic suggests that P. arctica is sometimes apomictic in that region. Over most of the rest of its range, P. arctica usually develops normal anthers. This and isozyme data for populations from alpine and low arctic regions suggest sexual reproduction is common in these habitats.</discussion>
  <discussion>The most reliable way to distinguish Poa arctica from P. pratensis (p. 522), particularly subsp. alpigena, is by the wider paleas and the presence of hairs between the palea keels. Bulbiferous forms of P. arctica differ from P. stenantha var. vivipara (p. 594) in not being glaucous, and in having rhizomes and terete, smooth panicle branches. Poa xgaspensis (p. 601) also resembles P. arctica, but it has sharply keeled, more scabrous glumes and a spikelet shape that is intermediate between P. pratensis and P. alpina (p. 518). Poa arctica forms natural hybrids with both P. pratensis and P. secunda (p. 586).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mont.;Wyo.;Colo.;N.Mex.;Wash.;Utah;Alaska;Alta.;B.C.;Greenland;Man.;Nfld. and Labr.;N.W.T.;Nunavut;Ont.;Que.;Sask.;Yukon;Idaho;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Plants lacking well-developed rhizomes; anthers aborted late in development; plants of the high arctic</description>
      <determination>Poa arctica subsp. caespitans</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Plants usually with well-developed rhizomes; anthers normal or plants not of the high arctic.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Panicles erect, the branches relatively stout, fairly straight; longest branches of the lowest panicle nodes 1/4-1/2 the length of the panicles; culms wiry, usually several together; calluses glabrous or shortly webbed; paleas sometimes glabrous; plants glaucous, growing in the southern Rocky-Mountains and adjacent portions of the Intermountain region</description>
      <determination>Poa arctica subsp. aperta</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Panicles lax to erect, the branches slender, flexuous to fairly stout and straight; longest branches of the lowest panicle nodes 2/5 – 3/5 the length of the panicles; culms slender to stout, varying from solitary to several together; calluses glabrous or webbed, the hairs usually more than 1/2 as long as the lemmas; paleas pubescent; plants sometimes glaucous, widespread in distribution. 3. Calluses glabrous; spikelets not bulbiferous</description>
      <determination>Poa arctica subsp. grayana</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Calluses webbed,  often copiously so, sometimes glabrous in bulbiferous spikelets; spikelets sometimes bulbiferous.</description>
      <next_statement_id>4.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Spikelets (5)6-8 mm long; lemmas 4-6 mm long; blades 2-6 mm wide; rachillas usually hairy; plants primarily of the western arctic, extending to northwestern  British Columbia</description>
      <determination>Poa arctica subsp. lanata</determination>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Spikelets (3.5)4-7 mm long; lemmas (2.7)3-4.5 mm long; blades 1.5-3 mm wide; rachillas commonly glabrous; plants widespread</description>
      <determination>Poa arctica subsp. arctica</determination>
    </key_statement>
  </key>
</bio:treatment>