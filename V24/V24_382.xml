<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">265</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">AEGILOPS</taxon_name>
    <taxon_name authority="Req. ex Bertol." date="unknown" rank="species">neglecta</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus aegilops;species neglecta</taxon_hierarchy>
  </taxon_identification>
  <number>7</number>
  <other_name type="common_name">Three-awned goatgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 20-55 cm, geniculate and semiprostrate at the base, usually with many tillers.</text>
      <biological_entity id="o20794" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="55" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s0" value="geniculate" value_original="geniculate" />
        <character constraint="at base" constraintid="o20795" is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="semiprostrate" value_original="semiprostrate" />
      </biological_entity>
      <biological_entity id="o20795" name="base" name_original="base" src="d0_s0" type="structure" />
      <biological_entity id="o20796" name="tiller" name_original="tillers" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="many" value_original="many" />
      </biological_entity>
      <relation from="o20794" id="r3281" modifier="usually" name="with" negation="false" src="d0_s0" to="o20796" />
    </statement>
    <statement id="d0_s1">
      <text>Sheaths with hyaline margins, ciliate;</text>
      <biological_entity id="o20797" name="sheath" name_original="sheaths" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" notes="" src="d0_s1" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o20798" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <relation from="o20797" id="r3282" name="with" negation="false" src="d0_s1" to="o20798" />
    </statement>
    <statement id="d0_s2">
      <text>blades 5-8 cm long, 3-4 mm wide.</text>
      <biological_entity id="o20799" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s2" to="8" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Spikes 2-4.5 cm long, 0.4-1.3 cm wide, ovoid-ellipsoid proximally, abruptly contracted distally to a narrow cylinder, with 3-6 spikelets, the distal 1-3 spikelets sterile;</text>
      <biological_entity id="o20800" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s3" to="1.3" to_unit="cm" />
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s3" value="ovoid-ellipsoid" value_original="ovoid-ellipsoid" />
        <character constraint="to cylinder" constraintid="o20801" is_modifier="false" modifier="abruptly" name="condition_or_size" src="d0_s3" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o20801" name="cylinder" name_original="cylinder" src="d0_s3" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s3" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o20802" name="spikelet" name_original="spikelets" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s3" to="6" />
      </biological_entity>
      <biological_entity constraint="distal" id="o20803" name="spikelet" name_original="spikelets" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
      <biological_entity id="o20804" name="spikelet" name_original="spikelets" src="d0_s3" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s3" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o20800" id="r3283" name="with" negation="false" src="d0_s3" to="o20802" />
    </statement>
    <statement id="d0_s4">
      <text>rudimentary spikelets (2) 3;</text>
    </statement>
    <statement id="d0_s5">
      <text>disarticulation at the base of the spikes.</text>
      <biological_entity id="o20805" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s4" value="rudimentary" value_original="rudimentary" />
        <character name="atypical_quantity" src="d0_s4" value="2" value_original="2" />
        <character name="quantity" src="d0_s4" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o20806" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o20807" name="spike" name_original="spikes" src="d0_s5" type="structure" />
      <relation from="o20805" id="r3284" name="at" negation="false" src="d0_s5" to="o20806" />
      <relation from="o20806" id="r3285" name="part_of" negation="false" src="d0_s5" to="o20807" />
    </statement>
    <statement id="d0_s6">
      <text>Fertile spikelets 8-12 mm, subventricose, urceolate, with 2 fertile florets and 2-3 sterile florets;</text>
      <biological_entity id="o20808" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s6" to="12" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" name="reproduction" notes="" src="d0_s6" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o20810" name="floret" name_original="florets" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
        <character is_modifier="true" name="reproduction" src="d0_s6" value="fertile" value_original="fertile" />
      </biological_entity>
      <relation from="o20808" id="r3286" name="with" negation="false" src="d0_s6" to="o20810" />
    </statement>
    <statement id="d0_s7">
      <text>apical spikelets 4-5 mm, usually with 1 sterile floret, cultivated forms often with 1 fertile floret and 1-2 sterile florets.</text>
      <biological_entity constraint="apical" id="o20811" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
        <character constraint="often with floret" constraintid="o20813" is_modifier="false" name="condition" notes="" src="d0_s7" value="cultivated" value_original="cultivated" />
      </biological_entity>
      <biological_entity id="o20812" name="floret" name_original="floret" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s7" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o20813" name="floret" name_original="floret" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="2" />
      </biological_entity>
      <biological_entity id="o20814" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o20811" id="r3287" modifier="usually" name="with" negation="false" src="d0_s7" to="o20812" />
    </statement>
    <statement id="d0_s8">
      <text>Glumes of fertile spikelets 7-10 mm, usually velutinous, sometimes scabrous, apices with (2) 3 awns, awns 2-5.5 cm;</text>
      <biological_entity id="o20815" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s8" value="velutinous" value_original="velutinous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o20816" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="fertile" value_original="fertile" />
      </biological_entity>
      <biological_entity id="o20817" name="apex" name_original="apices" src="d0_s8" type="structure" />
      <biological_entity id="o20818" name="awn" name_original="awns" src="d0_s8" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="true" name="quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o20819" name="awn" name_original="awns" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s8" to="5.5" to_unit="cm" />
      </biological_entity>
      <relation from="o20815" id="r3288" name="part_of" negation="false" src="d0_s8" to="o20816" />
      <relation from="o20817" id="r3289" name="with" negation="false" src="d0_s8" to="o20818" />
    </statement>
    <statement id="d0_s9">
      <text>lemmas of fertile spikelets 2-awned, awns 0.5-4 cm, sometimes with 1 lateral tooth.</text>
      <biological_entity id="o20820" name="lemma" name_original="lemmas" src="d0_s9" type="structure" constraint="spikelet" constraint_original="spikelet; spikelet">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="2-awned" value_original="2-awned" />
      </biological_entity>
      <biological_entity id="o20821" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="fertile" value_original="fertile" />
      </biological_entity>
      <biological_entity id="o20822" name="awn" name_original="awns" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s9" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o20823" name="tooth" name_original="tooth" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
      <relation from="o20820" id="r3290" name="part_of" negation="false" src="d0_s9" to="o20821" />
      <relation from="o20822" id="r3291" modifier="sometimes" name="with" negation="false" src="d0_s9" to="o20823" />
    </statement>
    <statement id="d0_s10">
      <text>Caryopses 5-7 mm, falling free of the lemmas and paleas.</text>
      <biological_entity id="o20825" name="lemma" name_original="lemmas" src="d0_s10" type="structure" />
      <biological_entity id="o20826" name="palea" name_original="paleas" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Haplomes UM, UMN.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 28, 48.</text>
      <biological_entity id="o20824" name="caryopse" name_original="caryopses" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
        <character is_modifier="false" name="life_cycle" src="d0_s10" value="falling" value_original="falling" />
        <character constraint="of paleas" constraintid="o20826" is_modifier="false" name="fusion" src="d0_s10" value="free" value_original="free" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20827" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="28" value_original="28" />
        <character name="quantity" src="d0_s12" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Aegilops neglecta is native around the Mediterranean and in western Asia. It has been collected in Arlington County, Virginia, and near Corvallis, Oregon; the Oregon record indicates it is persisting from previous cultivation and becoming weedy.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.;Oreg.;Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>