<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">216</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">BROMEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">BROMUS</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="section">Bromopsis</taxon_name>
    <taxon_name authority="(Hook.) Shear" date="unknown" rank="species">vulgaris</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe bromeae;genus bromus;section bromopsis;species vulgaris</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bromus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ciliatus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">glaberrimus</taxon_name>
    <taxon_hierarchy>genus bromus;species ciliatus;variety glaberrimus</taxon_hierarchy>
  </taxon_identification>
  <number>23</number>
  <other_name type="common_name">Common brome</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>not rhizomatous.</text>
      <biological_entity id="o18186" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 60-120 cm, erect or spreading;</text>
      <biological_entity id="o18187" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s2" to="120" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes (3) 4-6 (7), usually pilose;</text>
      <biological_entity id="o18188" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character name="atypical_quantity" src="d0_s3" value="3" value_original="3" />
        <character name="atypical_quantity" src="d0_s3" value="7" value_original="7" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s3" to="6" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>internodes glabrous.</text>
      <biological_entity id="o18189" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths pilose or glabrous;</text>
      <biological_entity id="o18190" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>auricles absent;</text>
      <biological_entity id="o18191" name="auricle" name_original="auricles" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules 2-6 mm, glabrous, obtuse or truncate, erose or lacerate;</text>
      <biological_entity id="o18192" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s7" value="erose" value_original="erose" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blades 13-25 (33) cm long, to 14 mm wide, flat, abaxial surfaces usually glabrous, sometimes pilose, adaxial surfaces usually pilose, sometimes glabrous.</text>
      <biological_entity id="o18193" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character name="length" src="d0_s8" unit="cm" value="33" value_original="33" />
        <character char_type="range_value" from="13" from_unit="cm" name="length" src="d0_s8" to="25" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s8" to="14" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o18194" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o18195" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Panicles 10-15 cm, open;</text>
      <biological_entity id="o18196" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s9" to="15" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>branches ascending to drooping.</text>
      <biological_entity id="o18197" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s10" to="drooping" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 15-30 mm, elliptic to lanceolate, terete to moderately laterally compressed, with (3) 4-9 florets.</text>
      <biological_entity id="o18198" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s11" to="30" to_unit="mm" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s11" to="lanceolate terete" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s11" to="lanceolate terete" />
      </biological_entity>
      <biological_entity id="o18199" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s11" value="3" value_original="3" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s11" to="9" />
      </biological_entity>
      <relation from="o18198" id="r2888" name="with" negation="false" src="d0_s11" to="o18199" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes glabrous or pilose;</text>
      <biological_entity id="o18200" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 5-8 mm, 1 (3) -veined;</text>
      <biological_entity constraint="lower" id="o18201" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="1(3)-veined" value_original="1(3)-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 8-12 mm, 3-veined;</text>
      <biological_entity constraint="upper" id="o18202" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s14" to="12" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 8-15 mm, lanceolate, rounded over the midvein, backs sparsely hairy or glabrous, margins usually coarsely pubescent, sometimes glabrous, apices subulate to acute, entire;</text>
      <biological_entity id="o18203" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s15" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character constraint="over midvein" constraintid="o18204" is_modifier="false" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o18204" name="midvein" name_original="midvein" src="d0_s15" type="structure" />
      <biological_entity id="o18205" name="back" name_original="backs" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s15" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o18206" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually coarsely" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o18207" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s15" to="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>awns (4) 6-12 mm, straight, arising less than 1.5 mm below the lemma apices;</text>
      <biological_entity id="o18208" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character name="atypical_some_measurement" src="d0_s16" unit="mm" value="4" value_original="4" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s16" to="12" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="arising" value_original="arising" />
        <character char_type="range_value" constraint="below lemma apices" constraintid="o18209" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o18209" name="apex" name_original="apices" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>anthers 2-4 mm. 2n = 14.</text>
      <biological_entity id="o18210" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s17" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18211" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bromus vulgaris grows in shaded or partially shaded, often damp, coniferous forests along the coast, and inland in montane pine, spruce, fir, and aspen forests, from sea level to about 2000 m. Its range extends from coastal British Columbia eastward to southwestern Alberta and southward to central California, northern Utah, and western Wyoming.</discussion>
  <discussion>Varieties have been described within Bromus vulgaris; because their variation is overlapping, none are recognized here.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Wash.;Utah;Alta.;B.C.;Idaho;Mont.;Wyo.;Calif.;Oreg.;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>