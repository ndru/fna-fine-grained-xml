<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">618</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Seidl" date="unknown" rank="genus">COLEANTHUS</taxon_name>
    <taxon_name authority="(Tratt.) Seidl" date="unknown" rank="species">subtilis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus coleanthus;species subtilis</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Moss grass</other_name>
  <other_name type="common_name">Mud grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o10866" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms ascending or decumbent.</text>
      <biological_entity id="o10867" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths strongly inflated, uppermost sheath enclosing the base of the panicles;</text>
      <biological_entity id="o10868" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s2" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity constraint="uppermost" id="o10869" name="sheath" name_original="sheath" src="d0_s2" type="structure" />
      <biological_entity id="o10870" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o10871" name="panicle" name_original="panicles" src="d0_s2" type="structure" />
      <relation from="o10869" id="r1729" name="enclosing the" negation="false" src="d0_s2" to="o10870" />
      <relation from="o10869" id="r1730" name="part_of" negation="false" src="d0_s2" to="o10871" />
    </statement>
    <statement id="d0_s3">
      <text>ligules 1-1.5 mm;</text>
      <biological_entity id="o10872" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 1-2 cm long, 0.5-1.5 mm wide.</text>
      <biological_entity id="o10873" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles 1-5 cm, with 3-6 verticils.</text>
      <biological_entity id="o10874" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10875" name="verticil" name_original="verticils" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
      <relation from="o10874" id="r1731" name="with" negation="false" src="d0_s5" to="o10875" />
    </statement>
    <statement id="d0_s6">
      <text>Lemmas 0.75-1.5 mm, ciliate on the keels, awnlike apices about equaling the lower portion;</text>
      <biological_entity id="o10876" name="lemma" name_original="lemmas" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.75" from_unit="mm" name="some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
        <character constraint="on keels" constraintid="o10877" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o10877" name="keel" name_original="keels" src="d0_s6" type="structure" />
      <biological_entity id="o10878" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="awnlike" value_original="awnlike" />
      </biological_entity>
      <biological_entity constraint="lower" id="o10879" name="portion" name_original="portion" src="d0_s6" type="structure">
        <character is_modifier="true" name="variability" src="d0_s6" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>paleas about 0.5 mm, keels slightly prolonged;</text>
      <biological_entity id="o10880" name="palea" name_original="paleas" src="d0_s7" type="structure">
        <character name="some_measurement" src="d0_s7" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity id="o10881" name="keel" name_original="keels" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="slightly" name="length" src="d0_s7" value="prolonged" value_original="prolonged" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers about 0.3 mm.</text>
      <biological_entity id="o10882" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Caryopses about 1 mm. 2n = 14.</text>
      <biological_entity id="o10883" name="caryopse" name_original="caryopses" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10884" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Coleanthus subtilis is an ephemeral pioneer species of wet, open habitats. It grows on wet, muddy to sandy, calcium-deficient soils on the shores of lakes, sandbars, and islands. In the Flora region, it is known from the Columbia River, and around Hatzic, Arrow and Shuswap lakes in British Columbia. It also grows in Europe, Russia, and China. Throughout its range, C. subtilis is known from relatively few, scattered locations. It is easily overlooked because of its diminutive size, and because it flowers in early spring or late fall. It is not clear whether it is native or introduced in the Flora region.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.;Wash.;B.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>