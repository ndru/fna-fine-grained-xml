<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">265</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">AEGILOPS</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">triuncialis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus aegilops;species triuncialis</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>5</number>
  <other_name type="common_name">Barbed goatgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 17-60 cm, geniculate to semiprostrate at the base, usually with several tillers.</text>
      <biological_entity id="o21749" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="17" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s0" value="geniculate" value_original="geniculate" />
        <character constraint="at base" constraintid="o21750" is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="semiprostrate" value_original="semiprostrate" />
      </biological_entity>
      <biological_entity id="o21750" name="base" name_original="base" src="d0_s0" type="structure" />
      <biological_entity id="o21751" name="tiller" name_original="tillers" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="several" value_original="several" />
      </biological_entity>
      <relation from="o21749" id="r3428" modifier="usually" name="with" negation="false" src="d0_s0" to="o21751" />
    </statement>
    <statement id="d0_s1">
      <text>Sheaths with hyaline margins, lower cauline sheath margins usually ciliate;</text>
      <biological_entity id="o21752" name="sheath" name_original="sheaths" src="d0_s1" type="structure" />
      <biological_entity id="o21753" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity constraint="sheath" id="o21754" name="margin" name_original="margins" src="d0_s1" type="structure" constraint_original="lower cauline sheath">
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_shape" src="d0_s1" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <relation from="o21752" id="r3429" name="with" negation="false" src="d0_s1" to="o21753" />
    </statement>
    <statement id="d0_s2">
      <text>blades 1.5-7 cm long, 2-3 mm wide.</text>
      <biological_entity id="o21755" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s2" to="7" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Spikes 2.2-6 cm long, bases 0.4-0.5 cm wide, narrowly ellipsoid, becoming subcylindrical distally, with 2-7 fertile spikelets;</text>
      <biological_entity id="o21756" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character char_type="range_value" from="2.2" from_unit="cm" name="length" src="d0_s3" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21757" name="base" name_original="bases" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s3" to="0.5" to_unit="cm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="becoming; distally" name="shape" src="d0_s3" value="subcylindrical" value_original="subcylindrical" />
      </biological_entity>
      <biological_entity id="o21758" name="spikelet" name_original="spikelets" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s3" to="7" />
        <character is_modifier="true" name="reproduction" src="d0_s3" value="fertile" value_original="fertile" />
      </biological_entity>
      <relation from="o21757" id="r3430" name="with" negation="false" src="d0_s3" to="o21758" />
    </statement>
    <statement id="d0_s4">
      <text>rudimentary spikelets (2) 3;</text>
    </statement>
    <statement id="d0_s5">
      <text>disarticulation at the base of the spikes.</text>
      <biological_entity id="o21759" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s4" value="rudimentary" value_original="rudimentary" />
        <character name="atypical_quantity" src="d0_s4" value="2" value_original="2" />
        <character name="quantity" src="d0_s4" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o21760" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o21761" name="spike" name_original="spikes" src="d0_s5" type="structure" />
      <relation from="o21759" id="r3431" name="at" negation="false" src="d0_s5" to="o21760" />
      <relation from="o21760" id="r3432" name="part_of" negation="false" src="d0_s5" to="o21761" />
    </statement>
    <statement id="d0_s6">
      <text>Lower fertile spikelets 7-13 mm, lanceolate-ovate, with 3-5 florets, the first 1-2 florets fertile;</text>
      <biological_entity constraint="lower" id="o21762" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="13" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate-ovate" value_original="lanceolate-ovate" />
      </biological_entity>
      <biological_entity id="o21763" name="floret" name_original="florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="5" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="2" />
      </biological_entity>
      <biological_entity id="o21764" name="floret" name_original="florets" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="fertile" value_original="fertile" />
      </biological_entity>
      <relation from="o21762" id="r3433" name="with" negation="false" src="d0_s6" to="o21763" />
    </statement>
    <statement id="d0_s7">
      <text>upper spikelets 7-9 mm, reduced.</text>
      <biological_entity constraint="upper" id="o21765" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s7" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Glumes of lower fertile spikelets 6-10 mm, 2-3-awned, awns 1.5-6 cm, glabrous, scabrous, or velutinous, if 3-awned, the central awn often shorter than the lateral awns, sometimes reduced to a tooth;</text>
      <biological_entity id="o21766" name="glume" name_original="glumes" src="d0_s8" type="structure" />
      <biological_entity constraint="lower" id="o21767" name="awn" name_original="awn" src="d0_s8" type="structure" />
      <biological_entity id="o21768" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="2-3-awned" value_original="2-3-awned" />
      </biological_entity>
      <biological_entity id="o21769" name="awn" name_original="awns" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s8" to="6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="velutinous" value_original="velutinous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="velutinous" value_original="velutinous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="3-awned" value_original="3-awned" />
      </biological_entity>
      <biological_entity constraint="central" id="o21770" name="awn" name_original="awn" src="d0_s8" type="structure">
        <character constraint="than the lateral awns" constraintid="o21771" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="often shorter" value_original="often shorter" />
        <character constraint="to tooth" constraintid="o21772" is_modifier="false" modifier="sometimes" name="size" src="d0_s8" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o21771" name="awn" name_original="awns" src="d0_s8" type="structure" />
      <biological_entity id="o21772" name="tooth" name_original="tooth" src="d0_s8" type="structure" />
      <relation from="o21766" id="r3434" name="part_of" negation="false" src="d0_s8" to="o21767" />
    </statement>
    <statement id="d0_s9">
      <text>glumes of apical spikelets 6-8 mm, 3-awned or with 1 awn and 2 lateral teeth, awns 2.5-8 cm, if 3-awned, the central awn the longest;</text>
      <biological_entity id="o21773" name="glume" name_original="glumes" src="d0_s9" type="structure" constraint="spikelet" constraint_original="spikelet; spikelet">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="3-awned" value_original="3-awned" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="with 1 awn" value_original="with 1 awn" />
      </biological_entity>
      <biological_entity constraint="apical" id="o21774" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
      <biological_entity id="o21775" name="awn" name_original="awn" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o21776" name="tooth" name_original="teeth" src="d0_s9" type="structure" />
      <biological_entity id="o21777" name="awn" name_original="awns" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s9" to="8" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="3-awned" value_original="3-awned" />
      </biological_entity>
      <biological_entity constraint="central" id="o21778" name="awn" name_original="awn" src="d0_s9" type="structure">
        <character is_modifier="false" name="length" src="d0_s9" value="longest" value_original="longest" />
      </biological_entity>
      <relation from="o21773" id="r3435" name="part_of" negation="false" src="d0_s9" to="o21774" />
      <relation from="o21773" id="r3436" name="with" negation="false" src="d0_s9" to="o21775" />
    </statement>
    <statement id="d0_s10">
      <text>lemmas of lower fertile spikelets 7-11 mm, with 2-3 teeth, if 3-toothed, the central tooth the longest, sometimes extending into a 10 mm awn.</text>
      <biological_entity id="o21779" name="lemma" name_original="lemmas" src="d0_s10" type="structure" constraint="tooth" constraint_original="tooth; tooth" />
      <biological_entity constraint="lower" id="o21780" name="tooth" name_original="tooth" src="d0_s10" type="structure" />
      <biological_entity id="o21781" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="11" to_unit="mm" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="3-toothed" value_original="3-toothed" />
      </biological_entity>
      <biological_entity id="o21782" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" to="3" />
      </biological_entity>
      <biological_entity constraint="central" id="o21783" name="tooth" name_original="tooth" src="d0_s10" type="structure">
        <character is_modifier="false" name="length" src="d0_s10" value="longest" value_original="longest" />
      </biological_entity>
      <biological_entity id="o21784" name="awn" name_original="awn" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="sometimes" name="some_measurement" src="d0_s10" unit="mm" value="10" value_original="10" />
      </biological_entity>
      <relation from="o21779" id="r3437" name="part_of" negation="false" src="d0_s10" to="o21780" />
      <relation from="o21781" id="r3438" name="with" negation="false" src="d0_s10" to="o21782" />
      <relation from="o21783" id="r3439" modifier="sometimes" name="extending into a" negation="false" src="d0_s10" to="o21784" />
    </statement>
    <statement id="d0_s11">
      <text>Caryopses 5-8 mm, falling free from the lemmas and paleas.</text>
      <biological_entity id="o21786" name="lemma" name_original="lemmas" src="d0_s11" type="structure" />
      <biological_entity id="o21787" name="palea" name_original="paleas" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Haplomes UC.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 28.</text>
      <biological_entity id="o21785" name="caryopse" name_original="caryopses" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
        <character is_modifier="false" name="life_cycle" src="d0_s11" value="falling" value_original="falling" />
        <character constraint="from paleas" constraintid="o21787" is_modifier="false" name="fusion" src="d0_s11" value="free" value_original="free" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21788" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>North American collections of Aegilops triuncialis are from disturbed sites, mostly roadsides and railroads. The native range of the species extends from the Mediterranean area east to central Asia and south to Saudi Arabia. Specimens from the Flora region belong to Aegilops triuncialis L. var. triuncialis, in which the glumes of the apical spikelets have a 5-8 cm central awn flanked by shorter (1-3 cm) lateral awns, and the glumes of the lower fertile spikelets have 2-3 awns of 1.5-6 cm. In A. triuncialis var. persica (Boiss.) Eig, the glumes of the apical spikelets have a 2.5-5.5 cm central awn, and 2 lateral awns of 0.6-2.5 cm that are sometimes reduced to teeth; the lower fertile spikelets have 2 teeth, or 1 tooth and 1 awn to 1 cm long.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md.;Calif.;N.Y.;Pa.;Oreg.;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>