<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">475</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Pari." date="unknown" rank="genus">PUCCINELLIA</taxon_name>
    <taxon_name authority="J.I. Davis" date="unknown" rank="species">howellii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus puccinellia;species howellii</taxon_hierarchy>
  </taxon_identification>
  <number>18</number>
  <other_name type="common_name">Howell's alkali grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, not mat-forming.</text>
      <biological_entity id="o29585" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s1" value="mat-forming" value_original="mat-forming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 7-40 cm, erect to ascending.</text>
      <biological_entity id="o29586" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s2" to="40" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Ligules 1.5-2.7 mm, obtuse, entire or minutely and irregularly serrate;</text>
      <biological_entity id="o29587" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s3" to="2.7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s3" value="minutely" value_original="minutely" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades involute, 1.4-2.2 mm wide when flattened.</text>
      <biological_entity id="o29588" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="involute" value_original="involute" />
        <character char_type="range_value" from="1.4" from_unit="mm" modifier="when flattened" name="width" src="d0_s4" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles 2-13 cm, compact to diffuse at maturity, lower branches erect to descending, spikelet-bearing from near the base or confined to the distal 1/2;</text>
      <biological_entity id="o29589" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s5" to="13" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s5" value="compact" value_original="compact" />
        <character constraint="at lower branches" constraintid="o29590" is_modifier="false" name="density" src="d0_s5" value="diffuse" value_original="diffuse" />
        <character is_modifier="false" name="position_or_shape" src="d0_s5" value="distal" value_original="distal" />
        <character name="quantity" src="d0_s5" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity constraint="lower" id="o29590" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="maturity" value_original="maturity" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s5" to="descending" />
      </biological_entity>
      <biological_entity id="o29591" name="base" name_original="base" src="d0_s5" type="structure" />
      <relation from="o29589" id="r4662" name="from" negation="false" src="d0_s5" to="o29591" />
    </statement>
    <statement id="d0_s6">
      <text>pedicels smooth or with a few scattered scabrules, sometimes with tumid epidermal-cells.</text>
      <biological_entity id="o29592" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character constraint="with a few scattered scabrules" is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="with a few scattered scabrules" />
      </biological_entity>
      <biological_entity id="o29593" name="epidermal-cell" name_original="epidermal-cells" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="tumid" value_original="tumid" />
      </biological_entity>
      <relation from="o29592" id="r4663" modifier="sometimes" name="with" negation="false" src="d0_s6" to="o29593" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 3-8 mm, with (1) 2-5 florets.</text>
      <biological_entity id="o29594" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29595" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s7" value="1" value_original="1" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
      <relation from="o29594" id="r4664" name="with" negation="false" src="d0_s7" to="o29595" />
    </statement>
    <statement id="d0_s8">
      <text>Glumes rounded or weakly keeled over the back, veins obscure, apices acute to obtuse;</text>
      <biological_entity id="o29596" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
        <character constraint="over back" constraintid="o29597" is_modifier="false" modifier="weakly" name="shape" src="d0_s8" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o29597" name="back" name_original="back" src="d0_s8" type="structure" />
      <biological_entity id="o29598" name="vein" name_original="veins" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o29599" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lower glumes 0.8-1.9 mm;</text>
      <biological_entity constraint="lower" id="o29600" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s9" to="1.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>upper glumes 1.7-2.5 mm;</text>
      <biological_entity constraint="upper" id="o29601" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>calluses with a few hairs;</text>
      <biological_entity id="o29602" name="callus" name_original="calluses" src="d0_s11" type="structure" />
      <biological_entity id="o29603" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="few" value_original="few" />
      </biological_entity>
      <relation from="o29602" id="r4665" name="with" negation="false" src="d0_s11" to="o29603" />
    </statement>
    <statement id="d0_s12">
      <text>lemmas 2.4-3.3 mm, herbaceous, glabrous or with a few hairs near the base, principally along the veins, backs rounded or weakly keeled distally, 5-veined, veins obscure, not extending to the margins, apical margins uniformly and densely scabrous, apices acute to obtuse, entire;</text>
      <biological_entity id="o29604" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s12" to="3.3" to_unit="mm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s12" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="with a few hairs" value_original="with a few hairs" />
      </biological_entity>
      <biological_entity id="o29605" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o29606" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o29607" name="vein" name_original="veins" src="d0_s12" type="structure" />
      <biological_entity id="o29608" name="back" name_original="backs" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="weakly; distally" name="shape" src="d0_s12" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="5-veined" value_original="5-veined" />
      </biological_entity>
      <biological_entity id="o29609" name="vein" name_original="veins" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o29610" name="margin" name_original="margins" src="d0_s12" type="structure" />
      <biological_entity constraint="apical" id="o29611" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not; densely" name="pubescence_or_relief" src="d0_s12" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o29612" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s12" to="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o29604" id="r4666" name="with" negation="false" src="d0_s12" to="o29605" />
      <relation from="o29605" id="r4667" name="near" negation="false" src="d0_s12" to="o29606" />
      <relation from="o29604" id="r4668" modifier="principally" name="along" negation="false" src="d0_s12" to="o29607" />
      <relation from="o29609" id="r4669" name="extending to" negation="false" src="d0_s12" to="o29610" />
    </statement>
    <statement id="d0_s13">
      <text>palea veins smooth and glabrous proximally, smooth or scabrous distally;</text>
      <biological_entity constraint="palea" id="o29613" name="vein" name_original="veins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="distally" name="pubescence_or_relief" src="d0_s13" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers 1.5-2 mm. 2n = unknown.</text>
      <biological_entity id="o29614" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29615" name="chromosome" name_original="" src="d0_s14" type="structure" />
    </statement>
  </description>
  <discussion>Puccinellia howellii is known only from the type locality in Shasta County, California, where it is a dominant element of the vegetation associated with a group of three mineralized seeps. Isozyme profiles suggest that it is a polyploid.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>