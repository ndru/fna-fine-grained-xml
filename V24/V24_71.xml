<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Mary E. Barkworth;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">64</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="C.S. Campb." date="unknown" rank="tribe">DIARRHENEAE</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe diarrheneae</taxon_hierarchy>
  </taxon_identification>
  <number>8</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous.</text>
      <biological_entity id="o24878" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms annual, not branching above the base.</text>
      <biological_entity id="o24879" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="annual" value_original="annual" />
        <character constraint="above base" constraintid="o24880" is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o24880" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths open, margins not fused;</text>
      <biological_entity id="o24881" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o24882" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="fusion" src="d0_s3" value="fused" value_original="fused" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>collars glabrous, without tufts of hair at the sides;</text>
      <biological_entity id="o24883" name="collar" name_original="collars" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24884" name="tuft" name_original="tufts" src="d0_s4" type="structure" />
      <biological_entity id="o24885" name="hair" name_original="hair" src="d0_s4" type="structure" />
      <biological_entity id="o24886" name="side" name_original="sides" src="d0_s4" type="structure" />
      <relation from="o24883" id="r3929" name="without" negation="false" src="d0_s4" to="o24884" />
      <relation from="o24884" id="r3930" name="part_of" negation="false" src="d0_s4" to="o24885" />
      <relation from="o24884" id="r3931" name="at" negation="false" src="d0_s4" to="o24886" />
    </statement>
    <statement id="d0_s5">
      <text>auricles sometimes present;</text>
      <biological_entity id="o24887" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules stiff, scarious, ciliolate, those of the upper and lower cauline leaves usually similar;</text>
      <biological_entity id="o24888" name="ligule" name_original="ligules" src="d0_s6" type="structure" constraint="upper; upper">
        <character is_modifier="false" name="fragility" src="d0_s6" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o24889" name="upper" name_original="upper" src="d0_s6" type="structure" />
      <biological_entity constraint="lower cauline" id="o24890" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o24888" id="r3932" name="part_of" negation="false" src="d0_s6" to="o24889" />
    </statement>
    <statement id="d0_s7">
      <text>pseudopetioles absent;</text>
      <biological_entity id="o24891" name="pseudopetiole" name_original="pseudopetioles" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blades tapering both basally and apically, midveins usually eccentric, venation parallel, cross venation not evident;</text>
      <biological_entity id="o24892" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o24893" name="midvein" name_original="midveins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="basally; apically; usually" name="position" src="d0_s8" value="eccentric" value_original="eccentric" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="parallel" value_original="parallel" />
        <character is_modifier="false" name="shape" src="d0_s8" value="cross" value_original="cross" />
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s8" value="evident" value_original="evident" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>cross-sections non-kranz, without arm or fusoid cells;</text>
      <biological_entity id="o24894" name="cross-section" name_original="cross-sections" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="non-kranz" value_original="non-kranz" />
      </biological_entity>
      <biological_entity id="o24895" name="arm" name_original="arm" src="d0_s9" type="structure" />
      <biological_entity constraint="fusoid" id="o24896" name="cell" name_original="cells" src="d0_s9" type="structure" />
      <relation from="o24894" id="r3933" name="without" negation="false" src="d0_s9" to="o24895" />
      <relation from="o24894" id="r3934" name="without" negation="false" src="d0_s9" to="o24896" />
    </statement>
    <statement id="d0_s10">
      <text>epidermes without microhairs or with unicellular microhairs, cells not papillate.</text>
      <biological_entity id="o24897" name="epiderme" name_original="epidermes" src="d0_s10" type="structure" />
      <biological_entity id="o24898" name="microhair" name_original="microhairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="unicellular" value_original="unicellular" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s10" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity id="o24899" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="unicellular" value_original="unicellular" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s10" value="papillate" value_original="papillate" />
      </biological_entity>
      <relation from="o24897" id="r3935" name="without microhairs or with" negation="false" src="d0_s10" to="o24898" />
      <relation from="o24897" id="r3936" name="without microhairs or with" negation="false" src="d0_s10" to="o24899" />
    </statement>
    <statement id="d0_s11">
      <text>Inflorescences terminal panicles.</text>
      <biological_entity id="o24900" name="inflorescence" name_original="inflorescences" src="d0_s11" type="structure" />
      <biological_entity constraint="terminal" id="o24901" name="panicle" name_original="panicles" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets laterally compressed, pedicellate, with (2) 3-5 (7) florets, distal floret (s) reduced and sterile, sometimes concealed by the subterminal florets;</text>
      <biological_entity id="o24902" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity id="o24903" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s12" value="7" value_original="7" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s12" to="5" />
      </biological_entity>
      <biological_entity constraint="distal" id="o24904" name="floret" name_original="floret" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="sterile" value_original="sterile" />
        <character constraint="by subterminal florets" constraintid="o24905" is_modifier="false" modifier="sometimes" name="prominence" src="d0_s12" value="concealed" value_original="concealed" />
      </biological_entity>
      <biological_entity constraint="subterminal" id="o24905" name="floret" name_original="florets" src="d0_s12" type="structure" />
      <relation from="o24902" id="r3937" name="with" negation="false" src="d0_s12" to="o24903" />
    </statement>
    <statement id="d0_s13">
      <text>rachillas not prolonged beyond the terminal, sterile floret;</text>
      <biological_entity id="o24907" name="floret" name_original="floret" src="d0_s13" type="structure" constraint="terminal">
        <character is_modifier="true" name="reproduction" src="d0_s13" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>disarticulation above the glumes and beneath the florets.</text>
      <biological_entity id="o24906" name="rachilla" name_original="rachillas" src="d0_s13" type="structure">
        <character constraint="beyond floret" constraintid="o24907" is_modifier="false" modifier="not" name="length" src="d0_s13" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity id="o24908" name="floret" name_original="florets" src="d0_s14" type="structure" />
      <relation from="o24906" id="r3938" name="above the glumes and beneath" negation="false" src="d0_s14" to="o24908" />
    </statement>
    <statement id="d0_s15">
      <text>Glumes 2, 1-5-veined, at least the upper glumes longer than 1/4 the length of the adjacent floret;</text>
      <biological_entity id="o24909" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="2" value_original="2" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="1-5-veined" value_original="1-5-veined" />
      </biological_entity>
      <biological_entity constraint="upper" id="o24910" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1/4" value_original="1/4" />
      </biological_entity>
      <biological_entity id="o24911" name="floret" name_original="floret" src="d0_s15" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s15" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>florets laterally compressed;</text>
      <biological_entity id="o24912" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s16" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>calluses glabrous or with a few hairs, rounded;</text>
      <biological_entity id="o24913" name="callus" name_original="calluses" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="with a few hairs" value_original="with a few hairs" />
        <character is_modifier="false" name="shape" notes="" src="d0_s17" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o24914" name="hair" name_original="hairs" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="few" value_original="few" />
      </biological_entity>
      <relation from="o24913" id="r3939" name="with" negation="false" src="d0_s17" to="o24914" />
    </statement>
    <statement id="d0_s18">
      <text>lemmas lanceolate, cartilaginous to thinly coriaceous, 3 (5) -veined, veins inconspicuous, apices unawned, sometimes mucronate;</text>
      <biological_entity id="o24915" name="lemma" name_original="lemmas" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="cartilaginous" name="texture" src="d0_s18" to="thinly coriaceous" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="3(5)-veined" value_original="3(5)-veined" />
      </biological_entity>
      <biological_entity id="o24916" name="vein" name_original="veins" src="d0_s18" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s18" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o24917" name="apex" name_original="apices" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s18" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>paleas from 1/2 as long as to subequal to the lemmas, 2-veined;</text>
      <biological_entity id="o24918" name="palea" name_original="paleas" src="d0_s19" type="structure">
        <character modifier="from" name="quantity" src="d0_s19" value="1/2" value_original="1/2" />
        <character constraint="to lemmas" constraintid="o24919" is_modifier="false" name="size" src="d0_s19" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s19" value="2-veined" value_original="2-veined" />
      </biological_entity>
      <biological_entity id="o24919" name="lemma" name_original="lemmas" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>lodicules 2, membranous, ciliate;</text>
      <biological_entity id="o24920" name="lodicule" name_original="lodicules" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="2" value_original="2" />
        <character is_modifier="false" name="texture" src="d0_s20" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s20" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>anthers (1) 2 (3);</text>
      <biological_entity id="o24921" name="anther" name_original="anthers" src="d0_s21" type="structure">
        <character name="atypical_quantity" src="d0_s21" value="1" value_original="1" />
        <character name="quantity" src="d0_s21" value="2" value_original="2" />
        <character name="atypical_quantity" src="d0_s21" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>styles 2, bases free.</text>
      <biological_entity id="o24922" name="style" name_original="styles" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o24923" name="base" name_original="bases" src="d0_s22" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s22" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Caryopses obliquely ellipsoid, pericarp thick, easily peeled away at maturity, forming a conspicuous knob or beak, styles not persistent;</text>
      <biological_entity id="o24924" name="caryopse" name_original="caryopses" src="d0_s23" type="structure">
        <character is_modifier="false" modifier="obliquely" name="shape" src="d0_s23" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
      <biological_entity id="o24925" name="pericarp" name_original="pericarp" src="d0_s23" type="structure">
        <character is_modifier="false" name="width" src="d0_s23" value="thick" value_original="thick" />
        <character constraint="at maturity" is_modifier="false" modifier="easily" name="condition" src="d0_s23" value="peeled" value_original="peeled" />
      </biological_entity>
      <biological_entity id="o24926" name="knob" name_original="knob" src="d0_s23" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s23" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o24927" name="beak" name_original="beak" src="d0_s23" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s23" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o24928" name="style" name_original="styles" src="d0_s23" type="structure">
        <character is_modifier="false" modifier="not" name="duration" src="d0_s23" value="persistent" value_original="persistent" />
      </biological_entity>
      <relation from="o24925" id="r3940" name="forming a" negation="false" src="d0_s23" to="o24926" />
      <relation from="o24925" id="r3941" name="forming a" negation="false" src="d0_s23" to="o24927" />
    </statement>
    <statement id="d0_s24">
      <text>hila linear;</text>
      <biological_entity id="o24929" name="hilum" name_original="hila" src="d0_s24" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s24" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>embryos 1/4 - 1/3 as long as the fruits.</text>
      <biological_entity id="o24931" name="fruit" name_original="fruits" src="d0_s25" type="structure" />
    </statement>
    <statement id="d0_s26">
      <text>x = 10, 19.</text>
      <biological_entity id="o24930" name="embryo" name_original="embryos" src="d0_s25" type="structure">
        <character char_type="range_value" constraint="as-long-as fruits" constraintid="o24931" from="1/4" name="quantity" src="d0_s25" to="1/3" />
      </biological_entity>
      <biological_entity constraint="x" id="o24932" name="chromosome" name_original="" src="d0_s26" type="structure">
        <character name="quantity" src="d0_s26" value="10" value_original="10" />
        <character name="quantity" src="d0_s26" value="19" value_original="19" />
      </biological_entity>
    </statement>
  </description>
  <discussion>There are 1-2 genera in the Diarrheneae. The tribe is sometimes placed in the Bambusoideae, sometimes in the Pooideae. Its inclusion in the Bambusoideae is supported by embryo characteristics and not strongly opposed by others, but it lacks some of the diagnostic features of that subfamily, e.g., fusoid cells, leaf blades with evident cross venation, and a complex midvein. Its inclusion in the Pooideae reflects the findings of the Grass Phylogeny Working Group (2001).</discussion>
  <references>
    <reference>Grass Phylogeny Working Group. 2000. A phylogeny of the grass family (Poaceae), as inferred from eight character sets. Pp. 3-7 in S.W.L. Jacobs and J. Everett (eds.). Grasses: Systematics and Evolution. CSIRO Publishing, Collingwood, Victoria, Australia. 408 pp.</reference>
    <reference>Grass Phylogeny Working Group. 2001. Phylogeny and subfamilial classification of the grasses (Poaceae). Ann. Missouri Bot. Gard. 88:373-457</reference>
    <reference>Koyama, T. and S. Kawano. 1964. Critical taxa of grasses with North American and eastern Asiatic distribution. Canad. J. Bot. 42:859-864</reference>
    <reference>Tateoka, T. 1960. Cytology in grass systematics: A critical review. Nucleus (Calcutta) 3:81-110.</reference>
  </references>
  
</bio:treatment>