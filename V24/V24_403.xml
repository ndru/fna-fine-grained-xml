<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">283</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="Barkworth &amp; D.R. Dewey" date="unknown" rank="genus">×PSEUDELYMUS</taxon_name>
    <taxon_name authority="(Scribn. &amp; J.G. Sm.) Barkworth &amp; D.R. Dewey" date="unknown" rank="species">saxicola</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus ×pseudelymus;species saxicoia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">×Agrositanion</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">saxicola</taxon_name>
    <taxon_hierarchy>genus ×Agrositanion;species saxicola</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Elymus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">saxicola</taxon_name>
    <taxon_hierarchy>genus Elymus;species saxicola</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants not rhizomatous.</text>
      <biological_entity id="o6092" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 0.8-1 mm thick;</text>
      <biological_entity id="o6093" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="thickness" src="d0_s1" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>ligules 0.2-0.4 mm;</text>
      <biological_entity id="o6094" name="ligule" name_original="ligules" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s2" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 1.5-2 mm wide.</text>
      <biological_entity id="o6095" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikes 7-14 cm, with 1 spikelet per node;</text>
      <biological_entity id="o6096" name="spike" name_original="spikes" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s4" to="14" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6097" name="spikelet" name_original="spikelet" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o6098" name="node" name_original="node" src="d0_s4" type="structure" />
      <relation from="o6096" id="r978" name="with" negation="false" src="d0_s4" to="o6097" />
      <relation from="o6097" id="r979" name="per" negation="false" src="d0_s4" to="o6098" />
    </statement>
    <statement id="d0_s5">
      <text>internodes 7-9 mm.</text>
      <biological_entity id="o6099" name="internode" name_original="internodes" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spikelets 12-15 mm excluding the awns;</text>
      <biological_entity id="o6101" name="awn" name_original="awns" src="d0_s6" type="structure" />
      <relation from="o6100" id="r980" name="excluding the" negation="false" src="d0_s6" to="o6101" />
    </statement>
    <statement id="d0_s7">
      <text>disarticulation beneath the florets, sometimes also in the rachises.</text>
      <biological_entity id="o6100" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6102" name="floret" name_original="florets" src="d0_s7" type="structure" />
      <biological_entity id="o6103" name="rachis" name_original="rachises" src="d0_s7" type="structure" />
      <relation from="o6100" id="r981" name="beneath" negation="false" src="d0_s7" to="o6102" />
    </statement>
    <statement id="d0_s8">
      <text>Glumes 15-21 mm including the awns, glume bodies 6-8 mm long, 0.7-1 mm wide, (1) 3-4-veined;</text>
      <biological_entity id="o6104" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s8" to="21" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6105" name="awn" name_original="awns" src="d0_s8" type="structure" />
      <biological_entity constraint="glume" id="o6106" name="body" name_original="bodies" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s8" to="8" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s8" to="1" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="(1)3-4-veined" value_original="(1)3-4-veined" />
      </biological_entity>
      <relation from="o6104" id="r982" name="including the" negation="false" src="d0_s8" to="o6105" />
    </statement>
    <statement id="d0_s9">
      <text>lemmas 10-11 mm excluding the awns, (14) 18-37 mm including the awns, apices often bifid;</text>
      <biological_entity id="o6107" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="14" value_original="14" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s9" to="37" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6108" name="awn" name_original="awns" src="d0_s9" type="structure" />
      <biological_entity id="o6109" name="awn" name_original="awns" src="d0_s9" type="structure" />
      <biological_entity id="o6110" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s9" value="bifid" value_original="bifid" />
      </biological_entity>
      <relation from="o6107" id="r983" name="excluding the" negation="false" src="d0_s9" to="o6108" />
      <relation from="o6107" id="r984" name="including the" negation="false" src="d0_s9" to="o6109" />
    </statement>
    <statement id="d0_s10">
      <text>anthers about 2.5 mm.</text>
      <biological_entity id="o6111" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
    </statement>
  </description>
  <discussion>×Pseudelymus saxicoia consists of hybrids between Pseudoroegneria spicata and Elymus elymoides. It is a rather common hybrid in western North America. It differs from E. albicans, which is thought to be derived from hybrids between P. spicata and E. lanceolatus, in lacking rhizomes, having longer awns on its glumes and lemmas, and having disarticulating rachises. It is more likely to be confused with E. xsaundersii, but differs in its longer glume and lemma awns.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.;N.Mex.;Calif.;Oreg.;Wyo.;Mont.;Utah;Wash.;Ariz.;Idaho;Nev.;S.Dak.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>