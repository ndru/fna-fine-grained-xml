<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">621</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="Scribn." date="unknown" rank="genus">SPHENOPHOLIS</taxon_name>
    <taxon_name authority="(L.) Hitchc." date="unknown" rank="species">pensylvanica</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus sphenopholis;species pensylvanica</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Trisetum</taxon_name>
    <taxon_name authority="(L.) P. Beauv." date="unknown" rank="species">pensylvanicum</taxon_name>
    <taxon_hierarchy>genus trisetum;species pensylvanicum</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Swamp oats</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 30-120 cm, glabrous.</text>
      <biological_entity id="o31481" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Sheaths glabrous or pubescent;</text>
      <biological_entity id="o31482" name="sheath" name_original="sheaths" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>ligules 0.2-1 mm;</text>
      <biological_entity id="o31483" name="ligule" name_original="ligules" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 4-10 (20) cm long, (1) 2-8 mm wide, flat to slightly involute, smooth or scabridulous, sometimes pubescent.</text>
      <biological_entity id="o31484" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character name="length" src="d0_s3" unit="cm" value="20" value_original="20" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s3" to="10" to_unit="cm" />
        <character name="width" src="d0_s3" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="8" to_unit="mm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s3" to="slightly involute" />
        <character is_modifier="false" name="relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s3" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Panicles 7-35 cm long, 2-10 cm wide, erect to nodding, with relatively few, loosely arranged spikelets.</text>
      <biological_entity id="o31485" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s4" to="35" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s4" to="nodding" />
      </biological_entity>
      <biological_entity id="o31486" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="relatively" name="quantity" src="d0_s4" value="few" value_original="few" />
        <character is_modifier="true" modifier="loosely" name="arrangement" src="d0_s4" value="arranged" value_original="arranged" />
      </biological_entity>
      <relation from="o31485" id="r4942" name="with" negation="false" src="d0_s4" to="o31486" />
    </statement>
    <statement id="d0_s5">
      <text>Spikelets 4.5-9.5 mm.</text>
      <biological_entity id="o31487" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s5" to="9.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Lower glumes about 1/2 as wide as the upper glumes;</text>
      <biological_entity constraint="lower" id="o31488" name="glume" name_original="glumes" src="d0_s6" type="structure">
        <character constraint="as-wide-as upper glumes" constraintid="o31489" name="quantity" src="d0_s6" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity constraint="upper" id="o31489" name="glume" name_original="glumes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>upper glumes 3.6-6.2 mm, elliptical to oblanceolate, width/length ratio 0.15-0.28, apices acuminate to acute;</text>
      <biological_entity constraint="upper" id="o31490" name="glume" name_original="glumes" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.6" from_unit="mm" name="some_measurement" src="d0_s7" to="6.2" to_unit="mm" />
        <character char_type="range_value" from="elliptical" name="shape" src="d0_s7" to="oblanceolate" />
        <character is_modifier="false" name="width/length" src="d0_s7" value="ratio" value_original="ratio" />
        <character char_type="range_value" from="0.15" name="quantity" src="d0_s7" to="0.28" />
      </biological_entity>
      <biological_entity id="o31491" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s7" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lowest lemmas 3.5-6 mm, mostly smooth, apices scabridulous, unawned or awned, awns to 2.5 mm;</text>
      <biological_entity constraint="lowest" id="o31492" name="lemma" name_original="lemmas" src="d0_s8" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o31493" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="relief" src="d0_s8" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o31494" name="awn" name_original="awns" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>distal lemmas scabrous, awned, awns 3-9 mm, slightly to evidently bent;</text>
      <biological_entity constraint="distal" id="o31495" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o31496" name="awn" name_original="awns" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="slightly to evidently" name="shape" src="d0_s9" value="bent" value_original="bent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers 0.5-1.8 mm. 2n = 14.</text>
      <biological_entity id="o31497" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31498" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sphenopholis pensylvanica grows in springheads, seepage areas, swamps, marshes, and other moist to wet places, at 0-1100 m, in the eastern and southeastern United States. It hybridizes with S. obtusata (see below). The hybrids, which are called Sphenopholis xpallens (Biehler) Scribn., differ from S. pensylvanica in having generally shorter (3-5 mm) spikelets and shorter (0.1-4 mm), straight or bent awns on the distal lemmas. They differ from S. obtusata in having awns on the distal lemmas.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Del.;W.Va.;Fla.;N.J.;Conn.;Mass.;R.I.;La.;Tenn.;N.C.;S.C.;Pa.;N.Y.;Va.;Ala.;Ga.;Md.;Ohio;Mo.;Miss.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>