<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">523</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Sandy Long</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">POEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">POA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Poa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Poa</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">pratensis</taxon_name>
    <taxon_name authority="(B. Boivin &amp; D. Love) Roy L. Taylor &amp; MacBryde" date="unknown" rank="subspecies">agassizensis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus poa;subgenus poa;section poa;species pratensis;subspecies agassizensis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Poa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">agassizensis</taxon_name>
    <taxon_hierarchy>genus poa;species agassizensis</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants moderately to densely tufted, shoots clustered.</text>
      <biological_entity id="o25812" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal branching intra and extravaginal.</text>
      <biological_entity id="o25813" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" name="position" src="d0_s1" value="extravaginal" value_original="extravaginal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 20-40 (50) cm.</text>
      <biological_entity id="o25814" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="50" value_original="50" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="40" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Innovation shoot blades usually shorter than 15 cm, 0.8-2 mm wide, all involute or folded, adaxial surfaces sparsely pubescent.</text>
      <biological_entity constraint="shoot" id="o25815" name="blade" name_original="blades" src="d0_s3" type="structure" constraint_original="innovation shoot">
        <character modifier="usually shorter than" name="some_measurement" src="d0_s3" unit="cm" value="15" value_original="15" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s3" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="folded" value_original="folded" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o25816" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Panicles 4-8 cm, ovoid, narrowly pyramidal or loosely contracted, with 2-5 branches per node;</text>
      <biological_entity id="o25817" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s4" to="8" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="pyramidal" value_original="pyramidal" />
        <character is_modifier="false" modifier="loosely; loosely" name="condition_or_size" src="d0_s4" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o25818" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s4" to="5" />
      </biological_entity>
      <biological_entity id="o25819" name="node" name_original="node" src="d0_s4" type="structure" />
      <relation from="o25817" id="r4070" name="with" negation="false" src="d0_s4" to="o25818" />
      <relation from="o25818" id="r4071" name="per" negation="false" src="d0_s4" to="o25819" />
    </statement>
    <statement id="d0_s5">
      <text>branches ascending, smooth or sparsely to moderately densely scabrous, with several spikelets per branch.</text>
      <biological_entity id="o25820" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely to moderately densely" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o25821" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="several" value_original="several" />
      </biological_entity>
      <biological_entity id="o25822" name="branch" name_original="branch" src="d0_s5" type="structure" />
      <relation from="o25820" id="r4072" name="with" negation="false" src="d0_s5" to="o25821" />
      <relation from="o25821" id="r4073" name="per" negation="false" src="d0_s5" to="o25822" />
    </statement>
    <statement id="d0_s6">
      <text>Spikelets lanceolate, not bulbiferous;</text>
      <biological_entity id="o25823" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s6" value="bulbiferous" value_original="bulbiferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>florets normal.</text>
      <biological_entity id="o25824" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="false" name="variability" src="d0_s7" value="normal" value_original="normal" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Glume keels strongly compressed, sparsely to moderately scabrous;</text>
      <biological_entity constraint="glume" id="o25825" name="keel" name_original="keels" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s8" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>upper glumes shorter than to nearly equaling the lowest lemmas;</text>
      <biological_entity constraint="upper" id="o25826" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character constraint="than to nearly equaling the lowest lemmas" constraintid="o25827" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o25827" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="nearly" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lemmas 2-4 mm, finely muriculate, lateral-veins glabrous;</text>
      <biological_entity id="o25828" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s10" value="muriculate" value_original="muriculate" />
      </biological_entity>
      <biological_entity id="o25829" name="lateral-vein" name_original="lateral-veins" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>palea keels scabrous, glabrous, intercostal regions glabrous.</text>
      <biological_entity constraint="palea" id="o25830" name="keel" name_original="keels" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>2n = 56.</text>
      <biological_entity constraint="intercostal" id="o25831" name="region" name_original="regions" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25832" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Poa pratensis subsp. agassizensis was described as native to prairies and mountain grasslands of North America; this has not been confirmed. It grows throughout the drier, cool-temperate range of the species in North America. It may consist of ecotypes derived from cultivated material, or be a native form that has adapted to xeric conditions. The least distinctive of the subspecies treated here, it closely approaches subsp. angustifolia in having involute leaves and small spikelets, but has shorter and broader leaves, and more condensed panicles.</discussion>
  
</bio:treatment>