<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">370</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="Barkworth" date="unknown" rank="genus">×LEYDEUM</taxon_name>
    <taxon_name authority="(H.J. Hodgs. &amp; W.W. Mitch.) Barkworth" date="unknown" rank="species">littorale</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus ×leydeum;species littorale</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">xElymordeum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">littorale</taxon_name>
    <taxon_hierarchy>genus ×elymordeum;species littorale</taxon_hierarchy>
  </taxon_identification>
  <number>3</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants rhizomatous.</text>
      <biological_entity id="o23235" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms to 140 cm tall, about 3 mm thick.</text>
      <biological_entity id="o23236" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="height" src="d0_s1" to="140" to_unit="cm" />
        <character name="thickness" src="d0_s1" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves evenly distributed;</text>
      <biological_entity id="o23237" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="evenly" name="arrangement" src="d0_s2" value="distributed" value_original="distributed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheaths smooth, glabrous;</text>
      <biological_entity id="o23238" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>auricles 0.8-1 mm;</text>
      <biological_entity id="o23239" name="auricle" name_original="auricles" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.2-0.5 mm, truncate, entire;</text>
      <biological_entity id="o23240" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades to 20 cm long, 6-9 mm wide, glabrous, tapering from near the base, abaxial surfaces smooth, with 20+ veins, veins more or less equally prominent or the primary-veins slightly more prominent than the secondary-veins, adaxial surfaces scabridulous, apices narrowly acute.</text>
      <biological_entity id="o23241" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s6" to="20" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s6" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character constraint="from " constraintid="o23242" is_modifier="false" name="shape" src="d0_s6" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o23242" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity constraint="abaxial" id="o23243" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o23244" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s6" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o23245" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less equally; slightly" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
        <character constraint="than the secondary-veins" constraintid="o23246" is_modifier="false" name="prominence" src="d0_s6" value="more or less equally prominent or the primary-veins slightly more prominent" />
      </biological_entity>
      <biological_entity id="o23246" name="secondary-vein" name_original="secondary-veins" src="d0_s6" type="structure" />
      <biological_entity constraint="adaxial" id="o23248" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="relief" src="d0_s6" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o23249" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o23243" id="r3693" name="with" negation="false" src="d0_s6" to="o23244" />
    </statement>
    <statement id="d0_s7">
      <text>Spikes 6.5-15 cm long, 12-15 mm wide including the awns, 8-12 mm wide excluding the awns, nodes with 2-3 spikelets;</text>
      <biological_entity id="o23250" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character char_type="range_value" from="6.5" from_unit="cm" name="length" src="d0_s7" to="15" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s7" to="15" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23251" name="awn" name_original="awns" src="d0_s7" type="structure" />
      <biological_entity id="o23252" name="awn" name_original="awns" src="d0_s7" type="structure" />
      <biological_entity id="o23253" name="node" name_original="nodes" src="d0_s7" type="structure" />
      <biological_entity id="o23254" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o23250" id="r3694" name="including the" negation="false" src="d0_s7" to="o23251" />
      <relation from="o23250" id="r3695" name="excluding the" negation="false" src="d0_s7" to="o23252" />
      <relation from="o23253" id="r3696" name="with" negation="false" src="d0_s7" to="o23254" />
    </statement>
    <statement id="d0_s8">
      <text>internodes 3.5-5 mm, concealed by the spikelets;</text>
      <biological_entity id="o23256" name="spikelet" name_original="spikelets" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>disarticulation in the rachises, possibly also in the spikelets.</text>
      <biological_entity id="o23255" name="internode" name_original="internodes" src="d0_s8" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character constraint="by spikelets" constraintid="o23256" is_modifier="false" name="prominence" src="d0_s8" value="concealed" value_original="concealed" />
      </biological_entity>
      <biological_entity id="o23257" name="rachis" name_original="rachises" src="d0_s9" type="structure" />
      <biological_entity id="o23258" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
      <relation from="o23255" id="r3697" name="in" negation="false" src="d0_s9" to="o23257" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 12-15 mm, with 1-3 florets, the distal florets reduced.</text>
      <biological_entity id="o23259" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23260" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s10" to="3" />
      </biological_entity>
      <biological_entity constraint="distal" id="o23261" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="reduced" value_original="reduced" />
      </biological_entity>
      <relation from="o23259" id="r3698" name="with" negation="false" src="d0_s10" to="o23260" />
    </statement>
    <statement id="d0_s11">
      <text>Glumes equal, 11-13 mm long, 0.8-1.5 mm wide, 3-veined at midlength, hairy, hairs about 0.5 mm, somewhat divergent, tapering from below midlength, apices awned, awns 3-4 mm long;</text>
      <biological_entity id="o23262" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s11" to="13" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s11" to="1.5" to_unit="mm" />
        <character constraint="at midlength" constraintid="o23263" is_modifier="false" name="architecture" src="d0_s11" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o23263" name="midlength" name_original="midlength" src="d0_s11" type="structure" />
      <biological_entity id="o23264" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" modifier="somewhat" name="arrangement" src="d0_s11" value="divergent" value_original="divergent" />
        <character constraint="from midlength" constraintid="o23265" is_modifier="false" name="shape" src="d0_s11" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o23265" name="midlength" name_original="midlength" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="below midlength" name="position" src="d0_s11" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o23266" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o23267" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lemmas 13-15 mm, hairy, awns 1-3 mm;</text>
      <biological_entity id="o23268" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s12" to="15" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o23269" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers about 2.8 mm long, 0.1-0.3 mm thick.</text>
      <biological_entity id="o23270" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character name="length" src="d0_s13" unit="mm" value="2.8" value_original="2.8" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="thickness" src="d0_s13" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>×Leydeum littorale consists of hybrids between Hordeum brachyantherum and Leymus mollis. It has been collected in the Matanuska Valley, Alaska, and on the coast of Vancouver Island, British Columbia; it may be more widespread.</discussion>
  
</bio:treatment>