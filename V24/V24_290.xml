<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">209</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">BROMEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">BROMUS</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="section">Bromopsis</taxon_name>
    <taxon_name authority="(Scribn. ex Shear) Hitchc." date="unknown" rank="species">latiglumis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe bromeae;genus bromus;section bromopsis;species latiglumis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bromus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">purgans</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">latiglumis</taxon_name>
    <taxon_hierarchy>genus bromus;species purgans;variety latiglumis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bromus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">purgans</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="forma">incanus</taxon_name>
    <taxon_hierarchy>genus bromus;species purgans;forma incanus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bromus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">latiglumis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="forma">incanus</taxon_name>
    <taxon_hierarchy>genus bromus;species latiglumis;forma incanus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bromus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">altissimus</taxon_name>
    <taxon_hierarchy>genus bromus;species altissimus</taxon_hierarchy>
  </taxon_identification>
  <number>11</number>
  <other_name type="common_name">Hairy woodbrome</other_name>
  <other_name type="common_name">Flanged brome</other_name>
  <other_name type="common_name">Brome a larges glumes</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>not rhizomatous.</text>
      <biological_entity id="o25780" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 80-150 cm, erect;</text>
      <biological_entity id="o25781" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="80" from_unit="cm" name="some_measurement" src="d0_s2" to="150" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes 9-20, glabrous, usually concealed by the leaf-sheaths;</text>
      <biological_entity id="o25782" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s3" to="20" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character constraint="by leaf-sheaths" constraintid="o25783" is_modifier="false" modifier="usually" name="prominence" src="d0_s3" value="concealed" value_original="concealed" />
      </biological_entity>
      <biological_entity id="o25783" name="sheath" name_original="leaf-sheaths" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>internodes usually glabrous, sometimes hairy just below the nodes.</text>
      <biological_entity id="o25784" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character constraint="just below nodes" constraintid="o25785" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o25785" name="node" name_original="nodes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Sheaths overlapping, densely to moderately retrorsely pilose or glabrous over most of their surface, throats and collars densely pilose;</text>
      <biological_entity id="o25786" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" modifier="densely to moderately; moderately retrorsely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character constraint="of collars" constraintid="o25789" is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o25787" name="surface" name_original="surface" src="d0_s5" type="structure" />
      <biological_entity id="o25788" name="throat" name_original="throats" src="d0_s5" type="structure" />
      <biological_entity id="o25789" name="collar" name_original="collars" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>auricles 1-2.5 mm on most lower leaves;</text>
      <biological_entity id="o25790" name="auricle" name_original="auricles" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="on lower leaves" constraintid="o25791" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lower" id="o25791" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>ligules 0.8-1.4 mm, hirsute, ciliate, truncate, erose;</text>
      <biological_entity id="o25792" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s7" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s7" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blades 20-30 cm long, 5-15 mm wide, flat, usually glabrous, rarely pilose, with 2 prominent flanges at the collar.</text>
      <biological_entity id="o25793" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s8" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s8" to="15" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o25794" name="flange" name_original="flanges" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="true" name="prominence" src="d0_s8" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o25795" name="collar" name_original="collar" src="d0_s8" type="structure" />
      <relation from="o25793" id="r4067" name="with" negation="false" src="d0_s8" to="o25794" />
      <relation from="o25794" id="r4068" name="at" negation="false" src="d0_s8" to="o25795" />
    </statement>
    <statement id="d0_s9">
      <text>Panicles 10-22 cm, open, nodding;</text>
      <biological_entity id="o25796" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s9" to="22" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>branches spreading to ascending.</text>
      <biological_entity id="o25797" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s10" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 15-30 mm, elliptic to lanceolate, terete to moderately laterally compressed, with 4-9 florets.</text>
      <biological_entity id="o25798" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s11" to="30" to_unit="mm" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s11" to="lanceolate terete" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s11" to="lanceolate terete" />
      </biological_entity>
      <biological_entity id="o25799" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s11" to="9" />
      </biological_entity>
      <relation from="o25798" id="r4069" name="with" negation="false" src="d0_s11" to="o25799" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes pubescent or glabrous;</text>
      <biological_entity id="o25800" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 4-7.5 mm, 1 (3) -veined;</text>
      <biological_entity constraint="lower" id="o25801" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="7.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="1(3)-veined" value_original="1(3)-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 6-9 mm, 3-veined, sometimes mucronate;</text>
      <biological_entity constraint="upper" id="o25802" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="9" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s14" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 8-14 mm, elliptic to lanceolate, rounded over the midvein, backs glabrous or pilose to pubescent, margins long-pilose, apices obtuse to acute, entire;</text>
      <biological_entity id="o25803" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s15" to="14" to_unit="mm" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s15" to="lanceolate" />
        <character constraint="over midvein" constraintid="o25804" is_modifier="false" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o25804" name="midvein" name_original="midvein" src="d0_s15" type="structure" />
      <biological_entity id="o25805" name="back" name_original="backs" src="d0_s15" type="structure">
        <character char_type="range_value" from="pilose" name="pubescence" src="d0_s15" to="pubescent" />
      </biological_entity>
      <biological_entity id="o25806" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="long-pilose" value_original="long-pilose" />
      </biological_entity>
      <biological_entity id="o25807" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s15" to="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>awns 3-4.5 (7) mm, straight, arising less than 1.5 mm below the lemma apices;</text>
      <biological_entity id="o25808" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character name="atypical_some_measurement" src="d0_s16" unit="mm" value="7" value_original="7" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="arising" value_original="arising" />
        <character char_type="range_value" constraint="below lemma apices" constraintid="o25809" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o25809" name="apex" name_original="apices" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>anthers 2-3 mm. 2n = 14.</text>
      <biological_entity id="o25810" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s17" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25811" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bromus latiglumis grows in shaded or open woods, along stream banks, and on alluvial plains and slopes. Its range is mainly in the north-central and northeastern United States and adjacent Canadian provinces. Specimens with decumbent, weak, sprawling culms, densely hairy sheaths, and heavy panicles can be called Bromus latiglumis i. incanus (Shear) Fernald.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Del.;D.C.;Wis.;W.Va.;Kans.;N.Dak.;Nebr.;S.Dak.;N.H.;N.C.;Pa.;Alta.;B.C.;Man.;N.B.;Ont.;Que.;Sask.;Va.;Vt.;Ill.;Ind.;Iowa;Maine;Md.;Mass.;Ohio;Mo.;Minn.;Mich.;Mont.;Tenn.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>