<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 16:38:09</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">362</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="subfamily">POOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">TRITICEAE</taxon_name>
    <taxon_name authority="Hochst." date="unknown" rank="genus">LEYMUS</taxon_name>
    <taxon_name authority="(J. Presl) Á. Löve" date="unknown" rank="species">condensatus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus leymus;species condensatus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Elymus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">condensatus</taxon_name>
    <taxon_hierarchy>genus elymus;species condensatus</taxon_hierarchy>
  </taxon_identification>
  <number>10</number>
  <other_name type="common_name">Giant wildrye</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, weakly rhizomatous.</text>
      <biological_entity id="o8621" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 115-350 cm tall, 6-10 mm thick, usually several to many together.</text>
      <biological_entity id="o8622" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="115" from_unit="cm" name="height" src="d0_s1" to="350" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="mm" name="thickness" src="d0_s1" to="10" to_unit="mm" />
        <character char_type="range_value" from="usually several" modifier="together" name="quantity" src="d0_s1" to="many" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves exceeded by the inflorescences;</text>
      <biological_entity id="o8623" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o8624" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <relation from="o8623" id="r1394" name="exceeded by the" negation="false" src="d0_s2" to="o8624" />
    </statement>
    <statement id="d0_s3">
      <text>auricles absent;</text>
      <biological_entity id="o8625" name="auricle" name_original="auricles" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.7-6 mm on the basal leaves, 4-7.5 mm on the flag leaves;</text>
      <biological_entity id="o8626" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="on basal leaves" constraintid="o8627" from="0.7" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
        <character char_type="range_value" constraint="on flag, leaves" constraintid="o8628, o8629" from="4" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o8627" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o8628" name="flag" name_original="flag" src="d0_s4" type="structure" />
      <biological_entity id="o8629" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blades 10-28 mm wide, abaxial surfaces glabrous, smooth, adaxial surfaces scabridulous, veins numerous, subequal or unequal.</text>
      <biological_entity id="o8630" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s5" to="28" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8631" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o8632" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o8633" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s5" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="size" src="d0_s5" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="size" src="d0_s5" value="unequal" value_original="unequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences panicles, 17-44 cm long, 20-60 mm wide, lower nodes with 2-6 branches, branches to 8 cm, ascending, with 5-35 spikelets, upper nodes with pedicellate and sessile spikelets;</text>
      <biological_entity constraint="inflorescences" id="o8634" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="17" from_unit="cm" name="length" src="d0_s6" to="44" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s6" to="60" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lower" id="o8635" name="node" name_original="nodes" src="d0_s6" type="structure" />
      <biological_entity id="o8636" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="6" />
      </biological_entity>
      <biological_entity id="o8637" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s6" to="8" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o8638" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s6" to="35" />
      </biological_entity>
      <biological_entity constraint="upper" id="o8639" name="node" name_original="nodes" src="d0_s6" type="structure" />
      <biological_entity id="o8640" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <relation from="o8635" id="r1395" name="with" negation="false" src="d0_s6" to="o8636" />
      <relation from="o8637" id="r1396" name="with" negation="false" src="d0_s6" to="o8638" />
      <relation from="o8639" id="r1397" name="with" negation="false" src="d0_s6" to="o8640" />
    </statement>
    <statement id="d0_s7">
      <text>internodes 3.5-10 mm, glabrous.</text>
      <biological_entity id="o8641" name="internode" name_original="internodes" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 9-25 mm, usually pedicellate, pedicels 0.8-2 mm, with 3-7 florets.</text>
      <biological_entity id="o8642" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s8" to="25" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s8" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity id="o8643" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8644" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="7" />
      </biological_entity>
      <relation from="o8643" id="r1398" name="with" negation="false" src="d0_s8" to="o8644" />
    </statement>
    <statement id="d0_s9">
      <text>Glumes 6-16 mm long, 0.5-2.5 mm wide, narrowly lanceolate, stiff, keeled, the central portion thicker than the margins, glabrous, smooth proximally, scabrous distally, 0-1 (3) -veined, veins inconspicuous at midlength, apices tapering almost imperceptibly into an awn, awns subequal to the glume body;</text>
      <biological_entity id="o8645" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s9" to="16" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s9" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="fragility" src="d0_s9" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="shape" src="d0_s9" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity constraint="central" id="o8646" name="portion" name_original="portion" src="d0_s9" type="structure">
        <character constraint="than the margins" constraintid="o8647" is_modifier="false" name="width" src="d0_s9" value="thicker" value_original="thicker" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="distally" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="0-1(3)-veined" value_original="0-1(3)-veined" />
      </biological_entity>
      <biological_entity id="o8647" name="margin" name_original="margins" src="d0_s9" type="structure" />
      <biological_entity id="o8648" name="vein" name_original="veins" src="d0_s9" type="structure">
        <character constraint="at midlength" constraintid="o8649" is_modifier="false" name="prominence" src="d0_s9" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o8649" name="midlength" name_original="midlength" src="d0_s9" type="structure" />
      <biological_entity id="o8650" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character constraint="into awn" constraintid="o8651" is_modifier="false" modifier="almost imperceptibly" name="shape" src="d0_s9" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o8651" name="awn" name_original="awn" src="d0_s9" type="structure" />
      <biological_entity id="o8652" name="awn" name_original="awns" src="d0_s9" type="structure">
        <character constraint="to glume body" constraintid="o8653" is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity constraint="glume" id="o8653" name="body" name_original="body" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>lemmas 7-14 mm, usually glabrous, apices acute, sometimes awned, awns to 4 mm;</text>
      <biological_entity id="o8654" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="14" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o8655" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s10" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o8656" name="awn" name_original="awns" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers 3.5-7 mm, dehiscent.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 28, 56.</text>
      <biological_entity id="o8657" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8658" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="28" value_original="28" />
        <character name="quantity" src="d0_s12" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Leymus condensatus is found primarily on dry slopes and in open woodlands of the coastal mountains and offshore islands of California, at elevations of 0-1500 m. Both its large size and paniculate inflorescence tend to make it a distinctive species in the Triticeae. Hybrids between L. condensatus and L. triticoides, known as Leymus xmultiflorus, are relatively common where the parents are sympatric.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta.;B.C.;Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>