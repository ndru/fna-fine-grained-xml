<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">83</other_info_on_meta>
    <other_info_on_meta type="treatment_page">84</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Martinov" date="unknown" rank="family">aizoaceae</taxon_name>
    <taxon_name authority="N. E. Brown" date="1925" rank="genus">aptenia</taxon_name>
    <taxon_name authority="(Linnaeus f.) Schwantes" date="1928" rank="species">cordifolia</taxon_name>
    <place_of_publication>
      <publication_title>Gartenflora,</publication_title>
      <place_in_publication>57: 69. 1928</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family aizoaceae;genus aptenia;species cordifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220000996</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mesembryanthemum</taxon_name>
    <taxon_name authority="Linnaeus f." date="unknown" rank="species">cordifolium</taxon_name>
    <place_of_publication>
      <publication_title>Suppl. Pl.,</publication_title>
      <place_in_publication>260. 1782</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Mesembryanthemum;species cordifolium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants short-lived.</text>
      <biological_entity id="o13487" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 3–6 dm;</text>
      <biological_entity id="o13488" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s1" to="6" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes 1–5 cm;</text>
    </statement>
    <statement id="d0_s3">
      <text>often rooting at nodes.</text>
      <biological_entity id="o13489" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="5" to_unit="cm" />
        <character constraint="at nodes" constraintid="o13490" is_modifier="false" modifier="often" name="architecture" src="d0_s3" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o13490" name="node" name_original="nodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blades 1–3 × 0.5–5 cm, minutely papillate, appearing smooth.</text>
      <biological_entity id="o13491" name="leaf-blade" name_original="leaf-blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s4" value="papillate" value_original="papillate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences solitary flowers;</text>
      <biological_entity id="o13492" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o13493" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>axillary pedicel 8–15 mm.</text>
      <biological_entity constraint="axillary" id="o13494" name="pedicel" name_original="pedicel" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: hypanthium 6–7 mm;</text>
      <biological_entity id="o13495" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o13496" name="hypanthium" name_original="hypanthium" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>calyx lobes 2–5 mm;</text>
      <biological_entity id="o13497" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="calyx" id="o13498" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 80, reflexed, pink to purple (red in hybrid cultivar “Red Apple”), 3–4 mm;</text>
      <biological_entity id="o13499" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o13500" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="80" value_original="80" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s9" to="purple" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminodia 50, erect, white, 3 mm.</text>
      <biological_entity id="o13501" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o13502" name="staminodium" name_original="staminodia" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="50" value_original="50" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules 13–15 mm. 2n = 18.</text>
      <biological_entity id="o13503" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13504" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed places, coastal bluffs, margins of coastal wetlands, 0-100 m</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed places" />
        <character name="habitat" value="coastal bluffs" />
        <character name="habitat" value="margins" constraint="of coastal wetlands , 0-100 m" />
        <character name="habitat" value="coastal wetlands" />
        <character name="habitat" value="0-100 m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Oreg.; Europe (British Isles); Africa; Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" value="Europe (British Isles)" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Baby sun-rose</other_name>
  <discussion>The flowers of Aptenia cordifolia open during the bright hours of the day.</discussion>
  
</bio:treatment>