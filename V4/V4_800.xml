<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Kenneth R. Robertson</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">406</other_info_on_meta>
    <other_info_on_meta type="treatment_page">407</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">amaranthaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CELOSIA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 205. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 96. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amaranthaceae;genus CELOSIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek keleos, burning, alluding to color and/or appearance of the inflorescence of C. cristata</other_info_on_name>
    <other_info_on_name type="fna_id">105991</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs or subshrubs, annual or perennial.</text>
      <biological_entity id="o6379" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or clambering-straggling.</text>
      <biological_entity id="o6381" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="clambering-straggling" value_original="clambering-straggling" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, petiolate;</text>
      <biological_entity id="o6382" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade mostly lanceolate, ovate, or deltate.</text>
      <biological_entity id="o6383" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal and often axillary spikes or panicles, often fasciated in cultivated forms, many-flowered.</text>
      <biological_entity id="o6384" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character constraint="in cultivated forms" is_modifier="false" modifier="often" name="fusion" notes="" src="d0_s4" value="fasciated" value_original="fasciated" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="many-flowered" value_original="many-flowered" />
      </biological_entity>
      <biological_entity id="o6385" name="spike" name_original="spikes" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="often" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o6386" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="often" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers bisexual;</text>
      <biological_entity id="o6387" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s5" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>tepals 3–5, distinct, membranous or scarious, usually glabrous;</text>
      <biological_entity id="o6388" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="5" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments connate basally into cups;</text>
      <biological_entity id="o6389" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character constraint="into cups" constraintid="o6390" is_modifier="false" name="fusion" src="d0_s7" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o6390" name="cup" name_original="cups" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>anthers 4-locular;</text>
      <biological_entity id="o6391" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s8" value="4-locular" value_original="4-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pseudostaminodes absent;</text>
      <biological_entity id="o6392" name="pseudostaminode" name_original="pseudostaminodes" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovules 3–many;</text>
      <biological_entity id="o6393" name="ovule" name_original="ovules" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" is_modifier="false" name="quantity" src="d0_s10" to="many" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style persistent, 0.2–4 mm;</text>
      <biological_entity id="o6394" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas 2–3, capitate or subulate.</text>
      <biological_entity id="o6395" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s12" to="3" />
        <character is_modifier="false" name="shape" src="d0_s12" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subulate" value_original="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Utricles ovoid, membranaceous, dehiscence centrally circumscissile.</text>
      <biological_entity id="o6396" name="utricle" name_original="utricles" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="texture" src="d0_s13" value="membranaceous" value_original="membranaceous" />
        <character is_modifier="false" modifier="centrally" name="dehiscence" src="d0_s13" value="circumscissile" value_original="circumscissile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds (2–) 3–many, black, flattened.</text>
    </statement>
    <statement id="d0_s15">
      <text>x = 9.</text>
      <biological_entity id="o6397" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s14" value="(2-)3-many" value_original="(2-)3-many" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity constraint="x" id="o6398" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Primarily tropical, subtropical Americas, Asia, and Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Primarily tropical" establishment_means="native" />
        <character name="distribution" value="subtropical Americas" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="and Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Cockscomb</other_name>
  <discussion>Species ca. 65 (5 in the flora).</discussion>
  <references>
    <reference>Grant, W. F. 1961. Speciation and basic chromosome number in the genus Celosia. Canad. J. Bot. 39: 45–50.</reference>
    <reference>Grant, W. F. 1962. Speciation and nomenclature in the genus Celosia. Canad. J. Bot. 40: 1355–1363.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Styles 3-4 mm; inflorescences dense, cylindric, 13-20 mm diam. spikes, or fasciated, crested or plumose</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Styles 0.2-1 mm; inflorescences lax spikes or panicles, units less than 10 mm diam</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences cylindric or ovoid spikes; tepals silvery white or pinkish</description>
      <determination>4 Celosia argentea</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences fasciated, crested, or plumose; tepals pink, red, yellow, purple, or white</description>
      <determination>5 Celosia cristata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants annual herbs; tepals silvery, whitish, or tannish, 1-veined, 2-3 mm</description>
      <determination>3 Celosia trigyna</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants perennial herbs, subshrubs, or shrubs; tepals green, greenish white, pinkish green, or white with reddish base, drying tan, striate, 3.5-6(-7) mm</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades deltate to triangular-lanceolate, not lobed; stigmas 3.</description>
      <determination>1 Celosia nitida</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades moderately hastate, lanceolate to ovate-lanceolate; stigmas 2.</description>
      <determination>2 Celosia palmeri</determination>
    </key_statement>
  </key>
</bio:treatment>