<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">477</other_info_on_meta>
    <other_info_on_meta type="treatment_page">482</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Adanson" date="unknown" rank="family">portulacaceae</taxon_name>
    <taxon_name authority="Pursh" date="1814" rank="genus">lewisia</taxon_name>
    <taxon_name authority="A. H. Holmgren" date="1954" rank="species">maguirei</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>7: 136. 1954</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family portulacaceae;genus lewisia;species maguirei</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242415775</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Taproots gradually ramified distally.</text>
      <biological_entity id="o16259" name="taproot" name_original="taproots" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="gradually; distally" name="architecture" src="d0_s0" value="ramified" value_original="ramified" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading to suberect, 1.5–2 cm.</text>
      <biological_entity id="o16260" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s1" to="suberect" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s1" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal leaves withering at or soon after anthesis, sessile, blade narrowly oblanceolate, somewhat flattened, 1–2 cm, margins entire, apex obtuse;</text>
      <biological_entity id="o16261" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o16262" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="after blade" constraintid="o16263" is_modifier="false" name="life_cycle" src="d0_s2" value="withering" value_original="withering" />
        <character is_modifier="false" modifier="somewhat" name="shape" notes="" src="d0_s2" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16263" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="anthesis" value_original="anthesis" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o16264" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o16265" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline leaves absent.</text>
      <biological_entity id="o16266" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o16267" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences racemose cymes, 2–3-flowered, sometimes with flowers borne singly;</text>
      <biological_entity id="o16268" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s4" value="2-3-flowered" value_original="2-3-flowered" />
      </biological_entity>
      <biological_entity id="o16269" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="racemose" value_original="racemose" />
      </biological_entity>
      <biological_entity id="o16270" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
      </biological_entity>
      <relation from="o16268" id="r2361" modifier="sometimes" name="with" negation="false" src="d0_s4" to="o16270" />
    </statement>
    <statement id="d0_s5">
      <text>bracts 5, proximalmost 3 in whorl, distalmost 2 opposite, subtending 2d (and 3d) flowers, oblong to oblong-obovate, 3–5 mm, apex obtuse.</text>
      <biological_entity id="o16271" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="proximalmost" id="o16272" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character constraint="in whorl" constraintid="o16273" name="quantity" src="d0_s5" value="3" value_original="3" />
        <character is_modifier="false" name="position" notes="" src="d0_s5" value="distalmost" value_original="distalmost" />
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="position" src="d0_s5" value="subtending" value_original="subtending" />
        <character char_type="range_value" from="oblong" name="shape" notes="" src="d0_s5" to="oblong-obovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16273" name="whorl" name_original="whorl" src="d0_s5" type="structure" />
      <biological_entity id="o16274" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o16275" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers pedicellate, disarticulate in fruit;</text>
      <biological_entity id="o16276" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity id="o16277" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
      <relation from="o16276" id="r2362" name="disarticulate in" negation="false" src="d0_s6" to="o16277" />
    </statement>
    <statement id="d0_s7">
      <text>sepals 3–4, white to pinkish, 8–12 mm, scarious at anthesis, margins entire, apex obtuse;</text>
      <biological_entity id="o16278" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="4" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="pinkish" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
        <character constraint="at anthesis" is_modifier="false" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o16279" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o16280" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals 7–9, white to pinkish, oblanceloate, 8–12 mm;</text>
      <biological_entity id="o16281" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s8" to="9" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s8" to="pinkish" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblanceloate" value_original="oblanceloate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 7–9;</text>
      <biological_entity id="o16282" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s9" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmas 4–6;</text>
      <biological_entity id="o16283" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s10" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pedicel 3–9 mm.</text>
      <biological_entity id="o16284" name="pedicel" name_original="pedicel" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 7–10 mm.</text>
      <biological_entity id="o16285" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 5–10, 1.5–2.5 mm, smooth.</text>
      <biological_entity id="o16286" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s13" to="10" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, south-facing slopes on gravelly clay limestone-derived substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" constraint="on gravelly clay" />
        <character name="habitat" value="south-facing slopes" constraint="on gravelly clay" />
        <character name="habitat" value="gravelly clay" />
        <character name="habitat" value="limestone-derived substrates" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="2200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Lewisia maguirei is known only from the Quinn Canyon Range, Nye County.</discussion>
  
</bio:treatment>