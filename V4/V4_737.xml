<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">372</other_info_on_meta>
    <other_info_on_meta type="mention_page">379</other_info_on_meta>
    <other_info_on_meta type="treatment_page">374</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">atriplex</taxon_name>
    <taxon_name authority="(Torrey &amp; Frémont) S. L. Welsh" date="2001" rank="subgenus">Pterochiton</taxon_name>
    <taxon_name authority="(Moquin-Tandon) D. Dietrich" date="1852" rank="species">gardneri</taxon_name>
    <taxon_name authority="(C. A. Hanson) S. L. Welsh" date="1984" rank="variety">bonnevillensis</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>44: 190. 1984</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus atriplex;subgenus pterochiton;species gardneri;variety bonnevillensis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242415588</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atriplex</taxon_name>
    <taxon_name authority="C. A. Hanson" date="unknown" rank="species">bonnevillensis</taxon_name>
    <place_of_publication>
      <publication_title>Stud. Syst. Bot.</publication_title>
      <place_in_publication>1: 2. 1962</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Atriplex;species bonnevillensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs or shrubs, dioecious or sparingly monoecious, mainly 2–8 dm, as wide or wider.</text>
      <biological_entity id="o12260" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" modifier="sparingly" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character char_type="range_value" from="2" from_unit="dm" modifier="mainly" name="some_measurement" src="d0_s0" to="8" to_unit="dm" />
        <character is_modifier="false" name="width" src="d0_s0" value="wide" value_original="wide" />
        <character is_modifier="false" name="width" src="d0_s0" value="wider" value_original="wider" />
        <character name="growth_form" value="subshrub" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" modifier="sparingly" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character char_type="range_value" from="2" from_unit="dm" modifier="mainly" name="some_measurement" src="d0_s0" to="8" to_unit="dm" />
        <character is_modifier="false" name="width" src="d0_s0" value="wide" value_original="wide" />
        <character is_modifier="false" name="width" src="d0_s0" value="wider" value_original="wider" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending.</text>
      <biological_entity id="o12262" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves tardily deciduous, alternate, sessile or subsessile;</text>
      <biological_entity id="o12263" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="tardily" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade linear to oblanceolate or obovate-spatulate, 7–40 × 2–9 mm, 2–9 times longer than wide.</text>
      <biological_entity id="o12264" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="oblanceolate or obovate-spatulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s3" to="40" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="9" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="2-9" value_original="2-9" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Staminate flowers tan to brown or rarely yellow, in glomerules 2–3 mm thick, in terminal or axillary spikes or spicate panicles 1–9 cm.</text>
      <biological_entity id="o12265" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="tan" name="coloration" src="d0_s4" to="brown or rarely yellow" />
      </biological_entity>
      <biological_entity id="o12266" name="glomerule" name_original="glomerules" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="thickness" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12267" name="spike" name_original="spikes" src="d0_s4" type="structure" />
      <biological_entity id="o12268" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="spicate" value_original="spicate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="9" to_unit="cm" />
      </biological_entity>
      <relation from="o12265" id="r1769" name="in" negation="false" src="d0_s4" to="o12266" />
      <relation from="o12265" id="r1770" name="in" negation="false" src="d0_s4" to="o12267" />
      <relation from="o12265" id="r1771" name="in" negation="false" src="d0_s4" to="o12268" />
    </statement>
    <statement id="d0_s5">
      <text>Pistillate flowers in spicate panicles 8–25 cm.</text>
      <biological_entity id="o12270" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="spicate" value_original="spicate" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s5" to="25" to_unit="cm" />
      </biological_entity>
      <relation from="o12269" id="r1772" name="in" negation="false" src="d0_s5" to="o12270" />
    </statement>
    <statement id="d0_s6">
      <text>Fruiting bracteoles sessile or on stipes to 4 mm, body basically ovoid and bearing 4 lateral wings or rows of flattened tubercles, 5–8 × 3–9 mm, apex acuminate, 1–4 mm, tubercles lanceolate, sometimes vestigial, rarely absent, to 3 mm high.</text>
      <biological_entity id="o12269" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o12271" name="bracteole" name_original="bracteoles" src="d0_s6" type="structure" />
      <biological_entity id="o12272" name="stipe" name_original="stipes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12273" name="body" name_original="body" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="basically" name="shape" src="d0_s6" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o12274" name="wing" name_original="wings" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="4" value_original="4" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o12275" name="row" name_original="rows" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o12276" name="tubercle" name_original="tubercles" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity id="o12277" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12278" name="tubercle" name_original="tubercles" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s6" value="vestigial" value_original="vestigial" />
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character char_type="range_value" from="0" from_unit="mm" name="height" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o12269" id="r1773" name="fruiting" negation="false" src="d0_s6" to="o12271" />
      <relation from="o12273" id="r1774" name="bearing" negation="false" src="d0_s6" to="o12274" />
      <relation from="o12273" id="r1775" name="bearing" negation="false" src="d0_s6" to="o12275" />
      <relation from="o12273" id="r1776" name="part_of" negation="false" src="d0_s6" to="o12276" />
    </statement>
    <statement id="d0_s7">
      <text>Seeds 1.5 mm wide.</text>
      <biological_entity id="o12279" name="seed" name_original="seeds" src="d0_s7" type="structure">
        <character name="width" src="d0_s7" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Greasewood and saltbush communities in valley bottoms and playas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="greasewood" constraint="in valley bottoms and playas" />
        <character name="habitat" value="saltbush communities" constraint="in valley bottoms and playas" />
        <character name="habitat" value="valley bottoms" />
        <character name="habitat" value="playas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>51f.</number>
  <other_name type="common_name">Bonneville saltbush</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>The Bonneville saltbush is apparently a partially stabilized introgressant involving Atriplex gardneri var. falcata and A. canescens. The habitat is intermediate between that occupied by the parental taxa. There is evidence that the introgression is continuing in some populations at least. The plants are herein placed within A. gardneri, with which they are most similar morphologically.</discussion>
  
</bio:treatment>