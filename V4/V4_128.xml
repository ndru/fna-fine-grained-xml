<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">62</other_info_on_meta>
    <other_info_on_meta type="mention_page">64</other_info_on_meta>
    <other_info_on_meta type="treatment_page">65</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Jussieu" date="1789" rank="genus">abronia</taxon_name>
    <taxon_name authority="Greene" date="1898" rank="species">angustifolia</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 344. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus abronia;species angustifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415089</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Abronia</taxon_name>
    <taxon_name authority="Standley" date="unknown" rank="species">angustifolia</taxon_name>
    <taxon_name authority="(Stan dley) Kearney &amp; Peebles" date="unknown" rank="variety">arizonica</taxon_name>
    <taxon_hierarchy>genus Abronia;species angustifolia;variety arizonica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Abronia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">torreyi</taxon_name>
    <taxon_hierarchy>genus Abronia;species torreyi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual (perennial).</text>
      <biological_entity id="o5461" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to ascending, much branched, elongate, often reddish, glandular-pubescent, viscid.</text>
      <biological_entity id="o5462" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" name="coating" src="d0_s1" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 1–7 cm;</text>
      <biological_entity id="o5463" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o5464" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade ovate-oblong to elliptic, 1–5.5 × 0.7–3 cm, margins entire to sinuate, often ± undulate, infrequently shallowly lobed, surfaces viscid-pubescent.</text>
      <biological_entity id="o5465" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o5466" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate-oblong" name="shape" src="d0_s3" to="elliptic" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5467" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s3" to="sinuate" />
        <character is_modifier="false" modifier="often more or less" name="shape" src="d0_s3" value="undulate" value_original="undulate" />
        <character is_modifier="false" modifier="infrequently shallowly" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o5468" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="viscid-pubescent" value_original="viscid-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: peduncle longer than subtending petiole;</text>
      <biological_entity id="o5469" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o5470" name="peduncle" name_original="peduncle" src="d0_s4" type="structure">
        <character constraint="than subtending petiole" constraintid="o5471" is_modifier="false" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o5471" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>bracts lanceolate to oblong-lanceolate, 5–10 × 1–3 mm, papery, glandular-pubescent;</text>
      <biological_entity id="o5472" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o5473" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="oblong-lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="10" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="papery" value_original="papery" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>flowers 10–30.</text>
      <biological_entity id="o5474" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o5475" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s6" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Perianth: tube pink, 10–20 mm, limb bright magenta to pale-pink, infrequently pale rose, 6–8 mm diam.</text>
      <biological_entity id="o5476" name="perianth" name_original="perianth" src="d0_s7" type="structure" />
      <biological_entity id="o5477" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pink" value_original="pink" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5478" name="limb" name_original="limb" src="d0_s7" type="structure">
        <character char_type="range_value" from="bright magenta" name="coloration" src="d0_s7" to="pale-pink" />
        <character is_modifier="false" modifier="infrequently" name="coloration" src="d0_s7" value="pale rose" value_original="pale rose" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fruits broadly obdeltate in profile, 5–10 × 4–8 mm, scarious, apex narrowly tapered to a prominent beak;</text>
      <biological_entity id="o5479" name="fruit" name_original="fruits" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="broadly; in profile" name="shape" src="d0_s8" value="obdeltate" value_original="obdeltate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s8" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s8" to="8" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o5480" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character constraint="to beak" constraintid="o5481" is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o5481" name="beak" name_original="beak" src="d0_s8" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s8" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>wings 5, extending to or slightly beyond base of beak, truncate, with conspicuous dilations, cavities extending throughout.</text>
      <biological_entity id="o5482" name="wing" name_original="wings" src="d0_s9" type="structure" constraint="beak" constraint_original="beak; beak">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o5483" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o5484" name="beak" name_original="beak" src="d0_s9" type="structure" />
      <biological_entity id="o5485" name="dilation" name_original="dilations" src="d0_s9" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s9" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o5486" name="cavity" name_original="cavities" src="d0_s9" type="structure" />
      <relation from="o5482" id="r756" name="extending to" negation="false" src="d0_s9" to="o5483" />
      <relation from="o5482" id="r757" name="part_of" negation="false" src="d0_s9" to="o5484" />
      <relation from="o5482" id="r758" name="with" negation="false" src="d0_s9" to="o5485" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils, desert scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="desert scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; n Mexico (Chihuahua, Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="n Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="n Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <discussion>Plants on gypsum flats and knolls of White Sands, New Mexico, are perennial, but may flower in their first season.</discussion>
  
</bio:treatment>