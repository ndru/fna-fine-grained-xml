<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">392</other_info_on_meta>
    <other_info_on_meta type="treatment_page">395</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Forsskål ex J. F. Gmelin" date="unknown" rank="genus">suaeda</taxon_name>
    <taxon_name authority="(Moquin-Tandon) Volkens in H. G. A. Engler and K. Prantl" date="1893" rank="section">Brezia</taxon_name>
    <taxon_name authority="Bassett &amp; Crompton" date="1978" rank="species">rolandii</taxon_name>
    <place_of_publication>
      <publication_title>Canad. J. Bot.</publication_title>
      <place_in_publication>56: 588, figs. 5, 9, 13. 1978</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus suaeda;section brezia;species rolandii;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415629</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, procumbent to erect, dark green, 2–7 dm.</text>
      <biological_entity id="o13674" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="dark green" value_original="dark green" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="7" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending or erect, light brownish green, 1–several-branched mainly in distal part of plant, becoming woody at base;</text>
      <biological_entity id="o13675" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="light brownish" value_original="light brownish" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character constraint="in distal part" constraintid="o13676" is_modifier="false" name="architecture" src="d0_s1" value="1-several-branched" value_original="1-several-branched" />
        <character constraint="at base" constraintid="o13678" is_modifier="false" modifier="becoming" name="texture" notes="" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity constraint="distal" id="o13676" name="part" name_original="part" src="d0_s1" type="structure" />
      <biological_entity id="o13677" name="plant" name_original="plant" src="d0_s1" type="structure" />
      <biological_entity id="o13678" name="base" name_original="base" src="d0_s1" type="structure" />
      <relation from="o13676" id="r1974" name="part_of" negation="false" src="d0_s1" to="o13677" />
    </statement>
    <statement id="d0_s2">
      <text>branches ascending or erect.</text>
      <biological_entity id="o13679" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves ascending;</text>
      <biological_entity id="o13680" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear, 17–30 × 0.7–1.5 mm, widest proximal to middle, apex acute.</text>
      <biological_entity id="o13681" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="17" from_unit="mm" name="length" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s4" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s4" value="widest" value_original="widest" />
        <character char_type="range_value" from="proximal" name="position" src="d0_s4" to="middle" />
      </biological_entity>
      <biological_entity id="o13682" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Glomes on main-stem and lateral branches, not crowded into compound spikes, 1–3-flowered;</text>
      <biological_entity id="o13683" name="glome" name_original="glomes" src="d0_s5" type="structure">
        <character constraint="into spikes" constraintid="o13686" is_modifier="false" modifier="not" name="arrangement" notes="" src="d0_s5" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="1-3-flowered" value_original="1-3-flowered" />
      </biological_entity>
      <biological_entity id="o13684" name="main-stem" name_original="main-stem" src="d0_s5" type="structure" />
      <biological_entity constraint="lateral" id="o13685" name="branch" name_original="branches" src="d0_s5" type="structure" />
      <biological_entity id="o13686" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="compound" value_original="compound" />
      </biological_entity>
      <relation from="o13683" id="r1975" name="on" negation="false" src="d0_s5" to="o13684" />
      <relation from="o13683" id="r1976" name="on" negation="false" src="d0_s5" to="o13685" />
    </statement>
    <statement id="d0_s6">
      <text>bracts shorter than leaves, 4–12 mm.</text>
      <biological_entity id="o13687" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character constraint="than leaves" constraintid="o13688" is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13688" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers bisexual;</text>
      <biological_entity id="o13689" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth ± zygomorphic or irregular (segments slightly unequal), 2–4 mm diam.;</text>
      <biological_entity id="o13690" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s8" value="zygomorphic" value_original="zygomorphic" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="irregular" value_original="irregular" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perianth segments abaxially rounded and keeled, distally hooded, apex acute;</text>
      <biological_entity constraint="perianth" id="o13691" name="segment" name_original="segments" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s9" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s9" value="hooded" value_original="hooded" />
      </biological_entity>
      <biological_entity id="o13692" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmas 2–3.</text>
      <biological_entity id="o13693" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s10" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds dimorphic;</text>
    </statement>
    <statement id="d0_s12">
      <text>lenticular, 1.5–2.3 mm diam., with seed-coat black to reddish, finely reticulate;</text>
      <biological_entity id="o13695" name="seed-coat" name_original="seed-coat" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s12" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="black" name="coloration" src="d0_s12" to="reddish" />
        <character is_modifier="false" modifier="finely" name="architecture_or_coloration_or_relief" src="d0_s12" value="reticulate" value_original="reticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>or flat, 1.5–2.3 mm diam., with seed-coat brown, dull.</text>
      <biological_entity id="o13696" name="seed-coat" name_original="seed-coat" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s13" to="2.3" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character is_modifier="false" name="reflectance" src="d0_s13" value="dull" value_original="dull" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = 90.</text>
      <biological_entity id="o13694" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s11" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lenticular" value_original="lenticular" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s12" to="2.3" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s13" value="flat" value_original="flat" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s13" to="2.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13697" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="90" value_original="90" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tidal flats, salt marshes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tidal flats" />
        <character name="habitat" value="salt marshes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1-10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="1" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., N.S., Que.; N.J., N.Y.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Roland’s sea-blite</other_name>
  <discussion>Besides being confirmed in the range given above, this rare species is said to occur along the coast from New Hampshire to Canada (S. E. Clemants 1992).</discussion>
  <discussion>of conservation concern</discussion>
  
</bio:treatment>