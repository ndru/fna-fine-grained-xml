<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="treatment_page">415</other_info_on_meta>
    <other_info_on_meta type="mention_page">419</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">amaranthaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">amaranthus</taxon_name>
    <taxon_name authority="(Linnaeus) Aellen ex K. R. Robertson" date="1981" rank="subgenus">acnida</taxon_name>
    <taxon_name authority="(Linnaeus) Mosyakin &amp; K. R. Robertson" date="1996" rank="section">Acnida</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Bot. Fenn.</publication_title>
      <place_in_publication>33: 277. 1996</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amaranthaceae;genus amaranthus;subgenus acnida;section Acnida</taxon_hierarchy>
    <other_info_on_name type="fna_id">304252</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="section">Acnida</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1027. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 452. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>section Acnida;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Acnida</taxon_name>
    <taxon_name authority="Moquin-Tandon" date="unknown" rank="section">Acnidastrum</taxon_name>
    <taxon_hierarchy>genus Acnida;section Acnidastrum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Acnida</taxon_name>
    <taxon_name authority="Moquin-Tandon" date="unknown" rank="section">Montelia</taxon_name>
    <taxon_hierarchy>genus Acnida;section Montelia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, glabrous.</text>
      <biological_entity id="o18093" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bracts not enfolding flower, not leaflike, narrowly elliptic to subulate-linear, margins entire, plane (or in some species indistinctly undulate).</text>
      <biological_entity id="o18094" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" notes="" src="d0_s1" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s1" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s1" value="elliptic to subulate-linear" value_original="elliptic to subulate-linear" />
      </biological_entity>
      <biological_entity id="o18095" name="flower" name_original="flower" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="not" name="position" src="d0_s1" value="enfolding" value_original="enfolding" />
      </biological_entity>
      <biological_entity id="o18096" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s1" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Pistillate flowers: tepals absent or 1–2, reduced-linear or linear-lanceolate.</text>
      <biological_entity id="o18097" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o18098" name="tepal" name_original="tepals" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="2" />
        <character is_modifier="false" name="shape" src="d0_s2" value="reduced-linear" value_original="reduced-linear" />
        <character is_modifier="false" name="shape" src="d0_s2" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Utricles usually indehiscent (dehiscent in some forms of A. tuberculatus).</text>
      <biological_entity id="o18099" name="utricle" name_original="utricles" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="dehiscence" src="d0_s3" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3a.1.</number>
  <discussion>Species 4 (4 in the flora).</discussion>
  
</bio:treatment>