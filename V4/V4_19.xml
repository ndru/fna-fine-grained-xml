<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">3</other_info_on_meta>
    <other_info_on_meta type="mention_page">4</other_info_on_meta>
    <other_info_on_meta type="treatment_page">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">phytolaccaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1771" rank="genus">GISEKIA</taxon_name>
    <place_of_publication>
      <publication_title>Mant. Pl.</publication_title>
      <place_in_publication>2: 554, 562 (as Gisechia). 1771</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phytolaccaceae;genus GISEKIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Paul Dietrich Giseke, 1741–1796, German professor, botanist, and pupil of Linnaeus</other_info_on_name>
    <other_info_on_name type="fna_id">113578</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual.</text>
      <biological_entity id="o12573" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves opposite or appearing whorled.</text>
      <biological_entity id="o12574" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="opposite" value_original="opposite" />
        <character name="arrangement" src="d0_s1" value="appearing" value_original="appearing" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="whorled" value_original="whorled" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences axillary, compound dichasia, appearing umbelliform.</text>
      <biological_entity id="o12575" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o12576" name="dichasium" name_original="dichasia" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="compound" value_original="compound" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="umbelliform" value_original="umbelliform" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: sepals 5;</text>
      <biological_entity id="o12577" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o12578" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stamens 5;</text>
      <biological_entity id="o12579" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o12580" name="stamen" name_original="stamens" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>carpels 5, free;</text>
      <biological_entity id="o12581" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o12582" name="carpel" name_original="carpels" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s5" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ovaries 5, each 1-loculed;</text>
      <biological_entity id="o12583" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o12584" name="ovary" name_original="ovaries" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-loculed" value_original="1-loculed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style and stigma 1 per carpel;</text>
      <biological_entity id="o12585" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o12586" name="style" name_original="style" src="d0_s7" type="structure" />
      <biological_entity id="o12587" name="stigma" name_original="stigma" src="d0_s7" type="structure">
        <character constraint="per carpel" constraintid="o12588" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o12588" name="carpel" name_original="carpel" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>stigma terminal.</text>
      <biological_entity id="o12589" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o12590" name="stigma" name_original="stigma" src="d0_s8" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Fruits: group of thin-walled, lenticular achenes.</text>
      <biological_entity id="o12591" name="fruit" name_original="fruits" src="d0_s9" type="structure" />
      <biological_entity id="o12592" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="thin-walled" value_original="thin-walled" />
        <character is_modifier="true" name="shape" src="d0_s9" value="lenticular" value_original="lenticular" />
      </biological_entity>
      <relation from="o12591" id="r1820" name="part_of" negation="false" src="d0_s9" to="o12592" />
    </statement>
    <statement id="d0_s10">
      <text>Seed 1 per carpel.</text>
      <biological_entity id="o12593" name="seed" name_original="seed" src="d0_s10" type="structure">
        <character constraint="per carpel" constraintid="o12594" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o12594" name="carpel" name_original="carpel" src="d0_s10" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia, Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <discussion>Species ca. 5 (1 in the flora).</discussion>
  <discussion>Described as “debatably phytolaccaceous” by G. K. Rogers (1985), Gisekia has been referred also to Aizoaceae, Molluginaceae, and even Portulacaceae—and, perhaps the best solution, to its own family, Gisekiaceae.</discussion>
  <discussion>The generic name is sometimes misspelled as “Giesekia.”</discussion>
  <references>
    <reference>Bogle, A. L. 1970b. The genera of Molluginaceae and Aizoaceae in the southeastern United States. J. Arnold Arbor. 51: 431–462.</reference>
    <reference>Gilbert, M. G. 1993. A review of Gisekia (Gisekiaceae). Kew Bull. 48: 343–356.</reference>
    <reference>Godfrey, R. K. 1961. Gisekia pharnacioides, a new weed. Rhodora 63: 226–228.</reference>
    <reference>Narayana, L. L. and P. S. Narayana. 1989. The bearing of amino acids on the status and systematic position of Gisekia Linnaeus. J. Econ. Tax. Bot. 13: 699–704.</reference>
    <reference>Narayana, P. S. and L. L. Narayana. 1988. Systematic position of Gisekia Linnaeus—a numerical assessment. Feddes Repert. 99: 189–193.</reference>
  </references>
  
</bio:treatment>