<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">262</other_info_on_meta>
    <other_info_on_meta type="treatment_page">264</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Nuttall ex Moquin-Tandon in A. P. de Candolle and A. L. P. P. de Candolle" date="1849" rank="genus">aphanisma</taxon_name>
    <taxon_name authority="Nuttall ex Moquin-Tandon in A. P. de Candolle and A. L. P. P. de Candolle" date="1849" rank="species">blitoides</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>13(2): 54. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus aphanisma;species blitoides</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">220000941</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 1–6 dm.</text>
      <biological_entity id="o5017" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="6" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades 2–5 cm.</text>
      <biological_entity id="o5018" name="blade-leaf" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="distance" src="d0_s1" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers urceolate;</text>
      <biological_entity id="o5019" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="urceolate" value_original="urceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>perianth lobes 1-veined, obovate to oblong, obtuse, subequal;</text>
      <biological_entity constraint="perianth" id="o5020" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-veined" value_original="1-veined" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s3" to="oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="size" src="d0_s3" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stamens hypogynous;</text>
      <biological_entity id="o5021" name="stamen" name_original="stamens" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>filaments filiform;</text>
      <biological_entity id="o5022" name="filament" name_original="filaments" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>anthers ellipsoid-globose;</text>
      <biological_entity id="o5023" name="anther" name_original="anthers" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ellipsoid-globose" value_original="ellipsoid-globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stigmas recurved.</text>
      <biological_entity id="o5024" name="stigma" name_original="stigmas" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fruit: utricle broad, 1.2–2 mm.</text>
      <biological_entity id="o5025" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
      <biological_entity id="o5026" name="utricle" name_original="utricle" src="d0_s8" type="structure">
        <character is_modifier="false" name="width" src="d0_s8" value="broad" value_original="broad" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds 1–2 mm.</text>
      <biological_entity id="o5027" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal shrublands, bluffs, saline sands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal shrublands" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="saline sands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>According to the California Native Plant Society, this species is in steep decline on the mainland and is declining also on the Channel Islands.</discussion>
  
</bio:treatment>