<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">76</other_info_on_meta>
    <other_info_on_meta type="mention_page">85</other_info_on_meta>
    <other_info_on_meta type="mention_page">86</other_info_on_meta>
    <other_info_on_meta type="treatment_page">87</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Martinov" date="unknown" rank="family">aizoaceae</taxon_name>
    <taxon_name authority="N. E. Brown" date="1925" rank="genus">carpobrotus</taxon_name>
    <taxon_name authority="(Linnaeus) N. E. Brown in E. P. Phillips" date="1926" rank="species">edulis</taxon_name>
    <place_of_publication>
      <publication_title>in E. P. Phillips, Gen. S. Afr. Fl. Pl.,</publication_title>
      <place_in_publication>249. 1926</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family aizoaceae;genus carpobrotus;species edulis</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220002386</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mesembryanthemum</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">edule</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat. ed.</publication_title>
      <place_in_publication>10, 2: 1060. 1759</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Mesembryanthemum;species edule;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems to 3 m;</text>
      <biological_entity id="o757" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="3" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bark persistent, leathery.</text>
      <biological_entity id="o758" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="texture" src="d0_s1" value="leathery" value_original="leathery" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves green;</text>
      <biological_entity id="o759" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade sharply 3-angled in cross-section, widest proximal to middle, adaxial side concave, outer angle serrate near apex, 5–11 × 1–1.5 cm.</text>
      <biological_entity id="o760" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character constraint="in cross-section" constraintid="o761" is_modifier="false" modifier="sharply" name="shape" src="d0_s3" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="position" notes="" src="d0_s3" value="widest" value_original="widest" />
        <character char_type="range_value" from="proximal" name="position" src="d0_s3" to="middle" />
      </biological_entity>
      <biological_entity id="o761" name="cross-section" name_original="cross-section" src="d0_s3" type="structure" />
      <biological_entity constraint="adaxial" id="o762" name="side" name_original="side" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity constraint="outer" id="o763" name="angle" name_original="angle" src="d0_s3" type="structure">
        <character constraint="near apex" constraintid="o764" is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" notes="" src="d0_s3" to="11" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" notes="" src="d0_s3" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o764" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: pedicel 20–60 mm.</text>
      <biological_entity id="o765" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o766" name="pedicel" name_original="pedicel" src="d0_s4" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s4" to="60" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 8–10 cm diam.;</text>
      <biological_entity id="o767" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="diameter" src="d0_s5" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>calyx lobes 10–60 mm;</text>
    </statement>
    <statement id="d0_s7">
      <text>outer 2 lobes sharply 3-angled in cross-section, abaxial angle serrate near apex, 30–60 mm;</text>
      <biological_entity constraint="calyx" id="o768" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="60" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s7" value="outer" value_original="outer" />
      </biological_entity>
      <biological_entity id="o769" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
        <character constraint="in cross-section" constraintid="o770" is_modifier="false" modifier="sharply" name="shape" src="d0_s7" value="3-angled" value_original="3-angled" />
      </biological_entity>
      <biological_entity id="o770" name="cross-section" name_original="cross-section" src="d0_s7" type="structure" />
      <biological_entity id="o772" name="apex" name_original="apex" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>inner 3 lobes smaller, with membranous margins;</text>
      <biological_entity constraint="abaxial" id="o771" name="angle" name_original="angle" src="d0_s7" type="structure">
        <character constraint="near apex" constraintid="o772" is_modifier="false" name="architecture_or_shape" src="d0_s7" value="serrate" value_original="serrate" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="60" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s8" value="inner" value_original="inner" />
      </biological_entity>
      <biological_entity id="o773" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="size" src="d0_s8" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o774" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="true" name="texture" src="d0_s8" value="membranous" value_original="membranous" />
      </biological_entity>
      <relation from="o773" id="r88" name="with" negation="false" src="d0_s8" to="o774" />
    </statement>
    <statement id="d0_s9">
      <text>petals (including petaloid staminodia) 100–200, yellow, aging pink, 2–4 (–5) -seriate, 30–40 mm;</text>
      <biological_entity id="o775" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="100" name="quantity" src="d0_s9" to="200" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="life_cycle" src="d0_s9" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="2-4(-5)-seriate" value_original="2-4(-5)-seriate" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s9" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 100 per series, yellow, 4 (–7) -seriate, simple to plumose, 6–8 (–12) mm;</text>
      <biological_entity id="o776" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character constraint="per series" constraintid="o777" name="quantity" src="d0_s10" value="100" value_original="100" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s10" value="4(-7)-seriate" value_original="4(-7)-seriate" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s10" value="plumose" value_original="plumose" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="12" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o777" name="series" name_original="series" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow;</text>
      <biological_entity id="o778" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas radiating out over fruits, 8–15 mm.</text>
      <biological_entity id="o779" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character constraint="over fruits" constraintid="o780" is_modifier="false" name="arrangement" src="d0_s12" value="radiating" value_original="radiating" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o780" name="fruit" name_original="fruits" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Fruits yellowish, clavate to subglobose, depressed apically, 20–35 mm.</text>
      <biological_entity id="o781" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="clavate" name="shape" src="d0_s13" to="subglobose" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s13" value="depressed" value_original="depressed" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s13" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds ca. 1000.2n = 18.</text>
      <biological_entity id="o782" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="1000" value_original="1000" />
      </biological_entity>
      <biological_entity constraint="2n" id="o783" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round, mostly spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
        <character name="flowering time" char_type="range_value" modifier="mostly" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal dunes, bluffs and terraces, margins of estuaries</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal dunes" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="terraces" />
        <character name="habitat" value="margins" constraint="of estuaries" />
        <character name="habitat" value="estuaries" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Fla.; Mexico (Baja California); South America (Chile); Europe; s Africa; Pacific Islands (New Zealand); Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="South America (Chile)" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="s Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Hottentot-fig</other_name>
  <discussion>Carpobrotus edulis is extensively planted in gardens and along highways and is also used for dune and bluff stabilization. An invasive, introduced species escaped from cultivation, C. edulis hybridizes with other Carpobrotus species. According to W. Wisura and H. F. Glen (1993), pink-flowered plants are seen in the wild only when C. edulis comes in contact with species of Carpobrotus with purple flowers.</discussion>
  
</bio:treatment>