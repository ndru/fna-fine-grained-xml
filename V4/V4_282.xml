<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">125</other_info_on_meta>
    <other_info_on_meta type="treatment_page">146</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Opuntioideae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">opuntia</taxon_name>
    <taxon_name authority="Griffiths" date="unknown" rank="species">×columbiana</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>43: 523. 1916</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily opuntioideae;genus opuntia;species ×columbiana;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415222</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Opuntia</taxon_name>
    <taxon_name authority="Engelmann &amp; J. M. Bigelow" date="unknown" rank="species">erinacea</taxon_name>
    <taxon_name authority="(Griffiths) L. D. Benson" date="unknown" rank="variety">columbiana</taxon_name>
    <taxon_hierarchy>genus Opuntia;species erinacea;variety columbiana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, low, forming mats, 10–30 cm.</text>
      <biological_entity id="o10587" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="position" src="d0_s0" value="low" value_original="low" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o10588" name="mat" name_original="mats" src="d0_s0" type="structure" />
      <relation from="o10587" id="r1540" name="forming" negation="false" src="d0_s0" to="o10588" />
    </statement>
    <statement id="d0_s1">
      <text>Stem segments easily detached to firmly attached, green, flattened, narrowly to broadly obovate, 3.5–9 × 2.5–5 cm, low tuberculate, glabrous;</text>
      <biological_entity constraint="stem" id="o10589" name="segment" name_original="segments" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="easily" name="fusion" src="d0_s1" value="detached" value_original="detached" />
        <character is_modifier="false" modifier="firmly" name="fixation" src="d0_s1" value="attached" value_original="attached" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character char_type="range_value" from="flattened" name="shape" src="d0_s1" to="narrowly broadly obovate" />
        <character char_type="range_value" from="flattened" name="shape" src="d0_s1" to="narrowly broadly obovate" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s1" to="9" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s1" to="5" to_unit="cm" />
        <character is_modifier="false" name="position" src="d0_s1" value="low" value_original="low" />
        <character is_modifier="false" name="relief" src="d0_s1" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>areoles 4–6 (–7) per diagonal row across midstem segment, oval, 3.5 × 2.5 mm;</text>
      <biological_entity id="o10590" name="areole" name_original="areoles" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="7" />
        <character char_type="range_value" constraint="per diagonal row" constraintid="o10591" from="4" name="quantity" src="d0_s2" to="6" />
        <character is_modifier="false" name="shape" notes="" src="d0_s2" value="oval" value_original="oval" />
        <character name="length" src="d0_s2" unit="mm" value="3.5" value_original="3.5" />
        <character name="width" src="d0_s2" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
      <biological_entity constraint="diagonal" id="o10591" name="row" name_original="row" src="d0_s2" type="structure" />
      <biological_entity constraint="midstem" id="o10592" name="segment" name_original="segment" src="d0_s2" type="structure" />
      <relation from="o10591" id="r1541" name="across" negation="false" src="d0_s2" to="o10592" />
    </statement>
    <statement id="d0_s3">
      <text>wool white.</text>
      <biological_entity id="o10593" name="wool" name_original="wool" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spines in most areoles, straight, acicular, terete;</text>
      <biological_entity id="o10594" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character is_modifier="false" name="course" notes="" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acicular" value_original="acicular" />
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
      </biological_entity>
      <biological_entity id="o10595" name="areole" name_original="areoles" src="d0_s4" type="structure" />
      <relation from="o10594" id="r1542" name="in" negation="false" src="d0_s4" to="o10595" />
    </statement>
    <statement id="d0_s5">
      <text>major spines 0–1 per areole, reflexed to porrect (rarely ascending), gray-white to brown, (15–) 25–40 (–60) mm;</text>
      <biological_entity id="o10596" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character is_modifier="true" name="size" src="d0_s5" value="major" value_original="major" />
        <character char_type="range_value" constraint="per areole" constraintid="o10597" from="0" name="quantity" src="d0_s5" to="1" />
        <character char_type="range_value" from="reflexed" name="orientation" notes="" src="d0_s5" to="porrect" />
        <character char_type="range_value" from="gray-white" name="coloration" src="d0_s5" to="brown" />
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="25" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="60" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s5" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10597" name="areole" name_original="areole" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>intermediate spines 0–3, reflexed, 12–20 mm;</text>
      <biological_entity id="o10598" name="spine" name_original="spines" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="intermediate" value_original="intermediate" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s6" to="3" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>minor spines 2–4, reflexed, yellow-gray, 3–6 mm.</text>
      <biological_entity constraint="minor" id="o10599" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="4" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow-gray" value_original="yellow-gray" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Glochids loosely packed, in small tuft or crescent at adaxial margin of areole, yellowish, inconspicuous, to 4 mm.</text>
      <biological_entity id="o10600" name="glochid" name_original="glochids" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="loosely" name="arrangement" src="d0_s8" value="packed" value_original="packed" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10601" name="tuft" name_original="tuft" src="d0_s8" type="structure">
        <character is_modifier="true" name="size" src="d0_s8" value="small" value_original="small" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o10602" name="margin" name_original="margin" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="crescent" value_original="crescent" />
      </biological_entity>
      <biological_entity id="o10603" name="areole" name_original="areole" src="d0_s8" type="structure" />
      <relation from="o10600" id="r1543" name="in" negation="false" src="d0_s8" to="o10601" />
      <relation from="o10600" id="r1544" name="in" negation="false" src="d0_s8" to="o10602" />
      <relation from="o10602" id="r1545" name="part_of" negation="false" src="d0_s8" to="o10603" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: inner tepals yellow throughout, 30–45 mm;</text>
      <biological_entity id="o10604" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="inner" id="o10605" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="throughout" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s9" to="45" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments white to red;</text>
      <biological_entity id="o10606" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o10607" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow;</text>
      <biological_entity id="o10608" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o10609" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style white;</text>
      <biological_entity id="o10610" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o10611" name="style" name_original="style" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigma lobes green.</text>
      <biological_entity id="o10612" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="stigma" id="o10613" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits rarely set, top-shaped to barrel-shaped, 15–20 × 15 mm, glabrous;</text>
      <biological_entity id="o10614" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="top-shaped" name="shape" notes="" src="d0_s14" to="barrel-shaped" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s14" to="20" to_unit="mm" />
        <character name="width" src="d0_s14" unit="mm" value="15" value_original="15" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o10615" name="set" name_original="set" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>areoles 10–15, most bearing 3–12 spines or distal spines bearing 1–3 spines.</text>
      <biological_entity id="o10616" name="areole" name_original="areoles" src="d0_s15" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s15" to="15" />
      </biological_entity>
      <biological_entity constraint="distal" id="o10618" name="spine" name_original="spines" src="d0_s15" type="structure" />
      <biological_entity id="o10619" name="spine" name_original="spines" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s15" to="3" />
      </biological_entity>
      <relation from="o10616" id="r1546" name="bearing" negation="false" src="d0_s15" to="o10618" />
      <relation from="o10616" id="r1547" name="bearing" negation="false" src="d0_s15" to="o10619" />
    </statement>
    <statement id="d0_s16">
      <text>Seeds tan, large, 5–7 mm;</text>
      <biological_entity id="o10620" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="tan" value_original="tan" />
        <character is_modifier="false" name="size" src="d0_s16" value="large" value_original="large" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s16" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>girdle protruding 1–1.5 mm. 2n = 66.</text>
      <biological_entity id="o10621" name="girdle" name_original="girdle" src="d0_s17" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s17" value="protruding" value_original="protruding" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s17" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10622" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="66" value_original="66" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (late Jun-early Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="late Jun-early Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dark basaltic cliffs and derived sands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dark basaltic cliffs" />
        <character name="habitat" value="derived sands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Idaho, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>33.</number>
  
</bio:treatment>