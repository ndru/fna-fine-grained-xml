<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">40</other_info_on_meta>
    <other_info_on_meta type="treatment_page">45</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">mirabilis</taxon_name>
    <taxon_name authority="(Choisy) A. Gray in W. H. Emory" date="1859" rank="section">Quamoclidion</taxon_name>
    <taxon_name authority="Constance &amp; Rollins" date="1936" rank="species">macfarlanei</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Biol. Soc. Wash.</publication_title>
      <place_in_publication>49: 148. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus mirabilis;section quamoclidion;species macfarlanei;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242415048</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, forming hemispheric clumps 6–8 dm diam., glabrous or sparsely puberulent.</text>
      <biological_entity id="o12205" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_unit="dm" name="diameter" src="d0_s0" to="8" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="puberulent" value_original="puberulent" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o12206" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
      <relation from="o12205" id="r1759" name="forming" negation="false" src="d0_s0" to="o12206" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 6–10 dm.</text>
      <biological_entity id="o12207" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="6" from_unit="dm" name="some_measurement" src="d0_s1" to="10" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves spreading;</text>
      <biological_entity id="o12208" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles of proximal leaves 1–2.5 cm;</text>
      <biological_entity id="o12209" name="petiole" name_original="petioles" src="d0_s3" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o12210" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o12209" id="r1760" name="part_of" negation="false" src="d0_s3" to="o12210" />
    </statement>
    <statement id="d0_s4">
      <text>blades of midstem leaves suborbiculate to widely ovate, 3.5–5 × 3–5 cm, base obtuse to cordate, symmetric, apex obtuse to rounded, rarely acute.</text>
      <biological_entity id="o12211" name="blade" name_original="blades" src="d0_s4" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character char_type="range_value" from="suborbiculate" name="shape" src="d0_s4" to="widely ovate" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s4" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="midstem" id="o12212" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o12213" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="cordate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="symmetric" value_original="symmetric" />
      </biological_entity>
      <biological_entity id="o12214" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="rounded" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o12211" id="r1761" name="part_of" negation="false" src="d0_s4" to="o12212" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres: peduncle 4–25 mm;</text>
      <biological_entity id="o12215" name="involucre" name_original="involucres" src="d0_s5" type="structure" />
      <biological_entity id="o12216" name="peduncle" name_original="peduncle" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>involucres erect or ascending, 13–23 mm;</text>
      <biological_entity id="o12217" name="involucre" name_original="involucres" src="d0_s6" type="structure" />
      <biological_entity id="o12218" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s6" to="23" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts 5, 50% connate, 15–20 mm, apex acute to widely ovate.</text>
      <biological_entity id="o12219" name="involucre" name_original="involucres" src="d0_s7" type="structure" />
      <biological_entity id="o12220" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
        <character is_modifier="false" modifier="50%" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12221" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="widely ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers 6 per involucre;</text>
      <biological_entity id="o12222" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character constraint="per involucre" constraintid="o12223" name="quantity" src="d0_s8" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o12223" name="involucre" name_original="involucre" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>perianth magenta, funnelform, 1.5–2.5 cm.</text>
      <biological_entity id="o12224" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="magenta" value_original="magenta" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s9" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Fruits light-brown, with 10 slender ribs visible when wet, widely obovoid to ellipsoid, 6–7.5 mm, tuberculate, glabrous or very sparsely puberulent, secreting mucilage most abundantly on ribs when wetted.</text>
      <biological_entity id="o12225" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="light-brown" value_original="light-brown" />
        <character char_type="range_value" from="widely obovoid" name="shape" notes="" src="d0_s10" to="ellipsoid" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="7.5" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s10" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="very sparsely" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="slender" id="o12226" name="rib" name_original="ribs" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="10" value_original="10" />
        <character is_modifier="false" modifier="when wet" name="prominence" src="d0_s10" value="visible" value_original="visible" />
      </biological_entity>
      <biological_entity id="o12227" name="mucilage" name_original="mucilage" src="d0_s10" type="structure" />
      <biological_entity id="o12228" name="rib" name_original="ribs" src="d0_s10" type="structure" />
      <relation from="o12225" id="r1762" name="with" negation="false" src="d0_s10" to="o12226" />
      <relation from="o12225" id="r1763" name="secreting" negation="false" src="d0_s10" to="o12227" />
      <relation from="o12225" id="r1764" modifier="abundantly; when wetted" name="on" negation="false" src="d0_s10" to="o12228" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Exposed slopes in canyons</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="slopes" modifier="exposed" constraint="in canyons" />
        <character name="habitat" value="canyons" modifier="in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400-500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Mirabilis macfarlanei is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>