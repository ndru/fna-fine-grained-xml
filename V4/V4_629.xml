<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">332</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">atriplex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Atriplex</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="section">Teutliopsis</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">littoralis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1054. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus atriplex;subgenus atriplex;section teutliopsis;species littoralis;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242415499</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atriplex</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">hastata</taxon_name>
    <taxon_name authority="(Linnaeus) Farwell" date="unknown" rank="variety">littoralis</taxon_name>
    <taxon_hierarchy>genus Atriplex;species hastata;variety littoralis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atriplex</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">patula</taxon_name>
    <taxon_name authority="(Linnaeus) A. Gray" date="unknown" rank="variety">littoralis</taxon_name>
    <taxon_hierarchy>genus Atriplex;species patula;variety littoralis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atriplex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">patula</taxon_name>
    <taxon_name authority="(Linnaeus) H. M. Hall &amp; Clements" date="unknown" rank="subspecies">littoralis</taxon_name>
    <taxon_hierarchy>genus Atriplex;species patula;subspecies littoralis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, monoecious or subdioecious, 2.5–7.5 (–10) dm.</text>
      <biological_entity id="o9942" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="subdioecious" value_original="subdioecious" />
        <character char_type="range_value" from="7.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character char_type="range_value" from="2.5" from_unit="dm" name="some_measurement" src="d0_s0" to="7.5" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or forming sprawling tangled clumps and mostly green;</text>
      <biological_entity id="o9943" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character name="orientation" src="d0_s1" value="forming sprawling tangled clumps" value_original="forming sprawling tangled clumps" />
        <character is_modifier="false" modifier="mostly" name="coloration" src="d0_s1" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches erect-ascending, proximal ones opposite and ascending, sparsely scurfy when young, obtusely angled.</text>
      <biological_entity id="o9944" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect-ascending" value_original="erect-ascending" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o9945" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s2" value="scurfy" value_original="scurfy" />
        <character is_modifier="false" modifier="obtusely" name="shape" src="d0_s2" value="angled" value_original="angled" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate or only proximalmost opposite, all shortly petiolate;</text>
      <biological_entity id="o9946" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity id="o9947" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="only" name="position" src="d0_s3" value="proximalmost" value_original="proximalmost" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade green on both surfaces, linear, linear-oblong, linear-lanceolate, or oblong, (10–) 25–80 (–120) × 2–8 (–12) mm, thin, gradually narrowed, margin mostly entire or some sinuate-dentate with antrorse teeth, apex obtuse to acuminate.</text>
      <biological_entity id="o9948" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character constraint="on surfaces" constraintid="o9949" is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-oblong" value_original="linear-oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_length" src="d0_s4" to="25" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="120" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s4" to="80" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="12" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s4" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s4" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o9949" name="surface" name_original="surfaces" src="d0_s4" type="structure" />
      <biological_entity id="o9950" name="margin" name_original="margin" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s4" value="some" value_original="some" />
        <character constraint="with teeth" constraintid="o9951" is_modifier="false" name="architecture_or_shape" src="d0_s4" value="sinuate-dentate" value_original="sinuate-dentate" />
      </biological_entity>
      <biological_entity id="o9951" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s4" value="antrorse" value_original="antrorse" />
      </biological_entity>
      <biological_entity id="o9952" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences of long, dense or interrupted hairy spikes often forming panicle to 20 cm;</text>
      <biological_entity id="o9953" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9954" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s5" value="long" value_original="long" />
        <character is_modifier="true" name="density" src="d0_s5" value="dense" value_original="dense" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="interrupted" value_original="interrupted" />
        <character is_modifier="true" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o9955" name="panicle" name_original="panicle" src="d0_s5" type="structure" />
      <relation from="o9953" id="r1440" name="part_of" negation="false" src="d0_s5" to="o9954" />
      <relation from="o9953" id="r1441" name="forming" negation="false" src="d0_s5" to="o9955" />
    </statement>
    <statement id="d0_s6">
      <text>staminate flowers 4–5-merous.</text>
      <biological_entity id="o9956" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="4-5-merous" value_original="4-5-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pistillate bracteoles sessile, triangular to ovate or ovate-rhombic, (3–) 5–7 mm, mostly denticulate, rarely subentire, faces tuberculate almost distinct, green becoming brown or black.</text>
      <biological_entity id="o9957" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s7" to="ovate or ovate-rhombic" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s7" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s7" value="subentire" value_original="subentire" />
      </biological_entity>
      <biological_entity id="o9958" name="face" name_original="faces" src="d0_s7" type="structure">
        <character is_modifier="false" name="relief" src="d0_s7" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" modifier="almost" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s7" value="green becoming brown or black" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seeds dimorphic: brown, 2–2.8 mm wide, round and ± flattened, or black, 1.5–2 mm wide, round, evenly convex;</text>
      <biological_entity id="o9959" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s8" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="2.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="round" value_original="round" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="black" value_original="black" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="round" value_original="round" />
        <character is_modifier="false" modifier="evenly" name="shape" src="d0_s8" value="convex" value_original="convex" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>radicle inferior.</text>
      <biological_entity id="o9960" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s9" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>2n = 18.</text>
      <biological_entity id="o9961" name="radicle" name_original="radicle" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="inferior" value_original="inferior" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9962" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sea beaches and other saline habitats, old ports and ballast dumps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sea beaches" />
        <character name="habitat" value="other saline habitats" />
        <character name="habitat" value="old ports" />
        <character name="habitat" value="ballast dumps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>mainly below 100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" modifier="mainly" to="100" to_unit="m" from="any" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; N.B., N.S., P.E.I.; Ill., Ind., Maine, Mass., Mich., N.H., Ohio, Pa.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2</number>
  <other_name type="common_name">Narrow-leaved atriplex</other_name>
  
</bio:treatment>