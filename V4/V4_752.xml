<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">330</other_info_on_meta>
    <other_info_on_meta type="mention_page">369</other_info_on_meta>
    <other_info_on_meta type="mention_page">372</other_info_on_meta>
    <other_info_on_meta type="mention_page">373</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">atriplex</taxon_name>
    <taxon_name authority="(Torrey &amp; Frémont) S. L. Welsh" date="2001" rank="subgenus">Pterochiton</taxon_name>
    <taxon_name authority="(Pursh) Nuttall" date="1818" rank="species">canescens</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>1: 197. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus atriplex;subgenus pterochiton;species canescens;</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242100016</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calligonum</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="species">canescens</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer Sept.</publication_title>
      <place_in_publication>2: 370. 1814</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Calligonum;species canescens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atriplex</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">nuttallii</taxon_name>
    <taxon_hierarchy>genus Atriplex;species nuttallii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, dioecious or rarely monoecious, mainly 8–20 dm, as wide or wider, not especially armed.</text>
      <biological_entity id="o9627" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" modifier="rarely" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character char_type="range_value" from="8" from_unit="dm" modifier="mainly" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character is_modifier="false" name="width" src="d0_s0" value="wide" value_original="wide" />
        <character is_modifier="false" name="width" src="d0_s0" value="wider" value_original="wider" />
        <character is_modifier="false" modifier="not especially" name="architecture" src="d0_s0" value="armed" value_original="armed" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves persistent, alternate, sessile or nearly so, blade linear to oblanceolate, oblong, or obovate, mainly 10–40 × 3–8 mm, margin entire, apex retuse to obtuse.</text>
      <biological_entity id="o9628" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="sessile" value_original="sessile" />
        <character name="architecture" src="d0_s1" value="nearly" value_original="nearly" />
      </biological_entity>
      <biological_entity id="o9629" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s1" to="oblanceolate oblong or obovate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s1" to="oblanceolate oblong or obovate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s1" to="oblanceolate oblong or obovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s1" to="40" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s1" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9630" name="margin" name_original="margin" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mainly" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o9631" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character char_type="range_value" from="retuse" name="shape" src="d0_s1" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Staminate flowers yellow (rarely brown), in clusters 2–3 mm wide, borne in panicles 3–15 cm.</text>
      <biological_entity id="o9632" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow" value_original="yellow" />
        <character constraint="in clusters 2-3 mm" is_modifier="false" name="width" src="d0_s2" value="wide" value_original="wide" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9633" name="panicle" name_original="panicles" src="d0_s2" type="structure" />
      <relation from="o9632" id="r1381" name="borne in" negation="false" src="d0_s2" to="o9633" />
    </statement>
    <statement id="d0_s3">
      <text>Pistillate flowers borne in panicles 5–40 cm.</text>
      <biological_entity id="o9635" name="panicle" name_original="panicles" src="d0_s3" type="structure" />
      <relation from="o9634" id="r1382" name="borne in" negation="false" src="d0_s3" to="o9635" />
    </statement>
    <statement id="d0_s4">
      <text>Fruiting bracteoles 8–25 mm, as wide, on stipes 1–8 mm, with 4 prominent wings extending the bract length, united throughout, wings dentate to entire, apex toothed, surface of wings and body smooth or reticulate.</text>
      <biological_entity id="o9634" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="40" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9636" name="bracteole" name_original="bracteoles" src="d0_s4" type="structure" />
      <biological_entity id="o9637" name="stipe" name_original="stipes" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="as wide" from="8" from_unit="mm" name="some_measurement" src="d0_s4" to="25" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9638" name="wing" name_original="wings" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="4" value_original="4" />
        <character is_modifier="true" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o9639" name="bract" name_original="bract" src="d0_s4" type="structure" />
      <biological_entity id="o9640" name="whole-organism" name_original="" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="throughout" name="length" src="d0_s4" value="united" value_original="united" />
      </biological_entity>
      <biological_entity id="o9641" name="wing" name_original="wings" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s4" value="dentate to entire" value_original="dentate to entire" />
      </biological_entity>
      <biological_entity id="o9642" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o9643" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity id="o9644" name="wing" name_original="wings" src="d0_s4" type="structure" />
      <biological_entity id="o9645" name="body" name_original="body" src="d0_s4" type="structure" />
      <relation from="o9634" id="r1383" name="fruiting" negation="false" src="d0_s4" to="o9636" />
      <relation from="o9638" id="r1384" name="extending the" negation="false" src="d0_s4" to="o9639" />
      <relation from="o9643" id="r1385" name="part_of" negation="false" src="d0_s4" to="o9644" />
      <relation from="o9643" id="r1386" name="part_of" negation="false" src="d0_s4" to="o9645" />
    </statement>
    <statement id="d0_s5">
      <text>Seeds 1.5–2.5 mm wide.</text>
    </statement>
    <statement id="d0_s6">
      <text>2n = 18, 36+.</text>
      <biological_entity id="o9646" name="seed" name_original="seeds" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9647" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="18" value_original="18" />
        <character char_type="range_value" from="36" name="quantity" src="d0_s6" upper_restricted="false" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta.; Ariz., Calif., Colo., Idaho, Kans., Mont., N.Dak., N.Mex., Nebr., Nev., Okla., Oreg., S.Dak., Tex., Utah, Wash., Wyo.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>61.</number>
  <other_name type="common_name">Four-wing saltbu sh</other_name>
  <discussion>This species forms hybrids with Atriplex confertifolia and A. gardneri varieties (see var. bonnevillensis). Materials from the vicinity of the type locality of the species in South Dakota are low subherbaceous plants that differ from our shrubby tall material. However, the type area is presently covered with water from a dam on the Missouri River, and it is not possible to exclude the possibility of A. canescens as it has been interpreted for the past century to have existed at that site during the Lewis and Clark Expedition, if that is indeed where the lectotype was collected.</discussion>
  <discussion>Varieties 4 (4 in the flora).</discussion>
  <discussion>Nuttall’s new combination Atriplex canescens was based on Calligonum canescens Pursh. Watson based his new name A. nuttallii directly on A. canescens Nuttall, i.e., including the citation of Nuttall’s “Genera, 1. 197,” and on its basionym, C. canescens. The name A. nuttallii is thus a nomenclatural synonym of A. canescens and was thus illegitimate at its inception. It cannot be resurrected by even the most sophisticated arguments. A sheet of Atriplex canescens, noted as “a shrub,” taken by Nuttall on the 1810 Missouri River expedition is extant in the Lambert herbarium (PH). It bears several, obviously shrubby, staminate flowering branches, but the only pistillate branch is very immature. The name A. gardneri, also cited provisionally by Watson within the concept of A. nuttallii, clearly has priority over other names for that widely distributed species complex. Attempts at leptotypification of the name nuttallii by J. McNeill et al. (1983) and H. C. Stutz and S. C. Sanderson (1998) are both superfluous, the name being illegitimate.</discussion>
  <discussion>C. A. Hanson (1962) noted the similarity between the occasional wingless fruiting bracteoles of Atriplex canescens and A. gardneri var. falcata. He noted further that the bracts of both species lack lateral teeth subtending the terminal ones, have terminal teeth united half their length, and have indurate bracts. Whether such similarity indicates relationship or mere coincidence is open to question. However, A. canescens is known to form hybrids with most, if not all, portions of the gardneri complex and with other woody species whose range it overlaps as well.</discussion>
  <references>
    <reference>Stutz, H. C. and S. C. Sanderson. 1979. The role of polyploidy in the evolution of Atriplex canescens. In: J. R. Goodin and D. K. Northington, eds. 1929. Arid Plant Resources…. Lubbock. Pp. 615–621.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruiting bracteoles merely dentate to entire, commonly 8-25 mm wide; plants of broad or other distribution</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruiting bracteoles dentate to laciniate, mainly 5-10 mm wide; plants of s California</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Bracteoles mainly less than 12 mm wide; plants not layering in sand dunes, widespread</description>
      <determination>61a Atriplex canescens var. canescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Bracteoles mainly 15- mm wide; plants layering in sand dunes, cw Utah</description>
      <determination>61b Atriplex canescens var. gigantea</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Fruiting bracteoles mainly 4-8 mm wide, dentate or laciniate; shrubs mainly 3-10 dm</description>
      <determination>61c Atriplex canescens var. macilenta</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Fruiting bracteoles mainly 8-11 mm wide, la- ciniate; shrubs mainly 10-20 dm</description>
      <determination>61d Atriplex canescens var. laciniata</determination>
    </key_statement>
  </key>
</bio:treatment>