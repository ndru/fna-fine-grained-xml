<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="treatment_page">263</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="S. Watson" date="1871" rank="genus">nitrophila</taxon_name>
    <taxon_name authority="Munz &amp; J. C. Roos" date="1955" rank="species">mohavensis</taxon_name>
    <place_of_publication>
      <publication_title>Aliso</publication_title>
      <place_in_publication>3: 112, figs. 1–3. 1955</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus nitrophila;species mohavensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242415372</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 3–10 cm, base often buried with long internodes and scalelike leaves, aboveground portion densely leafy with overlapping leaves.</text>
      <biological_entity id="o1317" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1318" name="base" name_original="base" src="d0_s0" type="structure">
        <character constraint="with leaves" constraintid="o1320" is_modifier="false" modifier="often" name="location" src="d0_s0" value="buried" value_original="buried" />
      </biological_entity>
      <biological_entity id="o1319" name="internode" name_original="internodes" src="d0_s0" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s0" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o1320" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="scale-like" value_original="scalelike" />
      </biological_entity>
      <biological_entity id="o1321" name="portion" name_original="portion" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="aboveground" value_original="aboveground" />
        <character constraint="with leaves" constraintid="o1322" is_modifier="false" modifier="densely" name="architecture" src="d0_s0" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o1322" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s0" value="overlapping" value_original="overlapping" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves of main-stems often auriculate-clasping at base;</text>
      <biological_entity id="o1323" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character constraint="at base" constraintid="o1325" is_modifier="false" modifier="often" name="architecture_or_fixation" src="d0_s1" value="auriculate-clasping" value_original="auriculate-clasping" />
      </biological_entity>
      <biological_entity id="o1324" name="main-stem" name_original="main-stems" src="d0_s1" type="structure" />
      <biological_entity id="o1325" name="base" name_original="base" src="d0_s1" type="structure" />
      <relation from="o1323" id="r177" name="part_of" negation="false" src="d0_s1" to="o1324" />
    </statement>
    <statement id="d0_s2">
      <text>blade flat (not terete) with keel-like midrib, broadly ovate, 2.3–4 (–4.7) × 2.5–3.5 mm at base, apex mucronate or apiculate.</text>
      <biological_entity id="o1326" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character constraint="with midrib" constraintid="o1327" is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s2" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="4.7" to_unit="mm" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="length" src="d0_s2" to="4" to_unit="mm" />
        <character char_type="range_value" constraint="at base" constraintid="o1328" from="2.5" from_unit="mm" name="width" src="d0_s2" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1327" name="midrib" name_original="midrib" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="keel-like" value_original="keel-like" />
      </biological_entity>
      <biological_entity id="o1328" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o1329" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences solitary, sessile flowers.</text>
      <biological_entity id="o1330" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o1331" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: perianth segments erect, pinkish, ovate, 2.3–3.5 mm;</text>
      <biological_entity id="o1332" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity constraint="perianth" id="o1333" name="segment" name_original="segments" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s4" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stamens included;</text>
      <biological_entity id="o1334" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o1335" name="stamen" name_original="stamens" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>filaments shortly connate basally;</text>
      <biological_entity id="o1336" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o1337" name="filament" name_original="filaments" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="shortly; basally" name="fusion" src="d0_s6" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style equaling stigma branches.</text>
      <biological_entity id="o1338" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o1339" name="style" name_original="style" src="d0_s7" type="structure" />
      <biological_entity constraint="stigma" id="o1340" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="variability" src="d0_s7" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Utricle concealed by persistent calyx.</text>
      <biological_entity id="o1341" name="utricle" name_original="utricle" src="d0_s8" type="structure">
        <character constraint="by calyx" constraintid="o1342" is_modifier="false" name="prominence" src="d0_s8" value="concealed" value_original="concealed" />
      </biological_entity>
      <biological_entity id="o1342" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character is_modifier="true" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds black, ca 1.2 mm, shiny.</text>
      <biological_entity id="o1343" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="black" value_original="black" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="1.2" value_original="1.2" />
        <character is_modifier="false" name="reflectance" src="d0_s9" value="shiny" value_original="shiny" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist alkaline soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist alkaline soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600-750 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="750" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Amargosa niterwort</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Nitrophila mohavensis is endemic to the Amargosa Desert and occurs with Distichlis stricta, Cordylanthus tecopensis, and Cleomella brevipes.</discussion>
  
</bio:treatment>