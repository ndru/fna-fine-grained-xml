<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">boerhavia</taxon_name>
    <taxon_name authority="Standley" date="unknown" rank="species">megaptera</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>12: 379. 1909</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus boerhavia;species megaptera</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415014</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual;</text>
      <biological_entity id="o5028" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot tapered, soft or ± woody.</text>
      <biological_entity id="o5029" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="texture" src="d0_s1" value="soft" value_original="soft" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually erect or ascending, occasionally decumbent, branched several times, primarily distally, 3–6 dm, minutely puberulent with bent hairs and sometimes also glandular-hairs in basal portions;</text>
      <biological_entity id="o5030" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="occasionally" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="quantity" src="d0_s2" value="several" value_original="several" />
        <character char_type="range_value" from="3" from_unit="dm" modifier="primarily distally; distally" name="some_measurement" src="d0_s2" to="6" to_unit="dm" />
        <character constraint="with hairs" constraintid="o5031" is_modifier="false" modifier="minutely" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o5031" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="bent" value_original="bent" />
      </biological_entity>
      <biological_entity constraint="basal" id="o5033" name="portion" name_original="portions" src="d0_s2" type="structure" />
      <relation from="o5032" id="r679" modifier="sometimes" name="in" negation="false" src="d0_s2" to="o5033" />
    </statement>
    <statement id="d0_s3">
      <text>sparsely to moderately puberulent distally.</text>
      <biological_entity id="o5032" name="glandular-hair" name_original="glandular-hairs" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately; distally" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves mostly in basal 1/2 of plant;</text>
      <biological_entity id="o5034" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o5035" name="1/2" name_original="1/2" src="d0_s4" type="structure" />
      <biological_entity id="o5036" name="plant" name_original="plant" src="d0_s4" type="structure" />
      <relation from="o5034" id="r680" name="in" negation="false" src="d0_s4" to="o5035" />
      <relation from="o5035" id="r681" name="part_of" negation="false" src="d0_s4" to="o5036" />
    </statement>
    <statement id="d0_s5">
      <text>larger leaves with petiole 12–17 mm, blade oblong-lanceolate, 25–50 × 10–17 mm (distal leaves smaller, sometimes longer, proportionately narrower), adaxial surface glabrous or very sparsely puberulent, often punctate, abaxial surface paler than adaxial, glabrous or very sparsely puberulent, usually punctate with small dark-brown cells, base round, obtuse, or truncate, margins entire or slightly and irregularly crenulate, apex broadly to narrowly acute.</text>
      <biological_entity constraint="larger" id="o5037" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o5038" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s5" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5039" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s5" to="50" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s5" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o5040" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="very sparsely" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="often" name="coloration_or_relief" src="d0_s5" value="punctate" value_original="punctate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o5041" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character constraint="than adaxial surface" constraintid="o5042" is_modifier="false" name="coloration" src="d0_s5" value="paler" value_original="paler" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="very sparsely" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character constraint="with cells" constraintid="o5043" is_modifier="false" modifier="usually" name="coloration_or_relief" src="d0_s5" value="punctate" value_original="punctate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o5042" name="surface" name_original="surface" src="d0_s5" type="structure" />
      <biological_entity id="o5043" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="small dark-brown" value_original="small dark-brown" />
      </biological_entity>
      <biological_entity id="o5044" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="round" value_original="round" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o5045" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s5" value="slightly" value_original="slightly" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s5" value="crenulate" value_original="crenulate" />
      </biological_entity>
      <biological_entity id="o5046" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly to narrowly" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o5037" id="r682" name="with" negation="false" src="d0_s5" to="o5038" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, forked ± unevenly ca. 3–4 times, with sticky internodal bands;</text>
      <biological_entity id="o5047" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="more or less unevenly; unevenly" name="shape" src="d0_s6" value="forked" value_original="forked" />
        <character constraint="band" constraintid="o5048" is_modifier="false" name="size_or_quantity" src="d0_s6" value="3-4 times with sticky internodal bands" />
        <character constraint="band" constraintid="o5049" is_modifier="false" name="size_or_quantity" src="d0_s6" value="3-4 times with sticky internodal bands" />
      </biological_entity>
      <biological_entity constraint="internodal" id="o5048" name="band" name_original="bands" src="d0_s6" type="structure" />
      <biological_entity constraint="internodal" id="o5049" name="band" name_original="bands" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>branches strongly ascending, terminating in umbels (rarely flowers borne singly).</text>
      <biological_entity id="o5050" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o5051" name="umbel" name_original="umbels" src="d0_s7" type="structure" />
      <relation from="o5050" id="r683" name="terminating in" negation="false" src="d0_s7" to="o5051" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: pedicel 1–2 mm;</text>
      <biological_entity id="o5052" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o5053" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts at base of perianth quickly deciduous, 2, lanceolate, 0.6–1.5 mm;</text>
      <biological_entity id="o5054" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o5055" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5056" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="quickly" name="duration" src="d0_s9" value="deciduous" value_original="deciduous" />
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o5057" name="perianth" name_original="perianth" src="d0_s9" type="structure" />
      <relation from="o5055" id="r684" name="at" negation="false" src="d0_s9" to="o5056" />
      <relation from="o5056" id="r685" name="part_of" negation="false" src="d0_s9" to="o5057" />
    </statement>
    <statement id="d0_s10">
      <text>perianth whitish to pale-pink, campanulate distal to constriction, 1–1.5 mm;</text>
      <biological_entity id="o5058" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o5059" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s10" to="pale-pink" />
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
        <character constraint="to constriction" constraintid="o5060" is_modifier="false" name="position_or_shape" src="d0_s10" value="distal" value_original="distal" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5060" name="constriction" name_original="constriction" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>stamens 3–4, barely exserted.</text>
      <biological_entity id="o5061" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o5062" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s11" to="4" />
        <character is_modifier="false" modifier="barely" name="position" src="d0_s11" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits (1–) 3–14 per umbel, straw colored at maturity, broadly obconic, emarginate in profile (wings extended beyond apex of body), 3.2–3.8 × 2.2–2.6 mm (l/w: 1.3–1.5), glabrous;</text>
      <biological_entity id="o5063" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s12" to="3" to_inclusive="false" />
        <character char_type="range_value" constraint="per umbel" constraintid="o5064" from="3" name="quantity" src="d0_s12" to="14" />
        <character constraint="at maturity" is_modifier="false" name="coloration" notes="" src="d0_s12" value="straw colored" value_original="straw colored" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s12" value="obconic" value_original="obconic" />
        <character is_modifier="false" modifier="in profile" name="architecture_or_shape" src="d0_s12" value="emarginate" value_original="emarginate" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="length" src="d0_s12" to="3.8" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="width" src="d0_s12" to="2.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o5064" name="umbel" name_original="umbel" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>ribs 5, winglike, smooth;</text>
      <biological_entity id="o5065" name="rib" name_original="ribs" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="winglike" value_original="winglike" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>sulci 1–2 times as wide as base of ribs, smooth or very slightly rugose, not papillate.</text>
      <biological_entity id="o5066" name="sulcus" name_original="sulci" src="d0_s14" type="structure">
        <character constraint="of ribs" constraintid="o5068" is_modifier="false" name="width" src="d0_s14" value="1-2 times as wide as base" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="very slightly" name="relief" src="d0_s14" value="rugose" value_original="rugose" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s14" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity id="o5067" name="base" name_original="base" src="d0_s14" type="structure" />
      <biological_entity id="o5068" name="rib" name_original="ribs" src="d0_s14" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky ground, among desert shrubs or trees</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky ground" constraint="among desert shrubs or trees" />
        <character name="habitat" value="desert shrubs" />
        <character name="habitat" value="trees" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700-1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="past_name">Boerhaavia</other_name>
  <discussion>Boerhavia megaptera is often sympatric with B. intermedia.</discussion>
  
</bio:treatment>