<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">496</other_info_on_meta>
    <other_info_on_meta type="mention_page">497</other_info_on_meta>
    <other_info_on_meta type="treatment_page">498</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Adanson" date="unknown" rank="family">portulacaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">portulaca</taxon_name>
    <taxon_name authority="Hooker" date="1829" rank="species">grandiflora</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Mag.</publication_title>
      <place_in_publication>56: plate 2885. 1829</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family portulacaceae;genus portulaca;species grandiflora</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200007019</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
      <biological_entity id="o9529" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots fibrous.</text>
      <biological_entity id="o9530" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems prostrate to suberect;</text>
      <biological_entity id="o9531" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s2" to="suberect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>trichomes conspicuous at nodes and in inflorescence;</text>
      <biological_entity id="o9532" name="trichome" name_original="trichomes" src="d0_s3" type="structure">
        <character constraint="at nodes and in inflorescence" constraintid="o9533" is_modifier="false" name="prominence" src="d0_s3" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o9533" name="inflorescence" name_original="inflorescence" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>branches to 30 cm.</text>
      <biological_entity id="o9534" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaf-blades linear to lanceolate, terete to hemispheric, 5–30 × 1–5 mm, apex acute or subacute;</text>
      <biological_entity id="o9535" name="leaf-blade" name_original="leaf-blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="lanceolate terete" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="lanceolate terete" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="30" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9536" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subacute" value_original="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>involucrelike leaves 8–9 (–14).</text>
      <biological_entity id="o9537" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="involucrelike" value_original="involucrelike" />
        <character char_type="range_value" from="9" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="14" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s6" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 25–55 mm diam.;</text>
      <biological_entity id="o9538" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="diameter" src="d0_s7" to="55" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals pink, red, purple, yellow, bronze, or white, obovate, 15–25 × 15–20 mm;</text>
      <biological_entity id="o9539" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="bronze" value_original="bronze" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="bronze" value_original="bronze" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s8" to="25" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s8" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 40 or more;</text>
      <biological_entity id="o9540" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" unit="or more" value="40" value_original="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmas 5–8.</text>
      <biological_entity id="o9541" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules ovoid, (3.5–) 4–6.5 mm diam.</text>
      <biological_entity id="o9542" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="diameter" src="d0_s11" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s11" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds steely gray, often iridescent, orbiculate or elongate, flattened, 0.75–1 mm diam.;</text>
      <biological_entity id="o9543" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="gray" value_original="gray" />
        <character is_modifier="false" modifier="often" name="coloration_or_reflectance" src="d0_s12" value="iridescent" value_original="iridescent" />
        <character is_modifier="false" name="shape" src="d0_s12" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="0.75" from_unit="mm" name="diameter" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>surface cells obscurely stellate with tubercles mostly abaxially.</text>
      <biological_entity id="o9545" name="tubercle" name_original="tubercles" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 18.</text>
      <biological_entity constraint="surface" id="o9544" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character constraint="with tubercles" constraintid="o9545" is_modifier="false" modifier="obscurely" name="arrangement_or_shape" src="d0_s13" value="stellate" value_original="stellate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9546" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ont.; Ala., Ariz., Ark., Calif., Colo., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Md., Mass., Mich., Minn., Miss., Mo., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Pa., R.I., S.C., S.Dak., Tenn., Tex., Vt., Va., W.Va., Wis.; South America; naturalized in Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="naturalized in Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Moss-rose</other_name>
  <discussion>Portulaca grandiflora is naturalized in gardens and has escaped to roadsides and waste places. Studies of seed surfaces from specimens representing 100 years of collections with distribution over the United States show remarkable consistency in seed morphology. Tubercles may extend onto the lateral surface, where they are widely scattered. Only one specimen was found with no tubercles at all.</discussion>
  
</bio:treatment>