<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">40</other_info_on_meta>
    <other_info_on_meta type="mention_page">41</other_info_on_meta>
    <other_info_on_meta type="treatment_page">50</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">mirabilis</taxon_name>
    <taxon_name authority="(L’Heritier ex Willdenow) Heimerl in H. G. A. Engler and K. Prantl" date="unknown" rank="section">Oxybaphus</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler and K. Prantl, Nat. Pflanzenfam.</publication_title>
      <place_in_publication>31[III,1b]: 24. 1889</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus mirabilis;section Oxybaphus</taxon_hierarchy>
    <other_info_on_name type="fna_id">304225</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="L’Heritier ex Willdenow" date="unknown" rank="section">Oxybaphus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1(1): 170, 185. 1797</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>section Oxybaphus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, from cylindric, cordlike or thick and woody roots.</text>
      <biological_entity id="o19806" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o19807" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="cylindric" value_original="cylindric" />
        <character is_modifier="true" name="shape" src="d0_s0" value="cordlike" value_original="cordlike" />
        <character is_modifier="true" name="width" src="d0_s0" value="thick" value_original="thick" />
        <character is_modifier="true" name="texture" src="d0_s0" value="woody" value_original="woody" />
      </biological_entity>
      <relation from="o19806" id="r2880" name="from" negation="false" src="d0_s0" to="o19807" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to decumbent, sparsely to densely leafy primarily in proximal 2/3 or throughout.</text>
      <biological_entity id="o19808" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="decumbent" />
        <character constraint="in proximal 2/3" constraintid="o19809" is_modifier="false" modifier="sparsely to densely" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o19809" name="2/3" name_original="2/3" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal leaves petiolate, blade proportionately broader;</text>
      <biological_entity id="o19810" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o19811" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o19812" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="proportionately" name="width" src="d0_s2" value="broader" value_original="broader" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal leaves sessile or subsessile, blade narrower, margins entire to crisped.</text>
      <biological_entity id="o19813" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o19814" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subsessile" value_original="subsessile" />
      </biological_entity>
      <biological_entity id="o19815" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" src="d0_s3" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o19816" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s3" to="crisped" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences axillary and terminal in open or congested, few or repeatedly branched cymes (single in axils, especially in cleistogamous phases);</text>
      <biological_entity id="o19817" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character constraint="in cymes" constraintid="o19818" is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o19818" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="congested" value_original="congested" />
        <character is_modifier="true" name="quantity" src="d0_s4" value="few" value_original="few" />
        <character is_modifier="true" modifier="repeatedly" name="architecture" src="d0_s4" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>involucres usually notably accrescent, becoming tan, translucent, prominently net-veined, broadly bell-shaped to saucer-shaped, with (1–) 3 flowers inserted at base.</text>
      <biological_entity id="o19819" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually notably" name="size" src="d0_s5" value="accrescent" value_original="accrescent" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s5" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s5" value="translucent" value_original="translucent" />
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s5" value="net-veined" value_original="net-veined" />
        <character char_type="range_value" from="broadly bell-shaped" name="shape" src="d0_s5" to="saucer-shaped" />
      </biological_entity>
      <biological_entity id="o19820" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="atypical_quantity" src="d0_s5" to="3" to_inclusive="false" />
        <character is_modifier="true" name="quantity" src="d0_s5" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o19821" name="base" name_original="base" src="d0_s5" type="structure" />
      <relation from="o19819" id="r2881" name="with" negation="false" src="d0_s5" to="o19820" />
      <relation from="o19820" id="r2882" name="inserted at" negation="false" src="d0_s5" to="o19821" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: perianth broadly funnelform, abruptly flared from narrow tubes, deeply 5-lobed;</text>
      <biological_entity id="o19822" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o19823" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character constraint="from tubes" constraintid="o19824" is_modifier="false" modifier="abruptly" name="shape" src="d0_s6" value="flared" value_original="flared" />
        <character is_modifier="false" modifier="deeply" name="shape" notes="" src="d0_s6" value="5-lobed" value_original="5-lobed" />
      </biological_entity>
      <biological_entity id="o19824" name="tube" name_original="tubes" src="d0_s6" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s6" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens 3–5.</text>
      <biological_entity id="o19825" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o19826" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fruits with 5 low ribs, obovoid or narrowly obovate and tapering at both ends, base prominently constricted, apex rounded to nipplelike, surface of fruit slightly rugose to prominently tuberculate on ribs and/or sulci (or ribs and/or sulci without tubercles), glabrous or pubescent, mucilaginous when wetted.</text>
      <biological_entity id="o19827" name="fruit" name_original="fruits" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="obovate" value_original="obovate" />
        <character constraint="at ends" constraintid="o19829" is_modifier="false" name="shape" src="d0_s8" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o19828" name="rib" name_original="ribs" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="5" value_original="5" />
        <character is_modifier="true" name="position" src="d0_s8" value="low" value_original="low" />
      </biological_entity>
      <biological_entity id="o19829" name="end" name_original="ends" src="d0_s8" type="structure" />
      <biological_entity id="o19830" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="prominently" name="size" src="d0_s8" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o19831" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="nipplelike" value_original="nipplelike" />
      </biological_entity>
      <biological_entity id="o19832" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="on ribs" constraintid="o19834" from="slightly rugose" name="relief" src="d0_s8" to="prominently tuberculate" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s8" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
      <biological_entity id="o19833" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
      <biological_entity id="o19834" name="rib" name_original="ribs" src="d0_s8" type="structure" />
      <biological_entity id="o19835" name="sulcus" name_original="sulci" src="d0_s8" type="structure" />
      <relation from="o19827" id="r2883" name="with" negation="false" src="d0_s8" to="o19828" />
      <relation from="o19832" id="r2884" name="part_of" negation="false" src="d0_s8" to="o19833" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, Mexico, Central America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7e.</number>
  <other_name type="past_name">Eu-Oxybaphus</other_name>
  <discussion>Species ca. 25 (10 in the flora).</discussion>
  <discussion>Mirabilis sect. Oxybaphus is most diverse in the warm, semiarid regions of North America. Of all the subgroups (i.e., genera in P. C. Standley 1918) within a broadly constructed Mirabilis, sect. Oxybaphus has been the last to be maintained at the generic level by some authors of regional floras. The markedly accrescent involucres of most species make members of the section easily recognized. Standley (1931), in making transfers from Allionia and Oxybaphus to Mirabilis, noted that characteristics supposedly distinctive between segregate genera are not so in South America. C. F. Reed (1969) also used this justification for an inclusive Mirabilis in a treatment of Texas species.</discussion>
  <discussion>Taxonomy at the species level within the section is unusually problematic. In the introduction to his treatment for Mirabilis, L. H. Shinners (1951b) expressed biological and nomen-clatural problems that plague satisfactory classification. R. W. Cruden (1973) showed cleistogamic and chasmogamic flowers in M. nyctaginea, and self-compatibility in chasmogamic flowers, and R. Spellenberg (1998) mentioned other biological attributes that probably contribute to difficulties. The floral mechanism that Cruden described for M. nyctaginea (stamens and style curling together as the flowers close in late morning) is found in all species and ensures self-pollination. Cleistogamic flowers are found in many other species. Chasmogamic flowers are attractive in some species and are visited by various strong-flying pollinators, among them bees and hummingbirds. B. L. Turner (1993b) used the argument of cleistogamy and hybridization, combined with phenotypic plasticity, to support his concept of a widespread and variable M. albida. Ultimately, some species complexes in the section may be treated more satisfactorily with a number of broadly distributed and variable species, each consisting of a number of infraspecific taxa.</discussion>
  <discussion>Each species in this section intergrades in some respect with one or more others. Populations of these intergradient types are often highly uniform within. Here, insofar as practicable, species are recognized on combinations of characters that occur within some reasonably extensive geographic region and/or are found in populations that occur in certain rather well-defined ecological situations. Areas of difficult variation are the mountains of the south-western United States and the eastern base of the Rocky Mountains.</discussion>
  <discussion>Measurements for leaves in the following descriptions are for leaves from mid-stem area. Leaves located toward the base are commonly proportionately broader and on longer petioles; those near and in the inflorescence are proportionately narrower and have much shorter petioles or are sessile.</discussion>
  
</bio:treatment>