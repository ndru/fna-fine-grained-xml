<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">424</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">amaranthaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">amaranthus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Amaranthus</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">powellii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>10: 347. 1875</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amaranthaceae;genus amaranthus;subgenus amaranthus;species powellii;</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415662</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Amaranthus</taxon_name>
    <taxon_name authority="Uline &amp; W. L. Bray" date="unknown" rank="species">bracteosus</taxon_name>
    <taxon_hierarchy>genus Amaranthus;species bracteosus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Amaranthus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">retroflexus</taxon_name>
    <taxon_name authority="(S. Watson) B. Boivin" date="unknown" rank="variety">powellii</taxon_name>
    <taxon_hierarchy>genus Amaranthus;species retroflexus;variety powellii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants glabrous or moderately pubescent toward inflorescences, becoming glabrescent at maturity.</text>
      <biological_entity id="o8873" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character constraint="toward inflorescences" constraintid="o8874" is_modifier="false" modifier="moderately" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character constraint="at maturity" is_modifier="false" modifier="becoming" name="pubescence" notes="" src="d0_s0" value="glabrescent" value_original="glabrescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8874" name="inflorescence" name_original="inflorescences" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems usually erect, green or sometimes reddish purple, branched, mainly in inflorescences, to nearly simple, 0.3–1.5 (–2) m, stiff.</text>
      <biological_entity id="o8875" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character name="coloration" src="d0_s1" value="sometimes" value_original="sometimes" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish purple" value_original="reddish purple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="nearly" name="architecture" notes="" src="d0_s1" value="simple" value_original="simple" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s1" to="2" to_unit="m" />
        <character char_type="range_value" from="0.3" from_unit="m" name="some_measurement" src="d0_s1" to="1.5" to_unit="m" />
        <character is_modifier="false" name="fragility" src="d0_s1" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o8876" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure" />
      <relation from="o8875" id="r1261" modifier="mainly" name="in" negation="false" src="d0_s1" to="o8876" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole mostly equaling or longer than blade;</text>
      <biological_entity id="o8877" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o8878" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="variability" src="d0_s2" value="equaling" value_original="equaling" />
        <character constraint="than blade" constraintid="o8879" is_modifier="false" name="length_or_size" src="d0_s2" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o8879" name="blade" name_original="blade" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>blade rhombic-ovate to broadly lanceolate, 4–8 × 2–3 cm, occasionally larger in robust plants, base cuneate to broadly cuneate, margins entire, apex cuneate to obtuse or indistinctly emarginate, with mucro.</text>
      <biological_entity id="o8880" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o8881" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="rhombic-ovate" name="shape" src="d0_s3" to="broadly lanceolate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s3" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s3" to="3" to_unit="cm" />
        <character constraint="in plants" constraintid="o8882" is_modifier="false" modifier="occasionally" name="size" src="d0_s3" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o8882" name="plant" name_original="plants" src="d0_s3" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s3" value="robust" value_original="robust" />
      </biological_entity>
      <biological_entity id="o8883" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s3" to="broadly cuneate" />
      </biological_entity>
      <biological_entity id="o8884" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o8885" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s3" to="obtuse or indistinctly emarginate" />
      </biological_entity>
      <biological_entity id="o8886" name="mucro" name_original="mucro" src="d0_s3" type="structure" />
      <relation from="o8885" id="r1262" name="with" negation="false" src="d0_s3" to="o8886" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences mostly terminal, usually with spikes at distal axils, erect and rigid, green to silvery green, occasionally tinged red, leafless at least distally.</text>
      <biological_entity id="o8887" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="texture" src="d0_s4" value="rigid" value_original="rigid" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s4" to="silvery green" />
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s4" value="tinged red" value_original="tinged red" />
        <character is_modifier="false" modifier="at-least distally; distally" name="architecture" src="d0_s4" value="leafless" value_original="leafless" />
      </biological_entity>
      <biological_entity id="o8888" name="spike" name_original="spikes" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o8889" name="axil" name_original="axils" src="d0_s4" type="structure" />
      <relation from="o8887" id="r1263" modifier="usually" name="with" negation="false" src="d0_s4" to="o8888" />
      <relation from="o8888" id="r1264" name="at" negation="false" src="d0_s4" to="o8889" />
    </statement>
    <statement id="d0_s5">
      <text>Bracts lanceolate to linear-subulate, 4–7 mm, 2–3 times as long as tepals, rigid.</text>
      <biological_entity id="o8890" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="linear-subulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
        <character constraint="tepal" constraintid="o8891" is_modifier="false" name="length" src="d0_s5" value="2-3 times as long as tepals" />
        <character is_modifier="false" name="texture" src="d0_s5" value="rigid" value_original="rigid" />
      </biological_entity>
      <biological_entity id="o8891" name="tepal" name_original="tepals" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Pistillate flowers: tepals usually 3–5, not clawed, unequal;</text>
      <biological_entity id="o8892" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8893" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="5" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="clawed" value_original="clawed" />
        <character is_modifier="false" name="size" src="d0_s6" value="unequal" value_original="unequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>outer tepals narrowly ovate-elliptic or elliptic, 1.5–3.5 mm, apex aristate;</text>
      <biological_entity id="o8894" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o8895" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8896" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="aristate" value_original="aristate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>style-branches spreading, shorter than body of fruit;</text>
      <biological_entity id="o8897" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8898" name="style-branch" name_original="style-branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character constraint="than body" constraintid="o8899" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o8899" name="body" name_original="body" src="d0_s8" type="structure" />
      <biological_entity id="o8900" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
      <relation from="o8899" id="r1265" name="part_of" negation="false" src="d0_s8" to="o8900" />
    </statement>
    <statement id="d0_s9">
      <text>stigmas 3.</text>
      <biological_entity id="o8901" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8902" name="stigma" name_original="stigmas" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Staminate flowers clustered at tips of inflorescence branches;</text>
      <biological_entity id="o8903" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
        <character constraint="at tips" constraintid="o8904" is_modifier="false" name="arrangement_or_growth_form" src="d0_s10" value="clustered" value_original="clustered" />
      </biological_entity>
      <biological_entity id="o8904" name="tip" name_original="tips" src="d0_s10" type="structure" />
      <biological_entity constraint="inflorescence" id="o8905" name="branch" name_original="branches" src="d0_s10" type="structure" />
      <relation from="o8904" id="r1266" name="part_of" negation="false" src="d0_s10" to="o8905" />
    </statement>
    <statement id="d0_s11">
      <text>tepals 3–5;</text>
      <biological_entity id="o8906" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s11" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 3–5.</text>
      <biological_entity id="o8907" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s12" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Utricles subglobose or compressed-ovoid, 2–3 mm, equaling or shorter than tepals, smooth or lid slightly rugose or minutely verrucose, dehiscence regularly circumscissile.</text>
      <biological_entity id="o8908" name="utricle" name_original="utricles" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s13" value="compressed-ovoid" value_original="compressed-ovoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s13" value="equaling" value_original="equaling" />
        <character constraint="than tepals" constraintid="o8909" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o8909" name="tepal" name_original="tepals" src="d0_s13" type="structure" />
      <biological_entity id="o8910" name="lid" name_original="lid" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s13" value="rugose" value_original="rugose" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s13" value="verrucose" value_original="verrucose" />
        <character is_modifier="false" modifier="regularly" name="dehiscence" src="d0_s13" value="circumscissile" value_original="circumscissile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds black, subglobose to lenticular, 1–1.4 mm diam., smooth, shiny.</text>
      <biological_entity id="o8911" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="black" value_original="black" />
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s14" to="lenticular" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s14" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reflectance" src="d0_s14" value="shiny" value_original="shiny" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed habitats, agricultural fields, railroads, roadsides, waste areas, banks of rivers, lakes, and streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed habitats" />
        <character name="habitat" value="agricultural fields" />
        <character name="habitat" value="railroads" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="waste areas" />
        <character name="habitat" value="banks" constraint="of rivers , lakes , and streams" />
        <character name="habitat" value="rivers" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Ont., P.E.I., Que., Sask.; Ariz., Ark., Calif., Colo., Conn., Fla., Idaho, Ill., Ind., Kans., Ky., La., Maine, Mass., Mich., Minn., Miss., Mo., Mont., Nev., N.H., N.J., N.Mex., N.Y., N.C., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; Mexico; introduced or naturalized in South America, Eurasia, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="or naturalized in South America" establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <other_name type="past_name">Amarantus</other_name>
  <other_name type="common_name">Powell’s amaranth</other_name>
  <other_name type="common_name">green amaranth</other_name>
  <other_name type="common_name">Powell’s smooth amaranth</other_name>
  <discussion>Amaranthus powellii is originally native to southwestern United States and adjacent regions of Mexico; now, it is widely naturalized almost everywhere in temperate regions of North America. The distribution of A. powellii is probably underestimated both in North America and the Old World, and literature references are somewhat confusing, because A. powellii has been commonly confused with A. hybridus.</discussion>
  <discussion>Forms of Amaranthus powellii with indehiscent or occasionally irregularly dehiscent utricles were described from Europe (southwestern France, the Gironde estuary) as A. bouchonii Thellung. Similar forms occasionally occur in North America. According to J. M. Tucker and J. D. Sauer (1958) and J. D. Sauer (1967b, 1972b), they are mostly “mutant or aberrant forms” of A. powellii, or hybrids of A. powellii and/or A. hybridus with other species. Recent comparative studies of morphology and isozymes of A. bouchonii (P. Wilkin 1992) indicated that that taxon, whatever its origin was, now differs from its presumably parental species and probably deserves recognition, at least as a separate subspecies. It seems that in North America, the situation with indehiscent-fruited forms is much more complicated than in Europe, and multiple entities are involved, including deviate forms of A. powellii and also partly sterile hybrids of dioecious taxa with species belonging to the A. hybridus group. The formal recognition of A. bouchonii in North American material would be premature.</discussion>
  <discussion>The names Amaranthus hybridus, A. chlorostachys Willdenow, and A. hybridus subsp. chlorostachys (Willdenow) Hejný were occasionally misapplied to A. powellii in North America and Europe.</discussion>
  
</bio:treatment>