<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">6</other_info_on_meta>
    <other_info_on_meta type="mention_page">9</other_info_on_meta>
    <other_info_on_meta type="treatment_page">8</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">phytolaccaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">phytolacca</taxon_name>
    <taxon_name authority="H. Walter in H. G. A. Engler" date="1909" rank="species">heterotepala</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler, Pflanzenr.</publication_title>
      <place_in_publication>83[IV,39]: 51. 1909</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phytolaccaceae;genus phytolacca;species heterotepala</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242415004</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 2.5 m.</text>
      <biological_entity id="o4194" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="2.5" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole to 5 cm;</text>
      <biological_entity id="o4195" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o4196" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade lanceolate to ovate, to 13 × 6 cm, base obtuse, apex acute to acute-mucronate.</text>
      <biological_entity id="o4197" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o4198" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="ovate" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s2" to="13" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s2" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4199" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o4200" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="acute-mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Racemes open, mostly axillary, to 25 cm;</text>
      <biological_entity id="o4201" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
        <character is_modifier="false" modifier="mostly" name="position" src="d0_s3" value="axillary" value_original="axillary" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>peduncle to 5.5 cm;</text>
      <biological_entity id="o4202" name="peduncle" name_original="peduncle" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="5.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pedicel 2–5 mm.</text>
      <biological_entity id="o4203" name="pedicel" name_original="pedicel" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals 5 (–8), greenish, oblong, strikingly unequal, largest ca. twice as wide as smallest, 3–4 × 1.5–2.2 mm;</text>
      <biological_entity id="o4204" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o4205" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="8" />
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="strikingly" name="size" src="d0_s6" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="size" src="d0_s6" value="largest" value_original="largest" />
        <character is_modifier="false" name="width" src="d0_s6" value="2 times as wide as smallest" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens 15–22, usually in 2 whorls;</text>
      <biological_entity id="o4206" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o4207" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s7" to="22" />
      </biological_entity>
      <biological_entity id="o4208" name="whorl" name_original="whorls" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <relation from="o4207" id="r555" modifier="usually" name="in" negation="false" src="d0_s7" to="o4208" />
    </statement>
    <statement id="d0_s8">
      <text>carpels 8–11, connate;</text>
      <biological_entity id="o4209" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o4210" name="carpel" name_original="carpels" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s8" to="11" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary 8–11-loculed.</text>
      <biological_entity id="o4211" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o4212" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="8-11-loculed" value_original="8-11-loculed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Berries purple-black, 6–7 mm diam.</text>
      <biological_entity id="o4213" name="berry" name_original="berries" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="purple-black" value_original="purple-black" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds black, lenticular, 2–2.5 mm, shiny.</text>
      <biological_entity id="o4214" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s11" value="lenticular" value_original="lenticular" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s11" value="shiny" value_original="shiny" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–winter.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="winter" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed ground</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed ground" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10-60 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="60" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; Mexico; introduced in Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="in Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Mexican pokeweed</other_name>
  <discussion>Phytolacca heterotepala was first reported in the flora area by J. T. Howell (1960) on the basis of California (San Francisco) collections. Its current status in the flora is uncertain; J. C. Hickman (1993) recorded it as “probably not naturalized.” The few specimens we have seen possess in greater-or-lesser degree those features cited by H. P. H. Walter (1909) in his description of this taxon. The sepals are strikingly unequal, the largest being up to twice as wide as the smallest, and the stamens appearing to be in two whorls. The status of the species itself is in need of further investigation.</discussion>
  <references>
    <reference>Howell, J. T. 1960. A Mexican pokeberry in San Francisco, California. Leafl. W. Bot. 9: 81–83.</reference>
  </references>
  
</bio:treatment>