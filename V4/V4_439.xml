<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Allan D. Zimmerman,Bruce D. Parfitt</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">96</other_info_on_meta>
    <other_info_on_meta type="mention_page">212</other_info_on_meta>
    <other_info_on_meta type="mention_page">221</other_info_on_meta>
    <other_info_on_meta type="mention_page">238</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Scheidweiler" date="1838" rank="genus">ARIOCARPUS</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Acad. Roy. Sci. Bruxelles</publication_title>
      <place_in_publication>5: 491, figs. 1–5. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus ariocarpus;</taxon_hierarchy>
    <other_info_on_name type="etymology">The genus Aria and Greek karpos, fruit, referring to the Aria -like fruit</other_info_on_name>
    <other_info_on_name type="fna_id">102586</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Castañeda" date="unknown" rank="genus">Neogomesia</taxon_name>
    <taxon_hierarchy>genus Neogomesia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="A. Berger" date="unknown" rank="genus">Roseocactus</taxon_name>
    <taxon_hierarchy>genus Roseocactus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect, unbranched [or branched], deep-seated in the substrate [or somewhat subterranean for whole seasons].</text>
      <biological_entity id="o9585" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character constraint="in substrate" constraintid="o9586" is_modifier="false" name="location" src="d0_s0" value="deep-seated" value_original="deep-seated" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o9586" name="substrate" name_original="substrate" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Roots taproots.</text>
      <biological_entity constraint="roots" id="o9587" name="taproot" name_original="taproots" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Stem unsegmented, gray-green (yellow-green or purplish with age or stress), aboveground portion flat, concave, or weakly hemispheric, usually flush with soil surface and cryptic, strongly tuberculate, 0–2 (–10) × [3–] 5–10 (–15) cm, hard, rigid, tough skinned [thin skinned in A. agavoides of Mexico];</text>
      <biological_entity id="o9588" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="unsegmented" value_original="unsegmented" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray-green" value_original="gray-green" />
      </biological_entity>
      <biological_entity id="o9589" name="portion" name_original="portion" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aboveground" value_original="aboveground" />
        <character is_modifier="false" name="shape" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s2" value="concave" value_original="concave" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s2" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s2" value="concave" value_original="concave" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s2" value="hemispheric" value_original="hemispheric" />
        <character constraint="with soil, surface" constraintid="o9590, o9591" is_modifier="false" modifier="usually" name="prominence" src="d0_s2" value="flush" value_original="flush" />
        <character is_modifier="false" name="prominence" notes="" src="d0_s2" value="cryptic" value_original="cryptic" />
        <character is_modifier="false" modifier="strongly" name="relief" src="d0_s2" value="tuberculate" value_original="tuberculate" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s2" to="2" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_width" src="d0_s2" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s2" to="10" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s2" value="hard" value_original="hard" />
        <character is_modifier="false" name="texture" src="d0_s2" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="fragility" src="d0_s2" value="tough" value_original="tough" />
      </biological_entity>
      <biological_entity id="o9590" name="soil" name_original="soil" src="d0_s2" type="structure" />
      <biological_entity id="o9591" name="surface" name_original="surface" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>tubercles arranged in rosettes or mosaics, ± triangular, 8–20 [–60] × [3–] 11–25 mm, hard, exposed faces of tubercles strongly differentiated from sides [except in some Mexican species], prominently fissured [wrinkled, roughened, or nearly smooth];</text>
      <biological_entity id="o9592" name="tubercle" name_original="tubercles" src="d0_s3" type="structure">
        <character constraint="in mosaics" constraintid="o9594" is_modifier="false" name="arrangement" src="d0_s3" value="arranged" value_original="arranged" />
        <character is_modifier="false" modifier="more or less" name="shape" notes="" src="d0_s3" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="60" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s3" to="11" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="11" from_unit="mm" name="width" src="d0_s3" to="25" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="hard" value_original="hard" />
      </biological_entity>
      <biological_entity id="o9593" name="rosette" name_original="rosettes" src="d0_s3" type="structure" />
      <biological_entity id="o9594" name="mosaic" name_original="mosaics" src="d0_s3" type="structure" />
      <biological_entity id="o9595" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s3" value="exposed" value_original="exposed" />
        <character constraint="from sides" constraintid="o9597" is_modifier="false" modifier="strongly" name="variability" src="d0_s3" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" modifier="prominently" name="relief" notes="" src="d0_s3" value="fissured" value_original="fissured" />
      </biological_entity>
      <biological_entity id="o9596" name="tubercle" name_original="tubercles" src="d0_s3" type="structure" />
      <biological_entity id="o9597" name="side" name_original="sides" src="d0_s3" type="structure" />
      <relation from="o9595" id="r1373" name="part_of" negation="false" src="d0_s3" to="o9596" />
    </statement>
    <statement id="d0_s4">
      <text>areoles elongate [circular and axillary, circular and subapical, or 2-parted], forming a wide woolly groove on each tubercle;</text>
      <biological_entity id="o9598" name="areole" name_original="areoles" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o9599" name="groove" name_original="groove" src="d0_s4" type="structure">
        <character is_modifier="true" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character is_modifier="true" name="pubescence" src="d0_s4" value="woolly" value_original="woolly" />
      </biological_entity>
      <biological_entity id="o9600" name="tubercle" name_original="tubercle" src="d0_s4" type="structure" />
      <relation from="o9598" id="r1374" name="forming a" negation="false" src="d0_s4" to="o9599" />
      <relation from="o9598" id="r1375" name="on" negation="false" src="d0_s4" to="o9600" />
    </statement>
    <statement id="d0_s5">
      <text>areolar glands absent;</text>
      <biological_entity constraint="areolar" id="o9601" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cortex and pith not mucilaginous, “mucilage” restricted to elongate cavities.</text>
      <biological_entity id="o9602" name="cortex" name_original="cortex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="coating" src="d0_s6" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
      <biological_entity id="o9603" name="pith" name_original="pith" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="coating" src="d0_s6" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
      <biological_entity id="o9604" name="mucilage" name_original="mucilage" src="d0_s6" type="structure" />
      <biological_entity id="o9605" name="cavity" name_original="cavities" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="elongate" value_original="elongate" />
      </biological_entity>
      <relation from="o9604" id="r1376" name="restricted to" negation="false" src="d0_s6" to="o9605" />
    </statement>
    <statement id="d0_s7">
      <text>Spines absent [sporadic and rudimentary in some Mexican taxa].</text>
      <biological_entity id="o9606" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers diurnal, borne in axils of tubercles near stem apex, broadly funnelform to almost salverform, 1.5–5 × 1.5–5 cm;</text>
      <biological_entity id="o9607" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="diurnal" value_original="diurnal" />
        <character is_modifier="false" modifier="broadly; almost" name="shape" src="d0_s8" value="salverform" value_original="salverform" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s8" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s8" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9608" name="axil" name_original="axils" src="d0_s8" type="structure" />
      <biological_entity id="o9609" name="tubercle" name_original="tubercles" src="d0_s8" type="structure" />
      <biological_entity constraint="stem" id="o9610" name="apex" name_original="apex" src="d0_s8" type="structure" />
      <relation from="o9607" id="r1377" name="borne in" negation="false" src="d0_s8" to="o9608" />
      <relation from="o9607" id="r1378" name="borne in" negation="false" src="d0_s8" to="o9609" />
      <relation from="o9607" id="r1379" name="borne in" negation="false" src="d0_s8" to="o9610" />
    </statement>
    <statement id="d0_s9">
      <text>outer tepals brownish or greenish with pink tinge, 12–35 × 5–9 mm, margins entire;</text>
      <biological_entity constraint="outer" id="o9611" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="greenish with pink tinge" value_original="greenish with pink tinge" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s9" to="35" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9612" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>inner tepals pink or magenta [white or yellow], 13–34 × 4–10 mm, margins entire;</text>
      <biological_entity constraint="inner" id="o9613" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="magenta" value_original="magenta" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s10" to="34" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9614" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary smooth (scales, hairs, and spines absent);</text>
      <biological_entity id="o9615" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigma lobes 5–10, white, 1.2–5 mm.</text>
      <biological_entity constraint="stigma" id="o9616" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="10" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits indehiscent (very rarely explosively dehiscent), white or cream to pale greenish [reddish], spheric to clavate or cylindric, proximally or almost completely buried in copious wool of stem apex, 10–25 × 5–10 mm, initially fleshy, drying and collapsing a few days after ripening, scales and spines absent;</text>
      <biological_entity id="o9617" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s13" value="indehiscent" value_original="indehiscent" />
        <character char_type="range_value" from="cream" name="coloration" src="d0_s13" to="pale greenish" />
        <character char_type="range_value" from="spheric" name="shape" src="d0_s13" to="clavate or cylindric" />
        <character constraint="in wool" constraintid="o9618" is_modifier="false" modifier="proximally; almost completely" name="location" src="d0_s13" value="buried" value_original="buried" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" notes="" src="d0_s13" to="25" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" notes="" src="d0_s13" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="initially" name="texture" src="d0_s13" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="condition" src="d0_s13" value="drying" value_original="drying" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="collapsing" value_original="collapsing" />
        <character constraint="after spines" constraintid="o9621" is_modifier="false" name="quantity" src="d0_s13" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o9618" name="wool" name_original="wool" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="copious" value_original="copious" />
      </biological_entity>
      <biological_entity constraint="stem" id="o9619" name="apex" name_original="apex" src="d0_s13" type="structure" />
      <biological_entity id="o9620" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s13" value="ripening" value_original="ripening" />
      </biological_entity>
      <biological_entity id="o9621" name="spine" name_original="spines" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o9618" id="r1380" name="part_of" negation="false" src="d0_s13" to="o9619" />
    </statement>
    <statement id="d0_s14">
      <text>pulp white to pale greenish;</text>
      <biological_entity id="o9622" name="pulp" name_original="pulp" src="d0_s14" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s14" to="pale greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>floral remnant apparently persistent.</text>
      <biological_entity constraint="floral" id="o9623" name="remnant" name_original="remnant" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="apparently" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds black, spheric to obovoid, 1.2–1.6 (–2.5) mm, minutely tuberculate, shiny;</text>
      <biological_entity id="o9624" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="black" value_original="black" />
        <character char_type="range_value" from="spheric" name="shape" src="d0_s16" to="obovoid" />
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s16" to="1.6" to_unit="mm" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s16" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" name="reflectance" src="d0_s16" value="shiny" value_original="shiny" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>testa cells strongly convex (conspicuous with lens).</text>
    </statement>
    <statement id="d0_s18">
      <text>x = 11.</text>
      <biological_entity constraint="testa" id="o9625" name="cell" name_original="cells" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s17" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="x" id="o9626" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Arid regions, sw United States, Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Arid regions" establishment_means="native" />
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>31.</number>
  <discussion>Species 6 (1 in the flora).</discussion>
  <discussion>Ariocarpus species mostly grow in broken rock substrate and closely mimic it. Some Mexican species display additional adaptations, e.g., A. kotschoubeyanus (K. Schumann) K. Schumann withdraws into seasonally inundated, fine lacustrine soil and can be completely buried between growing seasons.</discussion>
  <references>
    <reference>Anderson, E. F. 1965. A taxonomic revision of Ariocarpus. Cact. Succ. J. (Los Angeles) 37: 39–49.</reference>
    <reference>Anderson, E. F. and W. A. Fitz Maurice. 1997. Ariocarpus revisited. Haseltonia 5: 1–20.</reference>
  </references>
  
</bio:treatment>