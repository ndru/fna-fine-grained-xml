<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">407</other_info_on_meta>
    <other_info_on_meta type="treatment_page">409</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">amaranthaceae</taxon_name>
    <taxon_name authority="Reichenbach" date="1828" rank="genus">hermbstaedtia</taxon_name>
    <taxon_name authority="(Burchell ex Moquin-Tandon) T. Cooke in W. H. Harvey et al." date="1910" rank="species">odorata</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Harvey et al., Fl. Cap.</publication_title>
      <place_in_publication>5(1): 407. 1910</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amaranthaceae;genus hermbstaedtia;species odorata</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415645</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Celosia</taxon_name>
    <taxon_name authority="Burchell ex Moquin-Tandon" date="unknown" rank="species">odorata</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>13(2): 244. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Celosia;species odorata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems branched distally, pubescent.</text>
      <biological_entity id="o1538" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves scattered on stem;</text>
      <biological_entity id="o1539" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character constraint="on stem" constraintid="o1540" is_modifier="false" name="arrangement" src="d0_s1" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity id="o1540" name="stem" name_original="stem" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>blade 1–2 cm × 1–1.5 mm.</text>
      <biological_entity id="o1541" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s2" to="2" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences elongate spikes;</text>
      <biological_entity id="o1542" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o1543" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts and bracteoles 2.5–5 mm, membranous.</text>
      <biological_entity id="o1544" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o1545" name="bracteole" name_original="bracteoles" src="d0_s4" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers white or pinkish;</text>
      <biological_entity id="o1546" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="pinkish" value_original="pinkish" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>tepals 3–5-veined at base, equal, 4–7 mm;</text>
      <biological_entity id="o1547" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character constraint="at base" constraintid="o1548" is_modifier="false" name="architecture" src="d0_s6" value="3-5-veined" value_original="3-5-veined" />
        <character is_modifier="false" name="variability" notes="" src="d0_s6" value="equal" value_original="equal" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1548" name="base" name_original="base" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>stamens 5;</text>
      <biological_entity id="o1549" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>ovary ovoid.</text>
      <biological_entity id="o1550" name="ovary" name_original="ovary" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="ovoid" value_original="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Utricles enclosed by staminal tube, 3–4 mm.</text>
      <biological_entity id="o1551" name="utricle" name_original="utricles" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="staminal" id="o1552" name="tube" name_original="tube" src="d0_s9" type="structure" />
      <relation from="o1551" id="r201" name="enclosed by" negation="false" src="d0_s9" to="o1552" />
    </statement>
    <statement id="d0_s10">
      <text>Seeds lenticular, ± circular in outline, 1 mm diam., glossy.</text>
      <biological_entity id="o1553" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="lenticular" value_original="lenticular" />
        <character constraint="in outline" constraintid="o1554" is_modifier="false" modifier="more or less" name="arrangement_or_shape" src="d0_s10" value="circular" value_original="circular" />
        <character name="diameter" notes="" src="d0_s10" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="reflectance" src="d0_s10" value="glossy" value_original="glossy" />
      </biological_entity>
      <biological_entity id="o1554" name="outline" name_original="outline" src="d0_s10" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Manganese ore piles</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ore piles" modifier="manganese" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Md.; South Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" value="South Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Guineaflower</other_name>
  <discussion>See C. F. Reed (1961) for evidently the only known occurrence of this species in the flora.</discussion>
  
</bio:treatment>