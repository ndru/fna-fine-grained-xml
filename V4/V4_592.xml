<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Leila M. Shultz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">260</other_info_on_meta>
    <other_info_on_meta type="treatment_page">308</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">AXYRIS</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 979. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 420. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus AXYRIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek axyros (a, not, and xyrios, razor), blunt, not cutting, in reference to the mild taste</other_info_on_name>
    <other_info_on_name type="fna_id">103273</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, monoecious, covered with rusty-colored, stellate, and whitish simple trichomes.</text>
      <biological_entity id="o19531" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o19532" name="trichome" name_original="trichomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="rusty-colored" value_original="rusty-colored" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s0" value="stellate" value_original="stellate" />
        <character is_modifier="true" name="coloration" src="d0_s0" value="whitish" value_original="whitish" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="simple" value_original="simple" />
      </biological_entity>
      <relation from="o19531" id="r2844" name="covered with" negation="false" src="d0_s0" to="o19532" />
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending, not jointed or armed, slender.</text>
      <biological_entity id="o19533" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="jointed" value_original="jointed" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="armed" value_original="armed" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, petiolate;</text>
      <biological_entity id="o19534" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade ovatelanceolate to narrowly lanceolate, base tapered, margins entire, plane or revolute, apex acute to acuminate.</text>
      <biological_entity id="o19535" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s3" to="narrowly lanceolate" />
      </biological_entity>
      <biological_entity id="o19536" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o19537" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s3" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o19538" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences glomerules, cymes, or flowers solitary.</text>
      <biological_entity constraint="inflorescences" id="o19539" name="glomerule" name_original="glomerules" src="d0_s4" type="structure" />
      <biological_entity id="o19540" name="cyme" name_original="cymes" src="d0_s4" type="structure" />
      <biological_entity id="o19541" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Staminate flowers in terminal glomerules or at end of pistillate cymes.</text>
      <biological_entity id="o19542" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o19543" name="end" name_original="end" src="d0_s5" type="structure" />
      <biological_entity id="o19544" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <relation from="o19542" id="r2845" name="in terminal glomerules or at" negation="false" src="d0_s5" to="o19543" />
      <relation from="o19543" id="r2846" name="part_of" negation="false" src="d0_s5" to="o19544" />
    </statement>
    <statement id="d0_s6">
      <text>Pistillate flowers solitary in upper leaf-axils or forming cymes commingled with staminate flowers.</text>
      <biological_entity id="o19545" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character constraint="in " constraintid="o19547" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="upper" id="o19546" name="leaf-axil" name_original="leaf-axils" src="d0_s6" type="structure" />
      <biological_entity id="o19547" name="cyme" name_original="cymes" src="d0_s6" type="structure" />
      <biological_entity id="o19548" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
      </biological_entity>
      <relation from="o19547" id="r2847" name="commingled with" negation="false" src="d0_s6" to="o19548" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers unisexual;</text>
    </statement>
    <statement id="d0_s8">
      <text>staminate with perianth segments 3–5, stamens 2–5;</text>
      <biological_entity id="o19549" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="unisexual" value_original="unisexual" />
        <character constraint="with perianth segments" constraintid="o19550" is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="perianth" id="o19550" name="segment" name_original="segments" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pistillate with bracteoles 2, perianth segments 3–4, stigmas 2, filiform.</text>
      <biological_entity id="o19551" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s8" to="5" />
        <character constraint="with bracteoles" constraintid="o19552" is_modifier="false" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o19552" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="perianth" id="o19553" name="segment" name_original="segments" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s9" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Fruiting structures persistent, accrescent perianth surrounding utricle;</text>
      <biological_entity id="o19554" name="stigma" name_original="stigmas" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s9" value="filiform" value_original="filiform" />
      </biological_entity>
      <biological_entity id="o19555" name="structure" name_original="structures" src="d0_s10" type="structure" />
      <biological_entity id="o19556" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="persistent" value_original="persistent" />
        <character is_modifier="true" name="size" src="d0_s10" value="accrescent" value_original="accrescent" />
      </biological_entity>
      <biological_entity id="o19557" name="utricle" name_original="utricle" src="d0_s10" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s10" value="surrounding" value_original="surrounding" />
      </biological_entity>
      <relation from="o19554" id="r2848" name="fruiting" negation="false" src="d0_s10" to="o19555" />
    </statement>
    <statement id="d0_s11">
      <text>utricles winged, obovate to cuneate, laterally compressed;</text>
      <biological_entity id="o19558" name="utricle" name_original="utricles" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="winged" value_original="winged" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s11" to="cuneate" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s11" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pericarp adherent.</text>
      <biological_entity id="o19559" name="pericarp" name_original="pericarp" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="adherent" value_original="adherent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds vertical, ovoid;</text>
      <biological_entity id="o19560" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="vertical" value_original="vertical" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>seed-coat grayish, granular;</text>
      <biological_entity id="o19561" name="seed-coat" name_original="seed-coat" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="grayish" value_original="grayish" />
        <character is_modifier="false" name="pubescence_or_relief_or_texture" src="d0_s14" value="granular" value_original="granular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>embryo horseshoe-shaped;</text>
      <biological_entity id="o19562" name="embryo" name_original="embryo" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="horseshoe--shaped" value_original="horseshoe--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>perisperm copious.</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 9.</text>
      <biological_entity id="o19563" name="perisperm" name_original="perisperm" src="d0_s16" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s16" value="copious" value_original="copious" />
      </biological_entity>
      <biological_entity constraint="x" id="o19564" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Eurasia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Russian pigweed</other_name>
  <discussion>Species ca. 5 (1 in the flora).</discussion>
  
</bio:treatment>