<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">161</other_info_on_meta>
    <other_info_on_meta type="mention_page">168</other_info_on_meta>
    <other_info_on_meta type="treatment_page">167</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Engelmann in F. A.Wislizenus" date="1848" rank="genus">echinocereus</taxon_name>
    <taxon_name authority="Peebles" date="1936" rank="species">ledingii</taxon_name>
    <place_of_publication>
      <publication_title>Cact. Succ. J. (Los Angeles)</publication_title>
      <place_in_publication>8: 35, figs. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus echinocereus;species ledingii;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415251</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinocereus</taxon_name>
    <taxon_name authority="(Engelmann) Sencke ex J. N. Haage" date="unknown" rank="species">fendleri</taxon_name>
    <taxon_name authority="(Peebles) N. P. Taylor" date="unknown" rank="variety">ledingii</taxon_name>
    <taxon_hierarchy>genus Echinocereus;species fendleri;variety ledingii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 4–15-branched, forming rather open clumps.</text>
      <biological_entity id="o17940" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="4-15-branched" value_original="4-15-branched" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o17941" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="rather" name="architecture" src="d0_s0" value="open" value_original="open" />
      </biological_entity>
      <relation from="o17940" id="r2582" name="forming" negation="false" src="d0_s0" to="o17941" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, cylindric, 15–50 × 6–8 cm;</text>
      <biological_entity id="o17942" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s1" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s1" to="50" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="width" src="d0_s1" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>ribs 12–14 (–16), crests slightly undulate;</text>
      <biological_entity id="o17943" name="rib" name_original="ribs" src="d0_s2" type="structure">
        <character char_type="range_value" from="14" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="16" />
        <character char_type="range_value" from="12" name="quantity" src="d0_s2" to="14" />
      </biological_entity>
      <biological_entity id="o17944" name="crest" name_original="crests" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s2" value="undulate" value_original="undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>areoles 15–25 mm apart.</text>
      <biological_entity id="o17945" name="areole" name_original="areoles" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s3" to="25" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="apart" value_original="apart" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spines 10–16 per areole, mostly straight or central spines strongly curving down near base;</text>
      <biological_entity id="o17946" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o17947" from="10" name="quantity" src="d0_s4" to="16" />
      </biological_entity>
      <biological_entity id="o17947" name="areole" name_original="areole" src="d0_s4" type="structure" />
      <biological_entity constraint="central" id="o17948" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="mostly" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character constraint="near base" constraintid="o17949" is_modifier="false" modifier="strongly" name="course" src="d0_s4" value="curving" value_original="curving" />
      </biological_entity>
      <biological_entity id="o17949" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>radial spines 9–11 per areole, pale to golden yellow, aging darker colored or black, 12–15 mm;</text>
      <biological_entity id="o17950" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="radial" value_original="radial" />
        <character char_type="range_value" constraint="per areole" constraintid="o17951" from="9" name="quantity" src="d0_s5" to="11" />
        <character char_type="range_value" from="pale" name="coloration" notes="" src="d0_s5" to="golden yellow" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="darker colored" value_original="darker colored" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="black" value_original="black" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17951" name="areole" name_original="areole" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>central spines 1 (–5) per areole, all yellow or whitish, terete, 20–25 mm.</text>
      <biological_entity constraint="central" id="o17952" name="spine" name_original="spines" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="5" />
        <character constraint="per areole" constraintid="o17953" name="quantity" src="d0_s6" value="1" value_original="1" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="shape" src="d0_s6" value="terete" value_original="terete" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s6" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17953" name="areole" name_original="areole" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers 5–6 × 5–6 cm;</text>
      <biological_entity id="o17954" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s7" to="6" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s7" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>flower tube 15–20 × 15–25 mm;</text>
      <biological_entity constraint="flower" id="o17955" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s8" to="20" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s8" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>flower tube hairs 2 mm;</text>
      <biological_entity constraint="tube" id="o17956" name="hair" name_original="hairs" src="d0_s9" type="structure" constraint_original="flower tube">
        <character name="some_measurement" src="d0_s9" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>inner tepals magenta to rose-pink, darker proximally, 30 × 5–12 mm, tips relatively thin and delicate;</text>
      <biological_entity constraint="inner" id="o17957" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="magenta" name="coloration" src="d0_s10" to="rose-pink" />
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s10" value="darker" value_original="darker" />
        <character name="length" src="d0_s10" unit="mm" value="30" value_original="30" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17958" name="tip" name_original="tips" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="relatively" name="width" src="d0_s10" value="thin" value_original="thin" />
        <character is_modifier="false" name="fragility" src="d0_s10" value="delicate" value_original="delicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow;</text>
      <biological_entity id="o17959" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectar chamber 5–8 mm.</text>
      <biological_entity constraint="nectar" id="o17960" name="chamber" name_original="chamber" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits green, sometimes tinged reddish to brownish, 20–30 mm, pulp white.</text>
      <biological_entity id="o17961" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="green" value_original="green" />
        <character char_type="range_value" from="tinged reddish" modifier="sometimes" name="coloration" src="d0_s13" to="brownish" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s13" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = 22.</text>
      <biological_entity id="o17962" name="pulp" name_original="pulp" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17963" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun; fruiting 3 1/2 months after flowering.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky mountainsides, interior chaparral, oak woodland, igneous substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky mountainsides" />
        <character name="habitat" value="interior chaparral" />
        <character name="habitat" value="oak woodland" />
        <character name="habitat" value="igneous substrates" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <other_name type="common_name">Leding’s hedgehog cactus</other_name>
  <discussion>More than half of the localities formerly attributed to Echinocereus ledingii (H. Bravo-H. and H. Sánchez-M. 1998–1991, vol. 3; L. D. Benson 1969) are undocumented and highly suspect. The decurved or deflexed central spines, supposedly diagnostic for this rare species, are misleadingly duplicated in many individuals of E. coccineus and E. arizonicus, and only the latter two species have been found in most of the mountain ranges mapped by A. A. Nichol for L. D. Benson. Echinocereus ledingii is found in the Graham (Pinaleño), Santa Theresa, and Chiricahua mountains.</discussion>
  
</bio:treatment>