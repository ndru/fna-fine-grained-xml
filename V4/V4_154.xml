<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John S. Clement</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">16</other_info_on_meta>
    <other_info_on_meta type="treatment_page">74</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Aublet" date="1775" rank="genus">GUAPIRA</taxon_name>
    <place_of_publication>
      <publication_title>Hist. Pl. Guiane</publication_title>
      <place_in_publication>1: 308, plate 119 (as Quapira). 1775</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus GUAPIRA</taxon_hierarchy>
    <other_info_on_name type="etymology">Portugese guapirá , a Brazilian name more commonly applied to Avicennia s pecies</other_info_on_name>
    <other_info_on_name type="fna_id">114137</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Vellozo" date="unknown" rank="genus">Torrubia</taxon_name>
    <taxon_hierarchy>genus Torrubia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees or shrubs, perennial, from tuberous taproot.</text>
      <biological_entity id="o5436" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="tree" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o5438" name="taproot" name_original="taproot" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="tuberous" value_original="tuberous" />
      </biological_entity>
      <relation from="o5436" id="r749" name="from" negation="false" src="d0_s0" to="o5438" />
      <relation from="o5436" id="r750" name="from" negation="false" src="d0_s0" to="o5438" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, divaricately branching, unarmed, internodes without glutinous bands, glabrate (short-pubescent).</text>
      <biological_entity id="o5439" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="divaricately" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unarmed" value_original="unarmed" />
      </biological_entity>
      <biological_entity id="o5440" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o5441" name="band" name_original="bands" src="d0_s1" type="structure">
        <character is_modifier="true" name="coating" src="d0_s1" value="glutinous" value_original="glutinous" />
      </biological_entity>
      <relation from="o5440" id="r751" name="without" negation="false" src="d0_s1" to="o5441" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves petiolate;</text>
      <biological_entity id="o5442" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade broadly lanceolate to obovate, thin, margins entire.</text>
      <biological_entity id="o5443" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="broadly lanceolate" name="shape" src="d0_s3" to="obovate" />
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o5444" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences in axils of leaves, compound cymose, loose, each flower subtended by 2–3 bracts.</text>
      <biological_entity id="o5445" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s4" value="compound" value_original="compound" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s4" value="loose" value_original="loose" />
      </biological_entity>
      <biological_entity id="o5446" name="axil" name_original="axils" src="d0_s4" type="structure" />
      <biological_entity id="o5447" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o5448" name="flower" name_original="flower" src="d0_s4" type="structure" />
      <biological_entity id="o5449" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
      <relation from="o5445" id="r752" name="in" negation="false" src="d0_s4" to="o5446" />
      <relation from="o5446" id="r753" name="part_of" negation="false" src="d0_s4" to="o5447" />
      <relation from="o5448" id="r754" name="subtended by" negation="false" src="d0_s4" to="o5449" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers unisexual, (plants dioecious), chamogamous, narrow;</text>
      <biological_entity id="o5450" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s5" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="chamogamous" value_original="chamogamous" />
        <character is_modifier="false" name="size_or_width" src="d0_s5" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>staminate perianth 5-toothed, tubular to narrowly campanulate, limb not reflexed at maturity, stamens 5–10, exceeding perianth;</text>
      <biological_entity id="o5451" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="5-toothed" value_original="5-toothed" />
        <character char_type="range_value" from="tubular" name="shape" src="d0_s6" to="narrowly campanulate" />
      </biological_entity>
      <biological_entity id="o5452" name="limb" name_original="limb" src="d0_s6" type="structure">
        <character constraint="at maturity" is_modifier="false" modifier="not" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o5453" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="10" />
      </biological_entity>
      <biological_entity id="o5454" name="perianth" name_original="perianth" src="d0_s6" type="structure" />
      <relation from="o5453" id="r755" name="exceeding" negation="false" src="d0_s6" to="o5454" />
    </statement>
    <statement id="d0_s7">
      <text>pistillate perianth 5-toothed, tubular, closing over ovary, styles slender, barely exserted, stigmas tipped by fine hairs.</text>
      <biological_entity id="o5455" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="5-toothed" value_original="5-toothed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="tubular" value_original="tubular" />
        <character constraint="over ovary" constraintid="o5456" is_modifier="false" name="condition" src="d0_s7" value="closing" value_original="closing" />
      </biological_entity>
      <biological_entity id="o5456" name="ovary" name_original="ovary" src="d0_s7" type="structure" />
      <biological_entity id="o5457" name="style" name_original="styles" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="barely" name="position" src="d0_s7" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o5458" name="stigma" name_original="stigmas" src="d0_s7" type="structure">
        <character constraint="by hairs" constraintid="o5459" is_modifier="false" name="architecture" src="d0_s7" value="tipped" value_original="tipped" />
      </biological_entity>
      <biological_entity id="o5459" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="width" src="d0_s7" value="fine" value_original="fine" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fruits bright-orange-red, becoming weakly 10-ribbed upon drying, obovoid to globose, fleshy, glandless.</text>
      <biological_entity id="o5460" name="fruit" name_original="fruits" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="bright-orange-red" value_original="bright-orange-red" />
        <character constraint="upon drying" is_modifier="false" modifier="becoming weakly" name="architecture_or_shape" src="d0_s8" value="10-ribbed" value_original="10-ribbed" />
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s8" to="globose" />
        <character is_modifier="false" name="texture" src="d0_s8" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="glandless" value_original="glandless" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Neotropics.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Neotropics" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <discussion>Species 10–50 (2 in the flora).</discussion>
  <discussion>The genus is closely allied with Neea Ruiz &amp; Pavón, another dioecious, fleshy-fruited, shrubby member of the Nyctaginaceae restricted to the Neotropics.</discussion>
  <discussion>See E. L. Little Jr. (1968) for additional synonymy.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades coriaceous, rigid when dry; young twigs, buds, and inflorescences glabrate or sparsely reddish pubescent</description>
      <determination>1 Guapira discolor</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades fleshy-chartaceous, thin and ± papery when dry; young twigs, buds, and inflo- rescences densely reddish pubescent</description>
      <determination>2 Guapira obtusata</determination>
    </key_statement>
  </key>
</bio:treatment>