<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">197</other_info_on_meta>
    <other_info_on_meta type="mention_page">198</other_info_on_meta>
    <other_info_on_meta type="mention_page">203</other_info_on_meta>
    <other_info_on_meta type="mention_page">206</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="treatment_page">205</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1922" rank="genus">sclerocactus</taxon_name>
    <taxon_name authority="(Engelmann) N. P. Taylor" date="1987" rank="species">papyracanthus</taxon_name>
    <place_of_publication>
      <publication_title>Bradleya</publication_title>
      <place_in_publication>5: 94. 1987</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus sclerocactus;species papyracanthus;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415297</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mammillaria</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="species">papyracantha</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 49. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Mammillaria;species papyracantha;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pediocactus</taxon_name>
    <taxon_name authority="(Engelmann) L. D. Benson" date="unknown" rank="species">papyracanthus</taxon_name>
    <taxon_hierarchy>genus Pediocactus;species papyracanthus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Toumeya</taxon_name>
    <taxon_name authority="(Engelmann) Britton &amp; Rose" date="unknown" rank="species">papyracantha</taxon_name>
    <taxon_hierarchy>genus Toumeya;species papyracantha;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems unbranched, cylindric or obconic cylindric, 2–7.5 (–8) × 1.2–2.5 cm;</text>
      <biological_entity id="o8757" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="shape" src="d0_s0" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s0" value="obconic" value_original="obconic" />
        <character is_modifier="false" name="shape" src="d0_s0" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="7.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s0" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s0" to="7.5" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="width" src="d0_s0" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>ribs not evident, tubercles prominent.</text>
      <biological_entity id="o8758" name="rib" name_original="ribs" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s1" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o8759" name="tubercle" name_original="tubercles" src="d0_s1" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s1" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Spines dense, obscuring stems;</text>
      <biological_entity id="o8760" name="spine" name_original="spines" src="d0_s2" type="structure">
        <character is_modifier="false" name="density" src="d0_s2" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o8761" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <relation from="o8760" id="r1236" name="obscuring" negation="false" src="d0_s2" to="o8761" />
    </statement>
    <statement id="d0_s3">
      <text>radial spines 5–10 per areole, white, straight, flat, (2–) 3–5 × 0.3–0.6 mm;</text>
      <biological_entity id="o8762" name="spine" name_original="spines" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="radial" value_original="radial" />
        <character char_type="range_value" constraint="per areole" constraintid="o8763" from="5" name="quantity" src="d0_s3" to="10" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s3" value="white" value_original="white" />
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_length" src="d0_s3" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s3" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8763" name="areole" name_original="areole" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>central spines 1 (–4) per areole;</text>
      <biological_entity constraint="central" id="o8764" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="4" />
        <character constraint="per areole" constraintid="o8765" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o8765" name="areole" name_original="areole" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>abaxial central spine 1 per areole, whitish to tan or gray, straight, wavy, twisting, or curling, flat, flexible, papery, with obscure adaxial midrib, sometimes pointing upward and obscuring apex of plant, lacking hook, 15–45 (–50) × 1–1.5 mm;</text>
      <biological_entity constraint="abaxial central" id="o8766" name="spine" name_original="spine" src="d0_s5" type="structure">
        <character constraint="per areole" constraintid="o8767" name="quantity" src="d0_s5" value="1" value_original="1" />
        <character char_type="range_value" from="whitish" name="coloration" notes="" src="d0_s5" to="tan or gray" />
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s5" value="wavy" value_original="wavy" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="twisting" value_original="twisting" />
        <character is_modifier="false" name="course" src="d0_s5" value="curling" value_original="curling" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="pliable" value_original="flexible" />
        <character is_modifier="false" name="texture" src="d0_s5" value="papery" value_original="papery" />
        <character constraint="upward apex" constraintid="o8769" is_modifier="false" modifier="sometimes" name="orientation" notes="" src="d0_s5" value="pointing" value_original="pointing" />
      </biological_entity>
      <biological_entity id="o8767" name="areole" name_original="areole" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial" id="o8768" name="midrib" name_original="midrib" src="d0_s5" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s5" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o8769" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <biological_entity id="o8770" name="plant" name_original="plant" src="d0_s5" type="structure" />
      <relation from="o8766" id="r1237" name="with" negation="false" src="d0_s5" to="o8768" />
      <relation from="o8769" id="r1238" name="part_of" negation="false" src="d0_s5" to="o8770" />
    </statement>
    <statement id="d0_s6">
      <text>lateral or adaxial central spines 0 (–3) per areole, white to tan, (3–) 20–40 × 0.5–1 mm.</text>
      <biological_entity id="o8771" name="hook" name_original="hook" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="lacking" value_original="lacking" />
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="50" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s5" to="45" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="central" id="o8772" name="spine" name_original="spines" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="3" />
        <character constraint="per areole" constraintid="o8773" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character char_type="range_value" from="white" name="coloration" notes="" src="d0_s6" to="tan" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s6" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s6" to="40" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8773" name="areole" name_original="areole" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers funnelform to narrowly campanulate, 2–2.5 × 1–2.5 cm;</text>
      <biological_entity id="o8774" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s7" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s7" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>outer tepals with green-purple to redbrown midstripes and cream to white margins, cuneate-spatulate, usually 9–20 × 1–3 mm, finely toothed;</text>
      <biological_entity constraint="outer" id="o8775" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="cream" name="coloration" notes="" src="d0_s8" to="white" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="cuneate-spatulate" value_original="cuneate-spatulate" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s8" to="20" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="usually; finely" name="shape" src="d0_s8" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o8776" name="midstripe" name_original="midstripes" src="d0_s8" type="structure">
        <character char_type="range_value" from="green-purple" is_modifier="true" name="coloration" src="d0_s8" to="redbrown" />
      </biological_entity>
      <biological_entity id="o8777" name="margin" name_original="margins" src="d0_s8" type="structure" />
      <relation from="o8775" id="r1239" name="with" negation="false" src="d0_s8" to="o8776" />
    </statement>
    <statement id="d0_s9">
      <text>inner tepals white with brown midstripes, largest tepals oblanceolate, 15–20 × 3.5–4.5 mm, apex acute to mucronate;</text>
      <biological_entity constraint="inner" id="o8778" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character constraint="with midstripes" constraintid="o8779" is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o8779" name="midstripe" name_original="midstripes" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity constraint="largest" id="o8780" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s9" to="20" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s9" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8781" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments white to greenish yellow;</text>
      <biological_entity id="o8782" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="greenish yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers cream to pale-yellow.</text>
      <biological_entity id="o8783" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="cream" name="coloration" src="d0_s11" to="pale-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits indehiscent or irregularly dehiscent, green, subspheric, 4–6 × 3–5 mm, dry at maturity;</text>
      <biological_entity id="o8784" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s12" value="indehiscent" value_original="indehiscent" />
        <character is_modifier="false" modifier="irregularly" name="dehiscence" src="d0_s12" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subspheric" value_original="subspheric" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s12" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s12" to="5" to_unit="mm" />
        <character constraint="at maturity" is_modifier="false" name="condition_or_texture" src="d0_s12" value="dry" value_original="dry" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>scales few or none.</text>
      <biological_entity id="o8785" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s13" value="few" value_original="few" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds black, 2.5–3 × 2–2.5 mm, shiny;</text>
      <biological_entity id="o8786" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="black" value_original="black" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s14" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s14" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s14" value="shiny" value_original="shiny" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>testa with fine, rounded papillae.</text>
      <biological_entity id="o8787" name="testa" name_original="testa" src="d0_s15" type="structure" />
      <biological_entity id="o8788" name="papilla" name_original="papillae" src="d0_s15" type="structure">
        <character is_modifier="true" name="width" src="d0_s15" value="fine" value_original="fine" />
        <character is_modifier="true" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o8787" id="r1240" name="with" negation="false" src="d0_s15" to="o8788" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Desert grasslands, pinyon-juniper woodlands, Chihuahuan desert scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="desert grasslands" />
        <character name="habitat" value="pinyon-juniper woodlands" />
        <character name="habitat" value="chihuahuan desert scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Grama-grass cactus</other_name>
  <other_name type="common_name">paper-spined cactus</other_name>
  <other_name type="common_name">toumeya</other_name>
  <discussion>With long, flexuous, flattened spines and pale flowers, Sclerocactus papyracanthus is surprisingly cryptic in the field. There has been a long debate concerning its taxonomic placement. Chloroplast DNA analyses of J. M. Porter et al. (2000) unambiguously placed this species within Sclerocactus, a position first suggested based on morphologic evidence (K. D. Heil et al. 1981; C. Glass and R. A. Foster 1984).</discussion>
  
</bio:treatment>