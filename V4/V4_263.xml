<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">129</other_info_on_meta>
    <other_info_on_meta type="mention_page">133</other_info_on_meta>
    <other_info_on_meta type="treatment_page">139</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Opuntioideae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">opuntia</taxon_name>
    <taxon_name authority="Griffiths" date="unknown" rank="species">×curvispina</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>43: 88, plate 2. 1916</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily opuntioideae;genus opuntia;species ×curvispina;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415205</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, shrubby, 0.5–1.5 m, with trunk to 20 cm.</text>
      <biological_entity id="o10718" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="shrubby" value_original="shrubby" />
        <character char_type="range_value" from="0.5" from_unit="m" name="some_measurement" src="d0_s0" to="1.5" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
      <biological_entity id="o10719" name="trunk" name_original="trunk" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
      </biological_entity>
      <relation from="o10718" id="r1567" name="with" negation="false" src="d0_s0" to="o10719" />
    </statement>
    <statement id="d0_s1">
      <text>Stem segments not disarticulating, gray-green to yellow-green, flattened, circular to broadly obovate, 12–22 × 12.5–20 cm, sometimes wider than long, ± tuberculate, glabrous;</text>
      <biological_entity constraint="stem" id="o10720" name="segment" name_original="segments" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="disarticulating" value_original="disarticulating" />
        <character char_type="range_value" from="gray-green" name="coloration" src="d0_s1" to="yellow-green" />
        <character is_modifier="false" name="shape" src="d0_s1" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="circular" name="shape" src="d0_s1" to="broadly obovate" />
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s1" to="22" to_unit="cm" />
        <character char_type="range_value" from="12.5" from_unit="cm" name="width" src="d0_s1" to="20" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s1" value="sometimes wider than long" value_original="sometimes wider than long" />
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s1" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>areoles 6–9 per diagonal row across midstem segment, prominent, circular to elliptic, 4–7 × 3–7 mm;</text>
      <biological_entity id="o10721" name="areole" name_original="areoles" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="per diagonal row" constraintid="o10722" from="6" name="quantity" src="d0_s2" to="9" />
        <character is_modifier="false" name="prominence" notes="" src="d0_s2" value="prominent" value_original="prominent" />
        <character char_type="range_value" from="circular" name="arrangement_or_shape" src="d0_s2" to="elliptic" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s2" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="diagonal" id="o10722" name="row" name_original="row" src="d0_s2" type="structure" />
      <biological_entity constraint="midstem" id="o10723" name="segment" name_original="segment" src="d0_s2" type="structure" />
      <relation from="o10722" id="r1568" name="across" negation="false" src="d0_s2" to="o10723" />
    </statement>
    <statement id="d0_s3">
      <text>wool tan to brown, aging gray.</text>
      <biological_entity id="o10724" name="wool" name_original="wool" src="d0_s3" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s3" to="brown" />
        <character is_modifier="false" name="life_cycle" src="d0_s3" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="gray" value_original="gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spines in most areoles, reddish yellow to redbrown;</text>
      <biological_entity id="o10725" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" from="reddish yellow" name="coloration" notes="" src="d0_s4" to="redbrown" />
      </biological_entity>
      <biological_entity id="o10726" name="areole" name_original="areoles" src="d0_s4" type="structure" />
      <relation from="o10725" id="r1569" name="in" negation="false" src="d0_s4" to="o10726" />
    </statement>
    <statement id="d0_s5">
      <text>abaxial ones sometimes whitish;</text>
      <biological_entity constraint="abaxial" id="o10727" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s5" value="whitish" value_original="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>adaxial spines often yellow;</text>
      <biological_entity constraint="adaxial" id="o10728" name="spine" name_original="spines" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>major spines (0–) 4–8 (–9) per areole, usually deflexed, sometimes spreading, particularly on stem segment margins, subterete to flattened or channeled adaxially, sometimes curved, stiff, the longest 40–60 mm.</text>
      <biological_entity id="o10729" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="major" value_original="major" />
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s7" to="4" to_inclusive="false" />
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="9" />
        <character char_type="range_value" constraint="per areole" constraintid="o10730" from="4" name="quantity" src="d0_s7" to="8" />
        <character is_modifier="false" modifier="usually" name="orientation" notes="" src="d0_s7" value="deflexed" value_original="deflexed" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="subterete" modifier="adaxially" name="shape" notes="" src="d0_s7" to="flattened or channeled" />
        <character is_modifier="false" modifier="sometimes" name="course" src="d0_s7" value="curved" value_original="curved" />
        <character is_modifier="false" name="fragility" src="d0_s7" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="length" src="d0_s7" value="longest" value_original="longest" />
        <character char_type="range_value" from="40" from_unit="mm" name="some_measurement" src="d0_s7" to="60" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10730" name="areole" name_original="areole" src="d0_s7" type="structure" />
      <biological_entity constraint="segment" id="o10731" name="margin" name_original="margins" src="d0_s7" type="structure" constraint_original="stem segment" />
      <relation from="o10729" id="r1570" modifier="particularly" name="on" negation="false" src="d0_s7" to="o10731" />
    </statement>
    <statement id="d0_s8">
      <text>Glochids crowded in extended marginal crescent, nearly encircling areole, and less dense subapical tuft obscured by long dense wool, yellow to yellowbrown, 1–6 (–12) mm.</text>
      <biological_entity id="o10732" name="glochid" name_original="glochids" src="d0_s8" type="structure">
        <character constraint="in marginal crescent" constraintid="o10733" is_modifier="false" name="arrangement" src="d0_s8" value="crowded" value_original="crowded" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o10733" name="crescent" name_original="crescent" src="d0_s8" type="structure">
        <character is_modifier="true" name="size" src="d0_s8" value="extended" value_original="extended" />
      </biological_entity>
      <biological_entity id="o10734" name="areole" name_original="areole" src="d0_s8" type="structure" />
      <biological_entity constraint="subapical" id="o10735" name="tuft" name_original="tuft" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="less" name="density" src="d0_s8" value="dense" value_original="dense" />
        <character constraint="by wool" constraintid="o10736" is_modifier="false" name="prominence" src="d0_s8" value="obscured" value_original="obscured" />
        <character char_type="range_value" from="yellow" name="coloration" notes="" src="d0_s8" to="yellowbrown" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="12" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10736" name="wool" name_original="wool" src="d0_s8" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character is_modifier="true" name="density" src="d0_s8" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o10732" id="r1571" name="nearly encircling" negation="false" src="d0_s8" to="o10734" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: inner tepals yellow sometimes with faint basal reddish blush, broadly obovate, 22–35 mm, apiculate;</text>
      <biological_entity id="o10737" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="inner" id="o10738" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character constraint="with basal reddish blush" constraintid="o10739" is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s9" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="22" from_unit="mm" name="some_measurement" src="d0_s9" to="35" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o10739" name="reddish blush" name_original="reddish blush" src="d0_s9" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s9" value="faint" value_original="faint" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments and anthers yellow;</text>
      <biological_entity id="o10740" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o10741" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o10742" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style white;</text>
      <biological_entity id="o10743" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o10744" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigma lobes greenish white.</text>
      <biological_entity id="o10745" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="stigma" id="o10746" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="greenish white" value_original="greenish white" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits dull red with greenish flesh, ovate to obovate, 32–50 × 15–30 mm, fleshy, glabrous, spineless;</text>
      <biological_entity id="o10747" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s13" value="dull" value_original="dull" />
        <character constraint="with flesh" constraintid="o10748" is_modifier="false" name="coloration" src="d0_s13" value="red" value_original="red" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s13" to="obovate" />
        <character char_type="range_value" from="32" from_unit="mm" name="length" src="d0_s13" to="50" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s13" to="30" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s13" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="spineless" value_original="spineless" />
      </biological_entity>
      <biological_entity id="o10748" name="flesh" name_original="flesh" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="greenish" value_original="greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>umbilicus 4–6 mm deep;</text>
      <biological_entity id="o10749" name="umbilicus" name_original="umbilicus" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
        <character is_modifier="false" name="depth" src="d0_s14" value="deep" value_original="deep" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>areoles 28–46.</text>
      <biological_entity id="o10750" name="areole" name_original="areoles" src="d0_s15" type="structure">
        <character char_type="range_value" from="28" name="quantity" src="d0_s15" to="46" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds yellowish, reniform to subcircular, 4–5 mm diam., sides flattened, smooth;</text>
      <biological_entity id="o10751" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="reniform" name="shape" src="d0_s16" to="subcircular" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s16" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10752" name="side" name_original="sides" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>girdle protruding 0.5–0.8 mm. 2n = 22.</text>
      <biological_entity id="o10753" name="girdle" name_original="girdle" src="d0_s17" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s17" value="protruding" value_original="protruding" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s17" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10754" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (Apr–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="Apr-Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Desert grasslands, oak and/or juniper woodlands, sandy to gravelly flats or slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="desert grasslands" />
        <character name="habitat" value="oak and\/or juniper woodlands" />
        <character name="habitat" value="gravelly flats" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <discussion>Opuntia ×curvispina is a nothospecies resulting from hybridization of O. chlorotica and O. phaeacantha. B. D. Parfitt (1980) separated the tetraploid Opuntia martiniana (L. D. Benson) B. D. Parfitt [O. littoralis Engelmann var. martiniana (L. D. Benson) L. D. Benson] from O. ×curvispina on the basis of having style obovoid (widest at or above the middle) versus ovoid (widest near the base) and other differences, often overlapping, such as more yellow spines, fewer areoles per stem segment, and size differences of fruit. Because both are tetraploid putative hybrids, grow in proximity, and share many character states, I am reluctant to separate them until more evidence is obtained.</discussion>
  
</bio:treatment>