<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">324</other_info_on_meta>
    <other_info_on_meta type="mention_page">325</other_info_on_meta>
    <other_info_on_meta type="treatment_page">341</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">atriplex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">atriplex</taxon_name>
    <taxon_name authority="Buxbaum" date="1927" rank="section">Spongiocarpus</taxon_name>
    <place_of_publication>
      <publication_title>Verh. Zool.-Bot. Ges. Wien</publication_title>
      <place_in_publication>76: 46. 1927</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus atriplex;subgenus atriplex;section Spongiocarpus</taxon_hierarchy>
    <other_info_on_name type="fna_id">304241</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atriplex</taxon_name>
    <taxon_name authority="Ulbrich" date="unknown" rank="unranked">Halimoides</taxon_name>
    <taxon_hierarchy>genus Atriplex;unranked Halimoides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or short-lived perennial, erect or procumbent.</text>
      <biological_entity id="o4042" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="procumbent" value_original="procumbent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves with Kranz-type anatomy.</text>
      <biological_entity id="o4043" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Pistillate flowers lacking perianth, enclosed by a pair of bracteoles.</text>
      <biological_entity id="o4044" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o4045" name="perianth" name_original="perianth" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="lacking" value_original="lacking" />
      </biological_entity>
      <biological_entity id="o4046" name="pair" name_original="pair" src="d0_s2" type="structure" />
      <biological_entity id="o4047" name="bracteole" name_original="bracteoles" src="d0_s2" type="structure" />
      <relation from="o4044" id="r531" name="enclosed by a" negation="false" src="d0_s2" to="o4046" />
      <relation from="o4044" id="r532" name="part_of" negation="false" src="d0_s2" to="o4047" />
    </statement>
    <statement id="d0_s3">
      <text>Bracteoles ovoid-globular to broadly turbinate or hemispheric, united almost to summit, erect.</text>
      <biological_entity id="o4048" name="bracteole" name_original="bracteoles" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovoid-globular" name="shape" src="d0_s3" to="broadly turbinate or hemispheric" />
        <character constraint="to summit" constraintid="o4049" is_modifier="false" name="fusion" src="d0_s3" value="united" value_original="united" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s3" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o4049" name="summit" name_original="summit" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Seeds erect, or basal and horizontal;</text>
      <biological_entity id="o4050" name="seed" name_original="seeds" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="horizontal" value_original="horizontal" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>radicle usually lateral.</text>
      <biological_entity id="o4051" name="radicle" name_original="radicle" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="position" src="d0_s5" value="lateral" value_original="lateral" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>20a.4.</number>
  <discussion>Species 6 (2 in the flora).</discussion>
  
</bio:treatment>