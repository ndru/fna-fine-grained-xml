<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">383</other_info_on_meta>
    <other_info_on_meta type="mention_page">385</other_info_on_meta>
    <other_info_on_meta type="treatment_page">384</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">salicornia</taxon_name>
    <taxon_name authority="Standley in N. L. Britton et al." date="1916" rank="species">depressa</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>21: 85. 1916</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus salicornia;species depressa</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415613</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, green, only occasionally developing some red or purple, usually with primary and some secondary branches, 10–70 cm, ultimate branches often long and cylindric;</text>
      <biological_entity id="o1920" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" modifier="only occasionally" name="development" src="d0_s0" value="developing" value_original="developing" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="purple" value_original="purple" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="70" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1921" name="primary" name_original="primary" src="d0_s0" type="structure" />
      <biological_entity constraint="secondary" id="o1922" name="branch" name_original="branches" src="d0_s0" type="structure" />
      <biological_entity constraint="ultimate" id="o1923" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="often" name="length_or_size" src="d0_s0" value="long" value_original="long" />
        <character is_modifier="false" name="shape" src="d0_s0" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <relation from="o1920" id="r243" modifier="usually" name="with" negation="false" src="d0_s0" to="o1921" />
    </statement>
    <statement id="d0_s1">
      <text>leaf and bract apices usually obtuse, not mucronate.</text>
      <biological_entity id="o1924" name="leaf" name_original="leaf" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s1" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s1" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity constraint="bract" id="o1925" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s1" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s1" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Spikes cylindric, 2–8 cm, with 5–25 fertile segments;</text>
      <biological_entity id="o1926" name="spike" name_original="spikes" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1927" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s2" to="25" />
        <character is_modifier="true" name="reproduction" src="d0_s2" value="fertile" value_original="fertile" />
      </biological_entity>
      <relation from="o1926" id="r244" name="with" negation="false" src="d0_s2" to="o1927" />
    </statement>
    <statement id="d0_s3">
      <text>bracts covering only base of cymes.</text>
      <biological_entity id="o1928" name="bract" name_original="bracts" src="d0_s3" type="structure" constraint="cyme" constraint_original="cyme; cyme" />
      <biological_entity id="o1929" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o1930" name="cyme" name_original="cymes" src="d0_s3" type="structure" />
      <relation from="o1928" id="r245" name="covering" negation="false" src="d0_s3" to="o1929" />
      <relation from="o1928" id="r246" name="part_of" negation="false" src="d0_s3" to="o1930" />
    </statement>
    <statement id="d0_s4">
      <text>Fertile segments (2d–4th in main spikes) ± cylindric, 3.2–7.6 × 2.4–5 mm, usually longer than wide, margins 0.3–0.4 mm wide, scarious.</text>
      <biological_entity id="o1931" name="segment" name_original="segments" src="d0_s4" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s4" value="fertile" value_original="fertile" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="length" src="d0_s4" to="7.6" to_unit="mm" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" name="length_or_size" src="d0_s4" value="usually longer than wide" value_original="usually longer than wide" />
      </biological_entity>
      <biological_entity id="o1932" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s4" to="0.4" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Central flowers oval or rhombic to semicircular distally, 1.7–3.5 × 1.4–2.7 mm, usually longer than wide, usually not conspicuously larger than lateral flowers, usually not reaching distal edge of segment;</text>
      <biological_entity constraint="central" id="o1933" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="rhombic" modifier="distally" name="shape" src="d0_s5" to="semicircular" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s5" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s5" to="2.7" to_unit="mm" />
        <character is_modifier="false" name="length_or_size" src="d0_s5" value="usually longer than wide" value_original="usually longer than wide" />
        <character constraint="than lateral flowers" constraintid="o1934" is_modifier="false" name="size" src="d0_s5" value="usually not conspicuously larger" value_original="usually not conspicuously larger" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o1934" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o1935" name="edge" name_original="edge" src="d0_s5" type="structure" />
      <biological_entity id="o1936" name="segment" name_original="segment" src="d0_s5" type="structure" />
      <relation from="o1933" id="r247" modifier="usually not" name="reaching" negation="false" src="d0_s5" to="o1935" />
      <relation from="o1933" id="r248" name="part_of" negation="false" src="d0_s5" to="o1936" />
    </statement>
    <statement id="d0_s6">
      <text>anthers always exserted, 0.3–0.5 mm, dehiscing after exsertion.</text>
    </statement>
    <statement id="d0_s7">
      <text>2n = 36.</text>
      <biological_entity id="o1937" name="anther" name_original="anthers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="always" name="position" src="d0_s6" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s6" to="0.5" to_unit="mm" />
        <character constraint="after exsertion" is_modifier="false" name="dehiscence" src="d0_s6" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1938" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Middle and lower levels of saltmarshes and sides of channels, rarely in saline areas inland</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="middle" constraint="of saltmarshes and sides of channels , rarely in saline areas inland" />
        <character name="habitat" value="lower levels" constraint="of saltmarshes and sides of channels , rarely in saline areas inland" />
        <character name="habitat" value="saltmarshes" constraint="of channels , rarely in saline areas inland" />
        <character name="habitat" value="sides" constraint="of channels , rarely in saline areas inland" />
        <character name="habitat" value="channels" />
        <character name="habitat" value="saline areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0(-100) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., N.B., N.S., P.E.I., Que.; Alaska, Calif., Del., Ga., Maine, Md., Mass., N.H., N.J., N.Y., N.C., Oreg., R.I., S.C., Va., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Salicornia depressa is the common and widespread species in coastal areas of North America. No detailed taxonomic investigation of the tetraploid populations has ever been undertaken in North America. It is possible that a number of different taxa exist; the Pacific Coast populations in particular seem distinct from those of the Atlantic Coast.</discussion>
  <discussion>This species has generally been called Salicornia europaea by North American authors, but that name refers to a diploid European species that does not occur in North America.</discussion>
  <discussion>The earliest name that is probably referable to this species is Salicornia virginica Linnaeus, which must be typified by specimens collected by John Clayton probably from Virginia. The specimens are sterile but clearly annual, as indicated by Linnaeus in the protologue. The name was applied to an annual species by P. C. Standley (1916), but subsequently came to be misapplied to the perennial species of the east coast now treated in Sarcocornia.</discussion>
  
</bio:treatment>