<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">351</other_info_on_meta>
    <other_info_on_meta type="mention_page">352</other_info_on_meta>
    <other_info_on_meta type="treatment_page">350</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">atriplex</taxon_name>
    <taxon_name authority="(Gaertner) S. L. Welsh" date="2001" rank="subgenus">Obione</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Obione</taxon_name>
    <taxon_name authority="(Standley) S. L. Welsh" date="2001" rank="subsection">Argenteae</taxon_name>
    <taxon_name authority="Nuttall" date="1818" rank="species">argentea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">argentea</taxon_name>
    <taxon_hierarchy>family chenopodiaceae;genus atriplex;subgenus obione;section obione;subsection argenteae;species argentea;variety argentea;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415534</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atriplex</taxon_name>
    <taxon_name authority="A. Nelson" date="unknown" rank="species">volutans</taxon_name>
    <taxon_hierarchy>genus Atriplex;species volutans;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, 3–4 dm.</text>
      <biological_entity id="o3219" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves proximalmost alternate, distal ones short petiolate;</text>
      <biological_entity id="o3220" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="proximalmost" value_original="proximalmost" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o3221" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade ovate or ovate-elliptic to deltoid, 6–50 × 4–40 mm, margin entire or repand-dentate, grayish scurfy when young.</text>
      <biological_entity id="o3222" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate-elliptic" name="shape" src="d0_s2" to="deltoid" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s2" to="50" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s2" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Fruiting bracteoles sessile or some stipitate (stipe 0.5–5 mm), cuneate to obovate to suborbicular in profile, 3.8–11.2 × 4–8.8 (–14) mm, usually compressed, united nearly to apex, green free margin extending nearly to base, dentate, sides smooth or sparsely to densely tuberculate or cristate.</text>
      <biological_entity id="o3223" name="margin" name_original="margin" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="repand-dentate" value_original="repand-dentate" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="grayish" value_original="grayish" />
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s2" value="scurfy" value_original="scurfy" />
      </biological_entity>
      <biological_entity id="o3224" name="bracteole" name_original="bracteoles" src="d0_s3" type="structure" />
      <biological_entity id="o3225" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="stipitate" value_original="stipitate" />
        <character char_type="range_value" from="cuneate" modifier="in profile" name="shape" src="d0_s3" to="obovate" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="fusion" src="d0_s3" value="united" value_original="united" />
      </biological_entity>
      <biological_entity id="o3226" name="margin" name_original="margin" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="true" name="fusion" src="d0_s3" value="free" value_original="free" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o3227" name="base" name_original="base" src="d0_s3" type="structure" />
      <relation from="o3223" id="r413" name="fruiting" negation="false" src="d0_s3" to="o3224" />
      <relation from="o3226" id="r414" name="extending" negation="false" src="d0_s3" to="o3227" />
    </statement>
    <statement id="d0_s4">
      <text>2n = 18.</text>
      <biological_entity id="o3228" name="side" name_original="sides" src="d0_s3" type="structure">
        <character char_type="range_value" from="smooth or" name="relief" src="d0_s3" to="sparsely densely tuberculate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cristate" value_original="cristate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3229" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry or saline substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="saline" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>above 1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="any" to_unit="m" from="1000" from_unit="m" constraint="above " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., Sask.; Ariz., Colo., Idaho, Kans., Mont., Nebr., Nev., N.Mex., N.Dak., S.Dak., Tex., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25a.</number>
  <discussion>Atriplex argentea var. argentea occasionally may be introduced in Michigan, Missouri, and reputedly, Ohio.</discussion>
  
</bio:treatment>