<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">6</other_info_on_meta>
    <other_info_on_meta type="treatment_page">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">phytolaccaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">phytolacca</taxon_name>
    <taxon_name authority="Linnaeus" date="1762" rank="species">octandra</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl. ed.</publication_title>
      <place_in_publication>2, 1: 631. 1762</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phytolaccaceae;genus phytolacca;species octandra</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415006</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 2 m.</text>
      <biological_entity id="o2536" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="2" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 0.5–3 cm;</text>
      <biological_entity id="o2537" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o2538" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade lanceolate to elliptic, to 22 × 7.5 cm, base obtuse to attenuate, apex acute to acute-acuminate, sometimes mucronate.</text>
      <biological_entity id="o2539" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o2540" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="elliptic" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s2" to="22" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s2" to="7.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2541" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="attenuate" />
      </biological_entity>
      <biological_entity id="o2542" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="acute-acuminate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences axillary or terminal spikes or spikelike, equaling or shorter than subtending leaves, to 14 cm;</text>
      <biological_entity id="o2543" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o2544" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="spikelike" value_original="spikelike" />
        <character is_modifier="false" name="variability" src="d0_s3" value="equaling" value_original="equaling" />
        <character constraint="than subtending leaves" constraintid="o2545" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="shorter" value_original="shorter" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="14" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o2545" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>peduncle to 3 cm;</text>
      <biological_entity id="o2546" name="peduncle" name_original="peduncle" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pedicel absent or to 2 mm.</text>
      <biological_entity id="o2547" name="pedicel" name_original="pedicel" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s5" value="0-2 mm" value_original="0-2 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals 5, white to pinkish or reddish, oblong to ovate, subequal, 2–3 × 1.2–2 mm;</text>
      <biological_entity id="o2548" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o2549" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s6" to="pinkish or reddish" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s6" to="ovate" />
        <character is_modifier="false" name="size" src="d0_s6" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s6" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens (7–) 8–10, in 1 whorl;</text>
      <biological_entity id="o2550" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o2551" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" name="atypical_quantity" src="d0_s7" to="8" to_inclusive="false" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s7" to="10" />
      </biological_entity>
      <biological_entity id="o2552" name="whorl" name_original="whorl" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <relation from="o2551" id="r325" name="in" negation="false" src="d0_s7" to="o2552" />
    </statement>
    <statement id="d0_s8">
      <text>carpels 7–10, connate;</text>
      <biological_entity id="o2553" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o2554" name="carpel" name_original="carpels" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s8" to="10" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary 7–10-loculed.</text>
      <biological_entity id="o2555" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o2556" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="7-10-loculed" value_original="7-10-loculed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Berries greenish, 4.5–6 mm diam.</text>
      <biological_entity id="o2557" name="berry" name_original="berries" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="diameter" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds black, lenticular, 2 mm, shiny.</text>
      <biological_entity id="o2558" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s11" value="lenticular" value_original="lenticular" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="reflectance" src="d0_s11" value="shiny" value_original="shiny" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall northward, probably year-round southward.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="northward" to="fall" from="summer" />
        <character name="flowering time" char_type="range_value" modifier="probably;  southward" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Wis.; Mexico; West Indies; Central America; South America; Asia; Africa; Pacific Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Red inkplant</other_name>
  <discussion>Phytolacca octandra is a species with nearly worldwide distribution. It is closely similar to, and perhaps not specifically distinct from, P. icosandra (J. I. Davis 1985).</discussion>
  
</bio:treatment>