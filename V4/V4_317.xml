<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">161</other_info_on_meta>
    <other_info_on_meta type="mention_page">164</other_info_on_meta>
    <other_info_on_meta type="treatment_page">165</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Engelmann in F. A.Wislizenus" date="1848" rank="genus">echinocereus</taxon_name>
    <taxon_name authority="(Engelmann ex S. Watson) L. D. Benson" date="1969" rank="species">fasciculatus</taxon_name>
    <place_of_publication>
      <publication_title>Cacti Arizona ed.</publication_title>
      <place_in_publication>3, 21. 1969</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus echinocereus;species fasciculatus;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415248</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mammillaria</taxon_name>
    <taxon_name authority="Engelmann ex S. Watson" date="unknown" rank="species">fasciculata</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Misc. Collect.</publication_title>
      <place_in_publication>258: 118. 1878</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Mammillaria;species fasciculata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinocereus</taxon_name>
    <taxon_name authority="(Engelmann) Lemaire" date="unknown" rank="species">engelmannii</taxon_name>
    <taxon_name authority="(Engelmann ex S. Watson) Blume &amp; Mich. Lange" date="unknown" rank="subspecies">fasciculatus</taxon_name>
    <taxon_hierarchy>genus Echinocereus;species engelmannii;subspecies fasciculatus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinocereus</taxon_name>
    <taxon_name authority="va r. boyce-thompsonii (Orcutt) L. D. Benson" date="unknown" rank="species">fasciculatus</taxon_name>
    <taxon_hierarchy>genus Echinocereus;species fasciculatus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinocereus</taxon_name>
    <taxon_name authority="(Engelmann) Sencke ex J. N. Haage" date="unknown" rank="species">fendleri</taxon_name>
    <taxon_name authority="(Peebles) L. D. Benson" date="unknown" rank="variety">robustus</taxon_name>
    <taxon_hierarchy>genus Echinocereus;species fendleri;variety robustus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinocereus</taxon_name>
    <taxon_name authority="Peebles" date="unknown" rank="species">rectispinus</taxon_name>
    <taxon_name authority="Peebles" date="unknown" rank="variety">robustus</taxon_name>
    <taxon_hierarchy>genus Echinocereus;species rectispinus;variety robustus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 5–30-branched, forming somewhat open clumps.</text>
      <biological_entity id="o24396" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="5-30-branched" value_original="5-30-branched" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o24397" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="somewhat" name="architecture" src="d0_s0" value="open" value_original="open" />
      </biological_entity>
      <relation from="o24396" id="r3550" name="forming" negation="false" src="d0_s0" to="o24397" />
    </statement>
    <statement id="d0_s1">
      <text>Stems mostly erect, cylindric, (5–) 10–40 × 4.5–8 cm;</text>
      <biological_entity id="o24398" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s1" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_length" src="d0_s1" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s1" to="40" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="width" src="d0_s1" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>ribs 10–15, crests slightly undulate;</text>
      <biological_entity id="o24399" name="rib" name_original="ribs" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s2" to="15" />
      </biological_entity>
      <biological_entity id="o24400" name="crest" name_original="crests" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s2" value="undulate" value_original="undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>areoles 10–15 mm apart.</text>
      <biological_entity id="o24401" name="areole" name_original="areoles" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="15" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="apart" value_original="apart" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spines 8–16 per areole, usually straight, individual spines with broad zones of different colors: yellowish, reddish-brown, or gray to black, or white to black, becoming gray;</text>
      <biological_entity id="o24402" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o24403" from="8" name="quantity" src="d0_s4" to="16" />
        <character is_modifier="false" modifier="usually" name="course" notes="" src="d0_s4" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o24403" name="areole" name_original="areole" src="d0_s4" type="structure" />
      <biological_entity constraint="individual" id="o24404" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" from="gray" name="coloration" notes="" src="d0_s4" to="black or white" />
        <character char_type="range_value" from="gray" name="coloration" src="d0_s4" to="black or white" />
        <character char_type="range_value" from="gray" name="coloration" src="d0_s4" to="black or white" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s4" value="gray" value_original="gray" />
      </biological_entity>
      <biological_entity id="o24405" name="zone" name_original="zones" src="d0_s4" type="structure">
        <character is_modifier="true" name="width" src="d0_s4" value="broad" value_original="broad" />
        <character is_modifier="false" modifier="of different colors" name="coloration" src="d0_s4" value="yellowish" value_original="yellowish" />
      </biological_entity>
      <relation from="o24404" id="r3551" name="with" negation="false" src="d0_s4" to="o24405" />
    </statement>
    <statement id="d0_s5">
      <text>radial spines 7–15 per areole, 5–15 mm;</text>
      <biological_entity id="o24406" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o24407" from="8" name="quantity" src="d0_s5" to="16" />
        <character is_modifier="false" modifier="usually" name="course" notes="" src="d0_s5" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o24407" name="areole" name_original="areole" src="d0_s5" type="structure" />
      <biological_entity constraint="individual" id="o24408" name="spine" name_original="spines" src="d0_s5" type="structure" />
      <biological_entity id="o24409" name="zone" name_original="zones" src="d0_s5" type="structure">
        <character is_modifier="true" name="width" src="d0_s5" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o24410" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="radial" value_original="radial" />
        <character char_type="range_value" constraint="per areole" constraintid="o24411" from="7" modifier="of different colors" name="quantity" src="d0_s5" to="15" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24411" name="areole" name_original="areole" src="d0_s5" type="structure" />
      <relation from="o24408" id="r3552" name="with" negation="false" src="d0_s5" to="o24409" />
    </statement>
    <statement id="d0_s6">
      <text>central spines 1–3 per areole, divergent-porrect, 15–75 mm, all terete.</text>
      <biological_entity id="o24412" name="spine" name_original="spines" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o24413" from="8" name="quantity" src="d0_s6" to="16" />
        <character is_modifier="false" modifier="usually" name="course" notes="" src="d0_s6" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o24413" name="areole" name_original="areole" src="d0_s6" type="structure" />
      <biological_entity constraint="individual" id="o24414" name="spine" name_original="spines" src="d0_s6" type="structure" />
      <biological_entity id="o24415" name="zone" name_original="zones" src="d0_s6" type="structure">
        <character is_modifier="true" name="width" src="d0_s6" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity constraint="central" id="o24416" name="spine" name_original="spines" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o24417" from="1" modifier="of different colors" name="quantity" src="d0_s6" to="3" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s6" value="divergent-porrect" value_original="divergent-porrect" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s6" to="75" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="terete" value_original="terete" />
      </biological_entity>
      <biological_entity id="o24417" name="areole" name_original="areole" src="d0_s6" type="structure" />
      <relation from="o24414" id="r3553" name="with" negation="false" src="d0_s6" to="o24415" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers 6–10 × 8–10 cm;</text>
      <biological_entity id="o24418" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s7" to="10" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="width" src="d0_s7" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>flower tube 10–20 × 15–40 mm;</text>
      <biological_entity constraint="flower" id="o24419" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s8" to="20" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s8" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>flower tube hairs 2.3–4 mm;</text>
      <biological_entity constraint="tube" id="o24420" name="hair" name_original="hairs" src="d0_s9" type="structure" constraint_original="flower tube">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>inner tepals rose-pink to magenta [rarely nearly white], with midstripes darker, darker magenta or sometimes purplish maroon near base, (20–) 35–52 × 12–20 mm, tips relatively thin and delicate;</text>
      <biological_entity constraint="inner" id="o24421" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="rose-pink" name="coloration" src="d0_s10" to="magenta" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="darker magenta" value_original="darker magenta" />
        <character name="coloration" src="d0_s10" value="sometimes" value_original="sometimes" />
        <character constraint="near base" constraintid="o24423" is_modifier="false" name="coloration" src="d0_s10" value="purplish maroon" value_original="purplish maroon" />
        <character char_type="range_value" from="20" from_unit="mm" name="atypical_length" notes="" src="d0_s10" to="35" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="35" from_unit="mm" name="length" notes="" src="d0_s10" to="52" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" notes="" src="d0_s10" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24422" name="midstripe" name_original="midstripes" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity id="o24423" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o24424" name="tip" name_original="tips" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="relatively" name="width" src="d0_s10" value="thin" value_original="thin" />
        <character is_modifier="false" name="fragility" src="d0_s10" value="delicate" value_original="delicate" />
      </biological_entity>
      <relation from="o24421" id="r3554" name="with" negation="false" src="d0_s10" to="o24422" />
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow;</text>
      <biological_entity id="o24425" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectar chamber 3.3–5 mm.</text>
      <biological_entity constraint="nectar" id="o24426" name="chamber" name_original="chamber" src="d0_s12" type="structure">
        <character char_type="range_value" from="3.3" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits bright red or orange-red, 20–30 mm, pulp white or sometimes pink.</text>
      <biological_entity id="o24427" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="bright red" value_original="bright red" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="orange-red" value_original="orange-red" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s13" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = 44.</text>
      <biological_entity id="o24428" name="pulp" name_original="pulp" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s13" value="pink" value_original="pink" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24429" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="44" value_original="44" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun; fruiting May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
        <character name="fruiting time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sonoran Desert, flats to steep canyonsides, desert scrub, semidesert grasslands, interior chaparral</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="flats" />
        <character name="habitat" value="steep canyonsides" />
        <character name="habitat" value="desert scrub" />
        <character name="habitat" value="semidesert grasslands" />
        <character name="habitat" value="interior chaparral" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>[30-]600-1000(-1500) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="600" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1500" to_unit="m" from="600" from_unit="m" />
        <character name="elevation" char_type="foreign_range" to="1000" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico (Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <discussion>New Mexico records of Echinocereus fasciculatus are at least in part based on vigorous old plants of E. fendleri, which sometimes have 1–2 short supplementary central spines (and which appear very different from younger plants in the same populations). Echinocereus fasciculatus may prove to intergrade clinally with E. engelmannii var. acicularis wherever their geographic ranges approach each other. At its upper altitudinal limit, E. fasciculatus tends to have shorter spines, fewer central spines, shorter stems, and more compact growth habit.</discussion>
  
</bio:treatment>