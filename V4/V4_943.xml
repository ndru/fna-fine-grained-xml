<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="treatment_page">472</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Adanson" date="unknown" rank="family">portulacaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">claytonia</taxon_name>
    <taxon_name authority="Donn ex Willdenow" date="1798" rank="species">perfoliata</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1186. 1798</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family portulacaceae;genus claytonia;species perfoliata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415745</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Montia</taxon_name>
    <taxon_name authority="(Donn ex Willdenow) Howell" date="unknown" rank="species">perfoliata</taxon_name>
    <taxon_hierarchy>genus Montia;species perfoliata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, with minute, tuberous bodies;</text>
      <biological_entity id="o13604" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o13605" name="body" name_original="bodies" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="minute" value_original="minute" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="tuberous" value_original="tuberous" />
      </biological_entity>
      <relation from="o13604" id="r1959" name="with" negation="false" src="d0_s0" to="o13605" />
    </statement>
    <statement id="d0_s1">
      <text>periderm absent.</text>
      <biological_entity id="o13606" name="periderm" name_original="periderm" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 5–50 cm.</text>
      <biological_entity id="o13607" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal leaves in suberect to erect, seldom flattened rosettes, petiolate, 1–30 cm, blade often with weak red pigmentation, broadly rhombic to deltate or reniform, 1–7 × 0.5–5 (–6) cm, apex obtuse to apiculate, mucro 1–3 mm;</text>
      <biological_entity id="o13608" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o13609" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="30" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13610" name="rosette" name_original="rosettes" src="d0_s3" type="structure">
        <character char_type="range_value" from="suberect" is_modifier="true" name="orientation" src="d0_s3" to="erect" />
        <character is_modifier="true" modifier="seldom" name="shape" src="d0_s3" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity id="o13611" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o13612" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s3" value="weak" value_original="weak" />
        <character is_modifier="true" name="coloration" src="d0_s3" value="red pigmentation" value_original="red pigmentation" />
        <character char_type="range_value" from="broadly rhombic" is_modifier="true" name="shape" src="d0_s3" to="deltate or reniform" />
        <character char_type="range_value" from="1" from_unit="cm" is_modifier="true" name="length" src="d0_s3" to="7" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" is_modifier="true" name="atypical_width" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" is_modifier="true" name="width" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="apiculate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13613" name="mucro" name_original="mucro" src="d0_s3" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s3" value="weak" value_original="weak" />
        <character is_modifier="true" name="coloration" src="d0_s3" value="red pigmentation" value_original="red pigmentation" />
        <character char_type="range_value" from="broadly rhombic" is_modifier="true" name="shape" src="d0_s3" to="deltate or reniform" />
        <character char_type="range_value" from="1" from_unit="cm" is_modifier="true" name="length" src="d0_s3" to="7" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" is_modifier="true" name="atypical_width" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" is_modifier="true" name="width" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="apiculate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o13609" id="r1960" name="in" negation="false" src="d0_s3" to="o13610" />
      <relation from="o13611" id="r1961" name="with" negation="false" src="d0_s3" to="o13612" />
      <relation from="o13611" id="r1962" name="with" negation="false" src="d0_s3" to="o13613" />
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves sessile, blade perfoliate or cleft or notched, 10 cm diam. or less.</text>
      <biological_entity id="o13614" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o13615" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o13616" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="perfoliate" value_original="perfoliate" />
        <character name="architecture" src="d0_s4" value="cleft or notched" value_original="cleft or notched" />
        <character name="diameter" src="d0_s4" value="less" value_original="less" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 1-bracteate;</text>
      <biological_entity id="o13617" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-bracteate" value_original="1-bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bract leaflike, 0.5–15 mm.</text>
      <biological_entity id="o13618" name="bract" name_original="bract" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="leaflike" value_original="leaflike" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 3–10 mm;</text>
      <biological_entity id="o13619" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 1.5–4 mm;</text>
      <biological_entity id="o13620" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals pink or white, 2–5 mm;</text>
      <biological_entity id="o13621" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovules 3.</text>
      <biological_entity id="o13622" name="ovule" name_original="ovules" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds 2–5 mm, shiny and smooth;</text>
      <biological_entity id="o13623" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s11" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>elaiosome 1–3 mm. 2n = 12, 24, 36, 48, 60.</text>
      <biological_entity id="o13624" name="elaiosome" name_original="elaiosome" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13625" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="12" value_original="12" />
        <character name="quantity" src="d0_s12" value="24" value_original="24" />
        <character name="quantity" src="d0_s12" value="36" value_original="36" />
        <character name="quantity" src="d0_s12" value="48" value_original="48" />
        <character name="quantity" src="d0_s12" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Ariz., Calif., Colo., Idaho, Mont., N.Mex., Nev., Oreg., Utah, Wash., Wyo.; Central America (Guatemala); adventive in Europe, Pacific Islands (New Zealand).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
        <character name="distribution" value="adventive in Europe" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Miner’s-lettuce</other_name>
  <discussion>Subspecies 3 (3 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal leaf rosettes erect, 20-50 cm; cauline leaf pairs connate into perfoliate discs, blade margins entire; seeds 3-5 mm</description>
      <determination>15c Claytonia perfoliata subsp. perfoliata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal leaf rosettes suberect to flattened, 2-30 cm; cauline leaf pairs perfoliate, blade margins notched or deeply cleft; seeds 2-4 mm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Basal leaf blades ovate to broadly rhombic, apex obtuse; leaf blades often beet red, gray-green, or purplish; epidermal gas pockets easily seen in field; seeds 3-4 mm</description>
      <determination>15a Claytonia perfoliata subsp. intermontana</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Basal leaf blades deltate, apex apiculate; leaf blades mostly green; not beet red; epidermal gas pockets usually not visible (except in populations of California Transverse Ranges); seeds 2-3 mm</description>
      <determination>15b Claytonia perfoliata subsp. mexicana</determination>
    </key_statement>
  </key>
</bio:treatment>