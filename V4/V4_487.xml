<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">266</other_info_on_meta>
    <other_info_on_meta type="mention_page">267</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">beta</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">vulgaris</taxon_name>
    <taxon_name authority="(Linnaeus) Arcangeli" date="1882" rank="subspecies">maritima</taxon_name>
    <place_of_publication>
      <publication_title>Comp. Fl. Ital.,</publication_title>
      <place_in_publication>593. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus beta;species vulgaris;subspecies maritima</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242415378</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Beta</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">maritima</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl. ed.</publication_title>
      <place_in_publication>2, 1: 322. 1762</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Beta;species maritima;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual to perennial;</text>
      <biological_entity id="o14049" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="annual" name="duration" src="d0_s0" to="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots brown, fibrous, sometimes swollen and woody, not fleshy.</text>
      <biological_entity id="o14050" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s1" value="swollen" value_original="swollen" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems procumbent to erect, simple, to 80 cm.</text>
      <biological_entity id="o14051" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s2" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="80" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole equaling blade;</text>
      <biological_entity id="o14052" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o14053" name="petiole" name_original="petiole" src="d0_s3" type="structure" />
      <biological_entity id="o14054" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="true" name="variability" src="d0_s3" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade oblanceolate, 1–10 × 0.5–8 cm.</text>
      <biological_entity id="o14055" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o14056" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cymes 1–3-flowered.</text>
    </statement>
    <statement id="d0_s6">
      <text>2n = 18.</text>
      <biological_entity id="o14057" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-3-flowered" value_original="1-3-flowered" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14058" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, sandy places near the coast and in waste areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" constraint="near the coast" />
        <character name="habitat" value="sandy places" constraint="near the coast" />
        <character name="habitat" value="the coast" />
        <character name="habitat" value="waste areas" modifier="and in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., N.J.; s Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" value="s Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1b.</number>
  <other_name type="common_name">Sea beet</other_name>
  
</bio:treatment>