<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Arthur C. Gibson</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">97</other_info_on_meta>
    <other_info_on_meta type="mention_page">187</other_info_on_meta>
    <other_info_on_meta type="treatment_page">186</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="(A. Berger) Riccobono" date="unknown" rank="genus">STENOCEREUS</taxon_name>
    <place_of_publication>
      <publication_title>Boll. Reale Orto Bot. Palermo</publication_title>
      <place_in_publication>8: 253. 1909</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus stenocereus;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek stenos, narrow, and Cereus, referring to the genus from which this segregate was removed</other_info_on_name>
    <other_info_on_name type="fna_id">131362</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Miller" date="unknown" rank="genus">Cereus</taxon_name>
    <taxon_name authority="A. Berger" date="unknown" rank="subgenus">Stenocereus</taxon_name>
    <place_of_publication>
      <publication_title>Rep. (Annual) Missouri Bot. Gard.</publication_title>
      <place_in_publication>16: 66, plate 3. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cereus;subgenus Stenocereus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Backeberg" date="unknown" rank="genus">Hertrichocereus</taxon_name>
    <taxon_hierarchy>genus Hertrichocereus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Britton &amp; Rose" date="unknown" rank="genus">Machaerocereus</taxon_name>
    <taxon_hierarchy>genus Machaerocereus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees or shrubs, erect, arching, or procumbent, mostly branched from base.</text>
      <biological_entity id="o11249" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="arching" value_original="arching" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="procumbent" value_original="procumbent" />
        <character constraint="from base" constraintid="o11251" is_modifier="false" modifier="mostly" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character name="growth_form" value="tree" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="arching" value_original="arching" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="procumbent" value_original="procumbent" />
        <character constraint="from base" constraintid="o11251" is_modifier="false" modifier="mostly" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o11251" name="base" name_original="base" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Roots adventitious if plant procumbent.</text>
      <biological_entity id="o11252" name="root" name_original="roots" src="d0_s1" type="structure">
        <character constraint="if plant" constraintid="o11253" is_modifier="false" name="derivation" src="d0_s1" value="adventitious" value_original="adventitious" />
      </biological_entity>
      <biological_entity id="o11253" name="plant" name_original="plant" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems unsegmented, often more narrow at branch bases and where showing growth increments, green to bluish green [or whitish from surface wax or purple-tinged from pigment], columnar, ribbed, [50–] 100–500+ × [5–] (9–) 11–18 [–20] cm;</text>
      <biological_entity id="o11254" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="unsegmented" value_original="unsegmented" />
        <character constraint="at branch bases" constraintid="o11255" is_modifier="false" modifier="often" name="size_or_width" src="d0_s2" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity constraint="branch" id="o11255" name="base" name_original="bases" src="d0_s2" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s2" to="bluish green" />
        <character is_modifier="false" name="shape" src="d0_s2" value="columnar" value_original="columnar" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="ribbed" value_original="ribbed" />
        <character char_type="range_value" from="50" from_unit="cm" name="atypical_length" src="d0_s2" to="100" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="100" from_unit="cm" name="length" src="d0_s2" to="500" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_width" src="d0_s2" to="11" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="20" to_unit="cm" />
        <character char_type="range_value" from="11" from_unit="cm" name="width" src="d0_s2" to="18" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="growth" id="o11256" name="increment" name_original="increments" src="d0_s2" type="structure" />
      <relation from="o11255" id="r1640" name="showing" negation="false" src="d0_s2" to="o11256" />
    </statement>
    <statement id="d0_s3">
      <text>ribs 4–20, rounded, margins nearly flat to sinusoidal or strongly tuberculate, with transverse folds between areoles or not;</text>
      <biological_entity id="o11257" name="rib" name_original="ribs" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s3" to="20" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o11258" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="nearly flat" name="shape" src="d0_s3" to="sinusoidal" />
        <character is_modifier="false" modifier="strongly; strongly" name="relief" src="d0_s3" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity constraint="between areoles" constraintid="o11260" id="o11259" name="fold" name_original="folds" src="d0_s3" type="structure" constraint_original="between  areoles, ">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s3" value="transverse" value_original="transverse" />
      </biological_entity>
      <biological_entity id="o11260" name="areole" name_original="areoles" src="d0_s3" type="structure" />
      <relation from="o11258" id="r1641" name="with" negation="false" src="d0_s3" to="o11259" />
    </statement>
    <statement id="d0_s4">
      <text>areoles 0.5–2.5 cm apart, circular to horizontally elliptic, young hairs whitish or tan to reddish-brown, aging darker;</text>
      <biological_entity id="o11261" name="areole" name_original="areoles" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="apart" value_original="apart" />
        <character char_type="range_value" from="circular" name="arrangement_or_shape" src="d0_s4" to="horizontally elliptic" />
      </biological_entity>
      <biological_entity id="o11262" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="young" value_original="young" />
        <character char_type="range_value" from="tan" name="coloration" src="d0_s4" to="reddish-brown" />
        <character is_modifier="false" name="life_cycle" src="d0_s4" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="darker" value_original="darker" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>areolar glands present or absent;</text>
      <biological_entity constraint="areolar" id="o11263" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cortex mucilaginous or not, mucilage throughout and slippery or restricted to sacs in outer cortex, green to white or yellowish;</text>
      <biological_entity id="o11264" name="cortex" name_original="cortex" src="d0_s6" type="structure">
        <character is_modifier="false" name="coating" src="d0_s6" value="mucilaginous" value_original="mucilaginous" />
        <character name="coating" src="d0_s6" value="not" value_original="not" />
      </biological_entity>
      <biological_entity id="o11265" name="mucilage" name_original="mucilage" src="d0_s6" type="structure">
        <character is_modifier="false" name="coating" src="d0_s6" value="slippery" value_original="slippery" />
        <character name="coating" src="d0_s6" value="restricted to sacs in outer cortex" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s6" to="white or yellowish" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pith mucilaginous or not, 1–8 cm wide.</text>
      <biological_entity id="o11266" name="pith" name_original="pith" src="d0_s7" type="structure">
        <character is_modifier="false" name="coating" src="d0_s7" value="mucilaginous" value_original="mucilaginous" />
        <character name="coating" src="d0_s7" value="not" value_original="not" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s7" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spines to 28 per areole, hemispherically arranged, initially rose to magenta, becoming darker or fading to grayish white, relatively thin and brittle to stout and very hard;</text>
      <biological_entity id="o11267" name="spine" name_original="spines" src="d0_s8" type="structure">
        <character constraint="per areole" constraintid="o11268" name="quantity" src="d0_s8" value="28" value_original="28" />
        <character is_modifier="false" modifier="hemispherically" name="arrangement" notes="" src="d0_s8" value="arranged" value_original="arranged" />
        <character char_type="range_value" from="initially rose" name="coloration" src="d0_s8" to="magenta" />
        <character char_type="range_value" from="fading" modifier="becoming" name="coloration" src="d0_s8" to="grayish white" />
        <character is_modifier="false" modifier="relatively" name="width" src="d0_s8" value="thin" value_original="thin" />
        <character char_type="range_value" from="brittle" name="fragility" src="d0_s8" to="stout" />
        <character is_modifier="false" modifier="very" name="texture" src="d0_s8" value="hard" value_original="hard" />
      </biological_entity>
      <biological_entity id="o11268" name="areole" name_original="areole" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>radial spines to 3.5 cm;</text>
      <biological_entity id="o11269" name="spine" name_original="spines" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="radial" value_original="radial" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>central spines usually weakly defined or absent, sometimes broad and downward pointing, to 7.5 cm.</text>
      <biological_entity constraint="central" id="o11270" name="spine" name_original="spines" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually weakly" name="prominence" src="d0_s10" value="defined" value_original="defined" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="width" src="d0_s10" value="broad" value_original="broad" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="downward" value_original="downward" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="pointing" value_original="pointing" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s10" to="7.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers diurnal or nocturnal, produced only once on areole [or not], terminal to lateral, funnelform [to tubular];</text>
      <biological_entity id="o11271" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="diurnal" value_original="diurnal" />
        <character is_modifier="false" name="duration" src="d0_s11" value="nocturnal" value_original="nocturnal" />
        <character char_type="range_value" from="terminal" name="position" src="d0_s11" to="lateral" />
      </biological_entity>
      <biological_entity id="o11272" name="areole" name_original="areole" src="d0_s11" type="structure" />
      <relation from="o11271" id="r1642" name="produced" negation="false" src="d0_s11" to="o11272" />
    </statement>
    <statement id="d0_s12">
      <text>flower tubes2–11 cm;</text>
      <biological_entity id="o11273" name="flower" name_original="flower" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s12" to="11" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>outer tepals with dark green to purplish centers but light margins, margins entire;</text>
      <biological_entity constraint="outer" id="o11274" name="tepal" name_original="tepals" src="d0_s13" type="structure" />
      <biological_entity id="o11275" name="center" name_original="centers" src="d0_s13" type="structure">
        <character char_type="range_value" from="dark green" is_modifier="true" name="coloration" src="d0_s13" to="purplish" />
      </biological_entity>
      <biological_entity id="o11276" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="light" value_original="light" />
      </biological_entity>
      <biological_entity id="o11277" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o11274" id="r1643" name="with" negation="false" src="d0_s13" to="o11275" />
    </statement>
    <statement id="d0_s14">
      <text>inner tepals white to rose-red [or yellow];</text>
      <biological_entity constraint="inner" id="o11278" name="tepal" name_original="tepals" src="d0_s14" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s14" to="rose-red" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary globose to barrel-shaped, similar to locule shape;</text>
      <biological_entity id="o11279" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s15" to="barrel-shaped" />
      </biological_entity>
      <biological_entity id="o11280" name="locule" name_original="locule" src="d0_s15" type="structure" />
      <relation from="o11279" id="r1644" name="to" negation="false" src="d0_s15" to="o11280" />
    </statement>
    <statement id="d0_s16">
      <text>scales persistent, reddish or green with red tips, small, triangular;</text>
      <biological_entity id="o11281" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character is_modifier="false" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="reddish" value_original="reddish" />
        <character constraint="with tips" constraintid="o11282" is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character is_modifier="false" name="size" notes="" src="d0_s16" value="small" value_original="small" />
        <character is_modifier="false" name="shape" src="d0_s16" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o11282" name="tip" name_original="tips" src="d0_s16" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s16" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>hairs and spines often present;</text>
      <biological_entity id="o11283" name="hair" name_original="hairs" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o11284" name="spine" name_original="spines" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigma lobes 5–15, inserted or exserted;</text>
      <biological_entity constraint="stigma" id="o11285" name="lobe" name_original="lobes" src="d0_s18" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s18" to="15" />
        <character is_modifier="false" name="position" src="d0_s18" value="inserted" value_original="inserted" />
        <character is_modifier="false" name="position" src="d0_s18" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>nectar chamber open.</text>
      <biological_entity constraint="nectar" id="o11286" name="chamber" name_original="chamber" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Fruits indehiscent or sometimes splitting irregularly, dark red to purplish green or green, spheric [to ovoid], 30–100 mm, fleshy to juicy [or somewhat dry], bearing deciduous spine clusters;</text>
      <biological_entity id="o11287" name="fruit" name_original="fruits" src="d0_s20" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s20" value="indehiscent" value_original="indehiscent" />
        <character is_modifier="false" modifier="sometimes; irregularly" name="architecture_or_dehiscence" src="d0_s20" value="splitting" value_original="splitting" />
        <character char_type="range_value" from="dark red" name="coloration" src="d0_s20" to="purplish green or green" />
        <character is_modifier="false" name="shape" src="d0_s20" value="spheric" value_original="spheric" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s20" to="100" to_unit="mm" />
        <character char_type="range_value" from="fleshy" name="texture" src="d0_s20" to="juicy" />
        <character is_modifier="false" name="arrangement" src="d0_s20" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o11288" name="spine" name_original="spine" src="d0_s20" type="structure">
        <character is_modifier="true" name="duration" src="d0_s20" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <relation from="o11287" id="r1645" name="bearing" negation="false" src="d0_s20" to="o11288" />
    </statement>
    <statement id="d0_s21">
      <text>pulp red, special pigment cells present;</text>
      <biological_entity id="o11289" name="pulp" name_original="pulp" src="d0_s21" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s21" value="red" value_original="red" />
      </biological_entity>
      <biological_entity id="o11290" name="cell" name_original="cells" src="d0_s21" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s21" value="pigment" value_original="pigment" />
        <character is_modifier="false" name="presence" src="d0_s21" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>floral remnant persistent or deciduous.</text>
      <biological_entity constraint="floral" id="o11291" name="remnant" name_original="remnant" src="d0_s22" type="structure">
        <character is_modifier="false" name="duration" src="d0_s22" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s22" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds brownish black or black, oblong to subspheric with oblique hilum, 0.7–3 mm, dull or rarely glossy;</text>
      <biological_entity id="o11292" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s23" value="brownish black" value_original="brownish black" />
        <character is_modifier="false" name="coloration" src="d0_s23" value="black" value_original="black" />
        <character char_type="range_value" constraint="with hilum" constraintid="o11293" from="oblong" name="shape" src="d0_s23" to="subspheric" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" notes="" src="d0_s23" to="3" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s23" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="rarely" name="reflectance" src="d0_s23" value="glossy" value_original="glossy" />
      </biological_entity>
      <biological_entity id="o11293" name="hilum" name_original="hilum" src="d0_s23" type="structure">
        <character is_modifier="true" name="orientation_or_shape" src="d0_s23" value="oblique" value_original="oblique" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>testa cells convex or nearly flat, with prominent to faint, raised waxy striations or not.</text>
      <biological_entity id="o11295" name="striation" name_original="striations" src="d0_s24" type="structure">
        <character is_modifier="true" name="texture" src="d0_s24" value="ceraceous" value_original="waxy" />
      </biological_entity>
      <relation from="o11294" id="r1646" name="raised" negation="false" src="d0_s24" to="o11295" />
    </statement>
    <statement id="d0_s25">
      <text>x = 11.</text>
      <biological_entity constraint="testa" id="o11294" name="cell" name_original="cells" src="d0_s24" type="structure">
        <character is_modifier="false" name="shape" src="d0_s24" value="convex" value_original="convex" />
        <character is_modifier="false" modifier="nearly" name="prominence_or_shape" src="d0_s24" value="flat" value_original="flat" />
        <character char_type="range_value" from="prominent" name="prominence" src="d0_s24" to="faint" />
      </biological_entity>
      <biological_entity constraint="x" id="o11296" name="chromosome" name_original="" src="d0_s25" type="structure">
        <character name="quantity" src="d0_s25" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>s Ariz., Mexico, West Indies, coastal Central America, n South America, cultivated and naturalized elsewhere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="s Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="coastal Central America" establishment_means="native" />
        <character name="distribution" value="n South America" establishment_means="native" />
        <character name="distribution" value="cultivated and naturalized elsewhere" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <discussion>Species ca. 20 (1 in the flora).</discussion>
  <discussion>During the nineteenth century, the ribbed columnar cacti, numbering in the hundreds, were generally classified as species of Cereus. In the early twentieth century, however, Cereus, in the broadest sense, was subdivided into many smaller and more homogeneous units, initially by N. L. Britton and J. N. Rose (1909, 1919–1923). The phylogenetic relationships of North American columnar species were clarified by studies of silica bodies in the epidermis and hypodermis covering the stems of certain Mexican species, distinctive pigment cells, called pearl cells, in the fruit pulp, and sugar-bearing oleanane triterpenes in stem tissues (A. C. Gibson and K. E. Horak 1978). Species possessing all three derived characters were removed from Lemaireocereus, Machaerocereus, Rathbunia, Hertrichocereus, Ritterocereus, and Marshallocereus and placed into the genus Stenocereus, which was further emended by removing species without the shared characters.</discussion>
  <discussion>Several of the Central American species assigned to Stenocereus by E. F. Anderson (2001) are too poorly studied to know whether or not they have the diagnostic characters for the genus. A carefully done DNA phylogeny for all taxa with possible inclusion in Stenocereus is needed, especially to define more precisely the phylogenetic lineages and patterns of speciation (R. S. Wallace and A. C. Gibson 2002).</discussion>
  <references>
    <reference>Cornejo, D. O. and B. B. Simpson. 1997. Analysis of form and function in North American columnar cacti (tribe Pachycereae). Amer. J. Bot. 84: 1482–1501.</reference>
    <reference>Gibson, A. C. 1988. The systematics and evolution of subtribe Stenocereinae. 1. Composition and definition of the subtribe. Cact. Succ. J. (Los Angeles) 60: 11–16.</reference>
    <reference>Gibson, A. C. 1990. The systematics and evolution of subtribe Stenocereinae. 8. Organ pipe cactus and its closest relatives. Cact. Succ. J. (Los Angeles) 62: 13–24.</reference>
    <reference>Gibson, A. C. and K. E. Horak. 1978. Systematic anatomy and phylogeny of Mexican columnar cacti. Ann. Missouri Bot. Gard. 65: 999–1057.</reference>
    <reference>Parker, K. C. 1987. Seedcrop characteristics and minimum reproductive size of organ pipe cactus (Stenocereus thurberi) in southern Arizona. Madroño 34: 294–303.</reference>
    <reference>Parker, K. C. 1987b. Site-related demographic patterns of organ pipe cactus populations in southern Arizona. Bull. Torrey Bot. Club 114: 149–155.</reference>
  </references>
  
</bio:treatment>