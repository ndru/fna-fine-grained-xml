<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Allan D. Zimmerman,Bruce D. Parfitt</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">97</other_info_on_meta>
    <other_info_on_meta type="mention_page">98</other_info_on_meta>
    <other_info_on_meta type="mention_page">206</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="mention_page">21</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1922" rank="genus">FEROCACTUS</taxon_name>
    <place_of_publication>
      <publication_title>Cact.</publication_title>
      <place_in_publication>3: 123, figs. 128-153b, plates 12-16. 1922</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus ferocactus;</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin ferus, fierce or wild, referring to the horrid spines, and Cactus, the genus from which this segregate was removed</other_info_on_name>
    <other_info_on_name type="fna_id">112736</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect or leaning, unbranched (rarely branched after apical injury) or sparingly branched from base with age in F. hamatacanthus and F. viridescens [or many branched], deep-seated in substrate only in F. viridescens.</text>
      <biological_entity id="o13904" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="leaning" value_original="leaning" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character constraint="from base" constraintid="o13905" is_modifier="false" modifier="sparingly" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o13905" name="base" name_original="base" src="d0_s0" type="structure" />
      <biological_entity id="o13906" name="age" name_original="age" src="d0_s0" type="structure" />
      <biological_entity id="o13907" name="viridescens" name_original="viridescens" src="d0_s0" type="taxon_name">
        <character constraint="in substrate; in f" is_modifier="false" modifier="in f" name="location" src="d0_s0" value="deep-seated" value_original="deep-seated" />
      </biological_entity>
      <biological_entity id="o13908" name="viridescens" name_original="viridescens" src="d0_s0" type="taxon_name" />
      <relation from="o13905" id="r2014" name="with" negation="false" src="d0_s0" to="o13906" />
    </statement>
    <statement id="d0_s1">
      <text>Roots diffuse.</text>
      <biological_entity id="o13909" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="density" src="d0_s1" value="diffuse" value_original="diffuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems unsegmented, green, ovoid, spheric, depressed-spheric, or cylindric [or flat-topped and flush with soil surface], (0–) 10–150 (–300) × 7.5–80 (–100) cm, glabrous;</text>
      <biological_entity id="o13910" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="unsegmented" value_original="unsegmented" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s2" value="spheric" value_original="spheric" />
        <character is_modifier="false" name="shape" src="d0_s2" value="depressed-spheric" value_original="depressed-spheric" />
        <character is_modifier="false" name="shape" src="d0_s2" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s2" value="depressed-spheric" value_original="depressed-spheric" />
        <character is_modifier="false" name="shape" src="d0_s2" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="0" from_unit="cm" name="atypical_length" src="d0_s2" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="150" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="300" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s2" to="150" to_unit="cm" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="100" to_unit="cm" />
        <character char_type="range_value" from="7.5" from_unit="cm" name="width" src="d0_s2" to="80" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ribs [8–] 10–32 (–40), very prominent, rib crests straight or undulate, uninterrupted (deeply crenate in F. hamatacanthus or sometimes nearly tuberculate in immature plants);</text>
      <biological_entity id="o13911" name="rib" name_original="ribs" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s3" to="10" to_inclusive="false" />
        <character char_type="range_value" from="32" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="40" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s3" to="32" />
        <character is_modifier="false" modifier="very" name="prominence" src="d0_s3" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity constraint="rib" id="o13912" name="crest" name_original="crests" src="d0_s3" type="structure">
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s3" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="uninterrupted" value_original="uninterrupted" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>areoles nearly circular to oblong or elliptic, with fertile portion as short adaxial prolongation confluent with spine cluster or connected to spine cluster by very broad groove, short woolly;</text>
      <biological_entity id="o13913" name="areole" name_original="areoles" src="d0_s4" type="structure">
        <character char_type="range_value" from="nearly circular" name="shape" src="d0_s4" to="oblong or elliptic" />
        <character constraint="to spine" constraintid="o13917" is_modifier="false" name="fusion" src="d0_s4" value="connected" value_original="connected" />
        <character is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="woolly" value_original="woolly" />
      </biological_entity>
      <biological_entity id="o13914" name="portion" name_original="portion" src="d0_s4" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s4" value="fertile" value_original="fertile" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o13915" name="portion" name_original="portion" src="d0_s4" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character constraint="with spine" constraintid="o13916" is_modifier="false" name="arrangement" src="d0_s4" value="confluent" value_original="confluent" />
      </biological_entity>
      <biological_entity id="o13916" name="spine" name_original="spine" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o13917" name="spine" name_original="spine" src="d0_s4" type="structure">
        <character constraint="by groove" constraintid="o13918" is_modifier="false" name="arrangement" src="d0_s4" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o13918" name="groove" name_original="groove" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="very" name="width" src="d0_s4" value="broad" value_original="broad" />
      </biological_entity>
      <relation from="o13913" id="r2015" name="with" negation="false" src="d0_s4" to="o13914" />
      <relation from="o13914" id="r2016" name="as" negation="false" src="d0_s4" to="o13915" />
    </statement>
    <statement id="d0_s5">
      <text>areolar glands usually present, sometimes very short and inconspicuous and/or short-lived, adaxial in areoles, ovoid to cylindric, blunt, peglike;</text>
      <biological_entity constraint="areolar" id="o13919" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes very" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="false" name="prominence" src="d0_s5" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="duration" src="d0_s5" value="short-lived" value_original="short-lived" />
        <character constraint="in areoles" constraintid="o13920" is_modifier="false" name="position" src="d0_s5" value="adaxial" value_original="adaxial" />
        <character char_type="range_value" from="ovoid" name="shape" notes="" src="d0_s5" to="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s5" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="peglike" value_original="peglike" />
      </biological_entity>
      <biological_entity id="o13920" name="areole" name_original="areoles" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>cortex and pith hard, not mucilaginous.</text>
      <biological_entity id="o13921" name="cortex" name_original="cortex" src="d0_s6" type="structure">
        <character is_modifier="false" name="texture" src="d0_s6" value="hard" value_original="hard" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s6" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
      <biological_entity id="o13922" name="pith" name_original="pith" src="d0_s6" type="structure">
        <character is_modifier="false" name="texture" src="d0_s6" value="hard" value_original="hard" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s6" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spines 6–32 per areole, yellow, brown, or reddish to salmon with color hidden by very thin, light gray layer, usually large and coarse, annulate-ridged (smooth in F. hamatacanthus), longest spines 30–130 (–170) × 0.5–4.5 mm;</text>
      <biological_entity id="o13923" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o13924" from="6" name="quantity" src="d0_s7" to="32" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s7" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s7" to="salmon" />
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s7" to="salmon" />
        <character constraint="by layer" constraintid="o13925" is_modifier="false" name="prominence" src="d0_s7" value="hidden" value_original="hidden" />
      </biological_entity>
      <biological_entity id="o13924" name="areole" name_original="areole" src="d0_s7" type="structure" />
      <biological_entity id="o13925" name="layer" name_original="layer" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="very" name="width" src="d0_s7" value="thin" value_original="thin" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="light gray" value_original="light gray" />
      </biological_entity>
      <biological_entity id="o13926" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="annulate-ridged" value_original="annulate-ridged" />
      </biological_entity>
      <biological_entity constraint="longest" id="o13927" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character char_type="range_value" from="130" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="170" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s7" to="130" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s7" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>radial spines 6–25 per areole, straight to curved, or crinkly bristles, 15–70 mm;</text>
      <biological_entity id="o13928" name="spine" name_original="spines" src="d0_s8" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s8" value="radial" value_original="radial" />
        <character char_type="range_value" constraint="per areole" constraintid="o13929" from="6" name="quantity" src="d0_s8" to="25" />
        <character char_type="range_value" from="straight" name="course" notes="" src="d0_s8" to="curved" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="70" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13929" name="areole" name_original="areole" src="d0_s8" type="structure" />
      <biological_entity id="o13930" name="bristle" name_original="bristles" src="d0_s8" type="structure">
        <character is_modifier="true" name="pubescence_or_shape" src="d0_s8" value="crinkly" value_original="crinkly" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>central spines 1–4 per areole, flattened, angled, or terete, all or only largest adaxial spine hooked.</text>
      <biological_entity constraint="central" id="o13931" name="spine" name_original="spines" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o13932" from="1" name="quantity" src="d0_s9" to="4" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="shape" src="d0_s9" value="angled" value_original="angled" />
        <character is_modifier="false" name="shape" src="d0_s9" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s9" value="angled" value_original="angled" />
        <character is_modifier="false" name="shape" src="d0_s9" value="terete" value_original="terete" />
      </biological_entity>
      <biological_entity id="o13932" name="areole" name_original="areole" src="d0_s9" type="structure" />
      <biological_entity constraint="adaxial" id="o13933" name="spine" name_original="spine" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="only" name="size" src="d0_s9" value="largest" value_original="largest" />
        <character is_modifier="false" name="shape" src="d0_s9" value="hooked" value_original="hooked" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers diurnal, near stem apex, on large plants often several cm from stem apex, at adaxial edges of areoles (or at axillary end of short areolar groove), funnelform, 2.5–8.5 (–10) cm;</text>
      <biological_entity id="o13934" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="diurnal" value_original="diurnal" />
        <character char_type="range_value" from="8.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s10" to="10" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" notes="" src="d0_s10" to="8.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="stem" id="o13935" name="apex" name_original="apex" src="d0_s10" type="structure" />
      <biological_entity id="o13936" name="plant" name_original="plants" src="d0_s10" type="structure">
        <character is_modifier="true" name="size" src="d0_s10" value="large" value_original="large" />
        <character constraint="from stem apex" constraintid="o13937" is_modifier="false" modifier="often" name="quantity" src="d0_s10" value="several" value_original="several" />
      </biological_entity>
      <biological_entity constraint="stem" id="o13937" name="apex" name_original="apex" src="d0_s10" type="structure" />
      <biological_entity constraint="adaxial" id="o13938" name="edge" name_original="edges" src="d0_s10" type="structure" />
      <biological_entity id="o13939" name="areole" name_original="areoles" src="d0_s10" type="structure" />
      <relation from="o13934" id="r2017" name="near" negation="false" src="d0_s10" to="o13935" />
      <relation from="o13934" id="r2018" name="on" negation="false" src="d0_s10" to="o13936" />
      <relation from="o13934" id="r2019" name="at" negation="false" src="d0_s10" to="o13938" />
      <relation from="o13938" id="r2020" name="part_of" negation="false" src="d0_s10" to="o13939" />
    </statement>
    <statement id="d0_s11">
      <text>outer tepals margins entire [fringed];</text>
      <biological_entity constraint="tepals" id="o13940" name="margin" name_original="margins" src="d0_s11" type="structure" constraint_original="outer tepals">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>inner tepals yellow, orange, red, or purple (or white with purplish midstripes);</text>
      <biological_entity constraint="inner" id="o13941" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary scaly, hairless, spineless;</text>
      <biological_entity id="o13942" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s13" value="scaly" value_original="scaly" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="hairless" value_original="hairless" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="spineless" value_original="spineless" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>scales numerous, broadly rounded to lanceolate, margins ± scarious, minutely to conspicuously fimbriate or denticulate, obtuse;</text>
      <biological_entity id="o13943" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s14" value="numerous" value_original="numerous" />
        <character char_type="range_value" from="broadly rounded" name="shape" src="d0_s14" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o13944" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s14" value="scarious" value_original="scarious" />
        <character is_modifier="false" modifier="minutely to conspicuously; conspicuously" name="shape" src="d0_s14" value="fimbriate" value_original="fimbriate" />
        <character is_modifier="false" name="shape" src="d0_s14" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="shape" src="d0_s14" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma lobes 13–20, yellow, orange, or red, unusually long, 7.5–15 mm.</text>
      <biological_entity constraint="stigma" id="o13945" name="lobe" name_original="lobes" src="d0_s15" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s15" to="20" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="red" value_original="red" />
        <character is_modifier="false" modifier="unusually" name="length_or_size" src="d0_s15" value="long" value_original="long" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" src="d0_s15" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits weakly dehiscent through basal pore (indehiscent in F. hamatacanthus), green, yellow, brownish, or dull purple-red [to bright red], spheric, ovoid, or cylindric, 20–60 × 10–40 mm, thick walled, leathery, hollow except for seeds and long persistent (thin walled and juicy in F. hamatacanthus), with numerous scales, spineless;</text>
      <biological_entity id="o13946" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character constraint="through basal pore" constraintid="o13947" is_modifier="false" modifier="weakly" name="dehiscence" src="d0_s16" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s16" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="purple-red" value_original="purple-red" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="purple-red" value_original="purple-red" />
        <character is_modifier="false" name="shape" src="d0_s16" value="spheric" value_original="spheric" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s16" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s16" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s16" to="60" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s16" to="40" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s16" value="thick" value_original="thick" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="walled" value_original="walled" />
        <character is_modifier="false" name="texture" src="d0_s16" value="leathery" value_original="leathery" />
        <character constraint="except-for seeds" constraintid="o13948" is_modifier="false" name="architecture" src="d0_s16" value="hollow" value_original="hollow" />
        <character is_modifier="false" name="length_or_size" notes="" src="d0_s16" value="long" value_original="long" />
        <character is_modifier="false" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s16" value="spineless" value_original="spineless" />
      </biological_entity>
      <biological_entity id="o13947" name="pore" name_original="pore" src="d0_s16" type="structure">
        <character is_modifier="true" modifier="through" name="position" src="d0_s16" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o13948" name="seed" name_original="seeds" src="d0_s16" type="structure" />
      <biological_entity id="o13949" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="numerous" value_original="numerous" />
      </biological_entity>
      <relation from="o13946" id="r2021" name="with" negation="false" src="d0_s16" to="o13949" />
    </statement>
    <statement id="d0_s17">
      <text>pulp whitish or, if fruit hollow, absent;</text>
      <biological_entity id="o13950" name="pulp" name_original="pulp" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="whitish" value_original="whitish" />
        <character name="coloration" src="d0_s17" value="," value_original="," />
      </biological_entity>
      <biological_entity id="o13951" name="fruit" name_original="fruit" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="hollow" value_original="hollow" />
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>floral remnant persistent, dried tepals often remaining distinct, papery, and straight or wavy.</text>
      <biological_entity constraint="floral" id="o13952" name="remnant" name_original="remnant" src="d0_s18" type="structure">
        <character is_modifier="false" name="duration" src="d0_s18" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o13953" name="tepal" name_original="tepals" src="d0_s18" type="structure">
        <character is_modifier="true" name="condition" src="d0_s18" value="dried" value_original="dried" />
        <character is_modifier="false" name="fusion" src="d0_s18" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="texture" src="d0_s18" value="papery" value_original="papery" />
        <character is_modifier="false" name="course" src="d0_s18" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s18" value="wavy" value_original="wavy" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds black or dark-brown, spheric to subreniform, 1–2.9 [–3.2] mm;</text>
      <biological_entity id="o13954" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s19" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="spheric" name="shape" src="d0_s19" to="subreniform" />
        <character char_type="range_value" from="2.9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s19" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s19" to="2.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>testa cells flat, weakly convex, concave, or flat with central depressions, pitted or reticulate.</text>
      <biological_entity constraint="central" id="o13956" name="depression" name_original="depressions" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>x = 11.</text>
      <biological_entity constraint="testa" id="o13955" name="cell" name_original="cells" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s20" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s20" value="concave" value_original="concave" />
        <character is_modifier="false" name="shape" src="d0_s20" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s20" value="concave" value_original="concave" />
        <character constraint="with central depressions" constraintid="o13956" is_modifier="false" name="shape" src="d0_s20" value="flat" value_original="flat" />
        <character is_modifier="false" name="relief" src="d0_s20" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="relief" src="d0_s20" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity constraint="x" id="o13957" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>33.</number>
  <other_name type="common_name">Barrel cactus</other_name>
  <other_name type="common_name">biznaga</other_name>
  <discussion>Species 25–30 (5 in the flora).</discussion>
  <discussion>Recent molecular studies by C. A. Butterworth et al. (2002) suggested that Ferocactus may be an artificial assemblage of primitive species not closely related to each other.</discussion>
  <references>
    <reference>Lindsay, G. S. 1956. Taxonomy and Ecology of the Genus Ferocactus. Ph.D. dissertation. Stanford University.</reference>
    <reference>Taylor, N. P. 1984. A review of Ferocactus Britton &amp; Rose. Bradleya 2: 19–38.</reference>
    <reference>Taylor, N. P. and J. Y. Clark. 1983. Seed morphology and classification in Ferocactus subg. Ferocactus. Bradleya 1: 1–16.</reference>
    <reference>Taylor, N. P. 1979. Notes on Ferocactus Britton &amp; Rose. Cact. Succ. J. Gr. Brit. 41: 88–94.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems 7.5-30 cm diam.; central spines very flexible, sometimes almost papery, (0.5-)1-3 mm wide; fruits thin walled, soft, green or maroon, indehiscent, locule juicy; El Paso and eastward in Texas</description>
      <determination>5 Ferocactus hamatacanthus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems 10-65(-100) cm diam. or, if smaller at maturity, then from coastal California; central spines rigid, 1.5-4(-7) mm wide; fruits thick walled, leathery or fleshy, bright yellow (rarely reddish), ± dehiscent through small basal pore (abscission scar), locule dry; El Paso, Texas, and westward to California</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Areoles with spines of approximately uniform thickness, all robust and rigid, more than 1 mm thick; flowers brilliant red inside and outside [yellow in Mexico]</description>
      <determination>4 Ferocactus emoryi</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Areoles with slender, bristlelike spines less than 1mm thick in addition to thick, rigid spines, or if spines all rigid (in "F. eastwoodiae" form of F. cylindraceus), then flowers yellow inside and yellow to maroon outside</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Ribs 13-21[-34]; stems usually 10-20 × 10-20 cm; coastal California</description>
      <determination>2 Ferocactus viridescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Ribs (18-)21-31(-40); stems usually 45-150 × 25-65 cm; California deserts eastward</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Seeds nearly smooth with slightly raised reticulations; flower color similar inside and outside: red, orange, or yellow; largest central spine of each areole usually strongly hooked; adult plants usually leaning southward; flowering late summer; c Arizona eastward</description>
      <determination>1 Ferocactus wislizeni</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Seeds pitted; flower maroon (or partly so) outside, yellow inside; largest central spine of each areole curved or twisted, not strongly hooked; adult plants usu- ally erect; flowering spring to early summer; sc Arizona westward</description>
      <determination>3 Ferocactus cylindraceus</determination>
    </key_statement>
  </key>
</bio:treatment>