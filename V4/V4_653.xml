<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">325</other_info_on_meta>
    <other_info_on_meta type="mention_page">326</other_info_on_meta>
    <other_info_on_meta type="treatment_page">343</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">atriplex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Atriplex</taxon_name>
    <taxon_name authority="(Standley) Ulbrich in H. G. A. Engler et al." date="1934" rank="section">Semibaccata</taxon_name>
    <taxon_name authority="I. Verdoorn" date="1954" rank="species">suberecta</taxon_name>
    <place_of_publication>
      <publication_title>Bothalia</publication_title>
      <place_in_publication>6: 418, figs. 2, 3(2). 1954</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus atriplex;subgenus atriplex;section semibaccata;species suberecta;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242415516</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or perennial, sprawling to ascending, 2–6 dm, branching from densely scaly base.</text>
      <biological_entity id="o18156" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="sprawling" name="orientation" src="d0_s0" to="ascending" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="6" to_unit="dm" />
        <character constraint="from base" constraintid="o18157" is_modifier="false" name="architecture" src="d0_s0" value="branching" value_original="branching" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o18157" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="densely" name="architecture_or_pubescence" src="d0_s0" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves mostly alternate, shortly petiolate;</text>
      <biological_entity id="o18158" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s1" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade narrow to broadly rhomboid, lanceolate, oblanceolate, or elliptic, (8–) 12–35 (–42) × 6–16 mm, thin, margin coarsely and irregularly dentate, glabrescent adaxially, somewhat scurfy abaxially.</text>
      <biological_entity id="o18159" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s2" value="narrow" value_original="narrow" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="rhomboid" value_original="rhomboid" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_length" src="d0_s2" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="42" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s2" to="35" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s2" to="16" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s2" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o18160" name="margin" name_original="margin" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s2" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" modifier="somewhat; abaxially" name="pubescence" src="d0_s2" value="scurfy" value_original="scurfy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Staminate flowers in subterminal, axillary glomerules.</text>
      <biological_entity id="o18161" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o18162" name="glomerule" name_original="glomerules" src="d0_s3" type="structure">
        <character is_modifier="true" name="position" src="d0_s3" value="subterminal" value_original="subterminal" />
      </biological_entity>
      <relation from="o18161" id="r2609" name="in" negation="false" src="d0_s3" to="o18162" />
    </statement>
    <statement id="d0_s4">
      <text>Pistillate flowers in axillary glomerules.</text>
      <biological_entity constraint="axillary" id="o18164" name="glomerule" name_original="glomerules" src="d0_s4" type="structure" />
      <relation from="o18163" id="r2610" name="in" negation="false" src="d0_s4" to="o18164" />
    </statement>
    <statement id="d0_s5">
      <text>Fruiting bracteoles sessile or on the stipe to 0.5 mm, rhombic to obovate, almost flat to convex, 2.2–4 × 1.7–2.7 mm, thin or somewhat thickened in age, connate in basal 1/2, margin entire in basal 1/2, 2–4-toothed in distal 1/2, apex acute, scurfy.</text>
      <biological_entity id="o18163" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o18165" name="bracteole" name_original="bracteoles" src="d0_s5" type="structure" />
      <biological_entity id="o18166" name="stipe" name_original="stipe" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="rhombic" name="shape" src="d0_s5" to="obovate" />
        <character char_type="range_value" from="flat" modifier="almost" name="shape" src="d0_s5" to="convex" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="length" src="d0_s5" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="width" src="d0_s5" to="2.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18167" name="age" name_original="age" src="d0_s5" type="structure">
        <character is_modifier="false" name="width" src="d0_s5" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="somewhat" name="size_or_width" src="d0_s5" value="thickened" value_original="thickened" />
        <character constraint="in basal 1/2" constraintid="o18168" is_modifier="false" name="fusion" src="d0_s5" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o18168" name="1/2" name_original="1/2" src="d0_s5" type="structure" />
      <biological_entity id="o18169" name="margin" name_original="margin" src="d0_s5" type="structure">
        <character constraint="in basal 1/2" constraintid="o18170" is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character constraint="in distal 1/2" constraintid="o18171" is_modifier="false" name="shape" notes="" src="d0_s5" value="2-4-toothed" value_original="2-4-toothed" />
      </biological_entity>
      <biological_entity constraint="basal" id="o18170" name="1/2" name_original="1/2" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o18171" name="1/2" name_original="1/2" src="d0_s5" type="structure" />
      <biological_entity id="o18172" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scurfy" value_original="scurfy" />
      </biological_entity>
      <relation from="o18163" id="r2611" name="fruiting" negation="false" src="d0_s5" to="o18165" />
    </statement>
    <statement id="d0_s6">
      <text>Seeds circular.</text>
    </statement>
    <statement id="d0_s7">
      <text>2n = 18.</text>
      <biological_entity id="o18173" name="seed" name_original="seeds" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s6" value="circular" value_original="circular" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18174" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed places, with other ruderal weeds</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="places" modifier="disturbed" constraint="with other ruderal weeds" />
        <character name="habitat" value="other ruderal weeds" modifier="with" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10-900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Utah; Australia; naturalized South Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="naturalized South Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <other_name type="common_name">Sprawling saltbush</other_name>
  
</bio:treatment>