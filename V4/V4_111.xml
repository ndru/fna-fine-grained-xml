<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Richard W. Spellenberg</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">16</other_info_on_meta>
    <other_info_on_meta type="treatment_page">57</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Choisy in A. P. de Candolle and A. L. P. P. de Candolle" date="1849" rank="genus">NYCTAGINIA</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>13(2): 429. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus NYCTAGINIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek nyct, night, in reference to noctural flowering</other_info_on_name>
    <other_info_on_name type="fna_id">122515</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, viscid-pubescent, from tuberous taproots.</text>
      <biological_entity id="o18800" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="viscid-pubescent" value_original="viscid-pubescent" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o18801" name="taproot" name_original="taproots" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="tuberous" value_original="tuberous" />
      </biological_entity>
      <relation from="o18800" id="r2711" name="from" negation="false" src="d0_s0" to="o18801" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to spreading, unarmed, without glutinous bands on internodes.</text>
      <biological_entity id="o18802" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unarmed" value_original="unarmed" />
      </biological_entity>
      <biological_entity id="o18803" name="band" name_original="bands" src="d0_s1" type="structure">
        <character is_modifier="true" name="coating" src="d0_s1" value="glutinous" value_original="glutinous" />
      </biological_entity>
      <biological_entity id="o18804" name="internode" name_original="internodes" src="d0_s1" type="structure" />
      <relation from="o18802" id="r2712" name="without" negation="false" src="d0_s1" to="o18803" />
      <relation from="o18803" id="r2713" name="on" negation="false" src="d0_s1" to="o18804" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves long petiolate, unequal in each pair, thick and fleshy, base ± asymmetric.</text>
      <biological_entity id="o18805" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s2" value="long" value_original="long" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
        <character constraint="in pair" constraintid="o18806" is_modifier="false" name="size" src="d0_s2" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="width" notes="" src="d0_s2" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o18806" name="pair" name_original="pair" src="d0_s2" type="structure" />
      <biological_entity id="o18807" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s2" value="asymmetric" value_original="asymmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences axillary and terminal, long pedunculate, capitate;</text>
      <biological_entity id="o18808" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="pedunculate" value_original="pedunculate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts 6–20, forming involucre, subtending cluster of more than 3 flowers;</text>
      <biological_entity id="o18809" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s4" to="20" />
        <character is_modifier="false" name="position" src="d0_s4" value="subtending" value_original="subtending" />
        <character constraint="of flowers" constraintid="o18811" is_modifier="false" name="arrangement" src="d0_s4" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o18810" name="involucre" name_original="involucre" src="d0_s4" type="structure" />
      <biological_entity id="o18811" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s4" upper_restricted="false" />
      </biological_entity>
      <relation from="o18809" id="r2714" name="forming" negation="false" src="d0_s4" to="o18810" />
    </statement>
    <statement id="d0_s5">
      <text>outer bracts ovate or lanceolate, apex acuminate or narrowly acute;</text>
      <biological_entity constraint="outer" id="o18812" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o18813" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>inner bracts persistent, not accrescent, distinct, narrower than outer, herbaceous.</text>
      <biological_entity constraint="inner" id="o18814" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="not" name="size" src="d0_s6" value="accrescent" value_original="accrescent" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character constraint="than outer bracts" constraintid="o18815" is_modifier="false" name="width" src="d0_s6" value="narrower" value_original="narrower" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s6" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity constraint="outer" id="o18815" name="bract" name_original="bracts" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers bisexual, chasmogamous and/or cleistogamous;</text>
      <biological_entity id="o18816" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="chasmogamous" value_original="chasmogamous" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="cleistogamous" value_original="cleistogamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>cleistogamous perianth small green dome atop developing turbinate base;</text>
      <biological_entity id="o18817" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="cleistogamous" value_original="cleistogamous" />
      </biological_entity>
      <biological_entity id="o18818" name="dome" name_original="dome" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="small green" value_original="small green" />
      </biological_entity>
      <biological_entity id="o18819" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="atop" name="development" src="d0_s8" value="developing" value_original="developing" />
        <character is_modifier="true" name="shape" src="d0_s8" value="turbinate" value_original="turbinate" />
      </biological_entity>
      <relation from="o18818" id="r2715" name="atop developing" negation="false" src="d0_s8" to="o18819" />
    </statement>
    <statement id="d0_s9">
      <text>chasmogamous perianth radially symmetric, orange-red, or all yellow, funnelform, constricted beyond ovary, abruptly expanded to 5-lobed limb;</text>
      <biological_entity id="o18820" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="chasmogamous" value_original="chasmogamous" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s9" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="orange-red" value_original="orange-red" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character constraint="beyond ovary" constraintid="o18821" is_modifier="false" name="size" src="d0_s9" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o18821" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="abruptly" name="size" notes="" src="d0_s9" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity id="o18822" name="limb" name_original="limb" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="5-lobed" value_original="5-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 5–8, slightly exserted;</text>
      <biological_entity id="o18823" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="8" />
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s10" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles exserted slightly beyond stamens;</text>
      <biological_entity id="o18824" name="style" name_original="styles" src="d0_s11" type="structure">
        <character constraint="beyond stamens" constraintid="o18825" is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o18825" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>stigmas capitate.</text>
      <biological_entity id="o18826" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits radially symmetric, turbinate, constricted beyond base, apex umbonate, stiffly coriaceous;</text>
      <biological_entity id="o18827" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s13" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="shape" src="d0_s13" value="turbinate" value_original="turbinate" />
        <character constraint="beyond base" constraintid="o18828" is_modifier="false" name="size" src="d0_s13" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o18828" name="base" name_original="base" src="d0_s13" type="structure" />
      <biological_entity id="o18829" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="umbonate" value_original="umbonate" />
        <character is_modifier="false" modifier="stiffly" name="texture" src="d0_s13" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ribs 10 low, rounded, each swollen at junction of distal, umbonate portion, glabrous, smooth, without glands.</text>
      <biological_entity id="o18830" name="rib" name_original="ribs" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="10" value_original="10" />
        <character is_modifier="false" name="position" src="d0_s14" value="low" value_original="low" />
        <character is_modifier="false" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
        <character constraint="at junction" constraintid="o18831" is_modifier="false" name="shape" src="d0_s14" value="swollen" value_original="swollen" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o18831" name="junction" name_original="junction" src="d0_s14" type="structure" />
      <biological_entity id="o18832" name="portion" name_original="portion" src="d0_s14" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s14" value="distal" value_original="distal" />
        <character is_modifier="true" name="shape" src="d0_s14" value="umbonate" value_original="umbonate" />
      </biological_entity>
      <biological_entity id="o18833" name="gland" name_original="glands" src="d0_s14" type="structure" />
      <relation from="o18831" id="r2716" name="part_of" negation="false" src="d0_s14" to="o18832" />
      <relation from="o18830" id="r2717" name="without" negation="false" src="d0_s14" to="o18833" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <discussion>Species 1.</discussion>
  
</bio:treatment>