<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">315</other_info_on_meta>
    <other_info_on_meta type="treatment_page">318</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">corispermum</taxon_name>
    <taxon_name authority="Mosyakin" date="1995" rank="species">hookeri</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>5: 349. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus corispermum;species hookeri</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415486</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants branched from base, 10–40 (–60) cm, sparsely covered with dendroid and almost stellate hairs.</text>
      <biological_entity id="o18716" name="whole_organism" name_original="" src="" type="structure">
        <character constraint="from base" constraintid="o18717" is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s0" to="60" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o18717" name="base" name_original="base" src="d0_s0" type="structure" />
      <biological_entity id="o18718" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="dendroid" value_original="dendroid" />
        <character is_modifier="true" modifier="almost" name="arrangement_or_shape" src="d0_s0" value="stellate" value_original="stellate" />
      </biological_entity>
      <relation from="o18716" id="r2699" modifier="sparsely" name="covered with" negation="false" src="d0_s0" to="o18718" />
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades narrowly lanceolate or linear-lanceolate, plane, 2–5 × (0.1–) 0.2–0.5 (–0.6) cm.</text>
      <biological_entity id="o18719" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="plane" value_original="plane" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s1" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="atypical_width" src="d0_s1" to="0.2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s1" to="0.6" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s1" to="0.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences usually dense, ovoid, ovate-clavate, or ovate-cylindric, rarely interrupted near base.</text>
      <biological_entity id="o18720" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="density" src="d0_s2" value="dense" value_original="dense" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-clavate" value_original="ovate-clavate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-cylindric" value_original="ovate-cylindric" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-clavate" value_original="ovate-clavate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-cylindric" value_original="ovate-cylindric" />
        <character constraint="near base" constraintid="o18721" is_modifier="false" modifier="rarely" name="architecture" src="d0_s2" value="interrupted" value_original="interrupted" />
      </biological_entity>
      <biological_entity id="o18721" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Bracts ovate or ovatelanceolate, 0.5–1.5 (–2) × 0.3–1 cm.</text>
      <biological_entity id="o18722" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s3" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s3" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Perianth segment 1.</text>
      <biological_entity constraint="perianth" id="o18723" name="segment" name_original="segment" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Fruits usually deep olive green, brown, or rarely to almost black, usually without spots and/or warts, or occasionally spotted, strongly convex abaxially, prominently concave to almost plane adaxially, usually broadest beyond middle, (3.2–) 3.5–4.5 (–5) × 2.2–3.3 (–3.5) mm;</text>
      <biological_entity id="o18724" name="fruit" name_original="fruits" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="depth" src="d0_s5" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="olive green" value_original="olive green" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="occasionally" name="coloration" notes="" src="d0_s5" value="spotted" value_original="spotted" />
        <character is_modifier="false" modifier="strongly; abaxially" name="shape" src="d0_s5" value="convex" value_original="convex" />
        <character is_modifier="false" modifier="prominently" name="shape" src="d0_s5" value="concave" value_original="concave" />
        <character is_modifier="false" name="shape" src="d0_s5" value="concave to almost" value_original="concave to almost" />
        <character is_modifier="false" modifier="almost; adaxially" name="shape" src="d0_s5" value="plane" value_original="plane" />
        <character constraint="beyond middle fruits" constraintid="o18727" is_modifier="false" modifier="usually" name="width" src="d0_s5" value="broadest" value_original="broadest" />
      </biological_entity>
      <biological_entity id="o18725" name="spot" name_original="spots" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="almost" name="coloration" src="d0_s5" value="black" value_original="black" />
      </biological_entity>
      <biological_entity id="o18726" name="wart" name_original="warts" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="almost" name="coloration" src="d0_s5" value="black" value_original="black" />
      </biological_entity>
      <biological_entity constraint="middle" id="o18727" name="fruit" name_original="fruits" src="d0_s5" type="structure">
        <character char_type="range_value" from="3.2" from_unit="mm" name="atypical_length" notes="alterIDs:o18727" src="d0_s5" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_length" notes="alterIDs:o18727" src="d0_s5" to="5" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" notes="alterIDs:o18727" src="d0_s5" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="3.3" from_inclusive="false" from_unit="mm" name="atypical_width" notes="alterIDs:o18727" src="d0_s5" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="width" notes="alterIDs:o18727" src="d0_s5" to="3.3" to_unit="mm" />
      </biological_entity>
      <relation from="o18724" id="r2700" modifier="rarely" name="to" negation="false" src="d0_s5" to="o18725" />
      <relation from="o18724" id="r2701" modifier="rarely" name="to" negation="false" src="d0_s5" to="o18726" />
    </statement>
    <statement id="d0_s6">
      <text>wing (when present) semitranslucent, to 0.2 mm wide, margins entire, apex rounded or indistinctly triangular.</text>
      <biological_entity id="o18728" name="wing" name_original="wing" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="semitranslucent" value_original="semitranslucent" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s6" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18729" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o18730" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="indistinctly" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Ont., Sask.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>The inflorescence bracts of Corispermum hookeri are conspicuously imbricate (usually strongly overlapping). Corispermum hookeri seems to be closely related to the narrow-winged taxa, C. villosum and C. ochotense, and to C. pallasii sensu stricto. The names C. orientale and C. hyssopifolium have been commonly misapplied to this species.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences dense throughout, rarely interrupted. Fruits oblong-obovate to obovate</description>
      <determination>6a Corispermum hookeri var. hookeri</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences dense only at apex, interrupted. Fruits elongate to oblong-obovate</description>
      <determination>6b Corispermum hookeri var. pseudodeclinatu</determination>
    </key_statement>
  </key>
</bio:treatment>