<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="treatment_page">418</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">amaranthaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">amaranthus</taxon_name>
    <taxon_name authority="(Linnaeus) Aellen ex K. R. Robertson" date="1981" rank="subgenus">acnida</taxon_name>
    <taxon_name authority="Mosyakin &amp; K. R. Robertson" date="1996" rank="section">Saueranthus</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Bot. Fenn.</publication_title>
      <place_in_publication>33: 277. 1996</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amaranthaceae;genus amaranthus;subgenus acnida;section Saueranthus</taxon_hierarchy>
    <other_info_on_name type="fna_id">304253</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, glabrous, glabrescent, or pubescent.</text>
      <biological_entity id="o19925" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bracts not enfolding flower, not leaflike, narrowly lanceolate to subulate-linear, margins entire, plane or indistinctly undulate at margins.</text>
      <biological_entity id="o19926" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" notes="" src="d0_s1" value="leaflike" value_original="leaflike" />
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s1" to="subulate-linear" />
      </biological_entity>
      <biological_entity id="o19927" name="flower" name_original="flower" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="not" name="position" src="d0_s1" value="enfolding" value_original="enfolding" />
      </biological_entity>
      <biological_entity id="o19928" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s1" value="plane" value_original="plane" />
        <character constraint="at margins" constraintid="o19929" is_modifier="false" modifier="indistinctly" name="shape" src="d0_s1" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o19929" name="margin" name_original="margins" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Pistillate flowers: tepals (4–) 5, spatulate or obovate-elliptic, sometimes only outer tepals distinctly spatulate.</text>
      <biological_entity id="o19930" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o19931" name="tepal" name_original="tepals" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s2" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s2" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s2" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="obovate-elliptic" value_original="obovate-elliptic" />
      </biological_entity>
      <biological_entity id="o19932" name="tepal" name_original="tepals" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="sometimes only" name="position" src="d0_s2" value="outer" value_original="outer" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s2" value="spatulate" value_original="spatulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Utricles dehiscence regularly circumscissile (indehiscent only in Amaranthus greggii).</text>
      <biological_entity id="o19933" name="utricle" name_original="utricles" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="regularly" name="dehiscence" src="d0_s3" value="circumscissile" value_original="circumscissile" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3a.2.</number>
  <discussion>Species 4 (4 in the flora).</discussion>
  
</bio:treatment>