<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">434</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">amaranthaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">amaranthus</taxon_name>
    <taxon_name authority="(Kunth) Grenier &amp; Godron" date="1855" rank="subgenus">Albersia</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">graecizans</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 990. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amaranthaceae;genus amaranthus;subgenus albersia;species graecizans;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242415677</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, pubescent in distal parts or becoming glabrescent at maturity.</text>
      <biological_entity id="o20349" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character constraint="in distal parts" constraintid="o20350" is_modifier="false" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character constraint="at maturity" is_modifier="false" modifier="becoming" name="pubescence" src="d0_s0" value="glabrescent" value_original="glabrescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="distal" id="o20350" name="part" name_original="parts" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending or decumbent, branched at or distal to base, 0.1–0.9 m.</text>
      <biological_entity id="o20351" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending or decumbent" />
        <character constraint="at distal" constraintid="o20352" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" notes="" src="d0_s1" to="0.9" to_unit="m" />
      </biological_entity>
      <biological_entity id="o20352" name="distal" name_original="distal" src="d0_s1" type="structure" />
      <biological_entity id="o20353" name="base" name_original="base" src="d0_s1" type="structure" />
      <relation from="o20352" id="r2965" name="to" negation="false" src="d0_s1" to="o20353" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole variable in length;</text>
      <biological_entity id="o20354" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o20355" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="length" src="d0_s2" value="variable" value_original="variable" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade lanceolate to nearly linear or rhombic-ovate to elliptic-ovate, (1.5–) 2–4 (–5) × 1–3 cm, base cuneate to broadly cuneate, margins entire, plane, rarely indistinctly undulate, apex subacute to obtuse or emarginate, mucronulate.</text>
      <biological_entity id="o20356" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o20357" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="nearly linear or rhombic-ovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_length" src="d0_s3" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20358" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s3" to="broadly cuneate" />
      </biological_entity>
      <biological_entity id="o20359" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="rarely indistinctly" name="shape" src="d0_s3" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o20360" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="subacute" name="shape" src="d0_s3" to="obtuse or emarginate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="mucronulate" value_original="mucronulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences axillary glomerules, green.</text>
      <biological_entity id="o20361" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s4" value="green" value_original="green" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o20362" name="glomerule" name_original="glomerules" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Bracts lanceolate, subspinescent, 1.5–2 mm, shorter or slightly longer than tepals.</text>
      <biological_entity id="o20363" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subspinescent" value_original="subspinescent" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
        <character constraint="than tepals" constraintid="o20364" is_modifier="false" name="length_or_size" src="d0_s5" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o20364" name="tepal" name_original="tepals" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Pistillate flowers: tepals 3, erect, elliptic to lanceolate-elliptic, equal or subequal, 1.5–2 mm, apex short-acuminate;</text>
      <biological_entity id="o20365" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o20366" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="3" value_original="3" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character char_type="range_value" from="elliptic" name="arrangement_or_shape" src="d0_s6" to="lanceolate-elliptic" />
        <character is_modifier="false" name="variability" src="d0_s6" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s6" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20367" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="short-acuminate" value_original="short-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style-branches slightly spreading;</text>
      <biological_entity id="o20368" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o20369" name="style-branch" name_original="style-branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stigmas (2–) 3.</text>
      <biological_entity id="o20370" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o20371" name="stigma" name_original="stigmas" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s8" to="3" to_inclusive="false" />
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Staminate flowers intermixed with pistillate;</text>
      <biological_entity id="o20372" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>tepals 3, equal or subequal;</text>
      <biological_entity id="o20373" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
        <character is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s10" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 3.</text>
      <biological_entity id="o20374" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Utricles subglobose to broadly elliptic, 2–2.5 mm, slightly rugose, dehiscence regularly circumscissile, rarely irregularly dehiscent.</text>
      <biological_entity id="o20375" name="utricle" name_original="utricles" src="d0_s12" type="structure">
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s12" to="broadly elliptic" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s12" value="rugose" value_original="rugose" />
        <character is_modifier="false" modifier="regularly" name="dehiscence" src="d0_s12" value="circumscissile" value_original="circumscissile" />
        <character is_modifier="false" modifier="rarely irregularly" name="dehiscence" src="d0_s12" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds black, lenticular, 1–1.3 (–1.6) mm diam., smooth or indistinctly punctate.</text>
      <biological_entity id="o20376" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lenticular" value_original="lenticular" />
        <character char_type="range_value" from="1.3" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s13" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s13" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="indistinctly" name="relief" src="d0_s13" value="punctate" value_original="punctate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>On ballast</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ballast" modifier="on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; N.J.; native to Eurasia (Mediterranean area, s Asia); n Africa; locally introduced in Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" value="native to Eurasia (Mediterranean area)" establishment_means="native" />
        <character name="distribution" value="native to Eurasia (s Asia)" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="locally  in Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>36.</number>
  <other_name type="common_name">Mediterranean amaranth</other_name>
  <discussion>In North America the name Amaranthus graecizans has been constantly misapplied to the common North American taxa A. albus and A. blitoides. Consequently, A. graecizans has been excluded from lists of North American plants. Recently, herbarium specimens (casual aliens collected in 1879 on ballast in Camden, New Jersey) of A. graecizans subsp. sylvestris were discovered (M. Costea et al. 2001b). Probably, the species disappeared in North America long ago, but, considering the long history of misidentification and confusion, there is also some chance that it may occur locally as an introduced species.</discussion>
  <discussion>Three subspecies are usually recognized within Amaranthus graecizans in the Old World: subsp. graecizans, subsp. sylvestris (Villars) Brenan, and subsp. thellungianus (Nevski) Gusev. Only subsp. sylvestris, characterized by rhombic-ovate to elliptic-ovate leaves (as compared to lanceolate to almost linear leaves in subsp. graecizans) and comparatively large seeds has so far been reported from North America.</discussion>
  <discussion>Despite its superficial similarity to Amaranthus albus and A. blitoides, A. graecizans seems to be more closely related to other Old World taxa with trimerous flowers.</discussion>
  
</bio:treatment>