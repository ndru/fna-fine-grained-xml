<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="treatment_page">360</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">atriplex</taxon_name>
    <taxon_name authority="(Gaertner) S. L. Welsh" date="2001" rank="subgenus">Obione</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Obione</taxon_name>
    <taxon_name authority="(Standley) S. L. Welsh" date="2001" rank="subsection">Pusillae</taxon_name>
    <taxon_name authority="S. Watson" date="1874" rank="species">coronata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">coronata</taxon_name>
    <taxon_hierarchy>family chenopodiaceae;genus atriplex;subgenus obione;section obione;subsection pusillae;species coronata;variety coronata;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415551</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atriplex</taxon_name>
    <taxon_name authority="Jepson" date="unknown" rank="species">coronata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">verna</taxon_name>
    <taxon_hierarchy>genus Atriplex;species coronata;variety verna;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atriplex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">verna</taxon_name>
    <taxon_hierarchy>genus Atriplex;species verna;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems decumbent to ascending;</text>
      <biological_entity id="o11673" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s0" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>basal branches and leaves typically alternate.</text>
    </statement>
    <statement id="d0_s2">
      <text>Fruiting bracteoles almost sessile, body subcircular in profile, ± compressed, (2–) 4–5 mm, united to summit, truncate at the irregularly toothed summit, sides smooth or obscurely to definitely tuberculate.</text>
      <biological_entity constraint="basal" id="o11674" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="typically" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o11675" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="typically" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity id="o11676" name="bracteole" name_original="bracteoles" src="d0_s2" type="structure" />
      <biological_entity id="o11677" name="whole-organism" name_original="" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="almost" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o11678" name="body" name_original="body" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="in profile" name="shape" src="d0_s2" value="subcircular" value_original="subcircular" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
        <character constraint="at summit" constraintid="o11680" is_modifier="false" name="architecture_or_shape" src="d0_s2" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o11679" name="summit" name_original="summit" src="d0_s2" type="structure" />
      <biological_entity id="o11680" name="summit" name_original="summit" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="irregularly" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o11681" name="side" name_original="sides" src="d0_s2" type="structure">
        <character char_type="range_value" from="smooth or" name="relief" src="d0_s2" to="obscurely definitely tuberculate" />
      </biological_entity>
      <relation from="o11674" id="r1708" name="fruiting" negation="false" src="d0_s2" to="o11676" />
      <relation from="o11675" id="r1709" name="fruiting" negation="false" src="d0_s2" to="o11676" />
      <relation from="o11678" id="r1710" name="united to" negation="false" src="d0_s2" to="o11679" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alkaline sink scrub, growing with Atriplex polycarpa and other salt desert shrubs and forbs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="scrub" modifier="alkaline sink" />
        <character name="habitat" value="atriplex polycarpa" />
        <character name="habitat" value="other salt desert shrubs" />
        <character name="habitat" value="forbs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>34a.</number>
  
</bio:treatment>