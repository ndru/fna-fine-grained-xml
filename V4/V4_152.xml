<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">72</other_info_on_meta>
    <other_info_on_meta type="treatment_page">73</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">pisonia</taxon_name>
    <taxon_name authority="(S. Watson) Standley" date="1911" rank="species">capitata</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>13: 388. 1911</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus pisonia;species capitata</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415109</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cryptocarpus</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">capitatus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>24: 71. 1889</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cryptocarpus;species capitatus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or small trees, to 6 m;</text>
      <biological_entity id="o4018" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="6" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="true" name="size" src="d0_s0" value="small" value_original="small" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="6" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branches dense, spreading at right angles, usually armed with stout, recurved spines 7–14 mm.</text>
      <biological_entity id="o4020" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="density" src="d0_s1" value="dense" value_original="dense" />
        <character constraint="at angles" constraintid="o4021" is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character constraint="with spines" constraintid="o4022" is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s1" value="armed" value_original="armed" />
      </biological_entity>
      <biological_entity id="o4021" name="angle" name_original="angles" src="d0_s1" type="structure" />
      <biological_entity id="o4022" name="spine" name_original="spines" src="d0_s1" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character is_modifier="true" name="orientation" src="d0_s1" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s1" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems arching, velvety when young, glabrate later.</text>
      <biological_entity id="o4023" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="arching" value_original="arching" />
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s2" value="velvety" value_original="velvety" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="condition" src="d0_s2" value="later" value_original="later" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades broadly obovate or suborbiculate, 2–6 × 2–6 cm, base rounded or broadly cuneate, apex rounded, rarely subacute.</text>
      <biological_entity id="o4024" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="suborbiculate" value_original="suborbiculate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s3" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4025" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o4026" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="subacute" value_original="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: [staminate inflorescences subglobose, dense, 1–2 cm diam.];</text>
      <biological_entity id="o4027" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>pistillate inflorescences densely subglobose at anthesis, more open in age;</text>
      <biological_entity id="o4028" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o4029" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
        <character constraint="at anthesis" is_modifier="false" modifier="densely" name="shape" src="d0_s5" value="subglobose" value_original="subglobose" />
        <character constraint="in age" constraintid="o4030" is_modifier="false" name="architecture" src="d0_s5" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o4030" name="age" name_original="age" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>branches divaricate or ascending;</text>
      <biological_entity id="o4031" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o4032" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>fruiting pedicel 0.5–2 cm.</text>
      <biological_entity id="o4033" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s7" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4034" name="pedicel" name_original="pedicel" src="d0_s7" type="structure" />
      <relation from="o4033" id="r529" name="fruiting" negation="false" src="d0_s7" to="o4034" />
    </statement>
    <statement id="d0_s8">
      <text>Perianths: [perianth of staminate flowers deep red, broadly campanulate, 2–3 mm, densely and shortly viscid-villous];</text>
      <biological_entity id="o4035" name="perianth" name_original="perianths" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>perianth of pistillate flowers greenish, often blushed with red, 2–2.5 mm, densely puberulent.</text>
      <biological_entity id="o4036" name="perianth" name_original="perianths" src="d0_s9" type="structure" />
      <biological_entity id="o4037" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s9" value="blushed with red" value_original="blushed with red" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o4038" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <relation from="o4037" id="r530" name="part_of" negation="false" src="d0_s9" to="o4038" />
    </statement>
    <statement id="d0_s10">
      <text>Fruits clavate, 7–10 × 3–4 mm, densely puberulent between glandular ribs, glands extending entire length of fruits.</text>
      <biological_entity id="o4039" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s10" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="4" to_unit="mm" />
        <character constraint="between glandular ribs" constraintid="o4040" is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="glandular" id="o4040" name="rib" name_original="ribs" src="d0_s10" type="structure" />
      <biological_entity id="o4041" name="gland" name_original="glands" src="d0_s10" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering winter–early spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early spring" from="winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Protected clefts on rhyolitic soils in arid scrub [tropical deciduous forests, thickets, along watercourses]</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="protected clefts" constraint="on rhyolitic soils in arid scrub ( tropical deciduous forests , thickets , along watercourses )" />
        <character name="habitat" value="rhyolitic soils" constraint="in arid scrub" />
        <character name="habitat" value="arid scrub" />
        <character name="habitat" value="tropical deciduous forests" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="watercourses" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Pisonia capitata is represented in the flora by a single, possibly clonal, population of a few pistillate plants in south-central Arizona several hundred kilometers north of known populations in Sonora, perhaps originating from a freak event of bird dispersal. Plants are not known to have set seed.</discussion>
  <discussion>As noted above, pistillate plants of Pisonia capitata and P. aculeata exhibit no apparent differences, nor are there apparent vegetative differences between the species. That plants known in the flora truly represent P. aculeata is an assumption based on geography.</discussion>
  
</bio:treatment>