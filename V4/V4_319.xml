<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">161</other_info_on_meta>
    <other_info_on_meta type="mention_page">166</other_info_on_meta>
    <other_info_on_meta type="treatment_page">167</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Engelmann in F. A.Wislizenus" date="1848" rank="genus">echinocereus</taxon_name>
    <taxon_name authority="(L. D. Benson) B. D. Parfitt" date="1987" rank="species">nicholii</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>63: 157. 1987</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus echinocereus;species nicholii;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415250</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinocereus</taxon_name>
    <taxon_name authority="(Parry ex Engelmann) Lemaire" date="unknown" rank="species">engelmannii</taxon_name>
    <taxon_name authority="L. D. Benson" date="unknown" rank="variety">nicholii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>4, 25: 258, plate 25, fig. B. 1944</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Echinocereus;species engelmannii;variety nicholii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 16–30-branched, forming clumps.</text>
      <biological_entity id="o9397" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="16-30-branched" value_original="16-30-branched" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o9398" name="clump" name_original="clumps" src="d0_s0" type="structure" />
      <relation from="o9397" id="r1342" name="forming" negation="false" src="d0_s0" to="o9398" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, cylindric or somewhat tapering distally, 20–30 (–70) × (4–) 6–9 cm;</text>
      <biological_entity id="o9399" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" name="shape" src="d0_s1" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="somewhat; distally" name="shape" src="d0_s1" value="tapering" value_original="tapering" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="70" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s1" to="30" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_width" src="d0_s1" to="6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="width" src="d0_s1" to="9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>ribs 10–13, crests slightly undulate;</text>
      <biological_entity id="o9400" name="rib" name_original="ribs" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s2" to="13" />
      </biological_entity>
      <biological_entity id="o9401" name="crest" name_original="crests" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s2" value="undulate" value_original="undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>areoles 15–25 mm apart.</text>
      <biological_entity id="o9402" name="areole" name_original="areoles" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s3" to="25" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="apart" value_original="apart" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spines 13–16 per areole, usually straight, divergent-porrect, pale translucent yellow;</text>
      <biological_entity id="o9403" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o9404" from="13" name="quantity" src="d0_s4" to="16" />
        <character is_modifier="false" modifier="usually" name="course" notes="" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="divergent-porrect" value_original="divergent-porrect" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pale translucent" value_original="pale translucent" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o9404" name="areole" name_original="areole" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>radial spines 8–12 per areole, 5–25 mm;</text>
      <biological_entity id="o9405" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="radial" value_original="radial" />
        <character char_type="range_value" constraint="per areole" constraintid="o9406" from="8" name="quantity" src="d0_s5" to="12" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9406" name="areole" name_original="areole" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>central spines 4–6 (–8) per areole, 30–72 mm, abaxial central spine usually descending, paler than other central spines, often fading whitish, somewhat flattened.</text>
      <biological_entity constraint="central" id="o9407" name="spine" name_original="spines" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="8" />
        <character char_type="range_value" constraint="per areole" constraintid="o9408" from="4" name="quantity" src="d0_s6" to="6" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="72" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9408" name="areole" name_original="areole" src="d0_s6" type="structure" />
      <biological_entity constraint="abaxial central" id="o9409" name="spine" name_original="spine" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s6" value="descending" value_original="descending" />
        <character constraint="than other central spines" constraintid="o9410" is_modifier="false" name="coloration" src="d0_s6" value="paler" value_original="paler" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s6" value="fading whitish" value_original="fading whitish" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s6" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity constraint="central" id="o9410" name="spine" name_original="spines" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers 4–6 × 5–7 cm;</text>
      <biological_entity id="o9411" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s7" to="6" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s7" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>flower tube 18–25 × 12–30 mm;</text>
      <biological_entity constraint="flower" id="o9412" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="length" src="d0_s8" to="25" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s8" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>flower tube hairs 1 mm;</text>
      <biological_entity constraint="tube" id="o9413" name="hair" name_original="hairs" src="d0_s9" type="structure" constraint_original="flower tube">
        <character name="some_measurement" src="d0_s9" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>inner tepals usually pale-pink with midstripes slightly darker, proximal 1/3 greenish, 30–40 × 5–10 mm, tips relatively thin and delicate;</text>
      <biological_entity constraint="inner" id="o9414" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character constraint="with midstripes" constraintid="o9415" is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="pale-pink" value_original="pale-pink" />
        <character is_modifier="false" name="position" notes="" src="d0_s10" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s10" value="1/3" value_original="1/3" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s10" to="40" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9415" name="midstripe" name_original="midstripes" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="slightly" name="coloration" src="d0_s10" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity id="o9416" name="tip" name_original="tips" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="relatively" name="width" src="d0_s10" value="thin" value_original="thin" />
        <character is_modifier="false" name="fragility" src="d0_s10" value="delicate" value_original="delicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow;</text>
      <biological_entity id="o9417" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectar chamber to 7 mm.</text>
      <biological_entity constraint="nectar" id="o9418" name="chamber" name_original="chamber" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits green, becoming bronze where exposed to sun, 23–34 mm, pulp white.</text>
      <biological_entity id="o9419" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="green" value_original="green" />
        <character is_modifier="false" modifier="where exposed to sun" name="coloration" src="d0_s13" value="bronze" value_original="bronze" />
        <character char_type="range_value" from="23" from_unit="mm" name="some_measurement" src="d0_s13" to="34" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = 22.</text>
      <biological_entity id="o9420" name="pulp" name_original="pulp" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9421" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr; fruiting Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
        <character name="fruiting time" char_type="range_value" to="Jun" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Arizona Upland Subdivision of Sonoran Desert, exposed slopes, bajadas, hills, mountains, desert scrub, igneous and sedimentary substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="arizona" />
        <character name="habitat" value="slopes" modifier="exposed" />
        <character name="habitat" value="bajadas" />
        <character name="habitat" value="hills" />
        <character name="habitat" value="mountains" />
        <character name="habitat" value="desert scrub" />
        <character name="habitat" value="igneous" />
        <character name="habitat" value="sedimentary substrates" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico (Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="common_name">Nichol’s hedgehog cactus</other_name>
  <other_name type="common_name">golden hedgehog cactus</other_name>
  <discussion>Although it was initially considered a variety of Echinocereus engelmannii, E. nicholii differs in chromosome number and a suite of morphologic characteristics. With its non-erumpent flower buds, diploid chromosome number, relatively small and usually pale flowers, green fruits, and exclusively yellow spines, E. nicholii more closely resembles E. ledingii than E. engelmannii.</discussion>
  
</bio:treatment>