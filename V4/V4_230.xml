<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="treatment_page">119</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Opuntioideae</taxon_name>
    <taxon_name authority="F. Reichenbach ex K. Schumann" date="1896" rank="genus">grusonia</taxon_name>
    <taxon_name authority="(Ralston &amp; Hilsenbeck) E. F. Anderson" date="1999" rank="species">aggeria</taxon_name>
    <place_of_publication>
      <publication_title>Cact. Succ. J. (Los Angeles)</publication_title>
      <place_in_publication>71: 325. 1999</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily opuntioideae;genus grusonia;species aggeria;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415173</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Opuntia</taxon_name>
    <taxon_name authority="Ralston &amp; Hilsenbeck" date="unknown" rank="species">aggeria</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>36: 226, fig. 2. 1989</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Opuntia;species aggeria;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, forming clumps, 3–9 cm.</text>
      <biological_entity id="o14431" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="9" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o14432" name="clump" name_original="clumps" src="d0_s0" type="structure" />
      <relation from="o14431" id="r2069" name="forming" negation="false" src="d0_s0" to="o14432" />
    </statement>
    <statement id="d0_s1">
      <text>Roots tuberlike, 7–8 × 2.5–3 cm.</text>
      <biological_entity id="o14433" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="tuberlike" value_original="tuberlike" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s1" to="8" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem segments short cylindric to clavate, 3.5–9 × 1.5–3 cm;</text>
      <biological_entity constraint="stem" id="o14434" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="short" value_original="short" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s2" to="clavate" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s2" to="9" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>tubercles 8–18 (–22) mm;</text>
      <biological_entity id="o14435" name="tubercle" name_original="tubercles" src="d0_s3" type="structure">
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="22" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s3" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>areoles circular 3–4 mm in diam.;</text>
      <biological_entity id="o14436" name="areole" name_original="areoles" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="circular" value_original="circular" />
        <character char_type="range_value" from="3" from_unit="mm" name="diam" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>wool yellowish white.</text>
      <biological_entity id="o14437" name="wool" name_original="wool" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellowish white" value_original="yellowish white" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spines (1–) 5–15 per areole, mostly in distal areoles, 3–5 cm;</text>
      <biological_entity id="o14439" name="areole" name_original="areole" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o14440" name="areole" name_original="areoles" src="d0_s6" type="structure" />
      <relation from="o14438" id="r2070" modifier="mostly" name="in" negation="false" src="d0_s6" to="o14440" />
    </statement>
    <statement id="d0_s7">
      <text>major 1–3 abaxial spines deflexed, usually chalky white (at least adaxially), flattened to angular-flattened, longest central abaxial spine commonly twisted or curved (at times the only spine in depauperate specimens, those mostly from Big Bend region of Texas);</text>
      <biological_entity id="o14438" name="spine" name_original="spines" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s6" to="5" to_inclusive="false" />
        <character char_type="range_value" constraint="per areole" constraintid="o14439" from="5" name="quantity" src="d0_s6" to="15" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" notes="" src="d0_s6" to="5" to_unit="cm" />
        <character is_modifier="false" name="size" src="d0_s7" value="major" value_original="major" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o14441" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="3" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="deflexed" value_original="deflexed" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s7" value="chalky white" value_original="chalky white" />
        <character char_type="range_value" from="flattened" name="shape" src="d0_s7" to="angular-flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>major 0–5 adaxial spines divergent, ascending, brown to blackish and sometimes chalky, ± terete.</text>
      <biological_entity constraint="longest central abaxial" id="o14442" name="spine" name_original="spine" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="commonly" name="architecture" src="d0_s7" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="course" src="d0_s7" value="curved" value_original="curved" />
        <character is_modifier="false" name="size" src="d0_s8" value="major" value_original="major" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o14443" name="spine" name_original="spines" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s8" to="5" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s8" to="blackish and sometimes chalky" />
        <character is_modifier="false" modifier="sometimes; more or less" name="shape" src="d0_s8" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Glochids adaxial in areole, yellow, to 4 mm.</text>
      <biological_entity id="o14444" name="glochid" name_original="glochids" src="d0_s9" type="structure">
        <character constraint="in areole" constraintid="o14445" is_modifier="false" name="position" src="d0_s9" value="adaxial" value_original="adaxial" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14445" name="areole" name_original="areole" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: inner tepals bright-yellow, 25 mm;</text>
      <biological_entity id="o14446" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="inner" id="o14447" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="bright-yellow" value_original="bright-yellow" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="25" value_original="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments green;</text>
      <biological_entity id="o14448" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o14449" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style cream;</text>
      <biological_entity id="o14450" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o14451" name="style" name_original="style" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="cream" value_original="cream" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigma lobes pale yellow-green.</text>
      <biological_entity id="o14452" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="stigma" id="o14453" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="pale yellow-green" value_original="pale yellow-green" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits yellow, aging gray, cylindric to ellipsoid, 20–25 (–50) × 10–15 mm, becoming dry, spineless, glochidiate;</text>
      <biological_entity id="o14454" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="life_cycle" src="d0_s14" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="gray" value_original="gray" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s14" to="ellipsoid" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s14" to="50" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s14" to="25" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s14" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="becoming" name="condition_or_texture" src="d0_s14" value="dry" value_original="dry" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="spineless" value_original="spineless" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glochidiate" value_original="glochidiate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>areoles 25–35.</text>
      <biological_entity id="o14455" name="areole" name_original="areoles" src="d0_s15" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s15" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds yellowish to brownish, ± circular, to 5 mm in diam., with various numbers and sizes of bumps.</text>
      <biological_entity id="o14457" name="number" name_original="numbers" src="d0_s16" type="structure">
        <character is_modifier="true" name="variability" src="d0_s16" value="various" value_original="various" />
      </biological_entity>
      <biological_entity id="o14458" name="bump" name_original="bumps" src="d0_s16" type="structure" />
      <relation from="o14456" id="r2071" name="with" negation="false" src="d0_s16" to="o14457" />
    </statement>
    <statement id="d0_s17">
      <text>2n = 22.</text>
      <biological_entity id="o14456" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s16" to="brownish" />
        <character is_modifier="false" modifier="more or less" name="arrangement_or_shape" src="d0_s16" value="circular" value_original="circular" />
        <character char_type="range_value" from="0" from_unit="mm" name="diam" src="d0_s16" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14459" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (Mar–May).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="Mar-May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Chihuahuan Desert, sandy or gravelly flats, scrub with creosote bush, lower slopes, limestone or igneous substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly flats" />
        <character name="habitat" value="scrub" constraint="with creosote bush , lower slopes , limestone or igneous substrates" />
        <character name="habitat" value="creosote bush" />
        <character name="habitat" value="lower slopes" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="igneous substrates" />
        <character name="habitat" value="desert" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Grusonia aggeria is based on the “type” of “Opuntia grahamii × O. schottii” described by M. S. Anthony (1956). It is not a hybrid, however, between the two tetraploid putative parental taxa; it is instead a fully fertile, diploid species.</discussion>
  
</bio:treatment>