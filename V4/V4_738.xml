<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">372</other_info_on_meta>
    <other_info_on_meta type="treatment_page">374</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">atriplex</taxon_name>
    <taxon_name authority="(Torrey &amp; Frémont) S. L. Welsh" date="2001" rank="subgenus">Pterochiton</taxon_name>
    <taxon_name authority="(Moquin-Tandon) D. Dietrich" date="1852" rank="species">gardneri</taxon_name>
    <taxon_name authority="(A. Nelson) S. L. Welsh &amp; Crompton" date="1995" rank="variety">aptera</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>55: 326. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus atriplex;subgenus pterochiton;species gardneri;variety aptera;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415589</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atriplex</taxon_name>
    <taxon_name authority="A. Nelson" date="unknown" rank="species">aptera</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>34: 356. 1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Atriplex;species aptera;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atriplex</taxon_name>
    <taxon_name authority="(Pursh) Nuttall" date="unknown" rank="species">canescens</taxon_name>
    <taxon_name authority="(A. Nelson) H. M. Hall &amp; Clements" date="unknown" rank="subspecies">aptera</taxon_name>
    <taxon_hierarchy>genus Atriplex;species canescens;subspecies aptera;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, dioecious or sparingly monoecious, mainly 2–5 dm.</text>
      <biological_entity id="o7611" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" modifier="sparingly" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character char_type="range_value" from="2" from_unit="dm" modifier="mainly" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems mainly from a basal crown.</text>
      <biological_entity id="o7612" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o7613" name="crown" name_original="crown" src="d0_s1" type="structure" />
      <relation from="o7612" id="r1042" name="from" negation="false" src="d0_s1" to="o7613" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous or tardily so, alternate or proximal ones opposite, sessile or subsessile;</text>
      <biological_entity id="o7614" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character name="duration" src="d0_s2" value="tardily" value_original="tardily" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o7615" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade green to yellow-green, oblanceolate to elliptic or spatulate, 15–40 × 6–12 mm, 2–6 times longer than wide.</text>
      <biological_entity id="o7616" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s3" to="yellow-green" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="elliptic or spatulate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s3" to="40" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s3" to="12" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="2-6" value_original="2-6" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Staminate flowers yellow, in glomerules 3 mm thick, in terminal or axillary spikes or spicate panicles to 12 cm.</text>
      <biological_entity id="o7617" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o7618" name="glomerule" name_original="glomerules" src="d0_s4" type="structure">
        <character name="thickness" src="d0_s4" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o7619" name="spike" name_original="spikes" src="d0_s4" type="structure" />
      <biological_entity id="o7620" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="spicate" value_original="spicate" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="12" to_unit="cm" />
      </biological_entity>
      <relation from="o7617" id="r1043" name="in" negation="false" src="d0_s4" to="o7618" />
      <relation from="o7617" id="r1044" name="in" negation="false" src="d0_s4" to="o7619" />
      <relation from="o7617" id="r1045" name="in" negation="false" src="d0_s4" to="o7620" />
    </statement>
    <statement id="d0_s5">
      <text>Pistillate flowers in spicate panicles 8–25 cm.</text>
      <biological_entity id="o7622" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="spicate" value_original="spicate" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s5" to="25" to_unit="cm" />
      </biological_entity>
      <relation from="o7621" id="r1046" name="in" negation="false" src="d0_s5" to="o7622" />
    </statement>
    <statement id="d0_s6">
      <text>Fruiting bracteoles sessile or on stipes to 5 mm, body basically ellipsoid, bearing 4 deeply lobed lateral wings or rows of flattened tubercles to 4 mm high, 4–8 mm and as wide, apex 1.5 mm, with 2–4 lateral smaller teeth, wings, sometimes vestigial, rarely absent.</text>
      <biological_entity id="o7621" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o7623" name="bracteole" name_original="bracteoles" src="d0_s6" type="structure" />
      <biological_entity id="o7624" name="stipe" name_original="stipes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7625" name="body" name_original="body" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="basically" name="shape" src="d0_s6" value="ellipsoid" value_original="ellipsoid" />
        <character name="quantity" src="d0_s6" value="4" value_original="4" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o7626" name="wing" name_original="wings" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="deeply" name="shape" src="d0_s6" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="0" from_unit="mm" name="height" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" constraint="as wide" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o7627" name="row" name_original="rows" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="deeply" name="shape" src="d0_s6" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="0" from_unit="mm" name="height" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" constraint="as wide" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7628" name="tubercle" name_original="tubercles" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity id="o7629" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character name="some_measurement" src="d0_s6" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity constraint="lateral smaller" id="o7630" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="4" />
      </biological_entity>
      <relation from="o7621" id="r1047" name="fruiting" negation="false" src="d0_s6" to="o7623" />
      <relation from="o7626" id="r1048" name="part_of" negation="false" src="d0_s6" to="o7628" />
      <relation from="o7627" id="r1049" name="part_of" negation="false" src="d0_s6" to="o7628" />
      <relation from="o7629" id="r1050" name="with" negation="false" src="d0_s6" to="o7630" />
    </statement>
    <statement id="d0_s7">
      <text>2n = 36.</text>
      <biological_entity id="o7631" name="wing" name_original="wings" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s6" value="vestigial" value_original="vestigial" />
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7632" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Saline, often moist, fine-textured substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="saline" />
        <character name="habitat" value="fine-textured substrates" modifier="often moist" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400-1100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Sask.; Mont., Nebr., N.Dak., S.Dak., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>51g.</number>
  <other_name type="common_name">Nelson’s saltbush</other_name>
  <discussion>Atriplex gardneri var. aptera is a Great Plains-Steppe endemic.</discussion>
  <discussion>The concept of Atriplex aptera is based on recurring and at least partially stabilized introgressant populations derived through hybridization between A. gardneri and A. canescens. Hence, the plants do not form a traditionally conceived taxon in the sense of being monophyletic. They are, however, of frequent occurrence, evidently fertile and long lived, and are taxon-like in distribution. While intermediate in many ways, they are habitually most similar to A. gardneri but bear yellow staminate flowers and the fruiting bracteoles are either winged as in A. canescens or the tubercles are aligned in four rows. They tend to occupy similar saline habitats as those occupied by A. gardneri. Despite the origin of the A. aptera material, and regardless of its local abundance, it does not appear to overwhelm either of the parental species. Staminate specimens of A. aptera are difficult, if not impossible, to distinguish from A. gardneri var. gardneri or A. canescens.</discussion>
  
</bio:treatment>