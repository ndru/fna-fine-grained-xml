<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">41</other_info_on_meta>
    <other_info_on_meta type="mention_page">52</other_info_on_meta>
    <other_info_on_meta type="treatment_page">54</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">mirabilis</taxon_name>
    <taxon_name authority="(L’Heritier ex Willdenow) Heimerl in H. G. A. Engler and K. Prantl" date="unknown" rank="section">Oxybaphus</taxon_name>
    <taxon_name authority="(Greene) Standley" date="1931" rank="species">rotundifolia</taxon_name>
    <place_of_publication>
      <publication_title>Publ. Field Mus. Nat. Hist., Bot. Ser.</publication_title>
      <place_in_publication>8: 305. 1931</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus mirabilis;section oxybaphus;species rotundifolia;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242415070</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Allionia</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">rotundifolia</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Baker.</publication_title>
      <place_in_publication>3: 33. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Allionia;species rotundifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect or ascending, 2–3 dm, leafy mostly in proximal 1/2, openly forked distally, spreading-soft hirsute throughout.</text>
      <biological_entity id="o1797" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character constraint="in proximal 1/2" constraintid="o1798" is_modifier="false" name="architecture" src="d0_s0" value="leafy" value_original="leafy" />
        <character is_modifier="false" modifier="openly; distally" name="shape" notes="" src="d0_s0" value="forked" value_original="forked" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s0" value="spreading-soft" value_original="spreading-soft" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s0" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o1798" name="1/2" name_original="1/2" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves usually widely ascending at 60–80°, abruptly reduced below inflorescence;</text>
      <biological_entity id="o1799" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="60-80°" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character constraint="below inflorescence" constraintid="o1800" is_modifier="false" modifier="abruptly" name="size" src="d0_s1" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o1800" name="inflorescence" name_original="inflorescence" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>petiole 0–0.9 cm;</text>
      <biological_entity id="o1801" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="0.9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade green adaxially, glaucescent abaxially, broadly ovate, ovate-triangular, or round, 4–7 × 3–6 cm, thick, moderately coriaceous, base cordate to round or cuneate, apex obtuse to round, surfaces glabrous or soft hirsute adaxially, soft hirsute abaxially.</text>
      <biological_entity id="o1802" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" modifier="abaxially" name="coating" src="d0_s3" value="glaucescent" value_original="glaucescent" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate-triangular" value_original="ovate-triangular" />
        <character is_modifier="false" name="shape" src="d0_s3" value="round" value_original="round" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate-triangular" value_original="ovate-triangular" />
        <character is_modifier="false" name="shape" src="d0_s3" value="round" value_original="round" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s3" to="7" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s3" to="6" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s3" value="thick" value_original="thick" />
        <character is_modifier="false" modifier="moderately" name="texture" src="d0_s3" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o1803" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s3" to="round or cuneate" />
      </biological_entity>
      <biological_entity id="o1804" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="round" />
      </biological_entity>
      <biological_entity id="o1805" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="soft" value_original="soft" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s3" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s3" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences primarily terminal, few branched, open;</text>
      <biological_entity id="o1806" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="few" value_original="few" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>peduncle 3–6 mm, spreading-pubescent, sometimes somewhat glandular, crosswalls of hairs pale;</text>
      <biological_entity id="o1807" name="peduncle" name_original="peduncle" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="spreading-pubescent" value_original="spreading-pubescent" />
        <character is_modifier="false" modifier="sometimes somewhat" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o1808" name="crosswall" name_original="crosswalls" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale" value_original="pale" />
      </biological_entity>
      <biological_entity id="o1809" name="hair" name_original="hairs" src="d0_s5" type="structure" />
      <relation from="o1808" id="r230" name="part_of" negation="false" src="d0_s5" to="o1809" />
    </statement>
    <statement id="d0_s6">
      <text>involucres grayish green, widely bell-shaped, 4–6 mm in flower, 7–8 mm in fruit, spreading-pubescent, 40–50% connate, lobes ovate.</text>
      <biological_entity id="o1810" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="grayish green" value_original="grayish green" />
        <character is_modifier="false" modifier="widely" name="shape" src="d0_s6" value="bell--shaped" value_original="bell--shaped" />
        <character char_type="range_value" constraint="in flower" constraintid="o1811" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o1812" from="7" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s6" value="spreading-pubescent" value_original="spreading-pubescent" />
        <character is_modifier="false" modifier="40-50%" name="fusion" src="d0_s6" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o1811" name="flower" name_original="flower" src="d0_s6" type="structure" />
      <biological_entity id="o1812" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
      <biological_entity id="o1813" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 3 per involucre;</text>
      <biological_entity id="o1814" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character constraint="per involucre" constraintid="o1815" name="quantity" src="d0_s7" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o1815" name="involucre" name_original="involucre" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>perianth purplish-pink, 0.9–1.1 cm.</text>
      <biological_entity id="o1816" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="purplish-pink" value_original="purplish-pink" />
        <character char_type="range_value" from="0.9" from_unit="cm" name="some_measurement" src="d0_s8" to="1.1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Fruits pale olive brown, narrowly obovoid, 4–5 mm, ± evenly puberulent with hairs 0.1 mm;</text>
      <biological_entity id="o1817" name="fruit" name_original="fruits" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale olive" value_original="pale olive" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
        <character constraint="with hairs" constraintid="o1818" is_modifier="false" modifier="more or less evenly" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o1818" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="0.1" value_original="0.1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ribs round, 0.3–0.5 times width of sulci, 0.5 times as wide as high, slightly rugose (more so on side);</text>
      <biological_entity id="o1819" name="rib" name_original="ribs" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="round" value_original="round" />
        <character constraint="sulcus" constraintid="o1820" is_modifier="false" name="width" src="d0_s10" value="0.3-0.5 times width of sulci" />
        <character is_modifier="false" name="width_or_height" src="d0_s10" value="0.5 times as wide as high slightly rugose" />
        <character is_modifier="false" name="width_or_height" src="d0_s10" value="0.5 times as wide as high slightly rugose" />
      </biological_entity>
      <biological_entity id="o1820" name="sulcus" name_original="sulci" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>sulci slightly darker than ribs, slightly rugose.</text>
      <biological_entity id="o1821" name="sulcus" name_original="sulci" src="d0_s11" type="structure">
        <character constraint="than ribs" constraintid="o1822" is_modifier="false" name="coloration" src="d0_s11" value="slightly darker" value_original="slightly darker" />
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s11" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity id="o1822" name="rib" name_original="ribs" src="d0_s11" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring-mid summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, calcareous, shaley outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous" modifier="open" />
        <character name="habitat" value="shaley outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600-1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Mirabilis rotundifolia is clearly closely related to Mirabilis albida and may be only a variant.</discussion>
  <discussion>Mirabilis rotundifolia is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>