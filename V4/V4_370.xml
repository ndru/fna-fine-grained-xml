<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">195</other_info_on_meta>
    <other_info_on_meta type="treatment_page">196</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1922" rank="genus">echinomastus</taxon_name>
    <taxon_name authority="(Engelmann) Britton &amp; Rose" date="1922" rank="species">intertextus</taxon_name>
    <taxon_name authority="(Engelmann) Backeberg" date="1961" rank="variety">dasyacanthus</taxon_name>
    <place_of_publication>
      <publication_title>Cactaceae</publication_title>
      <place_in_publication>5: 2832. 1961</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus echinomastus;species intertextus;variety dasyacanthus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415282</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinocactus</taxon_name>
    <taxon_name authority="Englemann" date="unknown" rank="species">intertextus</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="variety">dasyacanthus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. A mer. Acad. Arts</publication_title>
      <place_in_publication>3: 277. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Echinocactus;species intertextus;variety dasyacanthus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Neolloydia</taxon_name>
    <taxon_name authority="(Englemann) L. D. Benson" date="unknown" rank="species">intertexta</taxon_name>
    <taxon_name authority="(Engelmann) L. D. Benson" date="unknown" rank="variety">dasyacantha</taxon_name>
    <taxon_hierarchy>genus Neolloydia;species intertexta;variety dasyacantha;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sclerocactus</taxon_name>
    <taxon_name authority="(Engelmann) N. P. Taylor" date="unknown" rank="species">intertextus</taxon_name>
    <taxon_name authority="(Engelmann) N. P. Taylor" date="unknown" rank="variety">dasyacanthus</taxon_name>
    <taxon_hierarchy>genus Sclerocactus;species intertextus;variety dasyacanthus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems spheric, obovoid, or ovoid to cylindric, appearing bristly and dangerous to handle.</text>
      <biological_entity id="o14770" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="spheric" value_original="spheric" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s0" to="cylindric" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s0" to="cylindric" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="bristly" value_original="bristly" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Spines slightly appressed to spreading, except for the porrect adaxial central spine which 4–15 (–20) mm.</text>
      <biological_entity constraint="adaxial central" id="o14772" name="spine" name_original="spine" src="d0_s1" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s1" value="porrect" value_original="porrect" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="20" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s1" to="15" to_unit="mm" />
      </biological_entity>
      <relation from="o14771" id="r2148" name="except for" negation="false" src="d0_s1" to="o14772" />
    </statement>
    <statement id="d0_s2">
      <text>2n = 22.</text>
      <biological_entity id="o14771" name="spine" name_original="spines" src="d0_s1" type="structure">
        <character char_type="range_value" from="slightly appressed" name="orientation" src="d0_s1" to="spreading" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14773" name="chromosome" name_original="" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Apr; fruiting Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Feb" />
        <character name="fruiting time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Desert grasslands and plains grasslands, upper edge of Chihuahuan desert scrub, grassy hills, bajadas, sometimes with oaks and junipers, igneous substrates (rarely limestone)</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="desert grasslands" />
        <character name="habitat" value="plains grasslands" />
        <character name="habitat" value="upper edge" constraint="of chihuahuan desert scrub" />
        <character name="habitat" value="chihuahuan desert scrub" />
        <character name="habitat" value="grassy hills" />
        <character name="habitat" value="bajadas" />
        <character name="habitat" value="oaks" modifier="sometimes with" />
        <character name="habitat" value="junipers" />
        <character name="habitat" value="igneous substrates" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100-2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5b.</number>
  <discussion>In the Chisos Mountains of Texas, one isolated population of Echinomastus intertextus, identified as var. dasyacanthus on the basis of spine length, is sympatric with the vegetatively similar E. warnockii. The figure captioned as “Neolloydia” intertexta var. dasyacantha (L. D. Benson 1982, color fig. 155) is typical E. warnockii.</discussion>
  <discussion>Echinomastus intertextus var. dasyacanthus presumably occurs in extreme northern Chihuahua, but we have not seen any specimens.</discussion>
  
</bio:treatment>