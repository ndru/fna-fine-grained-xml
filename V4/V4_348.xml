<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">181</other_info_on_meta>
    <other_info_on_meta type="mention_page">183</other_info_on_meta>
    <other_info_on_meta type="treatment_page">184</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="(A. Berger) Britton &amp; Rose" date="1909" rank="genus">pachycereus</taxon_name>
    <taxon_name authority="(Engelmann) D. R. Hunt" date="1987" rank="species">schottii</taxon_name>
    <place_of_publication>
      <publication_title>Bradleya</publication_title>
      <place_in_publication>5: 93. 1987</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus pachycereus;species schottii;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415267</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cereus</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="species">schottii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>3: 288. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cereus;species schottii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lophocereus</taxon_name>
    <taxon_name authority="(Engelmann) Britton &amp; Rose" date="unknown" rank="species">schottii</taxon_name>
    <taxon_hierarchy>genus Lophocereus;species schottii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pilocereuss</taxon_name>
    <taxon_name authority="(Engelmann) Lemaire" date="unknown" rank="species">chottii</taxon_name>
    <taxon_hierarchy>genus Pilocereuss;species chottii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs [to small trees], with basal branches sometimes rooting to form thickets.</text>
      <biological_entity id="o1579" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1580" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character constraint="to form, thickets" constraintid="o1581, o1582" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o1581" name="form" name_original="form" src="d0_s0" type="structure" />
      <biological_entity id="o1582" name="thicket" name_original="thickets" src="d0_s0" type="structure" />
      <relation from="o1579" id="r206" name="with" negation="false" src="d0_s0" to="o1580" />
    </statement>
    <statement id="d0_s1">
      <text>Stems green with red around young areoles, aging gray-green with faint waxy chevrons;</text>
      <biological_entity id="o1583" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character constraint="around young areoles" constraintid="o1584" is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="life_cycle" notes="" src="d0_s1" value="aging" value_original="aging" />
        <character constraint="with chevrons" constraintid="o1585" is_modifier="false" name="coloration" src="d0_s1" value="gray-green" value_original="gray-green" />
      </biological_entity>
      <biological_entity id="o1584" name="areole" name_original="areoles" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="around" name="life_cycle" src="d0_s1" value="young" value_original="young" />
      </biological_entity>
      <biological_entity id="o1585" name="chevron" name_original="chevrons" src="d0_s1" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s1" value="faint" value_original="faint" />
        <character is_modifier="true" name="texture" src="d0_s1" value="ceraceous" value_original="waxy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>ribs somewhat rounded, margins relatively flat;</text>
      <biological_entity id="o1586" name="rib" name_original="ribs" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o1587" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="relatively" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>areoles 1–1.5 cm apart on rib crest, shield-shaped, on proximal stems 1 cm wide, those on distal stems closer together, circular;</text>
      <biological_entity id="o1588" name="areole" name_original="areoles" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="1.5" to_unit="cm" />
        <character constraint="on rib crest" constraintid="o1589" is_modifier="false" name="arrangement" src="d0_s3" value="apart" value_original="apart" />
        <character is_modifier="false" name="shape" notes="" src="d0_s3" value="shield--shaped" value_original="shield--shaped" />
        <character is_modifier="false" name="arrangement_or_shape" notes="" src="d0_s3" value="circular" value_original="circular" />
      </biological_entity>
      <biological_entity constraint="rib" id="o1589" name="crest" name_original="crest" src="d0_s3" type="structure" />
      <biological_entity constraint="proximal" id="o1590" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character name="width" src="d0_s3" unit="cm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1591" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="together" name="arrangement" src="d0_s3" value="closer" value_original="closer" />
      </biological_entity>
      <relation from="o1588" id="r207" name="on" negation="false" src="d0_s3" to="o1590" />
      <relation from="o1588" id="r208" name="on" negation="false" src="d0_s3" to="o1591" />
    </statement>
    <statement id="d0_s4">
      <text>young hairs whitish gray, short.</text>
      <biological_entity id="o1592" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="young" value_original="young" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="whitish gray" value_original="whitish gray" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spines strongly dimorphic, those on proximal stems usually 7–10 per areole, rose-red, aging gray, stout, usually 5–7 mm, to less than 30 mm, weakly differentiated as central and radial spines, those on distal stems usually more than 15 per areole, gray to blackish (rarely aging copper-brown), thin, flattened, twisted, to 7.5 cm;</text>
      <biological_entity id="o1593" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="strongly" name="growth_form" src="d0_s5" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s5" value="rose-red" value_original="rose-red" />
        <character is_modifier="false" name="life_cycle" src="d0_s5" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="gray" value_original="gray" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s5" value="stout" value_original="stout" />
        <character char_type="range_value" from="5" from_unit="mm" modifier="usually" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="30" to_unit="mm" />
        <character constraint="as central spines" constraintid="o1596" is_modifier="false" modifier="weakly" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
        <character char_type="range_value" from="gray" name="coloration" notes="" src="d0_s5" to="blackish" />
        <character is_modifier="false" name="width" src="d0_s5" value="thin" value_original="thin" />
        <character is_modifier="false" name="shape" src="d0_s5" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="twisted" value_original="twisted" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="7.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o1594" name="stem" name_original="stems" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o1595" from="7" name="quantity" src="d0_s5" to="10" />
      </biological_entity>
      <biological_entity id="o1595" name="areole" name_original="areole" src="d0_s5" type="structure" />
      <biological_entity constraint="central" id="o1596" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="radial" value_original="radial" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1597" name="stem" name_original="stems" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o1598" from="15" name="quantity" src="d0_s5" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o1598" name="areole" name_original="areole" src="d0_s5" type="structure" />
      <relation from="o1593" id="r209" name="on" negation="false" src="d0_s5" to="o1594" />
      <relation from="o1593" id="r210" name="on" negation="false" src="d0_s5" to="o1597" />
    </statement>
    <statement id="d0_s6">
      <text>bristles 4–10 cm.</text>
      <biological_entity id="o1599" name="bristle" name_original="bristles" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s6" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers lateral on stems, often several per areole, on specialized bristle-forming or transition areoles, short funnelform, 3–4.5 × 2–3 cm;</text>
      <biological_entity id="o1600" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character constraint="on stems" constraintid="o1601" is_modifier="false" name="position" src="d0_s7" value="lateral" value_original="lateral" />
        <character constraint="per areole" constraintid="o1602" is_modifier="false" modifier="often" name="quantity" notes="" src="d0_s7" value="several" value_original="several" />
        <character is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s7" value="short" value_original="short" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s7" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s7" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1601" name="stem" name_original="stems" src="d0_s7" type="structure" />
      <biological_entity id="o1602" name="areole" name_original="areole" src="d0_s7" type="structure" />
      <biological_entity id="o1603" name="areole" name_original="areoles" src="d0_s7" type="structure">
        <character is_modifier="true" name="development" src="d0_s7" value="specialized" value_original="specialized" />
        <character is_modifier="true" name="duration" src="d0_s7" value="transition" value_original="transition" />
      </biological_entity>
      <relation from="o1600" id="r211" name="on" negation="false" src="d0_s7" to="o1603" />
    </statement>
    <statement id="d0_s8">
      <text>flower tube thin walled, very narrow, less than 1.5 cm;</text>
      <biological_entity constraint="flower" id="o1604" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character is_modifier="false" name="width" src="d0_s8" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="walled" value_original="walled" />
        <character is_modifier="false" modifier="very" name="size_or_width" src="d0_s8" value="narrow" value_original="narrow" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s8" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>nectar chamber open;</text>
      <biological_entity constraint="nectar" id="o1605" name="chamber" name_original="chamber" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>scales rose with green margins;</text>
      <biological_entity id="o1606" name="scale" name_original="scales" src="d0_s10" type="structure" />
      <biological_entity id="o1607" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="green" value_original="green" />
      </biological_entity>
      <relation from="o1606" id="r212" name="rose with" negation="false" src="d0_s10" to="o1607" />
    </statement>
    <statement id="d0_s11">
      <text>tepals spreading, reflexed by morning;</text>
      <biological_entity id="o1608" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character constraint="by morning" constraintid="o1609" is_modifier="false" name="orientation" src="d0_s11" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o1609" name="morning" name_original="morning" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>outer tepals somewhat fimbriate, minutely papillate;</text>
      <biological_entity constraint="outer" id="o1610" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s12" value="fimbriate" value_original="fimbriate" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s12" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>inner tepals 25, with pink-orange center, 11–16 mm, apex acute;</text>
      <biological_entity constraint="inner" id="o1611" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="25" value_original="25" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" notes="" src="d0_s13" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1612" name="center" name_original="center" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="pink-orange" value_original="pink-orange" />
      </biological_entity>
      <biological_entity id="o1613" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o1611" id="r213" name="with" negation="false" src="d0_s13" to="o1612" />
    </statement>
    <statement id="d0_s14">
      <text>stamens 100, white, turning pink by next morning, to 2 cm;</text>
      <biological_entity id="o1614" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="100" value_original="100" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character constraint="by next morning" constraintid="o1615" is_modifier="false" name="coloration" src="d0_s14" value="pink" value_original="pink" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s14" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="next" id="o1615" name="morning" name_original="morning" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>anthers cream;</text>
      <biological_entity id="o1616" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="cream" value_original="cream" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary small, tubercles and spines absent;</text>
      <biological_entity id="o1617" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" name="size" src="d0_s16" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o1618" name="tubercle" name_original="tubercles" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o1619" name="spine" name_original="spines" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles white, short, narrow;</text>
      <biological_entity id="o1620" name="style" name_original="styles" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="white" value_original="white" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s17" value="short" value_original="short" />
        <character is_modifier="false" name="size_or_width" src="d0_s17" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigma lobes erect, relatively short.</text>
      <biological_entity constraint="stigma" id="o1621" name="lobe" name_original="lobes" src="d0_s18" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s18" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" src="d0_s18" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Fruits candy-apple red, when immature greenish, ovoid to subspheric, 25–40 mm.</text>
      <biological_entity id="o1622" name="fruit" name_original="fruits" src="d0_s19" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s19" value="candy-apple red" value_original="candy-apple red" />
        <character is_modifier="false" modifier="when immature" name="coloration" src="d0_s19" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s19" to="subspheric" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s19" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds helmet-shaped, often with a prominent raphe.</text>
      <biological_entity id="o1624" name="raphe" name_original="raphe" src="d0_s20" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s20" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o1623" id="r214" modifier="often" name="with" negation="false" src="d0_s20" to="o1624" />
    </statement>
    <statement id="d0_s21">
      <text>2n = 22.</text>
      <biological_entity id="o1623" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="helmet--shaped" value_original="helmet--shaped" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1625" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Mar–Sep[-Dec]; fruiting often 1 month after flowering.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="late Mar" constraint=" [-Dec" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cercidium-Larrea-Olneya-Carnegiea communities, around washes, on sandy [gravelly] soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cercidium-larrea-olneya-carnegiea communities" constraint="around washes" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="sandy ( gravelly ) soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400-500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico (Baja California, Baja California Sur, Sinaloa, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California Sur)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sinaloa)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Senita</other_name>
  <other_name type="common_name">old-man-cactus</other_name>
  <discussion>Pachycereus schottii is primarily a Mexican species occuring in southernmost Arizona only in Organ Pipe Cactus National Monument at nine sites very close to the international boundary, where winter temperatures do not generally reach freezing. The Arizona populations do not appear to be experiencing much recruitment via seeds, but persist by vegetative propagation. In Mexico, plant height, rib number, and rib shape vary considerably with microhabitat. In subtropical thorn forest P. schottii may be a small tree with a very short trunk. Three varieties of P. schottii were formally recognized (G. S. Lindsay 1963) but need to be re-examined using DNA techniques. If varieties or subspecies are accepted, Arizona populations would represent the typical form.</discussion>
  <discussion>In contrast to other species of Pachycereus, P. schottii and its narrowly restricted cousin P. gatesii of Baja California Sur, Mexico have the smallest flowers and seeds and the most fleshy and naked fruits. The flowering portion of each adult stem always has long, grayish bristles, inspiring the anthropomorphic vernacular names referring to old age.</discussion>
  <discussion>The moth Upiga virescens (Pyralidae) is the obligate pollinator of Pachycereus schottii, forming a mutualistic system comparable with the yucca and yucca moth (J. N. Holland and T. H. Fleming 1999, 1999b; R. S. Folger and A. D. Zimmerman 2000). In populations near the international border, flowers of P. schottii open ten minutes after sunset (J. N. Holland, pers. comm.).</discussion>
  
</bio:treatment>