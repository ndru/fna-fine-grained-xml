<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">324</other_info_on_meta>
    <other_info_on_meta type="mention_page">367</other_info_on_meta>
    <other_info_on_meta type="treatment_page">366</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">atriplex</taxon_name>
    <taxon_name authority="(Gaertner) S. L. Welsh" date="2001" rank="subgenus">Obione</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Obione</taxon_name>
    <taxon_name authority="(Standley) S. L. Welsh" date="2001" rank="subsection">Californicae</taxon_name>
    <taxon_name authority="Moquin-Tandon in A. P. de Candolle and A. L. P. P. de Candolle" date="1849" rank="species">californica</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>13(2): 98. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus atriplex;subgenus obione;section obione;subsection californicae;species californica;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415570</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, monoecious or dioecious, prostrate or procumbent-decumbent, from fleshy fusiform or variously shaped taproot.</text>
      <biological_entity id="o2802" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="procumbent-decumbent" value_original="procumbent-decumbent" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o2803" name="taproot" name_original="taproot" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="fleshy" value_original="fleshy" />
        <character is_modifier="true" name="shape" src="d0_s0" value="fusiform" value_original="fusiform" />
        <character is_modifier="true" name="shape" src="d0_s0" value="variously--shaped" value_original="variously--shaped" />
      </biological_entity>
      <relation from="o2802" id="r361" name="from" negation="false" src="d0_s0" to="o2803" />
    </statement>
    <statement id="d0_s1">
      <text>Stems many branched, subterete, 1.5–5 dm, white scurfy when young.</text>
      <biological_entity id="o2804" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="many" value_original="many" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s1" value="subterete" value_original="subterete" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s1" to="5" to_unit="dm" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="white" value_original="white" />
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s1" value="scurfy" value_original="scurfy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves numerous, often crowded, alternate or proximalmost opposite;</text>
      <biological_entity id="o2805" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="numerous" value_original="numerous" />
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity constraint="proximalmost" id="o2806" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade narrowly lanceolate to narrowly oblanceolate or elliptic, 4–20 × 1–5 mm, acute at both ends, gray scurfy.</text>
      <biological_entity id="o2807" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s3" to="narrowly oblanceolate or elliptic" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="5" to_unit="mm" />
        <character constraint="at ends" constraintid="o2808" is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s3" value="gray" value_original="gray" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="scurfy" value_original="scurfy" />
      </biological_entity>
      <biological_entity id="o2808" name="end" name_original="ends" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Staminate flowers in terminal bracteate spikes, or mixed with pistillate in rather dense axillary clusters, 4-merous.</text>
      <biological_entity constraint="terminal" id="o2810" name="spike" name_original="spikes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="bracteate" value_original="bracteate" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o2811" name="cluster" name_original="clusters" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
        <character is_modifier="true" modifier="rather" name="density" src="d0_s4" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o2809" id="r362" name="in" negation="false" src="d0_s4" to="o2810" />
    </statement>
    <statement id="d0_s5">
      <text>Fruiting bracteoles sessile, rhombic-ovate to ovate, scarcely united, 2.5–4 mm, margin entire, acute.</text>
      <biological_entity id="o2809" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
        <character constraint="with axillary clusters" constraintid="o2811" is_modifier="false" name="arrangement" notes="" src="d0_s4" value="mixed" value_original="mixed" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s4" value="4-merous" value_original="4-merous" />
      </biological_entity>
      <biological_entity id="o2812" name="bracteole" name_original="bracteoles" src="d0_s5" type="structure" />
      <biological_entity id="o2813" name="whole-organism" name_original="" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="rhombic-ovate" name="shape" src="d0_s5" to="ovate" />
        <character is_modifier="false" modifier="scarcely" name="fusion" src="d0_s5" value="united" value_original="united" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2814" name="margin" name_original="margin" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o2809" id="r363" name="fruiting" negation="false" src="d0_s5" to="o2812" />
    </statement>
    <statement id="d0_s6">
      <text>Seeds dark (black), 1–2 mm.</text>
      <biological_entity id="o2815" name="seed" name_original="seeds" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="dark" value_original="dark" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sea bluffs, sandy coasts, crevices in sea cliffs, coastal strands, edges of coastal salt marsh, coastal sage scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sea bluffs" />
        <character name="habitat" value="sandy coasts" />
        <character name="habitat" value="crevices" constraint="in sea cliffs , coastal strands , edges of coastal salt marsh , coastal sage scrub" />
        <character name="habitat" value="sea cliffs" />
        <character name="habitat" value="coastal strands" />
        <character name="habitat" value="edges" constraint="of coastal salt marsh , coastal sage scrub" />
        <character name="habitat" value="coastal salt marsh" />
        <character name="habitat" value="coastal sage scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>44.</number>
  <other_name type="common_name">California orach</other_name>
  <discussion>H. M. Hall and F. E. Clements (1923) placed great emphasis on the inferior radicle, dioecious habit, and free bracts in stating that there are no close relatives of Atriplex californica in North America. P. C. Standley (1916) regarded the radicle as lateral or superior, not inferior, and placed the species at the beginning of his treatment of the American species. Plants of A. californica, however, form a mirror-image, matched pair with A. watsonii, with which they share habit, leaf conformation, staminate glomerules arranged, at least partially, in terminal interrupted spikes, and Kranz anatomy, but from which they differ in the radicle position, valves of the fruiting bracteoles free beyond the middle, monoecious habit, and mostly alternate leaves. The interpretation by Hall and Clements of radicle position as fundamental is made problematic by the apparent random occurrence of a great many morphologic features from place to place within the genus and often within the taxon. Each character fails individually as having definitive taxonomic importance, making difficult or impossible an ultimately satisfactory phylogenetic arrangement. It is not, however, the character that makes the species, rather, it is the entire syndrome of features that constitutes the taxon. Most certainly A. californica is more closely allied to American taxa than to those from other regions of the world.</discussion>
  
</bio:treatment>