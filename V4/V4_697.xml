<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">358</other_info_on_meta>
    <other_info_on_meta type="treatment_page">359</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">atriplex</taxon_name>
    <taxon_name authority="(Gaertner) S. L. Welsh" date="2001" rank="subgenus">Obione</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Obione</taxon_name>
    <taxon_name authority="(Standley) S. L. Welsh" date="2001" rank="subsection">Pusillae</taxon_name>
    <taxon_name authority="Jepson" date="1892" rank="species">cordulata</taxon_name>
    <taxon_name authority="(Stutz" date="2001" rank="variety">erecticaulis</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>102: 423. 2001</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus atriplex;subgenus obione;section obione;subsection pusillae;species cordulata;variety erecticaulis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242415548</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atriplex</taxon_name>
    <taxon_name authority="Stutz" date="unknown" rank="species">erecticaulis</taxon_name>
    <place_of_publication>
      <publication_title>G. L. Chu &amp; S. C. Sanderson, Madroño</publication_title>
      <place_in_publication>44: 89, figs. 1, 2(right). 1997</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Atriplex;species erecticaulis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems highly branched from base, 3–5 (–8) dm;</text>
      <biological_entity id="o21819" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character constraint="from base" constraintid="o21820" is_modifier="false" modifier="highly" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s0" to="8" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" notes="" src="d0_s0" to="5" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o21820" name="base" name_original="base" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>branches mainly steeply ascending.</text>
      <biological_entity id="o21821" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mainly steeply" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade deltoid-ovate to ovatelanceolate or subcordate, 5–15 × 5–12 mm, base rounded to cordate-clasping, typically entire, or sparingly toothed below middle.</text>
      <biological_entity id="o21822" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o21823" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="deltoid-ovate" name="shape" src="d0_s2" to="ovatelanceolate or subcordate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s2" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21824" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s2" value="cordate-clasping" value_original="cordate-clasping" />
        <character is_modifier="false" modifier="typically" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sparingly" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
        <character is_modifier="false" modifier="typically" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character constraint="below middle leaves" constraintid="o21825" is_modifier="false" modifier="sparingly" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o21825" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="below" name="position" src="d0_s2" value="middle" value_original="middle" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Anthers yellow.</text>
    </statement>
    <statement id="d0_s4">
      <text>Fruiting bracteoles deltoid-rhombic to flabellate, 3–3.5 × 3–4 mm, central tooth subequal to lateral, scarcely if at all tuberculate.</text>
      <biological_entity id="o21826" name="anther" name_original="anthers" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o21827" name="bracteole" name_original="bracteoles" src="d0_s4" type="structure" />
      <biological_entity id="o21828" name="whole-organism" name_original="" src="d0_s4" type="structure">
        <character char_type="range_value" from="deltoid-rhombic" name="shape" src="d0_s4" to="flabellate" />
      </biological_entity>
      <relation from="o21826" id="r3174" name="fruiting" negation="false" src="d0_s4" to="o21827" />
    </statement>
    <statement id="d0_s5">
      <text>2n = 54.</text>
      <biological_entity constraint="central" id="o21829" name="tooth" name_original="tooth" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="position" src="d0_s4" value="lateral" value_original="lateral" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21830" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="54" value_original="54" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, fields, in alkaline grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="alkaline grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>32b</number>
  <other_name type="common_name">Earlimart orach</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Atriplex cordulata var. erecticaulis occurs with Distichlis spicata, Centromadia pungens, Frankenia salina, Cressa truxillensis, Heliotropium curassavicum, and other annual species. Stutz, Chu, and Sanderson indicated this taxon’s relationship to var. cordulata from which it differs in the more bushy habit resulting from profuse branching, later flowering time (August and September, not June and July), yellow anthers (not purple or red, which is possibly only a function of age), deltoid-rhombic to flabellate fruiting bracteoles (not broadly deltoid-ovate), central tooth and lateral of same size (not with larger central tooth), and hexaploid (not tetraploid) chromosome condition. Specimens known previous to the segregation by Stutz et al. were included within A. cordulata, and despite the apparently different chromosome number and morphologic differences, the taxon is nevertheless closely similar morphologically to the only slightly, if at all, disjunct A. cordulata, hence the treatment at infraspecific level herein.</discussion>
  
</bio:treatment>