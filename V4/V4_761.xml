<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="treatment_page">383</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">salicornia</taxon_name>
    <taxon_name authority="S. L. Wolff &amp; Jefferies" date="1987" rank="species">maritima</taxon_name>
    <place_of_publication>
      <publication_title>Canad. J. Bot.</publication_title>
      <place_in_publication>65: 1424, fig. 1. 1987</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus salicornia;species maritima</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415611</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems procumbent to erect, infrequently prostrate, green, often becoming red or purple, especially at apex of segments, around flowers, and at sepal tips, simple or with primary and secondary branches, rarely with tertiary branches except when damaged, 5–26 cm, ultimate branches usually short;</text>
      <biological_entity id="o8789" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="infrequently" name="growth_form_or_orientation" src="d0_s0" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" modifier="often becoming; becoming" name="coloration" src="d0_s0" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="usually" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
      </biological_entity>
      <biological_entity constraint="and primary secondary" id="o8790" name="branch" name_original="branches" src="d0_s0" type="structure" />
      <biological_entity constraint="tertiary" id="o8791" name="branch" name_original="branches" src="d0_s0" type="structure" />
      <biological_entity constraint="ultimate" id="o8792" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="true" name="condition" src="d0_s0" value="damaged" value_original="damaged" />
        <character char_type="range_value" from="5" from_unit="cm" is_modifier="true" name="some_measurement" src="d0_s0" to="26" to_unit="cm" />
      </biological_entity>
      <relation from="o8789" id="r1241" modifier="especially" name="at apex of segments , around flowers , and at sepal tips , simple or with" negation="false" src="d0_s0" to="o8790" />
      <relation from="o8789" id="r1242" modifier="rarely" name="with" negation="false" src="d0_s0" to="o8791" />
      <relation from="o8791" id="r1243" name="except when" negation="false" src="d0_s0" to="o8792" />
    </statement>
    <statement id="d0_s1">
      <text>leaf and bract apices obtuse to subacute, not mucronate.</text>
      <biological_entity id="o8793" name="leaf" name_original="leaf" src="d0_s1" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s1" to="subacute" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s1" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity constraint="bract" id="o8794" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s1" to="subacute" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s1" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Spikes ± torulose, 0.7–5 cm, with (3–) 5–10 (–14) fertile segments;</text>
      <biological_entity id="o8795" name="spike" name_original="spikes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="torulose" value_original="torulose" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8796" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="atypical_quantity" src="d0_s2" to="5" to_inclusive="false" />
        <character char_type="range_value" from="10" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s2" to="14" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s2" to="10" />
        <character is_modifier="true" name="reproduction" src="d0_s2" value="fertile" value_original="fertile" />
      </biological_entity>
      <relation from="o8795" id="r1244" name="with" negation="false" src="d0_s2" to="o8796" />
    </statement>
    <statement id="d0_s3">
      <text>bracts covering only base of cymes.</text>
      <biological_entity id="o8797" name="bract" name_original="bracts" src="d0_s3" type="structure" constraint="cyme" constraint_original="cyme; cyme" />
      <biological_entity id="o8798" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o8799" name="cyme" name_original="cymes" src="d0_s3" type="structure" />
      <relation from="o8797" id="r1245" name="covering" negation="false" src="d0_s3" to="o8798" />
      <relation from="o8797" id="r1246" name="part_of" negation="false" src="d0_s3" to="o8799" />
    </statement>
    <statement id="d0_s4">
      <text>Fertile segments (2d–4th in main spikes) 2.9–5.2 × 2.4–4 (–4.3) mm, usually slightly longer than wide, widest distally, margins 0.2–0.3 mm wide, scarious.</text>
      <biological_entity id="o8800" name="segment" name_original="segments" src="d0_s4" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s4" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="2.9" from_unit="mm" name="length" src="d0_s4" to="5.2" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="4.3" to_unit="mm" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
        <character is_modifier="false" name="length_or_size" src="d0_s4" value="usually slightly longer than wide" />
        <character is_modifier="false" modifier="distally" name="width" src="d0_s4" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity id="o8801" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s4" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Central flowers semicircular distally, 1.5–2.6 × 1.4–2.4 mm, usually longer than wide, usually larger than lateral flowers and not reaching top of segment;</text>
      <biological_entity constraint="central" id="o8802" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="distally" name="arrangement_or_shape" src="d0_s5" value="semicircular" value_original="semicircular" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s5" to="2.6" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s5" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="length_or_size" src="d0_s5" value="usually longer than wide" value_original="usually longer than wide" />
        <character constraint="than lateral flowers" constraintid="o8803" is_modifier="false" name="size" src="d0_s5" value="usually larger" value_original="usually larger" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o8803" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o8804" name="segment" name_original="segment" src="d0_s5" type="structure" />
      <relation from="o8802" id="r1247" name="reaching top of" negation="true" src="d0_s5" to="o8804" />
    </statement>
    <statement id="d0_s6">
      <text>anthers commonly not exserted, (0.1–) 0.2–0.3 mm, usually dehiscing within flowers.</text>
      <biological_entity id="o8806" name="flower" name_original="flowers" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>2n = 18.</text>
      <biological_entity id="o8805" name="anther" name_original="anthers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="commonly not" name="position" src="d0_s6" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="0.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s6" to="0.3" to_unit="mm" />
        <character constraint="within flowers" constraintid="o8806" is_modifier="false" modifier="usually" name="dehiscence" src="d0_s6" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8807" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Upper levels of salt marshes and sides of channels on coast, very rarely in salt springs inland</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="upper levels" constraint="of salt marshes" />
        <character name="habitat" value="salt marshes" />
        <character name="habitat" value="sides" modifier="and" constraint="of channels on coast" />
        <character name="habitat" value="channels" constraint="on coast" />
        <character name="habitat" value="coast" />
        <character name="habitat" value="salt springs" modifier="very rarely in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0(-150) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="150" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Salicornia maritima was treated by P. C. Standley (1916) as S. prostrata, although his circumscription included only prostrate and procumbent individuals. He appears to have included erect plants in S. europaea. Salicornia prostrata is a Eurasian species which occurs mostly in inland habitats in its native range.</discussion>
  <discussion>The populations identified as Salicornia maritima from James Bay, in Ontario and Quebec, are morphologically similar to those from the Atlantic Coast, but their isozyme profile is identical to that of S. rubra. The report of S. maritima from Maine is based on Standley’s citation of S. prostrata, but it requires confirmation. The species occurs in New Brunswick adjacent to the Maine border.</discussion>
  
</bio:treatment>