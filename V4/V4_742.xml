<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">375</other_info_on_meta>
    <other_info_on_meta type="treatment_page">376</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">atriplex</taxon_name>
    <taxon_name authority="(Torrey &amp; Frémont) S. L. Welsh" date="2001" rank="subgenus">Pterochiton</taxon_name>
    <taxon_name authority="Rydberg" date="1912" rank="species">garrettii</taxon_name>
    <taxon_name authority="(C. A. Hanson) S. L. Welsh &amp; Crompton" date="1995" rank="variety">navajoensis</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>55: 326. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus atriplex;subgenus pterochiton;species garrettii;variety navajoensis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415593</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atriplex</taxon_name>
    <taxon_name authority="C. A. Hanson" date="unknown" rank="species">navajoensis</taxon_name>
    <place_of_publication>
      <publication_title>Stud. Syst. Bot.</publication_title>
      <place_in_publication>1: 3. 1962</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Atriplex;species navajoensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants mostly 5–10 × 5–16 dm.</text>
      <biological_entity id="o1562" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="dm" name="length" src="d0_s0" to="10" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="dm" name="width" src="d0_s0" to="16" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 7 or more on first flowering branch of current growth, subopposite proximally, alternate distally;</text>
      <biological_entity id="o1563" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" unit="or moreon" value="7" value_original="7" />
      </biological_entity>
      <biological_entity id="o1564" name="branch" name_original="branch" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="flowering" value_original="flowering" />
        <character is_modifier="false" modifier="proximally" name="arrangement" src="d0_s1" value="subopposite" value_original="subopposite" />
        <character is_modifier="false" modifier="distally" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity constraint="current" id="o1565" name="growth" name_original="growth" src="d0_s1" type="structure" />
      <relation from="o1564" id="r202" name="part_of" negation="false" src="d0_s1" to="o1565" />
    </statement>
    <statement id="d0_s2">
      <text>petiole 1–5 mm;</text>
      <biological_entity id="o1566" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade orbiculate to elliptic or oblanceolate, 20–35 × (7–) 10–20 mm, margin entire, apex retuse to obtuse.</text>
      <biological_entity id="o1567" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s3" to="elliptic or oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s3" to="35" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_width" src="d0_s3" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1568" name="margin" name_original="margin" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o1569" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="retuse" name="shape" src="d0_s3" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Staminate flowers yellow, in panicles mainly 5–15 cm.</text>
      <biological_entity id="o1570" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o1571" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s4" to="15" to_unit="cm" />
      </biological_entity>
      <relation from="o1570" id="r203" name="in" negation="false" src="d0_s4" to="o1571" />
    </statement>
    <statement id="d0_s5">
      <text>Pistillate flowers in panicles 15–30 cm.</text>
      <biological_entity id="o1573" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s5" to="30" to_unit="cm" />
      </biological_entity>
      <relation from="o1572" id="r204" name="in" negation="false" src="d0_s5" to="o1573" />
    </statement>
    <statement id="d0_s6">
      <text>Fruiting bracteoles sessile or on stipes to 3 mm, body 4-winged, 6–10 mm and wide, wings lateral, 2–4 mm wide, dentate, apical tooth 0.8–1.5 mm.</text>
      <biological_entity id="o1572" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o1574" name="bracteole" name_original="bracteoles" src="d0_s6" type="structure" />
      <biological_entity id="o1575" name="stipe" name_original="stipes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1576" name="body" name_original="body" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="4-winged" value_original="4-winged" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s6" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity id="o1577" name="wing" name_original="wings" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity constraint="apical" id="o1578" name="tooth" name_original="tooth" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
      </biological_entity>
      <relation from="o1572" id="r205" name="fruiting" negation="false" src="d0_s6" to="o1574" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandstone or limestone rimrock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandstone" />
        <character name="habitat" value="limestone rimrock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900-1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>53b.</number>
  <other_name type="common_name">Navajo Bridge saltbush</other_name>
  <discussion>Atriplex gardneri var. navahoensis occurs in communities with ephedra, four-wing saltbush, shadscale, Indian rice-grass, and galleta.</discussion>
  
</bio:treatment>