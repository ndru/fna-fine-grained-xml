<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John M. Miller</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">457</other_info_on_meta>
    <other_info_on_meta type="mention_page">458</other_info_on_meta>
    <other_info_on_meta type="treatment_page">465</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Adanson" date="unknown" rank="family">portulacaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CLAYTONIA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 204. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 96. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family portulacaceae;genus CLAYTONIA</taxon_hierarchy>
    <other_info_on_name type="etymology">for John Clayton, 1686–1773, physician and plant collector in Virginia</other_info_on_name>
    <other_info_on_name type="fna_id">107275</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Steller ex J. G. Gmelin" date="unknown" rank="genus">Belia</taxon_name>
    <taxon_hierarchy>genus Belia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Haworth" date="unknown" rank="genus">Limnia</taxon_name>
    <taxon_hierarchy>genus Limnia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, usually annual or perennial, occasionally biennial in Claytonia rubra.</text>
      <biological_entity id="o9698" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character constraint="in claytonia" constraintid="o9699" is_modifier="false" modifier="occasionally" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o9699" name="claytonia" name_original="claytonia" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Roots branched, capillary or fibrous.</text>
      <biological_entity id="o9700" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s1" value="capillary" value_original="capillary" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems: subterranean stems tubers, rhizomes, or woody caudices, sometimes with multiple forms on single individuals (e.g., C. umbellata and C. tuberosa);</text>
      <biological_entity id="o9701" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity constraint="stems" id="o9702" name="tuber" name_original="tubers" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="subterranean" value_original="subterranean" />
      </biological_entity>
      <biological_entity id="o9703" name="rhizome" name_original="rhizomes" src="d0_s2" type="structure" />
      <biological_entity id="o9704" name="caudex" name_original="caudices" src="d0_s2" type="structure">
        <character is_modifier="true" name="texture" src="d0_s2" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o9705" name="form" name_original="forms" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="multiple" value_original="multiple" />
      </biological_entity>
      <biological_entity id="o9706" name="individual" name_original="individuals" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="single" value_original="single" />
      </biological_entity>
      <relation from="o9703" id="r1394" name="with" negation="false" src="d0_s2" to="o9705" />
      <relation from="o9704" id="r1395" name="with" negation="false" src="d0_s2" to="o9705" />
      <relation from="o9705" id="r1396" name="on" negation="false" src="d0_s2" to="o9706" />
    </statement>
    <statement id="d0_s3">
      <text>aerial stems erect or decumbent;</text>
      <biological_entity id="o9707" name="stem" name_original="stems" src="d0_s3" type="structure" />
      <biological_entity id="o9708" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>nodes glabrous.</text>
      <biological_entity id="o9709" name="stem" name_original="stems" src="d0_s4" type="structure" />
      <biological_entity id="o9710" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves basal and cauline, not articulate at base, somewhat to markedly clasping, attachment points linear;</text>
      <biological_entity id="o9711" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s5" value="cauline" value_original="cauline" />
        <character constraint="at base" constraintid="o9712" is_modifier="false" modifier="not" name="architecture" src="d0_s5" value="articulate" value_original="articulate" />
        <character is_modifier="false" modifier="somewhat to markedly" name="architecture_or_fixation" notes="" src="d0_s5" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o9712" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity constraint="attachment" id="o9713" name="point" name_original="points" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal leaves few to several in rosettes, blade linear, lanceolate, oblanceolate, spatulate, trullate, rhomboid, ovate, or deltate, apex obtuse to apiculate;</text>
      <biological_entity constraint="basal" id="o9714" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in rosettes" constraintid="o9715" from="few" name="quantity" src="d0_s6" to="several" />
      </biological_entity>
      <biological_entity id="o9715" name="rosette" name_original="rosettes" src="d0_s6" type="structure" />
      <biological_entity id="o9716" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="trullate" value_original="trullate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rhomboid" value_original="rhomboid" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="deltate" value_original="deltate" />
      </biological_entity>
      <biological_entity id="o9717" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s6" to="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cauline leaves 2 and opposite, rarely 3 and whorled, distinct or partially or completely connate, or perfoliate, blade linear to ovate.</text>
      <biological_entity constraint="cauline" id="o9718" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="opposite" value_original="opposite" />
        <character modifier="rarely" name="quantity" src="d0_s7" value="3" value_original="3" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="whorled" value_original="whorled" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="partially" name="fusion" src="d0_s7" value="or" value_original="or" />
        <character is_modifier="false" modifier="completely" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="perfoliate" value_original="perfoliate" />
      </biological_entity>
      <biological_entity id="o9719" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences terminal, racemose or umbellate, secund, bracteate;</text>
      <biological_entity id="o9720" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="umbellate" value_original="umbellate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="secund" value_original="secund" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="bracteate" value_original="bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts leaflike or membranous and scalelike.</text>
      <biological_entity id="o9721" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="texture" src="d0_s9" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="shape" src="d0_s9" value="scale-like" value_original="scalelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers showy;</text>
      <biological_entity id="o9722" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s10" value="showy" value_original="showy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals persistent, leaflike, unequal;</text>
      <biological_entity id="o9723" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="size" src="d0_s11" value="unequal" value_original="unequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 5;</text>
      <biological_entity id="o9724" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 5, adnate to petal bases;</text>
      <biological_entity id="o9725" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character constraint="to petal bases" constraintid="o9726" is_modifier="false" name="fusion" src="d0_s13" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity constraint="petal" id="o9726" name="base" name_original="bases" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>ovary globose, ovules 3 or 6;</text>
      <biological_entity id="o9727" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="globose" value_original="globose" />
      </biological_entity>
      <biological_entity id="o9728" name="ovule" name_original="ovules" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" unit="or" value="3" value_original="3" />
        <character name="quantity" src="d0_s14" unit="or" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style 1;</text>
      <biological_entity id="o9729" name="style" name_original="style" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigmas 3.</text>
      <biological_entity id="o9730" name="stigma" name_original="stigmas" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Capsules 3-valved, longitudinally dehiscent from apex, valves not deciduous, margins hygroscopic, involute.</text>
      <biological_entity id="o9731" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="3-valved" value_original="3-valved" />
        <character constraint="from apex" constraintid="o9732" is_modifier="false" modifier="longitudinally" name="dehiscence" src="d0_s17" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o9732" name="apex" name_original="apex" src="d0_s17" type="structure" />
      <biological_entity id="o9733" name="valve" name_original="valves" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="not" name="duration" src="d0_s17" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity id="o9734" name="margin" name_original="margins" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="hygroscopic" value_original="hygroscopic" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s17" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds (1–) 3–6, black, rounded, shiny and smooth to tuberculate, with white elaiosome;</text>
      <biological_entity id="o9735" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s18" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s18" to="6" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s18" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="reflectance" src="d0_s18" value="shiny" value_original="shiny" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s18" to="tuberculate" />
      </biological_entity>
      <biological_entity id="o9736" name="elaiosome" name_original="elaiosome" src="d0_s18" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s18" value="white" value_original="white" />
      </biological_entity>
      <relation from="o9735" id="r1397" name="with" negation="false" src="d0_s18" to="o9736" />
    </statement>
    <statement id="d0_s19">
      <text>seeds dispersed ballistically and by ants.</text>
    </statement>
    <statement id="d0_s20">
      <text>x = 5, 6, 7, 8.</text>
      <biological_entity id="o9737" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character constraint="by ants" is_modifier="false" modifier="ballistically" name="density" src="d0_s19" value="dispersed" value_original="dispersed" />
      </biological_entity>
      <biological_entity constraint="x" id="o9738" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="5" value_original="5" />
        <character name="quantity" src="d0_s20" value="6" value_original="6" />
        <character name="quantity" src="d0_s20" value="7" value_original="7" />
        <character name="quantity" src="d0_s20" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, (including Mexico), Central America (Guatemala), Asia; adventive in Europe and New Zealand. A related species, Claytonia joanneana Roemer &amp; Schultes, occurs in the Altai Mountains of Siberia and Mongolia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="(including Mexico)" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="adventive in Europe and New Zealand. A related species" establishment_means="native" />
        <character name="distribution" value="Claytonia joanneana Roemer &amp; Schultes" establishment_means="native" />
        <character name="distribution" value="occurs in the Altai Mountains of Siberia and Mongolia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Spring beauty</other_name>
  <discussion>Species 26 (25 in the flora).</discussion>
  <references>
    <reference>Chambers, K. L. 1993. Claytonia. In: J. C. Hickman, ed. 1993. The Jepson Manual. Higher Plants of California. Berkeley, Los Angeles, and London. Pp. 898–900.</reference>
    <reference>Davis, R. G. 1966. The North American perennial species of Claytonia. Brittonia 18: 285–303.</reference>
    <reference>Doyle, J. J. 1984. Karyotypic variation of eastern North American Claytonia chemical races. Amer. J. Bot. 71: 970–978.</reference>
    <reference>Halleck, D. K. and D. Wiens. 1966. Taxonomic status of Claytonia rosea and C. lanceolata (Portulacaceae). Ann. Missouri Bot. Gard. 53: 205–212.</reference>
    <reference>Lewis, W. H. 1967. Cytocatalytic evolution in plants. Bot. Rev. (Lancaster) 33: 105–115.</reference>
    <reference>Lewis, W. H., R. L. Oliver, and Y. Suda. 1967. Cytogeography of Claytonia virginica and its allies. Ann. Missouri Bot. Gard. 54: 153–171.</reference>
    <reference>Lewis, W. H. and Y. Suda. 1968. Karyotypes in relation to classification and phylogeny in Claytonia. Ann. Missouri Bot. Gard. 55: 64–67.</reference>
    <reference>McNeill, J. 1972. New taxa of Claytonia section Claytonia (Portulacaceae). Canad. J. Bot. 50: 1895–1898.</reference>
    <reference>Miller, J. M. and K. L. Chambers. 1993. Nomenclatural changes and new taxa in Claytonia (Portulacaceae) in western North America. Novon 3: 268–273.</reference>
    <reference>Packer, J. G., ed. 1995+. Flora of the Russian Arctic.... 3+ vols. Edmonton. Vol. 3, pp. 195–204.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants annual, sometimes biennial, with minute, tuberous bodies</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants perennial, with stolons, rhizomes, tubers, or woody caudices</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences with 2-several bracts (rarely only 2); proximalmost bract leaflike, distal bracts leaflike or reduced to membranous scales</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescence ebracteate or 1-bracteate; bract leaflike</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Flowers 4-8 mm diam.; petals not candy-striped; basal leaf blades rhombic or ovate</description>
      <determination>25 Claytonia washingtoniana</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Flowers 8-20 mm diam.; petals white, candy-striped, or pink; basal leaf blades linear to deltate</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Cauline leaf blades linear; distal bracts reduced to membranous scales; seeds (1-)6</description>
      <determination>3 Claytonia arenicola</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Cauline leaf blades broad; distal bracts leaflike; seeds (1-)3</description>
      <determination>21 Claytonia sibirica</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Flowers 10-15 mm diam.; inflorescences ebracteate</description>
      <determination>19 Claytonia saxosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Flowers 2-12 mm diam.; inflorescences 1-bracteate</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Basal leaf blades usually trullate or rhombic to deltate, sometimes spatulate, less than 3 times longer than wide</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Basal leaf blades usually linear, sometimes spatulate, blade much longer than wide</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Basal rosettes flattened to suberect; leaf blades trullate, spatulate, or narrowly rhombic to ovate, apex obtuse, red pigmentation often strong even in juvenile plants</description>
      <determination>17 Claytonia rubra</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Basal rosettes suberect to erect, seldom flattened; leaf blades broadly rhombic to deltate or reniform, apex obtuse to apiculate, red pigmentation often weak</description>
      <determination>15 Claytonia perfoliata</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Seeds often smooth or with low tubercles, not pebbly; leaf blades not glau- cous, green or pink</description>
      <determination>14 Claytonia parviflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Seeds often tuberculate and pebbly; leaf blades glaucous, gray, beige, or pink</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Flowers 3-5 mm diam</description>
      <determination>6 Claytonia exigua</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Flowers 6-12 mm diam</description>
      <determination>7 Claytonia gypsophiloides</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Plants with tubers sometimes connected by rhizomes</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Plants with rhizomes or woody caudices</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Bracts 2 or more, distal bracts leaflike or reduced to membranous scales; cauline leaves often strongly tapered in basal 1/2</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Bracts 1 or absent, (rarely 2 in C. caroliniana); cauline leaves seldom tapered in no more than basal 1/4</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Bracts all leaflike; tubers napiform</description>
      <determination>12 Claytonia ogilviensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Proximalmost bract leaflike, distal bracts reduced to membranous scales; tubers globose</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Petals white with yellow blotch at base</description>
      <determination>22 Claytonia tuberosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Petals white, pink, rose, magenta, cream, yellow, or yellow-orange, lacking yellow blotch at base</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Petals pink, rose, or magenta</description>
      <determination>16 Claytonia rosea</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Petals white, cream, yellow, or yellow-orange</description>
      <determination>10 Claytonia multiscapa</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Bracts absent; petals pink to magenta</description>
      <determination>23 Claytonia umbellata</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Bracts present; petals white, white with yellow blotch at base, yellow, orange, or candy-striped (sometimes pink, rose, or magenta in C. caroliniana, C. lanceolata, and C. virginica)</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Cauline leaf blades linear, 0.2-1.2 cm wide, tapered to slender base</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Cauline leaf blades lanceolate or spatulate to ovate, 0.4-2.5 cm wide, abruptly petiolate or sessile</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Petals white, pinkish, or rose, candy-striped, rarely yellow or orange, or white with pink-lavender candy-stripes; e North America to Texas</description>
      <determination>24 Claytonia virginica</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Petals white with yellow blotch at base; Alaska, Yukon</description>
      <determination>22 Claytonia tuberosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Basal leaves 6-21; cauline leaves petiolate; e North America</description>
      <determination>4 Claytonia caroliniana</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Basal leaves 1-6 or absent; cauline leaves sessile; w North American cordillera</description>
      <determination>8 Claytonia lanceolata</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Plants with woody caudices</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Plants rhizomatous</description>
      <next_statement_id>22</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Basal leaves and bracts with apex acute; petals white or pink to bright rose with yellow blotches at base, or white</description>
      <determination>1 Claytonia acutifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Basal leaves and bracts (where present) with apex obtuse; petals usually pink to magenta, sometimes yellow or white</description>
      <next_statement_id>21</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Stems 1-10 cm; bracts absent; Alaska</description>
      <determination>2 Claytonia arctica</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Stems 10-50 cm; bracts present; not in Alaska</description>
      <determination>9 Claytonia megarhiza</determination>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Bracts present; seeds (1-)3</description>
      <next_statement_id>23</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Bracts absent; seeds (1-)6</description>
      <next_statement_id>24</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Cauline leaves equal, sessile, blade lanceolate to ovate; basal leaf blades 1-5 cm wide</description>
      <determination>21 Claytonia sibirica</determination>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Cauline leaves unequal, petiolate, blade linear to spatulate; basal leaf blades 0.1-1 cm wide</description>
      <determination>13 Claytonia palustris</determination>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Rhizomes 0.5-3 mm diam.; Alaska, Yukon, n British Columbia</description>
      <next_statement_id>25</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Rhizomes 4-8 mm diam.; w United States</description>
      <next_statement_id>26</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Basal leaf baldes linear to narrowly spatualte, 1-10 × 0.1-1 cm</description>
      <determination>20 Claytonia scammaniana</determination>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Basal leaf blades elliptic to spatulate, 1-8 × 1-2 cm</description>
      <determination>18 Claytonia sarmentosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Basal leaf blades deltate to ovate or cordate; cauline leaf blades 1-5 cm; petals white</description>
      <determination>5 Claytonia cordifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Basal leaf blades spatulate to rhombic or ovate; cauline leaf blades 0.5-2 cm; petals pink to magenta</description>
      <determination>11 Claytonia nevadensis</determination>
    </key_statement>
  </key>
</bio:treatment>