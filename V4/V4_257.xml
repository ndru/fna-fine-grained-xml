<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">135</other_info_on_meta>
    <other_info_on_meta type="treatment_page">136</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Opuntioideae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">opuntia</taxon_name>
    <taxon_name authority="Salm-Dyck ex Engelmann" date="unknown" rank="species">engelmannii</taxon_name>
    <taxon_name authority="(Griffiths) B. D. Parfitt &amp; Pinkava" date="1988" rank="variety">flexispina</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>35: 348. 1988</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily opuntioideae;genus opuntia;species engelmannii;variety flexispina;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415199</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Opuntia</taxon_name>
    <taxon_name authority="Griffiths" date="unknown" rank="species">flexispina</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>43: 87. 1916</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Opuntia;species flexispina;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Opuntia</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="species">strigil</taxon_name>
    <taxon_name authority="(Griffiths) L. D. Benson" date="unknown" rank="variety">flexispina</taxon_name>
    <taxon_hierarchy>genus Opuntia;species strigil;variety flexispina;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stem segments ovate to subcircular, 20–35 × 15–30 cm, to 1.5 times longer than wide.</text>
      <biological_entity constraint="stem" id="o24181" name="segment" name_original="segments" src="d0_s0" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s0" to="subcircular" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s0" to="35" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="width" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s0" value="0-1.5" value_original="0-1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Spines (0–) 1–5 per areole, evenly distributed on stem segments, deflexed sharply (not arching) or recurving laterally, yellow, sometimes with brown basal portions, flexible, the longest 50–80 mm.</text>
      <biological_entity id="o24182" name="spine" name_original="spines" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s1" to="1" to_inclusive="false" />
        <character char_type="range_value" constraint="per areole" constraintid="o24183" from="1" name="quantity" src="d0_s1" to="5" />
        <character constraint="on stem segments" constraintid="o24184" is_modifier="false" modifier="evenly" name="arrangement" notes="" src="d0_s1" value="distributed" value_original="distributed" />
        <character is_modifier="false" modifier="sharply" name="orientation" notes="" src="d0_s1" value="deflexed" value_original="deflexed" />
        <character is_modifier="false" modifier="laterally" name="orientation" src="d0_s1" value="recurving" value_original="recurving" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="fragility" notes="" src="d0_s1" value="pliable" value_original="flexible" />
        <character is_modifier="false" name="length" src="d0_s1" value="longest" value_original="longest" />
        <character char_type="range_value" from="50" from_unit="mm" name="some_measurement" src="d0_s1" to="80" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24183" name="areole" name_original="areole" src="d0_s1" type="structure" />
      <biological_entity constraint="stem" id="o24184" name="segment" name_original="segments" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o24185" name="portion" name_original="portions" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="brown" value_original="brown" />
      </biological_entity>
      <relation from="o24182" id="r3513" modifier="sometimes" name="with" negation="false" src="d0_s1" to="o24185" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rangelands, on hills</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rangelands" constraint="on hills" />
        <character name="habitat" value="hills" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12e.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Opuntia engelmannii var. flexispina is recognized here reluctantly because it is so poorly known; more study is required to assess its status. Its distribution is limited to Webb and Zapata counties, Texas, according to D. Weniger (1970).</discussion>
  
</bio:treatment>