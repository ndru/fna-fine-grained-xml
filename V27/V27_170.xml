<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">134</other_info_on_meta>
    <other_info_on_meta type="mention_page">140</other_info_on_meta>
    <other_info_on_meta type="treatment_page">138</other_info_on_meta>
    <other_info_on_meta type="illustration_page">139</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwägrichen" date="unknown" rank="family">polytrichaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">polytrichum</taxon_name>
    <taxon_name authority="R. Brown" date="1823" rank="species">hyperboreum</taxon_name>
    <place_of_publication>
      <publication_title>Chlor. Melvill.,</publication_title>
      <place_in_publication>36. 1823,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polytrichaceae;genus polytrichum;species hyperboreum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250065138</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polytrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">piliferum</taxon_name>
    <taxon_name authority="(R. Brown) Müller Hal." date="unknown" rank="variety">hyperboreum</taxon_name>
    <taxon_hierarchy>genus Polytrichum;species piliferum;variety hyperboreum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to fairly robust, green to attractive chestnut-brown with age.</text>
      <biological_entity id="o6463" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" modifier="fairly" name="fragility" src="d0_s0" value="robust" value_original="robust" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character constraint="with age" constraintid="o6464" is_modifier="false" name="coloration" src="d0_s0" value="chestnut-brown" value_original="chestnut-brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6464" name="age" name_original="age" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 2–6 (–10) cm, simple or more commonly fasciculately branched, whitish tomentose only at base.</text>
      <biological_entity id="o6465" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="commonly fasciculately" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="whitish" value_original="whitish" />
        <character constraint="at base" constraintid="o6466" is_modifier="false" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o6466" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves 3–6 mm, crowded in distal half stem, erect and closely appressed when dry, erect-spreading when moist;</text>
      <biological_entity id="o6467" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="6" to_unit="mm" />
        <character constraint="in distal leaves, stem" constraintid="o6468, o6469" is_modifier="false" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when dry" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s2" value="erect-spreading" value_original="erect-spreading" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6468" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o6469" name="stem" name_original="stem" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>sheath rectangular, abruptly contracted to the blade;</text>
      <biological_entity id="o6470" name="sheath" name_original="sheath" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rectangular" value_original="rectangular" />
        <character constraint="to blade" constraintid="o6471" is_modifier="false" modifier="abruptly" name="condition_or_size" src="d0_s3" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o6471" name="blade" name_original="blade" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade linear-lanceolate, rather flat, with sharply infolded margins, channeled at the apex and tapering to the bicolored awn;</text>
      <biological_entity id="o6472" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="rather" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character constraint="at apex" constraintid="o6474" is_modifier="false" name="shape" notes="" src="d0_s4" value="channeled" value_original="channeled" />
        <character constraint="to awn" constraintid="o6475" is_modifier="false" name="shape" notes="" src="d0_s4" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o6473" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="sharply" name="shape" src="d0_s4" value="infolded" value_original="infolded" />
      </biological_entity>
      <biological_entity id="o6474" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <biological_entity id="o6475" name="awn" name_original="awn" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="bicolored" value_original="bicolored" />
      </biological_entity>
      <relation from="o6472" id="r1793" name="with" negation="false" src="d0_s4" to="o6473" />
    </statement>
    <statement id="d0_s5">
      <text>marginal lamina 7–9 cells wide, 1-stratose, entire, membranous, infolded and overlapping, completely enclosing the lamellae;</text>
      <biological_entity constraint="marginal" id="o6476" name="lamina" name_original="lamina" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s5" to="9" />
      </biological_entity>
      <biological_entity id="o6477" name="bud" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="width" src="d0_s5" value="wide" value_original="wide" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="shape" src="d0_s5" value="infolded" value_original="infolded" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o6478" name="lamella" name_original="lamellae" src="d0_s5" type="structure" />
      <relation from="o6477" id="r1794" modifier="completely" name="enclosing the" negation="false" src="d0_s5" to="o6478" />
    </statement>
    <statement id="d0_s6">
      <text>costa excurrent with low blunt teeth abaxially near apex, the awn coarsely spinulose at the base, brown in the basal 1/3 to 1/2, hyaline distally;</text>
      <biological_entity id="o6479" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character constraint="with teeth" constraintid="o6480" is_modifier="false" name="architecture" src="d0_s6" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o6480" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character is_modifier="true" name="position" src="d0_s6" value="low" value_original="low" />
        <character is_modifier="true" name="shape" src="d0_s6" value="blunt" value_original="blunt" />
      </biological_entity>
      <biological_entity id="o6481" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <biological_entity id="o6482" name="awn" name_original="awn" src="d0_s6" type="structure">
        <character constraint="at base" constraintid="o6483" is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s6" value="spinulose" value_original="spinulose" />
        <character constraint="in basal 1/3-1/2" constraintid="o6484" is_modifier="false" name="coloration" notes="" src="d0_s6" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="distally" name="coloration" notes="" src="d0_s6" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o6483" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity constraint="basal" id="o6484" name="1/3-1/2" name_original="1/3-1/2" src="d0_s6" type="structure" />
      <relation from="o6480" id="r1795" name="near" negation="false" src="d0_s6" to="o6481" />
    </statement>
    <statement id="d0_s7">
      <text>lamellae 5–9 cells high, the marginal cells in section ovoid, somewhat larger than cells below, thin-walled or very slightly thickened at apex;</text>
      <biological_entity id="o6485" name="lamella" name_original="lamellae" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="9" />
      </biological_entity>
      <biological_entity id="o6486" name="bud" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="height" src="d0_s7" value="high" value_original="high" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o6487" name="bud" name_original="cells" src="d0_s7" type="structure">
        <character constraint="than cells" constraintid="o6489" is_modifier="false" name="size" notes="" src="d0_s7" value="somewhat larger" value_original="somewhat larger" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="thin-walled" value_original="thin-walled" />
        <character constraint="at apex" constraintid="o6490" is_modifier="false" modifier="very slightly" name="size_or_width" src="d0_s7" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o6488" name="section" name_original="section" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovoid" value_original="ovoid" />
      </biological_entity>
      <biological_entity id="o6489" name="bud" name_original="cells" src="d0_s7" type="structure" />
      <biological_entity id="o6490" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <relation from="o6487" id="r1796" name="in" negation="false" src="d0_s7" to="o6488" />
    </statement>
    <statement id="d0_s8">
      <text>median sheath cells 45–90 × 12–15 µm, narrowly rectangular, thin-walled;</text>
      <biological_entity constraint="sheath" id="o6491" name="bud" name_original="cells" src="d0_s8" type="structure" constraint_original="median sheath">
        <character char_type="range_value" from="45" from_unit="um" name="length" src="d0_s8" to="90" to_unit="um" />
        <character char_type="range_value" from="12" from_unit="um" name="width" src="d0_s8" to="15" to_unit="um" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>cells of marginal lamina 10–15 × 24–45 µm, thick-walled, obliquely oriented and shorter toward the margin and thin-walled.</text>
      <biological_entity constraint="marginal" id="o6493" name="lamina" name_original="lamina" src="d0_s9" type="structure" />
      <biological_entity id="o6494" name="margin" name_original="margin" src="d0_s9" type="structure" />
      <relation from="o6492" id="r1797" name="part_of" negation="false" src="d0_s9" to="o6493" />
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition dioicous;</text>
      <biological_entity id="o6492" name="bud" name_original="cells" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="um" name="length" src="d0_s9" to="15" to_unit="um" />
        <character char_type="range_value" from="24" from_unit="um" name="width" src="d0_s9" to="45" to_unit="um" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="thick-walled" value_original="thick-walled" />
        <character is_modifier="false" modifier="obliquely" name="orientation" src="d0_s9" value="oriented" value_original="oriented" />
        <character constraint="toward margin" constraintid="o6494" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s9" value="thin-walled" value_original="thin-walled" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>perichaetial leaves elongate, with hyaline lamina and long, tapering awn.</text>
      <biological_entity constraint="perichaetial" id="o6495" name="leaf" name_original="leaves" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o6496" name="lamina" name_original="lamina" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o6497" name="awn" name_original="awn" src="d0_s11" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s11" value="long" value_original="long" />
        <character is_modifier="true" name="shape" src="d0_s11" value="tapering" value_original="tapering" />
      </biological_entity>
      <relation from="o6495" id="r1798" name="with" negation="false" src="d0_s11" to="o6496" />
      <relation from="o6495" id="r1799" name="with" negation="false" src="d0_s11" to="o6497" />
    </statement>
    <statement id="d0_s12">
      <text>Seta rather short for plant size, 1.2–3 cm, reddish.</text>
      <biological_entity id="o6498" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character constraint="for plant" constraintid="o6499" is_modifier="false" modifier="rather" name="height_or_length_or_size" src="d0_s12" value="short" value_original="short" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="size" notes="" src="d0_s12" to="3" to_unit="cm" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity id="o6499" name="plant" name_original="plant" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Capsule 2.3–3 mm, ovoid-rectangular (1.2–2:1), somewhat broader at base, sharply 4 (–5) -angled, erect, becoming inclined to horizontal when mature;</text>
      <biological_entity id="o6500" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid-rectangular" value_original="ovoid-rectangular" />
        <character char_type="range_value" from="1.2" name="quantity" src="d0_s13" to="2" />
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
        <character constraint="at base" constraintid="o6501" is_modifier="false" modifier="somewhat" name="width" src="d0_s13" value="broader" value_original="broader" />
        <character is_modifier="false" modifier="sharply" name="shape" notes="" src="d0_s13" value="4(-5)-angled" value_original="4(-5)-angled" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character char_type="range_value" from="inclined" modifier="when mature" name="orientation" src="d0_s13" to="horizontal" />
      </biological_entity>
      <biological_entity id="o6501" name="base" name_original="base" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>peristome 150–230 µm, divided to 0.6, the teeth pale brownish, acute.</text>
      <biological_entity id="o6502" name="capsule" name_original="capsule" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid-rectangular" value_original="ovoid-rectangular" />
        <character char_type="range_value" from="1.2" name="quantity" src="d0_s14" to="2" />
      </biological_entity>
      <biological_entity id="o6503" name="peristome" name_original="peristome" src="d0_s14" type="structure">
        <character char_type="range_value" from="150" from_unit="um" name="some_measurement" src="d0_s14" to="230" to_unit="um" />
        <character is_modifier="false" name="shape" src="d0_s14" value="divided" value_original="divided" />
        <character name="quantity" src="d0_s14" value="0.6" value_original="0.6" />
      </biological_entity>
      <biological_entity id="o6504" name="tooth" name_original="teeth" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="pale brownish" value_original="pale brownish" />
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Spores 15–17 µm.</text>
      <biological_entity id="o6505" name="spore" name_original="spores" src="d0_s15" type="structure">
        <character char_type="range_value" from="15" from_unit="um" name="some_measurement" src="d0_s15" to="17" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sandy or stony ground, ridges, moraines, and open tundra, and in deep masses on stream banks and margins of lakes and ravines</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" modifier="open" />
        <character name="habitat" value="stony ground" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="moraines" />
        <character name="habitat" value="open tundra" />
        <character name="habitat" value="deep masses" modifier="and in" constraint="on stream banks and margins of lakes and ravines" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="margins" constraint="of lakes and ravines" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="ravines" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-1500 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Nfld. and Labr. (Labr.), N.W.T., Nunavut, Que.,Yukon; Alaska; n Europe (Scandinavia); n Asia (Russia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="n Europe (Scandinavia)" establishment_means="native" />
        <character name="distribution" value="n Asia (Russia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>Polytrichum hyperboreum is a handsome species, widespread in arctic America. Sporophytes are commonly produced. In Nunavut, it is known from Baffin, Ellesmere, and Melville islands. The most distinctive characters are the branching habit, channeled leaf apex, and the distinctive bicolored awn, which is coarsely and thickly spinulose at the base, brownish below and hyaline in the distal 1/2. In P. juniperinum, P. strictum, and P. piliferum the awns are ± evenly roughened throughout to almost smooth. It differs from these species also in the rather thin-walled, ovoid marginal cells of the lamellae, and the broad marginal laminae overlapping and completely enclosing the lamellae.</discussion>
  
</bio:treatment>