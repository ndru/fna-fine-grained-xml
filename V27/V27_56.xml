<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">63</other_info_on_meta>
    <other_info_on_meta type="mention_page">66</other_info_on_meta>
    <other_info_on_meta type="mention_page">72</other_info_on_meta>
    <other_info_on_meta type="mention_page">75</other_info_on_meta>
    <other_info_on_meta type="mention_page">84</other_info_on_meta>
    <other_info_on_meta type="mention_page">89</other_info_on_meta>
    <other_info_on_meta type="mention_page">93</other_info_on_meta>
    <other_info_on_meta type="treatment_page">74</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">sphagnaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sphagnum</taxon_name>
    <taxon_name authority="Lindberg" date="1862" rank="section">cuspidata</taxon_name>
    <taxon_name authority="(Lindberg) Warnstorf" date="1900" rank="species">pulchrum</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Centralbl.</publication_title>
      <place_in_publication>82: 42. 1900,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sphagnaceae;genus sphagnum;section cuspidata;species pulchrum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443231</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sphagnum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">intermedium</taxon_name>
    <taxon_name authority="Lindberg" date="unknown" rank="variety">pulchrum</taxon_name>
    <place_of_publication>
      <publication_title>in R. Braithwaite, Sphagnac. Europe,</publication_title>
      <place_in_publication>81, fig. 25g. 1880</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sphagnum;species intermedium;variety pulchrum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants moderate-sized to robust, often quite dense and compact;</text>
    </statement>
    <statement id="d0_s1">
      <text>green, brownish green, golden brown to dark-brown;</text>
      <biological_entity id="o7846" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized" value_original="moderate-sized" />
        <character is_modifier="false" name="fragility" src="d0_s0" value="robust" value_original="robust" />
        <character is_modifier="false" modifier="often quite" name="density" src="d0_s0" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="brownish green" value_original="brownish green" />
        <character char_type="range_value" from="golden brown" name="coloration" src="d0_s1" to="dark-brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>capitulum flattopped and not especially 5-radiate.</text>
      <biological_entity id="o7847" name="capitulum" name_original="capitulum" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not especially" name="architecture_or_arrangement" src="d0_s2" value="5-radiate" value_original="5-radiate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems green to dark-brown;</text>
      <biological_entity id="o7848" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s3" to="dark-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>superficial cortex of 2 layers of enlarged, moderately differentiated cells.</text>
      <biological_entity constraint="superficial" id="o7849" name="cortex" name_original="cortex" src="d0_s4" type="structure" />
      <biological_entity id="o7850" name="layer" name_original="layers" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o7851" name="bud" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="enlarged" value_original="enlarged" />
        <character is_modifier="true" modifier="moderately" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <relation from="o7849" id="r2190" name="consist_of" negation="false" src="d0_s4" to="o7850" />
      <relation from="o7850" id="r2191" name="part_of" negation="false" src="d0_s4" to="o7851" />
    </statement>
    <statement id="d0_s5">
      <text>Stem-leaves triangular to triangular-lingulate, 0.9–1.1 mm;</text>
    </statement>
    <statement id="d0_s6">
      <text>appressed to spreading;</text>
      <biological_entity id="o7852" name="leaf-stem" name_original="stem-leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s5" to="triangular-lingulate" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="distance" src="d0_s5" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s6" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>apex apiculate, acute or narrowly obtuse, appressed to spreading;</text>
      <biological_entity id="o7853" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s7" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hyaline cells nonseptate and efibrillose.</text>
      <biological_entity id="o7854" name="bud" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="efibrillose" value_original="efibrillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Branches straight to more typically curved, typically stout and blunt ended;</text>
    </statement>
    <statement id="d0_s10">
      <text>strongly 5-ranked, leaves not much elongate at distal end.</text>
      <biological_entity id="o7855" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="typically" name="course" src="d0_s9" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="typically" name="fragility_or_size" src="d0_s9" value="stout" value_original="stout" />
        <character is_modifier="false" name="shape" src="d0_s9" value="blunt" value_original="blunt" />
        <character is_modifier="false" modifier="strongly" name="arrangement" src="d0_s10" value="5-ranked" value_original="5-ranked" />
      </biological_entity>
      <biological_entity id="o7856" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character constraint="at distal end" constraintid="o7857" is_modifier="false" modifier="not much" name="shape" src="d0_s10" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o7857" name="end" name_original="end" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Branch fascicles with 2 spreading and 2 pendent branches.</text>
      <biological_entity id="o7858" name="branch" name_original="branch" src="d0_s11" type="structure">
        <character constraint="with branches" constraintid="o7859" is_modifier="false" name="arrangement" src="d0_s11" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o7859" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="true" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="true" name="orientation" src="d0_s11" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Branch stems green but often reddish at proximal end, with cortex enlarged with conspicuous retort cells.</text>
      <biological_entity constraint="branch" id="o7860" name="stem" name_original="stems" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="green" value_original="green" />
        <character constraint="at proximal end" constraintid="o7861" is_modifier="false" modifier="often" name="coloration" src="d0_s12" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o7861" name="end" name_original="end" src="d0_s12" type="structure" />
      <biological_entity id="o7862" name="cortex" name_original="cortex" src="d0_s12" type="structure">
        <character constraint="with retort cells" constraintid="o7863" is_modifier="false" name="size" src="d0_s12" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity constraint="retort" id="o7863" name="bud" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s12" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <relation from="o7860" id="r2192" name="with" negation="false" src="d0_s12" to="o7862" />
    </statement>
    <statement id="d0_s13">
      <text>Branch leaves ovate to ovatelanceolate, 1.4–1.8 mm;</text>
    </statement>
    <statement id="d0_s14">
      <text>straight to often subsecund;</text>
    </statement>
    <statement id="d0_s15">
      <text>weakly undulate and slightly recurved;</text>
      <biological_entity constraint="branch" id="o7864" name="leaf" name_original="leaves" src="d0_s13" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s13" to="ovatelanceolate" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s13" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s14" value="subsecund" value_original="subsecund" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s15" value="undulate" value_original="undulate" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s15" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>hyaline cells on convex surface with 1 pore per cell at apical end of cell, on concave surface with round wall-thinnings in the cells ends and angles;</text>
      <biological_entity id="o7865" name="bud" name_original="cells" src="d0_s16" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s16" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o7866" name="surface" name_original="surface" src="d0_s16" type="structure">
        <character is_modifier="true" name="shape" src="d0_s16" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o7867" name="pore" name_original="pore" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o7868" name="cell" name_original="cell" src="d0_s16" type="structure" />
      <biological_entity constraint="apical" id="o7869" name="end" name_original="end" src="d0_s16" type="structure" />
      <biological_entity id="o7870" name="cell" name_original="cell" src="d0_s16" type="structure" />
      <biological_entity id="o7871" name="surface" name_original="surface" src="d0_s16" type="structure">
        <character is_modifier="true" name="shape" src="d0_s16" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity id="o7872" name="wall-thinning" name_original="wall-thinnings" src="d0_s16" type="structure">
        <character is_modifier="true" name="shape" src="d0_s16" value="round" value_original="round" />
      </biological_entity>
      <biological_entity constraint="cells" id="o7873" name="end" name_original="ends" src="d0_s16" type="structure" />
      <biological_entity id="o7874" name="angle" name_original="angles" src="d0_s16" type="structure" />
      <relation from="o7865" id="r2193" name="on" negation="false" src="d0_s16" to="o7866" />
      <relation from="o7866" id="r2194" name="with" negation="false" src="d0_s16" to="o7867" />
      <relation from="o7867" id="r2195" name="per" negation="false" src="d0_s16" to="o7868" />
      <relation from="o7868" id="r2196" name="at" negation="false" src="d0_s16" to="o7869" />
      <relation from="o7869" id="r2197" name="part_of" negation="false" src="d0_s16" to="o7870" />
      <relation from="o7865" id="r2198" name="on" negation="false" src="d0_s16" to="o7871" />
      <relation from="o7871" id="r2199" name="with" negation="false" src="d0_s16" to="o7872" />
      <relation from="o7872" id="r2200" name="in" negation="false" src="d0_s16" to="o7873" />
      <relation from="o7872" id="r2201" name="in" negation="false" src="d0_s16" to="o7874" />
    </statement>
    <statement id="d0_s17">
      <text>chlorophyllous cells triangular to triangular-ovate in transverse-section, very well-enclosed within concave surface.</text>
      <biological_entity id="o7876" name="transverse-section" name_original="transverse-section" src="d0_s17" type="structure" />
      <biological_entity id="o7877" name="surface" name_original="surface" src="d0_s17" type="structure">
        <character is_modifier="true" name="shape" src="d0_s17" value="concave" value_original="concave" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Sexual condition dioicous.</text>
      <biological_entity id="o7875" name="bud" name_original="cells" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="chlorophyllous" value_original="chlorophyllous" />
        <character char_type="range_value" constraint="in transverse-section" constraintid="o7876" from="triangular" name="shape" src="d0_s17" to="triangular-ovate" />
        <character constraint="within surface" constraintid="o7877" is_modifier="false" modifier="very" name="position" notes="" src="d0_s17" value="well-enclosed" value_original="well-enclosed" />
        <character is_modifier="false" name="reproduction" src="d0_s18" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s18" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Spores 25–28 µm; roughly papillous on both surfaces;</text>
      <biological_entity id="o7878" name="spore" name_original="spores" src="d0_s19" type="structure">
        <character char_type="range_value" constraint="roughly on surfaces" constraintid="o7879" from="25" from_unit="um" name="some_measurement" src="d0_s19" to="28" to_unit="um" />
      </biological_entity>
      <biological_entity id="o7879" name="surface" name_original="surfaces" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>proximal laesura more than 0.5 the length of the spore.</text>
      <biological_entity constraint="proximal" id="o7880" name="laesurum" name_original="laesura" src="d0_s20" type="structure">
        <character char_type="range_value" from="0.5" name="quantity" src="d0_s20" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o7881" name="spore" name_original="spore" src="d0_s20" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Abundant in poor fens and raised bogs, forming dense carpets at water level, especially on floating mats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="poor fens" modifier="abundant in" />
        <character name="habitat" value="bogs" modifier="and raised" />
        <character name="habitat" value="dense carpets" constraint="at water level" />
        <character name="habitat" value="water level" />
        <character name="habitat" value="floating mats" modifier="especially on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., Que.; Alaska, Conn., Ind., Maine, Mass., Mich., Minn., N.H., N.J., N.Y., Wis., W.Va.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>42.</number>
  <discussion>Sporophytes are uncommon in Sphagnum pulchrum. With its distinctive broad and strongly 5-ranked branch leaves, It is one of our most easily recognized species.</discussion>
  
</bio:treatment>