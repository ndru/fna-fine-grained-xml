<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="treatment_page">163</other_info_on_meta>
    <other_info_on_meta type="illustration_page">164</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="M. Fleischer" date="unknown" rank="family">diphysciaceae</taxon_name>
    <taxon_name authority="D. Mohr" date="1803" rank="genus">diphyscium</taxon_name>
    <taxon_name authority="Mitten in F. Dozy and J. H. Molkenboer" date="1855" rank="species">mucronifolium</taxon_name>
    <place_of_publication>
      <publication_title>in F. Dozy and J. H. Molkenboer, Bryol. Jav.</publication_title>
      <place_in_publication>1: 35, plate 26. 1855,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family diphysciaceae;genus diphyscium;species mucronifolium</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443355</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Diphyscium</taxon_name>
    <taxon_name authority="Harvill" date="unknown" rank="species">cumberlandianum</taxon_name>
    <taxon_hierarchy>genus Diphyscium;species cumberlandianum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Diphyscium</taxon_name>
    <taxon_name authority="Mitten" date="unknown" rank="species">involutum</taxon_name>
    <taxon_hierarchy>genus Diphyscium;species involutum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants dark green to brownish, somewhat glossy, tightly affixed to substratum.</text>
      <biological_entity id="o4287" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="dark green" name="coloration" src="d0_s0" to="brownish" />
        <character is_modifier="false" modifier="somewhat" name="reflectance" src="d0_s0" value="glossy" value_original="glossy" />
        <character is_modifier="false" modifier="tightly" name="fixation" src="d0_s0" value="affixed" value_original="affixed" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.5–1 mm, erect, strongly radiculose.</text>
      <biological_entity id="o4288" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s1" to="1" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 0.5–5 mm, and somewhat crisped when dry, apex acute, the most proximal leaves reduced, laminal cells smooth, margins entire.</text>
      <biological_entity id="o4289" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s2" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o4290" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o4291" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="laminal" id="o4292" name="bud" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o4293" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Perichaetial leaves with smooth awn, 9–12 mm, lamina at awn base lacerate but not membranaceous.</text>
      <biological_entity constraint="perichaetial" id="o4294" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" notes="" src="d0_s3" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4295" name="awn" name_original="awn" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o4296" name="lamina" name_original="lamina" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="membranaceous" value_original="membranaceous" />
      </biological_entity>
      <biological_entity constraint="awn" id="o4297" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lacerate" value_original="lacerate" />
      </biological_entity>
      <relation from="o4294" id="r1173" name="with" negation="false" src="d0_s3" to="o4295" />
      <relation from="o4296" id="r1174" name="at" negation="false" src="d0_s3" to="o4297" />
    </statement>
    <statement id="d0_s4">
      <text>Capsule narrowly ovoid, 2–3 mm, stomata absent, mature capsules sheathed in penicellate perichaetium, awns extending more than twice the length of the immersed capsule.</text>
      <biological_entity id="o4298" name="capsule" name_original="capsule" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4299" name="stoma" name_original="stomata" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o4300" name="capsule" name_original="capsules" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="mature" value_original="mature" />
        <character constraint="in perichaetium" constraintid="o4301" is_modifier="false" name="architecture" src="d0_s4" value="sheathed" value_original="sheathed" />
      </biological_entity>
      <biological_entity id="o4301" name="perichaetium" name_original="perichaetium" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="penicellate" value_original="penicellate" />
      </biological_entity>
      <biological_entity id="o4302" name="awn" name_original="awns" src="d0_s4" type="structure" />
      <biological_entity id="o4303" name="capsule" name_original="capsule" src="d0_s4" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s4" value="immersed" value_original="immersed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spores 9–12 µm.</text>
      <biological_entity id="o4304" name="spore" name_original="spores" src="d0_s5" type="structure">
        <character char_type="range_value" from="9" from_unit="um" name="some_measurement" src="d0_s5" to="12" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporophytes infrequent, capsules mature summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Always on somewhat shaded humid rock surfaces, especially sandstones, conglomerate, and schist</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shaded humid rock surfaces" modifier="always on somewhat" />
        <character name="habitat" value="sandstones" />
        <character name="habitat" value="conglomerate" />
        <character name="habitat" value="schist" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate elevations (900-1000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="moderate" />
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="900" from_unit="m" constraint="moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga., Ky., N.C., Tenn., Va.; Asia (China, India, Japan, Philippines, Sri Lanka).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="Asia (India)" establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Asia (Philippines)" establishment_means="native" />
        <character name="distribution" value="Asia (Sri Lanka)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Diphyscium mucronifolium is an example of an east Asian disjunctive species, and is found only as local, small populations.</discussion>
  <references>
    <reference>Harvill, A. M. 1950. Diphyscium cumberlandianum, a pre-Pliocene relic with palaeotropical affinities. Bryologist 53: 277–282.</reference>
  </references>
  
</bio:treatment>