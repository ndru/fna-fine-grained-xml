<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">448</other_info_on_meta>
    <other_info_on_meta type="treatment_page">449</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Limpricht" date="unknown" rank="family">ditrichaceae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="1846" rank="genus">distichium</taxon_name>
    <taxon_name authority="(Hedwig) Bruch &amp; Schimper" date="1846" rank="species">capillaceum</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Europ.</publication_title>
      <place_in_publication>2: 156. 1846,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ditrichaceae;genus distichium;species capillaceum</taxon_hierarchy>
    <other_info_on_name type="fna_id">200000857</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cynontodium</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="species">capillaceum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>57. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cynontodium;species capillaceum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems to ca. 6 cm, occasionally longer.</text>
    </statement>
    <statement id="d0_s1">
      <text>Sexual condition paroicous.</text>
      <biological_entity id="o7685" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="6" to_unit="cm" />
        <character is_modifier="false" modifier="occasionally" name="length_or_size" src="d0_s0" value="longer" value_original="longer" />
        <character is_modifier="false" name="reproduction" src="d0_s1" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s1" value="paroicous" value_original="paroicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Seta to 2 cm, straight to somewhat flexuose, smooth, red or reddish-brown, occasionally yellowish-brown.</text>
      <biological_entity id="o7686" name="seta" name_original="seta" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="2" to_unit="cm" />
        <character is_modifier="false" name="course" src="d0_s2" value="straight to somewhat" value_original="straight to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="course" src="d0_s2" value="flexuose" value_original="flexuose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s2" value="yellowish-brown" value_original="yellowish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Capsule brown, 1–2 mm, erect, cylindric to ovoid-cylindric, straight to weakly arcuate, becoming ± wrinkled when dry;</text>
      <biological_entity id="o7687" name="capsule" name_original="capsule" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="brown" value_original="brown" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s3" to="ovoid-cylindric" />
        <character char_type="range_value" from="straight" name="course" src="d0_s3" to="weakly arcuate" />
        <character is_modifier="false" modifier="when dry" name="relief" src="d0_s3" value="wrinkled" value_original="wrinkled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>operculum to 0.5 mm;</text>
      <biological_entity id="o7688" name="operculum" name_original="operculum" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>peristome teeth evenly spaced, lanceolate, divided nearly to the base into 2 (–3) filaments, smooth to papillose or sometimes ± striolate.</text>
      <biological_entity constraint="peristome" id="o7689" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="evenly" name="arrangement" src="d0_s5" value="spaced" value_original="spaced" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character constraint="to base" constraintid="o7690" is_modifier="false" name="shape" src="d0_s5" value="divided" value_original="divided" />
        <character char_type="range_value" from="smooth" name="relief" notes="" src="d0_s5" to="papillose" />
        <character is_modifier="false" modifier="sometimes more or less" name="relief" src="d0_s5" value="striolate" value_original="striolate" />
      </biological_entity>
      <biological_entity id="o7690" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o7691" name="filament" name_original="filaments" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s5" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <relation from="o7690" id="r1835" name="into" negation="false" src="d0_s5" to="o7691" />
    </statement>
    <statement id="d0_s6">
      <text>Spores densely and finely papillose, 15-25 µm.</text>
      <biological_entity id="o7692" name="spore" name_original="spores" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s6" value="papillose" value_original="papillose" />
        <character char_type="range_value" from="15" from_unit="um" name="some_measurement" src="d0_s6" to="25" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil, rock, crevices, ledges, banks, occasionally bark</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="crevices" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., N.B., Nfld. and Labr., N.W.T., N.S., Nunavut, Ont., Que., Sask., Yukon; Alaska, Ariz., Calif., Colo., Idaho, Ind., Iowa, Maine, Mich., Mont., Nev., N.H., N.Mex., N.Y., N.Dak., Oreg., S.Dak., Utah, Vt., Wash., Wis., Wyo.; Mexico; Central America; South America; Eurasia; n, c, e, se Asia; Africa; Pacific Islands; Australia; Arctic; Antarctic.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="n" establishment_means="native" />
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="e" establishment_means="native" />
        <character name="distribution" value="se Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="Arctic" establishment_means="native" />
        <character name="distribution" value="Antarctic" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Plants of Distichium capillaceum form dense, rather silky tufts and the slender, spreading, distichous leaves with shiny, whitish, oblong, sheathing bases are characteristic. This is a taller species than D. inclinatum, with plants less crowded, capsules erect-symmetrical and cylindric, and spores smaller. In more northerly or alpine regions with more severe climatic conditions, the plants are often shorter, more compact, and have shorter leaves.</discussion>
  
</bio:treatment>