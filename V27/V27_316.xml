<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="mention_page">233</other_info_on_meta>
    <other_info_on_meta type="mention_page">247</other_info_on_meta>
    <other_info_on_meta type="treatment_page">232</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">grimmia</taxon_name>
    <taxon_name authority="Schimper" date="1856" rank="subgenus">Grimmia</taxon_name>
    <taxon_name authority="Cardot" date="1890" rank="species">crinitoleucophaea</taxon_name>
    <place_of_publication>
      <publication_title>Rev. Bryol.</publication_title>
      <place_in_publication>17: 18. 1890,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus grimmia;subgenus Grimmia;species crinitoleucophaea</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075417</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in loose tufts, olivaceous to black.</text>
      <biological_entity id="o5745" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="olivaceous" name="coloration" notes="" src="d0_s0" to="black" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o5746" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o5745" id="r1356" name="in" negation="false" src="d0_s0" to="o5746" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.5–0.9 cm.</text>
      <biological_entity id="o5747" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s1" to="0.9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves oblong-ovate to oblong-lanceolate, 1.6–2 × 0.3–0.6 mm, concave, awn 0.3–0.6 mm;</text>
      <biological_entity id="o5748" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblong-ovate" name="shape" src="d0_s2" to="oblong-lanceolate" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="length" src="d0_s2" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s2" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity id="o5749" name="awn" name_original="awn" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s2" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal juxtacostal laminal cells quadrate to long-rectangular, straight, thin-walled;</text>
      <biological_entity constraint="basal" id="o5750" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="laminal" id="o5751" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s3" to="long-rectangular" />
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal marginal laminal cells quadrate to short-rectangular, straight, thin-walled;</text>
      <biological_entity constraint="basal marginal laminal" id="o5752" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s4" to="short-rectangular" />
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>medial laminal cells rounded, straight, thick-walled;</text>
      <biological_entity constraint="medial laminal" id="o5753" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal laminal cells 2-stratose, marginal cells 2-stratose.</text>
      <biological_entity constraint="distal laminal" id="o5754" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition dioicous.</text>
      <biological_entity constraint="marginal" id="o5755" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seta sigmoid, 0.3–0.5 mm.</text>
      <biological_entity id="o5756" name="seta" name_original="seta" src="d0_s8" type="structure">
        <character is_modifier="false" name="course_or_shape" src="d0_s8" value="sigmoid" value_original="sigmoid" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s8" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule usually present, exothecial cells thin-walled, annulus of 2–3 rows, rectangular, thick-walled, revoluble, operculum obliquely rostrate, peristome present, rudimentary, teeth composed of only a few rows of cells, perforated, papillose.</text>
      <biological_entity id="o5757" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="exothecial" id="o5758" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <biological_entity id="o5759" name="annulus" name_original="annulus" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="thick-walled" value_original="thick-walled" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="revoluble" value_original="revoluble" />
      </biological_entity>
      <biological_entity id="o5760" name="row" name_original="rows" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s9" to="3" />
      </biological_entity>
      <biological_entity id="o5761" name="operculum" name_original="operculum" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="obliquely" name="shape" src="d0_s9" value="rostrate" value_original="rostrate" />
      </biological_entity>
      <biological_entity id="o5762" name="peristome" name_original="peristome" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s9" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <biological_entity id="o5763" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s9" value="perforated" value_original="perforated" />
        <character is_modifier="false" name="relief" src="d0_s9" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o5764" name="row" name_original="rows" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="only" name="quantity" src="d0_s9" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o5765" name="cell" name_original="cells" src="d0_s9" type="structure" />
      <relation from="o5759" id="r1357" name="consist_of" negation="false" src="d0_s9" to="o5760" />
      <relation from="o5764" id="r1358" name="composed of" negation="false" src="d0_s9" to="o5764" />
      <relation from="o5764" id="r1359" name="part_of" negation="false" src="d0_s9" to="o5765" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Basalt, granite, schist and limestone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="basalt" />
        <character name="habitat" value="granite" />
        <character name="habitat" value="schist" />
        <character name="habitat" value="limestone" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (500-2100 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="500" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., N.W.T., Yukon; Ariz., Calif., Colo., Nev., N.Mex., Utah, Wash.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <discussion>In North America, Grimmia crinitoleucophaea is known from only scattered localities in the American west and in three extremely disjunct sites in Canada: in southern British Columbia, near the Keele River of the Northwest Territories, and along the Dempster Highway in the Yukon. It is found on a broad range of both basic and acidic rock types. Its subgeneric placement is problematic. Gametophytically the species is inseparable from G. tergestina, of subg. Litoneuron. Indeed, based on areolation and leaf shape, L. Loeske (1913–1914, part 1) placed it as a subspecies of the latter. This close similarity may account for reports by J. Muñoz and F. Pando (2000) and D. H. Norris and J. R. Shevock (2004) of G. tergestina from North America. We have seen all cited specimens in these papers and they all represent G. crinitoleucophaea or G. ovalis. Therefore, we reject G. tergestina being in North America. Sporophytic characters of G. crinitoleucophaea (a short, arcuate to sigmoid seta that is eccentrically attached to the capsule, an immersed ventricose capsule with a small, mitrate calyptra, and 3–4 large stomata) clearly indicate membership in subg. Grimmia. Further, G. crinitoleucophaea and G. tergestina have never been collected together, suggesting that the two are also ecologically distinct. Despite its dioicous sexuality, G. crinitoleucophaea is usually fertile; its rudimentary peristome and large annulus are thus readily evident. Its 2-stratose laminal stratification separates it from specimens of G. plagiopodia that may have broken peristome teeth.</discussion>
  
</bio:treatment>