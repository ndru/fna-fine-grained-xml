<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="treatment_page">195</other_info_on_meta>
    <other_info_on_meta type="illustration_page">196</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwägrichen" date="unknown" rank="family">funariaceae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="1849" rank="genus">physcomitrella</taxon_name>
    <taxon_name authority="(Müller Hal.) I. G. Stone &amp; G. A. M. Scott" date="1974" rank="species">readeri</taxon_name>
    <place_of_publication>
      <publication_title>J. Bryol.</publication_title>
      <place_in_publication>7: 604. 1974,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family funariaceae;genus physcomitrella;species readeri</taxon_hierarchy>
    <other_info_on_name type="fna_id">250063093</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ephemerella</taxon_name>
    <taxon_name authority="Müller Hal." date="unknown" rank="species">readeri</taxon_name>
    <place_of_publication>
      <publication_title>Hedwigia</publication_title>
      <place_in_publication>41: 120. 1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Ephemerella;species readeri;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Physcomitrella</taxon_name>
    <taxon_name authority="H. A. Crum &amp; L. E. Anderson" date="unknown" rank="species">californica</taxon_name>
    <taxon_hierarchy>genus Physcomitrella;species californica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Physcomitrella</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">patens</taxon_name>
    <taxon_name authority="(H. A. Crum &amp; L. E. Anderson) B. C. Tan" date="unknown" rank="subspecies">californica</taxon_name>
    <taxon_hierarchy>genus Physcomitrella;species patens;subspecies californica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Physcomitrella</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">patens</taxon_name>
    <taxon_name authority="(Müller Hal.) B. C. Tan" date="unknown" rank="subspecies">readeri</taxon_name>
    <taxon_hierarchy>genus Physcomitrella;species patens;subspecies readeri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 4.5 mm.</text>
      <biological_entity id="o3608" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s0" to="4.5" to_unit="mm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems to 3 mm, branching near base.</text>
      <biological_entity id="o3609" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="3" to_unit="mm" />
        <character constraint="near base" constraintid="o3610" is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o3610" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves to 2 × 0.6 mm, ovatelanceolate, acuminate, margins plane, serrulate in distal 1/3;</text>
      <biological_entity id="o3611" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="in distal 1/3" constraintid="o3613" is_modifier="false" name="architecture_or_shape" notes="" src="d0_s2" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o3612" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="true" name="length" src="d0_s2" unit="mm" value="2" value_original="2" />
        <character is_modifier="true" name="width" src="d0_s2" unit="mm" value="0.6" value_original="0.6" />
        <character is_modifier="true" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="true" name="shape" src="d0_s2" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity constraint="distal" id="o3613" name="1/3" name_original="1/3" src="d0_s2" type="structure" />
      <relation from="o3611" id="r972" name="to" negation="false" src="d0_s2" to="o3612" />
    </statement>
    <statement id="d0_s3">
      <text>costa extending 1/2–2/3 of the leaf length, occasionally slightly forked at apex;</text>
      <biological_entity id="o3614" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character char_type="range_value" from="1/2" name="length" src="d0_s3" to="2/3" />
        <character constraint="at apex" constraintid="o3615" is_modifier="false" modifier="occasionally slightly" name="shape" src="d0_s3" value="forked" value_original="forked" />
      </biological_entity>
      <biological_entity id="o3615" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>proximal cells 75–180 × 35–60 µm, distal cells 45–115 × 15–50 µm, marginal cells not differentiated.</text>
      <biological_entity constraint="proximal" id="o3616" name="bud" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="75" from_unit="um" name="length" src="d0_s4" to="180" to_unit="um" />
        <character char_type="range_value" from="35" from_unit="um" name="width" src="d0_s4" to="60" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="distal" id="o3617" name="bud" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="45" from_unit="um" name="length" src="d0_s4" to="115" to_unit="um" />
        <character char_type="range_value" from="15" from_unit="um" name="width" src="d0_s4" to="50" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o3618" name="bud" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Calyptra entire at base, often with a single slit extending toward apex.</text>
      <biological_entity id="o3619" name="calyptra" name_original="calyptra" src="d0_s5" type="structure">
        <character constraint="at base" constraintid="o3620" is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o3620" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o3621" name="slit" name_original="slit" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o3622" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <relation from="o3619" id="r973" modifier="often" name="with" negation="false" src="d0_s5" to="o3621" />
      <relation from="o3621" id="r974" name="extending toward" negation="false" src="d0_s5" to="o3622" />
    </statement>
    <statement id="d0_s6">
      <text>Spores 27–42 µm.</text>
      <biological_entity id="o3623" name="spore" name_original="spores" src="d0_s6" type="structure">
        <character char_type="range_value" from="27" from_unit="um" name="some_measurement" src="d0_s6" to="42" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature Jan–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Apr" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mineral soil (mudflats) in lake banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mineral soil" />
        <character name="habitat" value="mudflats" />
        <character name="habitat" value="lake banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="low" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Asia (China, Japan); Pacific Islands (New Zealand); Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Physcomitrella readeri is distinguished from the sympatric P. patens mainly by its shorter costa. The size of the rostrum has also been given taxonomic significance, but capsules of P. patens may also bear an apiculum reaching 0.2 mm. Similarly, the forked costa has been considered diagnostic of P. readeri, but this feature is not constant. The status of P. readeri has been repeatedly debated, but the geographic restriction of plants with short costae to a circum-Pacific range may justify taxonomic recognition. Physcomitrella readeri grows with P. patens in two localities in California.</discussion>
  
</bio:treatment>