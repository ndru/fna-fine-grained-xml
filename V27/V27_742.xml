<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">513</other_info_on_meta>
    <other_info_on_meta type="mention_page">517</other_info_on_meta>
    <other_info_on_meta type="treatment_page">516</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="(Schimper) Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">trichostomoideae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">weissia</taxon_name>
    <taxon_name authority="(Sullivant) W. D. Reese &amp; B. A. E. Lemmon" date="1965" rank="species">ludoviciana</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>68: 282. 1965,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily trichostomoideae;genus weissia;species ludoviciana</taxon_hierarchy>
    <other_info_on_name type="fna_id">250061783</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Phascum</taxon_name>
    <taxon_name authority="Sullivant" date="unknown" rank="species">ludovicianum</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray, Manual ed.</publication_title>
      <place_in_publication>2, 616. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Phascum;species ludovicianum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Astomum</taxon_name>
    <taxon_name authority="(Sullivant) Sullivant" date="unknown" rank="species">ludovicianum</taxon_name>
    <taxon_hierarchy>genus Astomum;species ludovicianum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves longlanceolate, base long-elliptic or narrowly ovate, shoulders absent, distal laminal margins plane or erect, apex broadly channeled, acute, mucro very strong, of 7–10 (–14) cells;</text>
      <biological_entity id="o8486" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o8487" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="long-elliptic" value_original="long-elliptic" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s0" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o8488" name="all" name_original="shoulders" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="distal laminal" id="o8489" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="plane" value_original="plane" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o8490" name="apex" name_original="apex" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s0" value="channeled" value_original="channeled" />
        <character is_modifier="false" name="shape" src="d0_s0" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o8491" name="mucro" name_original="mucro" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="very" name="fragility" src="d0_s0" value="strong" value_original="strong" />
      </biological_entity>
      <biological_entity id="o8492" name="cell" name_original="cells" src="d0_s0" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s0" to="14" />
        <character char_type="range_value" from="7" is_modifier="true" name="quantity" src="d0_s0" to="10" />
      </biological_entity>
      <relation from="o8491" id="r2030" name="consist_of" negation="false" src="d0_s0" to="o8492" />
    </statement>
    <statement id="d0_s1">
      <text>costal adaxial stereid band smaller than the abaxial;</text>
      <biological_entity constraint="costal adaxial" id="o8493" name="leaf" name_original="leaf" src="d0_s1" type="structure" />
      <biological_entity id="o8494" name="band" name_original="band" src="d0_s1" type="structure">
        <character constraint="than the abaxial leaf" constraintid="o8495" is_modifier="false" name="size" src="d0_s1" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8495" name="leaf" name_original="leaf" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>distal laminal cells 8–10 (–12) µm wide.</text>
    </statement>
    <statement id="d0_s3">
      <text>Sexual condition rhizautoicous.</text>
      <biological_entity constraint="distal laminal" id="o8496" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="um" name="width" src="d0_s2" to="12" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s2" to="10" to_unit="um" />
        <character is_modifier="false" name="reproduction" src="d0_s3" value="sexual" value_original="sexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Seta short, 0.08–0.015 cm not including the vaginula.</text>
      <biological_entity id="o8497" name="seta" name_original="seta" src="d0_s4" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character char_type="range_value" from="0.08" from_unit="cm" modifier="not" name="some_measurement" src="d0_s4" to="0.015" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8498" name="vaginulum" name_original="vaginula" src="d0_s4" type="structure" />
      <relation from="o8497" id="r2031" name="including the" negation="false" src="d0_s4" to="o8498" />
    </statement>
    <statement id="d0_s5">
      <text>Capsule cleistocarpic, ovoid with a short-rostrate apiculus.</text>
      <biological_entity id="o8499" name="capsule" name_original="capsule" src="d0_s5" type="structure">
        <character constraint="with apiculus" constraintid="o8500" is_modifier="false" name="shape" src="d0_s5" value="ovoid" value_original="ovoid" />
      </biological_entity>
      <biological_entity id="o8500" name="apiculu" name_original="apiculus" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="short-rostrate" value_original="short-rostrate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature fall–spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="spring" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist soil, fields, among grasses, roadside banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist soil" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="grasses" />
        <character name="habitat" value="roadside banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ky., La., Miss., N.C., S.C., Tex., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <discussion>The gametophyte of Weissia ludoviciana is quite like that of W. brachycarpa. If it has a hybrid origin, then the archegoniate parent is the latter species. Weissia ludoviciana, W. muehlenbergiana, and W. phascopsis are similar in the short-seta, cleistocarpic capsules and long-lanceolate leaves, but differ most constantly in the traits given in the key. Additional differences observed, which may prove inconstant on revision, are: W. ludoviciana is rhizautoicous, with antheridia in extremely small, serrate-leaved buds, on soil near base of the archegoniophore, the capsule has stomates and a weakly differentiated annulus of 3–4 rows of smaller cells, and the calyptra is ca. 1 mm; W. muehlenbergiana is cladautoicous, the capsule lacks stomates and an annulus is not distinguishable, and the calyptra is 0.5–0.6 mm; W. phascopsis is cladautoicous, the capsule has stomates and the annulus somewhat differentiated as a ridge, and the calyptra is 1.2–1.4 mm. The hybrid Weissia ludoviciana × W. controversa was reported by W. D. Reese and B. E. Lemmon (1965).</discussion>
  
</bio:treatment>