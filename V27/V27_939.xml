<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Richard H. Zander</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">480</other_info_on_meta>
    <other_info_on_meta type="mention_page">604</other_info_on_meta>
    <other_info_on_meta type="mention_page">638</other_info_on_meta>
    <other_info_on_meta type="treatment_page">637</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="Müller Hal." date="1847" rank="genus">ACAULON</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Zeitung (Berlin)</publication_title>
      <place_in_publication>5: 99. 1847  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus ACAULON</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek a-, without, and kaulos, stalk or stem, alluding to stemless habit</other_info_on_name>
    <other_info_on_name type="fna_id">100158</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants bulbiform, gregarious or scattered, reddish to yellowish-brown distally, brown proximally.</text>
      <biological_entity id="o4226" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="bulbiform" value_original="bulbiform" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="gregarious" value_original="gregarious" />
        <character is_modifier="false" name="arrangement" src="d0_s0" value="scattered" value_original="scattered" />
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s0" to="yellowish-brown distally" />
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems short, to 0.5 mm;</text>
      <biological_entity id="o4227" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hyalodermis absent, sclerodermis absent, central strand absent;</text>
      <biological_entity id="o4228" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o4229" name="scleroderm" name_original="sclerodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="central" id="o4230" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>axillary hairs to ca. 5 cells in length, proximal cell-walls sometimes thickened.</text>
      <biological_entity constraint="axillary" id="o4231" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s3" to="5" />
      </biological_entity>
      <biological_entity id="o4232" name="cell" name_original="cells" src="d0_s3" type="structure" />
      <biological_entity constraint="proximal" id="o4233" name="cell-wall" name_original="cell-walls" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes" name="size_or_width" src="d0_s3" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stem-leaves strongly appressed and apices usually reflexed when dry, appressed to weakly spreading when moist;</text>
      <biological_entity id="o4234" name="stem-leaf" name_original="stem-leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="strongly" name="fixation_or_orientation" src="d0_s4" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ovate, adaxial surface deeply concave, 0.5–1.75 mm;</text>
      <biological_entity id="o4235" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s4" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="appressed" modifier="when moist" name="orientation" src="d0_s4" to="weakly spreading" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4236" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s5" value="concave" value_original="concave" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1.75" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>base not differentiated in shape, proximal margins not differentiated;</text>
      <biological_entity id="o4237" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o4238" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s6" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal margins plane or very weakly recurved, entire to serrulate or dentate;</text>
      <biological_entity constraint="distal" id="o4239" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="very weakly" name="orientation" src="d0_s7" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="entire" name="architecture_or_shape" src="d0_s7" to="serrulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>apex broadly acute, abruptly apiculate;</text>
      <biological_entity id="o4240" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="abruptly" name="architecture_or_shape" src="d0_s8" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>costa excurrent in a stout, sharp apiculus, occasionally as a short, occasionally dentate awn, adaxial outgrowths absent in American species, adaxial cells elongate, in 3–4 rows;</text>
      <biological_entity id="o4241" name="costa" name_original="costa" src="d0_s9" type="structure">
        <character constraint="in apiculus" constraintid="o4242" is_modifier="false" name="architecture" src="d0_s9" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o4242" name="apiculu" name_original="apiculus" src="d0_s9" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s9" value="stout" value_original="stout" />
        <character is_modifier="true" name="shape" src="d0_s9" value="sharp" value_original="sharp" />
      </biological_entity>
      <biological_entity id="o4243" name="awn" name_original="awn" src="d0_s9" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
        <character is_modifier="true" modifier="occasionally" name="architecture_or_shape" src="d0_s9" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4244" name="outgrowth" name_original="outgrowths" src="d0_s9" type="structure">
        <character constraint="in american, species" constraintid="o4245, o4246" is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o4245" name="american" name_original="american" src="d0_s9" type="structure" />
      <biological_entity id="o4246" name="species" name_original="species" src="d0_s9" type="taxon_name" />
      <biological_entity constraint="adaxial" id="o4247" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o4248" name="row" name_original="rows" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s9" to="4" />
      </biological_entity>
      <relation from="o4241" id="r1023" modifier="occasionally" name="as" negation="false" src="d0_s9" to="o4243" />
      <relation from="o4247" id="r1024" name="in" negation="false" src="d0_s9" to="o4248" />
    </statement>
    <statement id="d0_s10">
      <text>transverse-section round, adaxial epidermis present, adaxial stereid band absent, guide cells 0–4 in 1 layer, hydroid strand usually present, occasionally central, abaxial stereid band present, usually weak, rounded in sectional shape, abaxial epidermis present;</text>
      <biological_entity id="o4249" name="transverse-section" name_original="transverse-section" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="round" value_original="round" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4250" name="epidermis" name_original="epidermis" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4251" name="epidermis" name_original="epidermis" src="d0_s10" type="structure" />
      <biological_entity id="o4252" name="band" name_original="band" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="guide" id="o4253" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="in layer" constraintid="o4254" from="0" name="quantity" src="d0_s10" to="4" />
      </biological_entity>
      <biological_entity id="o4254" name="layer" name_original="layer" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="hydroid" id="o4255" name="strand" name_original="strand" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o4256" name="epidermis" name_original="epidermis" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="occasionally" name="position" src="d0_s10" value="central" value_original="central" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4257" name="epidermis" name_original="epidermis" src="d0_s10" type="structure" />
      <biological_entity id="o4258" name="band" name_original="band" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="usually" name="fragility" src="d0_s10" value="weak" value_original="weak" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4259" name="epidermis" name_original="epidermis" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>proximal cells not differentiated in shape, rectangular, little wider than distal cells, 3–4: 1, walls of proximal cells thin;</text>
      <biological_entity constraint="proximal" id="o4260" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s11" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rectangular" value_original="rectangular" />
        <character constraint="than distal cells" constraintid="o4261" is_modifier="false" name="width" src="d0_s11" value="wider" value_original="wider" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s11" to="4" />
        <character name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4261" name="cell" name_original="cells" src="d0_s11" type="structure" />
      <biological_entity id="o4262" name="wall" name_original="walls" src="d0_s11" type="structure">
        <character is_modifier="false" name="width" src="d0_s11" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o4263" name="cell" name_original="cells" src="d0_s11" type="structure" />
      <relation from="o4262" id="r1025" name="part_of" negation="false" src="d0_s11" to="o4263" />
    </statement>
    <statement id="d0_s12">
      <text>distal medial cells roundedquadrate to rhomboid, ca. 13–15 µm wide, 1–4: 1, 1-stratose;</text>
      <biological_entity constraint="distal medial" id="o4264" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character char_type="range_value" from="roundedquadrate" name="shape" src="d0_s12" to="rhomboid" />
        <character char_type="range_value" from="13" from_unit="um" name="width" src="d0_s12" to="15" to_unit="um" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s12" to="4" />
        <character name="quantity" src="d0_s12" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>papillae absent or occasionally large and simple, one per lumen, cell-walls evenly thickened, occasionally highly thickened on abaxial walls, convex on both sides of lamina.</text>
      <biological_entity constraint="distal medial" id="o4265" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character char_type="range_value" from="roundedquadrate" name="shape" src="d0_s13" to="rhomboid" />
        <character char_type="range_value" from="13" from_unit="um" name="width" src="d0_s13" to="15" to_unit="um" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s13" to="4" />
      </biological_entity>
      <biological_entity id="o4266" name="papilla" name_original="papillae" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="occasionally" name="size" src="d0_s13" value="large" value_original="large" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o4267" name="lumen" name_original="lumen" src="d0_s13" type="structure" />
      <biological_entity constraint="abaxial" id="o4269" name="wall" name_original="walls" src="d0_s13" type="structure" />
      <biological_entity id="o4270" name="side" name_original="sides" src="d0_s13" type="structure" />
      <biological_entity id="o4271" name="lamina" name_original="lamina" src="d0_s13" type="structure" />
      <relation from="o4266" id="r1026" name="per" negation="false" src="d0_s13" to="o4267" />
      <relation from="o4270" id="r1027" name="part_of" negation="false" src="d0_s13" to="o4271" />
    </statement>
    <statement id="d0_s14">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s15">
      <text>Sexual condition dioicous and perigoniate plants smaller, or monoicous, usually paroicous.</text>
      <biological_entity id="o4268" name="cell-wall" name_original="cell-walls" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="evenly" name="size_or_width" src="d0_s13" value="thickened" value_original="thickened" />
        <character constraint="on abaxial walls" constraintid="o4269" is_modifier="false" modifier="occasionally highly" name="size_or_width" src="d0_s13" value="thickened" value_original="thickened" />
        <character constraint="on sides" constraintid="o4270" is_modifier="false" name="shape" notes="" src="d0_s13" value="convex" value_original="convex" />
        <character is_modifier="false" name="development" src="d0_s14" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s15" value="sexual" value_original="sexual" />
      </biological_entity>
      <biological_entity id="o4272" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s15" value="dioicous" value_original="dioicous" />
        <character is_modifier="true" name="architecture" src="d0_s15" value="perigoniate" value_original="perigoniate" />
        <character is_modifier="false" name="size" src="d0_s15" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="reproduction" src="d0_s15" value="monoicous" value_original="monoicous" />
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s15" value="paroicous" value_original="paroicous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Perichaetia terminal, interior leaves somewhat enlarged.</text>
      <biological_entity id="o4273" name="perichaetium" name_original="perichaetia" src="d0_s16" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s16" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity constraint="interior" id="o4274" name="leaf" name_original="leaves" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="somewhat" name="size" src="d0_s16" value="enlarged" value_original="enlarged" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seta very short, to 0.2 mm.</text>
      <biological_entity id="o4275" name="seta" name_original="seta" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s17" value="short" value_original="short" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s17" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Capsule cleistocarpous, spheric, apiculus lacking, ca. 0.4–0.7 mm, annulus absent.</text>
      <biological_entity id="o4276" name="capsule" name_original="capsule" src="d0_s18" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s18" value="cleistocarpous" value_original="cleistocarpous" />
        <character is_modifier="false" name="shape" src="d0_s18" value="spheric" value_original="spheric" />
      </biological_entity>
      <biological_entity id="o4277" name="apiculu" name_original="apiculus" src="d0_s18" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s18" value="lacking" value_original="lacking" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s18" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4278" name="annulus" name_original="annulus" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Calyptra mitrate, often lobed.</text>
      <biological_entity id="o4279" name="calyptra" name_original="calyptra" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="mitrate" value_original="mitrate" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s19" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Spores ca. 25–35 (–50) µm. KOH laminal color reaction red.</text>
      <biological_entity id="o4280" name="spore" name_original="spores" src="d0_s20" type="structure">
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s20" to="50" to_unit="um" />
        <character char_type="range_value" from="25" from_unit="um" name="some_measurement" src="d0_s20" to="35" to_unit="um" />
        <character is_modifier="false" name="coloration" src="d0_s20" value="laminal" value_original="laminal" />
        <character is_modifier="false" name="coloration" src="d0_s20" value="red" value_original="red" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide, mainly in temperate regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
        <character name="distribution" value="mainly in temperate regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>38.</number>
  <discussion>Species 15 (3 in the flora).</discussion>
  <discussion>Acaulon is similar to species of Microbryum in the bulbiform habit, leaves red in KOH, but its species are even smaller in size, and the capsules are spheric, lacking an apiculus. Papillae are absent in two of the three species. Although spore size and ornamentation have figured in taxonomy in the past, according to H. A. Crum and L. E. Anderson (1981) there is much overlap; but, similarly, their use of differences in margin recurvature and seta length do not hold. Taxa belonging to the old, superficially similar Phascum have been recently placed largely with Tortula and Microbryum (R. H. Zander 1993). I. G. Stone (e.g., 1989) has studied the genus extensively in Australia, and Casas de Puig (e.g., C. Casas de Puig et al. 1990) and her students have done the same for the Iberian Peninsula.</discussion>
  <references>
    <reference>Bryan, V. S. 1956b. Cytological and taxonomic studies of some species of Astomum, Acaulon and Phascum. Bryologist 59: 118–129.</reference>
    <reference>Grout, A. J. 1922–1940. Acaulon. In: A. J. Grout, Moss Flora of North America, North of Mexico.   3 vols. in 12 parts. Newfane, Vt. and New York. Vol. 1, pp. 194–195.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves awned, laminal cells papillose abaxially.</description>
      <determination>2 Acaulon schimperianum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves cuspidate or muticus, laminal cells smooth</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants often three-angled, about 1 mm, leaves keeled, seta about as long as the diameter of the capsule, spores about 30 µm, finely papillose.</description>
      <determination>1 Acaulon triquetrum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants flattened-globose or three-angled, about 2 mm, leaves broadly channeled, seta short, about 0.3 the diameter of the capsule, spores 30-50 µm, smooth or papillose.</description>
      <determination>3 Acaulon muticum</determination>
    </key_statement>
  </key>
</bio:treatment>