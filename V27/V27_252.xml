<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">15</other_info_on_meta>
    <other_info_on_meta type="mention_page">25</other_info_on_meta>
    <other_info_on_meta type="mention_page">189</other_info_on_meta>
    <other_info_on_meta type="mention_page">191</other_info_on_meta>
    <other_info_on_meta type="treatment_page">190</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwägrichen" date="unknown" rank="family">funariaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">funaria</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="species">hygrometrica</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>172. 1801,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family funariaceae;genus funaria;species hygrometrica</taxon_hierarchy>
    <other_info_on_name type="fna_id">200001339</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 4–10 or more mm, with a basal antheridial branch, medium green to yellowish green;</text>
      <biological_entity constraint="basal" id="o3030" name="antheridial" name_original="antheridial" src="d0_s0" type="structure" />
      <biological_entity constraint="basal" id="o3031" name="branch" name_original="branch" src="d0_s0" type="structure" />
      <relation from="o3029" id="r818" name="with" negation="false" src="d0_s0" to="o3030" />
      <relation from="o3029" id="r819" name="with" negation="false" src="d0_s0" to="o3031" />
    </statement>
    <statement id="d0_s1">
      <text>leafless proximally with leaves crowded and bulbiform distally, sometimes laxly foliate throughout.</text>
      <biological_entity id="o3029" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s0" to="10" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="medium" value_original="medium" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="yellowish green" />
        <character constraint="with leaves" constraintid="o3032" is_modifier="false" name="architecture" src="d0_s1" value="leafless" value_original="leafless" />
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s1" value="bulbiform" value_original="bulbiform" />
        <character is_modifier="false" modifier="sometimes laxly; throughout" name="architecture" src="d0_s1" value="foliate" value_original="foliate" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o3032" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="crowded" value_original="crowded" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves smaller proximally, distal leaves 2–4 mm, deeply concave, oblong-ovate to broadly obovate distally, acute to apiculate or short-acuminate, entire or weakly serrulate distally;</text>
      <biological_entity id="o3033" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="proximally" name="size" src="d0_s2" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity constraint="distal" id="o3034" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s2" value="concave" value_original="concave" />
        <character char_type="range_value" from="oblong-ovate" name="shape" src="d0_s2" to="broadly obovate distally" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="weakly; distally" name="architecture_or_shape" src="d0_s2" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>costa subpercurrent to short-excurrent;</text>
      <biological_entity id="o3035" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="subpercurrent" value_original="subpercurrent" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="short-excurrent" value_original="short-excurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>distal laminal cells thin-walled and inflated, hexagonal or oblong-hexagonal becoming much more oblong proximally.</text>
      <biological_entity constraint="distal laminal" id="o3036" name="bud" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="thin-walled" value_original="thin-walled" />
        <character is_modifier="false" name="shape" src="d0_s4" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="shape" src="d0_s4" value="hexagonal" value_original="hexagonal" />
        <character is_modifier="false" modifier="becoming much; much" name="shape" src="d0_s4" value="oblong-hexagonal" value_original="oblong-hexagonal" />
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Seta usually (12–) 20–45 (–80) mm, slender and flexuose, usually hygroscopic.</text>
      <biological_entity id="o3037" name="seta" name_original="seta" src="d0_s5" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="80" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s5" to="45" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s5" value="slender" value_original="slender" />
        <character is_modifier="false" name="course" src="d0_s5" value="flexuose" value_original="flexuose" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s5" value="hygroscopic" value_original="hygroscopic" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsule 2–3.5 mm, pyriform, asymmetric, curved to straight, horizontal to pendent or merely inclined or nearly erect, becoming sulcate when dry below the strongly oblique mouth;</text>
      <biological_entity id="o3038" name="capsule" name_original="capsule" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="asymmetric" value_original="asymmetric" />
        <character char_type="range_value" from="curved" name="course" src="d0_s6" to="straight" />
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s6" to="pendent or merely inclined or nearly erect" />
        <character is_modifier="false" modifier="when dry below the strongly oblique mouth" name="architecture" src="d0_s6" value="sulcate" value_original="sulcate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>annulus revoluble, operculum slightly convex;</text>
      <biological_entity id="o3039" name="annulus" name_original="annulus" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="revoluble" value_original="revoluble" />
      </biological_entity>
      <biological_entity id="o3040" name="operculum" name_original="operculum" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s7" value="convex" value_original="convex" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peristome brown, papillose-striate proximally and papillose distally, strongly trabeculate, becoming appendiculate distally, forming a lattice by fusion of the tips;</text>
      <biological_entity id="o3041" name="peristome" name_original="peristome" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="proximally" name="coloration_or_pubescence_or_relief" src="d0_s8" value="papillose-striate" value_original="papillose-striate" />
        <character is_modifier="false" modifier="distally" name="relief" src="d0_s8" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s8" value="trabeculate" value_original="trabeculate" />
        <character is_modifier="false" modifier="becoming; distally" name="architecture" src="d0_s8" value="appendiculate" value_original="appendiculate" />
      </biological_entity>
      <biological_entity id="o3042" name="lattice" name_original="lattice" src="d0_s8" type="structure" />
      <biological_entity id="o3043" name="tip" name_original="tips" src="d0_s8" type="structure" />
      <relation from="o3041" id="r820" name="forming a" negation="false" src="d0_s8" to="o3042" />
    </statement>
    <statement id="d0_s9">
      <text>endostome segments lanceolate about 2/3 as long as the teeth, yellowish, finely papillose-striate.</text>
      <biological_entity constraint="endostome" id="o3044" name="segment" name_original="segments" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character constraint="as-long-as teeth" constraintid="o3045" name="quantity" src="d0_s9" value="2/3" value_original="2/3" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s9" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="finely" name="coloration_or_pubescence_or_relief" src="d0_s9" value="papillose-striate" value_original="papillose-striate" />
      </biological_entity>
      <biological_entity id="o3045" name="tooth" name_original="teeth" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Calyptra cucullate, smooth.</text>
      <biological_entity id="o3046" name="calyptra" name_original="calyptra" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spores mostly 12–21 µm, finely papillose.</text>
      <biological_entity id="o3047" name="spore" name_original="spores" src="d0_s11" type="structure">
        <character char_type="range_value" from="12" from_unit="um" name="some_measurement" src="d0_s11" to="21" to_unit="um" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s11" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide except Antarctica.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide except Antarctica" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Varieties ca. 20 (2 in the flora).</discussion>
  <discussion>Funaria hygrometrica is one of the most common, weedy, and widely distributed mosses in the world; its distribution closely parallels that of Bryum argenteum. It is widely illustrated in textbooks to demonstrate the life cycle of a typical moss, possibly because of the abundant conspicuous sporophytes produced and its frequent presence in greenhouses. However, the peristome with opposite, instead of alternate, teeth in the two peristome rows is clearly atypical among the majority of mosses. Most of the varieties that have been described probably do not merit recognition because of the morphological plasticity of the species in response to environmental conditions.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Capsule 2-3.5 mm, horizontal to pendent, curved, capsule neck less tapered, mouth of capsule narrow</description>
      <determination>2a Funaria hygrometrica var. hygrometrica</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Capsule 2-3 mm, inclined to nearly erect, straight or weakly curved, capsule narrowly tapered to a long slender neck, mouth of capsule wide.</description>
      <determination>2b Funaria hygrometrica var. calvescens</determination>
    </key_statement>
  </key>
</bio:treatment>