<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ronald A. Pursell</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="treatment_page">329</other_info_on_meta>
    <other_info_on_meta type="illustrator">Patricia M. Eckel</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Bescherelle" date="unknown" rank="family">BRYOXIPHIACEAE</taxon_name>
    <taxon_hierarchy>family BRYOXIPHIACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10131</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small.</text>
      <biological_entity id="o6671" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems unbranched or infrequently branched subapically, bulblike or not at proximal end, erect or pendent, loosely to densely tufted, rarely solitary;</text>
      <biological_entity id="o6672" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="infrequently" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="bulblike" value_original="bulblike" />
        <character name="architecture_or_shape" src="d0_s1" value="not" value_original="not" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="pendent" value_original="pendent" />
        <character is_modifier="false" modifier="loosely to densely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6673" name="end" name_original="end" src="d0_s1" type="structure" />
      <relation from="o6672" id="r1583" name="at" negation="false" src="d0_s1" to="o6673" />
    </statement>
    <statement id="d0_s2">
      <text>axillary hairs filiform, hyaline, or basal and adjacent cell pale tan or pale rose-violet;</text>
      <biological_entity constraint="axillary" id="o6674" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o6675" name="cell" name_original="cell" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="adjacent" value_original="adjacent" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="pale tan" value_original="pale tan" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="pale rose-violet" value_original="pale rose-violet" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>paraphyllia none;</text>
      <biological_entity id="o6676" name="paraphyllium" name_original="paraphyllia" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="0" value_original="none" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>pseudoparaphyllia none;</text>
      <biological_entity id="o6677" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s4" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s4" value="0" value_original="none" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>rhizoids basal, light-brown to reddish, smooth.</text>
      <biological_entity id="o6678" name="rhizoid" name_original="rhizoids" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
        <character char_type="range_value" from="light-brown" name="coloration" src="d0_s5" to="reddish" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaves distichous, tightly imbricate, changing little when dry, conduplicate;</text>
    </statement>
    <statement id="d0_s7">
      <text>1-costate, costae supporting low abaxial lamellae;</text>
      <biological_entity id="o6679" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="distichous" value_original="distichous" />
        <character is_modifier="false" modifier="tightly" name="arrangement" src="d0_s6" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="when dry" name="arrangement_or_vernation" src="d0_s6" value="conduplicate" value_original="conduplicate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-costate" value_original="1-costate" />
      </biological_entity>
      <biological_entity id="o6680" name="costa" name_original="costae" src="d0_s7" type="structure" />
      <biological_entity constraint="abaxial" id="o6681" name="lamella" name_original="lamellae" src="d0_s7" type="structure">
        <character is_modifier="true" name="position" src="d0_s7" value="low" value_original="low" />
      </biological_entity>
      <relation from="o6680" id="r1584" name="supporting" negation="false" src="d0_s7" to="o6681" />
    </statement>
    <statement id="d0_s8">
      <text>laminal cells firm-walled, 1-stratose, smooth, somewhat bulging.</text>
    </statement>
    <statement id="d0_s9">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="laminal" id="o6682" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="firm-walled" value_original="firm-walled" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="somewhat" name="pubescence_or_shape" src="d0_s8" value="bulging" value_original="bulging" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perigonia and perichaetia terminal, indistinct, without and with paraphyses, respectively, archegonia about 1/2 length of paraphyses;</text>
      <biological_entity id="o6683" name="perigonium" name_original="perigonia" src="d0_s10" type="structure" constraint="paraphyse; paraphyse">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s10" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="prominence" src="d0_s10" value="indistinct" value_original="indistinct" />
        <character is_modifier="false" modifier="respectively; respectively; respectively; respectively" name="character" notes="" src="d0_s10" value="length" value_original="length" />
      </biological_entity>
      <biological_entity id="o6684" name="perichaetium" name_original="perichaetia" src="d0_s10" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s10" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="prominence" src="d0_s10" value="indistinct" value_original="indistinct" />
        <character is_modifier="false" modifier="respectively; respectively; respectively; respectively" name="character" notes="" src="d0_s10" value="length" value_original="length" />
      </biological_entity>
      <biological_entity id="o6685" name="paraphyse" name_original="paraphyses" src="d0_s10" type="structure" />
      <biological_entity id="o6686" name="archegonium" name_original="archegonia" src="d0_s10" type="structure">
        <character modifier="respectively; respectively; respectively; respectively" name="quantity" src="d0_s10" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o6687" name="paraphyse" name_original="paraphyses" src="d0_s10" type="structure" />
      <relation from="o6683" id="r1585" name="without and with" negation="false" src="d0_s10" to="o6685" />
      <relation from="o6683" id="r1586" name="without and with" negation="false" src="d0_s10" to="o6686" />
      <relation from="o6684" id="r1587" modifier="respectively; respectively; respectively; respectively" name="without and with" negation="false" src="d0_s10" to="o6685" />
      <relation from="o6684" id="r1588" modifier="respectively; respectively; respectively; respectively" name="without and with" negation="false" src="d0_s10" to="o6686" />
      <relation from="o6683" id="r1589" modifier="respectively; respectively; respectively; respectively" name="part_of" negation="false" src="d0_s10" to="o6687" />
      <relation from="o6684" id="r1590" modifier="respectively; respectively; respectively; respectively" name="part_of" negation="false" src="d0_s10" to="o6687" />
    </statement>
    <statement id="d0_s11">
      <text>antheridia and archegonia few.</text>
      <biological_entity id="o6688" name="antheridium" name_original="antheridia" src="d0_s11" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s11" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o6689" name="archegonium" name_original="archegonia" src="d0_s11" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s11" value="few" value_original="few" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Sporophytes single.</text>
      <biological_entity id="o6690" name="sporophyte" name_original="sporophytes" src="d0_s12" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s12" value="single" value_original="single" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seta short, erect or somewhat curved.</text>
      <biological_entity id="o6691" name="seta" name_original="seta" src="d0_s13" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="short" value_original="short" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="somewhat" name="course" src="d0_s13" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsule subglobose, emergent, erect or somewhat inclined, radially symmetric, stomatose;</text>
      <biological_entity id="o6692" name="capsule" name_original="capsule" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="location" src="d0_s14" value="emergent" value_original="emergent" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="somewhat" name="orientation" src="d0_s14" value="inclined" value_original="inclined" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s14" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>annulus absent;</text>
      <biological_entity id="o6693" name="annulus" name_original="annulus" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>peristome absent;</text>
      <biological_entity id="o6694" name="peristome" name_original="peristome" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>operculum persistent, obliquely rostellate, attached to columella after dehiscence.</text>
      <biological_entity id="o6695" name="operculum" name_original="operculum" src="d0_s17" type="structure">
        <character is_modifier="false" name="duration" src="d0_s17" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="obliquely" name="shape" src="d0_s17" value="rostellate" value_original="rostellate" />
        <character constraint="to columella" constraintid="o6696" is_modifier="false" name="fixation" src="d0_s17" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o6696" name="columella" name_original="columella" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Calyptra cucullate.</text>
      <biological_entity id="o6697" name="calyptra" name_original="calyptra" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="cucullate" value_original="cucullate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Spores spheric.</text>
      <biological_entity id="o6698" name="spore" name_original="spores" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="spheric" value_original="spheric" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Europe, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <discussion>Genus 1, species 2 (1 in the flora).</discussion>
  <references>
    <reference>Britton, E. G. 1913. Bryoxiphiaceae. In: N. L. Britton et al., eds. 1905+. North American Flora.... 47+ vols. New York. Vol. 15, pp. 69–70.</reference>
    <reference>Löve, Á. and D. Löve. 1953. Studies on Bryoxiphium. Bryologist 56: 73–94, 183–203.</reference>
    <reference>Steere, W. C. 1937. Bryoxiphium norvegicum, the sword moss, as a preglacial and interglacial relic. Ecology 18: 346–358.</reference>
  </references>
  
</bio:treatment>