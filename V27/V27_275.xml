<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ann E. Rushing</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="treatment_page">202</other_info_on_meta>
    <other_info_on_meta type="illustrator">Patricia M. Eckel</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindberg" date="unknown" rank="family">GIGASPERMACEAE</taxon_name>
    <taxon_hierarchy>family GIGASPERMACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10369</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants minute, with upright branches arising from pale, fleshy, subterranean, aphyllous stems.</text>
      <biological_entity id="o4104" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="minute" value_original="minute" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4105" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="upright" value_original="upright" />
        <character constraint="from stems" constraintid="o4106" is_modifier="false" name="orientation" src="d0_s0" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o4106" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="pale" value_original="pale" />
        <character is_modifier="true" name="texture" src="d0_s0" value="fleshy" value_original="fleshy" />
        <character is_modifier="true" name="location" src="d0_s0" value="subterranean" value_original="subterranean" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="aphyllous" value_original="aphyllous" />
      </biological_entity>
      <relation from="o4104" id="r1120" name="with" negation="false" src="d0_s0" to="o4105" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves crowded distally, broadly concave, ovate, elliptic to obovate costa single or absent;</text>
      <biological_entity id="o4107" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="distally" name="arrangement" src="d0_s1" value="crowded" value_original="crowded" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s1" value="concave" value_original="concave" />
        <character is_modifier="false" name="shape" src="d0_s1" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s1" to="obovate" />
      </biological_entity>
      <biological_entity id="o4108" name="costa" name_original="costa" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="single" value_original="single" />
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cell-walls of lamina often thickened at corners.</text>
      <biological_entity id="o4110" name="lamina" name_original="lamina" src="d0_s2" type="structure" />
      <biological_entity id="o4111" name="corner" name_original="corners" src="d0_s2" type="structure" />
      <relation from="o4109" id="r1121" name="part_of" negation="false" src="d0_s2" to="o4110" />
    </statement>
    <statement id="d0_s3">
      <text>Sexual condition paroicous [synoicous, or occasionally dioicous].</text>
      <biological_entity id="o4109" name="cell-wall" name_original="cell-walls" src="d0_s2" type="structure">
        <character constraint="at corners" constraintid="o4111" is_modifier="false" modifier="often" name="size_or_width" src="d0_s2" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="reproduction" src="d0_s3" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s3" value="paroicous" value_original="paroicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Seta short to moderately elongate.</text>
      <biological_entity id="o4112" name="seta" name_original="seta" src="d0_s4" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" modifier="moderately" name="shape" src="d0_s4" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Capsule globose or hemispheric, commonly spongy or wrinkled, immersed or exserted, gymnostomous or cleistocarpous.</text>
      <biological_entity id="o4113" name="capsule" name_original="capsule" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s5" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" modifier="commonly" name="texture" src="d0_s5" value="spongy" value_original="spongy" />
        <character is_modifier="false" name="relief" src="d0_s5" value="wrinkled" value_original="wrinkled" />
        <character is_modifier="false" name="prominence" src="d0_s5" value="immersed" value_original="immersed" />
        <character is_modifier="false" name="position" src="d0_s5" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="dehiscence" src="d0_s5" value="cleistocarpous" value_original="cleistocarpous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Calyptra very small, conic [mitrate], fugacious.</text>
      <biological_entity id="o4114" name="calyptra" name_original="calyptra" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="very" name="size" src="d0_s6" value="small" value_original="small" />
        <character is_modifier="false" name="shape" src="d0_s6" value="conic" value_original="conic" />
        <character is_modifier="false" name="duration" src="d0_s6" value="fugacious" value_original="fugacious" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>s United States, usually Southern Hemisphere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="s United States" establishment_means="native" />
        <character name="distribution" value="usually Southern Hemisphere" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <discussion>Genera 6, species 9 (1 in the flora).</discussion>
  <discussion>The major defining feature of Gigaspermaceae is the fleshy, underground stem from which short gametophore branches are produced in abundance. Sporophyte features differentiate the six, mostly monotypic genera, Chamaebryum Thériot &amp; Dixon in South Africa, Costesia Thériot in South America, Gigaspermum Lindberg in Mexico, South Africa, Australia, and Pacific Islands (New Zealand), Lorentziella in southern United States and South America, Neosharpiella H. Robinson &amp; Delgadillo in Mexico and South America, and Oedipodiella Dixon in South Africa.</discussion>
  <references>
    <reference>Fife, A. J. 1980. The affinities of Costesia and Neosharpiella and notes on the Gigaspermaceae (Musci). Bryologist 83: 466–476.</reference>
  </references>
  
</bio:treatment>