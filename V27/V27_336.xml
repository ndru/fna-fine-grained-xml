<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">229</other_info_on_meta>
    <other_info_on_meta type="mention_page">235</other_info_on_meta>
    <other_info_on_meta type="mention_page">242</other_info_on_meta>
    <other_info_on_meta type="mention_page">258</other_info_on_meta>
    <other_info_on_meta type="treatment_page">245</other_info_on_meta>
    <other_info_on_meta type="illustration_page">246</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">grimmia</taxon_name>
    <taxon_name authority="I. Hagen" date="1909" rank="subgenus">litoneuron</taxon_name>
    <taxon_name authority="Greven" date="2002" rank="species">nevadensis</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>105: 273, fig. 1. 2002 (as nevadense),</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus grimmia;subgenus litoneuron;species nevadensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443396</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in dense cushions, blackish green.</text>
      <biological_entity id="o4077" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="blackish green" value_original="blackish green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4078" name="cushion" name_original="cushions" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o4077" id="r935" name="in" negation="false" src="d0_s0" to="o4078" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.5–0.7 (–1) cm, central strand weak.</text>
      <biological_entity id="o4079" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="1" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s1" to="0.7" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o4080" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s1" value="weak" value_original="weak" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves ovatelanceolate, 1.5–2 mm, both margins plane, incurved distally, intermarginal bands absent, awn 0.4–0.8 mm, costal transverse-section prominent, semicircular;</text>
      <biological_entity id="o4081" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s2" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4082" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="distally" name="orientation" src="d0_s2" value="incurved" value_original="incurved" />
      </biological_entity>
      <biological_entity constraint="intermarginal" id="o4083" name="band" name_original="bands" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o4084" name="awn" name_original="awn" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s2" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="costal" id="o4085" name="transverse-section" name_original="transverse-section" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="semicircular" value_original="semicircular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal juxtacostal laminal cells short-rectangular, straight, thin-walled;</text>
      <biological_entity constraint="basal" id="o4086" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="laminal" id="o4087" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="short-rectangular" value_original="short-rectangular" />
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal marginal laminal cells short-rectangular, straight, with thick transverse and thin lateral walls, not hyaline;</text>
      <biological_entity constraint="basal marginal laminal" id="o4088" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="short-rectangular" value_original="short-rectangular" />
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="not" name="coloration" notes="" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o4089" name="wall" name_original="walls" src="d0_s4" type="structure">
        <character is_modifier="true" name="width" src="d0_s4" value="thick" value_original="thick" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s4" value="transverse" value_original="transverse" />
        <character is_modifier="true" name="width" src="d0_s4" value="thin" value_original="thin" />
      </biological_entity>
      <relation from="o4088" id="r936" name="with" negation="false" src="d0_s4" to="o4089" />
    </statement>
    <statement id="d0_s5">
      <text>medial laminal cells quadrate to rounded, straight, thin-walled;</text>
      <biological_entity constraint="medial laminal" id="o4090" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s5" to="rounded" />
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal laminal cells 2-stratose.</text>
      <biological_entity constraint="distal laminal" id="o4091" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Perichaetial leaves not enlarged.</text>
    </statement>
    <statement id="d0_s8">
      <text>Sexuality dioicous.</text>
      <biological_entity constraint="perichaetial" id="o4092" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s7" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seta straight, 0.9–1.1 mm.</text>
      <biological_entity id="o4093" name="seta" name_original="seta" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s9" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsule commonly present, emergent, chestnut-brown, ovate with distinct constriction below rim, exothecial cells rectangular, thick-walled, stomata absent, annulus of 1 row of quadrate, thick-walled cells, operculum conic, peristome absent.</text>
      <biological_entity id="o4094" name="capsule" name_original="capsule" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="commonly" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="location" src="d0_s10" value="emergent" value_original="emergent" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="chestnut-brown" value_original="chestnut-brown" />
        <character constraint="with constriction" constraintid="o4095" is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o4095" name="constriction" name_original="constriction" src="d0_s10" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o4096" name="rim" name_original="rim" src="d0_s10" type="structure" />
      <biological_entity constraint="exothecial" id="o4097" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
      <biological_entity id="o4098" name="stoma" name_original="stomata" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o4099" name="annulus" name_original="annulus" src="d0_s10" type="structure" />
      <biological_entity id="o4100" name="row" name_original="row" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o4101" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="quadrate" value_original="quadrate" />
        <character is_modifier="true" name="architecture" src="d0_s10" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
      <biological_entity id="o4102" name="operculum" name_original="operculum" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="conic" value_original="conic" />
      </biological_entity>
      <biological_entity id="o4103" name="peristome" name_original="peristome" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o4095" id="r937" name="below" negation="false" src="d0_s10" to="o4096" />
      <relation from="o4099" id="r938" name="consist_of" negation="false" src="d0_s10" to="o4100" />
      <relation from="o4100" id="r939" name="part_of" negation="false" src="d0_s10" to="o4101" />
    </statement>
    <statement id="d0_s11">
      <text>Calyptra cucullate.</text>
      <biological_entity id="o4104" name="calyptra" name_original="calyptra" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="cucullate" value_original="cucullate" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Acidic sedimentary rocks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="acidic sedimentary rocks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (1900-2500 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1900" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Grimmia nevadensis is a rare but locally abundant endemic, known only from the Sierra Nevada Mountains. Fertile specimens of it can be separated from G. mariniana by the absence of peristome teeth and by the chestnut brown capsules. Gametophytically, specimens of G. nevadensis contrast with those of G. mariniana by having a uniform basal areolation of short-rectangular cells with thin lateral walls, and margins not hyaline. If fertile, the immersed, eperistomate capsules will separate G. nevadensis from G. ovalis, G. alpestris, and G. montana. Sterile specimens can be separated from G. alpestris by the absence of bulging cells, and the uniformly short-rectangular, thin-walled basal laminal cells. While both G. nevadensis and G. montana have plane to incurved distal margins and both lack bulging laminal cells, the uniformly short-rectangular, thin-walled basal laminal cells and concave leaves will identify G. nevadensis. The latter is easily separated from G. arizonae and G. pilifera, in contrast with which it lacks stomata and peristome teeth, and its concave leaves with plane to incurved margins differ markedly from the keeled leaves with recurved leaf margins of the others.</discussion>
  
</bio:treatment>