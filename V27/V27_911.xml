<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">619</other_info_on_meta>
    <other_info_on_meta type="treatment_page">623</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="Bridel" date="1801" rank="genus">syntrichia</taxon_name>
    <taxon_name authority="(Müller Hal.) Ochyra" date="1992" rank="species">sinensis</taxon_name>
    <place_of_publication>
      <publication_title>Fragm. Florist. Geobot.</publication_title>
      <place_in_publication>37: 213. 1992,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus syntrichia;species sinensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">240002111</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Barbula</taxon_name>
    <taxon_name authority="Müller Hal." date="unknown" rank="species">sinensis</taxon_name>
    <place_of_publication>
      <publication_title>Nuovo Giorn. Bot. Ital., n. s.</publication_title>
      <place_in_publication>3: 100. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Barbula;species sinensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tortula</taxon_name>
    <taxon_name authority="(Müller Hal.) Brotherus" date="unknown" rank="species">sinensis</taxon_name>
    <taxon_hierarchy>genus Tortula;species sinensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 4–15 mm.</text>
      <biological_entity id="o2421" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s0" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves longitudinally folded and spirally twisted around the stem but little crisped when dry, widespreading when moist, oblong-lingulate to spatulate, 2–4.5 × 0.6–1.6 mm;</text>
      <biological_entity id="o2422" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="longitudinally" name="architecture_or_shape" src="d0_s1" value="folded" value_original="folded" />
        <character constraint="around stem" constraintid="o2423" is_modifier="false" modifier="spirally" name="architecture" src="d0_s1" value="twisted" value_original="twisted" />
        <character is_modifier="false" modifier="when moist" name="orientation" notes="" src="d0_s1" value="widespreading" value_original="widespreading" />
        <character char_type="range_value" from="oblong-lingulate" name="shape" src="d0_s1" to="spatulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s1" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s1" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2423" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s1" value="crisped" value_original="crisped" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>margins revolute in the proximal 1/2, entire;</text>
      <biological_entity id="o2424" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character constraint="in proximal 1/2" constraintid="o2425" is_modifier="false" name="shape_or_vernation" src="d0_s2" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o2425" name="1/2" name_original="1/2" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>apices acute;</text>
      <biological_entity id="o2426" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>costa excurrent into a smooth to slightly toothed, hyaline awn, brown or reddish, smooth;</text>
      <biological_entity id="o2427" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character constraint="into awn" constraintid="o2428" is_modifier="false" name="architecture" src="d0_s4" value="excurrent" value_original="excurrent" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o2428" name="awn" name_original="awn" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="true" modifier="slightly" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal-cells abruptly differentiated, narrower toward the margins;</text>
      <biological_entity id="o2429" name="basal-cell" name_original="basal-cells" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abruptly" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
        <character constraint="toward margins" constraintid="o2430" is_modifier="false" name="width" src="d0_s5" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o2430" name="margin" name_original="margins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>distal cells polygonal, or quadrate, 12–20 µm, with 8–10 papillae per cell;</text>
      <biological_entity constraint="distal" id="o2431" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="polygonal" value_original="polygonal" />
        <character is_modifier="false" name="shape" src="d0_s6" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="polygonal" value_original="polygonal" />
        <character is_modifier="false" name="shape" src="d0_s6" value="quadrate" value_original="quadrate" />
        <character char_type="range_value" from="12" from_unit="um" name="some_measurement" src="d0_s6" to="20" to_unit="um" />
      </biological_entity>
      <biological_entity id="o2432" name="papilla" name_original="papillae" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s6" to="10" />
      </biological_entity>
      <biological_entity id="o2433" name="cell" name_original="cell" src="d0_s6" type="structure" />
      <relation from="o2431" id="r579" name="with" negation="false" src="d0_s6" to="o2432" />
      <relation from="o2432" id="r580" name="per" negation="false" src="d0_s6" to="o2433" />
    </statement>
    <statement id="d0_s7">
      <text>marginal cells not differentiated.</text>
    </statement>
    <statement id="d0_s8">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s9">
      <text>Sexual condition autoicous.</text>
      <biological_entity constraint="marginal" id="o2434" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s7" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="development" src="d0_s8" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seta red, 15–25 mm.</text>
      <biological_entity id="o2435" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="red" value_original="red" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s10" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsule red, 3–3.8 mm, straight or slightly curved, with a distinct neck;</text>
      <biological_entity id="o2436" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="red" value_original="red" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="3.8" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s11" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o2437" name="neck" name_original="neck" src="d0_s11" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o2436" id="r581" name="with" negation="false" src="d0_s11" to="o2437" />
    </statement>
    <statement id="d0_s12">
      <text>operculum 1.2–1.5 mm, red;</text>
      <biological_entity id="o2438" name="operculum" name_original="operculum" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>peristome 0.8–1.1 mm, red, the basal membrane pale, about 1/4 the total length.</text>
      <biological_entity id="o2439" name="peristome" name_original="peristome" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s13" to="1.1" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="red" value_original="red" />
      </biological_entity>
      <biological_entity constraint="basal" id="o2440" name="membrane" name_original="membrane" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="pale" value_original="pale" />
        <character name="length" src="d0_s13" value="1/4" value_original="1/4" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Spores 12–18 µm, papillose.</text>
      <biological_entity id="o2441" name="spore" name_original="spores" src="d0_s14" type="structure">
        <character char_type="range_value" from="12" from_unit="um" name="some_measurement" src="d0_s14" to="18" to_unit="um" />
        <character is_modifier="false" name="relief" src="d0_s14" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Vertical limestone faces</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="vertical limestone" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="high" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., N.Mex.; Europe; Asia; n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <discussion>Only recently discovered in the Rocky Mountains, Syntrichia sinensis may have been overlooked elsewhere in the flora area. For example, it is to be expected in the northern part of the Sierra Nevada of California where limestone is exposed.</discussion>
  
</bio:treatment>