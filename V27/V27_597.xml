<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Steven G. Newmaster</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">359</other_info_on_meta>
    <other_info_on_meta type="mention_page">362</other_info_on_meta>
    <other_info_on_meta type="mention_page">421</other_info_on_meta>
    <other_info_on_meta type="mention_page">423</other_info_on_meta>
    <other_info_on_meta type="treatment_page">420</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="I. Hagen" date="1915" rank="genus">KIAERIA</taxon_name>
    <place_of_publication>
      <publication_title>Kongel. Norske Vidensk. Selsk. Skr. (Trondheim)</publication_title>
      <place_in_publication>1914: 109. 1915  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus KIAERIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Frantz Caspar Kiaer, 1835–1893, Norwegian bryologist</other_info_on_name>
    <other_info_on_name type="fna_id">117084</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dicranum</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="unknown" rank="section">Falcata</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Europ.</publication_title>
      <place_in_publication>1: 117. 1847</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Dicranum;section Falcata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in erect, loose to dense tufts, green, yellowish or brownish, shiny or dull.</text>
      <biological_entity id="o4256" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="dull" value_original="dull" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4257" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o4256" id="r970" name="in" negation="false" src="d0_s0" to="o4257" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–2 (–6) cm, erect-spreading, simple branches, sparsely radiculose near apex.</text>
      <biological_entity id="o4258" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="2" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect-spreading" value_original="erect-spreading" />
      </biological_entity>
      <biological_entity id="o4259" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o4260" name="apex" name_original="apex" src="d0_s1" type="structure" />
      <relation from="o4259" id="r971" modifier="sparsely" name="near" negation="false" src="d0_s1" to="o4260" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves lanceolate, gradually subulate, erect-spreading, sometimes falcate-secund;</text>
      <biological_entity id="o4261" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s2" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s2" value="falcate-secund" value_original="falcate-secund" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins erect, entire, to serrulate near the tips;</text>
      <biological_entity id="o4262" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character char_type="range_value" from="entire" name="architecture_or_shape" src="d0_s3" to="serrulate" />
        <character char_type="range_value" constraint="near tips" constraintid="o4263" from="entire" name="architecture_or_shape" src="d0_s3" to="serrulate" />
      </biological_entity>
      <biological_entity id="o4263" name="tip" name_original="tips" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>costa mostly excurrent, narrow, stereids poorly differentiated from median guide cells;</text>
      <biological_entity id="o4264" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s4" value="excurrent" value_original="excurrent" />
        <character is_modifier="false" name="size_or_width" src="d0_s4" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o4265" name="stereid" name_original="stereids" src="d0_s4" type="structure">
        <character constraint="from median guide cells" constraintid="o4266" is_modifier="false" modifier="poorly" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity constraint="median guide" id="o4266" name="cell" name_original="cells" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>distal laminal cells linear to subquadrate, smooth or slightly mammillose;</text>
      <biological_entity constraint="distal laminal" id="o4267" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="subquadrate" />
        <character is_modifier="false" name="relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s5" value="mammillose" value_original="mammillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal laminal cells elongate, smooth, sometimes porose, alar cells usually brown and well differentiated.</text>
      <biological_entity constraint="basal laminal" id="o4268" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s6" value="porose" value_original="porose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition cladautoicous or gonioautoicous.</text>
    </statement>
    <statement id="d0_s8">
      <text>Pericheatial leaves with a sheathing base.</text>
      <biological_entity id="o4269" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s6" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="well" name="variability" src="d0_s6" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="cladautoicous" value_original="cladautoicous" />
      </biological_entity>
      <biological_entity id="o4270" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o4271" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s8" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <relation from="o4270" id="r972" name="with" negation="false" src="d0_s8" to="o4271" />
    </statement>
    <statement id="d0_s9">
      <text>Seta solitary, 7–16 mm, erect, yellow turning red with age.</text>
      <biological_entity id="o4272" name="seta" name_original="seta" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s9" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="16" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character constraint="with age" constraintid="o4273" is_modifier="false" name="coloration" src="d0_s9" value="yellow turning red" value_original="yellow turning red" />
      </biological_entity>
      <biological_entity id="o4273" name="age" name_original="age" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Capsule suberect, curved, cylindric, smooth to furrowed when dry, indistinctly strumose;</text>
      <biological_entity id="o4274" name="capsule" name_original="capsule" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="suberect" value_original="suberect" />
        <character is_modifier="false" name="course" src="d0_s10" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s10" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="smooth" modifier="when dry" name="architecture" src="d0_s10" to="furrowed" />
        <character is_modifier="false" modifier="indistinctly" name="shape" src="d0_s10" value="strumose" value_original="strumose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>operculum obliquely rostrate;</text>
      <biological_entity id="o4275" name="operculum" name_original="operculum" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="obliquely" name="shape" src="d0_s11" value="rostrate" value_original="rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>peristome single, of 16, teeth, divided halfway into two segments, papillose to vertically striolate or pitted-striolate, redbrown.</text>
      <biological_entity id="o4276" name="peristome" name_original="peristome" src="d0_s12" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s12" value="single" value_original="single" />
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
      </biological_entity>
      <biological_entity id="o4277" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="divided" value_original="divided" />
        <character constraint="into segments" constraintid="o4278" is_modifier="false" name="position" src="d0_s12" value="halfway" value_original="halfway" />
        <character char_type="range_value" from="papillose" name="relief" notes="" src="d0_s12" to="vertically striolate" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pitted-striolate" value_original="pitted-striolate" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="redbrown" value_original="redbrown" />
      </biological_entity>
      <biological_entity id="o4278" name="segment" name_original="segments" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Calyptrae cucullate, occasionally rough at apex.</text>
      <biological_entity id="o4279" name="calyptra" name_original="calyptrae" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="cucullate" value_original="cucullate" />
        <character constraint="at apex" constraintid="o4280" is_modifier="false" modifier="occasionally" name="pubescence_or_relief" src="d0_s13" value="rough" value_original="rough" />
      </biological_entity>
      <biological_entity id="o4280" name="apex" name_original="apex" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Spores spheric, 14–24 µm, finely roughened, yellow to greenish yellow.</text>
      <biological_entity id="o4281" name="spore" name_original="spores" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="spheric" value_original="spheric" />
        <character char_type="range_value" from="14" from_unit="um" name="some_measurement" src="d0_s14" to="24" to_unit="um" />
        <character is_modifier="false" modifier="finely" name="relief_or_texture" src="d0_s14" value="roughened" value_original="roughened" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s14" to="greenish yellow" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, South America, Europe, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <discussion>Species ca. 6 (4 in the flora).</discussion>
  <discussion>A northern and alpine genus, Kiaeria occurs on siliceous rock or soil and is recognized by its medium-sized, Dicranum-like habit, with poorly differentiated stereid and guide cells. It may be confused with several other tufted acrocarps of similar size, such as Dicranoweisia, which has crisped, contorted leaves when dry; Oncophorus, which has distinctly strumose capsules and occurs on wood or humus; or Dicranella, which differs in its obliquely grooved capsules, and narrow hairlike leaves. The species in Kiaeria are difficult to distinguish, but some characters are useful for tentative field identification, including presence of grooves on capsule, leaf luster, growth form, and habitat.</discussion>
  <references>
    <reference>Franklova, H. 2001. Distribution of the species of Kiaeria. Cas. Nár. Muz. Rada Prír. 170: 75–79.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Distal laminal cells elongate; capsule ribbed or grooved when dry</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Distal laminal cells short, rectangular or quadrate; capsule smooth when dry</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Distal laminal cells not porose, basal laminal cells occasionally porose.</description>
      <determination>1 Kiaeria starkei</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Distal and basal laminal cells porose.</description>
      <determination>4 Kiaeria glacialis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants in loose tufts; leaves erect-spreading, flexuose.</description>
      <determination>2 Kiaeria blyttii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants in dense tufts; leaves falcate-secund.</description>
      <determination>3 Kiaeria falcata</determination>
    </key_statement>
  </key>
</bio:treatment>