<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">206</other_info_on_meta>
    <other_info_on_meta type="mention_page">228</other_info_on_meta>
    <other_info_on_meta type="mention_page">247</other_info_on_meta>
    <other_info_on_meta type="treatment_page">249</other_info_on_meta>
    <other_info_on_meta type="illustration_page">248</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">grimmia</taxon_name>
    <taxon_name authority="Limpricht" date="1889" rank="subgenus">rhabdogrimmia</taxon_name>
    <taxon_name authority="(Müller Hal. &amp; Kindberg) Kindberg" date="1898" rank="species">attenuata</taxon_name>
    <place_of_publication>
      <publication_title>Eur. N. Amer. Bryin.</publication_title>
      <place_in_publication>2: 228. 1898,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus grimmia;subgenus rhabdogrimmia;species attenuata</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443436</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Racomitrium</taxon_name>
    <taxon_name authority="Müller Hal. &amp; Kindberg" date="unknown" rank="species">alternuatum</taxon_name>
    <place_of_publication>
      <publication_title>in J. Macoun and N. C. Kindberg, Cat. Canad. Pl., Musci,</publication_title>
      <place_in_publication>73. 1892</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Racomitrium;species alternuatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants robust, brownish to reddish in distal part, black and frequently defoliated proximally.</text>
      <biological_entity id="o6273" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s0" value="robust" value_original="robust" />
        <character char_type="range_value" constraint="in distal part" constraintid="o6274" from="brownish" name="coloration" src="d0_s0" to="reddish" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="black" value_original="black" />
        <character is_modifier="false" modifier="frequently; proximally" name="architecture" src="d0_s0" value="defoliated" value_original="defoliated" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6274" name="part" name_original="part" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 5–10 cm, central strand absent.</text>
      <biological_entity id="o6275" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o6276" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves straight appressed when dry, erectopatent when moist, lanceolate, 2.7–3.5 × 0.5–0.75 mm, sharply keeled distally, margins narrowly reflexed on both sides, awns terete, firm and short, denticulate, costa 70-120 µm wide at base, channeled distally, circular, projecting on dorsal side;</text>
      <biological_entity id="o6277" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="when dry" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s2" value="erectopatent" value_original="erectopatent" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="length" src="d0_s2" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="0.75" to_unit="mm" />
        <character is_modifier="false" modifier="sharply; distally" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o6278" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character constraint="on sides" constraintid="o6279" is_modifier="false" modifier="narrowly" name="orientation" src="d0_s2" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o6279" name="side" name_original="sides" src="d0_s2" type="structure" />
      <biological_entity id="o6280" name="awn" name_original="awns" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
        <character is_modifier="false" name="texture" src="d0_s2" value="firm" value_original="firm" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s2" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o6281" name="costa" name_original="costa" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="at base" constraintid="o6282" from="70" from_unit="um" name="width" src="d0_s2" to="120" to_unit="um" />
        <character is_modifier="false" modifier="distally" name="shape" notes="" src="d0_s2" value="channeled" value_original="channeled" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="circular" value_original="circular" />
        <character constraint="on dorsal side" constraintid="o6283" is_modifier="false" name="orientation" src="d0_s2" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity id="o6282" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity constraint="dorsal" id="o6283" name="side" name_original="side" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>basal juxtacostal laminal cells elongate with extremely thick, slightly nodulose walls;</text>
      <biological_entity constraint="basal" id="o6284" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="laminal" id="o6285" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character constraint="with walls" constraintid="o6286" is_modifier="false" name="shape" src="d0_s3" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o6286" name="wall" name_original="walls" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="extremely" name="width" src="d0_s3" value="thick" value_original="thick" />
        <character is_modifier="true" modifier="slightly" name="shape" src="d0_s3" value="nodulose" value_original="nodulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal marginal laminal cells in a few rows short-rectangular, thin-walled;</text>
      <biological_entity constraint="basal marginal laminal" id="o6287" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s4" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <biological_entity id="o6288" name="row" name_original="rows" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="few" value_original="few" />
        <character is_modifier="false" name="shape" src="d0_s4" value="short-rectangular" value_original="short-rectangular" />
      </biological_entity>
      <relation from="o6287" id="r1506" name="in" negation="false" src="d0_s4" to="o6288" />
    </statement>
    <statement id="d0_s5">
      <text>medial laminal cells short to long-rectangular with extremely thick and sinuose lateral walls;</text>
      <biological_entity constraint="medial laminal" id="o6289" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character constraint="with lateral walls" constraintid="o6290" is_modifier="false" name="shape" src="d0_s5" value="long-rectangular" value_original="long-rectangular" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o6290" name="wall" name_original="walls" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="extremely" name="width" src="d0_s5" value="thick" value_original="thick" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sinuose" value_original="sinuose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal laminal cells 1-stratose, apex and margins 2-stratose.</text>
      <biological_entity constraint="distal laminal" id="o6291" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity constraint="distal laminal" id="o6292" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity constraint="distal laminal" id="o6293" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Gemmae absent.</text>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition dioicous.</text>
      <biological_entity id="o6294" name="gemma" name_original="gemmae" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seta cygneous, 3–4 mm.</text>
      <biological_entity id="o6295" name="seta" name_original="seta" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cygneous" value_original="cygneous" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsule sporadically present, exserted, yellowish-brown, ovoid, smooth, exothecial cells thin-walled with incrassate corners, annulus present, operculum rostrate, peristome teeth orange, 40–60 µm wide at base, split and perforated, smooth proximally, papillose distally.</text>
      <biological_entity id="o6296" name="capsule" name_original="capsule" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sporadically" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellowish-brown" value_original="yellowish-brown" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="exothecial" id="o6297" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character constraint="with corners" constraintid="o6298" is_modifier="false" name="architecture" src="d0_s10" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <biological_entity id="o6298" name="corner" name_original="corners" src="d0_s10" type="structure">
        <character is_modifier="true" name="size" src="d0_s10" value="incrassate" value_original="incrassate" />
      </biological_entity>
      <biological_entity id="o6299" name="annulus" name_original="annulus" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o6300" name="operculum" name_original="operculum" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rostrate" value_original="rostrate" />
      </biological_entity>
      <biological_entity constraint="peristome" id="o6301" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="orange" value_original="orange" />
        <character char_type="range_value" constraint="at base" constraintid="o6302" from="40" from_unit="um" name="width" src="d0_s10" to="60" to_unit="um" />
      </biological_entity>
      <biological_entity id="o6302" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o6303" name="split" name_original="split" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="perforated" value_original="perforated" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="distally" name="relief" src="d0_s10" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Calyptra not seen.</text>
      <biological_entity id="o6304" name="calyptra" name_original="calyptra" src="d0_s11" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Loose tufts on dry boulders</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="loose tufts" constraint="on dry boulders" />
        <character name="habitat" value="dry boulders" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-1000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska, Idaho, Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>27.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Grimmia attenuata resembles forms of the extremely variable Racomitrium heterostichum (= Bucklandiella heterosticha), and because G. N. Jones (1933) mentioned five varieties of the latter, it is not surprising that in some herbaria, e.g. NY, all specimens of G. attenuata have been filed as varieties of R. heterostichum. In 1898, Kindberg realized that his species is not a Racomitrium, but a Grimmia, and wrote: “Habit of G. elatior.”  In spite of Kindberg’s correct transfer to Grimmia, T. C. Frye (1917–1918) synonymized G. attenuata with R. macounii. Adding to the confusion, H. A. Möller (1929) synonymized it with G. elatior, and J. Muñoz (1999b) cited it as G. arcuatifolia, which is a synonym of G. lisae. Grimmia elatior is characterized by ellipsoid striate capsules, leaf margin broadly recurved on just one side, and an opaque 2-stratose distal lamina with rounded usually mammillose to papillose cells, the mid leaf cells quadrate to short-rectangular with slightly sinuose, thick walls. Like Grimmia leibergii, G. attenuata is characterized by its elongate to linear, strongly nodulose and thick-walled basal juxtacostal cells, its extremely thick-walled and sinuouse medial cells, leaf margins both recurved, and stem central strand absent. However, the two differ in costa shape and basal marginal cell shape.</discussion>
  
</bio:treatment>