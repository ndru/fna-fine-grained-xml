<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Patricia M. Eckel</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">477</other_info_on_meta>
    <other_info_on_meta type="mention_page">497</other_info_on_meta>
    <other_info_on_meta type="mention_page">498</other_info_on_meta>
    <other_info_on_meta type="mention_page">499</other_info_on_meta>
    <other_info_on_meta type="mention_page">562</other_info_on_meta>
    <other_info_on_meta type="treatment_page">496</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="(Schimper) Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">trichostomoideae</taxon_name>
    <taxon_name authority="Lindberg" date="1864" rank="genus">PLEUROCHAETE</taxon_name>
    <place_of_publication>
      <publication_title>Öfvers. Kongl. Vetensk.-Akad. Förh.</publication_title>
      <place_in_publication>21: 253. 1864  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily trichostomoideae;genus PLEUROCHAETE</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek pleura, side or rib, and chaite, long hair or mane, alluding to laterally borne sporophytes</other_info_on_name>
    <other_info_on_name type="fna_id">125998</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Barbula</taxon_name>
    <taxon_name authority="(Lindberg) Müller Hal." date="unknown" rank="section">Pleurochaete</taxon_name>
    <taxon_hierarchy>genus Barbula;section Pleurochaete;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants coarse, robust, forming deep, loose turfs, distally sordid yellow-green, proximally pale to dark-brown.</text>
      <biological_entity id="o8005" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="relief" src="d0_s0" value="coarse" value_original="coarse" />
        <character is_modifier="false" name="fragility" src="d0_s0" value="robust" value_original="robust" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s0" value="sordid yellow-green" value_original="sordid yellow-green" />
        <character char_type="range_value" from="proximally pale" name="coloration" src="d0_s0" to="dark-brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8006" name="turf" name_original="turfs" src="d0_s0" type="structure">
        <character is_modifier="true" name="depth" src="d0_s0" value="deep" value_original="deep" />
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o8005" id="r1919" name="forming" negation="false" src="d0_s0" to="o8006" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 3–4 (–6) cm, ascending from a prostrate base, sparsely and irregularly branched, hyalodermis present, inconspicuous, sclerodermis of 2–4 rows of thickened cells, central strand with a few, small, thin-walled cells, axillary hairs all hyaline, to 15 cells in length, tomentum lacking.</text>
      <biological_entity id="o8007" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="6" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" />
        <character constraint="from base" constraintid="o8008" is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="sparsely; irregularly" name="architecture" notes="" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o8008" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
      </biological_entity>
      <biological_entity id="o8009" name="hyalodermi" name_original="hyalodermis" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s1" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o8010" name="scleroderm" name_original="sclerodermis" src="d0_s1" type="structure" />
      <biological_entity id="o8011" name="row" name_original="rows" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s1" to="4" />
      </biological_entity>
      <biological_entity id="o8012" name="cell" name_original="cells" src="d0_s1" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s1" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity constraint="central" id="o8013" name="strand" name_original="strand" src="d0_s1" type="structure" />
      <biological_entity id="o8014" name="cell" name_original="cells" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="few" value_original="few" />
        <character is_modifier="true" name="size" src="d0_s1" value="small" value_original="small" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o8015" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s1" to="15" />
      </biological_entity>
      <biological_entity id="o8016" name="cell" name_original="cells" src="d0_s1" type="structure" />
      <biological_entity id="o8017" name="tomentum" name_original="tomentum" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="lacking" value_original="lacking" />
      </biological_entity>
      <relation from="o8010" id="r1920" name="consist_of" negation="false" src="d0_s1" to="o8011" />
      <relation from="o8011" id="r1921" name="part_of" negation="false" src="d0_s1" to="o8012" />
      <relation from="o8013" id="r1922" name="with" negation="false" src="d0_s1" to="o8014" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves strongly and loosely incurved-contorted when dry, squarrose-recurved beyond the erect, sheathing base when wet;</text>
      <biological_entity id="o8019" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="sheathing" value_original="sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>narrowly to broadly oblong-lanceolate, distally broadly channeled to tubulose, base erect, broadly oblong-ovate, gradually tapering to the narrow 1–2-twisted limb;</text>
      <biological_entity id="o8018" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s2" value="incurved-contorted" value_original="incurved-contorted" />
        <character constraint="beyond base" constraintid="o8019" is_modifier="false" name="orientation" src="d0_s2" value="squarrose-recurved" value_original="squarrose-recurved" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character char_type="range_value" from="distally broadly channeled" name="shape" src="d0_s3" to="tubulose" />
      </biological_entity>
      <biological_entity id="o8020" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="oblong-ovate" value_original="oblong-ovate" />
        <character constraint="to limb" constraintid="o8021" is_modifier="false" modifier="gradually" name="shape" src="d0_s3" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o8021" name="limb" name_original="limb" src="d0_s3" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s3" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="1-2-twisted" value_original="1-2-twisted" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins undulate, incurved to plane, in the distal region regularly to irregularly denticulate-serrulate, lamina 1-stratose throughout, bordered in the proximal 1/4–3/4 region with elongate, hyaline, thin-walled rhomboidal cells in a band 4–10 cells wide, widest in the basal region at the abaxial angle of base and limb, strongly narrowed proximally to the leaf insertion with cells becoming quadrate to shortrectangular, marginal band narrowing distally and disappearing into the limb, the border smooth throughout or distally the cells becoming irregularly serrulate at their distal ends, the serrulations extending into the distal region of chlorophyllose cells;</text>
      <biological_entity id="o8022" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="incurved" value_original="incurved" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8023" name="region" name_original="region" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="regularly to irregularly" name="architecture_or_shape" src="d0_s4" value="denticulate-serrulate" value_original="denticulate-serrulate" />
      </biological_entity>
      <biological_entity id="o8024" name="lamina" name_original="lamina" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="throughout" name="architecture" src="d0_s4" value="1-stratose" value_original="1-stratose" />
        <character constraint="in region" constraintid="o8025" is_modifier="false" name="architecture" src="d0_s4" value="bordered" value_original="bordered" />
      </biological_entity>
      <biological_entity id="o8025" name="region" name_original="region" src="d0_s4" type="structure">
        <character is_modifier="true" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="1/4" is_modifier="true" name="quantity" src="d0_s4" to="3/4" />
      </biological_entity>
      <biological_entity id="o8026" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="elongate" value_original="elongate" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="thin-walled" value_original="thin-walled" />
        <character is_modifier="true" name="shape" src="d0_s4" value="rhomboidal" value_original="rhomboidal" />
      </biological_entity>
      <biological_entity id="o8027" name="band" name_original="band" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s4" to="10" />
      </biological_entity>
      <biological_entity id="o8028" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character constraint="in basal region" constraintid="o8029" is_modifier="false" name="width" src="d0_s4" value="widest" value_original="widest" />
        <character constraint="to leaf" constraintid="o8033" is_modifier="false" modifier="strongly" name="shape" notes="" src="d0_s4" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity constraint="basal" id="o8029" name="region" name_original="region" src="d0_s4" type="structure" />
      <biological_entity constraint="abaxial" id="o8030" name="angle" name_original="angle" src="d0_s4" type="structure" />
      <biological_entity id="o8031" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o8032" name="limb" name_original="limb" src="d0_s4" type="structure" />
      <biological_entity id="o8033" name="leaf" name_original="leaf" src="d0_s4" type="structure" />
      <biological_entity id="o8034" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character constraint="to shortrectangular" constraintid="o8035" is_modifier="false" modifier="becoming" name="insertion" src="d0_s4" value="quadrate" value_original="quadrate" />
      </biological_entity>
      <biological_entity id="o8035" name="shortrectangular" name_original="shortrectangular" src="d0_s4" type="structure" />
      <biological_entity constraint="marginal" id="o8036" name="band" name_original="band" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="distally" name="width" src="d0_s4" value="narrowing" value_original="narrowing" />
        <character constraint="into limb" constraintid="o8037" is_modifier="false" name="prominence" src="d0_s4" value="disappearing" value_original="disappearing" />
      </biological_entity>
      <biological_entity id="o8037" name="limb" name_original="limb" src="d0_s4" type="structure" />
      <biological_entity id="o8038" name="border" name_original="border" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="throughout" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o8039" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character constraint="at distal ends" constraintid="o8040" is_modifier="false" modifier="becoming irregularly" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8040" name="end" name_original="ends" src="d0_s4" type="structure" />
      <biological_entity id="o8041" name="serrulation" name_original="serrulations" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o8042" name="region" name_original="region" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o8043" name="cell" name_original="cells" src="d0_s4" type="structure" />
      <relation from="o8022" id="r1923" name="in" negation="false" src="d0_s4" to="o8023" />
      <relation from="o8025" id="r1924" name="with" negation="false" src="d0_s4" to="o8026" />
      <relation from="o8026" id="r1925" name="in" negation="false" src="d0_s4" to="o8027" />
      <relation from="o8029" id="r1926" name="at" negation="false" src="d0_s4" to="o8030" />
      <relation from="o8030" id="r1927" name="part_of" negation="false" src="d0_s4" to="o8031" />
      <relation from="o8030" id="r1928" name="part_of" negation="false" src="d0_s4" to="o8032" />
      <relation from="o8033" id="r1929" name="with" negation="false" src="d0_s4" to="o8034" />
      <relation from="o8041" id="r1930" name="extending into the" negation="false" src="d0_s4" to="o8042" />
      <relation from="o8041" id="r1931" name="extending into the" negation="false" src="d0_s4" to="o8043" />
    </statement>
    <statement id="d0_s5">
      <text>apex gradually acuminate and short-aristate with smooth margins to broadly acute and mucronate;</text>
      <biological_entity id="o8044" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character constraint="with margins" constraintid="o8045" is_modifier="false" name="shape" src="d0_s5" value="short-aristate" value_original="short-aristate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o8045" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa excurrent as a short, pointed awn or mucro, covered adaxially with quadrate, papillose cells, abaxially elongate and smooth;</text>
      <biological_entity id="o8046" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character constraint="as mucro" constraintid="o8048" is_modifier="false" name="architecture" src="d0_s6" value="excurrent" value_original="excurrent" />
        <character constraint="with cells" constraintid="o8049" is_modifier="false" name="position_relational" notes="" src="d0_s6" value="covered" value_original="covered" />
        <character is_modifier="false" modifier="abaxially" name="shape" notes="" src="d0_s6" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o8047" name="awn" name_original="awn" src="d0_s6" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s6" value="short" value_original="short" />
        <character is_modifier="true" name="shape" src="d0_s6" value="pointed" value_original="pointed" />
      </biological_entity>
      <biological_entity id="o8048" name="mucro" name_original="mucro" src="d0_s6" type="structure" />
      <biological_entity id="o8049" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="quadrate" value_original="quadrate" />
        <character is_modifier="true" name="relief" src="d0_s6" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>transverse-section semicircular to reniform, adaxial epidermis present, adaxial stereid band present, robust, guide cells 4–6 in 1 layer, hydroid strand absent, basal-cells (4–) 6–7 (–10):1, echlorophyllose and hyaline, walls as thick as distal cells, smooth, often porose juxtacostally, distal cells subquadrate, 7–12 (–14) µm wide, chlorophyllose, evenly thickened, densely papillose, papillae dense, 2-fid, 1–4 per lumen, cells of basal margin forming a border of oblong or oblong-linear, large, inflated, thin-walled and hyaline cells.</text>
      <biological_entity id="o8050" name="transverse-section" name_original="transverse-section" src="d0_s7" type="structure">
        <character char_type="range_value" from="semicircular" name="shape" src="d0_s7" to="reniform" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o8051" name="epidermis" name_original="epidermis" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o8052" name="epidermis" name_original="epidermis" src="d0_s7" type="structure" />
      <biological_entity id="o8053" name="band" name_original="band" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="fragility" src="d0_s7" value="robust" value_original="robust" />
      </biological_entity>
      <biological_entity constraint="guide" id="o8054" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="in layer" constraintid="o8055" from="4" name="quantity" src="d0_s7" to="6" />
      </biological_entity>
      <biological_entity id="o8055" name="layer" name_original="layer" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="hydroid" id="o8056" name="strand" name_original="strand" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o8057" name="basal-cell" name_original="basal-cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s7" to="6" to_inclusive="false" />
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="10" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s7" to="7" />
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o8058" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="often; juxtacostally" name="architecture" src="d0_s7" value="porose" value_original="porose" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8059" name="cell" name_original="cells" src="d0_s7" type="structure" />
      <biological_entity constraint="distal" id="o8060" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="subquadrate" value_original="subquadrate" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="um" name="width" src="d0_s7" to="14" to_unit="um" />
        <character char_type="range_value" from="7" from_unit="um" name="width" src="d0_s7" to="12" to_unit="um" />
        <character is_modifier="false" modifier="evenly" name="size_or_width" src="d0_s7" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="densely" name="relief" src="d0_s7" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o8061" name="papilla" name_original="papillae" src="d0_s7" type="structure">
        <character is_modifier="false" name="density" src="d0_s7" value="dense" value_original="dense" />
        <character is_modifier="false" name="shape" src="d0_s7" value="2-fid" value_original="2-fid" />
        <character char_type="range_value" constraint="per lumen" constraintid="o8062" from="1" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <biological_entity id="o8062" name="lumen" name_original="lumen" src="d0_s7" type="structure" />
      <biological_entity constraint="basal" id="o8064" name="margin" name_original="margin" src="d0_s7" type="structure" />
      <biological_entity id="o8065" name="border" name_original="border" src="d0_s7" type="structure" />
      <biological_entity id="o8066" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character is_modifier="true" name="shape" src="d0_s7" value="oblong-linear" value_original="oblong-linear" />
        <character is_modifier="true" name="size" src="d0_s7" value="large" value_original="large" />
        <character is_modifier="true" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="thin-walled" value_original="thin-walled" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <relation from="o8058" id="r1932" name="as thick as" negation="false" src="d0_s7" to="o8059" />
      <relation from="o8063" id="r1933" name="part_of" negation="false" src="d0_s7" to="o8064" />
      <relation from="o8063" id="r1934" name="forming a" negation="false" src="d0_s7" to="o8065" />
      <relation from="o8063" id="r1935" name="part_of" negation="false" src="d0_s7" to="o8066" />
    </statement>
    <statement id="d0_s8">
      <text>Specialized asexual reproduction none.</text>
    </statement>
    <statement id="d0_s9">
      <text>Sexual condition dioicous;</text>
      <biological_entity id="o8063" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="development" src="d0_s8" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="quantity" src="d0_s8" value="0" value_original="none" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perichaetia and perigonia lateral, at the apex of small branches;</text>
      <biological_entity id="o8067" name="perichaetium" name_original="perichaetia" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="lateral" value_original="lateral" />
      </biological_entity>
      <biological_entity id="o8068" name="perigonium" name_original="perigonia" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="lateral" value_original="lateral" />
      </biological_entity>
      <biological_entity id="o8069" name="apex" name_original="apex" src="d0_s10" type="structure" />
      <biological_entity id="o8070" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="true" name="size" src="d0_s10" value="small" value_original="small" />
      </biological_entity>
      <relation from="o8067" id="r1936" name="at" negation="false" src="d0_s10" to="o8069" />
      <relation from="o8068" id="r1937" name="at" negation="false" src="d0_s10" to="o8069" />
      <relation from="o8069" id="r1938" name="part_of" negation="false" src="d0_s10" to="o8070" />
    </statement>
    <statement id="d0_s11">
      <text>perichaetial leaves similar to the cauline, somewhat narrower.</text>
    </statement>
    <statement id="d0_s12">
      <text>[Seta to 1.7 cm. Capsule stegocarpous, theca erect, cylindric; annulus persistent; operculum bluntly high-conic or conic-rostrate; peristome divided to the base into 32 brownish orange, branched-spiculose, filiform divisions, loosely twisted counterclockwise in 1 turn. Calyptra cucullate. Spores spherical, moderately papillose, 10–13 µm.] KOH reaction pale lemon-yellow.</text>
      <biological_entity constraint="perichaetial" id="o8071" name="leaf" name_original="leaves" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="somewhat" name="width" src="d0_s11" value="narrower" value_original="narrower" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pale lemon-yellow" value_original="pale lemon-yellow" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Temperate North America, Mexico, West Indies, Central America, South America, Europe, n, c Africa, Atlantic Islands (Azores, Canary Islands), Asia (including the Middle East).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Temperate North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="n" establishment_means="native" />
        <character name="distribution" value="c Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Azores)" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Canary Islands)" establishment_means="native" />
        <character name="distribution" value="Asia (including the Middle East)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <discussion>Species 2 (1 in the flora).</discussion>
  <discussion>A review of specimens of this genus worldwide, including the type specimens and protologues of Pleurochaete malacophylla (Müller Hal.) Brotherus and P. ecuadorensis Brotherus, and numerous specimens of P. beccarii Venturi, indicates that the genus comprises a species complex, P. squarrosa (Bridel) Lindberg in the loose sense. However, in the New World there is a facies, P. luteola, that is variable around a norm displaying a hyaline marginal band reaching farther in the proximal part of the leaf (usually reaching 1/2–2/3 leaf length), with corresponding stronger serrations in the distal part of the band. No other difference, including the habit of the leaves on the stem in the dry state or size of stem central strand, could be identified apart from basal cell length being more extreme in well-developed P. luteola. Variation fluctuates above and below the average of 1/2 of the leaf length for marginal band length. In the Old World, these characters are reduced: the norm is a shorter marginal band relative to the leaf length, just short of 1/2 the leaf length in its best expression (it is usually 1/8 to 1/6). There is generally little or usually no dentition at the distal terminus of the hyaline band. Small or depauperate plants display extreme reduction as they do in the New World, but in P. squarrosa the hyaline band is nearly absent. R. H. Zander (1993) treated P. luteola as a variety of P. squarrosa, but due to the size of geographic distribution separating these two facies, P. luteola is here retained at the species level and P. squarrosa is excluded from the New World. Specimens from North and South America with relatively shorter marginal bands, broader and shorter leaves and other variation may be interpreted as simply the low end of New World variation of P. luteola. Even in plants with small leaves, the marginal band extends higher up the leaf, at least in some leaves, in specimens from the New World than specimens with small leaves from the Old World. There are no robust specimens of P. squarrosa in the strict sense in the New World.</discussion>
  <discussion>The genus is distinct from Tortella, which it closely resembles, by the hyaline basal cells not in a true V-shape, but present only on the basal margins as two clear membranaceous linear strips different from the colored, thick-walled basal cells interior to them, also by the absence of radicles cloaking the stem, the denticulate-serrulate distal leaf margins, and the axillary inflorescences. The genus resembles the Pottiaceous genera Molendoa and Anoectangium in the presence of lateral gametoecia. Pleurochaete does not have close morphological relationship with Pseudosymblepharis or the genera close to Tortella because of the lateral stalked perichaetia and perigonia as well as the restriction of the distinctive marginal border to the edge of the leaf, the relatively small, thick-walled proximal leaf cells gradually intergrading with the distal laminal cells. The marginal band of elongate, vesciculose cells also tends to become narrowed as it approaches the leaf insertion, sometimes becoming only one or two cells wide from a width of up to 10 cells.</discussion>
  
</bio:treatment>