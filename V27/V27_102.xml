<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">87</other_info_on_meta>
    <other_info_on_meta type="mention_page">89</other_info_on_meta>
    <other_info_on_meta type="mention_page">90</other_info_on_meta>
    <other_info_on_meta type="mention_page">91</other_info_on_meta>
    <other_info_on_meta type="mention_page">93</other_info_on_meta>
    <other_info_on_meta type="mention_page">95</other_info_on_meta>
    <other_info_on_meta type="mention_page">99</other_info_on_meta>
    <other_info_on_meta type="treatment_page">98</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">sphagnaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sphagnum</taxon_name>
    <taxon_name authority="Wilson" date="1855" rank="section">acutifolia</taxon_name>
    <taxon_name authority="Sjörs" date="1944" rank="species">subfulvum</taxon_name>
    <place_of_publication>
      <publication_title>Svensk Bot. Tidskr.</publication_title>
      <place_in_publication>38: 404. 1944,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sphagnaceae;genus sphagnum;section acutifolia;species subfulvum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443332</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sphagnum</taxon_name>
    <taxon_name authority="Warnstorf" date="unknown" rank="species">nitidum</taxon_name>
    <place_of_publication>
      <publication_title>Allg. Bot. Z. Syst.</publication_title>
      <place_in_publication>1: 94. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sphagnum;species nitidum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants moderate-sized to robust, usually soft and lax, sometimes moderately stiff, capitulum typically enlarged and flattopped, ± stellate;</text>
      <biological_entity id="o8584" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized" value_original="moderate-sized" />
        <character is_modifier="false" name="fragility" src="d0_s0" value="robust" value_original="robust" />
        <character is_modifier="false" modifier="usually" name="pubescence_or_texture" src="d0_s0" value="soft" value_original="soft" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s0" value="lax" value_original="lax" />
        <character is_modifier="false" modifier="sometimes moderately" name="fragility" src="d0_s0" value="stiff" value_original="stiff" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>green to golden brown, unshaded plants often reddish purple, plants with metallic sheen when dry.</text>
      <biological_entity id="o8585" name="capitulum" name_original="capitulum" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="typically" name="size" src="d0_s0" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" modifier="more or less" name="arrangement_or_shape" src="d0_s0" value="stellate" value_original="stellate" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s1" to="golden brown" />
      </biological_entity>
      <biological_entity id="o8586" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="true" name="habitat" src="d0_s1" value="unshaded" value_original="unshaded" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish purple" value_original="reddish purple" />
        <character name="growth_form" value="plant" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems yellowish to dark-brown;</text>
      <biological_entity id="o8588" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s2" to="dark-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>superficial cortical cells aporose.</text>
      <biological_entity constraint="superficial cortical" id="o8589" name="bud" name_original="cells" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Stem-leaves triangular-lingulate to broadly lingulate, 0.9–1.3 mm, apex broadly rounded to obtusely angled, border very strong and broad at base (more than 0.4 width);</text>
      <biological_entity id="o8590" name="leaf-stem" name_original="stem-leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="triangular-lingulate" name="shape" src="d0_s4" to="broadly lingulate" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="distance" src="d0_s4" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8591" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly rounded" name="shape" src="d0_s4" to="obtusely angled" />
      </biological_entity>
      <biological_entity id="o8592" name="border" name_original="border" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="very" name="fragility" src="d0_s4" value="strong" value_original="strong" />
        <character constraint="at base" constraintid="o8593" is_modifier="false" name="width" src="d0_s4" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o8593" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>hyaline cells rhombic, efibrillose, most 0–1-septate.</text>
      <biological_entity id="o8594" name="bud" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="efibrillose" value_original="efibrillose" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="0-1-septate" value_original="0-1-septate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Branches long, tapering, imbricate, not 5-ranked.</text>
      <biological_entity id="o8595" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s6" value="long" value_original="long" />
        <character is_modifier="false" name="shape" src="d0_s6" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s6" value="5-ranked" value_original="5-ranked" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Branch fascicles with 2 spreading and 1–2 pendent branches.</text>
      <biological_entity id="o8596" name="branch" name_original="branch" src="d0_s7" type="structure">
        <character constraint="with branches" constraintid="o8597" is_modifier="false" name="arrangement" src="d0_s7" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o8597" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="true" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="2" />
        <character is_modifier="true" name="orientation" src="d0_s7" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Branch leaves ovate, 2–2.5 mm, concave, straight, apex involute;</text>
      <biological_entity constraint="branch" id="o8598" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="concave" value_original="concave" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o8599" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s8" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hyaline cells on convex surface with elliptic pores along the commissures grading from moderate-sized pores near leaf apex to large pores at the base, concave surface with large round pores in proximal portions of leaf.</text>
      <biological_entity id="o8600" name="bud" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o8601" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o8602" name="pore" name_original="pores" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s9" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o8603" name="commissure" name_original="commissures" src="d0_s9" type="structure" />
      <biological_entity id="o8604" name="pore" name_original="pores" src="d0_s9" type="structure">
        <character is_modifier="true" name="size" src="d0_s9" value="medium-sized" value_original="moderate-sized" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o8605" name="apex" name_original="apex" src="d0_s9" type="structure" />
      <biological_entity id="o8606" name="pore" name_original="pores" src="d0_s9" type="structure">
        <character is_modifier="true" name="size" src="d0_s9" value="large" value_original="large" />
      </biological_entity>
      <biological_entity id="o8607" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o8609" name="pore" name_original="pores" src="d0_s9" type="structure">
        <character is_modifier="true" name="size" src="d0_s9" value="large" value_original="large" />
        <character is_modifier="true" name="shape" src="d0_s9" value="round" value_original="round" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o8610" name="portion" name_original="portions" src="d0_s9" type="structure" />
      <biological_entity id="o8611" name="leaf" name_original="leaf" src="d0_s9" type="structure" />
      <relation from="o8600" id="r2377" name="on" negation="false" src="d0_s9" to="o8601" />
      <relation from="o8601" id="r2378" name="with" negation="false" src="d0_s9" to="o8602" />
      <relation from="o8602" id="r2379" name="along" negation="false" src="d0_s9" to="o8603" />
      <relation from="o8603" id="r2380" name="from" negation="false" src="d0_s9" to="o8604" />
      <relation from="o8604" id="r2381" name="near" negation="false" src="d0_s9" to="o8605" />
      <relation from="o8605" id="r2382" name="to" negation="false" src="d0_s9" to="o8606" />
      <relation from="o8606" id="r2383" name="at" negation="false" src="d0_s9" to="o8607" />
      <relation from="o8608" id="r2384" name="with" negation="false" src="d0_s9" to="o8609" />
      <relation from="o8609" id="r2385" name="in" negation="false" src="d0_s9" to="o8610" />
      <relation from="o8610" id="r2386" name="part_of" negation="false" src="d0_s9" to="o8611" />
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition monoicous.</text>
      <biological_entity id="o8608" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="concave" value_original="concave" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="monoicous" value_original="monoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spores 23–31 µm, irregularly coarsely papillose on both surfaces;</text>
      <biological_entity id="o8612" name="spore" name_original="spores" src="d0_s11" type="structure">
        <character char_type="range_value" from="23" from_unit="um" name="some_measurement" src="d0_s11" to="31" to_unit="um" />
        <character constraint="on surfaces" constraintid="o8613" is_modifier="false" modifier="irregularly coarsely" name="relief" src="d0_s11" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o8613" name="surface" name_original="surfaces" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>proximal laesura less than or equal to 0.5 spore radius</text>
      <biological_entity constraint="proximal" id="o8614" name="laesurum" name_original="laesura" src="d0_s12" type="structure">
        <character is_modifier="false" name="variability" src="d0_s12" value="equal" value_original="equal" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s12" to="0.5" />
      </biological_entity>
      <biological_entity id="o8615" name="spore" name_original="spore" src="d0_s12" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="early summer" from="early summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Minerotrophic and hygrophytic, forming hummocks in shrubby and wooded medium and rich fens</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="forming hummocks" modifier="minerotrophic and hygrophytic" constraint="in shrubby and wooded medium and rich fens" />
        <character name="habitat" value="shrubby" />
        <character name="habitat" value="wooded medium" />
        <character name="habitat" value="rich fens" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; B.C., Nfld. and Labr. (Nfld.), Ont., Que., Yukon; Alaska, Maine, Mich., Minn., N.H., N.J., N.Y., Vt.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>83.</number>
  <discussion>Sporophytes of Sphagnum subfulvum are common. This species is associated with S. centrale, S. contortum, S. teres, and S. warnstorfii. Although it is normally more minerotrophic, S. subfulvum does occasionally (in Newfoundland) occur in the same mires as S. flavicomans. The latter lacks the metallic sheen of S. subfulvum and its stem leaves are not as narrow and acute. In some forms S. subfulvum may develop a purplish gloss that may lead to confusion with S. subnitens but the color of that species has a definite red component and its stem leaves are narrower and more sharply pointed than those of S. subfulvum.</discussion>
  
</bio:treatment>