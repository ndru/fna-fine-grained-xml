<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>William A. Weber</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">359</other_info_on_meta>
    <other_info_on_meta type="mention_page">381</other_info_on_meta>
    <other_info_on_meta type="treatment_page">425</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="Bridel" date="1826" rank="genus">OREAS</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Univ.</publication_title>
      <place_in_publication>1: 380. 1826  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus OREAS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek Oread, mythological nymph of hills and mountains, alluding to alpine habitat</other_info_on_name>
    <other_info_on_name type="fna_id">123084</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in dense cushions, yellow-green distally, brown and compacted with redbrown radicles proximally.</text>
      <biological_entity id="o189" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="distally" name="coloration" notes="" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o190" name="cushion" name_original="cushions" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o191" name="radicle" name_original="radicles" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="redbrown" value_original="redbrown" />
      </biological_entity>
      <relation from="o189" id="r41" name="in" negation="false" src="d0_s0" to="o190" />
      <relation from="o189" id="r42" modifier="proximally" name="compacted with" negation="false" src="d0_s0" to="o191" />
    </statement>
    <statement id="d0_s1">
      <text>Stems forked.</text>
      <biological_entity id="o192" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="forked" value_original="forked" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect-spreading, crisped when dry, lance-acuminate, keeled, ending in a sharp, hyaline cell or short point;</text>
      <biological_entity id="o193" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s2" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lance-acuminate" value_original="lance-acuminate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o194" name="cell" name_original="cell" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="sharp" value_original="sharp" />
        <character is_modifier="true" name="coloration" src="d0_s2" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o195" name="point" name_original="point" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="sharp" value_original="sharp" />
        <character is_modifier="true" name="coloration" src="d0_s2" value="hyaline" value_original="hyaline" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
      </biological_entity>
      <relation from="o193" id="r43" name="ending in" negation="false" src="d0_s2" to="o194" />
      <relation from="o193" id="r44" name="ending in" negation="false" src="d0_s2" to="o195" />
    </statement>
    <statement id="d0_s3">
      <text>margins 2-stratose and narrowly revolute nearly to the apex, entire or somewhat irregular;</text>
      <biological_entity id="o196" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="2-stratose" value_original="2-stratose" />
        <character constraint="to apex" constraintid="o197" is_modifier="false" modifier="narrowly" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_course" src="d0_s3" value="irregular" value_original="irregular" />
      </biological_entity>
      <biological_entity id="o197" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>costa prominent at base, smooth, ending near the leaf apex, entire to shortly excurrent;</text>
      <biological_entity id="o198" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character constraint="at base" constraintid="o199" is_modifier="false" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s4" value="smooth" value_original="smooth" />
        <character char_type="range_value" from="entire" name="architecture" src="d0_s4" to="shortly excurrent" />
      </biological_entity>
      <biological_entity id="o199" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity constraint="leaf" id="o200" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <relation from="o198" id="r45" name="ending near the" negation="false" src="d0_s4" to="o200" />
    </statement>
    <statement id="d0_s5">
      <text>distal cells irregularly roundedquadrate, smooth, thick-walled;</text>
      <biological_entity constraint="distal" id="o201" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s5" value="roundedquadrate" value_original="roundedquadrate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal-cells narrowly rectangular, thick-walled, the alar cells not differentiated.</text>
      <biological_entity id="o202" name="basal-cell" name_original="basal-cells" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition autoicous;</text>
      <biological_entity id="o203" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s6" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perigonial buds minute, at base of perichaetium;</text>
      <biological_entity constraint="perigonial" id="o204" name="bud" name_original="buds" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o205" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity id="o206" name="perichaetium" name_original="perichaetium" src="d0_s8" type="structure" />
      <relation from="o204" id="r46" name="at" negation="false" src="d0_s8" to="o205" />
      <relation from="o205" id="r47" name="part_of" negation="false" src="d0_s8" to="o206" />
    </statement>
    <statement id="d0_s9">
      <text>perichaetial leaves similar to stem-leaves, ending in a short, hyaline awn.</text>
      <biological_entity constraint="perichaetial" id="o207" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <biological_entity id="o208" name="stem-leaf" name_original="stem-leaves" src="d0_s9" type="structure" />
      <biological_entity id="o209" name="awn" name_original="awn" src="d0_s9" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <relation from="o207" id="r48" name="to" negation="false" src="d0_s9" to="o208" />
      <relation from="o207" id="r49" name="ending in" negation="false" src="d0_s9" to="o209" />
    </statement>
    <statement id="d0_s10">
      <text>Seta straight or somewhat curved when dry, cygneous when moist, yellow.</text>
      <biological_entity id="o210" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="when dry" name="course" src="d0_s10" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="when moist" name="shape" src="d0_s10" value="cygneous" value_original="cygneous" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsule mostly erect when dry, pendent when moist, short-necked, symmetric, subglobose but becoming broader at the mouth and somewhat contracted at the middle when dry, orangebrown, strongly ribbed;</text>
      <biological_entity id="o211" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s11" value="pendent" value_original="pendent" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="short-necked" value_original="short-necked" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="shape" src="d0_s11" value="subglobose" value_original="subglobose" />
        <character constraint="at mouth" constraintid="o212" is_modifier="false" modifier="becoming" name="width" src="d0_s11" value="broader" value_original="broader" />
        <character constraint="at middle" constraintid="o213" is_modifier="false" modifier="somewhat" name="condition_or_size" notes="" src="d0_s11" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o212" name="mouth" name_original="mouth" src="d0_s11" type="structure" />
      <biological_entity id="o213" name="middle" name_original="middle" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="when dry" name="coloration" src="d0_s11" value="orangebrown" value_original="orangebrown" />
        <character is_modifier="false" modifier="strongly" name="architecture_or_shape" src="d0_s11" value="ribbed" value_original="ribbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>annulus persistent;</text>
      <biological_entity id="o214" name="annulus" name_original="annulus" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>operculum obliquely rostrate from a convex base;</text>
      <biological_entity id="o215" name="operculum" name_original="operculum" src="d0_s13" type="structure">
        <character constraint="from base" constraintid="o216" is_modifier="false" modifier="obliquely" name="shape" src="d0_s13" value="rostrate" value_original="rostrate" />
      </biological_entity>
      <biological_entity id="o216" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="convex" value_original="convex" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>peristome teeth inserted slightly below the mouth, lance-acuminate, brown at base, entire or occasionally somewhat perforated, rarely cleft, yellowish-brown above, pale at the slender tips, vertically striolate throughout.</text>
      <biological_entity constraint="peristome" id="o217" name="tooth" name_original="teeth" src="d0_s14" type="structure">
        <character constraint="slightly below mouth" constraintid="o218" is_modifier="false" name="position" src="d0_s14" value="inserted" value_original="inserted" />
        <character is_modifier="false" name="shape" notes="" src="d0_s14" value="lance-acuminate" value_original="lance-acuminate" />
        <character constraint="at base" constraintid="o219" is_modifier="false" name="coloration" src="d0_s14" value="brown" value_original="brown" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s14" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="occasionally somewhat" name="architecture" src="d0_s14" value="perforated" value_original="perforated" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s14" value="cleft" value_original="cleft" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellowish-brown" value_original="yellowish-brown" />
        <character constraint="at slender tips" constraintid="o220" is_modifier="false" name="coloration" src="d0_s14" value="pale" value_original="pale" />
        <character is_modifier="false" modifier="vertically; throughout" name="relief" notes="" src="d0_s14" value="striolate" value_original="striolate" />
      </biological_entity>
      <biological_entity id="o218" name="mouth" name_original="mouth" src="d0_s14" type="structure" />
      <biological_entity id="o219" name="base" name_original="base" src="d0_s14" type="structure" />
      <biological_entity constraint="slender" id="o220" name="tip" name_original="tips" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Calyptra cucullate, smooth.</text>
      <biological_entity id="o221" name="calyptra" name_original="calyptra" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Spores warty-papillose, brown.</text>
      <biological_entity id="o222" name="spore" name_original="spores" src="d0_s16" type="structure">
        <character is_modifier="false" name="relief" src="d0_s16" value="warty-papillose" value_original="warty-papillose" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Europe, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <discussion>Species 1.</discussion>
  <discussion>Oreas much resembles Cynodontium, but has a cygneous seta (when moist) and symmetric capsules. European specimens are more slender, with shorter leaves, but the differences are not significant.</discussion>
  <references>
    <reference>Steere, W. C. 1958. The discovery of Oreas martiana in arctic Alaska, a genus new to North America. Bryologist 61: 119–124.</reference>
    <reference>Weber, W. A. 1960. A second American record for Oreas martiana, from Colorado. Bryologist 63: 241–244.</reference>
  </references>
  
</bio:treatment>