<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">639</other_info_on_meta>
    <other_info_on_meta type="treatment_page">638</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="Müller Hal." date="1847" rank="genus">acaulon</taxon_name>
    <taxon_name authority="(Spruce) Müller Hal." date="1847" rank="species">triquetrum</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Zeitung (Berlin)</publication_title>
      <place_in_publication>5: 100. 1847,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus acaulon;species triquetrum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443890</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Phascum</taxon_name>
    <taxon_name authority="Spruce" date="unknown" rank="species">triquetrum</taxon_name>
    <place_of_publication>
      <publication_title>London J. Bot.</publication_title>
      <place_in_publication>4: 189. 1845</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Phascum;species triquetrum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants three-angled, about 1 mm.</text>
      <biological_entity id="o1902" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="3-angled" value_original="3-angled" />
        <character name="some_measurement" src="d0_s0" unit="mm" value="1" value_original="1" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stem-leaves cuspidate, keeled;</text>
      <biological_entity id="o1903" name="stem-leaf" name_original="stem-leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="cuspidate" value_original="cuspidate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>laminal cells, 13–18 µm wide, 1–3: 1, smooth.</text>
      <biological_entity constraint="laminal" id="o1904" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character char_type="range_value" from="13" from_unit="um" name="width" src="d0_s2" to="18" to_unit="um" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="3" />
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Seta as long as the diameter of the capsule.</text>
      <biological_entity id="o1905" name="seta" name_original="seta" src="d0_s3" type="structure" />
      <biological_entity id="o1906" name="capsule" name_original="capsule" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Spores 25–30 (–40) µm, finely papillose.</text>
      <biological_entity id="o1907" name="spore" name_original="spores" src="d0_s4" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s4" to="40" to_unit="um" />
        <character char_type="range_value" from="25" from_unit="um" name="some_measurement" src="d0_s4" to="30" to_unit="um" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s4" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature late autumn-spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="spring" from="late autumn" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil, sand, clay, old fields, pastures, roadside banks, temporarily moist areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" />
        <character name="habitat" value="sand" />
        <character name="habitat" value="clay" />
        <character name="habitat" value="old fields" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="roadside banks" />
        <character name="habitat" value="moist areas" modifier="temporarily" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont., Sask.; Calif., Ill., Iowa, Mass., N.J., Tex., Va., W.Va.; Europe; Asia; n Africa; Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Acaulon muticum var. rufescens has reflexed apices while A. triquetrum does not always have these, though illustrated as such by H. A. Crum and L. E. Anderson (1981). The exsiccat C. F. Austin, Musci Appalachiani 52 has a short seta and the plant is 1.2 mm, and is placed here with A. muticum var. rufescens.</discussion>
  
</bio:treatment>