<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">173</other_info_on_meta>
    <other_info_on_meta type="mention_page">178</other_info_on_meta>
    <other_info_on_meta type="treatment_page">177</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">encalyptaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">encalypta</taxon_name>
    <taxon_name authority="Smith in J. E. Smith et al." date="1805" rank="species">alpina</taxon_name>
    <place_of_publication>
      <publication_title>in J. E. Smith et al., Engl. Bot.</publication_title>
      <place_in_publication>20: 1419. 1805,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family encalyptaceae;genus encalypta;species alpina</taxon_hierarchy>
    <other_info_on_name type="fna_id">200001089</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 20–50 mm, central strand weak, small when present, cells very thin-walled.</text>
      <biological_entity id="o1245" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s0" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="central" id="o1246" name="strand" name_original="strand" src="d0_s0" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s0" value="weak" value_original="weak" />
        <character is_modifier="false" modifier="when present" name="size" src="d0_s0" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o1247" name="bud" name_original="cells" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="very" name="architecture" src="d0_s0" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves oblong-ovate to lanceolate, 2–4 mm;</text>
      <biological_entity id="o1248" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="oblong-ovate" name="shape" src="d0_s1" to="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s1" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>apices acute to acuminate, mucronate to apiculate;</text>
      <biological_entity id="o1249" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="acuminate mucronate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="acuminate mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins plane;</text>
      <biological_entity id="o1250" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>costa percurrent to excurrent, narrow, papillose;</text>
      <biological_entity id="o1251" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character char_type="range_value" from="percurrent" name="architecture" src="d0_s4" to="excurrent" />
        <character is_modifier="false" name="size_or_width" src="d0_s4" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="relief" src="d0_s4" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>laminal cells 8–16 µm; basal-cells long-rectangular, smooth;</text>
      <biological_entity constraint="laminal" id="o1252" name="bud" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="um" name="some_measurement" src="d0_s5" to="16" to_unit="um" />
      </biological_entity>
      <biological_entity id="o1253" name="basal-bud" name_original="basal-cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="long-rectangular" value_original="long-rectangular" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal marginal cells weakly differentiated, longer than laminal cells forming border in leaf base.</text>
      <biological_entity constraint="laminal" id="o1255" name="bud" name_original="cells" src="d0_s6" type="structure" />
      <biological_entity id="o1256" name="border" name_original="border" src="d0_s6" type="structure" />
      <biological_entity constraint="leaf" id="o1257" name="base" name_original="base" src="d0_s6" type="structure" />
      <relation from="o1255" id="r346" name="forming" negation="false" src="d0_s6" to="o1256" />
      <relation from="o1254" id="r347" name="in" negation="false" src="d0_s6" to="o1257" />
    </statement>
    <statement id="d0_s7">
      <text>Specialized asexual reproduction absent.</text>
      <biological_entity constraint="basal marginal" id="o1254" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="weakly" name="variability" src="d0_s6" value="differentiated" value_original="differentiated" />
        <character constraint="than laminal cells" constraintid="o1255" is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
        <character is_modifier="false" name="development" src="d0_s7" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seta 6–12 mm, brownish red to black.</text>
      <biological_entity id="o1258" name="seta" name_original="seta" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
        <character char_type="range_value" from="brownish red" name="coloration" src="d0_s8" to="black" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule cylindric, 1–3 mm, ribbed, gymnostomous, brown;</text>
      <biological_entity id="o1259" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>exothecial cells rectangular, walls thickened;</text>
      <biological_entity constraint="exothecial" id="o1260" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rectangular" value_original="rectangular" />
      </biological_entity>
      <biological_entity id="o1261" name="wall" name_original="walls" src="d0_s10" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s10" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>peristome absent;</text>
      <biological_entity id="o1262" name="peristome" name_original="peristome" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>operculum 1.5 mm.</text>
      <biological_entity id="o1263" name="operculum" name_original="operculum" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Calyptra 3–6 mm, lacerate at base, smooth.</text>
      <biological_entity id="o1264" name="calyptra" name_original="calyptra" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
        <character constraint="at base" constraintid="o1265" is_modifier="false" name="shape" src="d0_s13" value="lacerate" value_original="lacerate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s13" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o1265" name="base" name_original="base" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Spores 30–36 µm, irregular granulate, brown.</text>
      <biological_entity id="o1266" name="spore" name_original="spores" src="d0_s14" type="structure">
        <character char_type="range_value" from="30" from_unit="um" name="some_measurement" src="d0_s14" to="36" to_unit="um" />
        <character is_modifier="false" name="architecture_or_course" src="d0_s14" value="irregular" value_original="irregular" />
        <character is_modifier="false" name="texture" src="d0_s14" value="granulate" value_original="granulate" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mesic sites, soil and rock around waterfalls and seeps in mountain and alpine habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mesic sites" />
        <character name="habitat" value="soil" constraint="around waterfalls and seeps" />
        <character name="habitat" value="rock" constraint="around waterfalls and seeps" />
        <character name="habitat" value="waterfalls" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="mountain" />
        <character name="habitat" value="alpine habitats" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., Nfld. and Labr. (Labr.), N.W.T., Nunavut, Que., Yukon; Alaska, Ariz., Colo., Mont., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <discussion>Encalyta alpina is most distinct in its lanceolate leaves, apiculate leaf apices, and lacerate base on the calyptrae. It might be confused with E. rhaptocarpa, but that species has a longer awn, ribbed capsule, and peristome.</discussion>
  
</bio:treatment>