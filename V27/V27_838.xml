<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Patricia M. Eckel</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">478</other_info_on_meta>
    <other_info_on_meta type="mention_page">513</other_info_on_meta>
    <other_info_on_meta type="mention_page">582</other_info_on_meta>
    <other_info_on_meta type="mention_page">583</other_info_on_meta>
    <other_info_on_meta type="mention_page">585</other_info_on_meta>
    <other_info_on_meta type="mention_page">642</other_info_on_meta>
    <other_info_on_meta type="treatment_page">584</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="(Herzog) Hilpert" date="1933" rank="subfamily">barbuloideae</taxon_name>
    <taxon_name authority="Bridel" date="1827" rank="genus">HYOPHILA</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Univ.</publication_title>
      <place_in_publication>1: 760. 1827,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily barbuloideae;genus HYOPHILA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek hyo, rain, and philia, fondness, alluding to wet habitats</other_info_on_name>
    <other_info_on_name type="fna_id">116155</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Bridel" date="unknown" rank="genus">Rottleria</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Univ.</publication_title>
      <place_in_publication>1: 105. 1826,</place_in_publication>
      <other_info_on_pub>not Willdenow 1797</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Rottleria;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants turf-forming, sometimes loosely cespitose, dull, green distally, red to reddish-brown or dark green proximally.</text>
      <biological_entity id="o6391" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="sometimes loosely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character char_type="range_value" from="red" modifier="proximally" name="coloration" src="d0_s0" to="reddish-brown or dark green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stem erect, rarely branched, to 1 cm, hyalodermis absent or weakly present and thin-walled, sclerodermis usually present, central cylinder of thick-walled cells, central strand usually strong;</text>
      <biological_entity id="o6392" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6393" name="hyalodermi" name_original="hyalodermis" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="weakly" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <biological_entity id="o6394" name="scleroderm" name_original="sclerodermis" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="central" id="o6395" name="cylinder" name_original="cylinder" src="d0_s1" type="structure" />
      <biological_entity id="o6396" name="cell" name_original="cells" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
      <relation from="o6395" id="r1508" name="part_of" negation="false" src="d0_s1" to="o6396" />
    </statement>
    <statement id="d0_s2">
      <text>radiculose;</text>
      <biological_entity constraint="central" id="o6397" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="fragility" src="d0_s1" value="strong" value_original="strong" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>axillary hairs 6–10 cells long, hyaline.</text>
      <biological_entity constraint="axillary" id="o6398" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s3" to="10" />
      </biological_entity>
      <biological_entity id="o6399" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves tubulose-twisted, incurved, sometimes contorted when dry, spreading when moist, commonly spathulate or ligulate, ovate, oblongelliptic, usually constricted at the base;</text>
      <biological_entity id="o6400" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="tubulose-twisted" value_original="tubulose-twisted" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="incurved" value_original="incurved" />
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s4" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="commonly" name="shape" src="d0_s4" value="spathulate" value_original="spathulate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="ligulate" value_original="ligulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblongelliptic" value_original="oblongelliptic" />
        <character constraint="at base" constraintid="o6401" is_modifier="false" modifier="usually" name="size" src="d0_s4" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o6401" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>distal lamina broadly channeled, occasionally concave, shallowly grooved along the costa;</text>
      <biological_entity constraint="distal" id="o6402" name="lamina" name_original="lamina" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="channeled" value_original="channeled" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s5" value="concave" value_original="concave" />
        <character constraint="along costa" constraintid="o6403" is_modifier="false" modifier="shallowly" name="architecture" src="d0_s5" value="grooved" value_original="grooved" />
      </biological_entity>
      <biological_entity id="o6403" name="costa" name_original="costa" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>base not different in shape;</text>
      <biological_entity id="o6404" name="base" name_original="base" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>margins plane to broadly incurved, sometimes narrowly recurved in proximal 2/3, entire or denticulate to dentate in distal 1/4 or at the apex;</text>
      <biological_entity id="o6405" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="broadly" name="orientation" src="d0_s7" value="incurved" value_original="incurved" />
        <character constraint="in proximal 2/3" constraintid="o6406" is_modifier="false" modifier="sometimes narrowly" name="orientation" src="d0_s7" value="recurved" value_original="recurved" />
        <character char_type="range_value" constraint="in " constraintid="o6408" from="denticulate" name="shape" notes="" src="d0_s7" to="dentate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6406" name="2/3" name_original="2/3" src="d0_s7" type="structure" />
      <biological_entity constraint="distal" id="o6407" name="1/4" name_original="1/4" src="d0_s7" type="structure" />
      <biological_entity id="o6408" name="apex" name_original="apex" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>apex broadly acute to rounded, rarely cucullate or emarginate;</text>
      <biological_entity id="o6409" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="broadly acute" name="shape" src="d0_s8" to="rounded rarely cucullate or emarginate" />
        <character char_type="range_value" from="broadly acute" name="shape" src="d0_s8" to="rounded rarely cucullate or emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>costa subpercurrent or percurrent, ending in an apiculus or mucro, adaxial surface cells quadrate to shortrectangular, adaxial and abaxial epidermis present, stereid bands 2, guide cells 4 (–6) in one layer;</text>
      <biological_entity id="o6410" name="costa" name_original="costa" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="subpercurrent" value_original="subpercurrent" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="percurrent" value_original="percurrent" />
      </biological_entity>
      <biological_entity id="o6411" name="apiculu" name_original="apiculus" src="d0_s9" type="structure" />
      <biological_entity id="o6412" name="mucro" name_original="mucro" src="d0_s9" type="structure" />
      <biological_entity constraint="surface" id="o6413" name="cell" name_original="cells" src="d0_s9" type="structure" constraint_original="adaxial surface" />
      <biological_entity constraint="adaxial adaxial surface and abaxial" id="o6414" name="epidermis" name_original="epidermis" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="quadrate" value_original="quadrate" />
        <character is_modifier="true" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o6415" name="band" name_original="bands" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="guide" id="o6416" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="6" />
        <character constraint="in layer" constraintid="o6417" name="quantity" src="d0_s9" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o6417" name="layer" name_original="layer" src="d0_s9" type="structure" />
      <relation from="o6410" id="r1509" name="ending in an" negation="false" src="d0_s9" to="o6411" />
      <relation from="o6410" id="r1510" name="ending in an" negation="false" src="d0_s9" to="o6412" />
      <relation from="o6410" id="r1511" name="ending in an" negation="false" src="d0_s9" to="o6413" />
      <relation from="o6410" id="r1512" name="ending in an" negation="false" src="d0_s9" to="o6414" />
    </statement>
    <statement id="d0_s10">
      <text>hydroid strand sometimes present;</text>
      <biological_entity constraint="hydroid" id="o6418" name="strand" name_original="strand" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>basal-cells differentiated across the leaf to only in the median basal region, usually only in a small area near the insertion;</text>
      <biological_entity id="o6419" name="basal-cell" name_original="basal-cells" src="d0_s11" type="structure">
        <character constraint="across leaf" constraintid="o6420" is_modifier="false" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o6420" name="leaf" name_original="leaf" src="d0_s11" type="structure" />
      <biological_entity constraint="median basal" id="o6421" name="region" name_original="region" src="d0_s11" type="structure" />
      <biological_entity id="o6422" name="area" name_original="area" src="d0_s11" type="structure">
        <character is_modifier="true" name="size" src="d0_s11" value="small" value_original="small" />
      </biological_entity>
      <relation from="o6420" id="r1513" modifier="only" name="in" negation="false" src="d0_s11" to="o6421" />
      <relation from="o6419" id="r1514" modifier="usually only; only" name="in" negation="false" src="d0_s11" to="o6422" />
    </statement>
    <statement id="d0_s12">
      <text>distal cells roundedquadrate to hexagonal, small, walls evenly thickened, bulging equally on both sides or bulging adaxially and plane abaxially, papillae absent or simple.</text>
      <biological_entity constraint="distal" id="o6423" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character char_type="range_value" from="roundedquadrate" name="shape" src="d0_s12" to="hexagonal" />
        <character is_modifier="false" name="size" src="d0_s12" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o6425" name="side" name_original="sides" src="d0_s12" type="structure" />
      <biological_entity id="o6426" name="side" name_original="sides" src="d0_s12" type="structure" />
      <biological_entity id="o6427" name="papilla" name_original="papillae" src="d0_s12" type="structure">
        <character is_modifier="true" name="pubescence_or_shape" src="d0_s12" value="bulging" value_original="bulging" />
        <character is_modifier="true" name="shape" src="d0_s12" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o6428" name="papilla" name_original="papillae" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o6429" name="side" name_original="sides" src="d0_s12" type="structure" />
      <biological_entity id="o6430" name="papilla" name_original="papillae" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o6431" name="side" name_original="sides" src="d0_s12" type="structure" />
      <biological_entity id="o6432" name="papilla" name_original="papillae" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="plane" value_original="plane" />
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o6428" id="r1515" name="on" negation="false" src="d0_s12" to="o6429" />
      <relation from="o6428" id="r1516" modifier="on both sides or bulging and plane , papillae" name="on" negation="false" src="d0_s12" to="o6430" />
      <relation from="o6430" id="r1517" name="on" negation="false" src="d0_s12" to="o6431" />
      <relation from="o6430" id="r1518" modifier="on both sides or bulging and plane , papillae" name="on" negation="false" src="d0_s12" to="o6432" />
    </statement>
    <statement id="d0_s13">
      <text>Specialized asexual reproduction by axillary gemmae, these clavate, stellate or dentate-elliptic in the leaf-axils on densely-branched stalks.</text>
      <biological_entity constraint="axillary" id="o6433" name="gemma" name_original="gemmae" src="d0_s13" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s13" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="shape" src="d0_s13" value="clavate" value_original="clavate" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s13" value="stellate" value_original="stellate" />
        <character constraint="in leaf-axils" constraintid="o6434" is_modifier="false" name="arrangement_or_shape" src="d0_s13" value="dentate-elliptic" value_original="dentate-elliptic" />
      </biological_entity>
      <biological_entity id="o6434" name="leaf-axil" name_original="leaf-axils" src="d0_s13" type="structure" />
      <biological_entity id="o6435" name="stalk" name_original="stalks" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="densely-branched" value_original="densely-branched" />
      </biological_entity>
      <relation from="o6434" id="r1519" name="on" negation="false" src="d0_s13" to="o6435" />
    </statement>
    <statement id="d0_s14">
      <text>Sexual condition dioicous or monoicous;</text>
      <biological_entity id="o6424" name="wall" name_original="walls" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="evenly" name="size_or_width" src="d0_s12" value="thickened" value_original="thickened" />
        <character constraint="on " constraintid="o6428" is_modifier="false" name="pubescence_or_shape" src="d0_s12" value="bulging" value_original="bulging" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="simple" value_original="simple" />
        <character is_modifier="false" name="development" src="d0_s13" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="dioicous" value_original="dioicous" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="monoicous" value_original="monoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>perigonia terminal on perigoniate plants or as lateral buds on perichaetiate plants;</text>
      <biological_entity id="o6436" name="perigonium" name_original="perigonia" src="d0_s15" type="structure">
        <character constraint="on perigoniate plants or as lateral buds" constraintid="o6437" is_modifier="false" name="position_or_structure_subtype" src="d0_s15" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o6437" name="bud" name_original="buds" src="d0_s15" type="structure" />
      <biological_entity id="o6438" name="plant" name_original="plants" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="perichaetiate" value_original="perichaetiate" />
      </biological_entity>
      <relation from="o6437" id="r1520" name="on" negation="false" src="d0_s15" to="o6438" />
    </statement>
    <statement id="d0_s16">
      <text>perichaetia terminal, perichaetial leaves similar to or smaller than cauline leaves.</text>
      <biological_entity id="o6439" name="perichaetium" name_original="perichaetia" src="d0_s16" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s16" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity constraint="perichaetial" id="o6440" name="leaf" name_original="leaves" src="d0_s16" type="structure">
        <character constraint="than cauline leaves" constraintid="o6441" is_modifier="false" name="size" src="d0_s16" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o6441" name="leaf" name_original="leaves" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>Seta elongate.</text>
      <biological_entity id="o6442" name="seta" name_original="seta" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Capsule long-ovoid to cylindric, operculum conic to long-conic or rostrate, peristome teeth absent.</text>
      <biological_entity id="o6443" name="capsule" name_original="capsule" src="d0_s18" type="structure">
        <character char_type="range_value" from="long-ovoid" name="shape" src="d0_s18" to="cylindric" />
      </biological_entity>
      <biological_entity id="o6444" name="operculum" name_original="operculum" src="d0_s18" type="structure">
        <character char_type="range_value" from="conic" name="shape" src="d0_s18" to="long-conic or rostrate" />
      </biological_entity>
      <biological_entity constraint="peristome" id="o6445" name="tooth" name_original="teeth" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Calyptra cucullate.</text>
      <biological_entity id="o6446" name="calyptra" name_original="calyptra" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="cucullate" value_original="cucullate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Spores 7–10 µm, papillose.</text>
      <biological_entity id="o6447" name="spore" name_original="spores" src="d0_s20" type="structure">
        <character char_type="range_value" from="7" from_unit="um" name="some_measurement" src="d0_s20" to="10" to_unit="um" />
        <character is_modifier="false" name="relief" src="d0_s20" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide in temperate and tropical areas.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide in temperate and tropical areas" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25.</number>
  <discussion>Species 85 (1 in the flora).</discussion>
  <references>
    <reference>Andrews, S. and P. L. Redfearn Jr. 1965. Observations on the germination of the gemmae of Hyophila tortula (Schwaegr.) Hampe. Bryologist 68: 345–347.</reference>
    <reference>Britton, E. G. 1904. Hyophila—A new genus to the United States. Bryologist 7: 69–71.</reference>
    <reference>Sharp, A. J. 1955. Factors in the distribution of Hyophila tortula and an extension of its known range to include Michigan. Mitt. Thüring. Bot. Ges. 1: 222–224.</reference>
  </references>
  
</bio:treatment>