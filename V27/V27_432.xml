<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">315</other_info_on_meta>
    <other_info_on_meta type="treatment_page">317</other_info_on_meta>
    <other_info_on_meta type="illustration_page">316</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">archidiaceae</taxon_name>
    <taxon_name authority="Bridel" date="1827" rank="genus">archidium</taxon_name>
    <taxon_name authority="Austin" date="1877" rank="species">donnellii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>6: 190. 1877,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family archidiaceae;genus archidium;species donnellii</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443509</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Archidium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ohioense</taxon_name>
    <taxon_name authority="(Austin) Lesquereux &amp; James" date="unknown" rank="variety">donnellii</taxon_name>
    <taxon_hierarchy>genus Archidium;species ohioense;variety donnellii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2–10 mm, perennial, in dense turfs, yellow-green.</text>
      <biological_entity id="o5308" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s0" to="10" to_unit="mm" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o5309" name="turf" name_original="turfs" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o5308" id="r1248" name="in" negation="false" src="d0_s0" to="o5309" />
    </statement>
    <statement id="d0_s1">
      <text>Stems simple or usually branched by several sterile innovations, becoming prostrate with age.</text>
      <biological_entity id="o5310" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character constraint="by innovations" constraintid="o5311" is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character constraint="with age" constraintid="o5312" is_modifier="false" modifier="becoming" name="growth_form_or_orientation" notes="" src="d0_s1" value="prostrate" value_original="prostrate" />
      </biological_entity>
      <biological_entity id="o5311" name="innovation" name_original="innovations" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="several" value_original="several" />
        <character is_modifier="true" name="reproduction" src="d0_s1" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o5312" name="age" name_original="age" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves erect to erect-spreading, lanceolate to narrowly ovatelanceolate, acuminate, 1–2.5 mm;</text>
      <biological_entity id="o5313" name="leaf-stem" name_original="stem-leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="erect-spreading" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="narrowly ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="acuminate" value_original="acuminate" />
        <character char_type="range_value" from="1" from_unit="mm" name="distance" src="d0_s2" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>costa percurrent or short-excurrent, sometimes somewhat subulate;</text>
      <biological_entity id="o5314" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="short-excurrent" value_original="short-excurrent" />
        <character is_modifier="false" modifier="sometimes somewhat" name="shape" src="d0_s3" value="subulate" value_original="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>laminal margins recurved, smooth or finely serrulate distally;</text>
      <biological_entity constraint="laminal" id="o5315" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="finely; distally" name="architecture" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>median and distal laminal cells irregular in shape, mixed quadrate, trapezoidal, short-rectangular, short to long-rhomboidal, 14–75 × 8–13 µm, proximal cells quadrate to short-rectangular, similar to median and distal cells;</text>
      <biological_entity constraint="median and distal laminal" id="o5316" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="irregular" value_original="irregular" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="mixed" value_original="mixed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="trapezoidal" value_original="trapezoidal" />
        <character is_modifier="false" name="shape" src="d0_s5" value="short-rectangular" value_original="short-rectangular" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s5" value="long-rhomboidal" value_original="long-rhomboidal" />
        <character char_type="range_value" from="14" from_unit="um" name="length" src="d0_s5" to="75" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s5" to="13" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o5317" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s5" to="short-rectangular" />
      </biological_entity>
      <biological_entity constraint="median and distal" id="o5318" name="cell" name_original="cells" src="d0_s5" type="structure" />
      <relation from="o5317" id="r1249" name="to" negation="false" src="d0_s5" to="o5318" />
    </statement>
    <statement id="d0_s6">
      <text>leaves of innovations similar to stem-leaves, not much reduced proximally.</text>
      <biological_entity id="o5319" name="leaf" name_original="leaves" src="d0_s6" type="structure" constraint="innovation" constraint_original="innovation; innovation">
        <character is_modifier="false" modifier="not much; proximally" name="size" notes="" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o5320" name="innovation" name_original="innovations" src="d0_s6" type="structure" />
      <biological_entity id="o5321" name="stem-leaf" name_original="stem-leaves" src="d0_s6" type="structure" />
      <relation from="o5319" id="r1250" name="part_of" negation="false" src="d0_s6" to="o5320" />
      <relation from="o5319" id="r1251" name="to" negation="false" src="d0_s6" to="o5321" />
    </statement>
    <statement id="d0_s7">
      <text>Perichaetial leaves broadly ovatelanceolate, narrowly acuminate to subulate, sometimes flexuose to secund, same size as stem-leaves;</text>
      <biological_entity constraint="perichaetial" id="o5322" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="narrowly acuminate" name="shape" src="d0_s7" to="subulate" />
        <character is_modifier="false" modifier="sometimes" name="course" src="d0_s7" value="flexuose" value_original="flexuose" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="secund" value_original="secund" />
      </biological_entity>
      <biological_entity id="o5323" name="stem-leaf" name_original="stem-leaves" src="d0_s7" type="structure" />
      <relation from="o5322" id="r1252" name="as" negation="false" src="d0_s7" to="o5323" />
    </statement>
    <statement id="d0_s8">
      <text>costae short to long excurrent, hairpoint pellucid, sometimes finely spinose;</text>
      <biological_entity id="o5324" name="costa" name_original="costae" src="d0_s8" type="structure">
        <character is_modifier="false" name="size_or_length" src="d0_s8" value="short to long" value_original="short to long" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o5325" name="hairpoint" name_original="hairpoint" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="pellucid" value_original="pellucid" />
        <character is_modifier="false" modifier="sometimes finely" name="architecture_or_shape" src="d0_s8" value="spinose" value_original="spinose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>laminal margins recurved, smooth to finely serrulate;</text>
      <biological_entity constraint="laminal" id="o5326" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="smooth" name="architecture" src="d0_s9" to="finely serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>distal and median laminal cells similar to stem-leaves, proximal laminal cells lax, wider, rectangular, 2–4: 1, 35–115 × 12–25 µm, sometimes somewhat hyaline.</text>
      <biological_entity constraint="distal and median laminal" id="o5327" name="cell" name_original="cells" src="d0_s10" type="structure" />
      <biological_entity id="o5328" name="stem-leaf" name_original="stem-leaves" src="d0_s10" type="structure" />
      <biological_entity constraint="proximal laminal" id="o5329" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s10" value="lax" value_original="lax" />
        <character is_modifier="false" name="width" src="d0_s10" value="wider" value_original="wider" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rectangular" value_original="rectangular" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s10" to="4" />
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
        <character char_type="range_value" from="35" from_unit="um" name="length" src="d0_s10" to="115" to_unit="um" />
        <character char_type="range_value" from="12" from_unit="um" name="width" src="d0_s10" to="25" to_unit="um" />
        <character is_modifier="false" modifier="sometimes somewhat" name="coloration" src="d0_s10" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <relation from="o5327" id="r1253" name="to" negation="false" src="d0_s10" to="o5328" />
    </statement>
    <statement id="d0_s11">
      <text>Specialized asexual gemmae absent.</text>
    </statement>
    <statement id="d0_s12">
      <text>Sexual condition autoicous;</text>
      <biological_entity id="o5330" name="gemma" name_original="gemmae" src="d0_s11" type="structure">
        <character is_modifier="true" name="development" src="d0_s11" value="specialized" value_original="specialized" />
        <character is_modifier="true" name="reproduction" src="d0_s11" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>antheridia terminating axillary leaf buds.</text>
      <biological_entity id="o5331" name="antheridium" name_original="antheridia" src="d0_s13" type="structure" />
      <biological_entity constraint="leaf" id="o5332" name="bud" name_original="buds" src="d0_s13" type="structure" constraint_original="axillary leaf" />
      <relation from="o5331" id="r1254" name="terminating" negation="false" src="d0_s13" to="o5332" />
    </statement>
    <statement id="d0_s14">
      <text>Capsule terminal, 425–750 µm. Spores typically 28 (4–60) per capsule, angular to irregularly polyhedral, 120–230 µm, smooth or finely granulose, yellow to orange-red.</text>
      <biological_entity id="o5333" name="capsule" name_original="capsule" src="d0_s14" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s14" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="425" from_unit="um" name="some_measurement" src="d0_s14" to="750" to_unit="um" />
      </biological_entity>
      <biological_entity id="o5334" name="spore" name_original="spores" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="28" value_original="28" />
        <character char_type="range_value" constraint="per capsule" constraintid="o5335" from="4" name="atypical_quantity" src="d0_s14" to="60" />
        <character char_type="range_value" from="angular" name="shape" notes="" src="d0_s14" to="irregularly polyhedral" />
        <character char_type="range_value" from="120" from_unit="um" name="some_measurement" src="d0_s14" to="230" to_unit="um" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s14" value="granulose" value_original="granulose" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s14" to="orange-red" />
      </biological_entity>
      <biological_entity id="o5335" name="capsule" name_original="capsule" src="d0_s14" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature early spring (Mar), more rarely in autumn or winter (Oct–Dec).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="early spring" from="early spring" />
        <character name="capsules maturing time" char_type="atypical_range" to="Mar" from="early spring" />
        <character name="capsules maturing time" char_type="atypical_range" modifier="rarely" to="winter" from="autumn" />
        <character name="capsules maturing time" char_type="atypical_range" modifier="rarely" to="Dec" from="Oct" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Uncommon on moist to dry soil along roadsides, in fields, rarely on rock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" modifier="uncommon on" constraint="along roadsides" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="rock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-250 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="250" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ill., Md., Okla., N.C., S.C., Tex., Va.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Archidium donnellii usually can be distinguished from other species by irregular areolation in median and distal portions of lamina, proximal lamina cells not strongly differentiated, and sexuality autoicous. It tends to grow in drier habitats than do other species of the genus.</discussion>
  
</bio:treatment>