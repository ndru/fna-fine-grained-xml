<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">472</other_info_on_meta>
    <other_info_on_meta type="treatment_page">471</other_info_on_meta>
    <other_info_on_meta type="illustration_page">469</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Müller Hal." date="unknown" rank="family">erpodiaceae</taxon_name>
    <taxon_name authority="(Bridel) Bridel in H. G. L. Reichenbach" date="1828" rank="genus">erpodium</taxon_name>
    <taxon_name authority="Pursell" date="1966" rank="species">acrifolium</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>69: 465. 1966,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family erpodiaceae;genus erpodium;species acrifolium</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443787</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants shiny, dark green, irregularly branched, branches ± erect, in loose mats.</text>
      <biological_entity id="o7059" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s0" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="dark green" value_original="dark green" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7060" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s0" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o7061" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o7060" id="r1685" name="in" negation="false" src="d0_s0" to="o7061" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves imbricate when dry, widely spreading when wet, not complanate, symmetric to ± asymmetric, 0.6–1.1 mm, ovate to oblong-lanceolate, subulate, subula hyaline, smooth to serrulate;</text>
      <biological_entity id="o7062" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when dry" name="arrangement" src="d0_s1" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="when wet" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s1" value="complanate" value_original="complanate" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s1" value="symmetric to more" value_original="symmetric to more" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s1" value="asymmetric" value_original="asymmetric" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s1" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s1" to="oblong-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="subulate" value_original="subulate" />
      </biological_entity>
      <biological_entity id="o7063" name="subulum" name_original="subula" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="smooth" name="architecture" src="d0_s1" to="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>laminal cells bulging, smooth, thin but firm-walled, quadrate to oblate-hexagonal, 16–38 µm × 20–36 µm; marginal cells slightly smaller.</text>
      <biological_entity constraint="laminal" id="o7064" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence_or_shape" src="d0_s2" value="bulging" value_original="bulging" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="width" src="d0_s2" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="firm-walled" value_original="firm-walled" />
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s2" to="oblate-hexagonal" />
        <character char_type="range_value" from="16" from_unit="um" name="length" src="d0_s2" to="38" to_unit="um" />
        <character char_type="range_value" from="20" from_unit="um" name="width" src="d0_s2" to="36" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o7065" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s2" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Perichaetial leaves enlarged, sheathing.</text>
      <biological_entity constraint="perichaetial" id="o7066" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Seta short to virtually absent, 0.04–0.08 mm.</text>
      <biological_entity id="o7067" name="seta" name_original="seta" src="d0_s4" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" modifier="virtually" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character char_type="range_value" from="0.04" from_unit="mm" name="some_measurement" src="d0_s4" to="0.08" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Capsule immersed to emergent, yellowbrown, ovoid-cylindric, 0.9–1.1 mm, stomatose;</text>
      <biological_entity id="o7068" name="capsule" name_original="capsule" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="immersed" value_original="immersed" />
        <character is_modifier="false" name="location" src="d0_s5" value="emergent" value_original="emergent" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovoid-cylindric" value_original="ovoid-cylindric" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s5" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>annuli broad, 2–3 rows of ± quadrate cells wide, persistent;</text>
      <biological_entity id="o7069" name="annulus" name_original="annuli" src="d0_s6" type="structure">
        <character is_modifier="false" name="width" src="d0_s6" value="broad" value_original="broad" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity id="o7070" name="row" name_original="rows" src="d0_s6" type="structure">
        <character is_modifier="false" name="width" src="d0_s6" value="wide" value_original="wide" />
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o7071" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s6" value="quadrate" value_original="quadrate" />
      </biological_entity>
      <relation from="o7070" id="r1686" name="part_of" negation="false" src="d0_s6" to="o7071" />
    </statement>
    <statement id="d0_s7">
      <text>peristome absent;</text>
    </statement>
    <statement id="d0_s8">
      <text>operculm erect-rostrate.</text>
      <biological_entity id="o7072" name="peristome" name_original="peristome" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s8" value="erect-rostrate" value_original="erect-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calpytra to 0.6 mm, smooth, plicate, plicae serrate.</text>
      <biological_entity id="o7073" name="calpytrum" name_original="calpytra" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s9" value="plicate" value_original="plicate" />
      </biological_entity>
      <biological_entity id="o7074" name="plica" name_original="plicae" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spores 23–32 µm, minutely papillose.</text>
      <biological_entity id="o7075" name="spore" name_original="spores" src="d0_s10" type="structure">
        <character char_type="range_value" from="23" from_unit="um" name="some_measurement" src="d0_s10" to="32" to_unit="um" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s10" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry areas on bark of trees and rocks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry areas" constraint="on bark of trees and rocks" />
        <character name="habitat" value="bark" constraint="of trees and rocks" />
        <character name="habitat" value="trees" />
        <character name="habitat" value="rocks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Sonora, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>In general aspect, Erpodium acrifolium resembles E. beccarii G. Venturi, a wide ranging species found in Mexico but not known from the United States. Both species have subulate leaves. However, E. acrifolium has smooth laminal cells whereas the laminal cells of E. beccarii are pluripapillose.</discussion>
  
</bio:treatment>