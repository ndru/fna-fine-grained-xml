<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">334</other_info_on_meta>
    <other_info_on_meta type="treatment_page">354</other_info_on_meta>
    <other_info_on_meta type="illustration_page">354</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">fissidentaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">fissidens</taxon_name>
    <taxon_name authority="(R. S. Williams) Grout" date="1939" rank="species">littlei</taxon_name>
    <place_of_publication>
      <publication_title>Moss Fl. N. Amer.</publication_title>
      <place_in_publication>1: 249. 1939,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fissidentaceae;genus fissidens;species littlei</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075591</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Moenkemeyera</taxon_name>
    <taxon_name authority="R. S. Williams" date="unknown" rank="species">littlei</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>39: 40, plate 4. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Moenkemeyera;species littlei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 3 × 2 mm.</text>
      <biological_entity id="o4845" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s0" to="3" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s0" to="2" to_unit="mm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stem unbranched and sparingly branched;</text>
      <biological_entity id="o4846" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="sparingly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>axillary hyaline nodules absent;</text>
      <biological_entity constraint="axillary" id="o4847" name="nodule" name_original="nodules" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>central strand absent.</text>
      <biological_entity constraint="central" id="o4848" name="strand" name_original="strand" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves as many as 8 pairs, ligulate, obtuse-apiculate to acute, to 1.5 × 0.25 mm;</text>
      <biological_entity id="o4849" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s4" value="ligulate" value_original="ligulate" />
        <character char_type="range_value" from="obtuse-apiculate" name="shape" src="d0_s4" to="acute" />
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s4" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s4" to="0.25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4850" name="pair" name_original="pairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="8" value_original="8" />
      </biological_entity>
      <relation from="o4849" id="r1116" name="as many as" negation="false" src="d0_s4" to="o4850" />
    </statement>
    <statement id="d0_s5">
      <text>dorsal lamina narrowed proximally, ending at insertion or slightly decurrent;</text>
      <biological_entity constraint="dorsal" id="o4851" name="lamina" name_original="lamina" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s5" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>vaginant laminae 1/2–2/3 leaf length, ± equal, minor lamina ending on or near margin;</text>
      <biological_entity constraint="vaginant" id="o4852" name="lamina" name_original="laminae" src="d0_s6" type="structure">
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s6" to="2/3" />
      </biological_entity>
      <biological_entity id="o4853" name="leaf" name_original="leaf" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="length" src="d0_s6" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity constraint="minor" id="o4854" name="lamina" name_original="lamina" src="d0_s6" type="structure" />
      <biological_entity id="o4855" name="margin" name_original="margin" src="d0_s6" type="structure" />
      <relation from="o4854" id="r1117" name="ending on" negation="false" src="d0_s6" to="o4855" />
    </statement>
    <statement id="d0_s7">
      <text>margin crenulate, often irregularly so on vaginant laminae, elimbate;</text>
      <biological_entity id="o4856" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="elimbate" value_original="elimbate" />
      </biological_entity>
      <biological_entity constraint="vaginant" id="o4857" name="lamina" name_original="laminae" src="d0_s7" type="structure" />
      <relation from="o4856" id="r1118" modifier="often irregularly; irregularly" name="on" negation="false" src="d0_s7" to="o4857" />
    </statement>
    <statement id="d0_s8">
      <text>costa ending 3–5 cells before apex, bryoides-type;</text>
      <biological_entity id="o4858" name="costa" name_original="costa" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_ref_taxa" notes="" src="d0_s8" value="bryoides-type" value_original="bryoides-type" />
      </biological_entity>
      <biological_entity id="o4859" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <biological_entity id="o4860" name="apex" name_original="apex" src="d0_s8" type="structure" />
      <relation from="o4858" id="r1119" name="ending" negation="false" src="d0_s8" to="o4859" />
      <relation from="o4858" id="r1120" name="before" negation="false" src="d0_s8" to="o4860" />
    </statement>
    <statement id="d0_s9">
      <text>lamina cells 1-stratose, distinct, mammillose, firm-walled, irregularly quadrate to hexagonal, oblate on margin, 8–10 µm, somewhat larger, ± oblong in proximal parts of vaginant laminae.</text>
      <biological_entity constraint="lamina" id="o4861" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="relief" src="d0_s9" value="mammillose" value_original="mammillose" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="firm-walled" value_original="firm-walled" />
        <character char_type="range_value" from="irregularly quadrate" name="shape" src="d0_s9" to="hexagonal" />
        <character constraint="on margin" constraintid="o4862" is_modifier="false" name="shape" src="d0_s9" value="oblate" value_original="oblate" />
        <character char_type="range_value" from="8" from_unit="um" name="some_measurement" notes="" src="d0_s9" to="10" to_unit="um" />
        <character is_modifier="false" modifier="somewhat" name="size" src="d0_s9" value="larger" value_original="larger" />
        <character constraint="in proximal parts" constraintid="o4863" is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o4862" name="margin" name_original="margin" src="d0_s9" type="structure" />
      <biological_entity constraint="proximal" id="o4863" name="part" name_original="parts" src="d0_s9" type="structure" />
      <biological_entity constraint="vaginant" id="o4864" name="lamina" name_original="laminae" src="d0_s9" type="structure" />
      <relation from="o4863" id="r1121" name="part_of" negation="false" src="d0_s9" to="o4864" />
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition rhizautoicous and gonioautoicous;</text>
    </statement>
    <statement id="d0_s11">
      <text>perigonia gemmiform and on elongate stems.</text>
      <biological_entity id="o4865" name="perigonium" name_original="perigonia" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="gemmiform" value_original="gemmiform" />
      </biological_entity>
      <biological_entity id="o4866" name="stem" name_original="stems" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="elongate" value_original="elongate" />
      </biological_entity>
      <relation from="o4865" id="r1122" name="on" negation="false" src="d0_s11" to="o4866" />
    </statement>
    <statement id="d0_s12">
      <text>Sporophytes 1 per perichaetium.</text>
      <biological_entity id="o4867" name="sporophyte" name_original="sporophytes" src="d0_s12" type="structure">
        <character constraint="per perichaetium" constraintid="o4868" name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o4868" name="perichaetium" name_original="perichaetium" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Seta 1.4–1.8 mm.</text>
      <biological_entity id="o4869" name="seta" name_original="seta" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s13" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsule theca exserted, erect, radially symmetric, to 0.5 mm long;</text>
      <biological_entity constraint="capsule" id="o4870" name="theca" name_original="theca" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s14" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s14" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>peristome teeth anomalous, undivided, papillose;</text>
      <biological_entity constraint="peristome" id="o4871" name="tooth" name_original="teeth" src="d0_s15" type="structure">
        <character is_modifier="false" name="variability" src="d0_s15" value="anomalous" value_original="anomalous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="undivided" value_original="undivided" />
        <character is_modifier="false" name="relief" src="d0_s15" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>operculum to 0.3 mm.</text>
      <biological_entity id="o4872" name="operculum" name_original="operculum" src="d0_s16" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Calyptra cucullate, smooth, 0.3 mm.</text>
      <biological_entity id="o4873" name="calyptra" name_original="calyptra" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character name="some_measurement" src="d0_s17" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Spores 8–11 µm.</text>
      <biological_entity id="o4874" name="spore" name_original="spores" src="d0_s18" type="structure">
        <character char_type="range_value" from="8" from_unit="um" name="some_measurement" src="d0_s18" to="11" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Walls of small pockets in gypsum sinkholes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="walls" constraint="of small pockets in gypsum sinkholes" />
        <character name="habitat" value="small pockets" constraint="in gypsum sinkholes" />
        <character name="habitat" value="gypsum sinkholes" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>32.</number>
  <discussion>This rare Fissidens, named in honor of the American botanist Elbert Little, is known only from the type locality and was recently re-discovered there by K. W. Allred (1998). The plants are pale green, probably the result of their occurrence in small pocketlike depressions where direct sunlight does not penetrate. The species has been confused with F. amoenus, but differs from the latter by its elimbate leaves, slightly smaller, mammillose laminal cells, oblate marginal cells, absence of enlarged pellucid cells in the proximal parts of the vaginant laminae, and undivided, papillose peristome teeth.</discussion>
  
</bio:treatment>