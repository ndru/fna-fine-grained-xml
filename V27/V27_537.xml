<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">377</other_info_on_meta>
    <other_info_on_meta type="mention_page">378</other_info_on_meta>
    <other_info_on_meta type="mention_page">381</other_info_on_meta>
    <other_info_on_meta type="treatment_page">380</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper in W. P. Schimper" date="1856" rank="genus">cynodontium</taxon_name>
    <taxon_name authority="(Wahlenberg) Milde" date="1869" rank="species">alpestre</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Siles.,</publication_title>
      <place_in_publication>51. 1869,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus cynodontium;species alpestre</taxon_hierarchy>
    <other_info_on_name type="fna_id">240000032</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dicranum</taxon_name>
    <taxon_name authority="Wahlenberg" date="unknown" rank="species">alpestre</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Lapp.,</publication_title>
      <place_in_publication>339, fig. 21. 1812</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Dicranum;species alpestre;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cnestrum</taxon_name>
    <taxon_name authority="(Wahlenberg) Nyholm" date="unknown" rank="species">alpestre</taxon_name>
    <taxon_hierarchy>genus Cnestrum;species alpestre;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems to 1.5 cm.</text>
      <biological_entity id="o687" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves to 1.5 mm, broadly lanceolate, nearly all leaves with bluntly rounded apices;</text>
      <biological_entity id="o688" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="to 1.5 mm; broadly" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o689" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o690" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="bluntly" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o689" id="r173" modifier="nearly" name="with" negation="false" src="d0_s1" to="o690" />
    </statement>
    <statement id="d0_s2">
      <text>leaf margins recurved proximally, the cells mostly 1-stratose;</text>
      <biological_entity constraint="leaf" id="o691" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="proximally" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o692" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s2" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cells of distal lamina (6–) 8–9 µm wide, with moderately developed or high mammillae and 1 (–2) papillae per cell, walls rather thin, angular to somewhat rounded.</text>
      <biological_entity id="o693" name="cell" name_original="cells" src="d0_s3" type="structure" constraint="lamina" constraint_original="lamina; lamina">
        <character char_type="range_value" from="6" from_unit="um" name="width" src="d0_s3" to="8" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s3" to="9" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="distal" id="o694" name="lamina" name_original="lamina" src="d0_s3" type="structure" />
      <biological_entity id="o695" name="mammilla" name_original="mammillae" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="moderately" name="development" src="d0_s3" value="developed" value_original="developed" />
        <character is_modifier="true" name="height" src="d0_s3" value="high" value_original="high" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="2" />
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o696" name="papilla" name_original="papillae" src="d0_s3" type="structure" />
      <biological_entity id="o697" name="cell" name_original="cell" src="d0_s3" type="structure" />
      <biological_entity id="o698" name="wall" name_original="walls" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="rather" name="width" src="d0_s3" value="thin" value_original="thin" />
        <character is_modifier="false" name="shape" src="d0_s3" value="angular to somewhat" value_original="angular to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o693" id="r174" name="part_of" negation="false" src="d0_s3" to="o694" />
      <relation from="o693" id="r175" name="with" negation="false" src="d0_s3" to="o695" />
      <relation from="o696" id="r176" name="per" negation="false" src="d0_s3" to="o697" />
    </statement>
    <statement id="d0_s4">
      <text>Perigonium short-stalked.</text>
      <biological_entity id="o699" name="perigonium" name_original="perigonium" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="short-stalked" value_original="short-stalked" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Seta ca. 0.6 cm, straight wet or dry.</text>
      <biological_entity id="o700" name="seta" name_original="seta" src="d0_s5" type="structure">
        <character name="some_measurement" src="d0_s5" unit="cm" value="0.6" value_original="0.6" />
        <character is_modifier="false" name="condition" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="condition" src="d0_s5" value="wet" value_original="wet" />
        <character is_modifier="false" name="condition" src="d0_s5" value="dry" value_original="dry" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsule symmetrical, erect, not strumose;</text>
      <biological_entity id="o701" name="capsule" name_original="capsule" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="symmetrical" value_original="symmetrical" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="strumose" value_original="strumose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>annulus weakly developed, adherent.</text>
      <biological_entity id="o702" name="annulus" name_original="annulus" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="weakly" name="development" src="d0_s7" value="developed" value_original="developed" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="adherent" value_original="adherent" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock crevices, commonly on calcareous substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="crevices" modifier="rock" />
        <character name="habitat" value="calcareous substrates" modifier="commonly on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., N.W.T., Sask., Yukon; Alaska, Maine, Wis.; n Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="n Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>The description of Cynodontium alpestre above follows authors who did not consider the species to be a variant of C. tenellum. Authors who do unite the two (as C. alpestre) conflate the characters, emphasizing those of C. tenellum. The present description and that of, for example, H. A. Crum and L. E. Anderson (1981) for C. alpestre would not be parallel and would be misleading. Cynodontium tenellum as C. alpestre, for example, would have strongly 2- to multistratose margins and sessile perigonial buds, and the cells would be smooth on the lamina; all these features are contradicted by the present description. The distribution in the flora area is poorly known for the United States because of confusion with C. tenellum, and here follows W. C. Steere (1978) and R. R. Ireland et al. (1987). See G. S. Mogensen (1980) for clarification of the taxonomic position of Cynodontium alpestre.</discussion>
  <references>
    <reference>Mogensen, G. S. 1980. Cnestrum alpestre, with notes on the nomenclature of Dicranum alpestre Wahlenb. Lindbergia 6: 118–120.</reference>
  </references>
  
</bio:treatment>