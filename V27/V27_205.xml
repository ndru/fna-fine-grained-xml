<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="treatment_page">163</other_info_on_meta>
    <other_info_on_meta type="illustration_page">164</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="M. Fleischer" date="unknown" rank="family">diphysciaceae</taxon_name>
    <taxon_name authority="D. Mohr" date="1803" rank="genus">diphyscium</taxon_name>
    <taxon_name authority="(Hedwig) D. Mohr" date="1803" rank="species">foliosum</taxon_name>
    <place_of_publication>
      <publication_title>Observ. Bot.,</publication_title>
      <place_in_publication>35. 1803,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family diphysciaceae;genus diphyscium;species foliosum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443353</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Buxbaumia</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="species">foliosa</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>166. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Buxbaumia;species foliosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants dark green to brownish, dull, forming hard tufts.</text>
      <biological_entity id="o4733" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="dark green" name="coloration" src="d0_s0" to="brownish" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="dull" value_original="dull" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4734" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="hard" value_original="hard" />
      </biological_entity>
      <relation from="o4733" id="r1314" name="forming" negation="false" src="d0_s0" to="o4734" />
    </statement>
    <statement id="d0_s1">
      <text>Stems unbranched, erect, 0.5–1 mm, strongly radiculose.</text>
      <biological_entity id="o4735" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s1" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 0.5–4 mm, crisped and imbricate when dry, margins entire or weakly toothed with papillae, apex blunt, the most proximal leaves shorter than the most distal, laminal cells mammillose or papillose through most of lamima.</text>
      <biological_entity id="o4736" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s2" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="crisped" value_original="crisped" />
        <character is_modifier="false" modifier="when dry" name="arrangement" src="d0_s2" value="imbricate" value_original="imbricate" />
      </biological_entity>
      <biological_entity id="o4737" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character constraint="with papillae" constraintid="o4738" is_modifier="false" modifier="weakly" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o4738" name="papilla" name_original="papillae" src="d0_s2" type="structure" />
      <biological_entity id="o4739" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="blunt" value_original="blunt" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o4740" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="than the most distal leaves" constraintid="o4741" is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4741" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="laminal" id="o4742" name="bud" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="relief" src="d0_s2" value="mammillose" value_original="mammillose" />
        <character constraint="of lamima" constraintid="o4743" is_modifier="false" name="relief" src="d0_s2" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o4743" name="lamimum" name_original="lamima" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Perichaetial leaves brownish when mature, with spinulose awn, lamina at awn base lacerate and membranaceous.</text>
      <biological_entity constraint="perichaetial" id="o4744" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="when mature" name="coloration" src="d0_s3" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity id="o4745" name="awn" name_original="awn" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s3" value="spinulose" value_original="spinulose" />
      </biological_entity>
      <biological_entity id="o4746" name="lamina" name_original="lamina" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="membranaceous" value_original="membranaceous" />
      </biological_entity>
      <biological_entity constraint="awn" id="o4747" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lacerate" value_original="lacerate" />
      </biological_entity>
      <relation from="o4744" id="r1315" name="with" negation="false" src="d0_s3" to="o4745" />
      <relation from="o4746" id="r1316" name="at" negation="false" src="d0_s3" to="o4747" />
    </statement>
    <statement id="d0_s4">
      <text>Capsule broadly ovoid, (2–) 3–4 mm, stomata phaneropore near capsule base;</text>
      <biological_entity id="o4748" name="capsule" name_original="capsule" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="stomata" id="o4749" name="phaneropore" name_original="phaneropore" src="d0_s4" type="structure" />
      <biological_entity constraint="capsule" id="o4750" name="base" name_original="base" src="d0_s4" type="structure" />
      <relation from="o4749" id="r1317" name="near" negation="false" src="d0_s4" to="o4750" />
    </statement>
    <statement id="d0_s5">
      <text>mature sporangium emergent from spreading perichaetium.</text>
      <biological_entity id="o4751" name="sporangium" name_original="sporangium" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="mature" value_original="mature" />
        <character constraint="from perichaetium" constraintid="o4752" is_modifier="false" name="location" src="d0_s5" value="emergent" value_original="emergent" />
      </biological_entity>
      <biological_entity id="o4752" name="perichaetium" name_original="perichaetium" src="d0_s5" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spores 6–8 µm.</text>
      <biological_entity id="o4753" name="spore" name_original="spores" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_unit="um" name="some_measurement" src="d0_s6" to="8" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="early summer" from="early summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil banks and soil of forest floors, also in tundras</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil banks" />
        <character name="habitat" value="soil" constraint="of forest floors , also" />
        <character name="habitat" value="forest floors" />
        <character name="habitat" value="tundras" modifier="also" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (50-1000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="50" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que.; Ala., Alaska, Ark., Conn., Del., Ga., Ill., Ind., Kans., Ky., La., Maine, Md., Mass., Mo., N.H., N.J., N.Y., N.C., Ohio, Okla., Pa., S.C., Vt., Va., W.Va., Wis.; Mexico; Central America (Guatemala); Europe; Asia; Atlantic Islands (Azores, Iceland, Madeira).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Azores)" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Iceland)" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Madeira)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>In western North America, Diphyscium foliosum is terrestrial in tundra sites, often in blowouts; it is also found as humid perpendicular sods pendent from ledges and on rock in canyon walls; in eastern North America it is found on banks and horizontal surfaces in forests. The unique golf-tee-like protonemal flaps, which can be excavated from the rhizoids, are a distinctive family trait.</discussion>
  <references>
    <reference>Shaw, A. J., L. E. Anderson, and B. D. Mishler. 1987. Peristome development in mosses in relation to systematics and evolution. I. Diphyscium foliosum (Buxbaumiaceae). Mem. New York Bot. Gard. 45: 55–70.</reference>
  </references>
  
</bio:treatment>