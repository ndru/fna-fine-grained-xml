<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">566</other_info_on_meta>
    <other_info_on_meta type="treatment_page">567</other_info_on_meta>
    <other_info_on_meta type="illustration_page">567</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="(Herzog) Hilpert" date="1933" rank="subfamily">barbuloideae</taxon_name>
    <taxon_name authority="P. C. Chen" date="1941" rank="genus">bryoerythrophyllum</taxon_name>
    <taxon_name authority="(Taylor) R. H. Zander" date="1980" rank="species">inaequalifolium</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>83: 232. 1980,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily barbuloideae;genus bryoerythrophyllum;species inaequalifolium</taxon_hierarchy>
    <other_info_on_name type="fna_id">240002009</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Barbula</taxon_name>
    <taxon_name authority="Taylor" date="unknown" rank="species">inaequalifolia</taxon_name>
    <place_of_publication>
      <publication_title>London J. Bot.</publication_title>
      <place_in_publication>5: 49. 1846</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Barbula;species inaequalifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems to 1.5 cm.</text>
      <biological_entity id="o4776" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stem-leaves ovate to ovatelanceolate, to 1–2 mm, distal lamina often 2-stratose in small patches above, margins narrowly recurved or revolute to near apex, entire, rounded to obtusely acute apically;</text>
      <biological_entity id="o4777" name="leaf-stem" name_original="stem-leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s1" to="ovatelanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="distance" src="d0_s1" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4778" name="lamina" name_original="lamina" src="d0_s1" type="structure">
        <character constraint="in patches" constraintid="o4779" is_modifier="false" modifier="often" name="architecture" src="d0_s1" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity id="o4779" name="patch" name_original="patches" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o4780" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="narrowly" name="orientation" src="d0_s1" value="recurved" value_original="recurved" />
        <character constraint="to apex" constraintid="o4781" is_modifier="false" name="shape_or_vernation" src="d0_s1" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape" notes="" src="d0_s1" value="entire" value_original="entire" />
        <character char_type="range_value" from="rounded" modifier="apically" name="shape" src="d0_s1" to="obtusely acute" />
      </biological_entity>
      <biological_entity id="o4781" name="apex" name_original="apex" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>costa ending 1–2 cells before apex, not much widened or tapering, to ca. 6 cells wide at mid leaf, adaxial surface of the costa weakly convex;</text>
      <biological_entity id="o4782" name="costa" name_original="costa" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not much" name="width" notes="" src="d0_s2" value="widened" value_original="widened" />
        <character is_modifier="false" name="shape" src="d0_s2" value="tapering" value_original="tapering" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s2" to="6" />
      </biological_entity>
      <biological_entity id="o4783" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s2" to="2" />
      </biological_entity>
      <biological_entity id="o4784" name="apex" name_original="apex" src="d0_s2" type="structure" />
      <biological_entity id="o4785" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character constraint="at mid leaf" constraintid="o4786" is_modifier="false" name="width" src="d0_s2" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity constraint="mid" id="o4786" name="leaf" name_original="leaf" src="d0_s2" type="structure" />
      <biological_entity constraint="adaxial" id="o4787" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s2" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o4788" name="costa" name_original="costa" src="d0_s2" type="structure" />
      <relation from="o4782" id="r1127" name="ending" negation="false" src="d0_s2" to="o4783" />
      <relation from="o4782" id="r1128" name="before" negation="false" src="d0_s2" to="o4784" />
      <relation from="o4787" id="r1129" name="part_of" negation="false" src="d0_s2" to="o4788" />
    </statement>
    <statement id="d0_s3">
      <text>proximal cells differentiated across leaf, rectangular, 3–4: 1.</text>
      <biological_entity id="o4790" name="leaf" name_original="leaf" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Specialized asexual reproduction often present, by masses of unicellular gemmae on stalks in leaf-axils.</text>
      <biological_entity id="o4791" name="mass" name_original="masses" src="d0_s4" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s4" value="asexual" value_original="asexual" />
        <character is_modifier="false" modifier="often" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o4792" name="gemma" name_original="gemmae" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="unicellular" value_original="unicellular" />
      </biological_entity>
      <biological_entity id="o4793" name="stalk" name_original="stalks" src="d0_s4" type="structure" />
      <biological_entity id="o4794" name="leaf-axil" name_original="leaf-axils" src="d0_s4" type="structure" />
      <relation from="o4791" id="r1130" name="part_of" negation="false" src="d0_s4" to="o4792" />
      <relation from="o4791" id="r1131" name="on" negation="false" src="d0_s4" to="o4793" />
      <relation from="o4793" id="r1132" name="in" negation="false" src="d0_s4" to="o4794" />
    </statement>
    <statement id="d0_s5">
      <text>Sexual condition dioicous.</text>
      <biological_entity constraint="proximal" id="o4789" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character constraint="across leaf" constraintid="o4790" is_modifier="false" name="variability" src="d0_s3" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" notes="" src="d0_s3" value="rectangular" value_original="rectangular" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="4" />
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
        <character is_modifier="false" name="development" src="d0_s4" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>[Capsule theca 1.5–3 mm; operculum 0.8–1.3 mm; peristome well developed, twisted, 500–950 µm. Spores 9–11 µm.]</text>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporophytes absent in the area of the flora.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="moderate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C.; Mexico; West Indies; Central America; South America; Eurasia; n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Bryoerythrophyllum inaequalifolium is found at only two locations in the flora area. It is easily distinguished from other species in the area by the rounded leaf apices and masses of unicellular gemmae usually present in the leaf axils. Unicellular gemmae are not unique to this species in the Pottiaceae; they are also found in Didymodon revolutus, which is distinguished by the spurred costa having only one stereid band, the distal laminal cell walls evenly thickened, and papillae solid, low, broad, simple to multiplex, usually lenslike. For additional synonymy and discussion, see R. H. Zander (1968).</discussion>
  
</bio:treatment>