<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">62</other_info_on_meta>
    <other_info_on_meta type="mention_page">64</other_info_on_meta>
    <other_info_on_meta type="treatment_page">76</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">sphagnaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sphagnum</taxon_name>
    <taxon_name authority="Lindberg" date="1862" rank="section">cuspidata</taxon_name>
    <taxon_name authority="R. E. Andrus" date="1988" rank="species">rubroflexuosum</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>91: 364, figs. 1–8. 1988,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sphagnaceae;genus sphagnum;section cuspidata;species rubroflexuosum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443235</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small, soft, fairly weak-stemmed;</text>
    </statement>
    <statement id="d0_s1">
      <text>pale green to pale-yellow brown;</text>
      <biological_entity id="o8823" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s0" value="soft" value_original="soft" />
        <character is_modifier="false" modifier="fairly" name="architecture" src="d0_s0" value="weak-stemmed" value_original="weak-stemmed" />
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s1" to="pale-yellow brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>capitulum not 5-radiate or only weakly so, may tinged with red;</text>
    </statement>
    <statement id="d0_s3">
      <text>loose to somewhat compact.</text>
      <biological_entity id="o8824" name="capitulum" name_original="capitulum" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement" src="d0_s2" value="5-radiate" value_original="5-radiate" />
        <character name="architecture_or_arrangement" src="d0_s2" value="only weakly" value_original="only weakly" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="tinged with red" value_original="tinged with red" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="loose to somewhat" value_original="loose to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_arrangement" src="d0_s3" value="compact" value_original="compact" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems pale green to pink;</text>
      <biological_entity id="o8825" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s4" to="pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>superficial cortex of undifferentiated.</text>
      <biological_entity constraint="superficial" id="o8826" name="cortex" name_original="cortex" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Stem-leaves 0.7–1 mm (to 1.2 mm in hemiisophyllous forms) elongate-triangular to triangular-lingulate, apex obtuse-erose, to apiculate;</text>
      <biological_entity id="o8827" name="leaf-stem" name_original="stem-leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="distance" src="d0_s6" to="1" to_unit="mm" />
        <character char_type="range_value" from="elongate-triangular" name="shape" src="d0_s6" to="triangular-lingulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>usually fibrillose at least apically;</text>
    </statement>
    <statement id="d0_s8">
      <text>in hemiisophyllous forms spreading and in anisophyllous forms appressed;</text>
      <biological_entity id="o8828" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="obtuse-erose" name="architecture" src="d0_s6" to="apiculate" />
        <character char_type="range_value" from="obtuse-erose" name="architecture" src="d0_s6" to="apiculate" />
        <character is_modifier="false" modifier="usually; at-least apically; apically" name="texture" src="d0_s7" value="fibrillose" value_original="fibrillose" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character constraint="in anisophyllous forms" is_modifier="false" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hyaline cells often septate at base.</text>
      <biological_entity id="o8829" name="bud" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="hyaline" value_original="hyaline" />
        <character constraint="at base" constraintid="o8830" is_modifier="false" modifier="often" name="architecture" src="d0_s9" value="septate" value_original="septate" />
      </biological_entity>
      <biological_entity id="o8830" name="base" name_original="base" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Branches moderately long and tapering, unranked to weakly 5-ranked, leaves not much elongated at distal end.</text>
      <biological_entity id="o8831" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="moderately" name="length_or_size" src="d0_s10" value="long" value_original="long" />
        <character is_modifier="false" name="shape" src="d0_s10" value="tapering" value_original="tapering" />
        <character char_type="range_value" from="unranked" name="arrangement" src="d0_s10" to="weakly 5-ranked" />
      </biological_entity>
      <biological_entity id="o8832" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character constraint="at distal end" constraintid="o8833" is_modifier="false" modifier="not much" name="length" src="d0_s10" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8833" name="end" name_original="end" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Branch fascicles with 2 spreading and 2–3 pendent branches.</text>
      <biological_entity id="o8834" name="branch" name_original="branch" src="d0_s11" type="structure">
        <character constraint="with branches" constraintid="o8835" is_modifier="false" name="arrangement" src="d0_s11" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o8835" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="true" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s11" to="3" />
        <character is_modifier="true" name="orientation" src="d0_s11" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Branch stem cortex enlarged and with conspicuous retort cells.</text>
      <biological_entity constraint="stem" id="o8836" name="cortex" name_original="cortex" src="d0_s12" type="structure" constraint_original="branch stem">
        <character is_modifier="false" name="size" src="d0_s12" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity constraint="retort" id="o8837" name="bud" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s12" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <relation from="o8836" id="r2429" name="with" negation="false" src="d0_s12" to="o8837" />
    </statement>
    <statement id="d0_s13">
      <text>Branch leaves 1–1.7 mm, ovatelanceolate, undulate and recurved when dry;</text>
      <biological_entity constraint="branch" id="o8838" name="leaf" name_original="leaves" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="undulate" value_original="undulate" />
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s13" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>hyaline cells on convex surface with 3–10 round pores per cell in the cell angles and free, on concave surface with round wall-thinnings in the ends and angles.</text>
      <biological_entity id="o8840" name="surface" name_original="surface" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o8841" name="pore" name_original="pores" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s14" to="10" />
        <character is_modifier="true" name="shape" src="d0_s14" value="round" value_original="round" />
      </biological_entity>
      <biological_entity id="o8842" name="cell" name_original="cell" src="d0_s14" type="structure" />
      <biological_entity constraint="cell" id="o8843" name="angle" name_original="angles" src="d0_s14" type="structure" />
      <biological_entity id="o8844" name="surface" name_original="surface" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity id="o8845" name="wall-thinning" name_original="wall-thinnings" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="round" value_original="round" />
      </biological_entity>
      <biological_entity id="o8846" name="end" name_original="ends" src="d0_s14" type="structure" />
      <biological_entity id="o8847" name="angle" name_original="angles" src="d0_s14" type="structure" />
      <relation from="o8839" id="r2430" name="on" negation="false" src="d0_s14" to="o8840" />
      <relation from="o8840" id="r2431" name="with" negation="false" src="d0_s14" to="o8841" />
      <relation from="o8841" id="r2432" name="per" negation="false" src="d0_s14" to="o8842" />
      <relation from="o8842" id="r2433" name="in" negation="false" src="d0_s14" to="o8843" />
      <relation from="o8839" id="r2434" name="on" negation="false" src="d0_s14" to="o8844" />
      <relation from="o8844" id="r2435" name="with" negation="false" src="d0_s14" to="o8845" />
      <relation from="o8845" id="r2436" name="in" negation="false" src="d0_s14" to="o8846" />
      <relation from="o8845" id="r2437" name="in" negation="false" src="d0_s14" to="o8847" />
    </statement>
    <statement id="d0_s15">
      <text>Sexual condition unknown.</text>
      <biological_entity id="o8839" name="bud" name_original="cells" src="d0_s14" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s14" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s14" value="free" value_original="free" />
        <character is_modifier="false" name="reproduction" src="d0_s15" value="sexual" value_original="sexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Spores not seen.</text>
      <biological_entity id="o8848" name="spore" name_original="spores" src="d0_s16" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Forming carpets in weakly minerotrophic fens</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="carpets" modifier="forming" constraint="in weakly minerotrophic fens" />
        <character name="habitat" value="weakly minerotrophic fens" modifier="in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="moderate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md., Pa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>45.</number>
  <discussion>Sporophytes are unknown in Sphagnum rubroflexuosum. Compared to the closely related S. flexuosum, this species is paler and may have a reddish stem. Otherwise, identification must be made microscopically on the basis of branch leaf porosity. Although we have not seen this species in the field, it should be separable from S. majus, the only other large, aquatic species of sect. Cuspidata, in its range by traits of stem leaves and its color. Sphagnum majus is also typically a much darker brown.</discussion>
  
</bio:treatment>