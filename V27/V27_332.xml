<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">228</other_info_on_meta>
    <other_info_on_meta type="mention_page">244</other_info_on_meta>
    <other_info_on_meta type="treatment_page">243</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">grimmia</taxon_name>
    <taxon_name authority="I. Hagen" date="1909" rank="subgenus">litoneuron</taxon_name>
    <taxon_name authority="Hooker in R. K. Greville" date="1824" rank="species">unicolor</taxon_name>
    <place_of_publication>
      <publication_title>in R. K. Greville, Scott. Crypt. Fl.</publication_title>
      <place_in_publication>3: plate 123. 1824,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus grimmia;subgenus litoneuron;species unicolor</taxon_hierarchy>
    <other_info_on_name type="fna_id">200001293</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in dense to loose patches, pale green to redbrown.</text>
      <biological_entity id="o7756" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="pale green" name="coloration" notes="" src="d0_s0" to="redbrown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7757" name="patch" name_original="patches" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o7756" id="r1851" name="in" negation="false" src="d0_s0" to="o7757" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1.5–4 (–5) cm.</text>
      <biological_entity id="o7758" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves narrowly oblong-lanceolate to ligulate from an ovate base, 1.5–2.5 × 0.5–0.7 mm, both margins incurved, intermarginal bands absent, often sheathing, muticous, cucullate, obtuse-rounded, costa narrow proximally;</text>
      <biological_entity id="o7759" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character constraint="from base" constraintid="o7760" is_modifier="false" name="architecture" src="d0_s2" value="ligulate" value_original="ligulate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" notes="" src="d0_s2" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" notes="" src="d0_s2" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7760" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o7761" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="incurved" value_original="incurved" />
      </biological_entity>
      <biological_entity constraint="intermarginal" id="o7762" name="band" name_original="bands" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s2" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" name="shape" src="d0_s2" value="muticous" value_original="muticous" />
        <character is_modifier="false" name="shape" src="d0_s2" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="obtuse-rounded" value_original="obtuse-rounded" />
      </biological_entity>
      <biological_entity id="o7763" name="costa" name_original="costa" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="proximally" name="size_or_width" src="d0_s2" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal juxtacostal laminal cells short-rectangular, straight, thick lateral walled, pale-yellow;</text>
      <biological_entity constraint="basal" id="o7764" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="laminal" id="o7765" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="short-rectangular" value_original="short-rectangular" />
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" name="width" src="d0_s3" value="thick" value_original="thick" />
        <character is_modifier="false" name="position" src="d0_s3" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="walled" value_original="walled" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="pale-yellow" value_original="pale-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal marginal laminal cells short-rectangular, straight, thick lateral walled, pale-yellow, hyaline;</text>
      <biological_entity constraint="basal marginal laminal" id="o7766" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="short-rectangular" value_original="short-rectangular" />
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" name="width" src="d0_s4" value="thick" value_original="thick" />
        <character is_modifier="false" name="position" src="d0_s4" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="walled" value_original="walled" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>medial laminal cells rounded to quadrate, straight, thick-walled;</text>
      <biological_entity constraint="medial laminal" id="o7767" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="quadrate" />
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal laminal cells 2–3-stratose, rounded, thick-walled.</text>
      <biological_entity constraint="distal laminal" id="o7768" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-3-stratose" value_original="2-3-stratose" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Perichaetial leaves enlarged.</text>
      <biological_entity constraint="perichaetial" id="o7769" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="enlarged" value_original="enlarged" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seta straight to slightly sigmoid, 2–4 mm.</text>
      <biological_entity id="o7770" name="seta" name_original="seta" src="d0_s8" type="structure">
        <character char_type="range_value" from="straight" name="course" src="d0_s8" to="slightly sigmoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule occasionally present, exserted, brown, oblong-ovoid, exothecial cells short-rectangular, thin-walled, stomata present, annulus of 2–3 rows of rectangular, thick-walled cells, operculum long-rostrate, peristome perforate and split in distal half.</text>
      <biological_entity id="o7771" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="occasionally" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong-ovoid" value_original="oblong-ovoid" />
      </biological_entity>
      <biological_entity constraint="exothecial" id="o7772" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="short-rectangular" value_original="short-rectangular" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <biological_entity id="o7773" name="stoma" name_original="stomata" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o7774" name="annulus" name_original="annulus" src="d0_s9" type="structure" />
      <biological_entity id="o7775" name="row" name_original="rows" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s9" to="3" />
      </biological_entity>
      <biological_entity id="o7776" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="rectangular" value_original="rectangular" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
      <biological_entity id="o7777" name="operculum" name_original="operculum" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="long-rostrate" value_original="long-rostrate" />
      </biological_entity>
      <biological_entity id="o7778" name="peristome" name_original="peristome" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="perforate" value_original="perforate" />
      </biological_entity>
      <biological_entity id="o7779" name="split" name_original="split" src="d0_s9" type="structure" />
      <biological_entity constraint="distal" id="o7780" name="cell" name_original="cells" src="d0_s9" type="structure" />
      <relation from="o7774" id="r1852" name="consist_of" negation="false" src="d0_s9" to="o7775" />
      <relation from="o7775" id="r1853" name="part_of" negation="false" src="d0_s9" to="o7776" />
      <relation from="o7779" id="r1854" name="in" negation="false" src="d0_s9" to="o7780" />
    </statement>
    <statement id="d0_s10">
      <text>Calyptra mitrate.</text>
      <biological_entity id="o7781" name="calyptra" name_original="calyptra" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="mitrate" value_original="mitrate" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cracks of wet acidic, siliceous rocks especially along streams or splash zones of lake shores</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cracks" constraint="of wet acidic , siliceous rocks" />
        <character name="habitat" value="wet acidic" />
        <character name="habitat" value="siliceous rocks" />
        <character name="habitat" value="streams" modifier="especially" />
        <character name="habitat" value="splash zones" constraint="of lake shores" />
        <character name="habitat" value="lake shores" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (200-2000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="200" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; B.C., Nfld. and Labr. (Nfld.), Ont., Que.; Alaska, Calif., Maine, Mich., Minn., N.Y., Vt.; Eurasia; Africa (Ethiopia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa (Ethiopia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <discussion>In Canada, Grimmia unicolor is found predominantly around the Great Lakes of southern Ontario eastward into southwestern Quebec, with a disjunct site in Newfoundland. In the United States, it is also commonly found in the Great Lakes region and extending eastward into southwestern Maine. The disjunct sites in British Columbia and on the Seward Peninsula may be a Beringial link with populations in Asia, where this species is widely distributed, and in northern Europe. Grimmia unicolor is often found in the splash zone of rocky shorelines, especially cliffs, and along rivers. This habitat is rarely occupied by other species of the genus, except for G. olneyi. Grimmia unicolor is readily distinguished from other species in the subgenus by its leaf morphology. Its leaves are oblong-lanceolate to ligulate, with an obtuse, rounded apex which is muticous. Grimmia olneyi has an acute leaf apex with long awns, and its basal marginal cells are quadrate, contrasting with the rectangular cells of G. unicolor. Sporophytically, G. unicolor typically has a straight seta while that of G. olneyi is sigmoid. Grimmia unicolor has a much narrower costa proximally and lacks an awn, in marked contrast to the broad costa and robust awn of G. laevigata. The leaf shapes of these two species are distinct, with G. unicolor having a pronounced ovate base and narrow leaves tending towards being ligulate, while G. laevigata has broadly oblong-ovate leaves without a shoulder separating the distal and proximal lamina.</discussion>
  
</bio:treatment>