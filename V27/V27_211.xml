<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">168</other_info_on_meta>
    <other_info_on_meta type="treatment_page">167</other_info_on_meta>
    <other_info_on_meta type="illustration_page">167</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">timmiaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">timmia</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="species">megapolitana</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">megapolitana</taxon_name>
    <taxon_hierarchy>family timmiaceae;genus timmia;species megapolitana;subspecies megapolitana</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443362</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Timmia</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">cucullata</taxon_name>
    <taxon_hierarchy>genus Timmia;species cucullata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Cells of mid-limb lamina (8–) 9–12 (–14) µm wide (mean 10.5 µm);</text>
      <biological_entity id="o4046" name="bud" name_original="cells" src="d0_s0" type="structure">
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s0" to="9" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="um" name="width" src="d0_s0" to="14" to_unit="um" />
        <character char_type="range_value" from="9" from_unit="um" name="width" src="d0_s0" to="12" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="mid-limb" id="o4047" name="lamina" name_original="lamina" src="d0_s0" type="structure" />
      <relation from="o4046" id="r1097" name="part_of" negation="false" src="d0_s0" to="o4047" />
    </statement>
    <statement id="d0_s1">
      <text>cells of distal part of leaf-sheath with 1–6 papillae on the abaxial surface.</text>
      <biological_entity id="o4048" name="bud" name_original="cells" src="d0_s1" type="structure" constraint="part" constraint_original="part; part" />
      <biological_entity constraint="distal" id="o4049" name="part" name_original="part" src="d0_s1" type="structure" />
      <biological_entity id="o4050" name="sheath" name_original="leaf-sheath" src="d0_s1" type="structure" />
      <biological_entity id="o4051" name="papilla" name_original="papillae" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s1" to="6" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4052" name="surface" name_original="surface" src="d0_s1" type="structure" />
      <relation from="o4048" id="r1098" name="part_of" negation="false" src="d0_s1" to="o4049" />
      <relation from="o4048" id="r1099" name="part_of" negation="false" src="d0_s1" to="o4050" />
      <relation from="o4048" id="r1100" name="with" negation="false" src="d0_s1" to="o4051" />
      <relation from="o4051" id="r1101" name="on" negation="false" src="d0_s1" to="o4052" />
    </statement>
    <statement id="d0_s2">
      <text>Calyptra often remaining attached to setae at base of capsule.</text>
      <biological_entity id="o4053" name="calyptra" name_original="calyptra" src="d0_s2" type="structure" />
      <biological_entity id="o4054" name="seta" name_original="setae" src="d0_s2" type="structure">
        <character is_modifier="true" name="fixation" src="d0_s2" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o4055" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o4056" name="capsule" name_original="capsule" src="d0_s2" type="structure" />
      <relation from="o4053" id="r1102" name="remaining" negation="false" src="d0_s2" to="o4054" />
      <relation from="o4053" id="r1103" name="at" negation="false" src="d0_s2" to="o4055" />
      <relation from="o4055" id="r1104" name="part_of" negation="false" src="d0_s2" to="o4056" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Almost always with sporophytes. Most often in deciduous forests, on humus in moist, shady calcareous sites, northwards in forested localities, especially along major rivers, and as an adventive in disturbed sites such as lawns, golf courses or cemeteries</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sporophytes" modifier="almost always with" />
        <character name="habitat" value="deciduous forests" modifier="most often in" />
        <character name="habitat" value="humus" modifier="on" constraint="in moist" />
        <character name="habitat" value="moist" />
        <character name="habitat" value="shady calcareous sites" />
        <character name="habitat" value="northwards" constraint="in forested localities" />
        <character name="habitat" value="forested localities" />
        <character name="habitat" value="major rivers" modifier="especially along" />
        <character name="habitat" value="an adventive" modifier="and as" constraint="in disturbed sites such as lawns" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="lawns" />
        <character name="habitat" value="golf courses" />
        <character name="habitat" value="cemeteries" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-500 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., N.S., Ont., Que., Sask., Yukon; Alaska, Ark., Conn., Ill., Ind., Iowa, Ky., Mass., Mich., Minn., Mo., Mont., Nebr., N.J., N.Y., Pa., S.Dak., Tenn., Vt., Va., Wis.; Eurasia (China, Finland, Germany [extinct], Netherlands, Russia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Eurasia (China)" establishment_means="native" />
        <character name="distribution" value="Eurasia (Finland)" establishment_means="native" />
        <character name="distribution" value="Eurasia (Germany [extinct])" establishment_means="native" />
        <character name="distribution" value="Eurasia (Netherlands)" establishment_means="native" />
        <character name="distribution" value="Eurasia (Russia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2a.</number>
  <other_name type="common_name">Indian feather moss</other_name>
  <discussion>Subspecies megapolitana is the only taxon in the genus that is not an arctic or montane moss; it occurs primarily in temperate regions, and sporadic in the boreal region. It is also the only taxon occurring in man-made habitats. The common name reflects the resemblance of the erect, persistent calyptra to the feather headdresses used by some North American Indians.</discussion>
  
</bio:treatment>