<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">142</other_info_on_meta>
    <other_info_on_meta type="treatment_page">143</other_info_on_meta>
    <other_info_on_meta type="illustration_page">144</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwägrichen" date="unknown" rank="family">polytrichaceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1805" rank="genus">oligotrichum</taxon_name>
    <taxon_name authority="Mitten" date="1864" rank="species">aligerum</taxon_name>
    <place_of_publication>
      <publication_title>J. Linn. Soc., Bot.</publication_title>
      <place_in_publication>8: 48, plate 8. 1864,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polytrichaceae;genus oligotrichum;species aligerum</taxon_hierarchy>
    <other_info_on_name type="fna_id">200002619</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants simple to sparingly branched by innovations, light olive green.</text>
      <biological_entity id="o4182" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" constraint="by innovations" constraintid="o4183" from="simple" name="architecture" src="d0_s0" to="sparingly branched" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="light olive" value_original="light olive" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4183" name="innovation" name_original="innovations" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 2–3 cm, slender and wiry, erect.</text>
      <biological_entity id="o4184" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="wiry" value_original="wiry" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 3–5 mm, ovatelanceolate, with a subsheathing base, ± erect-appressed when dry, somewhat spreading when moist, channeled distally, with lamellae on both surfaces;</text>
      <biological_entity id="o4185" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" modifier="when dry" name="fixation_or_orientation" notes="" src="d0_s2" value="erect-appressed" value_original="erect-appressed" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s2" value="channeled" value_original="channeled" />
      </biological_entity>
      <biological_entity id="o4186" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="subsheathing" value_original="subsheathing" />
      </biological_entity>
      <biological_entity id="o4187" name="lamella" name_original="lamellae" src="d0_s2" type="structure" />
      <biological_entity id="o4188" name="surface" name_original="surfaces" src="d0_s2" type="structure" />
      <relation from="o4185" id="r1140" name="with" negation="false" src="d0_s2" to="o4186" />
      <relation from="o4185" id="r1141" name="with" negation="false" src="d0_s2" to="o4187" />
      <relation from="o4187" id="r1142" name="on" negation="false" src="d0_s2" to="o4188" />
    </statement>
    <statement id="d0_s3">
      <text>costa percurrent, ending in a short apiculus;</text>
      <biological_entity id="o4189" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="percurrent" value_original="percurrent" />
      </biological_entity>
      <biological_entity id="o4190" name="apiculu" name_original="apiculus" src="d0_s3" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
      </biological_entity>
      <relation from="o4189" id="r1143" name="ending in" negation="false" src="d0_s3" to="o4190" />
    </statement>
    <statement id="d0_s4">
      <text>margins narrowly reflexed, denticulate to distinctly serrate in distal 3/4;</text>
      <biological_entity id="o4191" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="orientation" src="d0_s4" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" constraint="in distal 3/4" constraintid="o4192" from="denticulate" name="shape" src="d0_s4" to="distinctly serrate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4192" name="3/4" name_original="3/4" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>abaxial lamellae well-developed, on the back of both costa and lamina, the lamina with 3–4 regularly spaced lamellae on either side of the costa, 1–3 cells high, serrate like the leaf margins;</text>
      <biological_entity constraint="abaxial" id="o4193" name="lamella" name_original="lamellae" src="d0_s5" type="structure">
        <character is_modifier="false" name="development" src="d0_s5" value="well-developed" value_original="well-developed" />
      </biological_entity>
      <biological_entity id="o4194" name="back" name_original="back" src="d0_s5" type="structure" />
      <biological_entity id="o4195" name="costa" name_original="costa" src="d0_s5" type="structure" />
      <biological_entity id="o4196" name="lamina" name_original="lamina" src="d0_s5" type="structure" />
      <biological_entity id="o4197" name="lamina" name_original="lamina" src="d0_s5" type="structure" />
      <biological_entity id="o4198" name="lamella" name_original="lamellae" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="4" />
        <character is_modifier="true" modifier="regularly" name="arrangement" src="d0_s5" value="spaced" value_original="spaced" />
      </biological_entity>
      <biological_entity id="o4199" name="side" name_original="side" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
      <biological_entity id="o4200" name="costa" name_original="costa" src="d0_s5" type="structure" />
      <biological_entity id="o4201" name="bud" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="height" src="d0_s5" value="high" value_original="high" />
        <character constraint="like leaf margins" constraintid="o4202" is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o4202" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <relation from="o4193" id="r1144" name="on" negation="false" src="d0_s5" to="o4194" />
      <relation from="o4194" id="r1145" name="part_of" negation="false" src="d0_s5" to="o4195" />
      <relation from="o4194" id="r1146" name="part_of" negation="false" src="d0_s5" to="o4196" />
      <relation from="o4197" id="r1147" name="with" negation="false" src="d0_s5" to="o4198" />
      <relation from="o4198" id="r1148" name="on" negation="false" src="d0_s5" to="o4199" />
      <relation from="o4199" id="r1149" name="part_of" negation="false" src="d0_s5" to="o4200" />
    </statement>
    <statement id="d0_s6">
      <text>adaxial lamellae 5–7, undulate, 5–9 cells high, confined to the midrib, undulate, serrate like the leaf margins;</text>
      <biological_entity constraint="adaxial" id="o4203" name="lamella" name_original="lamellae" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="7" />
        <character is_modifier="false" name="shape" src="d0_s6" value="undulate" value_original="undulate" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="9" />
      </biological_entity>
      <biological_entity id="o4204" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="height" src="d0_s6" value="high" value_original="high" />
      </biological_entity>
      <biological_entity id="o4205" name="midrib" name_original="midrib" src="d0_s6" type="structure" />
      <biological_entity constraint="leaf" id="o4206" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="undulate" value_original="undulate" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
        <character is_modifier="true" name="shape" src="d0_s6" value="like" value_original="like" />
      </biological_entity>
      <relation from="o4204" id="r1150" name="confined to the" negation="false" src="d0_s6" to="o4205" />
      <relation from="o4204" id="r1151" name="confined to the" negation="false" src="d0_s6" to="o4206" />
    </statement>
    <statement id="d0_s7">
      <text>cells of leaf base short-rectangular;</text>
      <biological_entity id="o4207" name="bud" name_original="cells" src="d0_s7" type="structure" constraint="base" constraint_original="base; base">
        <character is_modifier="false" name="shape" src="d0_s7" value="short-rectangular" value_original="short-rectangular" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o4208" name="base" name_original="base" src="d0_s7" type="structure" />
      <relation from="o4207" id="r1152" name="part_of" negation="false" src="d0_s7" to="o4208" />
    </statement>
    <statement id="d0_s8">
      <text>median cells of lamina roundedquadrate, 12.5–20 µm wide, in ± regular longitudinal rows;</text>
      <biological_entity constraint="lamina" id="o4209" name="bud" name_original="cells" src="d0_s8" type="structure" constraint_original="lamina median; lamina">
        <character is_modifier="false" name="shape" src="d0_s8" value="roundedquadrate" value_original="roundedquadrate" />
        <character char_type="range_value" from="12.5" from_unit="um" name="width" src="d0_s8" to="20" to_unit="um" />
      </biological_entity>
      <biological_entity id="o4210" name="lamina" name_original="lamina" src="d0_s8" type="structure" />
      <biological_entity id="o4211" name="row" name_original="rows" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s8" value="regular" value_original="regular" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s8" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <relation from="o4209" id="r1153" name="part_of" negation="false" src="d0_s8" to="o4210" />
      <relation from="o4209" id="r1154" name="in" negation="false" src="d0_s8" to="o4211" />
    </statement>
    <statement id="d0_s9">
      <text>perigonial leaves broadly-triangular-ovate, forming a large, conspicuous rosette;</text>
      <biological_entity constraint="perigonial" id="o4212" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="broadly-triangular-ovate" value_original="broadly-triangular-ovate" />
      </biological_entity>
      <biological_entity id="o4213" name="rosette" name_original="rosette" src="d0_s9" type="structure">
        <character is_modifier="true" name="size" src="d0_s9" value="large" value_original="large" />
        <character is_modifier="true" name="prominence" src="d0_s9" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <relation from="o4212" id="r1155" name="forming a" negation="false" src="d0_s9" to="o4213" />
    </statement>
    <statement id="d0_s10">
      <text>perichaetial leaves larger and broader than the vegetative leaves, erect when dry, subsquarrose when moist.</text>
      <biological_entity constraint="perichaetial" id="o4214" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="larger" value_original="larger" />
        <character constraint="than the vegetative leaves" constraintid="o4215" is_modifier="false" name="width" src="d0_s10" value="larger and broader" value_original="larger and broader" />
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when moist" name="orientation_or_pubescence" src="d0_s10" value="subsquarrose" value_original="subsquarrose" />
      </biological_entity>
      <biological_entity id="o4215" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="vegetative" value_original="vegetative" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seta 3–4 cm, reddish yellow, wiry, often strongly twisted in distal half when dry.</text>
      <biological_entity id="o4216" name="seta" name_original="seta" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s11" to="4" to_unit="cm" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="reddish yellow" value_original="reddish yellow" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="wiry" value_original="wiry" />
        <character constraint="in distal half" constraintid="o4217" is_modifier="false" modifier="often strongly" name="architecture" src="d0_s11" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4217" name="half" name_original="half" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Capsule 4–5 mm, cylindric, rather pale, terete or with 4 or more longitudinal ridges;</text>
      <biological_entity id="o4218" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="rather" name="coloration" src="d0_s12" value="pale" value_original="pale" />
        <character is_modifier="false" name="shape" src="d0_s12" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s12" value="with 4 or morelongitudinal ridges" />
      </biological_entity>
      <biological_entity id="o4219" name="ridge" name_original="ridges" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="4" value_original="4" />
      </biological_entity>
      <relation from="o4218" id="r1156" name="with" negation="false" src="d0_s12" to="o4219" />
    </statement>
    <statement id="d0_s13">
      <text>hypophysis with numerous superficial stomata;</text>
      <biological_entity id="o4220" name="hypophysis" name_original="hypophysis" src="d0_s13" type="structure" />
      <biological_entity constraint="superficial" id="o4221" name="stoma" name_original="stomata" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="numerous" value_original="numerous" />
      </biological_entity>
      <relation from="o4220" id="r1157" name="with" negation="false" src="d0_s13" to="o4221" />
    </statement>
    <statement id="d0_s14">
      <text>peristome teeth 32, pale, double.</text>
      <biological_entity constraint="peristome" id="o4222" name="tooth" name_original="teeth" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="32" value_original="32" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pale" value_original="pale" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Spores 10.5–13 µm.</text>
      <biological_entity id="o4223" name="spore" name_original="spores" src="d0_s15" type="structure">
        <character char_type="range_value" from="10.5" from_unit="um" name="some_measurement" src="d0_s15" to="13" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist sandy or clay banks along roadsides, humus and upturned tree roots in rainforest</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" modifier="moist" constraint="along roadsides , humus and upturned tree roots in rainforest" />
        <character name="habitat" value="clay banks" constraint="along roadsides , humus and upturned tree roots in rainforest" />
        <character name="habitat" value="roadsides" modifier="along" />
        <character name="habitat" value="humus" />
        <character name="habitat" value="upturned tree roots" constraint="in rainforest" />
        <character name="habitat" value="rainforest" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (50-1000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="50" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Idaho, Mont., Oreg., Wash.; Mexico; Central America (Costa Rica); West Indies (Dominican Republic, Jamaica); Asia (Japan, China in Taiwan, Philippines).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Costa Rica)" establishment_means="native" />
        <character name="distribution" value="West Indies (Dominican Republic)" establishment_means="native" />
        <character name="distribution" value="West Indies (Jamaica)" establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Asia (China in Taiwan)" establishment_means="native" />
        <character name="distribution" value="Asia (Philippines)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Reports from mainland China and the Himalayas (Bhutan) require confirmation.</discussion>
  <discussion>A North Pacific radiant species, Oligotrichum aligerum occurs on both sides of the north Pacific, extending southward to Central America and to Taiwan and the Philippines. The development of abaxial lamellae on the leaves in this species is striking and apparent, often even at low magnification.</discussion>
  
</bio:treatment>