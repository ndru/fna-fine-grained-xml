<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="treatment_page">613</other_info_on_meta>
    <other_info_on_meta type="illustration_page">614</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="Juratzka" date="1882" rank="genus">crossidium</taxon_name>
    <taxon_name authority="(De Notaris) Juratzka" date="1882" rank="species">crassinervium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">crassinervium</taxon_name>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus crossidium;species crassinervium;variety crassinervium</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075623</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crossidium</taxon_name>
    <taxon_name authority="Holzinger &amp; E. B. Bartram" date="unknown" rank="species">desertorum</taxon_name>
    <taxon_hierarchy>genus Crossidium;species desertorum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crossidium</taxon_name>
    <taxon_name authority="Holzinger &amp; E. B. Bartram" date="unknown" rank="species">erosum</taxon_name>
    <taxon_hierarchy>genus Crossidium;species erosum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1–5 mm.</text>
      <biological_entity id="o5617" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s0" to="5" to_unit="mm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves lanceolate, lingulate to lingulate-ovate, 0.5–1.3 mm, margins recurved to revolute from near apex to near base, apex obtuse, rounded or emarginate, piliferous;</text>
      <biological_entity id="o5618" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="lingulate" name="shape" src="d0_s1" to="lingulate-ovate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s1" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5619" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="recurved" value_original="recurved" />
        <character constraint="from apex" constraintid="o5620" is_modifier="false" name="shape_or_vernation" src="d0_s1" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o5620" name="apex" name_original="apex" src="d0_s1" type="structure" />
      <biological_entity id="o5621" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o5622" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s1" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="piliferous" value_original="piliferous" />
      </biological_entity>
      <relation from="o5620" id="r1304" name="to" negation="false" src="d0_s1" to="o5621" />
    </statement>
    <statement id="d0_s2">
      <text>costa excurrent, with an abaxial epidermis, filaments 2–12 cells, cells cylindric, thin-walled, with 2–4 hollow papillae on apical cell, terminal cell cylindric or conic;</text>
      <biological_entity id="o5623" name="costa" name_original="costa" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o5624" name="epidermis" name_original="epidermis" src="d0_s2" type="structure" />
      <biological_entity id="o5625" name="filament" name_original="filaments" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="12" />
      </biological_entity>
      <biological_entity id="o5626" name="cell" name_original="cells" src="d0_s2" type="structure" />
      <biological_entity id="o5627" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <biological_entity id="o5628" name="papilla" name_original="papillae" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s2" to="4" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="hollow" value_original="hollow" />
      </biological_entity>
      <biological_entity constraint="apical" id="o5629" name="cell" name_original="cell" src="d0_s2" type="structure" />
      <biological_entity constraint="terminal" id="o5630" name="cell" name_original="cell" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s2" value="conic" value_original="conic" />
      </biological_entity>
      <relation from="o5623" id="r1305" name="with" negation="false" src="d0_s2" to="o5624" />
      <relation from="o5627" id="r1306" name="with" negation="false" src="d0_s2" to="o5628" />
      <relation from="o5628" id="r1307" name="on" negation="false" src="d0_s2" to="o5629" />
    </statement>
    <statement id="d0_s3">
      <text>cells of leaf base 7–58 µm, medial and distal cells 7–22 µm, smooth.</text>
      <biological_entity id="o5631" name="cell" name_original="cells" src="d0_s3" type="structure" constraint="base" constraint_original="base; base">
        <character char_type="range_value" from="7" from_unit="um" name="some_measurement" src="d0_s3" to="58" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o5632" name="base" name_original="base" src="d0_s3" type="structure" />
      <relation from="o5631" id="r1308" name="part_of" negation="false" src="d0_s3" to="o5632" />
    </statement>
    <statement id="d0_s4">
      <text>Sexual condition dioicous or cladautoicous.</text>
      <biological_entity constraint="medial and distal" id="o5633" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character char_type="range_value" from="7" from_unit="um" name="some_measurement" src="d0_s3" to="22" to_unit="um" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reproduction" src="d0_s4" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s4" value="dioicous" value_original="dioicous" />
        <character is_modifier="false" name="reproduction" src="d0_s4" value="cladautoicous" value_original="cladautoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Seta 6–13 mm.</text>
      <biological_entity id="o5634" name="seta" name_original="seta" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsule urn ovoid-cylindric, 0.9–2.2 mm;</text>
      <biological_entity constraint="capsule" id="o5635" name="urn" name_original="urn" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovoid-cylindric" value_original="ovoid-cylindric" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s6" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>operculum 0.6–1 mm;</text>
      <biological_entity id="o5636" name="operculum" name_original="operculum" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peristome strongly twisted, 400–900 µm. Spores spheric, nearly smooth to conspicuously papillose, 9–15 µm.</text>
      <biological_entity id="o5637" name="peristome" name_original="peristome" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s8" value="twisted" value_original="twisted" />
        <character char_type="range_value" from="400" from_unit="um" name="some_measurement" src="d0_s8" to="900" to_unit="um" />
      </biological_entity>
      <biological_entity id="o5638" name="spore" name_original="spores" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="spheric" value_original="spheric" />
        <character char_type="range_value" from="nearly smooth" name="relief" src="d0_s8" to="conspicuously papillose" />
        <character char_type="range_value" from="9" from_unit="um" name="some_measurement" src="d0_s8" to="15" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsule mature Dec–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Jul" from="Dec" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil and rocks, on banks and dry washes, under shrubs in desert areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" constraint="on banks and dry washes" />
        <character name="habitat" value="rocks" constraint="on banks and dry washes" />
        <character name="habitat" value="banks" />
        <character name="habitat" value="dry washes" />
        <character name="habitat" value="shrubs" constraint="in desert areas" />
        <character name="habitat" value="desert areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (500-1700 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="500" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex., Utah; Mexico; Europe; Asia (China, India); n Africa; Atlantic Islands (Canary Islands).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="Asia (India)" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Canary Islands)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3a.</number>
  <discussion>Crossidium crassinervium is characterized by the smooth-walled leaf cells and long, thin-walled filaments with cylindric to conic terminal cells. Papillae are present on the apex of the terminal cell and, occasionally, on the distal leaf cells.</discussion>
  
</bio:treatment>