<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">377</other_info_on_meta>
    <other_info_on_meta type="mention_page">378</other_info_on_meta>
    <other_info_on_meta type="mention_page">382</other_info_on_meta>
    <other_info_on_meta type="treatment_page">379</other_info_on_meta>
    <other_info_on_meta type="illustration_page">379</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper in W. P. Schimper" date="1856" rank="genus">cynodontium</taxon_name>
    <taxon_name authority="(Schimper) Stirton" date="1906" rank="species">jenneri</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Scott. Nat. Hist.</publication_title>
      <place_in_publication>15(58): 106. 1906,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus cynodontium;species jenneri</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075585</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Didymodon</taxon_name>
    <taxon_name authority="Schimper" date="unknown" rank="species">jenneri</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Bot. Soc. Edinburgh</publication_title>
      <place_in_publication>9: 314, fig. 5. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Didymodon;species jenneri;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cynodontium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">polycarpum</taxon_name>
    <taxon_name authority="(Schimper) Stirton" date="unknown" rank="variety">laxirete</taxon_name>
    <taxon_hierarchy>genus Cynodontium;species polycarpum;variety laxirete;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems to 5 cm.</text>
      <biological_entity id="o6194" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves to 6 mm, narrowly lanceolate, long-acuminate to aciculose;</text>
      <biological_entity id="o6195" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="to 6 mm; narrowly" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>leaf margins plane or some leaves shortly recurved at mid leaf, never to the apex, 1-stratose with patches of 2-stratose cells on one or both margins;</text>
      <biological_entity constraint="leaf" id="o6196" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="plane" value_original="plane" />
        <character name="shape" src="d0_s2" value="some" value_original="some" />
      </biological_entity>
      <biological_entity id="o6197" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="at mid leaf" constraintid="o6198" is_modifier="false" modifier="shortly" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character constraint="with patches" constraintid="o6200" is_modifier="false" name="architecture" notes="" src="d0_s2" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <biological_entity constraint="mid" id="o6198" name="leaf" name_original="leaf" src="d0_s2" type="structure" />
      <biological_entity id="o6199" name="apex" name_original="apex" src="d0_s2" type="structure" />
      <biological_entity id="o6200" name="patch" name_original="patches" src="d0_s2" type="structure" />
      <biological_entity id="o6201" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity id="o6202" name="margin" name_original="margins" src="d0_s2" type="structure" />
      <relation from="o6197" id="r1483" modifier="never" name="to" negation="false" src="d0_s2" to="o6199" />
      <relation from="o6200" id="r1484" name="part_of" negation="false" src="d0_s2" to="o6201" />
      <relation from="o6200" id="r1485" name="on" negation="false" src="d0_s2" to="o6202" />
    </statement>
    <statement id="d0_s3">
      <text>cells of lamina essentially smooth with scattered low papilla, nearly smooth in apical region, thin-walled, often lax, angular, distal cells 12–14 (–20) µm wide.</text>
      <biological_entity id="o6203" name="cell" name_original="cells" src="d0_s3" type="structure" constraint="lamina" constraint_original="lamina; lamina">
        <character constraint="with papilla" constraintid="o6205" is_modifier="false" modifier="essentially" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character constraint="in apical region" constraintid="o6206" is_modifier="false" modifier="nearly" name="architecture_or_pubescence_or_relief" notes="" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="thin-walled" value_original="thin-walled" />
        <character is_modifier="false" modifier="often" name="architecture_or_arrangement" src="d0_s3" value="lax" value_original="lax" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s3" value="angular" value_original="angular" />
      </biological_entity>
      <biological_entity id="o6204" name="lamina" name_original="lamina" src="d0_s3" type="structure" />
      <biological_entity id="o6205" name="papilla" name_original="papilla" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="position" src="d0_s3" value="low" value_original="low" />
      </biological_entity>
      <biological_entity constraint="apical" id="o6206" name="region" name_original="region" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o6207" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="um" name="width" src="d0_s3" to="20" to_unit="um" />
        <character char_type="range_value" from="12" from_unit="um" name="width" src="d0_s3" to="14" to_unit="um" />
      </biological_entity>
      <relation from="o6203" id="r1486" name="part_of" negation="false" src="d0_s3" to="o6204" />
    </statement>
    <statement id="d0_s4">
      <text>Perigonium sessile.</text>
      <biological_entity id="o6208" name="perigonium" name_original="perigonium" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Seta ca. 1 cm, straight wet or dry.</text>
      <biological_entity id="o6209" name="seta" name_original="seta" src="d0_s5" type="structure">
        <character name="some_measurement" src="d0_s5" unit="cm" value="1" value_original="1" />
        <character is_modifier="false" name="condition" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="condition" src="d0_s5" value="wet" value_original="wet" />
        <character is_modifier="false" name="condition" src="d0_s5" value="dry" value_original="dry" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsule symmetrical, erect, not strumose;</text>
      <biological_entity id="o6210" name="capsule" name_original="capsule" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="symmetrical" value_original="symmetrical" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="strumose" value_original="strumose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>annulus well developed, deciduous, of 2 rows of large, separating cells.</text>
      <biological_entity id="o6211" name="annulus" name_original="annulus" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s7" value="developed" value_original="developed" />
        <character is_modifier="false" name="duration" src="d0_s7" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity id="o6212" name="row" name_original="rows" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o6213" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="large" value_original="large" />
        <character is_modifier="true" name="arrangement" src="d0_s7" value="separating" value_original="separating" />
      </biological_entity>
      <relation from="o6211" id="r1487" name="consist_of" negation="false" src="d0_s7" to="o6212" />
      <relation from="o6212" id="r1488" name="part_of" negation="false" src="d0_s7" to="o6213" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shady rock, soil over rock, occasionally rotting wood</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shady rock" />
        <character name="habitat" value="soil" constraint="over rock" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="rotting wood" modifier="occasionally" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Nfld. and Labr. (Nfld.), Yukon; Alaska, Wash., Wyo.; Europe; Atlantic Islands (Faroes).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Faroes)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Cynodontium jenneri is a tall, luxuriant species with long, flexuose leaves of a dilute yellow color, and pellucid areolation due to the relative scarcity of cell ornamen-tation (papillae) or distortion (mammillae). This is an oceanic species of western Europe, the Pacific Northwest, and the Avalon Peninsula of Newfoundland (H. A. Crum and L. E. Anderson 1981). See discussion under 8. C. polycarpon.</discussion>
  
</bio:treatment>