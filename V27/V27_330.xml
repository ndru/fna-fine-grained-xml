<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">227</other_info_on_meta>
    <other_info_on_meta type="mention_page">230</other_info_on_meta>
    <other_info_on_meta type="mention_page">233</other_info_on_meta>
    <other_info_on_meta type="mention_page">256</other_info_on_meta>
    <other_info_on_meta type="treatment_page">241</other_info_on_meta>
    <other_info_on_meta type="illustration_page">242</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">grimmia</taxon_name>
    <taxon_name authority="(Hampe) Schimper" date="1856" rank="subgenus">guembelia</taxon_name>
    <taxon_name authority="Mielichhofer ex Hornschuch" date="1819" rank="species">atrata</taxon_name>
    <place_of_publication>
      <publication_title>Flora</publication_title>
      <place_in_publication>2: 85. 1819,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus grimmia;subgenus guembelia;species atrata</taxon_hierarchy>
    <other_info_on_name type="fna_id">200001265</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dryptodon</taxon_name>
    <taxon_name authority="(Mielichhofer ex Hornschuch) Hartman" date="unknown" rank="species">atratus</taxon_name>
    <taxon_hierarchy>genus Dryptodon;species atratus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in variable loose patches, dark green to black, frequently rust colored proximally.</text>
      <biological_entity id="o784" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="dark green" name="coloration" notes="" src="d0_s0" to="black" />
        <character is_modifier="false" modifier="frequently; proximally" name="coloration" src="d0_s0" value="rust colored" value_original="rust colored" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o785" name="patch" name_original="patches" src="d0_s0" type="structure">
        <character is_modifier="true" name="variability" src="d0_s0" value="variable" value_original="variable" />
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o784" id="r195" name="in" negation="false" src="d0_s0" to="o785" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 2–7 cm, central strand absent.</text>
      <biological_entity id="o786" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o787" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves lanceolate to ligulate, 1.5–3 × 0.3–0.6 mm, keeled, margins recurved proximally, incurved distally, tapering to a blunt cucullate apex, muticous, costal transverse-section prominent, usually semicircular;</text>
      <biological_entity id="o788" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="ligulate" value_original="ligulate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s2" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s2" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o789" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="proximally" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="distally" name="orientation" src="d0_s2" value="incurved" value_original="incurved" />
        <character constraint="to apex" constraintid="o790" is_modifier="false" name="shape" src="d0_s2" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="shape" notes="" src="d0_s2" value="muticous" value_original="muticous" />
      </biological_entity>
      <biological_entity id="o790" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="blunt" value_original="blunt" />
        <character is_modifier="true" name="shape" src="d0_s2" value="cucullate" value_original="cucullate" />
      </biological_entity>
      <biological_entity constraint="costal" id="o791" name="transverse-section" name_original="transverse-section" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="prominent" value_original="prominent" />
        <character is_modifier="false" modifier="usually" name="arrangement_or_shape" src="d0_s2" value="semicircular" value_original="semicircular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal juxtacostal laminal cells rectangular, straight to slightly sinuose walled;</text>
      <biological_entity constraint="basal" id="o792" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="laminal" id="o793" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s3" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="walled" value_original="walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal marginal laminal cells in 1–3 rows quadrate, hyaline with straight to slightly sinuose, thick transverse walls;</text>
      <biological_entity constraint="basal marginal laminal" id="o794" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character constraint="with walls" constraintid="o796" is_modifier="false" name="coloration" notes="" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o795" name="row" name_original="rows" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s4" to="3" />
        <character is_modifier="false" name="shape" src="d0_s4" value="quadrate" value_original="quadrate" />
      </biological_entity>
      <biological_entity id="o796" name="wall" name_original="walls" src="d0_s4" type="structure">
        <character is_modifier="true" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="true" modifier="slightly" name="architecture" src="d0_s4" value="sinuose" value_original="sinuose" />
        <character is_modifier="true" name="width" src="d0_s4" value="thick" value_original="thick" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s4" value="transverse" value_original="transverse" />
      </biological_entity>
      <relation from="o794" id="r196" name="in" negation="false" src="d0_s4" to="o795" />
    </statement>
    <statement id="d0_s5">
      <text>medial laminal cells rectangular, nearly straight to sinuose or nodulose, thick-walled;</text>
      <biological_entity constraint="medial laminal" id="o797" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" modifier="nearly" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" name="shape" src="d0_s5" value="nodulose" value_original="nodulose" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal laminal cells 1-stratose with 2-stratose ridges, to completely 2-stratose.</text>
      <biological_entity id="o799" name="ridge" name_original="ridges" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition dioicous, perichaetial leaves not enlarged.</text>
      <biological_entity constraint="distal laminal" id="o798" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character constraint="with ridges" constraintid="o799" is_modifier="false" name="architecture" src="d0_s6" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" modifier="completely" name="architecture" notes="" src="d0_s6" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
      </biological_entity>
      <biological_entity id="o800" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="dioicous" value_original="dioicous" />
      </biological_entity>
      <biological_entity constraint="perichaetial" id="o801" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s7" value="enlarged" value_original="enlarged" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seta straight, 2–6 mm.</text>
      <biological_entity id="o802" name="seta" name_original="seta" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule occasionally present, long-exserted, yellowbrown, oblong to cylindric, exothecial cells rectangular, thick-walled, annulus of 3–4 rows of rectangular, thick-walled cells, stomata present, operculum conic to rostrate, peristome present, fully-developed, perforated and split distally, weakly papillose.</text>
      <biological_entity id="o803" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="occasionally" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="position" src="d0_s9" value="long-exserted" value_original="long-exserted" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellowbrown" value_original="yellowbrown" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s9" to="cylindric" />
      </biological_entity>
      <biological_entity constraint="exothecial" id="o804" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
      <biological_entity id="o805" name="annulus" name_original="annulus" src="d0_s9" type="structure" />
      <biological_entity id="o806" name="row" name_original="rows" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s9" to="4" />
      </biological_entity>
      <biological_entity id="o807" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="rectangular" value_original="rectangular" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
      <biological_entity id="o808" name="stoma" name_original="stomata" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o809" name="operculum" name_original="operculum" src="d0_s9" type="structure">
        <character char_type="range_value" from="conic" name="shape" src="d0_s9" to="rostrate" />
      </biological_entity>
      <biological_entity id="o810" name="peristome" name_original="peristome" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="development" src="d0_s9" value="fully-developed" value_original="fully-developed" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="perforated" value_original="perforated" />
      </biological_entity>
      <biological_entity id="o811" name="split" name_original="split" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="weakly" name="relief" src="d0_s9" value="papillose" value_original="papillose" />
      </biological_entity>
      <relation from="o805" id="r197" name="consist_of" negation="false" src="d0_s9" to="o806" />
      <relation from="o806" id="r198" name="part_of" negation="false" src="d0_s9" to="o807" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp, heavy-metal-bearing rock from the lowlands to the alpine</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="heavy-metal-bearing rock" constraint="from the lowlands to the alpine" />
        <character name="habitat" value="the lowlands" />
        <character name="habitat" value="the alpine" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (1100-2600 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="1100" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Nfld. and Labr. (Labr.), Yukon; South America (Bolivia); Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" value="South America (Bolivia)" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Grimmia atrata is rare in North America, being known only from three widely scattered areas. It is known to geologists as one of the “copper-mosses,” i.e., it is an indicator of heavy-metal-bearing rock. Because it prefers damp gneiss and mica schists, the tufts are often orange inside on account of the presence of heavy-metal oxides. The placement of G. atrata has been problematic. Because of the curved distal leaves and the absence of awns, it does not immediately appear to be a Grimmia. As a result it has previously been placed in a separate genus, Dryptodon, intermediate between Grimmia and Racomitrium. Following Cao T. and D. H. Vitt (1986b), Hastings placed it in subg. Guembelia based on its thick, keeled leaves, long, straight setae, and smooth capsules. With its recurved margin, rectangular, thick-walled, sometimes sinuous basal laminal cells, prominent annulus, and mitrate calyptra, it would seem closest to the group including G. longirostris and G. pilifera. However, its large size and muticous, cucullate leaves, which are often ligulate, coupled with its preference for moist habitats, readily separate this species from other members of this group. Sterile specimens with 1-stratose laminae may be confused as belonging to the subg. Rhabdogrimmia. In densely shaded habitats, it grows in loose patches and the areolation shows a near absence of sinuosity. On dry rock, however, the plants have extremely thick, nodulose cell walls that place the species firmly in Grimmia.</discussion>
  
</bio:treatment>