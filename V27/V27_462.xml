<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">332</other_info_on_meta>
    <other_info_on_meta type="mention_page">333</other_info_on_meta>
    <other_info_on_meta type="mention_page">334</other_info_on_meta>
    <other_info_on_meta type="mention_page">335</other_info_on_meta>
    <other_info_on_meta type="mention_page">339</other_info_on_meta>
    <other_info_on_meta type="treatment_page">337</other_info_on_meta>
    <other_info_on_meta type="illustration_page">338</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">fissidentaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">fissidens</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="species">asplenioides</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>156. 1801,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fissidentaceae;genus fissidens;species asplenioides</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075422</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 250 × 4 mm.</text>
      <biological_entity id="o6786" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s0" to="250" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s0" to="4" to_unit="mm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stem unbranched and sparingly branched;</text>
      <biological_entity id="o6787" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="sparingly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>axillary hyaline nodules weak;</text>
      <biological_entity constraint="axillary" id="o6788" name="nodule" name_original="nodules" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="fragility" src="d0_s2" value="weak" value_original="weak" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>central strand present.</text>
      <biological_entity constraint="central" id="o6789" name="strand" name_original="strand" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves in as many as 25 pairs, often undulate, mostly lingulate, rounded to obtuse to broadly acute, sometimes apiculate, to 4 × 0.5 mm;</text>
      <biological_entity id="o6790" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="shape" notes="" src="d0_s4" value="undulate" value_original="undulate" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s4" value="lingulate" value_original="lingulate" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="obtuse" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s4" value="apiculate" value_original="apiculate" />
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s4" to="4" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s4" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6791" name="pair" name_original="pairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="25" value_original="25" />
      </biological_entity>
      <relation from="o6790" id="r1614" name="as many as" negation="false" src="d0_s4" to="o6791" />
    </statement>
    <statement id="d0_s5">
      <text>dorsal lamina narrowed proximally, ending before or at insertion, not decurrent;</text>
      <biological_entity constraint="dorsal" id="o6792" name="lamina" name_original="lamina" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s5" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>vaginant laminae 1/2–3/4 the leaf length, unequal, minor lamina of most leaves rounded and free distally, or narrowed distally and ending on or near costa;</text>
      <biological_entity constraint="vaginant" id="o6793" name="lamina" name_original="laminae" src="d0_s6" type="structure">
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s6" to="3/4" />
      </biological_entity>
      <biological_entity id="o6794" name="leaf" name_original="leaf" src="d0_s6" type="structure">
        <character is_modifier="false" name="length" src="d0_s6" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity constraint="minor" id="o6795" name="lamina" name_original="lamina" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="distally" src="d0_s6" value="free" value_original="free" />
        <character is_modifier="false" modifier="distally" name="distally" src="d0_s6" value="," value_original="," />
        <character is_modifier="false" name="distally" src="d0_s6" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o6796" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o6797" name="costa" name_original="costa" src="d0_s6" type="structure" />
      <relation from="o6795" id="r1615" name="part_of" negation="false" src="d0_s6" to="o6796" />
      <relation from="o6795" id="r1616" name="ending on" negation="false" src="d0_s6" to="o6797" />
    </statement>
    <statement id="d0_s7">
      <text>margin ± entire to crenulate-serrulate, sometimes unevenly so distally, elimbate except for a weak limbidium in the proximal parts of vaginant laminae, limbidial cells 1-stratose;</text>
      <biological_entity id="o6798" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s7" value="entire to crenulate-serrulate" value_original="entire to crenulate-serrulate" />
        <character constraint="except-for limbidium" constraintid="o6799" is_modifier="false" modifier="sometimes unevenly; unevenly; distally" name="architecture" src="d0_s7" value="elimbate" value_original="elimbate" />
      </biological_entity>
      <biological_entity id="o6799" name="limbidium" name_original="limbidium" src="d0_s7" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s7" value="weak" value_original="weak" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6800" name="part" name_original="parts" src="d0_s7" type="structure" />
      <biological_entity constraint="vaginant" id="o6801" name="lamina" name_original="laminae" src="d0_s7" type="structure" />
      <biological_entity id="o6802" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="limbidial" value_original="limbidial" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <relation from="o6799" id="r1617" name="in" negation="false" src="d0_s7" to="o6800" />
      <relation from="o6800" id="r1618" name="part_of" negation="false" src="d0_s7" to="o6801" />
    </statement>
    <statement id="d0_s8">
      <text>costa ending several cells before apex, oblongifolius-type, distal part of leaf in transverse-section showing enlarged cells arranged in a single row;</text>
      <biological_entity id="o6803" name="costa" name_original="costa" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s8" value="oblongifolius-type" value_original="oblongifolius-type" />
      </biological_entity>
      <biological_entity id="o6804" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="several" value_original="several" />
      </biological_entity>
      <biological_entity id="o6805" name="apex" name_original="apex" src="d0_s8" type="structure" />
      <biological_entity constraint="distal" id="o6806" name="part" name_original="part" src="d0_s8" type="structure">
        <character constraint="in row" constraintid="o6810" is_modifier="false" name="arrangement" src="d0_s8" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o6807" name="leaf" name_original="leaf" src="d0_s8" type="structure" />
      <biological_entity id="o6808" name="transverse-section" name_original="transverse-section" src="d0_s8" type="structure" />
      <biological_entity id="o6809" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="true" name="size" src="d0_s8" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity id="o6810" name="row" name_original="row" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="single" value_original="single" />
      </biological_entity>
      <relation from="o6803" id="r1619" name="ending" negation="false" src="d0_s8" to="o6804" />
      <relation from="o6803" id="r1620" name="before" negation="false" src="d0_s8" to="o6805" />
      <relation from="o6806" id="r1621" name="part_of" negation="false" src="d0_s8" to="o6807" />
      <relation from="o6806" id="r1622" name="in" negation="false" src="d0_s8" to="o6808" />
      <relation from="o6808" id="r1623" name="showing" negation="false" src="d0_s8" to="o6809" />
    </statement>
    <statement id="d0_s9">
      <text>laminal cells of dorsal and ventral laminae 1-stratose, distinct, smooth, lenticularly thickened but appearing bulging, firm-walled, irregularly hexagonal, 7–12 µm long;</text>
      <biological_entity constraint="dorsal and ventral" id="o6812" name="lamina" name_original="laminae" src="d0_s9" type="structure" />
      <relation from="o6811" id="r1624" name="part_of" negation="false" src="d0_s9" to="o6812" />
    </statement>
    <statement id="d0_s10">
      <text>juxtacostal and interior proximal cells of vaginant laminae 1-stratose, smooth, plane, quadrate to ± oblong, larger;</text>
      <biological_entity constraint="lamina" id="o6811" name="cell" name_original="cells" src="d0_s9" type="structure" constraint_original="laminal lamina; lamina">
        <character is_modifier="false" name="architecture" src="d0_s9" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="lenticularly" name="size_or_width" src="d0_s9" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="pubescence_or_shape" src="d0_s9" value="bulging" value_original="bulging" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="firm-walled" value_original="firm-walled" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s9" value="hexagonal" value_original="hexagonal" />
        <character char_type="range_value" from="7" from_unit="um" name="length" src="d0_s9" to="12" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="interior proximal" id="o6813" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s10" value="plane" value_original="plane" />
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s10" to="more or less oblong" />
        <character is_modifier="false" name="size" src="d0_s10" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity constraint="vaginant" id="o6814" name="lamina" name_original="laminae" src="d0_s10" type="structure" />
      <relation from="o6813" id="r1625" name="part_of" negation="false" src="d0_s10" to="o6814" />
    </statement>
    <statement id="d0_s11">
      <text>medial marginal cells of vaginant laminae ± elongate, oriented obliquely.</text>
      <biological_entity constraint="vaginant" id="o6816" name="lamina" name_original="laminae" src="d0_s11" type="structure" />
      <relation from="o6815" id="r1626" name="part_of" negation="false" src="d0_s11" to="o6816" />
    </statement>
    <statement id="d0_s12">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="medial marginal" id="o6815" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="obliquely" name="orientation" src="d0_s11" value="oriented" value_original="oriented" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>perigonia and perichaetia on elongate stems;</text>
      <biological_entity id="o6817" name="perigonium" name_original="perigonia" src="d0_s13" type="structure" />
      <biological_entity id="o6818" name="perichaetium" name_original="perichaetia" src="d0_s13" type="structure" />
      <biological_entity id="o6819" name="stem" name_original="stems" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="elongate" value_original="elongate" />
      </biological_entity>
      <relation from="o6817" id="r1627" name="on" negation="false" src="d0_s13" to="o6819" />
      <relation from="o6818" id="r1628" name="on" negation="false" src="d0_s13" to="o6819" />
    </statement>
    <statement id="d0_s14">
      <text>naked archegonia at times in axils of distal leaves.</text>
      <biological_entity id="o6820" name="archegonium" name_original="archegonia" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity id="o6821" name="axil" name_original="axils" src="d0_s14" type="structure" />
      <biological_entity constraint="distal" id="o6822" name="leaf" name_original="leaves" src="d0_s14" type="structure" />
      <relation from="o6820" id="r1629" name="in" negation="false" src="d0_s14" to="o6821" />
      <relation from="o6821" id="r1630" name="part_of" negation="false" src="d0_s14" to="o6822" />
    </statement>
    <statement id="d0_s15">
      <text>Sporophytes 1–2 per perichaetium, orange-red.</text>
      <biological_entity id="o6823" name="sporophyte" name_original="sporophytes" src="d0_s15" type="structure">
        <character char_type="range_value" constraint="per perichaetium" constraintid="o6824" from="1" name="quantity" src="d0_s15" to="2" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s15" value="orange-red" value_original="orange-red" />
      </biological_entity>
      <biological_entity id="o6824" name="perichaetium" name_original="perichaetium" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Seta to 6 mm.</text>
      <biological_entity id="o6825" name="seta" name_original="seta" src="d0_s16" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Capsule theca ± erect, radially symmetric to slightly arcuate, bilaterally symmetric, to 1.5 mm;</text>
      <biological_entity constraint="capsule" id="o6826" name="theca" name_original="theca" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s17" value="erect" value_original="erect" />
        <character char_type="range_value" from="radially symmetric" name="shape" src="d0_s17" to="slightly arcuate" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s17" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s17" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>peristome taxifolius-type;</text>
      <biological_entity id="o6827" name="peristome" name_original="peristome" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_ref_taxa" src="d0_s18" value="taxifolius-type" value_original="taxifolius-type" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>operculum ± as long as theca.</text>
      <biological_entity id="o6828" name="operculum" name_original="operculum" src="d0_s19" type="structure" />
      <biological_entity id="o6829" name="theca" name_original="theca" src="d0_s19" type="structure" />
      <relation from="o6828" id="r1631" name="as long as" negation="false" src="d0_s19" to="o6829" />
    </statement>
    <statement id="d0_s20">
      <text>Calyptra cucullate, smooth, 1–1.7 µm. Spores 7.5–12 µm.</text>
      <biological_entity id="o6830" name="calyptra" name_original="calyptra" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s20" value="smooth" value_original="smooth" />
        <character char_type="range_value" from="1" from_unit="um" name="some_measurement" src="d0_s20" to="1.7" to_unit="um" />
      </biological_entity>
      <biological_entity id="o6831" name="spore" name_original="spores" src="d0_s20" type="structure">
        <character char_type="range_value" from="7.5" from_unit="um" name="some_measurement" src="d0_s20" to="12" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandstone ledges and crevices in moist ravines and grottoes, usually along streams and waterfalls</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ledges" modifier="sandstone" />
        <character name="habitat" value="crevices" />
        <character name="habitat" value="moist ravines" modifier="in" />
        <character name="habitat" value="grottoes" />
        <character name="habitat" value="streams" modifier="usually along" />
        <character name="habitat" value="waterfalls" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga., Ky., La., Miss., N.C., S.C.; Mexico; West Indies; Central America; South America; Asia; Africa; Atlantic Islands (Macaronesia); Pacific Islands (New Zealand); Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Macaronesia)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Fissidens asplenioides, usually a robust species, is recognized by leaves typically curled tightly inward from the tips when dry, a minor lamina that in most leaves is rounded distally and attached more or less only along costa, lenticularly thickened dorsal and ventral laminal cells, elongate medial marginal cells of the vaginant laminae oriented obliquely, and oblongifolius-type costa which in the distal part of the leaf in transverse section shows a single row of enlarged cells. The oblongifolius-type costa, found in F. asplenioides and F. santa-clarensis, is unique to sect. Amblyothallia of subg. Pachyfissidens (R. A. Pursell and M. A. Bruggeman-Nannenga 2004). Subterranean, multicellular, irregularly globose, rhizoidal gemmae have been reported in Macaronesian specimens of F. asplenioides.</discussion>
  
</bio:treatment>