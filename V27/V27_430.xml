<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">10</other_info_on_meta>
    <other_info_on_meta type="mention_page">315</other_info_on_meta>
    <other_info_on_meta type="mention_page">318</other_info_on_meta>
    <other_info_on_meta type="mention_page">463</other_info_on_meta>
    <other_info_on_meta type="treatment_page">314</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">archidiaceae</taxon_name>
    <taxon_name authority="Bridel" date="1827" rank="genus">ARCHIDIUM</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Univ.</publication_title>
      <place_in_publication>1: 747. 1827  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family archidiaceae;genus ARCHIDIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek arche, primitive form or nature, alluding to small, simple plants and cleistocarpous capsule</other_info_on_name>
    <other_info_on_name type="fna_id">102471</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants mostly green.</text>
      <biological_entity id="o4372" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="mostly" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems terete, 1–20 mm.</text>
      <biological_entity id="o4373" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s1" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves of distal portions of stems and of perichaetia ovate, ovatelanceolate, linear-lanceolate to triangular, often reduced and distant in proximal portion of stem, flat or somewhat concave, apex acute to acuminate, sometimes incurved or rarely secund, margins plane, recurved or rarely incurved, distal margins smooth to finely serrulate;</text>
      <biological_entity id="o4374" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o4375" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character is_modifier="true" modifier="rarely" name="orientation" src="d0_s2" value="incurved" value_original="incurved" />
        <character char_type="range_value" from="smooth" name="architecture" src="d0_s2" to="finely serrulate" />
      </biological_entity>
      <relation from="o4374" id="r995" name="of distal portions of stems and of perichaetia ovate , ovatelanceolate , linear-lanceolate to triangular , often reduced and distant in proximal portion of stem , flat or somewhat concave , apex acute to acuminate , sometimes incurved or rarely secund , margins plane ," negation="false" src="d0_s2" to="o4375" />
    </statement>
    <statement id="d0_s3">
      <text>costa weak and not reaching apex or sometimes absent, to more often strong, single, percurrent to excurrent in a hairpoint;</text>
      <biological_entity id="o4376" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s3" value="weak" value_original="weak" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="often" name="fragility" src="d0_s3" value="strong" value_original="strong" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="single" value_original="single" />
        <character char_type="range_value" constraint="in hairpoint" constraintid="o4378" from="percurrent" name="architecture" src="d0_s3" to="excurrent" />
      </biological_entity>
      <biological_entity id="o4377" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <biological_entity id="o4378" name="hairpoint" name_original="hairpoint" src="d0_s3" type="structure" />
      <relation from="o4376" id="r996" name="reaching" negation="true" src="d0_s3" to="o4377" />
    </statement>
    <statement id="d0_s4">
      <text>laminal areolation uniform to somewhat differentiated, cells mostly rectangular to rhomboidal, sometimes irregular in median and distal portions, rectangular and rhomboidal cells mixed with short subquadrate cells, alar cells similar to adjacent cells or differentiated, hyaline, wide-rectangular to quadrate.</text>
      <biological_entity constraint="laminal" id="o4379" name="areolation" name_original="areolation" src="d0_s4" type="structure">
        <character is_modifier="false" name="variability" src="d0_s4" value="uniform to somewhat" value_original="uniform to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o4380" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="mostly rectangular" name="shape" src="d0_s4" to="rhomboidal" />
        <character constraint="in median distal portions" constraintid="o4381" is_modifier="false" modifier="sometimes" name="architecture_or_course" src="d0_s4" value="irregular" value_original="irregular" />
      </biological_entity>
      <biological_entity constraint="median and distal" id="o4381" name="portion" name_original="portions" src="d0_s4" type="structure" />
      <biological_entity id="o4382" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="rectangular" value_original="rectangular" />
        <character is_modifier="true" name="shape" src="d0_s4" value="rhomboidal" value_original="rhomboidal" />
        <character constraint="with cells" constraintid="o4383" is_modifier="false" name="arrangement" src="d0_s4" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o4383" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="true" name="shape" src="d0_s4" value="subquadrate" value_original="subquadrate" />
      </biological_entity>
      <biological_entity id="o4385" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <relation from="o4384" id="r997" name="to" negation="false" src="d0_s4" to="o4385" />
    </statement>
    <statement id="d0_s5">
      <text>Sexual condition autoicous, paroicous or synoicous, antheridia often naked in axils of distal stem-leaves or outer perichaetial leaves, sometimes enclosed in short-shoots with small leaves (bracts).</text>
      <biological_entity id="o4384" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="wide-rectangular" name="shape" src="d0_s4" to="quadrate" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="sexual" value_original="sexual" />
      </biological_entity>
      <biological_entity id="o4386" name="whole-organism" name_original="" src="d0_s5" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s5" value="autoicous" value_original="autoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="paroicous" value_original="paroicous" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="synoicous" value_original="synoicous" />
      </biological_entity>
      <biological_entity id="o4387" name="antheridium" name_original="antheridia" src="d0_s5" type="structure">
        <character constraint="in axils" constraintid="o4388" is_modifier="false" modifier="often" name="architecture" src="d0_s5" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity id="o4388" name="axil" name_original="axils" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o4389" name="stem-leaf" name_original="stem-leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="outer perichaetial" id="o4390" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o4391" name="short-shoot" name_original="short-shoots" src="d0_s5" type="structure" />
      <biological_entity id="o4392" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="true" name="size" src="d0_s5" value="small" value_original="small" />
      </biological_entity>
      <relation from="o4388" id="r998" name="part_of" negation="false" src="d0_s5" to="o4389" />
      <relation from="o4388" id="r999" name="part_of" negation="false" src="d0_s5" to="o4390" />
      <relation from="o4387" id="r1000" modifier="sometimes" name="enclosed in" negation="false" src="d0_s5" to="o4391" />
      <relation from="o4387" id="r1001" name="with" negation="false" src="d0_s5" to="o4392" />
    </statement>
    <statement id="d0_s6">
      <text>Perichaetial leaves often larger and more variable in shape than stem-leaves, sometimes broadly ovate to elliptic.</text>
      <biological_entity constraint="perichaetial" id="o4393" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often" name="size" src="d0_s6" value="larger" value_original="larger" />
        <character constraint="in stem-leaves" constraintid="o4395" is_modifier="false" name="variability" src="d0_s6" value="variable" value_original="variable" />
        <character char_type="range_value" from="sometimes broadly ovate" name="shape" notes="" src="d0_s6" to="elliptic" />
      </biological_entity>
      <biological_entity id="o4395" name="stem-leaf" name_original="stem-leaves" src="d0_s6" type="structure">
        <character is_modifier="true" name="character" src="d0_s6" value="shape" value_original="shape" />
      </biological_entity>
      <biological_entity id="o4394" name="stem-leaf" name_original="stem-leaves" src="d0_s6" type="structure">
        <character is_modifier="true" name="character" src="d0_s6" value="shape" value_original="shape" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsule immersed in perichaetial leaves, brown or golden at maturity.</text>
      <biological_entity id="o4396" name="capsule" name_original="capsule" src="d0_s7" type="structure">
        <character constraint="in perichaetial, leaves" constraintid="o4397, o4398" is_modifier="false" name="prominence" src="d0_s7" value="immersed" value_original="immersed" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="brown" value_original="brown" />
        <character constraint="at maturity" is_modifier="false" name="coloration" src="d0_s7" value="golden" value_original="golden" />
      </biological_entity>
      <biological_entity id="o4397" name="perichaetial" name_original="perichaetial" src="d0_s7" type="structure" />
      <biological_entity id="o4398" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Spores spherical to polyhedral, pale-yellow to yellowbrown, golden, orange or red-orange, smooth to finely papillose.</text>
      <biological_entity id="o4399" name="spore" name_original="spores" src="d0_s8" type="structure">
        <character char_type="range_value" from="spherical" name="shape" src="d0_s8" to="polyhedral" />
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s8" to="yellowbrown golden orange or red-orange" />
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s8" to="yellowbrown golden orange or red-orange" />
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s8" to="yellowbrown golden orange or red-orange" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s8" to="finely papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide except Antarctica, in subtropical to warm-temperate regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide except Antarctica" establishment_means="native" />
        <character name="distribution" value="in subtropical to warm-temperate regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Species 35 (6 in the flora).</discussion>
  <discussion>Superficially, species of Archidium can be mistaken for Pleuridium, and many collections of the latter genus have been identified as Archidium. Both genera consist of very small mosses that typically colonize soil in open sites and have ovate-lanceolate to lanceolate leaves, which are often subulate. However, species of Pleuridium have leaves that are often strongly shouldered and long-subulate, a seta, capsules that are typically ovoid (rather than globose) with an apiculate tip, a calyptra, and smaller, more numerous spores. In the descriptions, spore diameters are given for the longest axis (following J. A. Snider 1975).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants ephemeral, minute, stems mostly smaller than 3 mm, not branched by sterile innovations; leaves oblong to lanceolate, apex acute, costa weak in perichaetial and distal leaves, not reaching apex, sometimes absent; synoicous</description>
      <determination>5 Archidium minus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants perennial, larger, stems 2-20 mm, commonly branched by sterile innovations; leaves ovate-lanceolate to ovate, apex mostly acuminate; costa strong, percurrent to excurrent; autoicous or paroicous</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Paroicous; antheridia typically naked in axes of perichaetial leaves or distal stem leaves below perichaetium, or sometimes 1-2 small bracts present; sporophytes terminal</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Autoicous; antheridia in leafy axillary buds; sporophytes terminal or lateral</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Perichaetial leaves mostly erect, plane, with serrulate distal margins, median laminal cells narrow (8-13 µm), elongate-rhomboidal, 6-10:1.</description>
      <determination>1 Archidium alternifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Perichaetial leaves often spreading, plane to recurved, with smooth distal margins, median laminal cells wide (15-35 µm), short-rhomboidal, 3-5:1.</description>
      <determination>6 Archidium tenerrimum</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Median laminal cells of distal stem and perichaetial leaves very irregular in shape, from subquadrate to short-rectangular or rhomboidal, intermixed; alar cells not much differentiated from adjacent basal cells</description>
      <determination>2 Archidium donnellii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Median laminal cells of distal stem and perichaetial leaves with uniform areolation, either rhomboidal or rectangular, alar cells, especially along margin, quadrate to short-rectangular, differentiated from adjacent basal cells</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Median cells of perichaetial leaves wide (15-28 µm), distal leaf margins of distal and perichaetial leaves recurved.</description>
      <determination>3 Archidium hallii</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Median cells of perichaetial leaves narrow (9-15 µm), distal leaf margins of distal and perichaetial leaves plane.</description>
      <determination>4 Archidium ohioense</determination>
    </key_statement>
  </key>
</bio:treatment>