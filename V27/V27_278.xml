<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Roxanne I. Hastings,Ryszard Ochyra</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">39</other_info_on_meta>
    <other_info_on_meta type="mention_page">205</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="mention_page">265</other_info_on_meta>
    <other_info_on_meta type="mention_page">266</other_info_on_meta>
    <other_info_on_meta type="mention_page">286</other_info_on_meta>
    <other_info_on_meta type="mention_page">294</other_info_on_meta>
    <other_info_on_meta type="mention_page">306</other_info_on_meta>
    <other_info_on_meta type="mention_page">615</other_info_on_meta>
    <other_info_on_meta type="treatment_page">204</other_info_on_meta>
    <other_info_on_meta type="illustrator">Patricia M. Eckel</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">GRIMMIACEAE</taxon_name>
    <taxon_hierarchy>family GRIMMIACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10385</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants acrocarpous or cladocarpous, small to large, usually olivaceous to blackish green, growing in rigid cushions, tufts, mats or patches.</text>
      <biological_entity id="o2031" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="large" />
        <character char_type="range_value" from="usually olivaceous" name="coloration" src="d0_s0" to="blackish green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o2032" name="cushion" name_original="cushions" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="rigid" value_original="rigid" />
      </biological_entity>
      <biological_entity id="o2033" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="rigid" value_original="rigid" />
      </biological_entity>
      <biological_entity id="o2034" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="rigid" value_original="rigid" />
      </biological_entity>
      <biological_entity id="o2035" name="patch" name_original="patches" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="rigid" value_original="rigid" />
      </biological_entity>
      <relation from="o2031" id="r549" name="growing in" negation="false" src="d0_s0" to="o2032" />
      <relation from="o2031" id="r550" name="growing in" negation="false" src="d0_s0" to="o2033" />
      <relation from="o2031" id="r551" name="growing in" negation="false" src="d0_s0" to="o2034" />
      <relation from="o2031" id="r552" name="growing in" negation="false" src="d0_s0" to="o2035" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, ascending, or prostrate, dichotomously to irregularly branched.</text>
      <biological_entity id="o2036" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" modifier="dichotomously to irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect and tightly appressed to crisped when dry, erect-spreading to patent when wet, lanceolate to ovatelanceolate, less often ovate, oblong-ovate, linear, or lingulate, keeled, canaliculate, to broadly concave, smooth or sometimes longitudinally plicate, rarely with adaxial lamellae (Indusiella), margins plane, incurved, or variously recurved or revolute, mostly entire, 1- to multistratose, acuminate, acute to rounded-obtuse, typically with a hyaline awn, sometimes muticous, costa single, rarely spurred or forked distally (Codriophorus and Niphotrichum), usually strong, percurrent to excurrent, rarely subpercurrent, typically with one stereid band, distal lamina 1–2 (–4) -stratose;</text>
      <biological_entity id="o2037" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="tightly" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s2" value="crisped" value_original="crisped" />
        <character char_type="range_value" from="erect-spreading" modifier="when wet" name="orientation" src="d0_s2" to="patent" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="ovatelanceolate" />
        <character is_modifier="false" modifier="less often; often" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong-ovate" value_original="oblong-ovate" />
        <character char_type="range_value" from="lingulate keeled canaliculate" name="shape" src="d0_s2" to="broadly concave" />
        <character char_type="range_value" from="lingulate keeled canaliculate" name="shape" src="d0_s2" to="broadly concave" />
        <character char_type="range_value" from="lingulate keeled canaliculate" name="shape" src="d0_s2" to="broadly concave" />
        <character char_type="range_value" from="lingulate keeled canaliculate" name="shape" src="d0_s2" to="broadly concave" />
        <character char_type="range_value" from="lingulate keeled canaliculate" name="shape" src="d0_s2" to="broadly concave" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sometimes longitudinally" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="plicate" value_original="plicate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o2038" name="lamella" name_original="lamellae" src="d0_s2" type="structure" />
      <biological_entity id="o2039" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="plane" value_original="plane" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="incurved" value_original="incurved" />
        <character is_modifier="false" modifier="variously" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="incurved" value_original="incurved" />
        <character is_modifier="false" modifier="variously" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s2" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="mostly" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character char_type="range_value" from="1" name="architecture" src="d0_s2" to="multistratose" />
        <character is_modifier="false" name="shape" src="d0_s2" value="acuminate" value_original="acuminate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="rounded-obtuse" />
        <character is_modifier="false" modifier="sometimes" name="shape" notes="" src="d0_s2" value="muticous" value_original="muticous" />
      </biological_entity>
      <biological_entity id="o2040" name="awn" name_original="awn" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o2041" name="costa" name_original="costa" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="single" value_original="single" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="spurred" value_original="spurred" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s2" value="forked" value_original="forked" />
        <character is_modifier="false" modifier="usually" name="fragility" src="d0_s2" value="strong" value_original="strong" />
        <character char_type="range_value" from="percurrent" name="architecture" src="d0_s2" to="excurrent" />
        <character is_modifier="false" modifier="rarely" name="position" src="d0_s2" value="subpercurrent" value_original="subpercurrent" />
      </biological_entity>
      <biological_entity id="o2042" name="band" name_original="band" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o2043" name="lamina" name_original="lamina" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="1-2(-4)-stratose" value_original="1-2(-4)-stratose" />
      </biological_entity>
      <relation from="o2037" id="r553" modifier="rarely" name="with" negation="false" src="d0_s2" to="o2038" />
      <relation from="o2039" id="r554" modifier="typically" name="with" negation="false" src="d0_s2" to="o2040" />
      <relation from="o2041" id="r555" modifier="typically" name="with" negation="false" src="d0_s2" to="o2042" />
    </statement>
    <statement id="d0_s3">
      <text>basal-cells quadrate to elongate, rarely oblate, straight, sinuose, or nodulose, basal juxtacostal and marginal regions usually differentiated, alar cells undifferentiated or hyaline;</text>
      <biological_entity id="o2044" name="basal-bud" name_original="basal-cells" src="d0_s3" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s3" to="elongate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="oblate" value_original="oblate" />
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" name="shape" src="d0_s3" value="nodulose" value_original="nodulose" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o2045" name="region" name_original="regions" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="variability" src="d0_s3" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o2046" name="bud" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="undifferentiated" value_original="undifferentiated" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>mid leaf cells quadrate to elongate, commonly sinuose or sinuose-nodulose, usually thick-walled.</text>
      <biological_entity constraint="leaf" id="o2047" name="bud" name_original="cells" src="d0_s4" type="structure" constraint_original="mid leaf">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s4" to="elongate" />
        <character is_modifier="false" modifier="commonly" name="architecture" src="d0_s4" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" name="shape" src="d0_s4" value="sinuose-nodulose" value_original="sinuose-nodulose" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s4" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Perichaetia terminal on tips of stems or lateral branches;</text>
      <biological_entity id="o2048" name="perichaetium" name_original="perichaetia" src="d0_s5" type="structure">
        <character constraint="on tips" constraintid="o2049" is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o2049" name="tip" name_original="tips" src="d0_s5" type="structure" />
      <biological_entity id="o2050" name="stem" name_original="stems" src="d0_s5" type="structure" />
      <biological_entity constraint="lateral" id="o2051" name="branch" name_original="branches" src="d0_s5" type="structure" />
      <relation from="o2049" id="r556" name="part_of" negation="false" src="d0_s5" to="o2050" />
      <relation from="o2049" id="r557" name="part_of" negation="false" src="d0_s5" to="o2051" />
    </statement>
    <statement id="d0_s6">
      <text>perichaetial leaves differentiated or not.</text>
      <biological_entity constraint="perichaetial" id="o2052" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="variability" src="d0_s6" value="differentiated" value_original="differentiated" />
        <character name="variability" src="d0_s6" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Seta short to long, smooth or rarely papillose.</text>
      <biological_entity id="o2053" name="seta" name_original="seta" src="d0_s7" type="structure">
        <character is_modifier="false" name="size_or_length" src="d0_s7" value="short to long" value_original="short to long" />
        <character is_modifier="false" name="relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="rarely" name="relief" src="d0_s7" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsule usually erect, usually ovoid, obloid, cylindrical or cupulate, symmetric or rarely strongly ventricose at the base and gibbous, smooth or sulcate;</text>
      <biological_entity id="o2054" name="capsule" name_original="capsule" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s8" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obloid" value_original="obloid" />
        <character is_modifier="false" name="shape" src="d0_s8" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s8" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="rarely strongly" name="shape" src="d0_s8" value="ventricose" value_original="ventricose" />
        <character is_modifier="false" name="shape" src="d0_s8" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s8" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="symmetric" value_original="symmetric" />
        <character constraint="at base" constraintid="o2055" is_modifier="false" modifier="rarely strongly" name="shape" src="d0_s8" value="ventricose" value_original="ventricose" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="gibbous" value_original="gibbous" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="sulcate" value_original="sulcate" />
      </biological_entity>
      <biological_entity id="o2055" name="base" name_original="base" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>annulus present or absent, often compound, deciduous or persistent;</text>
      <biological_entity id="o2056" name="annulus" name_original="annulus" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s9" value="compound" value_original="compound" />
        <character is_modifier="false" name="duration" src="d0_s9" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>operculum mammillate to long-rostrate, sometimes attached to the columella after dehiscence (most Schistidium);</text>
      <biological_entity id="o2057" name="operculum" name_original="operculum" src="d0_s10" type="structure">
        <character char_type="range_value" from="mammillate" name="shape" src="d0_s10" to="long-rostrate" />
        <character constraint="to columella" constraintid="o2058" is_modifier="false" modifier="sometimes" name="fixation" src="d0_s10" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o2058" name="columella" name_original="columella" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>peristome present, seldom rudimentary or absent, consisting of 16 teeth, lanceolate to linear, entire, perforated or cribrose, variously split into 2 or 3 unequal prongs or divided nearly to the base into two filiform somewhat paired segments, smooth or variously ornamented.</text>
      <biological_entity id="o2059" name="peristome" name_original="peristome" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="seldom" name="prominence" src="d0_s11" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s11" to="linear" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="perforated" value_original="perforated" />
        <character name="architecture" src="d0_s11" value="cribrose" value_original="cribrose" />
      </biological_entity>
      <biological_entity id="o2060" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="16" value_original="16" />
      </biological_entity>
      <biological_entity id="o2061" name="split" name_original="split" src="d0_s11" type="structure">
        <character is_modifier="false" name="relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="variously" name="relief" src="d0_s11" value="ornamented" value_original="ornamented" />
      </biological_entity>
      <biological_entity id="o2062" name="prong" name_original="prongs" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o2063" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o2064" name="prong" name_original="prongs" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o2065" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o2066" name="segment" name_original="segments" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s11" value="filiform" value_original="filiform" />
        <character is_modifier="true" modifier="somewhat" name="arrangement" src="d0_s11" value="paired" value_original="paired" />
      </biological_entity>
      <relation from="o2059" id="r558" name="consisting of" negation="false" src="d0_s11" to="o2060" />
      <relation from="o2061" id="r559" name="into" negation="false" src="d0_s11" to="o2062" />
      <relation from="o2061" id="r560" name="into" negation="false" src="d0_s11" to="o2063" />
      <relation from="o2063" id="r561" name="into" negation="false" src="d0_s11" to="o2064" />
      <relation from="o2063" id="r562" name="into" negation="false" src="d0_s11" to="o2065" />
      <relation from="o2065" id="r563" name="into" negation="false" src="d0_s11" to="o2066" />
    </statement>
    <statement id="d0_s12">
      <text>Calyptra small to large, covering only the operculum to half or more of the capsule, cucullate, mitrate, or mitrate-campanulate, smooth or plicate, naked, sometimes papillose, slightly to distinctly lacerated or deeply lobed at the base.</text>
      <biological_entity id="o2067" name="calyptra" name_original="calyptra" src="d0_s12" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s12" to="large" />
        <character is_modifier="false" name="shape" src="d0_s12" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="mitrate" value_original="mitrate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="mitrate-campanulate" value_original="mitrate-campanulate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="mitrate" value_original="mitrate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="mitrate-campanulate" value_original="mitrate-campanulate" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="plicate" value_original="plicate" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="naked" value_original="naked" />
        <character is_modifier="false" modifier="sometimes" name="relief" src="d0_s12" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="slightly to distinctly; distinctly" name="shape" src="d0_s12" value="lacerated" value_original="lacerated" />
        <character constraint="at base" constraintid="o2070" is_modifier="false" modifier="deeply" name="shape" src="d0_s12" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o2068" name="operculum" name_original="operculum" src="d0_s12" type="structure" />
      <biological_entity id="o2069" name="capsule" name_original="capsule" src="d0_s12" type="structure" />
      <biological_entity id="o2070" name="base" name_original="base" src="d0_s12" type="structure" />
      <relation from="o2067" id="r564" name="covering" negation="false" src="d0_s12" to="o2068" />
      <relation from="o2067" id="r565" name="part_of" negation="false" src="d0_s12" to="o2069" />
    </statement>
    <statement id="d0_s13">
      <text>Spores globose, smooth or papillose.</text>
      <biological_entity id="o2071" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="globose" value_original="globose" />
        <character is_modifier="false" name="relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s13" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <discussion>Genera ca. 11, species ca. 325 (9 genera, 109 species in the flora).</discussion>
  <discussion>Most species in the Grimmiaceae are xerophytic and colonizers of bare, usually dry and exposed rocks and stones, forming predominantly dark green to blackish cushions or tufts. However, some species occur on wet or damp rocks along watercourses and lakes or in seepage sites. They rarely inhabit soil and only a few species are epiphytes.</discussion>
  <discussion>The Grimmiaceae is classically distinguished by quadrate to short-rectangular mid leaf cells typically sinuose to nodulose and thick-walled, and leaves usually awned, often with the awns long and toothed or papillose. There is a wide range of variation. While awns are present in most species, length varies from a short, translucent apiculus to exceeding the length of the lamina. Awns can be flat or terete, smooth or denticulate, spinose or papillose, and long-decurrent or not. Similarly, mid leaf cells range from oblate to long-rectangular and sinuose to almost straight. The range of variation in these characters makes it difficult to describe this family in simple and unequivocal terms.</discussion>
  <discussion>The generic classification within the Grimmiaceae has long been a subject of controversy. In traditional treatments the family is considered to include the two largest genera, Grimmia and Racomitrium, with several peripheral, mostly mono- or oligotypic genera, including Aligrimmia R. S. Williams, Coscinodon, Coscinodontella R. S. Williams, Indusiella, Jaffueliobryum, and Leucoperichaetium Magill. A number of segregates have been split from the large and heterogeneous Grimmia, namely Dryptodon Bridel, Guembelia Hampe, Hydrogrimmia (I. Hagen) Loeske, Orthogrimmia (Schimper) Ochyra &amp; Zarnoweic, Schistidium, and Streptocolea I. Hagen. In this treatment, only Schistidium is accepted. Racomitrium has been divided into four sharply delimited genera and the group is recognized at the subfamily level.</discussion>
  <discussion>R. Ochyra et al. (2003) used two peristome types to divide Grimmiaceae into two subfamilies, Grimmioideae and Racomitrioideae. The Schistidium-type peristome defines the Grimmioideae. This peristome has lanceolate teeth that are entire or perforate and distally usually split into two or three unequal prongs that are smooth to ornamented. They have a distinctly thick and trabeculate abaxial side and a thin adaxial side. There is no basal membrane. In the Racomitrium-type peristome, which defines the Racomitrioideae, the teeth are linear and divided nearly to the base into two filiform branches. The teeth are equally thickened and less prominently trabeculate on both adaxial and abaxial sides. They usually arise from a low, basal membrane and often have a prostome.</discussion>
  <discussion>Selected references   Churchill, S. P. 1981. A phylogenetic analysis, classification and synopsis of the genera of the Grimmiaceae (Musci). Advances Cladist. 1: 127–144.  Jones, G. N. 1933. Grimmiaceae. In: A. J. Grout. Moss Flora of North America, North of Mexico.  3 vols. in 12 parts. Newfane, Vt. and New York. Vol. 2, pp. 1–60.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Acrocarpous; stems erect to ascending, with or without central strand; basal cells of lamina without spiral thickenings, insertion concolorous with distal cells or hyaline; costa in transverse section with 2 or occasionally with 3-6 adaxial cells near base, usually not markedly larger than abaxial cells; autoicous or dioicous; peristome teeth without basal membrane, entire or divided distally; epidermal cells of vaginula with straight walls</description>
      <determination>15a Grimmiaceae subfam. Grimmioideae</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Cladocarpous or rarely acrocarpous; stems often prostrate, without central strand; basal cells of lamina often with spiral thickenings, forming a colored strip along the insertion; costa in transverse section with (2-)3-15 adaxial cells near base, much larger than abaxial cells; dioicous; peristome teeth mostly with basal membrane, cleft into 2-3 filiform branches at least to middle or irregularly divided in distal portion; epidermal cells of vaginula with sinuose-nodulose walls</description>
      <determination>15b Grimmiaceae subfam. Racomitrioideae</determination>
    </key_statement>
  </key>
</bio:treatment>