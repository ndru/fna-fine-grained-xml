<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">296</other_info_on_meta>
    <other_info_on_meta type="mention_page">303</other_info_on_meta>
    <other_info_on_meta type="treatment_page">302</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Bednarek-Ochyra &amp; Ochyra in R. Ochyra et al." date="2003" rank="subfamily">racomitrioideae</taxon_name>
    <taxon_name authority="P. Beauvois" date="1822" rank="genus">codriophorus</taxon_name>
    <taxon_name authority="(Bednarek-Ochyra) Bednarek-Ochyra &amp; Ochyra in R. Ochyra et al." date="2003" rank="section">Fascicularia</taxon_name>
    <place_of_publication>
      <publication_title>in R. Ochyra et al., Cens. Cat. Polish Mosses,</publication_title>
      <place_in_publication>140. 2003.,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily racomitrioideae;genus codriophorus;section Fascicularia</taxon_hierarchy>
    <other_info_on_name type="fna_id">317131</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Racomitrium</taxon_name>
    <taxon_name authority="Bednarek-Ochyra" date="unknown" rank="section">Fascicularia</taxon_name>
    <place_of_publication>
      <publication_title>Fragm. Florist. Geobot., Ser. Polon.</publication_title>
      <place_in_publication>2: 130. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Racomitrium;section Fascicularia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems often subpinnately branched with numerous short, tuft-like, lateral, horizontal branchlets, giving the plants a nodose appearance.</text>
      <biological_entity id="o5376" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character constraint="with branchlets" constraintid="o5377" is_modifier="false" modifier="often subpinnately" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s0" value="nodose" value_original="nodose" />
      </biological_entity>
      <biological_entity id="o5377" name="branchlet" name_original="branchlets" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="numerous" value_original="numerous" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="true" name="shape" src="d0_s0" value="tuft-like" value_original="tuft-like" />
        <character is_modifier="true" name="position" src="d0_s0" value="lateral" value_original="lateral" />
        <character is_modifier="true" name="orientation" src="d0_s0" value="horizontal" value_original="horizontal" />
      </biological_entity>
      <biological_entity id="o5378" name="plant" name_original="plants" src="d0_s0" type="structure" />
      <relation from="o5376" id="r1261" name="giving the" negation="false" src="d0_s0" to="o5378" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves ovatelanceolate, narrowly lanceolate to linear-lanceolate, from an ovate, oblong or ovatelanceolate, not or distinctly plicate base, gradually short or long-acuminate, often with a long, subulate, canaliculate-concave, straight or sometimes wavy to serpentine acumen;</text>
      <biological_entity id="o5379" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s1" to="linear-lanceolate" />
        <character is_modifier="false" modifier="gradually" name="height_or_length_or_size" notes="" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s1" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
      <biological_entity id="o5380" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="ovate" value_original="ovate" />
        <character is_modifier="true" name="shape" src="d0_s1" value="oblong" value_original="oblong" />
        <character is_modifier="true" name="shape" src="d0_s1" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="true" modifier="not; distinctly" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s1" value="plicate" value_original="plicate" />
      </biological_entity>
      <biological_entity id="o5381" name="acumen" name_original="acumen" src="d0_s1" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s1" value="long" value_original="long" />
        <character is_modifier="true" name="shape" src="d0_s1" value="subulate" value_original="subulate" />
        <character is_modifier="true" name="shape" src="d0_s1" value="canaliculate-concave" value_original="canaliculate-concave" />
        <character is_modifier="true" name="course" src="d0_s1" value="straight" value_original="straight" />
        <character char_type="range_value" from="wavy" is_modifier="true" name="shape" src="d0_s1" to="serpentine" />
      </biological_entity>
      <relation from="o5379" id="r1262" name="from" negation="false" src="d0_s1" to="o5380" />
      <relation from="o5379" id="r1263" modifier="often" name="with" negation="false" src="d0_s1" to="o5381" />
    </statement>
    <statement id="d0_s2">
      <text>margins 1-stratose, recurved to revolute on both sides;</text>
      <biological_entity id="o5382" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character constraint="on sides" constraintid="o5383" is_modifier="false" name="shape_or_vernation" src="d0_s2" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o5383" name="side" name_original="sides" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>apices muticous, acute, subacute to narrowly rounded-obtuse, entire, erose-dentate or denticulate-cristate, epilose or terminated with a hyaline, denticulate awn;</text>
      <biological_entity id="o5384" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="muticous" value_original="muticous" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character char_type="range_value" from="subacute" name="shape" src="d0_s3" to="narrowly rounded-obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="erose-dentate" value_original="erose-dentate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="denticulate-cristate" value_original="denticulate-cristate" />
      </biological_entity>
      <biological_entity id="o5385" name="awn" name_original="awn" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="hyaline" value_original="hyaline" />
        <character is_modifier="true" name="shape" src="d0_s3" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <relation from="o5384" id="r1264" name="terminated with" negation="false" src="d0_s3" to="o5385" />
    </statement>
    <statement id="d0_s4">
      <text>costa narrow, vanishing in mid leaf to subpercurrent or percurrent, in the proximal part lying at the bottom of a deep, wide or narrow-angled furrow, open or partly enclosed by the strongly infolded leaf base, in transverse-section 2-stratose throughout, with occasional 3-stratose spots near the base or 2-stratose in the distal and 3–4-stratose in the proximal parts, flattened or convex on the ventral side, not prominently convex, lunate or flattened on the abaxial side, with cells in the abaxial and adaxial rows of similar size and shape or with 1–2 abaxial rows composed of small stereid cells in the proximal 1/2 or at the extreme base;</text>
      <biological_entity id="o5386" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s4" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="open" value_original="open" />
        <character name="architecture" src="d0_s4" value="partly enclosed by the strongly infolded leaf base" />
        <character constraint="in proximal parts" constraintid="o5395" is_modifier="false" name="architecture" notes="" src="d0_s4" value="3-4-stratose" value_original="3-4-stratose" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flattened" value_original="flattened" />
        <character constraint="on ventral side" constraintid="o5396" is_modifier="false" name="shape" src="d0_s4" value="convex" value_original="convex" />
        <character is_modifier="false" modifier="not prominently" name="shape" notes="" src="d0_s4" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lunate" value_original="lunate" />
        <character constraint="on abaxial side" constraintid="o5397" is_modifier="false" name="shape" src="d0_s4" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity constraint="mid" id="o5387" name="leaf" name_original="leaf" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="subpercurrent" value_original="subpercurrent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o5388" name="part" name_original="part" src="d0_s4" type="structure" />
      <biological_entity id="o5389" name="bottom" name_original="bottom" src="d0_s4" type="structure" />
      <biological_entity id="o5390" name="furrow" name_original="furrow" src="d0_s4" type="structure">
        <character is_modifier="true" name="depth" src="d0_s4" value="deep" value_original="deep" />
        <character is_modifier="true" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character is_modifier="true" name="shape" src="d0_s4" value="narrow-angled" value_original="narrow-angled" />
      </biological_entity>
      <biological_entity id="o5391" name="transverse-section" name_original="transverse-section" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="throughout" name="architecture" src="d0_s4" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity id="o5392" name="spot" name_original="spots" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="occasional" value_original="occasional" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="3-stratose" value_original="3-stratose" />
      </biological_entity>
      <biological_entity id="o5393" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o5394" name="part" name_original="part" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o5395" name="part" name_original="parts" src="d0_s4" type="structure" />
      <biological_entity constraint="ventral" id="o5396" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity constraint="abaxial" id="o5397" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity constraint="abaxial" id="o5398" name="row" name_original="rows" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s4" to="2" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o5399" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s4" to="2" />
      </biological_entity>
      <biological_entity constraint="extreme" id="o5400" name="row" name_original="rows" src="d0_s4" type="structure" />
      <relation from="o5386" id="r1265" name="vanishing in" negation="false" src="d0_s4" to="o5387" />
      <relation from="o5386" id="r1266" name="in" negation="false" src="d0_s4" to="o5388" />
      <relation from="o5388" id="r1267" name="lying at the" negation="false" src="d0_s4" to="o5389" />
      <relation from="o5388" id="r1268" name="lying at the" negation="false" src="d0_s4" to="o5390" />
      <relation from="o5386" id="r1269" name="in" negation="false" src="d0_s4" to="o5391" />
      <relation from="o5386" id="r1270" name="with" negation="false" src="d0_s4" to="o5392" />
      <relation from="o5392" id="r1271" name="near" negation="false" src="d0_s4" to="o5393" />
      <relation from="o5392" id="r1272" name="near" negation="false" src="d0_s4" to="o5394" />
      <relation from="o5386" id="r1273" name="of small stereid cells in the proximal 1/2 or at" negation="false" src="d0_s4" to="o5398" />
      <relation from="o5386" id="r1274" name="of small stereid cells in the proximal 1/2 or at" negation="false" src="d0_s4" to="o5399" />
    </statement>
    <statement id="d0_s5">
      <text>laminal cells 1-stratose, short or long-rectangular to linear, thick and sinuose-walled throughout;</text>
      <biological_entity constraint="laminal" id="o5401" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character name="height_or_length_or_size" src="d0_s5" value="long-rectangular to linear" value_original="long-rectangular to linear" />
        <character is_modifier="false" name="width" src="d0_s5" value="thick" value_original="thick" />
        <character is_modifier="false" modifier="throughout" name="architecture" src="d0_s5" value="sinuose-walled" value_original="sinuose-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal marginal border differentiated, pellucid;</text>
      <biological_entity constraint="basal marginal" id="o5402" name="border" name_original="border" src="d0_s6" type="structure">
        <character is_modifier="false" name="variability" src="d0_s6" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pellucid" value_original="pellucid" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal laminal cells exceptionally variously 2-stratose and isodiametric.</text>
      <biological_entity constraint="distal laminal" id="o5403" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="exceptionally variously" name="architecture" src="d0_s7" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="isodiametric" value_original="isodiametric" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, South America (Argentina, Chile), Europe, arctic and temperate Asia, Atlantic Islands (Azores, Gough Island, Iceland, South Georgia, Tristan da Cunha), Pacific Islands (Hawaiian Islands, Society Islands).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="South America (Argentina)" establishment_means="native" />
        <character name="distribution" value="South America (Chile)" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="arctic and temperate Asia" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Azores)" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Gough Island)" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Iceland)" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (South Georgia)" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Tristan da Cunha)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaiian Islands)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Society Islands)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9b.</number>
  <discussion>Species 6 (3 in the flora).</discussion>
  <discussion>Members of sect. Fascicularia are recognized by their narrowly lanceolate to linear- or ovate-lanceolate leaves, short-rectangular to linear laminal cells, and peculiar anatomical structure of the costa, which is narrow, usually less than 80 µm wide at the base, and mostly 2-stratose throughout and composed of undifferentiated cells in both adaxial and abaxial rows. Only in the highly isolated C. varius is the costa broader, to 110 µm near the base, and 3–4-stratose in the proximal half, but in all species of the section it is lying at the base of a deep and narrow-angled groove that is often partly enclosed by the strongly folded leaf base.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Costa 3(-4)-stratose, lunate and distinctly convex on the abaxial side, 75-135 µm wide proximally; leaves usually piliferous, if muticous then entire; peristome teeth 1.0-1.8 mm.</description>
      <determination>9 Codriophorus varius</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Costa 2-stratose throughout except the extreme base, flattened and weakly convex on the abaxial side, less than 70 µm wide proximally; leaves muticous; peristome teeth less than 1 mm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Costa extending 3/4 way up the leaf to subpercurrent; leaf apex entire; leaf acumen straight</description>
      <determination>7 Codriophorus fascicularis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Costa ending in mid leaf or only slightly distally; leaf apex erose-dentate; leaf acumen serpentine, strongly wavy.</description>
      <determination>8 Codriophorus corrugatus</determination>
    </key_statement>
  </key>
</bio:treatment>