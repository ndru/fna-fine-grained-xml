<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">63</other_info_on_meta>
    <other_info_on_meta type="mention_page">65</other_info_on_meta>
    <other_info_on_meta type="mention_page">72</other_info_on_meta>
    <other_info_on_meta type="mention_page">93</other_info_on_meta>
    <other_info_on_meta type="treatment_page">77</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">sphagnaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sphagnum</taxon_name>
    <taxon_name authority="Lindberg" date="1862" rank="section">cuspidata</taxon_name>
    <taxon_name authority="Sullivant" date="1849" rank="species">torreyanum</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 174. 1849,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sphagnaceae;genus sphagnum;section cuspidata;species torreyanum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443238</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sphagnum</taxon_name>
    <taxon_name authority="Warnstorf" date="unknown" rank="species">cuspidatum</taxon_name>
    <taxon_name authority="Braithwaite" date="unknown" rank="variety">torreyi</taxon_name>
    <taxon_hierarchy>genus Sphagnum;species cuspidatum;variety torreyi;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sphagnum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">cuspidatum</taxon_name>
    <taxon_name authority="(Röll) Renauld &amp; Cardot" date="unknown" rank="variety">miquelonense</taxon_name>
    <taxon_hierarchy>genus Sphagnum;species cuspidatum;variety miquelonense;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sphagnum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">kearneyi</taxon_name>
    <taxon_hierarchy>genus Sphagnum;species kearneyi;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sphagnum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">laxifolium</taxon_name>
    <taxon_name authority="Röll" date="unknown" rank="variety">miquelonense</taxon_name>
    <taxon_hierarchy>genus Sphagnum;species laxifolium;variety miquelonense;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants robust and weak-stemmed;</text>
    </statement>
    <statement id="d0_s1">
      <text>green to golden yellow;</text>
      <biological_entity id="o5182" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s0" value="robust" value_original="robust" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="weak-stemmed" value_original="weak-stemmed" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s1" to="golden yellow" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>capitulum ± rounded in emergent forms, flat in submersed forms;</text>
      <biological_entity id="o5183" name="capitulum" name_original="capitulum" src="d0_s2" type="structure">
        <character constraint="in forms" constraintid="o5184" is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
        <character constraint="in forms" constraintid="o5185" is_modifier="false" name="prominence_or_shape" notes="" src="d0_s2" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o5184" name="form" name_original="forms" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="emergent" value_original="emergent" />
      </biological_entity>
      <biological_entity id="o5185" name="form" name_original="forms" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="submersed" value_original="submersed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>lacking distinct terminal bud.</text>
      <biological_entity constraint="terminal" id="o5186" name="bud" name_original="bud" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems green to brown;</text>
      <biological_entity id="o5187" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s4" to="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>superficial cortex of 2 layers of thin-walled and enlarged cells.</text>
      <biological_entity constraint="superficial" id="o5188" name="cortex" name_original="cortex" src="d0_s5" type="structure" />
      <biological_entity id="o5189" name="layer" name_original="layers" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o5190" name="bud" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="thin-walled" value_original="thin-walled" />
        <character is_modifier="true" name="size" src="d0_s5" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <relation from="o5188" id="r1442" name="consist_of" negation="false" src="d0_s5" to="o5189" />
      <relation from="o5189" id="r1443" name="part_of" negation="false" src="d0_s5" to="o5190" />
    </statement>
    <statement id="d0_s6">
      <text>Stem-leaves triangular, 1–1.7 mm, apex acute to slightly obtuse;</text>
      <biological_entity id="o5191" name="leaf-stem" name_original="stem-leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="1" from_unit="mm" name="distance" src="d0_s6" to="1.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5192" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="slightly obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>leaves usually appressed;</text>
      <biological_entity id="o5193" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>margins entire;</text>
      <biological_entity id="o5194" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hyaline cells fibrillose and usually septate at base and sides.</text>
      <biological_entity id="o5195" name="bud" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="texture" src="d0_s9" value="fibrillose" value_original="fibrillose" />
        <character constraint="at sides" constraintid="o5197" is_modifier="false" modifier="usually" name="architecture" src="d0_s9" value="septate" value_original="septate" />
      </biological_entity>
      <biological_entity id="o5196" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o5197" name="side" name_original="sides" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Branches unranked, long and tapering, leaves greatly elongated at distal end.</text>
      <biological_entity id="o5198" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="unranked" value_original="unranked" />
        <character is_modifier="false" name="length_or_size" src="d0_s10" value="long" value_original="long" />
        <character is_modifier="false" name="shape" src="d0_s10" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o5199" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character constraint="at distal end" constraintid="o5200" is_modifier="false" modifier="greatly" name="length" src="d0_s10" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity constraint="distal" id="o5200" name="end" name_original="end" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Branch fascicles with 2 spreading and 2 pendent branches.</text>
      <biological_entity id="o5201" name="branch" name_original="branch" src="d0_s11" type="structure">
        <character constraint="with branches" constraintid="o5202" is_modifier="false" name="arrangement" src="d0_s11" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o5202" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="true" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="true" name="orientation" src="d0_s11" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Branch stems green;</text>
      <biological_entity constraint="branch" id="o5203" name="stem" name_original="stems" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>cortex enlarged with conspicuous retort cells.</text>
      <biological_entity id="o5204" name="cortex" name_original="cortex" src="d0_s13" type="structure">
        <character constraint="with retort cells" constraintid="o5205" is_modifier="false" name="size" src="d0_s13" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity constraint="retort" id="o5205" name="bud" name_original="cells" src="d0_s13" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s13" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Branch leaves ovatelanceolate to lanceolate, 3–5.5 mm;</text>
    </statement>
    <statement id="d0_s15">
      <text>straight but sometimes slightly falcate-secund;</text>
    </statement>
    <statement id="d0_s16">
      <text>weakly undulate and recurved when dry;</text>
      <biological_entity constraint="branch" id="o5206" name="leaf" name_original="leaves" src="d0_s14" type="structure">
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s14" to="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="5.5" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="sometimes slightly" name="arrangement" src="d0_s15" value="falcate-secund" value_original="falcate-secund" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s16" value="undulate" value_original="undulate" />
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s16" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>margins entire;</text>
      <biological_entity id="o5207" name="margin" name_original="margins" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>hyaline cells on convex surface with 0–1 pore per cell, on concave surface with round wall-thinnings on the apices and angles;</text>
      <biological_entity id="o5208" name="bud" name_original="cells" src="d0_s18" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s18" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o5209" name="surface" name_original="surface" src="d0_s18" type="structure">
        <character is_modifier="true" name="shape" src="d0_s18" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o5210" name="pore" name_original="pore" src="d0_s18" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s18" to="1" />
      </biological_entity>
      <biological_entity id="o5211" name="cell" name_original="cell" src="d0_s18" type="structure" />
      <biological_entity id="o5212" name="surface" name_original="surface" src="d0_s18" type="structure">
        <character is_modifier="true" name="shape" src="d0_s18" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity id="o5213" name="wall-thinning" name_original="wall-thinnings" src="d0_s18" type="structure">
        <character is_modifier="true" name="shape" src="d0_s18" value="round" value_original="round" />
      </biological_entity>
      <biological_entity id="o5214" name="apex" name_original="apices" src="d0_s18" type="structure" />
      <biological_entity id="o5215" name="angle" name_original="angles" src="d0_s18" type="structure" />
      <relation from="o5208" id="r1444" name="on" negation="false" src="d0_s18" to="o5209" />
      <relation from="o5209" id="r1445" name="with" negation="false" src="d0_s18" to="o5210" />
      <relation from="o5210" id="r1446" name="per" negation="false" src="d0_s18" to="o5211" />
      <relation from="o5208" id="r1447" name="on" negation="false" src="d0_s18" to="o5212" />
      <relation from="o5212" id="r1448" name="with" negation="false" src="d0_s18" to="o5213" />
      <relation from="o5213" id="r1449" name="on" negation="false" src="d0_s18" to="o5214" />
      <relation from="o5213" id="r1450" name="on" negation="false" src="d0_s18" to="o5215" />
    </statement>
    <statement id="d0_s19">
      <text>chlorophyllous cells narrowly triangular in transverse-section and just enclosed on the concave surface.</text>
      <biological_entity id="o5217" name="transverse-section" name_original="transverse-section" src="d0_s19" type="structure" />
      <biological_entity id="o5218" name="surface" name_original="surface" src="d0_s19" type="structure">
        <character is_modifier="true" name="shape" src="d0_s19" value="concave" value_original="concave" />
      </biological_entity>
      <relation from="o5216" id="r1451" modifier="just" name="enclosed on the" negation="false" src="d0_s19" to="o5218" />
    </statement>
    <statement id="d0_s20">
      <text>Sexual condition dioicous.</text>
      <biological_entity id="o5216" name="bud" name_original="cells" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="chlorophyllous" value_original="chlorophyllous" />
        <character constraint="in transverse-section" constraintid="o5217" is_modifier="false" modifier="narrowly" name="shape" src="d0_s19" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="reproduction" src="d0_s20" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s20" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Spores 26–29 µm; both surfaces distinctly papillose, appearing pusticulate to irregularly pusticulate;</text>
      <biological_entity id="o5219" name="spore" name_original="spores" src="d0_s21" type="structure">
        <character char_type="range_value" from="26" from_unit="um" name="some_measurement" src="d0_s21" to="29" to_unit="um" />
      </biological_entity>
      <biological_entity id="o5220" name="surface" name_original="surfaces" src="d0_s21" type="structure">
        <character is_modifier="false" modifier="distinctly" name="relief" src="d0_s21" value="papillose" value_original="papillose" />
        <character char_type="range_value" from="pusticulate" name="relief" src="d0_s21" to="irregularly pusticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>proximal laesura mostly less than 0.5 spore radius.</text>
      <biological_entity constraint="proximal" id="o5221" name="laesurum" name_original="laesura" src="d0_s22" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s22" to="0.5" />
      </biological_entity>
      <biological_entity id="o5222" name="spore" name_original="spore" src="d0_s22" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Forming wet often floating carpets in weakly minerotrophic mires</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet floating carpets" modifier="forming often" constraint="in weakly minerotrophic mires" />
        <character name="habitat" value="weakly minerotrophic mires" modifier="in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>St. Pierre and Miquelon (Miquelon); N.B., Nfld. and Labr. (Nfld.), N.S., Que.; Ala., Conn., Del., Fla., Ga., La., Maine, Md., Mass., Miss., N.H., N.J., N.Y., N.C., Pa., R.I., S.C., Vt., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="St. Pierre and Miquelon (Miquelon)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>48.</number>
  <discussion>Sporophytes are uncommon in Sphagnum torreyanum. See discussion under 24. S. atlanticum for taxonomic distinctions.</discussion>
  
</bio:treatment>