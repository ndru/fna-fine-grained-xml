<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">334</other_info_on_meta>
    <other_info_on_meta type="mention_page">341</other_info_on_meta>
    <other_info_on_meta type="treatment_page">342</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">fissidentaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">fissidens</taxon_name>
    <taxon_name authority="(Cardot &amp; Thériot) Cardot &amp; Thériot" date="1904" rank="species">bushii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>37: 365. 1904,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fissidentaceae;genus fissidens;species bushii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075558</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Fissidens</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">subbasilaris</taxon_name>
    <taxon_name authority="Cardot &amp; Thériot" date="unknown" rank="variety">bushii</taxon_name>
    <taxon_hierarchy>genus Fissidens;species subbasilaris;variety bushii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 13 × 2–2.5 mm.</text>
      <biological_entity id="o6349" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s0" to="13" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s0" to="2.5" to_unit="mm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stem branched;</text>
      <biological_entity id="o6350" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>axillary hyaline nodules weak;</text>
      <biological_entity constraint="axillary" id="o6351" name="nodule" name_original="nodules" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="fragility" src="d0_s2" value="weak" value_original="weak" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>central strand weak.</text>
      <biological_entity constraint="central" id="o6352" name="strand" name_original="strand" src="d0_s3" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s3" value="weak" value_original="weak" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves as many as 20 pairs, oblong to lanceolate, obtuse-apiculate to acute-apiculate, to 2 × 0.5 mm;</text>
      <biological_entity id="o6353" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblong" name="shape" notes="" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="obtuse-apiculate" name="architecture_or_shape" src="d0_s4" to="acute-apiculate" />
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s4" to="2" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s4" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6354" name="pair" name_original="pairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="20" value_original="20" />
      </biological_entity>
      <relation from="o6353" id="r1522" name="as many as" negation="false" src="d0_s4" to="o6354" />
    </statement>
    <statement id="d0_s5">
      <text>dorsal lamina rounded proximally, ending at insertion, not decurrent;</text>
      <biological_entity constraint="dorsal" id="o6355" name="lamina" name_original="lamina" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>vaginant laminae ± 2/3 leaf length, ± equal, minor lamina ending on or near margin;</text>
      <biological_entity constraint="vaginant" id="o6356" name="lamina" name_original="laminae" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="2/3" value_original="2/3" />
      </biological_entity>
      <biological_entity id="o6357" name="leaf" name_original="leaf" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="length" src="d0_s6" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity constraint="minor" id="o6358" name="lamina" name_original="lamina" src="d0_s6" type="structure" />
      <biological_entity id="o6359" name="margin" name_original="margin" src="d0_s6" type="structure" />
      <relation from="o6358" id="r1523" name="ending on" negation="false" src="d0_s6" to="o6359" />
    </statement>
    <statement id="d0_s7">
      <text>margin crenulate-serrulate, more strongly serrulate on vaginant laminae;</text>
      <biological_entity id="o6360" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="crenulate-serrulate" value_original="crenulate-serrulate" />
        <character constraint="on vaginant laminae" constraintid="o6361" is_modifier="false" modifier="strongly" name="architecture_or_shape" src="d0_s7" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity constraint="vaginant" id="o6361" name="lamina" name_original="laminae" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>costa ending 1–2 cells before apex or ending in apiculus, taxifolius-type;</text>
      <biological_entity id="o6362" name="costa" name_original="costa" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_ref_taxa" notes="" src="d0_s8" value="taxifolius-type" value_original="taxifolius-type" />
      </biological_entity>
      <biological_entity id="o6363" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="2" />
      </biological_entity>
      <biological_entity id="o6364" name="apex" name_original="apex" src="d0_s8" type="structure" />
      <biological_entity id="o6365" name="apiculu" name_original="apiculus" src="d0_s8" type="structure" />
      <relation from="o6362" id="r1524" name="ending" negation="false" src="d0_s8" to="o6363" />
      <relation from="o6362" id="r1525" name="before" negation="false" src="d0_s8" to="o6364" />
      <relation from="o6362" id="r1526" name="before" negation="false" src="d0_s8" to="o6365" />
    </statement>
    <statement id="d0_s9">
      <text>laminal cells irregularly 2-stratose in dorsal and ventral laminae, smooth, bulging, firm-walled, rounded-hexagonal, 7–9 µm, papillose in cell corners with 2–4 ± inconspicuous papillae in vaginant laminae.</text>
      <biological_entity constraint="dorsal and ventral" id="o6367" name="lamina" name_original="laminae" src="d0_s9" type="structure" />
      <biological_entity constraint="cell" id="o6368" name="corner" name_original="corners" src="d0_s9" type="structure" />
      <biological_entity id="o6369" name="papilla" name_original="papillae" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s9" to="4" />
        <character is_modifier="true" modifier="more or less" name="prominence" src="d0_s9" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity constraint="vaginant" id="o6370" name="lamina" name_original="laminae" src="d0_s9" type="structure" />
      <relation from="o6368" id="r1527" name="with" negation="false" src="d0_s9" to="o6369" />
      <relation from="o6369" id="r1528" name="in" negation="false" src="d0_s9" to="o6370" />
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="laminal" id="o6366" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character constraint="in dorsal ventral laminae" constraintid="o6367" is_modifier="false" modifier="irregularly" name="architecture" src="d0_s9" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_shape" src="d0_s9" value="bulging" value_original="bulging" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="firm-walled" value_original="firm-walled" />
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded-hexagonal" value_original="rounded-hexagonal" />
        <character char_type="range_value" from="7" from_unit="um" name="some_measurement" src="d0_s9" to="9" to_unit="um" />
        <character constraint="in cell corners" constraintid="o6368" is_modifier="false" name="relief" src="d0_s9" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>perigonia and perichaetia gemmiform, axillary, confined to proximal parts of stems.</text>
      <biological_entity id="o6371" name="perigonium" name_original="perigonia" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="gemmiform" value_original="gemmiform" />
        <character is_modifier="false" name="position" src="d0_s11" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o6372" name="perichaetium" name_original="perichaetia" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="gemmiform" value_original="gemmiform" />
        <character is_modifier="false" name="position" src="d0_s11" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6373" name="part" name_original="parts" src="d0_s11" type="structure" />
      <biological_entity constraint="proximal" id="o6374" name="stem" name_original="stems" src="d0_s11" type="structure" />
      <relation from="o6371" id="r1529" name="confined to" negation="false" src="d0_s11" to="o6373" />
      <relation from="o6371" id="r1530" name="confined to" negation="false" src="d0_s11" to="o6374" />
      <relation from="o6372" id="r1531" name="confined to" negation="false" src="d0_s11" to="o6373" />
      <relation from="o6372" id="r1532" name="confined to" negation="false" src="d0_s11" to="o6374" />
    </statement>
    <statement id="d0_s12">
      <text>Sporophytes 1 per perichaetium.</text>
      <biological_entity id="o6375" name="sporophyte" name_original="sporophytes" src="d0_s12" type="structure">
        <character constraint="per perichaetium" constraintid="o6376" name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o6376" name="perichaetium" name_original="perichaetium" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Seta to 10 mm.</text>
      <biological_entity id="o6377" name="seta" name_original="seta" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsule theca to 1 mm, erect, ± arcuate, bilaterally symmetric, about 1 mm;</text>
      <biological_entity constraint="capsule" id="o6378" name="theca" name_original="theca" src="d0_s14" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="more or less" name="course_or_shape" src="d0_s14" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s14" value="symmetric" value_original="symmetric" />
        <character name="some_measurement" src="d0_s14" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>peristome taxifolius-type;</text>
      <biological_entity id="o6379" name="peristome" name_original="peristome" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_ref_taxa" src="d0_s15" value="taxifolius-type" value_original="taxifolius-type" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>operculum 1 mm.</text>
      <biological_entity id="o6380" name="operculum" name_original="operculum" src="d0_s16" type="structure">
        <character name="some_measurement" src="d0_s16" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Calyptra cucullate, smooth, 0.6 mm.</text>
      <biological_entity id="o6381" name="calyptra" name_original="calyptra" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character name="some_measurement" src="d0_s17" unit="mm" value="0.6" value_original="0.6" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Spores 14–18 µm.</text>
      <biological_entity id="o6382" name="spore" name_original="spores" src="d0_s18" type="structure">
        <character char_type="range_value" from="14" from_unit="um" name="some_measurement" src="d0_s18" to="18" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Usually on bare or disturbed clayey soil in open and in woods, along paths, roadside banks, along streams, ravines, infrequently on rocks, stones, tree bases</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bare" modifier="usually on" constraint="in open and in woods , along paths , roadside banks , along streams , ravines , infrequently on rocks , stones , tree bases" />
        <character name="habitat" value="disturbed clayey soil" constraint="in open and in woods , along paths , roadside banks , along streams , ravines , infrequently on rocks , stones , tree bases" />
        <character name="habitat" value="woods" constraint="along paths , roadside banks" />
        <character name="habitat" value="paths" />
        <character name="habitat" value="roadside banks" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="ravines" />
        <character name="habitat" value="rocks" />
        <character name="habitat" value="open" />
        <character name="habitat" value="in woods" />
        <character name="habitat" value="along paths" />
        <character name="habitat" value="along streams" />
        <character name="habitat" value="on rocks" modifier="infrequently" />
        <character name="habitat" value="stones" />
        <character name="habitat" value="tree bases" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., N.S., Ont., Que.; Ala., Ark., Del., D.C., Fla., Ga., Ill., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Miss., Mo., N.H., N.Y., N.C., Ohio, Okla., Pa., S.C., Tenn., Tex., Vt., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <discussion>Fissidens bushii, named in honor of the collector of the type, Benjamin Franklin Bush, is most apt to be confused with F. taxifolius. However, plants of F. bushii are usually much smaller, costa shorter, and leaves sometimes caducous. Moreover, small papillae restricted to the corners of cells in vaginant laminae are distinctive. The species is probably most closely related to the Asiatic F. teysmannianus Dozy &amp; Molkenboer, which also has inconspicuous papillae in the corners of vaginant laminal cells.</discussion>
  
</bio:treatment>