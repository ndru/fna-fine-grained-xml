<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">399</other_info_on_meta>
    <other_info_on_meta type="mention_page">404</other_info_on_meta>
    <other_info_on_meta type="treatment_page">403</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">dicranum</taxon_name>
    <taxon_name authority="Renauld &amp; Cardot" date="1888" rank="species">howellii</taxon_name>
    <place_of_publication>
      <publication_title>Rev. Bryol.</publication_title>
      <place_in_publication>15: 70. 1888,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus dicranum;species howellii</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443664</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in loose to dense tufts, green to yellowish green, glossy.</text>
      <biological_entity id="o117" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s0" to="yellowish green" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="glossy" value_original="glossy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o118" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o117" id="r24" name="in" negation="false" src="d0_s0" to="o118" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 2–8 cm, densely tomentose with white to reddish-brown rhizoids.</text>
      <biological_entity id="o119" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="8" to_unit="cm" />
        <character constraint="with rhizoids" constraintid="o120" is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o120" name="rhizoid" name_original="rhizoids" src="d0_s1" type="structure">
        <character char_type="range_value" from="white" is_modifier="true" name="coloration" src="d0_s1" to="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves falcate-secund to straight and erect, sometimes slightly crisped, smooth, (5–) 8–10 (–12) × 0.8–1.5 mm, concave proximally, keeled or tubulose above, lanceolate, apex acute;</text>
      <biological_entity id="o121" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="falcate-secund" value_original="falcate-secund" />
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes slightly" name="shape" src="d0_s2" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s2" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="12" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s2" to="10" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s2" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s2" value="concave" value_original="concave" />
        <character is_modifier="false" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="shape" src="d0_s2" value="tubulose" value_original="tubulose" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o122" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins strongly serrate in the distal part, entire proximally;</text>
      <biological_entity id="o123" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="in distal part" constraintid="o124" is_modifier="false" modifier="strongly" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_shape" notes="" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="distal" id="o124" name="part" name_original="part" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>laminae 1-stratose;</text>
      <biological_entity id="o125" name="lamina" name_original="laminae" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa ending before apex to shortly excurrent, 1/12–1/5 the width of the leaves at base, with 2, rarely 4, toothed ridges on the abaxial surface extending from about the leaf middle nearly to the apex, rarely almost smooth, with a row of guide cells, two thin stereid bands, adaxial epidermal layer of cells not differentiated, the abaxial layer interrupted by several enlarged cells that form part of the abaxial ridge, not extending to the apices;</text>
      <biological_entity id="o126" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s5" value="excurrent" value_original="excurrent" />
        <character char_type="range_value" from="1/12" name="quantity" src="d0_s5" to="1/5" />
      </biological_entity>
      <biological_entity id="o127" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <biological_entity id="o128" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o129" name="base" name_original="base" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
        <character modifier="rarely" name="quantity" src="d0_s5" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o130" name="ridge" name_original="ridges" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
        <character is_modifier="false" modifier="rarely almost" name="architecture_or_pubescence_or_relief" notes="" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="width" src="d0_s5" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o131" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="width" src="d0_s5" value="extending" value_original="extending" />
      </biological_entity>
      <biological_entity id="o132" name="leaf" name_original="leaf" src="d0_s5" type="structure">
        <character constraint="to apex" constraintid="o133" is_modifier="false" name="position" src="d0_s5" value="middle" value_original="middle" />
      </biological_entity>
      <biological_entity id="o133" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <biological_entity id="o134" name="row" name_original="row" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="guide" id="o135" name="cell" name_original="cells" src="d0_s5" type="structure" />
      <biological_entity id="o136" name="band" name_original="bands" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial epidermal" id="o137" name="layer" name_original="layer" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o138" name="cell" name_original="cells" src="d0_s5" type="structure" />
      <biological_entity constraint="abaxial" id="o139" name="layer" name_original="layer" src="d0_s5" type="structure">
        <character constraint="by cells" constraintid="o140" is_modifier="false" name="architecture" src="d0_s5" value="interrupted" value_original="interrupted" />
      </biological_entity>
      <biological_entity id="o140" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="several" value_original="several" />
        <character is_modifier="true" name="size" src="d0_s5" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity id="o141" name="part" name_original="part" src="d0_s5" type="structure" />
      <biological_entity constraint="abaxial" id="o142" name="ridge" name_original="ridge" src="d0_s5" type="structure" />
      <biological_entity id="o143" name="apex" name_original="apices" src="d0_s5" type="structure" />
      <relation from="o126" id="r25" name="ending before" negation="false" src="d0_s5" to="o127" />
      <relation from="o128" id="r26" name="at" negation="false" src="d0_s5" to="o129" />
      <relation from="o130" id="r27" name="on" negation="false" src="d0_s5" to="o131" />
      <relation from="o131" id="r28" name="from" negation="false" src="d0_s5" to="o132" />
      <relation from="o130" id="r29" name="with" negation="false" src="d0_s5" to="o134" />
      <relation from="o134" id="r30" name="part_of" negation="false" src="d0_s5" to="o135" />
      <relation from="o137" id="r31" name="part_of" negation="false" src="d0_s5" to="o138" />
      <relation from="o140" id="r32" name="part_of" negation="false" src="d0_s5" to="o142" />
      <relation from="o140" id="r33" name="extending to the" negation="true" src="d0_s5" to="o143" />
    </statement>
    <statement id="d0_s6">
      <text>cell-walls between lamina cells not bulging;</text>
      <biological_entity constraint="between cells" constraintid="o145" id="o144" name="cell-wall" name_original="cell-walls" src="d0_s6" type="structure" constraint_original="between  cells, ">
        <character is_modifier="false" modifier="not" name="pubescence_or_shape" src="d0_s6" value="bulging" value_original="bulging" />
      </biological_entity>
      <biological_entity constraint="lamina" id="o145" name="cell" name_original="cells" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>leaf cells smooth;</text>
    </statement>
    <statement id="d0_s8">
      <text>alar cells 2-stratose, well-differentiated, sometimes extending to costa;</text>
      <biological_entity constraint="leaf" id="o146" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o147" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" name="variability" src="d0_s8" value="well-differentiated" value_original="well-differentiated" />
      </biological_entity>
      <biological_entity id="o148" name="costa" name_original="costa" src="d0_s8" type="structure" />
      <relation from="o147" id="r34" modifier="sometimes" name="extending to" negation="false" src="d0_s8" to="o148" />
    </statement>
    <statement id="d0_s9">
      <text>proximal laminal cells linear-rectangular, pitted, (52–) 65–105 (–120) × (10–) 12–18 (–22) µm; distal laminal cells shorter, sinuose, pitted, (42–) 54–90 (–108) µm × (10–) 12–14 (–16) µm. Sexual condition pseudomonoicous or dioicous;</text>
      <biological_entity constraint="proximal laminal" id="o149" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="linear-rectangular" value_original="linear-rectangular" />
        <character is_modifier="false" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
        <character char_type="range_value" from="52" from_unit="um" name="atypical_length" src="d0_s9" to="65" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="105" from_inclusive="false" from_unit="um" name="atypical_length" src="d0_s9" to="120" to_unit="um" />
        <character char_type="range_value" from="65" from_unit="um" name="length" src="d0_s9" to="105" to_unit="um" />
        <character char_type="range_value" from="10" from_unit="um" name="atypical_width" src="d0_s9" to="12" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="um" name="atypical_width" src="d0_s9" to="22" to_unit="um" />
        <character char_type="range_value" from="12" from_unit="um" name="width" src="d0_s9" to="18" to_unit="um" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>dwarf males on rhizoids of female plants or male plants as large as females and growing intermixed or in ± separate tufts;</text>
      <biological_entity constraint="distal laminal" id="o150" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
        <character char_type="range_value" from="42" from_unit="um" name="atypical_length" src="d0_s9" to="54" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="90" from_inclusive="false" from_unit="um" name="atypical_length" src="d0_s9" to="108" to_unit="um" />
        <character char_type="range_value" from="54" from_unit="um" name="length" src="d0_s9" to="90" to_unit="um" />
        <character char_type="range_value" from="10" from_unit="um" name="atypical_width" src="d0_s9" to="12" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="um" name="atypical_width" src="d0_s9" to="16" to_unit="um" />
        <character char_type="range_value" from="12" from_unit="um" name="width" src="d0_s9" to="14" to_unit="um" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="pseudomonoicous" value_original="pseudomonoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="dioicous" value_original="dioicous" />
        <character is_modifier="false" name="height" src="d0_s10" value="dwarf" value_original="dwarf" />
      </biological_entity>
      <biological_entity id="o151" name="rhizoid" name_original="rhizoids" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="male" value_original="male" />
      </biological_entity>
      <biological_entity id="o152" name="plant" name_original="plants" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="female" value_original="female" />
      </biological_entity>
      <biological_entity id="o153" name="plant" name_original="plants" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="male" value_original="male" />
      </biological_entity>
      <biological_entity id="o154" name="tuft" name_original="tufts" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="more or less" name="arrangement" src="d0_s10" value="separate" value_original="separate" />
      </biological_entity>
      <relation from="o151" id="r35" name="part_of" negation="false" src="d0_s10" to="o152" />
      <relation from="o151" id="r36" name="part_of" negation="false" src="d0_s10" to="o153" />
    </statement>
    <statement id="d0_s11">
      <text>perichaetial leaves gradually acuminate, not or partially convolute-sheathing.</text>
      <biological_entity constraint="perichaetial" id="o155" name="leaf" name_original="leaves" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="not; partially" name="architecture_or_shape" src="d0_s11" value="convolute-sheathing" value_original="convolute-sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seta 1.5–4 cm, solitary, sometimes 2 per perichaetium, yellow to reddish-brown.</text>
      <biological_entity id="o156" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s12" to="4" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s12" value="solitary" value_original="solitary" />
        <character constraint="per perichaetium" constraintid="o157" modifier="sometimes" name="quantity" src="d0_s12" value="2" value_original="2" />
        <character char_type="range_value" from="yellow" name="coloration" notes="" src="d0_s12" to="reddish-brown" />
      </biological_entity>
      <biological_entity id="o157" name="perichaetium" name_original="perichaetium" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Capsule 2–3.8 mm, arcuate, inclined to horizontal, smooth to striate when dry, yellow to dark reddish-brown;</text>
      <biological_entity id="o158" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3.8" to_unit="mm" />
        <character is_modifier="false" name="course_or_shape" src="d0_s13" value="arcuate" value_original="arcuate" />
        <character char_type="range_value" from="inclined" name="orientation" src="d0_s13" to="horizontal" />
        <character char_type="range_value" from="smooth" modifier="when dry" name="pubescence" src="d0_s13" to="striate" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s13" to="dark reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>operculum 2–3.8 mm.</text>
      <biological_entity id="o159" name="operculum" name_original="operculum" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Spores 18–26 µm.</text>
      <biological_entity id="o160" name="spore" name_original="spores" src="d0_s15" type="structure">
        <character char_type="range_value" from="18" from_unit="um" name="some_measurement" src="d0_s15" to="26" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil, humus, humus over rock, rotting logs and stumps, tree trunks and bases of trees, sometimes in bogs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" />
        <character name="habitat" value="humus" />
        <character name="habitat" value="rock" modifier="humus over" />
        <character name="habitat" value="stumps" modifier="rotting logs" />
        <character name="habitat" value="tree trunks" />
        <character name="habitat" value="bases" constraint="of trees" />
        <character name="habitat" value="trees" />
        <character name="habitat" value="bogs" modifier="sometimes in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>20-600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska, Calif., Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Dicranum howellii occurrs only in the northwestern part of North America. It is closely related to the common and nearly ubiquitous D. scoparium. The shape of the interior perichaetial leaves is the most reliable way to distinguish the two. Gradually acuminate perichaetial leaves that loosely surround the seta are characteristic of D. howellii. Dicranum scoparium has abruptly acuminate exterior perichaetial leaves, interior ones that are abruptly acuminateto blunt at the apex and are convolute-sheathing around the seta. The interior perichaetial leaves of D. howellii are also longer and narrower than those of the D. scoparium. Other less significant features of D. howellii are the consistently glossy, long leaves, mostly 8–10 mm, with long distal cells, averaging 54–90 µm, and the common occurrence of only two, rarely four, serrated ridges on the abaxial surface of the costae. Dicranum scoparium by comparison has glossy to sometimes dull leaves that are often shorter, mostly 5–8.5 mm, with shorter distal cells, averaging 27–43 µm, and the costae usually have four, rarely two, serrated ridges on the abaxial surface. The perichaetial leaves are undoubtedly the most important feature separating the two species, as well as being the easiest to observe. Eastern North American plants of D. scoparium all have the abruptly acuminate perichaetial leaves that are characteristic of the species, as do plants elsewhere in the world. The perichaetial leaf character was one reason D. howellii was believed to be a distinct species by F. Renauld and J. Cardot (1889, plate 12B) who first illustrated that character and this remains the most significant reason for maintaining it as a distinct species.</discussion>
  
</bio:treatment>