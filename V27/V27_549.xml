<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">383</other_info_on_meta>
    <other_info_on_meta type="mention_page">387</other_info_on_meta>
    <other_info_on_meta type="mention_page">389</other_info_on_meta>
    <other_info_on_meta type="treatment_page">388</other_info_on_meta>
    <other_info_on_meta type="illustration_page">388</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="(Müller Hal.) Schimper" date="1856" rank="genus">dicranella</taxon_name>
    <taxon_name authority="(Dickson) E. F. Warburg" date="1962" rank="species">palustris</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Brit. Bryol. Soc.</publication_title>
      <place_in_publication>4(2): 247. 1962,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus dicranella;species palustris</taxon_hierarchy>
    <other_info_on_name type="fna_id">240000040</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bryum</taxon_name>
    <taxon_name authority="Dickson" date="unknown" rank="species">palustre</taxon_name>
    <place_of_publication>
      <publication_title>Fasc. Pl. Crypt. Brit.</publication_title>
      <place_in_publication>4: 11. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Bryum;species palustre;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dicranella</taxon_name>
    <taxon_name authority="(Schrader) Schimper" date="unknown" rank="species">squarrosa</taxon_name>
    <taxon_hierarchy>genus Dicranella;species squarrosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants relatively robust, 2–11 cm, in loose, light-green or yellow, ± shiny tufts.</text>
      <biological_entity id="o8144" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="relatively" name="fragility" src="d0_s0" value="robust" value_original="robust" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="11" to_unit="cm" />
        <character is_modifier="false" modifier="in loose" name="coloration" src="d0_s0" value="light-green" value_original="light-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow" value_original="yellow" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8145" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="more or less" name="reflectance" src="d0_s0" value="shiny" value_original="shiny" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves not crowded, 2.5–3 mm, abruptly narrowed to a narrow, squarrose limb from an erect, oblong or obovate base, subtubulose and generally ± twisted-crispate when dry, concave when moist, often cucullate at a rounded though often narrow apex, decurrent at base;</text>
      <biological_entity id="o8146" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s1" value="crowded" value_original="crowded" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s1" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s1" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" name="size_or_width" src="d0_s1" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o8147" name="limb" name_original="limb" src="d0_s1" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s1" value="squarrose" value_original="squarrose" />
        <character is_modifier="false" modifier="when dry" name="shape" notes="" src="d0_s1" value="twisted-crispate" value_original="twisted-crispate" />
        <character is_modifier="false" modifier="when moist" name="shape" src="d0_s1" value="concave" value_original="concave" />
        <character constraint="at apex" constraintid="o8149" is_modifier="false" modifier="often" name="shape" src="d0_s1" value="cucullate" value_original="cucullate" />
        <character constraint="at base" constraintid="o8150" is_modifier="false" name="shape" notes="" src="d0_s1" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o8148" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="true" name="shape" src="d0_s1" value="oblong" value_original="oblong" />
        <character is_modifier="true" name="shape" src="d0_s1" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o8149" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
        <character is_modifier="true" modifier="though often" name="size_or_width" src="d0_s1" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o8150" name="base" name_original="base" src="d0_s1" type="structure" />
      <relation from="o8147" id="r1945" name="from" negation="false" src="d0_s1" to="o8148" />
    </statement>
    <statement id="d0_s2">
      <text>margins erect, irregularly crenate at the extreme tip;</text>
      <biological_entity id="o8151" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="at extreme tip" constraintid="o8152" is_modifier="false" modifier="irregularly" name="shape" src="d0_s2" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity constraint="extreme" id="o8152" name="tip" name_original="tip" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>costa subpercurrent, smooth abaxially;</text>
      <biological_entity id="o8153" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="subpercurrent" value_original="subpercurrent" />
        <character is_modifier="false" modifier="abaxially" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>distal cells elongate, 4–9: 1, the basal-cells longer, yellow at the insertion, often ± differentiated at extreme basal angles.</text>
      <biological_entity constraint="distal" id="o8154" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s4" to="9" />
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="extreme basal" id="o8156" name="angle" name_original="angles" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Sexual condition dioicous.</text>
      <biological_entity id="o8155" name="basal-cell" name_original="basal-cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
        <character is_modifier="false" name="insertion" src="d0_s4" value="yellow" value_original="yellow" />
        <character constraint="at extreme basal angles" constraintid="o8156" is_modifier="false" modifier="often more or less" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Seta 10–30 mm, dark red.</text>
      <biological_entity id="o8157" name="seta" name_original="seta" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="30" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="dark red" value_original="dark red" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsule 1–1.5 mm, curved-inclined, smooth;</text>
      <biological_entity id="o8158" name="capsule" name_original="capsule" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="curved-inclined" value_original="curved-inclined" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>annulus present or absent;</text>
      <biological_entity id="o8159" name="annulus" name_original="annulus" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>operculum stoutly conic-rostrate;</text>
      <biological_entity id="o8160" name="operculum" name_original="operculum" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="stoutly" name="shape" src="d0_s9" value="conic-rostrate" value_original="conic-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>peristome teeth ca. 500–650 µm, divided about 1/2 way distally, vertically papillose-striolate.</text>
      <biological_entity constraint="peristome" id="o8161" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character char_type="range_value" from="500" from_unit="um" name="some_measurement" src="d0_s10" to="650" to_unit="um" />
        <character is_modifier="false" name="shape" src="d0_s10" value="divided" value_original="divided" />
        <character name="quantity" src="d0_s10" value="1/2" value_original="1/2" />
        <character is_modifier="false" modifier="distally; vertically" name="pubescence" src="d0_s10" value="papillose-striolate" value_original="papillose-striolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spores 16–25 µm, smooth or papillose.</text>
      <biological_entity id="o8162" name="spore" name_original="spores" src="d0_s11" type="structure">
        <character char_type="range_value" from="16" from_unit="um" name="some_measurement" src="d0_s11" to="25" to_unit="um" />
        <character is_modifier="false" name="relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s11" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet soil in springy places, often at roadsides, sometimes temporarily submerged, probably an acidophile</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet soil" constraint="in springy places" />
        <character name="habitat" value="springy places" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="an acidophile" modifier="submerged" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to medium elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="low" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., N.B., Nfld. and Labr. (Nfld.), N.S., Que., Yukon; Alaska, Calif., Maine, Mont., N.H., Ohio, Oreg., Wash.; Europe; Asia (Japan, Turkey).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Asia (Turkey)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Dicranella palustris is known by its squarrose-spreading, decurrent leaves ending in a narrow but rounded and crenulate tip. Lax, relatively large expressions of D. schreberiana, often given recognition as var. robusta, are smaller and have leaves with slender, acute tips, serrated margins, and broad cells. The New Hampshire and California reports were by A. J. Grout (1928–1940, vol. 1, as D. squarrosa) and D. H. Norris and J. R. Shevock (2004), respectively.</discussion>
  
</bio:treatment>