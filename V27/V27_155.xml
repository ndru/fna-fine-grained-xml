<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="treatment_page">129</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwägrichen" date="unknown" rank="family">polytrichaceae</taxon_name>
    <taxon_name authority="G. L. Smith" date="1971" rank="genus">polytrichastrum</taxon_name>
    <taxon_name authority="(Bridel) G. L. Smith" date="1971" rank="species">sexangulare</taxon_name>
    <taxon_name authority="(C. E. O. Jensen) G. L. Smith" date="1992" rank="variety">vulcanicum</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>95: 270. 1992,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polytrichaceae;genus polytrichastrum;species sexangulare;variety vulcanicum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250065241</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polytrichum</taxon_name>
    <taxon_name authority="(Bescherelle) Müller Hal." date="unknown" rank="species">sexangulare</taxon_name>
    <taxon_name authority="C. E. O. Jensen" date="unknown" rank="variety">vulcanicum</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Tidsskr.</publication_title>
      <place_in_publication>20: 109. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polytrichum;species sexangulare;variety vulcanicum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pogonatum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">sphaerothecium</taxon_name>
    <taxon_hierarchy>genus Pogonatum;species sphaerothecium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polytrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">sphaerothecium</taxon_name>
    <taxon_hierarchy>genus Polytrichum;species sphaerothecium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small, forming large brownish mats.</text>
      <biological_entity id="o2866" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o2867" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="large" value_original="large" />
        <character is_modifier="true" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
      </biological_entity>
      <relation from="o2866" id="r774" name="forming" negation="false" src="d0_s0" to="o2867" />
    </statement>
    <statement id="d0_s1">
      <text>Stems with 5–6 layers of thick-walled cortical cells.</text>
      <biological_entity id="o2868" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o2869" name="layer" name_original="layers" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s1" to="6" />
      </biological_entity>
      <biological_entity id="o2870" name="cortical" name_original="cortical" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
      <biological_entity id="o2871" name="bud" name_original="cells" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
      <relation from="o2868" id="r775" name="with" negation="false" src="d0_s1" to="o2869" />
      <relation from="o2869" id="r776" name="part_of" negation="false" src="d0_s1" to="o2870" />
      <relation from="o2869" id="r777" name="part_of" negation="false" src="d0_s1" to="o2871" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves incurved and closely appressed to the stem when dry, erect-spreading when moist;</text>
      <biological_entity id="o2872" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="incurved" value_original="incurved" />
        <character constraint="to stem" constraintid="o2873" is_modifier="false" modifier="closely" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o2873" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s2" value="erect-spreading" value_original="erect-spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheath poorly delimited;</text>
      <biological_entity id="o2874" name="sheath" name_original="sheath" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="poorly" name="prominence" src="d0_s3" value="delimited" value_original="delimited" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>marginal lamina expanded and involute, covering the lamellae;</text>
      <biological_entity constraint="marginal" id="o2875" name="lamina" name_original="lamina" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="expanded" value_original="expanded" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity id="o2876" name="lamella" name_original="lamellae" src="d0_s4" type="structure" />
      <relation from="o2875" id="r778" name="covering the" negation="false" src="d0_s4" to="o2876" />
    </statement>
    <statement id="d0_s5">
      <text>perichaetial leaves abruptly narrowed to the blade.</text>
      <biological_entity constraint="perichaetial" id="o2877" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="to blade" constraintid="o2878" is_modifier="false" modifier="abruptly" name="shape" src="d0_s5" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o2878" name="blade" name_original="blade" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Seta arcuate with age.</text>
      <biological_entity id="o2879" name="seta" name_original="seta" src="d0_s6" type="structure">
        <character constraint="with age" constraintid="o2880" is_modifier="false" name="course_or_shape" src="d0_s6" value="arcuate" value_original="arcuate" />
      </biological_entity>
      <biological_entity id="o2880" name="age" name_original="age" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Capsule nearly globose, horizontal to nodding;</text>
      <biological_entity id="o2881" name="capsule" name_original="capsule" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s7" value="globose" value_original="globose" />
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s7" to="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peristome teeth 32, short, attenuate, the alternate teeth often smaller.</text>
      <biological_entity constraint="peristome" id="o2882" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="32" value_original="32" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s8" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o2883" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s8" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="often" name="size" src="d0_s8" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Volcanic rocks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="volcanic rocks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="high" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska; e Asia (Japan); Atlantic Islands (Iceland).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="e Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Iceland)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3b.</number>
  <discussion>Variety vulcanicum is notable for the rather thick, curved seta, nodding, globose capsule, and the stem cortex composed of very thick-walled cells, in contrast to the thin-walled cortex of var. sexangulare. It is probably best considered a variety of P. sexangulare, despite its distinctive peristome and the absence of capsule angles. E. Lawton (1971) described intermediate forms in British Columbia with acute-tipped leaves and angled capsules, but with the stem cortical cells and peristome of var. vulcanicum.</discussion>
  
</bio:treatment>