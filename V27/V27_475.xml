<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">332</other_info_on_meta>
    <other_info_on_meta type="mention_page">336</other_info_on_meta>
    <other_info_on_meta type="treatment_page">344</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">fissidentaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">fissidens</taxon_name>
    <taxon_name authority="R. H. Zander" date="1969" rank="species">appalachensis</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>72: 406, figs. 1–8. 1969,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fissidentaceae;genus fissidens;species appalachensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075572</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 13 × 2–4 mm.</text>
      <biological_entity id="o5476" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s0" to="13" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s0" to="4" to_unit="mm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stem unbranched and branched;</text>
      <biological_entity id="o5477" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>axillary hyaline nodules weak or absent;</text>
      <biological_entity constraint="axillary" id="o5478" name="nodule" name_original="nodules" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="fragility" src="d0_s2" value="weak" value_original="weak" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>central strand weak.</text>
      <biological_entity constraint="central" id="o5479" name="strand" name_original="strand" src="d0_s3" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s3" value="weak" value_original="weak" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves as many as 22 pairs, ligulate to oblong-lanceolate, obtuse-mucronate to acute, to 4 × 0.3–0.5 mm;</text>
      <biological_entity id="o5480" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s4" value="ligulate" value_original="ligulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character char_type="range_value" from="obtuse-mucronate" name="shape" src="d0_s4" to="acute" />
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s4" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s4" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5481" name="pair" name_original="pairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="22" value_original="22" />
      </biological_entity>
      <relation from="o5480" id="r1296" name="as many as" negation="false" src="d0_s4" to="o5481" />
    </statement>
    <statement id="d0_s5">
      <text>dorsal lamina narrowed proximally, ending at insertion or slightly before;</text>
      <biological_entity constraint="dorsal" id="o5482" name="lamina" name_original="lamina" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s5" value="narrowed" value_original="narrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>vaginant laminae 1/2–2/3 leaf length, equal in proximal leaves, unequal in distal leaves, minor lamina ending between margin and costa or rounded and ending on or near costa;</text>
      <biological_entity constraint="vaginant" id="o5483" name="lamina" name_original="laminae" src="d0_s6" type="structure">
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s6" to="2/3" />
      </biological_entity>
      <biological_entity id="o5484" name="leaf" name_original="leaf" src="d0_s6" type="structure">
        <character constraint="in proximal leaves" constraintid="o5485" is_modifier="false" name="length" src="d0_s6" value="equal" value_original="equal" />
        <character constraint="in distal leaves" constraintid="o5486" is_modifier="false" name="size" notes="" src="d0_s6" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o5485" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o5486" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="minor" id="o5487" name="lamina" name_original="lamina" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o5488" name="margin" name_original="margin" src="d0_s6" type="structure" />
      <biological_entity id="o5489" name="costa" name_original="costa" src="d0_s6" type="structure" />
      <biological_entity id="o5490" name="costa" name_original="costa" src="d0_s6" type="structure" />
      <relation from="o5487" id="r1297" name="ending between" negation="false" src="d0_s6" to="o5488" />
      <relation from="o5487" id="r1298" name="ending between" negation="false" src="d0_s6" to="o5489" />
      <relation from="o5487" id="r1299" name="ending on" negation="false" src="d0_s6" to="o5490" />
    </statement>
    <statement id="d0_s7">
      <text>margin entire but sometimes serrulate distally, limbate on all laminae, limbidium confluent at apex or ending a few cells before, limbidial cells 2-stratose to 5-stratose;</text>
      <biological_entity id="o5491" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes; distally" name="architecture_or_shape" src="d0_s7" value="serrulate" value_original="serrulate" />
        <character constraint="on laminae" constraintid="o5492" is_modifier="false" name="coloration" src="d0_s7" value="limbate" value_original="limbate" />
      </biological_entity>
      <biological_entity id="o5492" name="lamina" name_original="laminae" src="d0_s7" type="structure" />
      <biological_entity id="o5493" name="limbidium" name_original="limbidium" src="d0_s7" type="structure">
        <character constraint="at " constraintid="o5495" is_modifier="false" name="arrangement" src="d0_s7" value="confluent" value_original="confluent" />
      </biological_entity>
      <biological_entity id="o5494" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o5495" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o5496" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="limbidial" value_original="limbidial" />
        <character char_type="range_value" from="2-stratose" name="architecture" src="d0_s7" to="5-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>costa percurrent or ending in mucro, bryoides-type;</text>
      <biological_entity id="o5497" name="costa" name_original="costa" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="percurrent" value_original="percurrent" />
        <character name="architecture" src="d0_s8" value="ending in mucro" value_original="ending in mucro" />
        <character is_modifier="false" name="architecture_ref_taxa" src="d0_s8" value="bryoides-type" value_original="bryoides-type" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>laminal cells 1-stratose to irregularly 2-stratose, smooth, slightly bulging, firm-walled, irregularly hexagonal, many somewhat elongate, 10–16 µm, juxtacostal and basal-cells of vaginant laminae somewhat larger, quadrate to oblong.</text>
      <biological_entity constraint="laminal" id="o5498" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character char_type="range_value" from="1-stratose" name="architecture" src="d0_s9" to="irregularly 2-stratose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="pubescence_or_shape" src="d0_s9" value="bulging" value_original="bulging" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="firm-walled" value_original="firm-walled" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s9" value="hexagonal" value_original="hexagonal" />
        <character is_modifier="false" name="quantity" src="d0_s9" value="many" value_original="many" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s9" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="10" from_unit="um" name="some_measurement" src="d0_s9" to="16" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="vaginant" id="o5500" name="lamina" name_original="laminae" src="d0_s9" type="structure" />
      <relation from="o5499" id="r1300" name="part_of" negation="false" src="d0_s9" to="o5500" />
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition rhizautoicous or synoicous;</text>
      <biological_entity id="o5499" name="basal-cell" name_original="basal-cells" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="somewhat" name="size" src="d0_s9" value="larger" value_original="larger" />
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s9" to="oblong" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="synoicous" value_original="synoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>perigonia and perichaetia on elongate stems.</text>
      <biological_entity id="o5501" name="perigonium" name_original="perigonia" src="d0_s11" type="structure" />
      <biological_entity id="o5502" name="perichaetium" name_original="perichaetia" src="d0_s11" type="structure" />
      <biological_entity id="o5503" name="stem" name_original="stems" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="elongate" value_original="elongate" />
      </biological_entity>
      <relation from="o5501" id="r1301" name="on" negation="false" src="d0_s11" to="o5503" />
      <relation from="o5502" id="r1302" name="on" negation="false" src="d0_s11" to="o5503" />
    </statement>
    <statement id="d0_s12">
      <text>Sporophytes 1–2 per perichaetium.</text>
      <biological_entity id="o5504" name="sporophyte" name_original="sporophytes" src="d0_s12" type="structure">
        <character char_type="range_value" constraint="per perichaetium" constraintid="o5505" from="1" name="quantity" src="d0_s12" to="2" />
      </biological_entity>
      <biological_entity id="o5505" name="perichaetium" name_original="perichaetium" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Seta 4–4.5 mm.</text>
      <biological_entity id="o5506" name="seta" name_original="seta" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsule theca exserted, erect, radially symmetric to slightly inclined, bilaterally symmetric, 0.6–0.85 mm;</text>
      <biological_entity constraint="capsule" id="o5507" name="theca" name_original="theca" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s14" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s14" value="inclined" value_original="inclined" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s14" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s14" to="0.85" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>peristome bryoides-type;</text>
      <biological_entity id="o5508" name="peristome" name_original="peristome" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_ref_taxa" src="d0_s15" value="bryoides-type" value_original="bryoides-type" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>operculum 0.4 mm.</text>
      <biological_entity id="o5509" name="operculum" name_original="operculum" src="d0_s16" type="structure">
        <character name="some_measurement" src="d0_s16" unit="mm" value="0.4" value_original="0.4" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Calyptra not seen.</text>
      <biological_entity id="o5510" name="calyptra" name_original="calyptra" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Spores 14–25 µm.</text>
      <biological_entity id="o5511" name="spore" name_original="spores" src="d0_s18" type="structure">
        <character char_type="range_value" from="14" from_unit="um" name="some_measurement" src="d0_s18" to="25" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Crevices and surfaces of rocks submerged in swiftly flowing but usually shallow water, sometimes partially emergent</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="crevices" constraint="of rocks" />
        <character name="habitat" value="surfaces" constraint="of rocks" />
        <character name="habitat" value="rocks" />
        <character name="habitat" value="shallow water" modifier="submerged in swiftly flowing but usually" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C., Pa., S.C., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <discussion>Fissidens appalachensis, restricted to rapidly moving water, is distinguished by its habitat and strong limbidium. It is most likely to be confused with an expression of F. bryoides that is usually found on wet rocks and stones along the edges of streams, but which differs in its smaller size and weaker limbidium that at times can be partially or completely absent. Laminal cells in size and shape, vaginant laminae, sexuality, thecae, and peristomes in both taxa are similar.</discussion>
  
</bio:treatment>