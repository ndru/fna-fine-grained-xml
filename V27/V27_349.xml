<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">227</other_info_on_meta>
    <other_info_on_meta type="mention_page">228</other_info_on_meta>
    <other_info_on_meta type="mention_page">249</other_info_on_meta>
    <other_info_on_meta type="mention_page">254</other_info_on_meta>
    <other_info_on_meta type="mention_page">257</other_info_on_meta>
    <other_info_on_meta type="treatment_page">253</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">grimmia</taxon_name>
    <taxon_name authority="Limpricht" date="1889" rank="subgenus">rhabdogrimmia</taxon_name>
    <taxon_name authority="De Notaris" date="1837" rank="species">lisae</taxon_name>
    <place_of_publication>
      <publication_title>Muscol. Ital. Spic.,</publication_title>
      <place_in_publication>15. 1837,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus grimmia;subgenus rhabdogrimmia;species lisae</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443459</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="Durieu &amp; Montagne" date="unknown" rank="species">ancistrodes</taxon_name>
    <taxon_hierarchy>genus Grimmia;species ancistrodes;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="Kindberg" date="unknown" rank="species">arcuatifolia</taxon_name>
    <taxon_hierarchy>genus Grimmia;species arcuatifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="Sullivant" date="unknown" rank="species">californica</taxon_name>
    <taxon_hierarchy>genus Grimmia;species californica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="Kindberg" date="unknown" rank="species">canadensis</taxon_name>
    <taxon_hierarchy>genus Grimmia;species canadensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="(Holzinger) Cardot" date="unknown" rank="species">flettii</taxon_name>
    <taxon_hierarchy>genus Grimmia;species flettii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in dense to loose tufts, olivaceous, brownish to blackish proximally.</text>
      <biological_entity id="o3495" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="olivaceous" value_original="olivaceous" />
        <character char_type="range_value" from="brownish" modifier="proximally" name="coloration" src="d0_s0" to="blackish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o3496" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o3495" id="r808" name="in" negation="false" src="d0_s0" to="o3496" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–4 cm, central strand present.</text>
      <biological_entity id="o3497" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o3498" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect and appressed when dry, recurved to squarrose when moist, broadly lanceolate, tapering to an acute apex, 1.5–2.5 × 0.4–0.6 mm, keeled, margins recurved on one or both sides, awns absent to rather long, stout and denticulate, costa reniform, projecting on dorsal side, median layer of stereids present;</text>
      <biological_entity id="o3499" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when dry" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="recurved" modifier="when moist" name="orientation" src="d0_s2" to="squarrose" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character constraint="to apex" constraintid="o3500" is_modifier="false" name="shape" src="d0_s2" value="tapering" value_original="tapering" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" notes="" src="d0_s2" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" notes="" src="d0_s2" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o3500" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o3501" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character constraint="on sides" constraintid="o3502" is_modifier="false" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o3502" name="side" name_original="sides" src="d0_s2" type="structure" />
      <biological_entity id="o3503" name="awn" name_original="awns" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="rather" name="length_or_size" src="d0_s2" value="long" value_original="long" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
        <character is_modifier="false" name="shape" src="d0_s2" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o3504" name="costa" name_original="costa" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="reniform" value_original="reniform" />
        <character constraint="on dorsal side" constraintid="o3505" is_modifier="false" name="orientation" src="d0_s2" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o3505" name="side" name_original="side" src="d0_s2" type="structure" />
      <biological_entity constraint="median" id="o3506" name="layer" name_original="layer" src="d0_s2" type="structure" />
      <biological_entity id="o3507" name="stereid" name_original="stereids" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o3506" id="r809" name="consist_of" negation="false" src="d0_s2" to="o3507" />
    </statement>
    <statement id="d0_s3">
      <text>basal juxtacostal laminal cells short-rectangular to occasionally elongate, straight to slightly sinuose, thin to thick-walled;</text>
      <biological_entity constraint="basal" id="o3508" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="laminal" id="o3509" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character char_type="range_value" from="short-rectangular" name="shape" src="d0_s3" to="occasionally elongate" />
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s3" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal marginal laminal cells quadrate to short-rectangular, thickened transverse walls;</text>
      <biological_entity constraint="basal marginal laminal" id="o3510" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s4" to="short-rectangular" />
      </biological_entity>
      <biological_entity id="o3511" name="wall" name_original="walls" src="d0_s4" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s4" value="thickened" value_original="thickened" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s4" value="transverse" value_original="transverse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>medial laminal cells oblate to roundedquadrate, straight, thick-walled;</text>
      <biological_entity constraint="medial laminal" id="o3512" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblate" name="shape" src="d0_s5" to="roundedquadrate" />
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal laminal cells 1-stratose with 2-stratose ridges, margins 2-stratose.</text>
      <biological_entity constraint="distal laminal" id="o3513" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character constraint="with ridges" constraintid="o3514" is_modifier="false" name="architecture" src="d0_s6" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <biological_entity id="o3514" name="ridge" name_original="ridges" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity id="o3515" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Gemmae in clusters, multicellular, occasionally present in leaf-axils.</text>
      <biological_entity id="o3517" name="leaf-axil" name_original="leaf-axils" src="d0_s7" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s7" value="cluster" value_original="cluster" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="multicellular" value_original="multicellular" />
        <character is_modifier="true" modifier="occasionally" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o3516" id="r810" name="in" negation="false" src="d0_s7" to="o3517" />
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition dioicous.</text>
      <biological_entity id="o3516" name="gemma" name_original="gemmae" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seta arcuate, 3–4.5 mm.</text>
      <biological_entity id="o3518" name="seta" name_original="seta" src="d0_s9" type="structure">
        <character is_modifier="false" name="course_or_shape" src="d0_s9" value="arcuate" value_original="arcuate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsule occasionally present, exserted, ovoid, brown, shiny, weakly striate, exothecial cells thin-walled, annulus present, operculum rostrate, peristome teeth orange, fully-developed to irregularly cleft at apex, papillose.</text>
      <biological_entity id="o3519" name="capsule" name_original="capsule" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="occasionally" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="brown" value_original="brown" />
        <character is_modifier="false" name="reflectance" src="d0_s10" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="weakly" name="coloration_or_pubescence_or_relief" src="d0_s10" value="striate" value_original="striate" />
      </biological_entity>
      <biological_entity constraint="exothecial" id="o3520" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <biological_entity id="o3521" name="annulus" name_original="annulus" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o3522" name="operculum" name_original="operculum" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rostrate" value_original="rostrate" />
      </biological_entity>
      <biological_entity constraint="peristome" id="o3523" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="orange" value_original="orange" />
        <character is_modifier="false" name="development" src="d0_s10" value="fully-developed" value_original="fully-developed" />
        <character constraint="at apex" constraintid="o3524" is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s10" value="cleft" value_original="cleft" />
        <character is_modifier="false" name="relief" notes="" src="d0_s10" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o3524" name="apex" name_original="apex" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Calyptra mitrate.</text>
      <biological_entity id="o3525" name="calyptra" name_original="calyptra" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="mitrate" value_original="mitrate" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry acidic to basic rock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry acidic to basic rock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (60-1000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="60" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Oreg., Wash.; Mexico; Eurasia; n Africa; Pacific Islands (Hawaii).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>34.</number>
  <discussion>Grimmia lisae is a thermophilous species with a preference for subtropical coastal areas. In North America, it occurs along the west coast, from Vancouver Island south to Mexico. From that region, it has been described frequently as a new species. It is closely related to G. trichophylla, but is distinguished by somewhat shorter and broader leaves that are straight and appressed when dry and recurved to squarrose when moist, and by a reniform costa. Furthermore, it is characterized by a grass-green mid leaf areolation with small, rounded, frequently oblate cells with straight walls.</discussion>
  
</bio:treatment>