<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">540</other_info_on_meta>
    <other_info_on_meta type="treatment_page">556</other_info_on_meta>
    <other_info_on_meta type="illustration_page">556</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="(Herzog) Hilpert" date="1933" rank="subfamily">barbuloideae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">didymodon</taxon_name>
    <taxon_name authority="Otnyukova" date="2002" rank="species">murrayae</taxon_name>
    <place_of_publication>
      <publication_title>Arctoa</publication_title>
      <place_in_publication>11: 345, fig. 6. 2002 (as murrayeae),</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily barbuloideae;genus didymodon;species murrayae</taxon_hierarchy>
    <other_info_on_name type="fna_id">242444290</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants green to redbrown.</text>
      <biological_entity id="o4376" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="redbrown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems to 1 cm, central strand present.</text>
      <biological_entity id="o4377" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o4378" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves weakly appressed when dry, weakly spreading and not keeled when moist, monomorphic, oblong-lanceolate, grooved adaxially along the costa near leaf apex, 0.6–0.9 mm, base oblong in shape, margins broadly recurved in proximal 3/4 of leaf to near apex, entire but often notched laterally, apex subulate to irregularly dentate-clavate, caducous (deciduous early), commonly ending in a conical cell;</text>
      <biological_entity id="o4379" name="leaf-stem" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when dry" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="weakly" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="when moist" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character constraint="along costa" constraintid="o4380" is_modifier="false" name="architecture" src="d0_s2" value="grooved" value_original="grooved" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="distance" notes="" src="d0_s2" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4380" name="costa" name_original="costa" src="d0_s2" type="structure" />
      <biological_entity constraint="leaf" id="o4381" name="apex" name_original="apex" src="d0_s2" type="structure" />
      <biological_entity id="o4382" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o4383" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character constraint="in proximal 3/4" constraintid="o4384" is_modifier="false" modifier="broadly" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="often; laterally" name="shape" src="d0_s2" value="notched" value_original="notched" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o4384" name="3/4" name_original="3/4" src="d0_s2" type="structure" />
      <biological_entity id="o4385" name="leaf" name_original="leaf" src="d0_s2" type="structure" />
      <biological_entity id="o4386" name="apex" name_original="apex" src="d0_s2" type="structure" />
      <biological_entity id="o4387" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s2" to="irregularly dentate-clavate" />
        <character is_modifier="false" name="duration" src="d0_s2" value="caducous" value_original="caducous" />
      </biological_entity>
      <biological_entity id="o4388" name="cell" name_original="cell" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="conical" value_original="conical" />
      </biological_entity>
      <relation from="o4380" id="r1046" name="near" negation="false" src="d0_s2" to="o4381" />
      <relation from="o4384" id="r1047" name="part_of" negation="false" src="d0_s2" to="o4385" />
      <relation from="o4384" id="r1048" name="to" negation="false" src="d0_s2" to="o4386" />
      <relation from="o4387" id="r1049" modifier="commonly" name="ending in a" negation="false" src="d0_s2" to="o4388" />
    </statement>
    <statement id="d0_s3">
      <text>costa percurrent to excurrent, not strongly spurred, not much widened or tapering, without a pad of cells, adaxial costal cells rectangular, ca. 4 cells wide at mid leaf, guide cells in 1 layer;</text>
      <biological_entity id="o4389" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character char_type="range_value" from="percurrent" name="architecture" src="d0_s3" to="excurrent" />
        <character is_modifier="false" modifier="not strongly" name="architecture_or_shape" src="d0_s3" value="spurred" value_original="spurred" />
        <character is_modifier="false" modifier="not much" name="width" src="d0_s3" value="widened" value_original="widened" />
        <character is_modifier="false" name="shape" src="d0_s3" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o4390" name="pad" name_original="pad" src="d0_s3" type="structure" />
      <biological_entity id="o4391" name="cell" name_original="cells" src="d0_s3" type="structure" />
      <biological_entity constraint="adaxial costal" id="o4392" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rectangular" value_original="rectangular" />
        <character name="quantity" src="d0_s3" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o4393" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character constraint="at mid leaf" constraintid="o4394" is_modifier="false" name="width" src="d0_s3" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity constraint="mid" id="o4394" name="leaf" name_original="leaf" src="d0_s3" type="structure" />
      <biological_entity constraint="guide" id="o4395" name="cell" name_original="cells" src="d0_s3" type="structure" />
      <biological_entity id="o4396" name="layer" name_original="layer" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
      <relation from="o4389" id="r1050" name="without" negation="false" src="d0_s3" to="o4390" />
      <relation from="o4390" id="r1051" name="part_of" negation="false" src="d0_s3" to="o4391" />
      <relation from="o4395" id="r1052" name="in" negation="false" src="d0_s3" to="o4396" />
    </statement>
    <statement id="d0_s4">
      <text>basal laminal cells weakly differentiated medially, walls thin to evenly thickened, shortrectangular, not perforated;</text>
      <biological_entity constraint="basal laminal" id="o4397" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="weakly; medially" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o4398" name="wall" name_original="walls" src="d0_s4" type="structure">
        <character char_type="range_value" from="thin" name="width" src="d0_s4" to="evenly thickened" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s4" value="perforated" value_original="perforated" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal laminal cells 8–10 µm wide, 1: 1, smooth or weakly conic-mammillose at apex, lumens quadrate, walls thin, weakly convex on both sides of lamina, distal leaf margins 2-stratose in deciduous part of apex.</text>
      <biological_entity constraint="distal laminal" id="o4399" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s5" to="10" to_unit="um" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character is_modifier="false" name="relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character constraint="at apex" constraintid="o4400" is_modifier="false" modifier="weakly" name="relief" src="d0_s5" value="conic-mammillose" value_original="conic-mammillose" />
      </biological_entity>
      <biological_entity id="o4400" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <biological_entity id="o4401" name="lumen" name_original="lumens" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="quadrate" value_original="quadrate" />
      </biological_entity>
      <biological_entity id="o4402" name="wall" name_original="walls" src="d0_s5" type="structure">
        <character is_modifier="false" name="width" src="d0_s5" value="thin" value_original="thin" />
        <character constraint="on sides" constraintid="o4403" is_modifier="false" modifier="weakly" name="shape" src="d0_s5" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o4403" name="side" name_original="sides" src="d0_s5" type="structure" />
      <biological_entity id="o4404" name="lamina" name_original="lamina" src="d0_s5" type="structure" />
      <biological_entity id="o4406" name="part" name_original="part" src="d0_s5" type="structure">
        <character is_modifier="true" name="duration" src="d0_s5" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity id="o4407" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <relation from="o4403" id="r1053" name="part_of" negation="false" src="d0_s5" to="o4404" />
      <relation from="o4406" id="r1054" name="part_of" negation="false" src="d0_s5" to="o4407" />
    </statement>
    <statement id="d0_s6">
      <text>Specialized asexual reproduction by thickened, caducous leaf apices, often toothed.</text>
      <biological_entity constraint="leaf" id="o4408" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="asexual" value_original="asexual" />
        <character is_modifier="true" name="size_or_width" src="d0_s6" value="thickened" value_original="thickened" />
        <character is_modifier="true" name="duration" src="d0_s6" value="caducous" value_original="caducous" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition unknown.</text>
      <biological_entity constraint="leaf" id="o4405" name="margin" name_original="margins" src="d0_s5" type="structure" constraint_original="distal leaf">
        <character constraint="in part" constraintid="o4406" is_modifier="false" name="architecture" src="d0_s5" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" name="development" src="d0_s6" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Sporophytes unknown.</text>
    </statement>
    <statement id="d0_s9">
      <text>Distal laminal KOH reaction red.</text>
      <biological_entity id="o4409" name="sporophyte" name_original="sporophytes" src="d0_s8" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character is_modifier="false" name="structure_in_adjective_form" src="d0_s9" value="laminal" value_original="laminal" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="red" value_original="red" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Willow limbs and dead tree bark</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="willow limbs" />
        <character name="habitat" value="dead tree bark" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <discussion>Didymodon murrayae was originally reported (R. H. Zander 1978f) as D. sinuosus (Mitten) Delogne, a European taxon that differs in its rupestral habitat, much larger leaves, quadrate adaxial costal cells and merely fragile leaf apex. Trichostomum tenuirostre may occasionally have similar sequentially constricted, fragile leaves, but the leaf cells are papillose and the plane margins are crenulate by projecting walls. Didymodon murrayae is similar to two other Asian species, D. gaochenii and D. hedysariformis (T. N. Otnyukova 2002), but the former differs in the rounded apex of the propagulum (no teeth), and the latter in its propagulum contorted in appearance before dispersal.</discussion>
  
</bio:treatment>