<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Donna H. Miller,Harvey A. Miller</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">10</other_info_on_meta>
    <other_info_on_meta type="mention_page">11</other_info_on_meta>
    <other_info_on_meta type="mention_page">12</other_info_on_meta>
    <other_info_on_meta type="mention_page">13</other_info_on_meta>
    <other_info_on_meta type="mention_page">181</other_info_on_meta>
    <other_info_on_meta type="mention_page">189</other_info_on_meta>
    <other_info_on_meta type="mention_page">192</other_info_on_meta>
    <other_info_on_meta type="mention_page">193</other_info_on_meta>
    <other_info_on_meta type="mention_page">203</other_info_on_meta>
    <other_info_on_meta type="treatment_page">188</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwägrichen" date="unknown" rank="family">funariaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">FUNARIA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>172. 1801  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family funariaceae;genus FUNARIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin funis, rope, alluding to cord-like twisted seta</other_info_on_name>
    <other_info_on_name type="fna_id">113074</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to medium-sized, gregarious or tufted, bright green to yellowish green.</text>
      <biological_entity id="o782" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="medium-sized" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="gregarious" value_original="gregarious" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character char_type="range_value" from="bright green" name="coloration" src="d0_s0" to="yellowish green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems short, erect, simple except for a short basal antheridial branch.</text>
      <biological_entity id="o783" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="except-for basal antheridial, branch" constraintid="o784, o785" is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity constraint="basal" id="o784" name="antheridial" name_original="antheridial" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
      <biological_entity constraint="basal" id="o785" name="branch" name_original="branch" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves larger and erect distally, reduced proximally, oblong-ovate to broadly obovate distally;</text>
    </statement>
    <statement id="d0_s3">
      <text>concave;</text>
      <biological_entity id="o786" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="larger" value_original="larger" />
        <character is_modifier="false" modifier="distally" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="proximally" name="size" src="d0_s2" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="oblong-ovate" modifier="distally" name="shape" src="d0_s2" to="broadly obovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="concave" value_original="concave" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apex usually acute or acuminate margins erect, entire to serrate beyond middle;</text>
      <biological_entity id="o787" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o788" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character char_type="range_value" constraint="beyond middle margins" constraintid="o789" from="entire" name="architecture_or_shape" src="d0_s4" to="serrate" />
      </biological_entity>
      <biological_entity constraint="middle" id="o789" name="margin" name_original="margins" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>costa single, ending before the tip to excurrent;</text>
      <biological_entity id="o790" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s5" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o791" name="tip" name_original="tip" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <relation from="o790" id="r233" name="ending before" negation="false" src="d0_s5" to="o791" />
    </statement>
    <statement id="d0_s6">
      <text>distal and medial laminal cells large, rhombic-hexagonal to rectangular, lax and rather thin-walled, proximal cells oblong-rectangular, differentiated alar cells absent.</text>
      <biological_entity constraint="distal and medial laminal" id="o792" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="large" value_original="large" />
        <character char_type="range_value" from="rhombic-hexagonal" name="shape" src="d0_s6" to="rectangular" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="lax" value_original="lax" />
        <character is_modifier="false" modifier="rather" name="architecture" src="d0_s6" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o793" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong-rectangular" value_original="oblong-rectangular" />
        <character is_modifier="false" name="variability" src="d0_s6" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition autoicous;</text>
      <biological_entity id="o794" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>antheridial branches 1–2, basal, perigonial paraphyses clavate with an enlarged inflated cell;</text>
      <biological_entity constraint="antheridial" id="o795" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s8" to="2" />
        <character is_modifier="false" name="position" src="d0_s8" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity constraint="perigonial" id="o796" name="paraphyse" name_original="paraphyses" src="d0_s8" type="structure">
        <character constraint="with cell" constraintid="o797" is_modifier="false" name="shape" src="d0_s8" value="clavate" value_original="clavate" />
      </biological_entity>
      <biological_entity id="o797" name="cell" name_original="cell" src="d0_s8" type="structure">
        <character is_modifier="true" name="size" src="d0_s8" value="enlarged" value_original="enlarged" />
        <character is_modifier="true" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perichaetia apparently absent paraphyses.</text>
      <biological_entity id="o798" name="perichaetium" name_original="perichaetia" src="d0_s9" type="structure" />
      <biological_entity id="o799" name="paraphyse" name_original="paraphyses" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="apparently" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seta elongate, erect to strongly curved or twisted.</text>
      <biological_entity id="o800" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="strongly" name="course" src="d0_s10" value="curved" value_original="curved" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="twisted" value_original="twisted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsule exserted, usually inclined to pendent, asymmetric and usually curved, yellow to brown, pyriform, often sulcate or plicate when dry and empty, annulus large and revoluble or not differentiated, exothecial cells oblong-hexagonal to linear, walls incrassate especially so on inner tangential wall, stomata immersed;</text>
      <biological_entity id="o801" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="usually inclined" name="orientation" src="d0_s11" to="pendent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="asymmetric" value_original="asymmetric" />
        <character is_modifier="false" modifier="usually" name="course" src="d0_s11" value="curved" value_original="curved" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s11" to="brown" />
        <character is_modifier="false" name="shape" src="d0_s11" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" modifier="when dry and empty" name="architecture" src="d0_s11" value="sulcate" value_original="sulcate" />
        <character is_modifier="false" modifier="when dry and empty" name="architecture" src="d0_s11" value="plicate" value_original="plicate" />
      </biological_entity>
      <biological_entity id="o802" name="annulus" name_original="annulus" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="large" value_original="large" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="revoluble" value_original="revoluble" />
        <character is_modifier="false" modifier="not" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity constraint="exothecial" id="o803" name="bud" name_original="cells" src="d0_s11" type="structure">
        <character char_type="range_value" from="oblong-hexagonal" name="shape" src="d0_s11" to="linear" />
      </biological_entity>
      <biological_entity id="o804" name="wall" name_original="walls" src="d0_s11" type="structure">
        <character constraint="on inner tangential wall" constraintid="o805" is_modifier="false" modifier="especially" name="size" src="d0_s11" value="incrassate" value_original="incrassate" />
      </biological_entity>
      <biological_entity constraint="inner tangential" id="o805" name="wall" name_original="wall" src="d0_s11" type="structure" />
      <biological_entity id="o806" name="stoma" name_original="stomata" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s11" value="immersed" value_original="immersed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>peristome double, inserted somewhat below the mouth, teeth well developed, obliquely directed, lance-acuminate, papillose-striate, often strongly trabeculate, frequently appendiculate at the tips and fusing with a latticed disk, endostome segments opposite the teeth, 1/6 or more the length of the teeth, papillose or weakly papillose-striate with a basal membrane and cilia absent.</text>
      <biological_entity id="o807" name="peristome" name_original="peristome" src="d0_s12" type="structure">
        <character constraint="somewhat below mouth" constraintid="o808" is_modifier="false" name="position" src="d0_s12" value="inserted" value_original="inserted" />
      </biological_entity>
      <biological_entity id="o808" name="mouth" name_original="mouth" src="d0_s12" type="structure" />
      <biological_entity id="o809" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s12" value="developed" value_original="developed" />
        <character is_modifier="false" modifier="obliquely" name="orientation" src="d0_s12" value="directed" value_original="directed" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lance-acuminate" value_original="lance-acuminate" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s12" value="papillose-striate" value_original="papillose-striate" />
        <character is_modifier="false" modifier="often strongly" name="architecture" src="d0_s12" value="trabeculate" value_original="trabeculate" />
        <character constraint="at tips; at the tips and fusing" constraintid="o811" is_modifier="false" modifier="frequently" name="architecture" src="d0_s12" value="appendiculate" value_original="appendiculate" />
      </biological_entity>
      <biological_entity id="o810" name="tip" name_original="tips" src="d0_s12" type="structure" />
      <biological_entity id="o811" name="tip" name_original="tips" src="d0_s12" type="structure" />
      <biological_entity id="o812" name="disk" name_original="disk" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="latticed" value_original="latticed" />
      </biological_entity>
      <biological_entity constraint="endostome" id="o813" name="segment" name_original="segments" src="d0_s12" type="structure">
        <character is_modifier="false" name="length" src="d0_s12" value="papillose" value_original="papillose" />
        <character constraint="with basal membrane" constraintid="o816" is_modifier="false" modifier="weakly" name="length" src="d0_s12" value="papillose-striate" value_original="papillose-striate" />
      </biological_entity>
      <biological_entity id="o814" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="1/6" value_original="1/6" />
      </biological_entity>
      <biological_entity id="o815" name="tooth" name_original="teeth" src="d0_s12" type="structure" />
      <biological_entity constraint="basal" id="o816" name="membrane" name_original="membrane" src="d0_s12" type="structure" />
      <biological_entity id="o817" name="cilium" name_original="cilia" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o810" id="r234" name="with" negation="false" src="d0_s12" to="o812" />
      <relation from="o813" id="r235" name="opposite" negation="false" src="d0_s12" to="o814" />
    </statement>
    <statement id="d0_s13">
      <text>Operculum usually oblique to the axis of the capsule, convex to weakly conic, cells in obliquely radial rows.</text>
      <biological_entity id="o818" name="operculum" name_original="operculum" src="d0_s13" type="structure">
        <character constraint="to axis" constraintid="o819" is_modifier="false" modifier="usually" name="orientation_or_shape" src="d0_s13" value="oblique" value_original="oblique" />
        <character char_type="range_value" from="convex" name="shape" notes="" src="d0_s13" to="weakly conic" />
      </biological_entity>
      <biological_entity id="o819" name="axis" name_original="axis" src="d0_s13" type="structure" />
      <biological_entity id="o820" name="capsule" name_original="capsule" src="d0_s13" type="structure" />
      <biological_entity id="o821" name="bud" name_original="cells" src="d0_s13" type="structure" />
      <biological_entity id="o822" name="row" name_original="rows" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="obliquely" name="arrangement" src="d0_s13" value="radial" value_original="radial" />
      </biological_entity>
      <relation from="o819" id="r236" name="part_of" negation="false" src="d0_s13" to="o820" />
      <relation from="o821" id="r237" name="in" negation="false" src="d0_s13" to="o822" />
    </statement>
    <statement id="d0_s14">
      <text>Calyptra large, cucullate, usually smooth, and often long-rostrate.</text>
      <biological_entity id="o823" name="calyptra" name_original="calyptra" src="d0_s14" type="structure">
        <character is_modifier="false" name="size" src="d0_s14" value="large" value_original="large" />
        <character is_modifier="false" name="shape" src="d0_s14" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s14" value="long-rostrate" value_original="long-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Spores spherical, smooth or papillose to baccate-insulate.</text>
      <biological_entity id="o824" name="spore" name_original="spores" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="spherical" value_original="spherical" />
        <character char_type="range_value" from="papillose" name="relief" src="d0_s15" to="baccate-insulate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Central America, South America, Europe, Asia (including Indonesia), Africa, Pacific Islands, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia (including Indonesia)" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Species ca. 200 (9 in the flora).</discussion>
  <discussion>Funaria comprises mainly small to medium seasonal mosses growing on moist mineral or peaty soils in strong light. For the most part, they are relatively short-lived pioneer species adapted to complete the life cycle by producing many spores quickly, in a cool, moist, bright (but not sunny for long periods), exposed, disturbed habitat. In North America, the best time to look for members of the family is spring before the soil dries out. The most common species can be recognized by the production of large numbers of sporophytes bearing a double peristome with inner and outer teeth opposite rather than alternate as is typical for most mosses. The teeth tend to be torqued in one direction with the tips of the exostome adhering weakly to a few-celled disk. Because the sporophyte shows more morphologic diversity than the gametophyte, it is often essential for identification. H. A. Crum and L. E. Anderson (1981) discussed the indistinct generic limits between Funaria and Entosthodon and the application of generic names.</discussion>
  <references>
    <reference>Bartram, E. B. 1928. Studies in Funaria from southwestern United States. Bryologist 31: 89–96</reference>
    <reference>Ireland, R. R. 1971c. Funaria. In: E. Lawton. 1971. Moss Flora of the Pacific Northwest. Nichinan. Pp. 152–154.</reference>
    <reference>Fife, A. J. 1979. Taxonomic observations on three species of North American Funaria. Bryologist 82: 204–214.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Annulus absent</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Annulus large, revoluble</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Distal leaves with a long, flexuose, excurrent costa with a hyaline tip.</description>
      <determination>6 Funaria apiculatopilosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Distal leaves acute to narrowly acuminate, costa percurrent or, if excurrent, straight and concolorous</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves with acute tips, distal marginal cells serrate from a series of elongate blade cells.</description>
      <determination>9 Funaria serrata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves with acuminate tips, marginal cells not different from the blade cells</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Costa ending in a narrowly acuminate tip or excurrent, endostome segments broadly triangular proximally and nearly as long as the exostome teeth, east of Rockies.</description>
      <determination>7 Funaria americana</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Costa ending before the slender, filiform acumination, endostome segments narrowly lanceolate and about 2/3 the length of the exostome teeth, western states.</description>
      <determination>8 Funaria muhlenbergii</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Endostome segments lanceolate, slender pointed, at least 2/3 the length of the exostome teeth</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Endostome segments narrow, less than 1/2 the length of the exostome teeth, or rudimentary</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Seta strongly twisted and hygroscopic, 20-80 mm, capsule with an oblique mouth sometimes almost parallel to the axis of the capsule, leaves acute to acuminate, often with a short excurrent costa.</description>
      <determination>2 Funaria hygrometrica</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Seta curved to cygneous, 8-12 mm, capsule mouth scarcely oblique to the axis of the capsule, leaves broadly acute to obtuse, costa ending before or in the tip.</description>
      <determination>5 Funaria arctica</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Capsule symmetric or nearly so, seta strongly curved, axis of the capsule aligned with the seta.</description>
      <determination>4 Funaria polaris</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Capsule curved to nearly straight, horizontal to pendent from the seta</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Capsule nearly straight and cylindrical, mouth 2/3 or more the diameter of the capsule, leaves tending to be broadly oblong-ovate.</description>
      <determination>1 Funaria flavicans</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Capsule curved and pyriform, mouth 1/2 or less the diameter of the capsule, leaves tending to be oblong-lanceolate to oblanceolate.</description>
      <determination>3 Funaria microstoma</determination>
    </key_statement>
  </key>
</bio:treatment>