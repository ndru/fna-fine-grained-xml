<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Terry T. McIntosh</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">443</other_info_on_meta>
    <other_info_on_meta type="mention_page">444</other_info_on_meta>
    <other_info_on_meta type="mention_page">458</other_info_on_meta>
    <other_info_on_meta type="treatment_page">445</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Limpricht" date="unknown" rank="family">ditrichaceae</taxon_name>
    <taxon_name authority="Bridel" date="1826" rank="genus">CERATODON</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Univ.</publication_title>
      <place_in_publication>1: 480. 1826  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ditrichaceae;genus CERATODON</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek keratos, horn, and odon, tooth, alluding to peristome teeth forked like goat horns</other_info_on_name>
    <other_info_on_name type="fna_id">106178</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in loose to dense tufts, turfs, or mats, green to dark green, brownish green, light green, or yellow-green, often tinged reddish-brown or purple.</text>
      <biological_entity id="o8719" name="whole_organism" name_original="" src="" type="structure">
        <character constraint="to mats" constraintid="o8722" is_modifier="false" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s0" to="dark green brownish green light green or yellow-green" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="dark green brownish green light green or yellow-green" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="dark green brownish green light green or yellow-green" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s0" value="tinged reddish-brown" value_original="tinged reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="purple" value_original="purple" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8720" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o8721" name="turf" name_original="turfs" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o8722" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="dark green brownish green light green or yellow-green" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (0.2–) 1–3 (–4) cm, often branched;</text>
      <biological_entity id="o8723" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rhizoids at base, papillose.</text>
      <biological_entity id="o8724" name="rhizoid" name_original="rhizoids" src="d0_s2" type="structure">
        <character is_modifier="false" name="relief" notes="" src="d0_s2" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o8725" name="base" name_original="base" src="d0_s2" type="structure" />
      <relation from="o8724" id="r2076" name="at" negation="false" src="d0_s2" to="o8725" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves erect-patent, contorted or somewhat crisped, rarely straight when dry, lanceolate, ovatelanceolate, or triangular-lanceolate, or elliptical to ovate and somewhat concave, margins recurved to near apex, rarely plane, irregularly serrate or smooth distally;</text>
      <biological_entity id="o8726" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect-patent" value_original="erect-patent" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s3" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s3" value="crisped" value_original="crisped" />
        <character is_modifier="false" modifier="when dry" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="triangular-lanceolate or elliptical" name="shape" src="d0_s3" to="ovate" />
        <character char_type="range_value" from="triangular-lanceolate or elliptical" name="shape" src="d0_s3" to="ovate" />
        <character char_type="range_value" from="triangular-lanceolate or elliptical" name="shape" src="d0_s3" to="ovate" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s3" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity id="o8727" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="to apex" constraintid="o8728" is_modifier="false" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="rarely" name="shape" notes="" src="d0_s3" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s3" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s3" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o8728" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>costa subpercurrent to excurrent, sometimes as a long, smooth awn, 1 row of guide cells, two stereid bands, hydroid cells present between guide cells and abaxial stereids;</text>
      <biological_entity id="o8729" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="subpercurrent" value_original="subpercurrent" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o8730" name="awn" name_original="awn" src="d0_s4" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character is_modifier="true" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o8731" name="row" name_original="row" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o8732" name="guide" name_original="guide" src="d0_s4" type="structure" />
      <biological_entity id="o8733" name="cell" name_original="cells" src="d0_s4" type="structure" />
      <biological_entity id="o8734" name="band" name_original="bands" src="d0_s4" type="structure" />
      <biological_entity constraint="hydroid" id="o8735" name="cell" name_original="cells" src="d0_s4" type="structure" />
      <biological_entity constraint="guide" id="o8736" name="guide" name_original="guide" src="d0_s4" type="structure" />
      <biological_entity constraint="guide" id="o8737" name="cell" name_original="cells" src="d0_s4" type="structure" />
      <biological_entity constraint="guide abaxial" id="o8738" name="stereid" name_original="stereids" src="d0_s4" type="structure" />
      <relation from="o8729" id="r2077" modifier="sometimes" name="as" negation="false" src="d0_s4" to="o8730" />
      <relation from="o8731" id="r2078" name="part_of" negation="false" src="d0_s4" to="o8732" />
      <relation from="o8731" id="r2079" name="part_of" negation="false" src="d0_s4" to="o8733" />
      <relation from="o8735" id="r2080" name="present between" negation="false" src="d0_s4" to="o8736" />
      <relation from="o8735" id="r2081" name="present between" negation="false" src="d0_s4" to="o8737" />
      <relation from="o8735" id="r2082" name="present between" negation="false" src="d0_s4" to="o8738" />
    </statement>
    <statement id="d0_s5">
      <text>medial laminal cells of somewhat uneven shape and size across leaf, more or less quadrate or shortrectangular, often irregularly angled or rarely rounded, non-pitted.</text>
      <biological_entity id="o8740" name="leaf" name_original="leaf" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="size" notes="alterIDs:o8740" src="d0_s5" value="quadrate" value_original="quadrate" />
      </biological_entity>
      <relation from="o8739" id="r2083" name="across" negation="false" src="d0_s5" to="o8740" />
    </statement>
    <statement id="d0_s6">
      <text>Specialized asexual reproduction absent or as multicellular filamentous propagules with thin walls scattered along the stems or occasionally as rhizome nodules.</text>
      <biological_entity id="o8741" name="propagule" name_original="propagules" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="multicellular" value_original="multicellular" />
        <character is_modifier="true" name="texture" src="d0_s6" value="filamentous" value_original="filamentous" />
      </biological_entity>
      <biological_entity id="o8742" name="wall" name_original="walls" src="d0_s6" type="structure">
        <character is_modifier="true" name="width" src="d0_s6" value="thin" value_original="thin" />
        <character constraint="along " constraintid="o8744" is_modifier="false" name="arrangement" src="d0_s6" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity id="o8743" name="stem" name_original="stems" src="d0_s6" type="structure" />
      <biological_entity constraint="rhizome" id="o8744" name="nodule" name_original="nodules" src="d0_s6" type="structure" />
      <biological_entity id="o8745" name="stem" name_original="stems" src="d0_s6" type="structure" />
      <biological_entity constraint="rhizome" id="o8746" name="nodule" name_original="nodules" src="d0_s6" type="structure" />
      <relation from="o8741" id="r2084" name="with" negation="false" src="d0_s6" to="o8742" />
      <relation from="o8744" id="r2085" name="along" negation="false" src="d0_s6" to="o8745" />
      <relation from="o8744" id="r2086" name="along" negation="false" src="d0_s6" to="o8746" />
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="medial laminal" id="o8739" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character name="size" src="d0_s5" value="shortrectangular" value_original="shortrectangular" />
        <character is_modifier="false" modifier="often irregularly" name="shape" src="d0_s5" value="angled" value_original="angled" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="relief" src="d0_s5" value="non-pitted" value_original="non-pitted" />
        <character is_modifier="false" name="development" src="d0_s6" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>male and female plants about the same size;</text>
      <biological_entity id="o8747" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="male" value_original="male" />
        <character is_modifier="true" name="reproduction" src="d0_s8" value="female" value_original="female" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perigonial leaves ovate, concave, short-acuminate or rarely long-acuminate in well-developed plants;</text>
      <biological_entity constraint="perigonial" id="o8748" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="concave" value_original="concave" />
        <character is_modifier="false" name="shape" src="d0_s9" value="short-acuminate" value_original="short-acuminate" />
        <character constraint="in plants" constraintid="o8749" is_modifier="false" modifier="rarely" name="shape" src="d0_s9" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
      <biological_entity id="o8749" name="plant" name_original="plants" src="d0_s9" type="structure">
        <character is_modifier="true" name="development" src="d0_s9" value="well-developed" value_original="well-developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perichaetial leaves convolute-sheathing, abruptly subulate to gradually acuminate.</text>
      <biological_entity constraint="perichaetial" id="o8750" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="convolute-sheathing" value_original="convolute-sheathing" />
        <character char_type="range_value" from="abruptly subulate" name="shape" src="d0_s10" to="gradually acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seta red, purplish, yellow, or yellow-orange, elongate, twisted when dry, erect.</text>
      <biological_entity id="o8751" name="seta" name_original="seta" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow-orange" value_original="yellow-orange" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow-orange" value_original="yellow-orange" />
        <character is_modifier="false" name="shape" src="d0_s11" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s11" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule erect to inclined or horizontal, exserted, dark red to reddish or purplish brown, to pale-brown, pale-yellow or yellow-orange, oblong-ovoid to oblong-cylindric or cylindric, often somewhat asymmetric and deeply furrowed, smooth to strongly sulcate when dry, usually strumose;</text>
      <biological_entity id="o8752" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s12" to="inclined or horizontal" />
        <character is_modifier="false" name="position" src="d0_s12" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="dark red" name="coloration" src="d0_s12" to="reddish or purplish brown" />
        <character char_type="range_value" from="dark red" name="coloration" src="d0_s12" to="reddish or purplish brown" />
        <character char_type="range_value" from="dark red" name="coloration" src="d0_s12" to="reddish or purplish brown" />
        <character char_type="range_value" from="oblong-ovoid" name="shape" src="d0_s12" to="oblong-cylindric or cylindric" />
        <character is_modifier="false" modifier="often somewhat" name="architecture_or_shape" src="d0_s12" value="asymmetric" value_original="asymmetric" />
        <character is_modifier="false" modifier="deeply" name="architecture" src="d0_s12" value="furrowed" value_original="furrowed" />
        <character char_type="range_value" from="smooth" modifier="when dry" name="architecture" src="d0_s12" to="strongly sulcate" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s12" value="strumose" value_original="strumose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>annulus of 2–3 rows of large, deciduous, revoluble cells;</text>
      <biological_entity id="o8753" name="annulus" name_original="annulus" src="d0_s13" type="structure" />
      <biological_entity id="o8754" name="row" name_original="rows" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s13" to="3" />
      </biological_entity>
      <biological_entity id="o8755" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character is_modifier="true" name="size" src="d0_s13" value="large" value_original="large" />
        <character is_modifier="true" name="duration" src="d0_s13" value="deciduous" value_original="deciduous" />
        <character is_modifier="true" name="architecture" src="d0_s13" value="revoluble" value_original="revoluble" />
      </biological_entity>
      <relation from="o8753" id="r2087" name="consist_of" negation="false" src="d0_s13" to="o8754" />
      <relation from="o8754" id="r2088" name="part_of" negation="false" src="d0_s13" to="o8755" />
    </statement>
    <statement id="d0_s14">
      <text>operculum conic to long-conic, straight;</text>
      <biological_entity id="o8756" name="operculum" name_original="operculum" src="d0_s14" type="structure">
        <character char_type="range_value" from="conic" name="shape" src="d0_s14" to="long-conic" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>peristome single, teeth 16, split nearly to their base into 2 filiform segments free to united at their nodes basally, 5–18 articulations, basal membrane present, finely papillose to spinulose-papillose.</text>
      <biological_entity id="o8757" name="peristome" name_original="peristome" src="d0_s15" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s15" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o8758" name="tooth" name_original="teeth" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="16" value_original="16" />
      </biological_entity>
      <biological_entity id="o8759" name="split" name_original="split" src="d0_s15" type="structure" />
      <biological_entity id="o8760" name="base" name_original="base" src="d0_s15" type="structure" />
      <biological_entity id="o8761" name="segment" name_original="segments" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s15" value="filiform" value_original="filiform" />
        <character char_type="range_value" constraint="at nodes" constraintid="o8762" from="free" name="fusion" src="d0_s15" to="united" />
      </biological_entity>
      <biological_entity id="o8762" name="node" name_original="nodes" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s15" to="18" />
      </biological_entity>
      <biological_entity id="o8763" name="articulation" name_original="articulations" src="d0_s15" type="structure" />
      <biological_entity constraint="basal" id="o8764" name="membrane" name_original="membrane" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character char_type="range_value" from="finely papillose" name="relief" src="d0_s15" to="spinulose-papillose" />
      </biological_entity>
      <relation from="o8759" id="r2089" name="to" negation="false" src="d0_s15" to="o8760" />
      <relation from="o8760" id="r2090" name="into" negation="false" src="d0_s15" to="o8761" />
    </statement>
    <statement id="d0_s16">
      <text>Calyptra cucullate.</text>
      <biological_entity id="o8765" name="calyptra" name_original="calyptra" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="cucullate" value_original="cucullate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Spores globose, smooth to finely papillose.</text>
      <biological_entity id="o8766" name="spore" name_original="spores" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="globose" value_original="globose" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s17" to="finely papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Species 3 (2 in the flora).</discussion>
  <discussion>Historically, Ceratodon has been a troubling genus. Taxonomic interpretations, especially with respect to C. purpureus in the broad sense, have varied widely, mainly because of the high degree of environmental and suspected genetic variation across its range. J. S. Burley and N. M. Pritchard (1990) provided the most thorough treatment of Ceratodon to date, reducing the number of species to four and subspecies of C. purpureus to three. One of their species, C. conicus, is treated here as a subspecies of C. purpureus, based on the apparent gradation and reduction of all of the characters that they used in their treatment. However, there remains a great need for a detailed study of this genus within North America.</discussion>
  <references>
    <reference>Burley, J. S. and N. M. Pritchard. 1990. Revision of the genus Ceratodon (Bryophyta). Harvard Pap. Bot. 2: 17–76.</reference>
    <reference>Snider, J. A. 1994. Ceratodon. In: A. J. Sharp, et al., eds. The moss flora of Mexico. Mem. New York Bot. Gard. 69: 103.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Vegetative leaves elliptic to ovate-lanceolate, concave, obtuse; distal laminal cells usually 12-16 mm, sometimes longer; leaf margins plane to weakly recurved and usually entire; most costa sub-percurrent; capsule ovate to ovate-cylindrical, about 1 mm; spores usually19-21 µm; restricted to Arctic regions.</description>
      <determination>1 Ceratodon heterophyllus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Vegetative leaves usually lanceolate to ovate-lanceolate, acute to short-acuminate; distal laminal cells usually 8-12 mm; leaf margins recurved and usually toothed distally; costa percurrent to long-excurrent; capsule ovate-cylindrical to cylindrical, usually longer, to 3 mm; spores usually 11-14 µm; widespread.</description>
      <determination>2 Ceratodon purpureus</determination>
    </key_statement>
  </key>
</bio:treatment>