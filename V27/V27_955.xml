<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">647</other_info_on_meta>
    <other_info_on_meta type="mention_page">649</other_info_on_meta>
    <other_info_on_meta type="mention_page">653</other_info_on_meta>
    <other_info_on_meta type="treatment_page">648</other_info_on_meta>
    <other_info_on_meta type="illustration_page">645</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">ephemeraceae</taxon_name>
    <taxon_name authority="Austin" date="1870" rank="genus">micromitrium</taxon_name>
    <taxon_name authority="(Bruch &amp; Schimper) Crosby" date="1968" rank="species">tenerum</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>71: 116. 1968,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ephemeraceae;genus micromitrium;species tenerum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250061794</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Phascum</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="unknown" rank="species">tenerum</taxon_name>
    <place_of_publication>
      <publication_title>Laubm. Eur. Monogr., Phascum,</publication_title>
      <place_in_publication>2, plate 1. 1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Phascum;species tenerum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Micromitrium</taxon_name>
    <taxon_name authority="Sullivant" date="unknown" rank="species">austinii</taxon_name>
    <taxon_hierarchy>genus Micromitrium;species austinii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nanomitrium</taxon_name>
    <taxon_name authority="(Sullivant) Lindberg" date="unknown" rank="species">austinii</taxon_name>
    <taxon_hierarchy>genus Nanomitrium;species austinii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nanomitrium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">austinii</taxon_name>
    <taxon_name authority="Grout" date="unknown" rank="variety">floridanum</taxon_name>
    <taxon_hierarchy>genus Nanomitrium;species austinii;variety floridanum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants scattered or gregarious in sparse remnants of protonemata, pale to bright green.</text>
      <biological_entity id="o6716" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s0" value="scattered" value_original="scattered" />
        <character constraint="in remnants" constraintid="o6717" is_modifier="false" name="growth_form" src="d0_s0" value="gregarious" value_original="gregarious" />
        <character char_type="range_value" from="pale" name="coloration" notes="" src="d0_s0" to="bright green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6717" name="remnant" name_original="remnants" src="d0_s0" type="structure">
        <character is_modifier="true" name="count_or_density" src="d0_s0" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o6718" name="protonema" name_original="protonemata" src="d0_s0" type="structure" />
      <relation from="o6717" id="r1591" name="part_of" negation="false" src="d0_s0" to="o6718" />
    </statement>
    <statement id="d0_s1">
      <text>Stems usually absent, but rarely to 1 mm.</text>
      <biological_entity id="o6719" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character char_type="range_value" from="0" from_unit="mm" modifier="rarely" name="some_measurement" src="d0_s1" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves spreading proximally, distally erect and lanceolate to ovate, obovate or ligulate, broadly to slenderly acuminate, less than 2.4 × 0.5 mm, margins plane, entire proximally, distally entire to serrulate.</text>
      <biological_entity id="o6720" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="proximally" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="distally" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="ligulate" value_original="ligulate" />
        <character is_modifier="false" modifier="broadly to slenderly" name="shape" src="d0_s2" value="acuminate" value_original="acuminate" />
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s2" to="2.4" to_unit="mm" />
        <character name="width" src="d0_s2" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sexual condition synoicous or dioicous.</text>
      <biological_entity id="o6721" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character char_type="range_value" from="distally entire" name="architecture_or_shape" src="d0_s2" to="serrulate" />
        <character is_modifier="false" name="reproduction" src="d0_s3" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s3" value="synoicous" value_original="synoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s3" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Capsule dehiscing by a somewhat indistinct ring of cells distal to the equator, orangebrown to black, ± globose with a barely discernable apiculus or with a dome-shaped apical cell;</text>
      <biological_entity id="o6722" name="capsule" name_original="capsule" src="d0_s4" type="structure">
        <character constraint="by ring" constraintid="o6723" is_modifier="false" name="dehiscence" src="d0_s4" value="dehiscing" value_original="dehiscing" />
        <character char_type="range_value" from="orangebrown" name="coloration" notes="" src="d0_s4" to="black" />
        <character constraint="with " constraintid="o6727" is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="globose" value_original="globose" />
      </biological_entity>
      <biological_entity id="o6723" name="ring" name_original="ring" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="somewhat" name="prominence" src="d0_s4" value="indistinct" value_original="indistinct" />
        <character constraint="to equator" constraintid="o6725" is_modifier="false" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o6724" name="cell" name_original="cells" src="d0_s4" type="structure" />
      <biological_entity id="o6725" name="equator" name_original="equator" src="d0_s4" type="structure" />
      <biological_entity id="o6726" name="apiculu" name_original="apiculus" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="barely" name="prominence" src="d0_s4" value="discernable" value_original="discernable" />
      </biological_entity>
      <biological_entity constraint="apical" id="o6727" name="cell" name_original="cell" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="dome--shaped" value_original="dome--shaped" />
      </biological_entity>
      <relation from="o6723" id="r1592" name="part_of" negation="false" src="d0_s4" to="o6724" />
    </statement>
    <statement id="d0_s5">
      <text>exothecial cells in 1 layer, stomates none.</text>
      <biological_entity constraint="exothecial" id="o6728" name="cell" name_original="cells" src="d0_s5" type="structure" />
      <biological_entity id="o6729" name="layer" name_original="layer" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o6730" name="stomate" name_original="stomates" src="d0_s5" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s5" value="0" value_original="none" />
      </biological_entity>
      <relation from="o6728" id="r1593" name="in" negation="false" src="d0_s5" to="o6729" />
    </statement>
    <statement id="d0_s6">
      <text>Spores about 100, reniform to globose, with or without a concave proximal face, 27–45 × 20–37 µm, orangebrown to brown or black.</text>
      <biological_entity id="o6731" name="spore" name_original="spores" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="100" value_original="100" />
        <character char_type="range_value" from="reniform" name="shape" src="d0_s6" to="globose" />
        <character char_type="range_value" from="27" from_unit="um" name="length" notes="" src="d0_s6" to="45" to_unit="um" />
        <character char_type="range_value" from="20" from_unit="um" name="width" notes="" src="d0_s6" to="37" to_unit="um" />
        <character char_type="range_value" from="orangebrown" name="coloration" src="d0_s6" to="brown or black" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6732" name="face" name_original="face" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="concave" value_original="concave" />
      </biological_entity>
      <relation from="o6731" id="r1594" name="with or without" negation="false" src="d0_s6" to="o6732" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature summer–spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="spring" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bare soil in old fields, drying ponds, moist or swampy woods, banks of streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bare soil" constraint="in old fields , drying ponds , moist or swampy woods , banks of streams" />
        <character name="habitat" value="old fields" />
        <character name="habitat" value="drying ponds" />
        <character name="habitat" value="moist" />
        <character name="habitat" value="swampy woods" />
        <character name="habitat" value="banks" constraint="of streams" />
        <character name="habitat" value="streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-1000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., N.S., Que.; Conn., Fla., Ga., Ill., Iowa, Ky., La., Maine, Md., Mich., Miss., N.J., N.Y., N.C., Ohio, Pa., S.C., Tenn., Va., W.Va.; West Indies (Puerto Rico, Virgin Islands); Central America (Panama); South America (Brazil); Europe; Asia (China, Japan, Korea, Tibet); Africa (Zimbabwe); Pacific Islands (New Zealand).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="West Indies (Puerto Rico)" establishment_means="native" />
        <character name="distribution" value="West Indies (Virgin Islands)" establishment_means="native" />
        <character name="distribution" value="Central America (Panama)" establishment_means="native" />
        <character name="distribution" value="South America (Brazil)" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Asia (Korea)" establishment_means="native" />
        <character name="distribution" value="Asia (Tibet)" establishment_means="native" />
        <character name="distribution" value="Africa (Zimbabwe)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Many specimens of Micromitrium tenerum include plants with leaves in which two or three cells in a double layer can be found in a median position, just distal to the middle of the leaf. The walls are not differentiated in any way, and their presence does not suggest even a meager costa. As in M. synoicum and M. megalosporum, mature capsules of M. tenerum may persist after the leaves have eroded away. Micromitrium synoicum and M. tenerum are remarkably similar, but are distinguished by stem length and leaf size and shape. Micromitrium tenerum is stemless or has very short stems, and M. synoicum has distinct stems. The somewhat longer leaves of M. tenerum have plane margins that may be almost entire but are usually serrulate distally, while the leaves of M. synoicum are commonly incurved distal to the middle with almost invariably entire margins.</discussion>
  
</bio:treatment>