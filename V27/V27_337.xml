<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">228</other_info_on_meta>
    <other_info_on_meta type="mention_page">247</other_info_on_meta>
    <other_info_on_meta type="mention_page">258</other_info_on_meta>
    <other_info_on_meta type="treatment_page">246</other_info_on_meta>
    <other_info_on_meta type="illustration_page">246</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">grimmia</taxon_name>
    <taxon_name authority="I. Hagen" date="1909" rank="subgenus">litoneuron</taxon_name>
    <taxon_name authority="J. Muñoz" date="2002" rank="species">serrana</taxon_name>
    <place_of_publication>
      <publication_title>Shevock &amp; D. R. Toren, J. Bryol.</publication_title>
      <place_in_publication>24: 143, figs. 1a–f. 2002,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus grimmia;subgenus litoneuron;species serrana</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443425</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in flat patches, olivaceous.</text>
      <biological_entity id="o2279" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="olivaceous" value_original="olivaceous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o2280" name="patch" name_original="patches" src="d0_s0" type="structure">
        <character is_modifier="true" name="prominence_or_shape" src="d0_s0" value="flat" value_original="flat" />
      </biological_entity>
      <relation from="o2279" id="r547" name="in" negation="false" src="d0_s0" to="o2280" />
    </statement>
    <statement id="d0_s1">
      <text>Stems to 3 cm.</text>
      <biological_entity id="o2281" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves ovatelanceolate from an ovate base, 2.5–3.5 × 0.6–0.7 mm, both margins plane, costa-like intermarginal bands 2–4 (–5) -stratose, awn to 1 mm, not decurrent, narrowly attached, acuminate, costa narrow proximally;</text>
      <biological_entity id="o2282" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="from base" constraintid="o2283" is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" notes="" src="d0_s2" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" notes="" src="d0_s2" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2283" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o2284" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity constraint="intermarginal" id="o2285" name="band" name_original="bands" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="costa-like" value_original="costa-like" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="2-4(-5)-stratose" value_original="2-4(-5)-stratose" />
      </biological_entity>
      <biological_entity id="o2286" name="awn" name_original="awn" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="decurrent" value_original="decurrent" />
        <character is_modifier="false" modifier="narrowly" name="fixation" src="d0_s2" value="attached" value_original="attached" />
        <character is_modifier="false" name="shape" src="d0_s2" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o2287" name="costa" name_original="costa" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="proximally" name="size_or_width" src="d0_s2" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal juxtacostal laminal cells short to long-rectangular, sinuose, thick lateral-walled, dense;</text>
      <biological_entity constraint="basal" id="o2288" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="laminal" id="o2289" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s3" value="long-rectangular" value_original="long-rectangular" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" name="width" src="d0_s3" value="thick" value_original="thick" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="lateral-walled" value_original="lateral-walled" />
        <character is_modifier="false" name="density" src="d0_s3" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal marginal laminal cells quadrate to short-rectangular, sinuose, thick lateral-walled, not hyaline;</text>
      <biological_entity constraint="basal marginal laminal" id="o2290" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s4" to="short-rectangular" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" name="width" src="d0_s4" value="thick" value_original="thick" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="lateral-walled" value_original="lateral-walled" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>medial laminal cells quadrate, slightly sinuose, thick-walled;</text>
      <biological_entity constraint="medial laminal" id="o2291" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s5" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal laminal cells 1–3-stratose, oblate to rectangular, thick-walled.</text>
      <biological_entity constraint="distal laminal" id="o2292" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-3-stratose" value_original="1-3-stratose" />
        <character char_type="range_value" from="oblate" name="shape" src="d0_s6" to="rectangular" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Perichaetial leaves enlarged.</text>
      <biological_entity constraint="perichaetial" id="o2293" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="enlarged" value_original="enlarged" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seta straight to slightly arcuate, to 3.5 mm.</text>
      <biological_entity id="o2294" name="seta" name_original="seta" src="d0_s8" type="structure">
        <character char_type="range_value" from="straight" name="course" src="d0_s8" to="slightly arcuate" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule occasionally present, exserted, pale-yellow, ovoid, exothecial cells oblong, thin-walled, stomata absent, annulus of 1–2 rows of quadrate, thick-walled cells, operculum short straight, peristome fully developed, not perforate, not split.</text>
      <biological_entity id="o2295" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="occasionally" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovoid" value_original="ovoid" />
      </biological_entity>
      <biological_entity constraint="exothecial" id="o2296" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <biological_entity id="o2297" name="stoma" name_original="stomata" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o2298" name="annulus" name_original="annulus" src="d0_s9" type="structure" />
      <biological_entity id="o2299" name="row" name_original="rows" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="2" />
      </biological_entity>
      <biological_entity id="o2300" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="quadrate" value_original="quadrate" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
      <biological_entity id="o2301" name="operculum" name_original="operculum" src="d0_s9" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o2302" name="peristome" name_original="peristome" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="fully" name="development" src="d0_s9" value="developed" value_original="developed" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s9" value="perforate" value_original="perforate" />
      </biological_entity>
      <biological_entity id="o2303" name="split" name_original="split" src="d0_s9" type="structure" />
      <relation from="o2298" id="r548" name="consist_of" negation="false" src="d0_s9" to="o2299" />
      <relation from="o2299" id="r549" name="part_of" negation="false" src="d0_s9" to="o2300" />
    </statement>
    <statement id="d0_s10">
      <text>Calyptra cucullate.</text>
      <biological_entity id="o2304" name="calyptra" name_original="calyptra" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="cucullate" value_original="cucullate" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Humid to dry areas, exposed granite, metamorphic rock, metavolcanic rocks and basalt, montane woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="humid to dry areas" />
        <character name="habitat" value="exposed granite" />
        <character name="habitat" value="metamorphic rock" />
        <character name="habitat" value="metavolcanic rocks" />
        <character name="habitat" value="basalt" />
        <character name="habitat" value="montane woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate elevations (700-1400 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="moderate" />
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="700" from_unit="m" constraint="moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  <discussion>According to the original description, Grimmia serrana is known only from “localities along the western slope of the Sierra Nevada and two occurrences in the northern Coast Range.”  Despite its limited distribution, H. C. Greven (2003) found it to be quite common in the area. With its thick, concave leaves with plane margins and flat costa, G. serrana is best placed in subg. Litoneuron. This is further supported by a number of other features shared by, but not unique to, all members of that subgenus, including a well-developed stem central strand, dioicous sexuality, and long-exserted capsule with a multi-layered, thick-walled annulus. Grimmia serrana is most readily identified by a prominent, costa-like, multi-layered band of cells that run along the laminal margin distally and become submarginal proximally. This inflated band is unique to this species and readily separates it from all other members of subg. Litoneuron. As stated by Muñoz et al., G. serrana is most likely to be confused with G. ovalis. Both are robust species with ovate-lanceolate leaves from an ovate base. However, the prominent intermarginal bands of G. serrana will readily separate these species.</discussion>
  
</bio:treatment>