<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">266</other_info_on_meta>
    <other_info_on_meta type="treatment_page">265</other_info_on_meta>
    <other_info_on_meta type="illustration_page">264</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Brotherus &amp; Müller Hal." date="1898" rank="genus">indusiella</taxon_name>
    <taxon_name authority="Brotherus &amp; Müller Hal." date="1898" rank="species">thianschanica</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Centralbl.</publication_title>
      <place_in_publication>75: 322. 1898,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus indusiella;species thianschanica</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075415</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in dense dark-brown to black cushions.</text>
      <biological_entity id="o6747" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6748" name="cushion" name_original="cushions" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="dense" value_original="dense" />
        <character char_type="range_value" from="dark-brown" is_modifier="true" name="coloration" src="d0_s0" to="black" />
      </biological_entity>
      <relation from="o6747" id="r1600" name="in" negation="false" src="d0_s0" to="o6748" />
    </statement>
    <statement id="d0_s1">
      <text>Stem central strand strong, to 1/3 stem diameter.</text>
      <biological_entity id="o6749" name="stem" name_original="stem" src="d0_s1" type="structure" />
      <biological_entity constraint="central" id="o6750" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s1" value="strong" value_original="strong" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s1" to="1/3" />
      </biological_entity>
      <biological_entity id="o6751" name="stem" name_original="stem" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves 0.8–1 mm, apex obtuse to broadly acute, in section abaxial cells with thickened quasi-opaque external walls, basal lamina 1-stratose;</text>
      <biological_entity id="o6752" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6753" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="broadly acute" />
      </biological_entity>
      <biological_entity id="o6754" name="section" name_original="section" src="d0_s2" type="structure" />
      <biological_entity constraint="abaxial" id="o6755" name="cell" name_original="cells" src="d0_s2" type="structure" />
      <biological_entity constraint="external" id="o6756" name="wall" name_original="walls" src="d0_s2" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s2" value="thickened" value_original="thickened" />
        <character is_modifier="true" name="coloration" src="d0_s2" value="quasi-opaque" value_original="quasi-opaque" />
      </biological_entity>
      <biological_entity constraint="basal" id="o6757" name="lamina" name_original="lamina" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <relation from="o6753" id="r1601" name="in" negation="false" src="d0_s2" to="o6754" />
      <relation from="o6755" id="r1602" name="with" negation="false" src="d0_s2" to="o6756" />
    </statement>
    <statement id="d0_s3">
      <text>largest distal leaves occasionally tipped with a hyaline apiculus of one or two cells, often eroded;</text>
      <biological_entity constraint="largest distal" id="o6758" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="with apiculus" constraintid="o6759" is_modifier="false" modifier="occasionally" name="architecture" src="d0_s3" value="tipped" value_original="tipped" />
        <character is_modifier="false" modifier="often" name="architecture_or_relief" notes="" src="d0_s3" value="eroded" value_original="eroded" />
      </biological_entity>
      <biological_entity id="o6759" name="apiculu" name_original="apiculus" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o6760" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
      <relation from="o6759" id="r1603" name="part_of" negation="false" src="d0_s3" to="o6760" />
    </statement>
    <statement id="d0_s4">
      <text>costa in section with adaxial hyaline cells somewhat larger and more bulging than the semi-opaque thick-walled abaxial cells, costa with stereid cells present, two or more hydroids in the proximal leaf region, fewer to absent toward the apex;</text>
      <biological_entity id="o6761" name="costa" name_original="costa" src="d0_s4" type="structure" />
      <biological_entity id="o6762" name="section" name_original="section" src="d0_s4" type="structure" />
      <biological_entity constraint="adaxial" id="o6763" name="cell" name_original="cells" src="d0_s4" type="structure" />
      <biological_entity id="o6764" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="somewhat" name="size" src="d0_s4" value="larger" value_original="larger" />
        <character is_modifier="false" name="pubescence_or_shape" src="d0_s4" value="bulging" value_original="bulging" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6765" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="semi-opaque" value_original="semi-opaque" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
      <biological_entity id="o6766" name="costa" name_original="costa" src="d0_s4" type="structure" />
      <biological_entity id="o6767" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character constraint="in proximal leaf region" constraintid="o6768" name="quantity" src="d0_s4" unit="or morehydroids" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o6768" name="region" name_original="region" src="d0_s4" type="structure" constraint_original="proximal leaf">
        <character char_type="range_value" constraint="toward apex" constraintid="o6769" from="fewer" name="quantity" notes="" src="d0_s4" to="absent" />
      </biological_entity>
      <biological_entity id="o6769" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <relation from="o6761" id="r1604" name="in" negation="false" src="d0_s4" to="o6762" />
      <relation from="o6762" id="r1605" name="with" negation="false" src="d0_s4" to="o6763" />
      <relation from="o6766" id="r1606" name="with" negation="false" src="d0_s4" to="o6767" />
    </statement>
    <statement id="d0_s5">
      <text>leaf cells smooth;</text>
      <biological_entity constraint="leaf" id="o6770" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal-cells with thicker transverse walls.</text>
      <biological_entity constraint="thicker" id="o6772" name="wall" name_original="walls" src="d0_s6" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s6" value="transverse" value_original="transverse" />
      </biological_entity>
      <relation from="o6771" id="r1607" name="with" negation="false" src="d0_s6" to="o6772" />
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition autoicous;</text>
      <biological_entity id="o6771" name="basal-cell" name_original="basal-cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perigonia sessile, situated within the perichaetium projecting beyond the group of archegonia (synautoicous or cryptoicous), leaves short, suborbicular, broadly acuminate.</text>
      <biological_entity id="o6773" name="perigonium" name_original="perigonia" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o6774" name="perichaetium" name_original="perichaetium" src="d0_s8" type="structure" />
      <biological_entity id="o6775" name="archegonium" name_original="archegonia" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity id="o6776" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s8" value="suborbicular" value_original="suborbicular" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o6773" id="r1608" name="situated within the" negation="false" src="d0_s8" to="o6774" />
      <relation from="o6773" id="r1609" name="situated within the" negation="false" src="d0_s8" to="o6775" />
    </statement>
    <statement id="d0_s9">
      <text>Seta centrally attached, 0.7–1 mm.</text>
      <biological_entity id="o6777" name="seta" name_original="seta" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="centrally" name="fixation" src="d0_s9" value="attached" value_original="attached" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsule smooth (not plicate), composed of irregularly shaped thick-walled cells;</text>
      <biological_entity id="o6778" name="capsule" name_original="capsule" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o6779" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="irregularly--shaped" value_original="irregularly--shaped" />
        <character is_modifier="true" name="architecture" src="d0_s10" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
      <relation from="o6778" id="r1610" name="composed of" negation="false" src="d0_s10" to="o6779" />
    </statement>
    <statement id="d0_s11">
      <text>peristome teeth erect, irregularly split in distal half into 2–3 filiform segments, irregularly perforate, spiculose, united at the base.</text>
      <biological_entity constraint="peristome" id="o6780" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o6781" name="split" name_original="split" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture" notes="" src="d0_s11" value="perforate" value_original="perforate" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="spiculose" value_original="spiculose" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6782" name="half" name_original="half" src="d0_s11" type="structure" />
      <biological_entity id="o6783" name="segment" name_original="segments" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s11" to="3" />
        <character is_modifier="true" name="shape" src="d0_s11" value="filiform" value_original="filiform" />
      </biological_entity>
      <biological_entity id="o6784" name="base" name_original="base" src="d0_s11" type="structure" />
      <relation from="o6781" id="r1611" name="in" negation="false" src="d0_s11" to="o6782" />
      <relation from="o6782" id="r1612" name="into" negation="false" src="d0_s11" to="o6783" />
      <relation from="o6781" id="r1613" name="united at the" negation="false" src="d0_s11" to="o6784" />
    </statement>
    <statement id="d0_s12">
      <text>Spores spherical, yellowish-brown, 9–12 µm.</text>
      <biological_entity id="o6785" name="spore" name_original="spores" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="spherical" value_original="spherical" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellowish-brown" value_original="yellowish-brown" />
        <character char_type="range_value" from="9" from_unit="um" name="some_measurement" src="d0_s12" to="12" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mineral soil over calcareous sedimentary rock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mineral soil" constraint="over calcareous sedimentary rock" />
        <character name="habitat" value="calcareous sedimentary rock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate elevations (300[-5100] m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="moderate" />
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="300" from_unit="m" constraint="moderate elevations " />
        <character name="elevation" char_type="foreign_range" to="5100" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska; Asia (Caucasus in Dagestan, China, Mongolia, Russia in Siberia); Africa (Chad).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia (Caucasus in Dagestan)" establishment_means="native" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="Asia (Mongolia)" establishment_means="native" />
        <character name="distribution" value="Asia (Russia in Siberia)" establishment_means="native" />
        <character name="distribution" value="Africa (Chad)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Indusiella thianschanicais is found in arid situations, with the single North American specimen on thin, fine mineral soil over calcareous sedimentary rock.</discussion>
  
</bio:treatment>