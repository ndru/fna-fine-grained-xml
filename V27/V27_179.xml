<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">143</other_info_on_meta>
    <other_info_on_meta type="mention_page">144</other_info_on_meta>
    <other_info_on_meta type="treatment_page">145</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwägrichen" date="unknown" rank="family">polytrichaceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1805" rank="genus">oligotrichum</taxon_name>
    <taxon_name authority="Steere" date="1958" rank="species">falcatum</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>61: 115, figs. 1–9. 1958,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polytrichaceae;genus oligotrichum;species falcatum</taxon_hierarchy>
    <other_info_on_name type="fna_id">200002622</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants green to silvery green, reddish-brown with age.</text>
      <biological_entity id="o4399" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="silvery green" />
        <character constraint="with age" constraintid="o4400" is_modifier="false" name="coloration" src="d0_s0" value="reddish-brown" value_original="reddish-brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4400" name="age" name_original="age" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1.5–2 (–5) cm, simple to much branched by innovations.</text>
      <biological_entity id="o4401" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s1" to="2" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple to much" value_original="simple to much" />
        <character constraint="by innovations" constraintid="o4402" is_modifier="false" modifier="much" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o4402" name="innovation" name_original="innovations" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves 1.5–3 mm, asymmetrically ovate to ovatelanceolate, arcuate-incurved and falcate-secund when wet or dry;</text>
      <biological_entity id="o4403" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s2" to="3" to_unit="mm" />
        <character char_type="range_value" from="asymmetrically ovate" name="shape" src="d0_s2" to="ovatelanceolate" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="arcuate-incurved" value_original="arcuate-incurved" />
        <character is_modifier="false" modifier="when wet or dry" name="arrangement" src="d0_s2" value="falcate-secund" value_original="falcate-secund" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>costa percurrent, the apex cucullate with a sharp point, smooth abaxially or slightly roughened;</text>
      <biological_entity id="o4404" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="percurrent" value_original="percurrent" />
      </biological_entity>
      <biological_entity id="o4405" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character constraint="with point" constraintid="o4406" is_modifier="false" name="shape" src="d0_s3" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" modifier="abaxially" name="architecture_or_pubescence_or_relief" notes="" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly; slightly" name="relief_or_texture" src="d0_s3" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity id="o4406" name="point" name_original="point" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="sharp" value_original="sharp" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lamina strongly inflexed distally, smooth abaxially or with low teeth or abaxial lamellae only near apex;</text>
      <biological_entity id="o4407" name="lamina" name_original="lamina" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="strongly; distally" name="orientation" src="d0_s4" value="inflexed" value_original="inflexed" />
        <character is_modifier="false" modifier="abaxially" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="with low teeth or abaxial lamellae" />
      </biological_entity>
      <biological_entity id="o4408" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="true" name="position" src="d0_s4" value="low" value_original="low" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4409" name="lamella" name_original="lamellae" src="d0_s4" type="structure" />
      <biological_entity id="o4410" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <relation from="o4407" id="r1210" name="with" negation="false" src="d0_s4" to="o4408" />
      <relation from="o4407" id="r1211" name="with" negation="false" src="d0_s4" to="o4409" />
      <relation from="o4409" id="r1212" name="near" negation="false" src="d0_s4" to="o4410" />
    </statement>
    <statement id="d0_s5">
      <text>margins of lamina distantly serrulate or almost entire;</text>
      <biological_entity id="o4411" name="margin" name_original="margins" src="d0_s5" type="structure" constraint="lamina" constraint_original="lamina; lamina">
        <character is_modifier="false" modifier="distantly" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" modifier="almost; almost" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4412" name="lamina" name_original="lamina" src="d0_s5" type="structure" />
      <relation from="o4411" id="r1213" name="part_of" negation="false" src="d0_s5" to="o4412" />
    </statement>
    <statement id="d0_s6">
      <text>adaxial lamellae 8–16, undulate, 4–10 (–13) cells high, in profile irregularly serrulate and notched, the marginal cells in section undifferentiated, smooth;</text>
      <biological_entity constraint="adaxial" id="o4413" name="lamella" name_original="lamellae" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s6" to="16" />
        <character is_modifier="false" name="shape" src="d0_s6" value="undulate" value_original="undulate" />
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="13" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s6" to="10" />
      </biological_entity>
      <biological_entity id="o4414" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="height" src="d0_s6" value="high" value_original="high" />
        <character is_modifier="false" modifier="in-profile irregularly" name="architecture_or_shape" src="d0_s6" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="notched" value_original="notched" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o4415" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s6" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o4416" name="section" name_original="section" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="undifferentiated" value_original="undifferentiated" />
      </biological_entity>
      <relation from="o4415" id="r1214" name="in" negation="false" src="d0_s6" to="o4416" />
    </statement>
    <statement id="d0_s7">
      <text>median basal-cells short-rectangular, sometimes with small groups of alar cells larger;</text>
      <biological_entity constraint="median" id="o4417" name="basal-bud" name_original="basal-cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="short-rectangular" value_original="short-rectangular" />
      </biological_entity>
      <biological_entity id="o4418" name="group" name_original="groups" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="small" value_original="small" />
        <character is_modifier="false" name="size" src="d0_s7" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o4419" name="alar" name_original="alar" src="d0_s7" type="structure" />
      <biological_entity id="o4420" name="bud" name_original="cells" src="d0_s7" type="structure" />
      <relation from="o4417" id="r1215" modifier="sometimes" name="with" negation="false" src="d0_s7" to="o4418" />
      <relation from="o4418" id="r1216" name="part_of" negation="false" src="d0_s7" to="o4419" />
      <relation from="o4418" id="r1217" name="part_of" negation="false" src="d0_s7" to="o4420" />
    </statement>
    <statement id="d0_s8">
      <text>median cells of lamina isodiametric to short-rectangular, 6–15 (–18) µm wide, walls firm, thickened at the angles;</text>
      <biological_entity constraint="lamina" id="o4421" name="bud" name_original="cells" src="d0_s8" type="structure" constraint_original="lamina median; lamina">
        <character char_type="range_value" from="isodiametric" name="shape" src="d0_s8" to="short-rectangular" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="um" name="width" src="d0_s8" to="18" to_unit="um" />
        <character char_type="range_value" from="6" from_unit="um" name="width" src="d0_s8" to="15" to_unit="um" />
      </biological_entity>
      <biological_entity id="o4422" name="lamina" name_original="lamina" src="d0_s8" type="structure" />
      <biological_entity id="o4423" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="firm" value_original="firm" />
        <character constraint="at angles" constraintid="o4424" is_modifier="false" name="size_or_width" src="d0_s8" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o4424" name="angle" name_original="angles" src="d0_s8" type="structure" />
      <relation from="o4421" id="r1218" name="part_of" negation="false" src="d0_s8" to="o4422" />
    </statement>
    <statement id="d0_s9">
      <text>marginal cells ± at right angles to the margin, marginal teeth minute, unicellular;</text>
      <biological_entity constraint="marginal" id="o4425" name="bud" name_original="cells" src="d0_s9" type="structure" />
      <biological_entity id="o4426" name="angle" name_original="angles" src="d0_s9" type="structure" />
      <biological_entity id="o4427" name="margin" name_original="margin" src="d0_s9" type="structure" />
      <biological_entity constraint="marginal" id="o4428" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="minute" value_original="minute" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="unicellular" value_original="unicellular" />
      </biological_entity>
      <relation from="o4425" id="r1219" name="at" negation="false" src="d0_s9" to="o4426" />
      <relation from="o4426" id="r1220" name="to" negation="false" src="d0_s9" to="o4427" />
    </statement>
    <statement id="d0_s10">
      <text>leaves of male plants only weakly falcate;</text>
      <biological_entity id="o4429" name="leaf" name_original="leaves" src="d0_s10" type="structure" constraint="plant" constraint_original="plant; plant">
        <character is_modifier="false" modifier="only weakly" name="shape" src="d0_s10" value="falcate" value_original="falcate" />
      </biological_entity>
      <biological_entity id="o4430" name="plant" name_original="plants" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="male" value_original="male" />
      </biological_entity>
      <relation from="o4429" id="r1221" name="part_of" negation="false" src="d0_s10" to="o4430" />
    </statement>
    <statement id="d0_s11">
      <text>perichaetial leaves longer and straighter than the vegetative leaves, convolute-tubulose, almost straight.</text>
      <biological_entity constraint="perichaetial" id="o4431" name="leaf" name_original="leaves" src="d0_s11" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s11" value="longer" value_original="longer" />
        <character constraint="than the vegetative leaves" constraintid="o4432" is_modifier="false" name="course" src="d0_s11" value="longer and straighter" value_original="longer and straighter" />
        <character is_modifier="false" name="shape" src="d0_s11" value="convolute-tubulose" value_original="convolute-tubulose" />
        <character is_modifier="false" modifier="almost" name="course" src="d0_s11" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o4432" name="leaf" name_original="leaves" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="vegetative" value_original="vegetative" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seta 1–1.5 cm.</text>
      <biological_entity id="o4433" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s12" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsule 3.5–4 mm, cylindric, widest at the base and tapering towards the mouth, sometimes indistinctly angled;</text>
      <biological_entity id="o4434" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
        <character constraint="at base" constraintid="o4435" is_modifier="false" name="width" src="d0_s13" value="widest" value_original="widest" />
        <character constraint="towards mouth" constraintid="o4436" is_modifier="false" name="shape" notes="" src="d0_s13" value="tapering" value_original="tapering" />
        <character is_modifier="false" modifier="sometimes indistinctly" name="shape" notes="" src="d0_s13" value="angled" value_original="angled" />
      </biological_entity>
      <biological_entity id="o4435" name="base" name_original="base" src="d0_s13" type="structure" />
      <biological_entity id="o4436" name="mouth" name_original="mouth" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>operculum conic with a short beak;</text>
      <biological_entity id="o4437" name="operculum" name_original="operculum" src="d0_s14" type="structure">
        <character constraint="with beak" constraintid="o4438" is_modifier="false" name="shape" src="d0_s14" value="conic" value_original="conic" />
      </biological_entity>
      <biological_entity id="o4438" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s14" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>peristome teeth 32, tapering, subacute, often compound.</text>
      <biological_entity constraint="peristome" id="o4439" name="tooth" name_original="teeth" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="32" value_original="32" />
        <character is_modifier="false" name="shape" src="d0_s15" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="shape" src="d0_s15" value="subacute" value_original="subacute" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s15" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Spores 19–21 µm.</text>
      <biological_entity id="o4440" name="spore" name_original="spores" src="d0_s16" type="structure">
        <character char_type="range_value" from="19" from_unit="um" name="some_measurement" src="d0_s16" to="21" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet soil and rocks, slopes and bare, non-calcareous gravelly soil, lake shores, dripping rocks by waterfalls, often beside or submerged in snow-melt streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet soil" />
        <character name="habitat" value="rocks" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="gravelly soil" modifier="bare non-calcareous" />
        <character name="habitat" value="lake shores" />
        <character name="habitat" value="rocks" modifier="dripping" constraint="by waterfalls" />
        <character name="habitat" value="waterfalls" modifier="by" />
        <character name="habitat" value="snow-melt streams" modifier="often beside or submerged in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (200-1800 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="200" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Nfld. and Labr. (Labr.), Nunavut, Yukon; Alaska; Asia (Russia in Siberia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia (Russia in Siberia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <discussion>Oligotrichum falcatum is widely distributed in the Arctic. It is locally common and not infrequently fruiting in northern Alaska and Yukon. In Nunavut, it is known from Axel Heiberg and Melville islands. Similar to O. hercynicum, it is distinguished by the regularly falcate-secund leaves, with abaxial lamellae scarcely developed. Steere noted that in the field, submerged plants have a silvery-green appearance, due to air trapped in the abaxial cells of the costa.</discussion>
  <references>
    <reference>Steere, W. C. and Gary L. Smith. 1976. The sporophyte of Oligotrichum falcatum. Bryologist 79: 447–451.</reference>
  </references>
  
</bio:treatment>