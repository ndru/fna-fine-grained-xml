<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">202</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindberg" date="unknown" rank="family">gigaspermaceae</taxon_name>
    <taxon_name authority="Müller Hal." date="1879" rank="genus">LORENTZIELLA</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>42: 229. 1879  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family gigaspermaceae;genus LORENTZIELLA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Paul Günter Lorentz, 1835–1881, German bryologist</other_info_on_name>
    <other_info_on_name type="fna_id">118966</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves imbricate distally, concave, broadly ovate to elliptic, abruptly narrowed to a long awn;</text>
      <biological_entity id="o702" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="distally" name="arrangement" src="d0_s0" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="shape" src="d0_s0" value="concave" value_original="concave" />
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s0" to="elliptic" />
        <character constraint="to awn" constraintid="o703" is_modifier="false" modifier="abruptly" name="shape" src="d0_s0" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o703" name="awn" name_original="awn" src="d0_s0" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s0" value="long" value_original="long" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>costa narrow, extending to base of awn.</text>
      <biological_entity id="o704" name="costa" name_original="costa" src="d0_s1" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s1" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o705" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o706" name="awn" name_original="awn" src="d0_s1" type="structure" />
      <relation from="o704" id="r207" name="extending to" negation="false" src="d0_s1" to="o705" />
      <relation from="o704" id="r208" name="extending to" negation="false" src="d0_s1" to="o706" />
    </statement>
    <statement id="d0_s2">
      <text>Capsule immersed to slightly emergent, base truncate, operculum not differentiated.</text>
      <biological_entity id="o707" name="capsule" name_original="capsule" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="immersed" value_original="immersed" />
        <character is_modifier="false" modifier="slightly" name="location" src="d0_s2" value="emergent" value_original="emergent" />
      </biological_entity>
      <biological_entity id="o708" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o709" name="operculum" name_original="operculum" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s2" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>United States, s South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="United States" establishment_means="native" />
        <character name="distribution" value="s South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Species 1 or 2 (1 in the flora).</discussion>
  <discussion>Lorentziella is a genus of minute, ephemeral mosses comprising one, or at most two, species. The perennial, subterranean stem or rhizome system gives rise to upright, above-ground plants in late fall–early winter. Above-ground plants are produced in abundance during mild, wet winters but less commonly when conditions are dry. The broadly concave leaves completely surround the globose, cleistocarpous capsules of immersed sporophytes. The glaucous, blue-green color of the above-ground plants is reminiscent of Bryum argenteum while their cabbage shape resembles a small Funaria gametophyte prior to elongation of seta.</discussion>
  <references>
    <reference>Lawton, E. 1953. Lorentziella, a moss genus new to North America. Bull. Torrey Bot. Club 80: 279–288.</reference>
    <reference>Rushing, A. E. and J. A. Snider. 1980. Observations on sporophyte development in Lorentziella imbricata (Mitt.) Broth. J. Hattori Bot. Lab. 47: 35–44.</reference>
  </references>
  
</bio:treatment>