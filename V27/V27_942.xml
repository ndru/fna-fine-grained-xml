<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">638</other_info_on_meta>
    <other_info_on_meta type="treatment_page">639</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="Müller Hal." date="1847" rank="genus">acaulon</taxon_name>
    <taxon_name authority="(Hedwig) Müller Hal." date="1847" rank="species">muticum</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Zeitung (Berlin)</publication_title>
      <place_in_publication>5: 99. 1847,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus acaulon;species muticum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443894</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Phascum</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="species">muticum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>23. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Phascum;species muticum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants elliptic to globose, ca. 2 mm.</text>
      <biological_entity id="o3170" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s0" to="globose" />
        <character name="some_measurement" src="d0_s0" unit="mm" value="2" value_original="2" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stem-leaves cuspidate, broadly channeled;</text>
      <biological_entity id="o3171" name="stem-leaf" name_original="stem-leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="cuspidate" value_original="cuspidate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s1" value="channeled" value_original="channeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>laminal cells smooth.</text>
      <biological_entity constraint="laminal" id="o3172" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Seta short, about 0.3 the diameter of the capsule.</text>
      <biological_entity id="o3173" name="seta" name_original="seta" src="d0_s3" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character name="quantity" src="d0_s3" value="0.3" value_original="0.3" />
      </biological_entity>
      <biological_entity id="o3174" name="capsule" name_original="capsule" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Spores spheric to elliptic, 30–50 µm, papillose or nearly smooth.</text>
      <biological_entity id="o3175" name="spore" name_original="spores" src="d0_s4" type="structure">
        <character char_type="range_value" from="spheric" name="shape" src="d0_s4" to="elliptic" />
        <character char_type="range_value" from="30" from_unit="um" name="some_measurement" src="d0_s4" to="50" to_unit="um" />
        <character is_modifier="false" name="relief" src="d0_s4" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="nearly" name="relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Temperate areas of the Northern Hemisphere, Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Temperate areas of the Northern Hemisphere" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>As H. A. Crum (1969) and A. J. Grout (1939) pointed out, American specimens reported as Acaulon rubrum are A. muticum var. rufescens. These plants are often small, rather three-sided and may be confused with A. triquetrum. The leaves of both varieties stain red, at least in blotches, in KOH, and they intergrade to some extent.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants yellow-brown in nature, spores papillose- punctulose</description>
      <determination>3a Acaulon muticum var. muticum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants yellow- to red-brown in nature, spores smooth or papillose-punctulate</description>
      <determination>3b Acaulon muticum var. rufescens</determination>
    </key_statement>
  </key>
</bio:treatment>