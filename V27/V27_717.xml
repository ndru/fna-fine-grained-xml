<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Patricia M. Eckel</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">477</other_info_on_meta>
    <other_info_on_meta type="mention_page">492</other_info_on_meta>
    <other_info_on_meta type="mention_page">565</other_info_on_meta>
    <other_info_on_meta type="treatment_page">494</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="(Schimper) Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">trichostomoideae</taxon_name>
    <taxon_name authority="Brotherus" date="1910" rank="genus">TUERCKHEIMIA</taxon_name>
    <place_of_publication>
      <publication_title>Öfvers. Finska Vetenskaps-Soc. Förh.</publication_title>
      <place_in_publication>52A(7): 1. 1910  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily trichostomoideae;genus TUERCKHEIMIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Hans von Türckheim, 1853–1920, plant collector in Guatemala and West Indies</other_info_on_name>
    <other_info_on_name type="fna_id">133964</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in turfs, light to dark green with a somewhat bluish cast, glaucous.</text>
      <biological_entity id="o4710" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" constraint="with cast" constraintid="o4712" from="light" name="coloration" notes="" src="d0_s0" to="dark green" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4711" name="turf" name_original="turfs" src="d0_s0" type="structure" />
      <biological_entity id="o4712" name="cast" name_original="cast" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="somewhat" name="coloration" src="d0_s0" value="bluish" value_original="bluish" />
      </biological_entity>
      <relation from="o4710" id="r1116" name="in" negation="false" src="d0_s0" to="o4711" />
    </statement>
    <statement id="d0_s1">
      <text>Stem to 2.5 cm, hyalodermis rarely distinct, sclerodermis absent or weakly developed, central strand present;</text>
      <biological_entity id="o4713" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4714" name="hyalodermi" name_original="hyalodermis" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="rarely" name="fusion" src="d0_s1" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o4715" name="scleroderm" name_original="sclerodermis" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="weakly" name="development" src="d0_s1" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity constraint="central" id="o4716" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>axillary hairs to 16 cells long, basal-cell occasionally brown, other axillary hair cells hyaline.</text>
      <biological_entity constraint="axillary" id="o4717" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s2" to="16" />
      </biological_entity>
      <biological_entity id="o4718" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s2" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o4719" name="basal-cell" name_original="basal-cell" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s2" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity constraint="hair axillary" id="o4720" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves crowded in the stem apex, spreading from the base with incurved tips, weakly contorted to crisped when dry, widespreading when moist;</text>
      <biological_entity constraint="stem" id="o4722" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <biological_entity id="o4723" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o4724" name="tip" name_original="tips" src="d0_s3" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s3" value="incurved" value_original="incurved" />
      </biological_entity>
      <relation from="o4723" id="r1117" name="with" negation="false" src="d0_s3" to="o4724" />
    </statement>
    <statement id="d0_s4">
      <text>oblong to linear-lanceolate, with lamina often of even width to the base, subtubulose, to 3.2 mm;</text>
      <biological_entity id="o4721" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="in stem apex" constraintid="o4722" is_modifier="false" name="arrangement" src="d0_s3" value="crowded" value_original="crowded" />
        <character constraint="from base" constraintid="o4723" is_modifier="false" name="orientation" notes="" src="d0_s3" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="weakly contorted" modifier="when dry" name="shape" notes="" src="d0_s3" to="crisped" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s3" value="widespreading" value_original="widespreading" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o4725" name="lamina" name_original="lamina" src="d0_s4" type="structure" />
      <biological_entity id="o4726" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="3.2" to_unit="mm" />
      </biological_entity>
      <relation from="o4725" id="r1118" name="to" negation="false" src="d0_s4" to="o4726" />
    </statement>
    <statement id="d0_s5">
      <text>base undifferentiated to short-ovate;</text>
      <biological_entity id="o4727" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="undifferentiated" value_original="undifferentiated" />
        <character is_modifier="false" name="shape" src="d0_s5" value="short-ovate" value_original="short-ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>margins plane to weakly incurved, entire or occasionally deeply lobed-dentate, sometimes with 2-stratose areas;</text>
      <biological_entity id="o4728" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="weakly" name="orientation" src="d0_s6" value="incurved" value_original="incurved" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="occasionally deeply" name="architecture_or_shape" src="d0_s6" value="lobed-dentate" value_original="lobed-dentate" />
      </biological_entity>
      <biological_entity id="o4729" name="area" name_original="areas" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <relation from="o4728" id="r1119" modifier="sometimes" name="with" negation="false" src="d0_s6" to="o4729" />
    </statement>
    <statement id="d0_s7">
      <text>apex acute, often narrowly so;</text>
      <biological_entity id="o4730" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>costa percurrent and ending in an apiculus or excurrent as a stout, short-to-elongate mucro, adaxial outgrowths absent, adaxial superficial cells quadrate to shortrectangular, 4–6 cells across costa at mid leaf;</text>
      <biological_entity id="o4731" name="costa" name_original="costa" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="percurrent" value_original="percurrent" />
        <character constraint="as mucro" constraintid="o4733" is_modifier="false" name="architecture" notes="" src="d0_s8" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o4732" name="apiculu" name_original="apiculus" src="d0_s8" type="structure" />
      <biological_entity id="o4733" name="mucro" name_original="mucro" src="d0_s8" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s8" value="stout" value_original="stout" />
        <character is_modifier="true" name="size" src="d0_s8" value="short-to-elongate" value_original="short-to-elongate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4734" name="outgrowth" name_original="outgrowths" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="adaxial superficial" id="o4735" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character constraint="to shortrectangular" constraintid="o4736" is_modifier="false" name="shape" src="d0_s8" value="quadrate" value_original="quadrate" />
      </biological_entity>
      <biological_entity id="o4736" name="shortrectangular" name_original="shortrectangular" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s8" to="6" />
      </biological_entity>
      <biological_entity id="o4737" name="cell" name_original="cells" src="d0_s8" type="structure" />
      <biological_entity id="o4738" name="costa" name_original="costa" src="d0_s8" type="structure" />
      <biological_entity constraint="mid" id="o4739" name="leaf" name_original="leaf" src="d0_s8" type="structure" />
      <relation from="o4731" id="r1120" name="ending in" negation="false" src="d0_s8" to="o4732" />
      <relation from="o4737" id="r1121" name="across" negation="false" src="d0_s8" to="o4738" />
      <relation from="o4738" id="r1122" name="at" negation="false" src="d0_s8" to="o4739" />
    </statement>
    <statement id="d0_s9">
      <text>transverse-section circular to ovate, adaxial epidermis present, adaxial stereid band distinct, 2 or more guide cells in one layer, hydroid strand absent, abaxial stereid band distinct, semicircular to lunate, abaxial epidermis occasionally present, weak;</text>
      <biological_entity id="o4740" name="transverse-section" name_original="transverse-section" src="d0_s9" type="structure">
        <character char_type="range_value" from="circular" name="shape" src="d0_s9" to="ovate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4741" name="epidermis" name_original="epidermis" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4742" name="epidermis" name_original="epidermis" src="d0_s9" type="structure" />
      <biological_entity id="o4743" name="band" name_original="band" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character name="quantity" src="d0_s9" unit="or moreguide" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o4744" name="cell" name_original="cells" src="d0_s9" type="structure" />
      <biological_entity id="o4745" name="layer" name_original="layer" src="d0_s9" type="structure" />
      <biological_entity constraint="hydroid" id="o4746" name="strand" name_original="strand" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4747" name="epidermis" name_original="epidermis" src="d0_s9" type="structure" />
      <biological_entity id="o4748" name="band" name_original="band" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="semicircular" name="shape" src="d0_s9" to="lunate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4749" name="epidermis" name_original="epidermis" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="occasionally" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="fragility" src="d0_s9" value="weak" value_original="weak" />
      </biological_entity>
      <relation from="o4744" id="r1123" name="in" negation="false" src="d0_s9" to="o4745" />
    </statement>
    <statement id="d0_s10">
      <text>proximal laminal cells weakly differentiated in a small group, rectangular, somewhat larger than distal cells, 1–2: 1, walls evenly thickened;</text>
      <biological_entity constraint="proximal laminal" id="o4750" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character constraint="in group" constraintid="o4751" is_modifier="false" modifier="weakly" name="variability" src="d0_s10" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="rectangular" value_original="rectangular" />
        <character constraint="than distal cells" constraintid="o4752" is_modifier="false" name="size" src="d0_s10" value="somewhat larger" value_original="somewhat larger" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s10" to="2" />
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o4751" name="group" name_original="group" src="d0_s10" type="structure">
        <character is_modifier="true" name="size" src="d0_s10" value="small" value_original="small" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4752" name="cell" name_original="cells" src="d0_s10" type="structure" />
      <biological_entity id="o4753" name="wall" name_original="walls" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="evenly" name="size_or_width" src="d0_s10" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>distal laminal cells subquadrate, hexagonal to elliptical, ca. 10–14 µm wide, 1: 1 (–2);</text>
      <biological_entity constraint="distal laminal" id="o4754" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="subquadrate" value_original="subquadrate" />
        <character char_type="range_value" from="hexagonal" name="shape" src="d0_s11" to="elliptical" />
        <character char_type="range_value" from="10" from_unit="um" name="width" src="d0_s11" to="14" to_unit="um" />
        <character name="quantity" src="d0_s11" value="1" value_original="1" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="2" />
        <character name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>papillae typically massive, variously lobed or simple to multifid;</text>
      <biological_entity constraint="distal laminal" id="o4755" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="subquadrate" value_original="subquadrate" />
        <character char_type="range_value" from="hexagonal" name="shape" src="d0_s12" to="elliptical" />
        <character char_type="range_value" from="10" from_unit="um" name="width" src="d0_s12" to="14" to_unit="um" />
        <character name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o4756" name="papilla" name_original="papillae" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="typically" name="size" src="d0_s12" value="massive" value_original="massive" />
        <character is_modifier="false" modifier="variously" name="shape" src="d0_s12" value="lobed" value_original="lobed" />
        <character name="shape" src="d0_s12" value="simple to multifid" value_original="simple to multifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>cell-walls evenly thickened, lumens rounded to subangular, superficial walls subconvex.</text>
      <biological_entity constraint="distal laminal" id="o4757" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="subquadrate" value_original="subquadrate" />
        <character char_type="range_value" from="hexagonal" name="shape" src="d0_s13" to="elliptical" />
        <character char_type="range_value" from="10" from_unit="um" name="width" src="d0_s13" to="14" to_unit="um" />
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o4758" name="cell-wall" name_original="cell-walls" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="evenly" name="size_or_width" src="d0_s13" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o4759" name="lumen" name_original="lumens" src="d0_s13" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s13" to="subangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s15">
      <text>Sexual condition dioicous.</text>
      <biological_entity constraint="superficial" id="o4760" name="wall" name_original="walls" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="subconvex" value_original="subconvex" />
        <character is_modifier="false" name="development" src="d0_s14" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s15" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s15" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Perichaetia terminal, leaves abruptly sheathing at the base, otherwise similar to cauline leaves.</text>
      <biological_entity id="o4761" name="perichaetium" name_original="perichaetia" src="d0_s16" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s16" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o4762" name="leaf" name_original="leaves" src="d0_s16" type="structure">
        <character constraint="at base" constraintid="o4763" is_modifier="false" modifier="abruptly" name="architecture_or_shape" src="d0_s16" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o4763" name="base" name_original="base" src="d0_s16" type="structure" />
      <biological_entity constraint="cauline" id="o4764" name="leaf" name_original="leaves" src="d0_s16" type="structure" />
      <relation from="o4762" id="r1124" modifier="otherwise" name="to" negation="false" src="d0_s16" to="o4764" />
    </statement>
    <statement id="d0_s17">
      <text>Seta to 8 mm.</text>
      <biological_entity id="o4765" name="seta" name_original="seta" src="d0_s17" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s17" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Capsule stegocarpous, theca ellipsoidal to short-cylindric, ca. 1–1.5 mm, annulus of persistent non or strongly vesiculose cells;</text>
      <biological_entity id="o4766" name="capsule" name_original="capsule" src="d0_s18" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s18" value="stegocarpous" value_original="stegocarpous" />
      </biological_entity>
      <biological_entity id="o4767" name="theca" name_original="theca" src="d0_s18" type="structure">
        <character char_type="range_value" from="ellipsoidal" name="shape" src="d0_s18" to="short-cylindric" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s18" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4768" name="annulus" name_original="annulus" src="d0_s18" type="structure" />
      <biological_entity id="o4769" name="cell" name_original="cells" src="d0_s18" type="structure">
        <character is_modifier="true" name="duration" src="d0_s18" value="persistent" value_original="persistent" />
        <character is_modifier="true" modifier="non; strongly" name="architecture" src="d0_s18" value="vesiculose" value_original="vesiculose" />
      </biological_entity>
      <relation from="o4768" id="r1125" name="consists_of" negation="false" src="d0_s18" to="o4769" />
    </statement>
    <statement id="d0_s19">
      <text>operculum long-rostrate of cells in straight rows;</text>
      <biological_entity id="o4770" name="operculum" name_original="operculum" src="d0_s19" type="structure">
        <character constraint="of cells" constraintid="o4771" is_modifier="false" name="shape" src="d0_s19" value="long-rostrate" value_original="long-rostrate" />
      </biological_entity>
      <biological_entity id="o4771" name="cell" name_original="cells" src="d0_s19" type="structure" />
      <biological_entity id="o4772" name="row" name_original="rows" src="d0_s19" type="structure">
        <character is_modifier="true" name="course" src="d0_s19" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o4771" id="r1126" name="in" negation="false" src="d0_s19" to="o4772" />
    </statement>
    <statement id="d0_s20">
      <text>peristome absent or rudimentary, papillose.</text>
      <biological_entity id="o4773" name="peristome" name_original="peristome" src="d0_s20" type="structure">
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s20" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="relief" src="d0_s20" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Calyptra unknown.</text>
      <biological_entity id="o4774" name="calyptra" name_original="calyptra" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>Spores 8–13 µm. Laminal KOH color reaction yellow.</text>
      <biological_entity id="o4775" name="spore" name_original="spores" src="d0_s22" type="structure">
        <character char_type="range_value" from="8" from_unit="um" name="some_measurement" src="d0_s22" to="13" to_unit="um" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="laminal" value_original="laminal" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>se United States, Mexico, Central America, e Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="se United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="e Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>Species 4 (1 in the flora).</discussion>
  <discussion>Tuerckheimia was considerably revised by R. H. Zander (1978). It is characterized by narrow leaves with acute apices, plane margins and peculiar, massive papillae, best seen in the distal portion of the leaf. Tuerckheimia calculosa R. H. Zander &amp; P. M. Eckel of the West Indies and Mexico, which possesses similar papillae characteristics, has been transferred to its own monotypic genus [Quaesticula navicularis (Mitten) R. H. Zander] on the basis of ligulate leaves with cucullate apices and strongly involute margins, subpercurrent costa, and well-developed peristome (Zander 1993).</discussion>
  <references>
    <reference>Zander, R. H. 1978. Synopsis of the genus Tuerckheimia (Pottiaceae). Misc. Bryol. Lichenol. 8: 25–28.</reference>
  </references>
  
</bio:treatment>