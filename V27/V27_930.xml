<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Richard H. Zander</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">265</other_info_on_meta>
    <other_info_on_meta type="mention_page">480</other_info_on_meta>
    <other_info_on_meta type="mention_page">608</other_info_on_meta>
    <other_info_on_meta type="mention_page">632</other_info_on_meta>
    <other_info_on_meta type="treatment_page">631</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="R. H. Zander" date="1989" rank="genus">HILPERTIA</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>65: 427. 1989  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus HILPERTIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Friedrich Hilpert, b. 1907, German bryologist</other_info_on_name>
    <other_info_on_name type="fna_id">202002</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in loose cushions, greenish brown distally, light-brown proximally.</text>
      <biological_entity id="o4000" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="distally" name="coloration" notes="" src="d0_s0" value="greenish brown" value_original="greenish brown" />
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s0" value="light-brown" value_original="light-brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4001" name="cushion" name_original="cushions" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o4000" id="r985" name="in" negation="false" src="d0_s0" to="o4001" />
    </statement>
    <statement id="d0_s1">
      <text>Stems to 1 cm, hyalodermis absent, sclerodermis not or weakly differentiated, central strand distinct;</text>
      <biological_entity id="o4002" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4003" name="hyalodermi" name_original="hyalodermis" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o4004" name="scleroderm" name_original="sclerodermis" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="weakly" name="variability" src="d0_s1" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity constraint="central" id="o4005" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s1" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>axillary hairs to 8 cells long, all hyaline or the proximal one yellowbrown.</text>
      <biological_entity constraint="axillary" id="o4006" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s2" to="8" />
      </biological_entity>
      <biological_entity id="o4007" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s2" value="long" value_original="long" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o4008" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowbrown" value_original="yellowbrown" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stem-leaves crowded, larger distally, appressed and tightly spiraled when dry, weakly spreading when moist, 1–1.4 (plus 0.3–0.6 of awn) mm;</text>
    </statement>
    <statement id="d0_s4">
      <text>ovate to circular, adaxial surface flat to quite concave, base not differentiated;</text>
      <biological_entity id="o4009" name="leaf-stem" name_original="stem-leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="crowded" value_original="crowded" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s3" value="larger" value_original="larger" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="when dry" name="arrangement" src="d0_s3" value="spiraled" value_original="spiraled" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="1" from_unit="mm" name="distance" src="d0_s3" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="circular" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4010" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="flat to quite" value_original="flat to quite" />
        <character is_modifier="false" modifier="quite" name="shape" src="d0_s4" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity id="o4011" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal margins strongly revolute (to 2 ×), entire or broadly dentate above, cells of revolute margin enlarged, strongly chlorophyllose;</text>
      <biological_entity constraint="distal" id="o4012" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="broadly" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o4013" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity id="o4014" name="margin" name_original="margin" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
      </biological_entity>
      <relation from="o4013" id="r986" name="part_of" negation="false" src="d0_s5" to="o4014" />
    </statement>
    <statement id="d0_s6">
      <text>apex broadly acute, hyaline in an apical triangle;</text>
      <biological_entity id="o4015" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character constraint="in apical triangle" constraintid="o4016" is_modifier="false" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity constraint="apical" id="o4016" name="triangle" name_original="triangle" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>costa narrow but broader above, excurrent as a hyaline awn, lamina inserted laterally, adaxial outgrowths none, superficial adaxial cells long-rectangular, in 2–4 rows;</text>
      <biological_entity id="o4017" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="width" src="d0_s7" value="broader" value_original="broader" />
        <character constraint="as awn" constraintid="o4018" is_modifier="false" name="architecture" src="d0_s7" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o4018" name="awn" name_original="awn" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o4019" name="lamina" name_original="lamina" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="laterally" name="position" src="d0_s7" value="inserted" value_original="inserted" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4020" name="outgrowth" name_original="outgrowths" src="d0_s7" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s7" value="0" value_original="none" />
      </biological_entity>
      <biological_entity constraint="adaxial superficial" id="o4021" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="long-rectangular" value_original="long-rectangular" />
      </biological_entity>
      <biological_entity id="o4022" name="row" name_original="rows" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <relation from="o4021" id="r987" name="in" negation="false" src="d0_s7" to="o4022" />
    </statement>
    <statement id="d0_s8">
      <text>transverse-section rounded, adaxial epidermis present, adaxial stereid band absent, guide cells 2 in 1 layer, hydroid strand present, abaxial stereid band present, rounded in cross-sectional shape, abaxial epidermis absent;</text>
      <biological_entity id="o4023" name="transverse-section" name_original="transverse-section" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4024" name="epidermis" name_original="epidermis" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4025" name="epidermis" name_original="epidermis" src="d0_s8" type="structure" />
      <biological_entity id="o4026" name="band" name_original="band" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="guide" id="o4027" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character constraint="in layer" constraintid="o4028" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o4028" name="layer" name_original="layer" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="hydroid" id="o4029" name="strand" name_original="strand" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4030" name="epidermis" name_original="epidermis" src="d0_s8" type="structure" />
      <biological_entity id="o4031" name="band" name_original="band" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4032" name="epidermis" name_original="epidermis" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>basal-cells weakly differentiated, rectangular, thin-walled;</text>
      <biological_entity id="o4033" name="basal-cell" name_original="basal-cells" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="weakly" name="variability" src="d0_s9" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" src="d0_s9" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>distal medial cells hexagonal to shortrectangular or rhomboid, rather large, (14–) 20–25 µm wide, 1–4: 1;</text>
      <biological_entity constraint="distal medial" id="o4034" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character constraint="to shortrectangular" constraintid="o4035" is_modifier="false" name="shape" src="d0_s10" value="hexagonal" value_original="hexagonal" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rhomboid" value_original="rhomboid" />
        <character is_modifier="false" modifier="rather" name="size" src="d0_s10" value="large" value_original="large" />
        <character char_type="range_value" from="14" from_unit="um" name="width" src="d0_s10" to="20" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="20" from_unit="um" name="width" src="d0_s10" to="25" to_unit="um" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s10" to="4" />
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o4035" name="shortrectangular" name_original="shortrectangular" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>papillae absent medially, hollow-papillose on revolute margins, cell-walls thin to thickened and porose, superficial abaxial cell-walls thick, weakly convex superficially on both sides, cells of leaf apex rhomboid to fusiform, smooth.</text>
      <biological_entity constraint="distal medial" id="o4036" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character constraint="to " constraintid="o4038" is_modifier="false" name="shape" src="d0_s11" value="hexagonal" value_original="hexagonal" />
        <character constraint="on margins" constraintid="o4043" is_modifier="false" name="relief" notes="" src="d0_s11" value="hollow-papillose" value_original="hollow-papillose" />
      </biological_entity>
      <biological_entity id="o4037" name="shortrectangular" name_original="shortrectangular" src="d0_s11" type="structure" />
      <biological_entity id="o4038" name="papilla" name_original="papillae" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="rhomboid" value_original="rhomboid" />
        <character is_modifier="true" modifier="rather" name="size" src="d0_s11" value="large" value_original="large" />
        <character is_modifier="true" name="width" src="d0_s11" value="wide" value_original="wide" />
        <character char_type="range_value" from="14" from_unit="um" name="atypical_some_measurement" src="d0_s11" to="20" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" constraint="to " constraintid="o4040" from="20" from_unit="um" name="some_measurement" src="d0_s11" to="25" to_unit="um" />
      </biological_entity>
      <biological_entity id="o4039" name="shortrectangular" name_original="shortrectangular" src="d0_s11" type="structure" />
      <biological_entity id="o4040" name="papilla" name_original="papillae" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="rhomboid" value_original="rhomboid" />
        <character is_modifier="true" modifier="rather" name="size" src="d0_s11" value="large" value_original="large" />
        <character is_modifier="true" name="width" src="d0_s11" value="wide" value_original="wide" />
        <character char_type="range_value" constraint="to " constraintid="o4042" from="1" name="quantity" src="d0_s11" to="4" />
      </biological_entity>
      <biological_entity id="o4041" name="shortrectangular" name_original="shortrectangular" src="d0_s11" type="structure" />
      <biological_entity id="o4042" name="papilla" name_original="papillae" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="rhomboid" value_original="rhomboid" />
        <character is_modifier="true" modifier="rather" name="size" src="d0_s11" value="large" value_original="large" />
        <character is_modifier="true" name="width" src="d0_s11" value="wide" value_original="wide" />
        <character is_modifier="false" modifier="medially" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o4043" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape_or_vernation" src="d0_s11" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o4044" name="cell-wall" name_original="cell-walls" src="d0_s11" type="structure">
        <character char_type="range_value" from="thin" name="width" src="d0_s11" to="thickened" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="porose" value_original="porose" />
      </biological_entity>
      <biological_entity constraint="superficial abaxial" id="o4045" name="cell-wall" name_original="cell-walls" src="d0_s11" type="structure">
        <character is_modifier="false" name="width" src="d0_s11" value="thick" value_original="thick" />
        <character constraint="on sides" constraintid="o4046" is_modifier="false" modifier="weakly" name="shape" src="d0_s11" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o4046" name="side" name_original="sides" src="d0_s11" type="structure" />
      <biological_entity constraint="leaf" id="o4048" name="apex" name_original="apex" src="d0_s11" type="structure" />
      <relation from="o4047" id="r988" name="part_of" negation="false" src="d0_s11" to="o4048" />
    </statement>
    <statement id="d0_s12">
      <text>Specialized asexual reproduction by brood bodies borne on proximal rhizoids, brown, spheric to elliptic.</text>
      <biological_entity id="o4047" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character char_type="range_value" from="rhomboid" name="shape" src="d0_s11" to="fusiform" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="development" src="d0_s12" value="specialized" value_original="specialized" />
      </biological_entity>
      <biological_entity id="o4049" name="brood" name_original="brood" src="d0_s12" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s12" value="asexual" value_original="asexual" />
      </biological_entity>
      <biological_entity id="o4050" name="body" name_original="bodies" src="d0_s12" type="structure" />
      <biological_entity constraint="proximal" id="o4051" name="rhizoid" name_original="rhizoids" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character char_type="range_value" from="spheric" name="shape" src="d0_s12" to="elliptic" />
      </biological_entity>
      <relation from="o4049" id="r989" name="borne on" negation="false" src="d0_s12" to="o4051" />
      <relation from="o4050" id="r990" name="borne on" negation="false" src="d0_s12" to="o4051" />
    </statement>
    <statement id="d0_s13">
      <text>Sexual condition polyoicous: synoicous, paroicous, autoicous or perigonia terminal on separate plants.</text>
      <biological_entity id="o4052" name="whole-organism" name_original="" src="d0_s13" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s13" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="polyoicous" value_original="polyoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="synoicous" value_original="synoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="paroicous" value_original="paroicous" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="autoicous" value_original="autoicous" />
      </biological_entity>
      <biological_entity id="o4053" name="perigonium" name_original="perigonia" src="d0_s13" type="structure">
        <character constraint="on plants" constraintid="o4054" is_modifier="false" name="position_or_structure_subtype" src="d0_s13" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o4054" name="plant" name_original="plants" src="d0_s13" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s13" value="separate" value_original="separate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Perichaetia terminal, interior leaves sheathing, usually differentiated, long-oval, margins usually somewhat differentiated, medial laminal cells rectangular, very thin-walled.</text>
      <biological_entity id="o4055" name="perichaetium" name_original="perichaetia" src="d0_s14" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s14" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity constraint="interior" id="o4056" name="leaf" name_original="leaves" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" modifier="usually" name="variability" src="d0_s14" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" src="d0_s14" value="long-oval" value_original="long-oval" />
      </biological_entity>
      <biological_entity id="o4057" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually somewhat" name="variability" src="d0_s14" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity constraint="medial laminal" id="o4058" name="cell" name_original="cells" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" modifier="very" name="architecture" src="d0_s14" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seta 0.35–0.4 cm.</text>
      <biological_entity id="o4059" name="seta" name_original="seta" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.35" from_unit="cm" name="some_measurement" src="d0_s15" to="0.4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsule stegocarpous, theca elliptic, occasionally weakly ventricose, 1.2–1.5 mm, annulus of 3 rows of smaller, quadrate, highly vesiculose cells;</text>
      <biological_entity id="o4060" name="capsule" name_original="capsule" src="d0_s16" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="stegocarpous" value_original="stegocarpous" />
      </biological_entity>
      <biological_entity id="o4061" name="theca" name_original="theca" src="d0_s16" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s16" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="occasionally weakly" name="shape" src="d0_s16" value="ventricose" value_original="ventricose" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4062" name="annulus" name_original="annulus" src="d0_s16" type="structure" />
      <biological_entity id="o4063" name="row" name_original="rows" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o4064" name="cell" name_original="cells" src="d0_s16" type="structure">
        <character is_modifier="true" name="size" src="d0_s16" value="smaller" value_original="smaller" />
        <character is_modifier="true" name="shape" src="d0_s16" value="quadrate" value_original="quadrate" />
        <character is_modifier="true" modifier="highly" name="architecture" src="d0_s16" value="vesiculose" value_original="vesiculose" />
      </biological_entity>
      <relation from="o4062" id="r991" name="consist_of" negation="false" src="d0_s16" to="o4063" />
      <relation from="o4063" id="r992" name="part_of" negation="false" src="d0_s16" to="o4064" />
    </statement>
    <statement id="d0_s17">
      <text>operculum broadly short-conic to long-conic, peristome teeth 32, long, linear, twisted counterclockwise about 1/2 turn.</text>
      <biological_entity id="o4065" name="operculum" name_original="operculum" src="d0_s17" type="structure">
        <character char_type="range_value" from="broadly short-conic" name="shape" src="d0_s17" to="long-conic" />
      </biological_entity>
      <biological_entity constraint="peristome" id="o4066" name="tooth" name_original="teeth" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="32" value_original="32" />
        <character is_modifier="false" name="length_or_size" src="d0_s17" value="long" value_original="long" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s17" value="linear" value_original="linear" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="twisted" value_original="twisted" />
        <character name="quantity" src="d0_s17" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Calyptra cucullate.</text>
      <biological_entity id="o4067" name="calyptra" name_original="calyptra" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="cucullate" value_original="cucullate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Spores 13–16 µm. KOH laminal color reaction red.</text>
      <biological_entity id="o4068" name="spore" name_original="spores" src="d0_s19" type="structure">
        <character char_type="range_value" from="13" from_unit="um" name="some_measurement" src="d0_s19" to="16" to_unit="um" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="laminal" value_original="laminal" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="red" value_original="red" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Canada, Europe, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Canada" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>35.</number>
  <discussion>Species 1.</discussion>
  <discussion>Hilpertia is unique among the Pottioideae in the elaboration of a photosynthetic organ consisting of laminal marginal cells rolled in a spiral tube, thin-walled and hollow-papillose. The species is characteristic of cold steppe habitats.</discussion>
  
</bio:treatment>