<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Patricia M. Eckel</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">205</other_info_on_meta>
    <other_info_on_meta type="mention_page">206</other_info_on_meta>
    <other_info_on_meta type="mention_page">615</other_info_on_meta>
    <other_info_on_meta type="treatment_page">265</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Brotherus &amp; Müller Hal." date="1898" rank="genus">INDUSIELLA</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Centralbl.</publication_title>
      <place_in_publication>75: 322. 1898  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus INDUSIELLA</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin indusium, tunic, and -ella, diminutive, alluding to inrolled hyaline leaf margins</other_info_on_name>
    <other_info_on_name type="fna_id">116415</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (6.5–) 7–9 mm, succulent, in dense cushions, brownish and translucent proximally, succulent, blackish green and opaque distally.</text>
      <biological_entity id="o4668" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6.5" from_unit="mm" name="atypical_some_measurement" src="d0_s0" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s0" to="9" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="brownish and translucent" value_original="brownish and translucent" />
        <character is_modifier="false" modifier="proximally" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s0" value="blackish green and opaque" value_original="blackish green and opaque" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4669" name="cushion" name_original="cushions" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o4668" id="r1071" name="in" negation="false" src="d0_s0" to="o4669" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves ellipsoid-ligulate, leaf limb ligulate and tubulose-fistulose beyond a flat, flaring and sheathing base, apex cucullate-fistulose;</text>
      <biological_entity id="o4670" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="ellipsoid-ligulate" value_original="ellipsoid-ligulate" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o4671" name="limb" name_original="limb" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="ligulate" value_original="ligulate" />
        <character constraint="beyond base" constraintid="o4672" is_modifier="false" name="shape" src="d0_s1" value="tubulose-fistulose" value_original="tubulose-fistulose" />
      </biological_entity>
      <biological_entity id="o4672" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="true" name="prominence_or_shape" src="d0_s1" value="flat" value_original="flat" />
        <character is_modifier="true" name="shape" src="d0_s1" value="flaring" value_original="flaring" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o4673" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="cucullate-fistulose" value_original="cucullate-fistulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>lamina beyond the base spirally inrolled, 2-stratose except for a margin 2-stratose or 1-stratose in several rows, adaxial cells and cells of inrolled margins thin-walled, hyaline, strongly chlorophyllose, muticous or minutely apiculate;</text>
      <biological_entity id="o4674" name="lamina" name_original="lamina" src="d0_s2" type="structure">
        <character constraint="except-for margin" constraintid="o4676" is_modifier="false" name="architecture" notes="" src="d0_s2" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity id="o4675" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="spirally" name="shape_or_vernation" src="d0_s2" value="inrolled" value_original="inrolled" />
      </biological_entity>
      <biological_entity id="o4676" name="margin" name_original="margin" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="2-stratose" value_original="2-stratose" />
        <character constraint="in rows" constraintid="o4677" is_modifier="false" name="architecture" src="d0_s2" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <biological_entity id="o4677" name="row" name_original="rows" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="several" value_original="several" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4679" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="thin-walled" value_original="thin-walled" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s2" value="muticous" value_original="muticous" />
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s2" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o4680" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape_or_vernation" src="d0_s2" value="inrolled" value_original="inrolled" />
      </biological_entity>
      <relation from="o4674" id="r1072" name="beyond" negation="false" src="d0_s2" to="o4675" />
      <relation from="o4679" id="r1073" name="part_of" negation="false" src="d0_s2" to="o4680" />
    </statement>
    <statement id="d0_s3">
      <text>basal-cells heterogeneous, oblate, quadrate and short-rectangular, with straight, somewhat thinner walls than cells in the leaf limb;</text>
      <biological_entity id="o4681" name="basal-cell" name_original="basal-cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="variability" src="d0_s3" value="heterogeneous" value_original="heterogeneous" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblate" value_original="oblate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="short-rectangular" value_original="short-rectangular" />
      </biological_entity>
      <biological_entity id="o4682" name="wall" name_original="walls" src="d0_s3" type="structure">
        <character is_modifier="true" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="true" modifier="somewhat" name="width" src="d0_s3" value="thinner" value_original="thinner" />
      </biological_entity>
      <biological_entity id="o4683" name="cell" name_original="cells" src="d0_s3" type="structure" />
      <biological_entity constraint="leaf" id="o4684" name="limb" name_original="limb" src="d0_s3" type="structure" />
      <relation from="o4681" id="r1074" name="with" negation="false" src="d0_s3" to="o4682" />
      <relation from="o4683" id="r1075" name="in" negation="false" src="d0_s3" to="o4684" />
    </statement>
    <statement id="d0_s4">
      <text>mid leaf and distal cells quadrate or rectangular, with straight, somewhat thick walls.</text>
      <biological_entity constraint="mid" id="o4685" name="leaf" name_original="leaf" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rectangular" value_original="rectangular" />
      </biological_entity>
      <biological_entity constraint="mid distal" id="o4686" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rectangular" value_original="rectangular" />
      </biological_entity>
      <biological_entity id="o4687" name="wall" name_original="walls" src="d0_s4" type="structure">
        <character is_modifier="true" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="true" modifier="somewhat" name="width" src="d0_s4" value="thick" value_original="thick" />
      </biological_entity>
      <relation from="o4685" id="r1076" name="with" negation="false" src="d0_s4" to="o4687" />
      <relation from="o4686" id="r1077" name="with" negation="false" src="d0_s4" to="o4687" />
    </statement>
    <statement id="d0_s5">
      <text>Gemmae absent.</text>
    </statement>
    <statement id="d0_s6">
      <text>Sexual condition autoicous;</text>
      <biological_entity id="o4688" name="gemma" name_original="gemmae" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perichaetial leaves slightly enlarged.</text>
      <biological_entity constraint="perichaetial" id="o4689" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s7" value="enlarged" value_original="enlarged" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>[Seta short, straight. Capsule erect, shortly exserted, symmetric, subglobose; annulus differentiated, of small non-vesciculose, quadrate, thin-walled cells, persistent; operculum long-rostrate, falling detached from the columella. Calyptra campanulate-mitrate, not erose, deeply lobed, large, covering the entire capsule, deeply plicate.]</text>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska, Asia, Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>Species 1.</discussion>
  <discussion>Affiliation of Indusiella with the Grimmiaceae is most evident in the large, campanulate-plicate calyptra and rather short, cribrose peristome. The thickened transverse basal cell walls contrasted with the thinner longitudinal walls are typical of many species of Grimmia, though also of Encalypta. Indusiella is the only genus in the Grimmiaceae with specialized photosynthetic modifications in the leaf blade, although the family contains one other monotypic genus, Aligrimmia R. S. Williams of arid regions of Peru, with peculiar costal lamellae that apparently enhance photosynthetic capacity. Enhanced laminal photosynthetic tissue within rolled laminal margins is, however, also characteristic of a few species in the pottiaceous genera Hilpertia, Pseudocrossidium, and Tortula. Although the habit and leaf shape of Indusiella closely resemble those of Aloina and Crossidium in the Pottiaceae, it lacks the filaments on the adaxial surface of the costa that are characteristic of those genera. B. M. Murray (1984) discussed xeromorphic specializations of genera in both families as well as phytogeographic considerations relevant to a relict Beringian flora on slopes along the Yukon River basin along the Alaska-Canada border where the North American collection of Indusiella was made.</discussion>
  <discussion>Indusiella is found in arctic-alpine, high altitude desert and steppe regions.</discussion>
  <references>
    <reference>Murray, B. M. 1984. A revision of the monotypic genera Indusiella, Aligrimmia and Coscinodontella (Musci: Grimmiaceae), with comments on convergent xeromorphological features. Bryologist 87: 24–36.</reference>
  </references>
  
</bio:treatment>