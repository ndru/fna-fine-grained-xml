<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">321</other_info_on_meta>
    <other_info_on_meta type="mention_page">326</other_info_on_meta>
    <other_info_on_meta type="treatment_page">324</other_info_on_meta>
    <other_info_on_meta type="illustration_page">325</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">seligeriaceae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="1856" rank="genus">seligeria</taxon_name>
    <taxon_name authority="Berggren" date="1875" rank="species">polaris</taxon_name>
    <place_of_publication>
      <publication_title>Kongl. Svenska Vetensk. Acad. Handl., n. s.</publication_title>
      <place_in_publication>13(7): 41. 1875,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family seligeriaceae;genus seligeria;species polaris</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443529</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small, black to red-black.</text>
      <biological_entity id="o5358" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character char_type="range_value" from="black" name="coloration" src="d0_s0" to="red-black" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves ovatelanceolate, subulate from oblong-ovate base, bluntly acute to narrowly obtuse;</text>
      <biological_entity id="o5359" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character constraint="from base" constraintid="o5360" is_modifier="false" name="shape" src="d0_s1" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="bluntly acute" name="shape" notes="" src="d0_s1" to="narrowly obtuse" />
      </biological_entity>
      <biological_entity id="o5360" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="oblong-ovate" value_original="oblong-ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>costa ending in apex, filling subula;</text>
      <biological_entity id="o5361" name="costa" name_original="costa" src="d0_s2" type="structure" />
      <biological_entity id="o5362" name="apex" name_original="apex" src="d0_s2" type="structure" />
      <biological_entity id="o5363" name="subulum" name_original="subula" src="d0_s2" type="structure" />
      <relation from="o5361" id="r1257" name="ending in" negation="false" src="d0_s2" to="o5362" />
      <relation from="o5361" id="r1258" name="filling" negation="false" src="d0_s2" to="o5363" />
    </statement>
    <statement id="d0_s3">
      <text>margins entire;</text>
      <biological_entity id="o5364" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaf cells (1–) 2: 1;</text>
      <biological_entity constraint="leaf" id="o5365" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s4" to="2" to_inclusive="false" />
        <character name="quantity" src="d0_s4" value="2" value_original="2" />
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>perichaetial leaves somewhat larger, similar to vegetative leaves, not much differentiated.</text>
      <biological_entity constraint="leaf" id="o5366" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s5" to="2" to_inclusive="false" />
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="perichaetial" id="o5367" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="somewhat" name="size" src="d0_s5" value="larger" value_original="larger" />
        <character is_modifier="false" modifier="not much" name="variability" notes="" src="d0_s5" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o5368" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s5" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <relation from="o5367" id="r1259" name="to" negation="false" src="d0_s5" to="o5368" />
    </statement>
    <statement id="d0_s6">
      <text>Seta 2–3 mm, straight, slightly flexuose or curved, slender.</text>
      <biological_entity id="o5369" name="seta" name_original="seta" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s6" value="flexuose" value_original="flexuose" />
        <character is_modifier="false" name="course" src="d0_s6" value="curved" value_original="curved" />
        <character is_modifier="false" name="size" src="d0_s6" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsule ovate to obovate, slightly longer than wide, widest at mouth;</text>
      <biological_entity id="o5370" name="capsule" name_original="capsule" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="obovate" />
        <character is_modifier="false" name="length_or_size" src="d0_s7" value="slightly longer than wide" value_original="slightly longer than wide" />
        <character constraint="at mouth" constraintid="o5371" is_modifier="false" name="width" src="d0_s7" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity id="o5371" name="mouth" name_original="mouth" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>peristome of 16 well-developed teeth;</text>
      <biological_entity id="o5372" name="peristome" name_original="peristome" src="d0_s8" type="structure" />
      <biological_entity id="o5373" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="16" value_original="16" />
        <character is_modifier="true" name="development" src="d0_s8" value="well-developed" value_original="well-developed" />
      </biological_entity>
      <relation from="o5372" id="r1260" name="consist_of" negation="false" src="d0_s8" to="o5373" />
    </statement>
    <statement id="d0_s9">
      <text>columella immersed.</text>
      <biological_entity id="o5374" name="columella" name_original="columella" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s9" value="immersed" value_original="immersed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spores (16–) 17–25 (–27) µm.</text>
      <biological_entity id="o5375" name="spore" name_original="spores" src="d0_s10" type="structure">
        <character char_type="range_value" from="16" from_unit="um" name="atypical_some_measurement" src="d0_s10" to="17" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s10" to="27" to_unit="um" />
        <character char_type="range_value" from="17" from_unit="um" name="some_measurement" src="d0_s10" to="25" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Calcareous substrate</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous substrate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; N.W.T., Nunavut, Yukon; Alaska; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <discussion>Seligeria polaris is known from the Canadian Arctic Islands and from northern Alaska and Yukon, and is disjunct in southwestern Northwest Territories. It is easily recognized by relatively large-sized, blackish plants; recurved-twisted leaves; long and slightly curved seta; and spores that are 17–25 µm. This species somewhat resembles Blindia acuta in color and the presence of somewhat differentiated alar cells, but is distinguished by calcareous habitat (versus acidic rock habitat in Blindia) and smaller plant size.</discussion>
  
</bio:treatment>