<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">210</other_info_on_meta>
    <other_info_on_meta type="mention_page">216</other_info_on_meta>
    <other_info_on_meta type="mention_page">218</other_info_on_meta>
    <other_info_on_meta type="treatment_page">214</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="1845" rank="genus">schistidium</taxon_name>
    <taxon_name authority="(Funck) Bruch &amp; Schimper" date="1845" rank="species">confertum</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Europ.</publication_title>
      <place_in_publication>3: 99. 1845,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus schistidium;species confertum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075491</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="Funck" date="unknown" rank="species">conferta</taxon_name>
    <place_of_publication>
      <publication_title>Deutschl. Moose,</publication_title>
      <place_in_publication>18. 1820</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Grimmia;species conferta;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in compact, often flattish cushions or tufts, olivaceous, brown, or yellowbrown, rarely nearly black.</text>
      <biological_entity id="o7038" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="olivaceous" value_original="olivaceous" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" modifier="rarely nearly" name="coloration" src="d0_s0" value="black" value_original="black" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7039" name="cushion" name_original="cushions" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character is_modifier="true" modifier="often" name="shape" src="d0_s0" value="flattish" value_original="flattish" />
      </biological_entity>
      <biological_entity id="o7040" name="tuft" name_original="tufts" src="d0_s0" type="structure" />
      <relation from="o7038" id="r1966" name="in" negation="false" src="d0_s0" to="o7039" />
      <relation from="o7038" id="r1967" name="in" negation="false" src="d0_s0" to="o7040" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.3–1.2 (–2) cm, central strand narrow or absent.</text>
      <biological_entity id="o7041" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s1" to="1.2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o7042" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s1" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect, sometimes slightly curved, imbricate when dry, ovatelanceolate to ovate-triangular, obtusely keeled or concave proximally, sharply keeled distally, 0.8–1.8 (–2) mm, 1-stratose or 2-stratose in striae distally;</text>
      <biological_entity id="o7043" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes slightly" name="course" src="d0_s2" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="when dry" name="arrangement" src="d0_s2" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s2" to="ovate-triangular obtusely keeled or concave proximally" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s2" to="ovate-triangular obtusely keeled or concave proximally" />
        <character is_modifier="false" modifier="sharply; distally" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="1.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s2" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="1-stratose" value_original="1-stratose" />
        <character constraint="in striae" constraintid="o7044" is_modifier="false" name="architecture" src="d0_s2" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity id="o7044" name="stria" name_original="striae" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>margins recurved in medial sections, usually less recurved on one side of leaf, rarely plane, smooth, 2-stratose, occasionally 3-stratose;</text>
      <biological_entity id="o7045" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="in medial sections" constraintid="o7046" is_modifier="false" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character constraint="on side" constraintid="o7047" is_modifier="false" modifier="usually less" name="orientation" notes="" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="rarely" name="shape" notes="" src="d0_s3" value="plane" value_original="plane" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s3" value="3-stratose" value_original="3-stratose" />
      </biological_entity>
      <biological_entity constraint="medial" id="o7046" name="section" name_original="sections" src="d0_s3" type="structure" />
      <biological_entity id="o7047" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o7048" name="leaf" name_original="leaf" src="d0_s3" type="structure" />
      <relation from="o7047" id="r1968" name="part_of" negation="false" src="d0_s3" to="o7048" />
    </statement>
    <statement id="d0_s4">
      <text>apices usually acute, rarely subobtuse;</text>
      <biological_entity id="o7049" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="subobtuse" value_original="subobtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa percurrent or excurrent as a short, spinulose-denticulate, often flexuose awn, smooth;</text>
      <biological_entity id="o7050" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="percurrent" value_original="percurrent" />
        <character constraint="as awn" constraintid="o7051" is_modifier="false" name="architecture" src="d0_s5" value="excurrent" value_original="excurrent" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s5" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o7051" name="awn" name_original="awn" src="d0_s5" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="true" name="shape" src="d0_s5" value="spinulose-denticulate" value_original="spinulose-denticulate" />
        <character is_modifier="true" modifier="often" name="course" src="d0_s5" value="flexuose" value_original="flexuose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal marginal cells quadrate or short-rectangular, transverse walls usually thicker than longitudinal walls, sometimes trigonous;</text>
      <biological_entity constraint="basal marginal" id="o7052" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="short-rectangular" value_original="short-rectangular" />
      </biological_entity>
      <biological_entity id="o7053" name="wall" name_original="walls" src="d0_s6" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s6" value="transverse" value_original="transverse" />
        <character constraint="than longitudinal walls" constraintid="o7054" is_modifier="false" name="width" src="d0_s6" value="usually thicker" value_original="usually thicker" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="trigonous" value_original="trigonous" />
      </biological_entity>
      <biological_entity id="o7054" name="wall" name_original="walls" src="d0_s6" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s6" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal cells mostly isodiametric, rounded or angular, or short-rectangular, 5–8 µm wide, smooth, weakly sinuose.</text>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition autoicous.</text>
      <biological_entity constraint="distal" id="o7055" name="bud" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s7" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="angular" value_original="angular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="short-rectangular" value_original="short-rectangular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="angular" value_original="angular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="short-rectangular" value_original="short-rectangular" />
        <character char_type="range_value" from="5" from_unit="um" name="width" src="d0_s7" to="8" to_unit="um" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s7" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule yellowbrown or light reddish-brown, short-cylindric or cupulate, rarely campanulate, rarely becoming finely striate with age, (0.5–) 0.7–1 mm;</text>
      <biological_entity id="o7056" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="light reddish-brown" value_original="light reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s9" value="short-cylindric" value_original="short-cylindric" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character constraint="with age" constraintid="o7057" is_modifier="false" modifier="rarely becoming finely" name="coloration_or_pubescence_or_relief" src="d0_s9" value="striate" value_original="striate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s9" to="0.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7057" name="age" name_original="age" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>exothecial cells mostly short-elongate, mixed with patches of angular isodiametric cells, walls unevenly thickened, trigonous;</text>
      <biological_entity constraint="exothecial" id="o7058" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s10" value="short-elongate" value_original="short-elongate" />
        <character constraint="with patches" constraintid="o7059" is_modifier="false" name="arrangement" src="d0_s10" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o7059" name="patch" name_original="patches" src="d0_s10" type="structure" />
      <biological_entity id="o7060" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s10" value="angular" value_original="angular" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s10" value="isodiametric" value_original="isodiametric" />
      </biological_entity>
      <biological_entity id="o7061" name="wall" name_original="walls" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="unevenly" name="size_or_width" src="d0_s10" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="shape" src="d0_s10" value="trigonous" value_original="trigonous" />
      </biological_entity>
      <relation from="o7059" id="r1969" name="part_of" negation="false" src="d0_s10" to="o7060" />
    </statement>
    <statement id="d0_s11">
      <text>stomata present;</text>
      <biological_entity id="o7062" name="stoma" name_original="stomata" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>peristome patent to squarrose, sometimes twisted, 230–320 µm, orange or yellowish red, coarsely papillose, usually strongly perforated and with uneven margins.</text>
      <biological_entity id="o7063" name="peristome" name_original="peristome" src="d0_s12" type="structure">
        <character char_type="range_value" from="patent" name="orientation" src="d0_s12" to="squarrose" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s12" value="twisted" value_original="twisted" />
        <character char_type="range_value" from="230" from_unit="um" name="some_measurement" src="d0_s12" to="320" to_unit="um" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellowish red" value_original="yellowish red" />
        <character is_modifier="false" modifier="coarsely" name="relief" src="d0_s12" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="usually strongly" name="architecture" src="d0_s12" value="perforated" value_original="perforated" />
      </biological_entity>
      <biological_entity id="o7064" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="true" name="variability" src="d0_s12" value="uneven" value_original="uneven" />
      </biological_entity>
      <relation from="o7063" id="r1970" name="with" negation="false" src="d0_s12" to="o7064" />
    </statement>
    <statement id="d0_s13">
      <text>Spores 8–12 µm, smooth or granulose.</text>
      <biological_entity id="o7065" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character char_type="range_value" from="8" from_unit="um" name="some_measurement" src="d0_s13" to="12" to_unit="um" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="granulose" value_original="granulose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature late spring to early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocks in somewhat shaded habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocks" constraint="in somewhat shaded habitats" />
        <character name="habitat" value="shaded habitats" modifier="somewhat" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (1000-3200 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="3200" to_unit="m" from="1000" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C.; Alaska, Calif., Colo., Idaho, Mich., Nev., Utah, Wash., Wyo.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <discussion>Schistidium confertum is one of the smaller species of the genus. It can be confused with S. dupretii and S. frigidum. Schistidium dupretii is distinguished by its consistently striate, larger, darker, and more cylindrical capsules and its oblate basal marginal laminal cells versus the quadrate or longitudinally short- rectangular marginal cells of S. confertum. Also, S. dupretii lacks the thicker transverse walls that characterize the basal marginal cells of S. confertum. The leaf margins of S. confertum are more weakly recurved than in S. dupretii, and it is usually olivaceous, often with yellowish or brownish tones, whereas S. dupretii is generally evenly brown.</discussion>
  
</bio:treatment>