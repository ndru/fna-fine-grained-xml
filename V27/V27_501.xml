<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">332</other_info_on_meta>
    <other_info_on_meta type="mention_page">333</other_info_on_meta>
    <other_info_on_meta type="mention_page">334</other_info_on_meta>
    <other_info_on_meta type="treatment_page">357</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">fissidentaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">fissidens</taxon_name>
    <taxon_name authority="Wilson &amp; Hooker f." date="1841" rank="species">hyalinus</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. (Hooker)</publication_title>
      <place_in_publication>3: 89, fig. 2. 1841,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fissidentaceae;genus fissidens;species hyalinus</taxon_hierarchy>
    <other_info_on_name type="fna_id">240002061</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 3.5 × 3 mm.</text>
      <biological_entity id="o2400" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s0" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s0" to="3" to_unit="mm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stem unbranched;</text>
      <biological_entity id="o2401" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>axillary hyaline nodules absent;</text>
      <biological_entity constraint="axillary" id="o2402" name="nodule" name_original="nodules" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>central strand absent.</text>
      <biological_entity constraint="central" id="o2403" name="strand" name_original="strand" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves as many as 6 pairs, lanceolate, acute to obtuse, sometimes apiculate, to 2.2 × 0.5 mm;</text>
      <biological_entity id="o2404" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s4" value="apiculate" value_original="apiculate" />
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s4" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s4" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2405" name="pair" name_original="pairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="6" value_original="6" />
      </biological_entity>
      <relation from="o2404" id="r566" name="as many as" negation="false" src="d0_s4" to="o2405" />
    </statement>
    <statement id="d0_s5">
      <text>dorsal lamina narrowed proximally, ending at insertion, not decurrent;</text>
      <biological_entity constraint="dorsal" id="o2406" name="lamina" name_original="lamina" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s5" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>vaginant laminae 1/3–1/2 leaf length, equal;</text>
      <biological_entity constraint="vaginant" id="o2407" name="lamina" name_original="laminae" src="d0_s6" type="structure">
        <character char_type="range_value" from="1/3" name="quantity" src="d0_s6" to="1/2" />
      </biological_entity>
      <biological_entity id="o2408" name="leaf" name_original="leaf" src="d0_s6" type="structure">
        <character is_modifier="false" name="length" src="d0_s6" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>margin entire but sometimes ± serrulate distally, limbate, limbidium 1–2 cells wide, ending before apex, often indistinct on vaginant laminae, limbidial cells 1-stratose;</text>
      <biological_entity id="o2409" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes more or less; distally" name="architecture_or_shape" src="d0_s7" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="limbate" value_original="limbate" />
      </biological_entity>
      <biological_entity id="o2410" name="limbidium" name_original="limbidium" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="2" />
      </biological_entity>
      <biological_entity id="o2411" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="width" src="d0_s7" value="wide" value_original="wide" />
        <character constraint="on vaginant laminae" constraintid="o2413" is_modifier="false" modifier="often" name="prominence" src="d0_s7" value="indistinct" value_original="indistinct" />
      </biological_entity>
      <biological_entity id="o2412" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity constraint="vaginant" id="o2413" name="lamina" name_original="laminae" src="d0_s7" type="structure" />
      <biological_entity id="o2414" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="limbidial" value_original="limbidial" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <relation from="o2411" id="r567" name="ending before" negation="false" src="d0_s7" to="o2412" />
    </statement>
    <statement id="d0_s8">
      <text>ecostate, or costa consisting of very short proximal vestige;</text>
      <biological_entity id="o2415" name="costa" name_original="costa" src="d0_s8" type="structure" />
      <biological_entity constraint="proximal" id="o2416" name="vestige" name_original="vestige" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="very" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
      </biological_entity>
      <relation from="o2415" id="r568" name="consisting of" negation="false" src="d0_s8" to="o2416" />
    </statement>
    <statement id="d0_s9">
      <text>laminal cells 1-stratose, smooth, thin-walled, hexagonal to elongate-hexagonal, 31–67 × 14–34 µm, ± oblong in proximal portions of leaves, particularly in vaginant laminae.</text>
      <biological_entity constraint="leaf" id="o2418" name="portion" name_original="portions" src="d0_s9" type="structure" constraint_original="leaf proximal; leaf" />
      <biological_entity id="o2419" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <biological_entity constraint="vaginant" id="o2420" name="lamina" name_original="laminae" src="d0_s9" type="structure" />
      <relation from="o2418" id="r569" name="part_of" negation="false" src="d0_s9" to="o2419" />
      <relation from="o2417" id="r570" modifier="particularly" name="in" negation="false" src="d0_s9" to="o2420" />
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition rhizautoicous;</text>
      <biological_entity constraint="laminal" id="o2417" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="thin-walled" value_original="thin-walled" />
        <character char_type="range_value" from="hexagonal" name="shape" src="d0_s9" to="elongate-hexagonal" />
        <character char_type="range_value" from="31" from_unit="um" name="length" src="d0_s9" to="67" to_unit="um" />
        <character char_type="range_value" from="14" from_unit="um" name="width" src="d0_s9" to="34" to_unit="um" />
        <character constraint="in proximal portions" constraintid="o2418" is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>perigonia gemmiform, proximal to perichaetial stems, and on elongate stems, to 1.2 mm.</text>
      <biological_entity id="o2421" name="perigonium" name_original="perigonia" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="gemmiform" value_original="gemmiform" />
        <character constraint="to perichaetial, stems" constraintid="o2422, o2423" is_modifier="false" name="position" src="d0_s11" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" notes="" src="d0_s11" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2422" name="perichaetial" name_original="perichaetial" src="d0_s11" type="structure" />
      <biological_entity id="o2423" name="stem" name_original="stems" src="d0_s11" type="structure" />
      <biological_entity id="o2424" name="stem" name_original="stems" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="elongate" value_original="elongate" />
      </biological_entity>
      <relation from="o2421" id="r571" name="on" negation="false" src="d0_s11" to="o2424" />
    </statement>
    <statement id="d0_s12">
      <text>Sporophytes 1 per perichaetium.</text>
      <biological_entity id="o2425" name="sporophyte" name_original="sporophytes" src="d0_s12" type="structure">
        <character constraint="per perichaetium" constraintid="o2426" name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o2426" name="perichaetium" name_original="perichaetium" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Seta to 3 mm.</text>
      <biological_entity id="o2427" name="seta" name_original="seta" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsule theca exserted, erect, radially symmetric, sometimes slightly arcuate, bilaterally symmetric, to 0.4 mm;</text>
      <biological_entity constraint="capsule" id="o2428" name="theca" name_original="theca" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s14" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="sometimes slightly" name="course_or_shape" src="d0_s14" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s14" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s14" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>peristome scariosus-type;</text>
      <biological_entity id="o2429" name="peristome" name_original="peristome" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_ref_taxa" src="d0_s15" value="scariosus-type" value_original="scariosus-type" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>operculum ± as long as theca.</text>
      <biological_entity id="o2430" name="operculum" name_original="operculum" src="d0_s16" type="structure" />
      <biological_entity id="o2431" name="theca" name_original="theca" src="d0_s16" type="structure" />
      <relation from="o2430" id="r572" name="as long as" negation="false" src="d0_s16" to="o2431" />
    </statement>
    <statement id="d0_s17">
      <text>Calyptra mitrate, prorate, 0.4 mm.</text>
      <biological_entity id="o2432" name="calyptra" name_original="calyptra" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="mitrate" value_original="mitrate" />
        <character name="some_measurement" src="d0_s17" unit="mm" value="0.4" value_original="0.4" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Spores 9–13 µm.</text>
      <biological_entity id="o2433" name="spore" name_original="spores" src="d0_s18" type="structure">
        <character char_type="range_value" from="9" from_unit="um" name="some_measurement" src="d0_s18" to="13" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bare, clayey soil in shaded ravines, gorges, and dry stream bottoms</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="clayey soil" modifier="bare" constraint="in shaded ravines , gorges , and dry stream bottoms" />
        <character name="habitat" value="shaded ravines" />
        <character name="habitat" value="gorges" />
        <character name="habitat" value="dry stream bottoms" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ky., La., Ohio, Pa., Tenn.; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>37.</number>
  <discussion>Fissidens hyalinus (Latin hyalus, literally “of glass,” alluding to the transparent, glasslike appearance) is doubtless more common than previously thought. B. E. Lemmon (1966) published a photograph showing a perigonial stem and a perichaetial stem with attached sporophyte. A. C. Risk (2002) gave a thorough account of the distribution and habitat of this tiny moss. Plants cannot be mistaken for any other species in North America, owing to the ecostate leaves and smooth, large, thin-walled laminal cells that shrink considerably when dry.</discussion>
  
</bio:treatment>